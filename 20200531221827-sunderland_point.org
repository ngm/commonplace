:PROPERTIES:
:ID:       3e1e8e45-eb46-4cab-a197-7541d1fab213
:mtime:    20211127120827 20210724222235
:ctime:    20200531221827
:END:
#+TITLE: Sunderland Point

I cycled to a tiny place called [[id:3e1e8e45-eb46-4cab-a197-7541d1fab213][Sunderland Point]] today.

To get there you have to go via a small road through a [[id:6bf128a8-7a13-4c34-9385-79cdfe0ef977][salt marsh]] that may or may not be passable, depending on the tide.  I like salt marshes.

#+ATTR_HTML: :width 100%
[[file:photos/sunderland-point.jpg]]

According to Wikipedia, "Sunderland is unique in the United Kingdom as being the only community to be on the mainland and yet dependent upon tidal access."

It's got a shitty history as being a port that was part of the slave trade.  Sadly there's quite a bit of that history around Lancaster.
