:PROPERTIES:
:ID:       6d207ece-0e7b-4d05-940b-ee91ee722faf
:mtime:    20211127120852 20210724222235
:ctime:    20200801131349
:END:
#+title: Small is Beautiful

- by [[id:351d0c3c-7862-429d-b29b-61d61d19231c][E. F. Schumacher]]
 
The first chapter of Small is Beautiful is outlining the problem with production.  The same story: we are overusing natural and human resources, treating them as capital and not income, and heading for a catastrophe as a result.  That we need to make drastic changes to how our economy functions.

The thing is, this was written in 1973. Nothing has changed in this regard - in fact it has gotten worse. Makes me think that things like the latest popular cover versions of the same tune (e.g. Doughnut Economics) are not going to change anything either, no matter how cogent and damning. 

What will *actually* bring about change?


