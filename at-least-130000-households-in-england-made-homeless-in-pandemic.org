:PROPERTIES:
:ID:       8b7a81a2-417a-413e-afd5-4add32e992a8
:mtime:    20211127120909 20210724222235
:ctime:    20210724222235
:END:
#+title: At least 130,000 households in England made homeless in pandemic
#+CREATED: [2021-06-13 Sun 12:39]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ URL :: https://www.theguardian.com/society/2021/jun/13/at-least-130000-households-in-england-made-homeless-in-pandemic
+ Publisher :: [[id:7f5a7ae4-7af4-44e8-876f-d1d83fb4c43d][The Guardian]]
+ Summary ::
#+begin_quote
While ban on evictions protected some people, domestic abuse and loss of temporary accommodation were common triggers for [[id:f1d765e1-2409-4e45-8015-e88e4918da25][homelessness]]
#+end_quote

#+begin_quote
“During the pandemic, the most common triggers for homelessness were no longer being able to stay with friends or family, losing a private tenancy, and domestic abuse.”
#+end_quote

#+begin_quote
The end of the [[id:4be01805-d1b9-405b-b959-793b24f5f54f][eviction ban]] in England will undoubtedly have an impact on the number of people turning to the council for help, but we are yet to see the end result of this change in policy.
#+end_quote

The eviction ban ended 1st June 2021 in the UK.
