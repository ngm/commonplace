:PROPERTIES:
:ID:       9f6bede9-8f2d-4f44-aa40-4ed1bf8eb1b4
:mtime:    20221028110253
:ctime:    20221028110253
:END:
#+TITLE: STS
#+CREATED: [2022-10-28 Fri]
#+LAST_MODIFIED: [2022-10-28 Fri 11:33]

Sometimes [[id:40209eac-a8de-4e73-b764-8d698d6fdf50][Science, technology and society]], sometimes [[id:b746a093-d6b6-4a6b-a9ed-f461018b2f95][Science and technology studies]].

Critical studies of the impact of science and technology on society.

* Technology and society

#+begin_quote
technologies are the product not only of technical work but also of social negotiations

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote

#+begin_quote
technologies are not value-neutral but rather are a product of the historical contexts in which they are made

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote

#+begin_quote
Technologies are historical texts. When we read them, we are able to read history

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote

#+begin_quote
political innovation can spur technological innovation.

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote

#+begin_quote
technology can shape the path of political history by making certain actions possible

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote

#+begin_quote
historical readings of technology can make visible the complexities internal to a political project

-- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
#+end_quote
