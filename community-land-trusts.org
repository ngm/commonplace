:PROPERTIES:
:ID:       84330e6d-1d4b-4992-b58a-516933094231
:mtime:    20220725230424 20211127120904 20210724222235
:ctime:    20200822150527
:END:
#+title: Community land trusts

#+begin_quote
Community land trusts are a great way to decommodify land, take land off speculative markets permanently, and mutualise control and benefits of real estate. CLTs help keep land under local control and allow it to be used for socially necessary purposes (e.g. organic local food) rather than for marketable purposes favoured by outside investors and markets.

-- David Bollier, Stir to Action Issue 30
#+end_quote

#+begin_quote
Community Land Trusts (CLTs) and similar cooperative structures like this are grass roots organisations tackling local land injustices. They are a fair and transparent way of grappling control of houses, shops, industrial land and public spaces away from the forces of gentrification. Giving local groups ownership, a say and rights to the benefits of what goes on in our neighbourhood. 

- [[id:0e6dee30-de53-4576-ae70-7450e7279655][Stokes Croft Land Trust]]
#+end_quote

#+begin_quote
A Community Land Trust (CLT) is a mechanism for democratic ownership of land by a local community. Land is taken out of the market and separated from the possibility of speculation so that the impact of land price inflation is removed, thereby enabling long-term affordable and sustainable local development.

-- [[https://www.fundsurfer.com/community-share/stokes-croft-land-trust][Help the Stokes Croft Land Trust purchase its first building and bring it int...]] 
#+end_quote

#+begin_quote
Community Land Trusts allow local people to ‘manage the commons’ democratically. Compared to the dominance of private and public ownership of land, ‘common land’ in the UK constitutes under eight per cent of the land area and most of this is ‘waste land'.  Reclaiming and extending the ‘[[id:0cb7cb18-cbb2-4060-bf7b-4ef6497dc375][commons]]’ is possible when people act collectively.

-- [[https://www.fundsurfer.com/community-share/stokes-croft-land-trust][Help the Stokes Croft Land Trust purchase its first building and bring it int...]] 
#+end_quote

* History 

#+begin_quote
The first CLTs emerged from the [[id:c80897af-6926-44c1-829b-4588d274fdff][Civil Rights Movement]] in the USA during the 1970s, and the idea has now spread round the world.

- [[id:0e6dee30-de53-4576-ae70-7450e7279655][Stokes Croft Land Trust]]
#+end_quote

* Examples

- [[id:0e6dee30-de53-4576-ae70-7450e7279655][Stokes Croft Land Trust]]

