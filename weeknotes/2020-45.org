:PROPERTIES:
:ID:       e9882a08-e6b1-487e-90a0-9cd70eb03af7
:mtime:    20211127121006 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Weeknotes 2020-45

- got interested in [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]]
  - made a few tweaks to my PKM ([[id:555611be-76b4-4c72-bd62-26e1c29c7d87][Flock]]) based on that
    - [[id:863e959e-4774-4f3c-b448-8fed7f7584a4][Placing my daily logs in a journal subfolder]]
    - start using [[id:9a560d90-a67a-427b-9823-9e67597a475d][person]] tag
- thought about my [[id:213af944-a976-4a91-9079-73846038d330][Music listening strategy]] and bought some tracks from [[id:0862318a-767e-4e1d-9c44-de9e43cb67e5][Bandcamp]]
- was very happy that Trump lost
- watched a webinar on [[id:646d5371-69ca-4f8c-8afe-ebcb584ab17d][Independent researcher]]s and [[id:d47158bb-bf5b-48fc-af81-fe6571a9d7f2][Tools for thought]].
- started reading about [[id:703b1d84-d999-430f-9331-85d39a653ae4][Ton's PKM]]
- discovered I have an [[id:a4801721-9e96-459e-8bab-033a2071aeaf][Antilibrary of articles]]
- thought a bit more about [[id:4db92dd5-f837-4ecb-b0cc-f97cb3d6f5ce][Vectoralism]]
- played around with [[id:a6662592-f7fe-49b0-b2d4-79f91b951b93][PlantUML for weeknote diagrams]].  Got somewhere with it... but to be honest, might just be easier with LibreOffice :/

  #+begin_src plantuml :file yakshave.png
    node yak
    node shave
    yak --> shave
  #+end_src

  #+RESULTS:
  [[file:yakshave.png]]
