:PROPERTIES:
:ID:       9c5143ea-a6a5-4b90-b0f7-a9c5c3afbed6
:mtime:    20220731220215
:ctime:    20220731220215
:END:
#+TITLE: the importance of joining class struggle with climate action
#+CREATED: [2022-07-31 Sun]
#+LAST_MODIFIED: [2022-07-31 Sun 22:02]

The importance of joining [[id:5c06724b-1bbe-42f7-b611-896c83222936][class struggle]] with [[id:6cc6be6a-0503-4455-b805-21dcc26ebb4f][climate action]].
