:PROPERTIES:
:ID:       c59d92ab-23ca-4ab4-8334-456f0d27ca93
:ROAM_ALIASES: "Community garden"
:mtime:    20211127120943 20210724222235
:ctime:    20200328141153
:END:
#+TITLE: community of gardens

The idea is that to a small degree we might be responsible for the upkeep of others' sites, such that our [[id:1316026d-4c00-413e-a825-e983ab9ddbbe][digital gardens]] are not quite so fenced off from each other.  It's sounds like something more than simply commenting on others' posts. It's a nice phrase, kind of a form of [[id:ca4d3b80-af2c-4897-a95f-0b9eb21340eb][networked learning]]. 

   #+begin_quote
The garden metaphor is a compelling vision for what a blog can be. It implies that our thoughts can grow over time with the right kind of nurturing care. 

[...] But sometimes it feels as though these gardens are enclosed.    Sure, a blog might allow comments, but this feels as though we are operating on a layer above the soil. Are others planting anything new, tending to the weeds in our garden, or are they talking to us from the fence that separates our garden from them?

-- [[https://blog.cjeller.site/community-of-gardens][Community of Gardens — CJ Eller]]
   #+end_quote

