:PROPERTIES:
:ID:       9f022ffd-70be-429a-8f96-835c152d31dc
:mtime:    20211127120919 20210724222235
:ctime:    20210724222235
:END:
#+title: An ethics of agency

Chris Webber's ethical framework.

#+begin_quote
the foundation of maximizing [[id:47b5a145-cfcd-433a-86af-87139e2578f3][agency]] "for you, for me, for everyone" and minimizing subjection

-- [[https://fossandcrafts.org/episodes/11-an-ethics-of-agency.html][11: An Ethics of Agency -- FOSS and Crafts]] 
#+end_quote

Chris wanted to rework the four (/three) free software freedoms in to a consequentialist framework (instead of Kantian).

Chris makes a link between ethics of agency and the [[id:bd7cb778-391e-4338-a6e9-40544afb9df2][Declaration of Digital Autonomy]].  I think he says the declaration is more principles, and the ethics of agency is more of a framework?  Something like that. 
