:PROPERTIES:
:ID:       8a41f43e-71f4-447e-83d0-14d9158861a9
:mtime:    20211127120908 20210724222235
:ctime:    20200717212719
:END:
#+title: This is Fine: Optimism and Emergency in the Decentralised Network

- https://newdesigncongress.org/en/pub/this-is-fine/

 Decentralisation has occurred and been defeated before.  Putting faith in just the protocols won't work.
