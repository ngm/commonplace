:PROPERTIES:
:ID:       3ebcb90f-6007-43f0-9ad5-e297564589ff
:mtime:    20211127120827 20210724222235
:ctime:    20210724222235
:END:
#+title: Provisioning
#+CREATED: [2021-05-29 Sat 11:09]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
a nonmarket version of production

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
Trained to see the dismemberment of complex production processes as efficient and natural, and its segregation from consumption as a core premise of “the economy,” economists tend to overlook a more elegant, practical approach to provisioning — [[id:a7ee13af-296f-478b-bbb3-3d7f60547b33][commoning]].

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
The goal of Provisioning through Commons is not maximum efficiency, profit, or higher Gross Domestic Product. It aims simply to meet needs and provide a stable, fair, satisfying, and ecologically minded way of life.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
