:PROPERTIES:
:ID:       1aeb8124-d741-4a4e-ba87-1ead2ffb8abe
:mtime:    20231012213622 20211127120808 20211025195835
:ctime:    20211025195835
:END:
#+TITLE: commons.hour number 2
#+CREATED: [2021-10-25 Mon]
#+LAST_MODIFIED: [2023-10-12 Thu 21:36]

#+begin_quote
the political economy of *community organising in digital spaces*.
#+end_quote

#+begin_quote
The ways that commitments and contributions to organising may hinge on economic privilege; and the importance of care work as a radical way into this tangle of challenges.
#+end_quote

- [[id:8d077133-8eba-42e6-b770-83ddd1f80bb9][Ela Kagel]]
  - [[id:a4e73057-ef27-455f-b357-2de99462ca3d][Supermarkt Berlin]]
 
* How to foster a culture of openness?

#+DOWNLOADED: screenshot @ 2021-10-25 19:23:56
[[file:2021-10-25_19-23-56_screenshot.png]]

** Economic

#+DOWNLOADED: screenshot @ 2021-10-25 19:25:05
[[file:2021-10-25_19-25-05_screenshot.png]]

activist commitments.
giving up personal time in order to engage.
child care, collective meals.
be very clear about pro bono work. time banking.  how is value accounted for in communities.
measuring value flows - where is value being generated in communities?
paid work opportunities.

** safety.

#+DOWNLOADED: screenshot @ 2021-10-25 19:29:52
[[file:2021-10-25_19-29-52_screenshot.png]]
 meeting in a safe space.
 privacy friendly.
 code of conduct (a living document)

** online governance

#+DOWNLOADED: screenshot @ 2021-10-25 19:33:09
[[file:2021-10-25_19-33-09_screenshot.png]]


** collective leadership

** privilege

#+DOWNLOADED: http://i.imgur.com/zfoChuA.png @ 2021-10-25 19:35:21
[[file:2021-10-25_19-35-21_zfoChuA.png]]
   
   
** time
#+DOWNLOADED: screenshot @ 2021-10-25 19:36:50
[[file:2021-10-25_19-36-50_screenshot.png]]
- levels of involvement

** language
   maybe have a main language for intro, but breakout rooms in different languages.
   (language may not just be about national languages, but understanding of concepts)
   the facilitators need to be up front about acknolwedging difficults of language barriers, and making sure that things are done to help include others.

** participation fatigue
   how to manage encouraging feedback from everyone, but avoid fatigue?
  
