:PROPERTIES:
:ID:       bd618f8a-5716-4214-9051-40964ce90f40
:mtime:    20211127120938 20210727175914
:ctime:    20210727175914
:END:
#+title: Why Debian?
#+CREATED: [2021-07-25 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

I am very familiar with apt for package management.

I've used Ubuntu and Mint in recent years, which are built on top of Debian.

[[id:d819826b-1a39-437a-a59d-8aa61fdbc63b][Debian]] seems to have good community guidelines and governance structure.
