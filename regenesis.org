:PROPERTIES:
:ID:       37a6b0bf-fdb3-41bd-a3ff-4c2e8903ce0c
:mtime:    20220815165219
:ctime:    20220815165219
:END:
#+TITLE: Regenesis
#+CREATED: [2022-08-15 Mon]
#+LAST_MODIFIED: [2022-08-15 Mon 17:03]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Subtitle :: "Feeding the World Without Devouring the Planet"
+ Author :: [[id:e622ffb8-7d2f-4c18-8633-d7b86b0af38d][George Monbiot]]

About the [[id:0342d44f-fc0b-47e2-826b-1af8f299df22][global food system]], the need to reduce the amount of [[id:c648b4ff-a614-4570-8aec-941108c18b87][farming]] we do, and alternatives.

Listened to George on a few podcasts now.  This article gives a good overview of his arguments in this book I think: [[https://www.newsroom.co.nz/rod-oram-if-everybody-ate-the-average-nzers-diet-wed-need-another-planet-to-sustain-us][Rod Oram: 'If everybody ate the average NZer’s diet we'd need another planet ...]]


* Reviews

- [[https://inquisitivebiologist.com/2022/06/01/book-review-regenesis-feeding-the-world-without-devouring-the-planet/][Book review – Regenesis: Feeding the World Without Devouring the Planet | The...]] 


