:PROPERTIES:
:ID:       2be448a3-fa31-4479-8df1-9766d233f322
:mtime:    20211127120818 20210724222235
:ctime:    20210724222235
:END:
#+title: Community Wealth Building 2.0
#+CREATED: [2021-02-25 Thu 18:46]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Event launching an updated strategy for the [[id:8572adc8-6416-4f24-bb33-f74bdb0ff88b][community wealth building]] model in Preston (AKA the [[id:174ed8ef-0f25-4a92-bdf3-c06dcaf6493c][Preston Model]]).

- [[https://www.youtube.com/watch?v=qs1jQyZxRLY&feature=youtu.be][Community Wealth Building 2.0: Leading resilience and recovery in Preston]]

* Notes from Paul Hayes
  
- [[https://docs.google.com/document/d/1LXVy3ZZ3XG-UvFWJLOkPqOi4BCZQTe2ccd0mtjhx7iY/edit][Preston City Council: “Community Wealth Building 2 - Google Docs]] 
  
Notes -  Preston City Council: “Community Wealth Building (CWB) 2.0: Leading Resilience and Recovery in Preston”

Thursday 25th February 2021 

** Councillor Freddie Bailey - Preston Cabinet member for CWB.

Claims for Preston Model so far:
Increased employment and self employment above NW average
Increased wage rises above NW average - stimulated by major push for real living wage for employees in council/anchor institutions. 
Local Public Procurement supporting upskilling of Preston workforce.
Providing opportunities for local/regional agenda
Helps reduce carbon footprint
What are new objectives in Preston Model 2.0?:
Changing the culture within the city - more money and resources going into CWB 
Supporting job security as part of pandemic recovery, including via targeted local public sector recruitment.
More cooperatives and promoting workplace democracy
Having a local community bank in 5 years. 

** J Phillip Thompson - Deputy Mayor for Strategy, New York City.
CWB looks to explicitly include minorities and women
Seeks to tackle inequalities and hoarding of wealth
 Addresses issues of fairness in communities feeling ‘left behind’. Looks at new ways to rebuild an economy that promotes social cohesion.
Framing the narrative - solidarity, cooperation and working together the only way forward for minority and disadvantaged communities. Examples of 19th century cooperatives.
Central vision to unite differing perspectives - need workers and community reps in decision making positions at all levels across the economy. Can’t have democracy without economic democracy. 
Pandemic responses - offering technical assistance for businesses wanting to sell to their employees. Bypassing traditional financing structures. Mandating community based businesses have a place in any property sales/developments. 
Lobbying for increases in minimum wage.
Looking to collaborate with other cities/places.
Can’t separate CWB from community politics - need to bring people together to jointly develop solutions.
Seeing local government more as an economic engine.
How to persuade people that CWB is worth it - take stakeholders on learning journeys - going to see places where CWB is happening and talk to people there.

** Panel - Neil McInroy (CLES), John McDonnell MP, Rachel Shabi (The Guardian), Cllr Matthew Brown (Preston Leader) 

   Defining features of an economy - understanding where does the wealth go, and who gets it? Not just wages, but finance and capital. 
Democratising and better distributing wealth (not just wages) through remaking the economy is what CWB is seeking to achieve.
Public sector Investment and economic development needs to change people's lives, and increase ownership - not support and encourage extractive businesses.
“It is a direct attack on those who have wealth and power, and keep it to themselves”
Preventing money and profits being taken out of the economy by externally based companies.
Supporting local businesses being exploited by the finance sector through high interest rates - through access to low cost credit and support locally. 
CWB has given back a sense of agency to councils and communities.
Other projects for Preston - community owned cinema, local bank, community land trusts. Local Taxi and food co-operatives.
More about quality of outcomes than equality of opportunity.
Urgent post pandemic issues - local recruitment by partners/anchor institutions from deprived communities/ those who have lost their jobs.
Economic and workplace and a say in economic strategy to communities will keep focus. 
Community land ownership in Scotland given as an example - if communities own land they make it work more effectively.
What is the ‘best deal’ in procurement and spend? - not financial, but social, environmental and community benefits should be more important. 
Collectivism may be coming back, through the rise of mutual aid in the pandemic. An opportunity to take forward.
Need to learn for experiments across the country and share information.
Benefits in influencing procurement across all local institutions.

** James Moore, North West Mutual Bank

   Setting up a North West community bank covering ‘from Wirral to Scottish Borders’
Looking at the German model of local banks (historical model in the UK is the Trustee Savings Bank before de-mutualisation).
Community banks should be owned by their customers. One member, one vote for savers.
Community banks have more trust from savers and borrowers. The bank has to be competent and also won’t ‘do you over’.
Current UK banks are seen as competent, but remote, not aligned with customers and not benevolent.
Many of the things people or places want to do require money. Banking system is an important part of this picture.
Local branches staffed by local people are vital - not decisions made by people in the City of London. 

** Panel - Sharon Robson (NHS procurement), Steve Fogg (Lancashire LEP), Ebrahim Adia (University Central Lancashire) Neetal Parekh (Community activist)
   
Public sector can transform services for community and maximise social benefit.
NHS is complex, but can change procurement.
Covid was an opportunity to change procurement policies. 
How to ensure CWB engages and retains all stakeholders? Recognise that there are multiple inequalities, stress on one partner can be relieved by intervening elsewhere (‘underlying issues’/ ‘wider determinants’). 
Need to collectively imagine a future that benefits all partners. Give stakeholders a reason to come together in a place.
Role of universities -need to see themselves as civic - local cultural, social and economic actors. Have significant spending power, locally and regionally. Also widening participation should be pursued.
Formula for places - create the wealth, retrain the wealth, fairly distribute the wealth.
How do you measure success of community inclusion and make sure CWB reaches everyone? Making agencies accessible - going to communities not having them come to you. Leaning again from pandemic - reaching out to grass roots communities. 
CWB should be a ‘ground up’ movement. Hard to reach communities need to identify with CWB.
Community and faith centre can build trust and support. Preston model literature translated into local minority community languages.  
Bring different stakeholders (business, education etc) together in the same arenas. 

** Paul Johnson (Preston Businessman)
Why should businesses support CWB? Better off doing business with people you know.
Local tendering supported local businesses, also improved quality. 10% of procurement score linked to the aims of CWB. An employer gave a £5 daily spend voucher for local shops to employees working in the city centre. 

** Cllr Matthew Brown - closing remarks
Challenge now is to bring this to scale.
CWB as a label helps create conversation and belief of ‘it doesn’t happen to be this way’.
‘Not an anti business agenda, but an anti business as usual agenda’
Small businesses are treated badly by big business as well.
Recovery is an opportunity for change. 

 
 


