:PROPERTIES:
:ID:       3a5f4fbc-7842-418a-bd47-97865574f345
:mtime:    20211127120825 20210724222235
:ctime:    20200630195923
:END:
#+title: Machine-learning algorithms

Reinforcing good behaviour, ignoring bad, and giving enough practice.

#+begin_quote
You give the machine data, a goal and feedback when it's on the right track - and leave it to work out the best way of achieving the end.

-- [[id:26bfd69b-6760-4a52-a6fc-28093ab1e046][Hello World]] 
#+end_quote

