:PROPERTIES:
:ID:       0b998139-15eb-475c-acbf-546c05fcec88
:ROAM_REFS: https://www.theguardian.com/technology/2020/nov/27/new-uk-tech-regulator-to-limit-power-of-google-and-facebook
:mtime:    20211127120759 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: New UK tech regulator to limit power of Google and Facebook
#+CREATED: [2020-11-27 Fri 09:25] 
#+LAST_MODIFIED: [2020-11-27 Fri 09:55]

[[https://www.theguardian.com/technology/2020/nov/27/new-uk-tech-regulator-to-limit-power-of-google-and-facebook][New UK tech regulator to limit power of Google and Facebook]]

Summary: The framing of this is all wrong.  It's all about innovation, competition, consumer choice, growth.  It should be about [[id:98822387-9731-4f34-bb97-5971f627b1a2][liberation]], [[id:9b63132d-58cc-449d-90b7-08e4af649e8d][user freedom]], [[id:47b5a145-cfcd-433a-86af-87139e2578f3][agency]] and [[id:89446ec8-bf0d-4df8-9f17-8fc4638de8dd][sustainability]].  Sounds like it'll be a weak opt-in 'code' that just moves the deckchairs around while the ship is sinking.

#+begin_quote
Alok Sharma says dominance of a few big companies hurts innovation and curtails customer choice
#+end_quote

Bullshit framing of innovation and customer choice.  See The Maintainers for this fetishisation of innovation.

#+begin_quote
the Competition and Markets Authority (CMA) will gain a dedicated Digital Markets Unit, empowered to write and enforce a new code of practice on technology companies which will set out the limits of acceptable behaviour.
#+end_quote

A code of practice sounds a bit weak.

#+begin_quote
“But the dominance of just a few big tech companies is leading to less innovation, higher advertising prices and less choice and control for consumers. Our new, pro-competition regime for digital markets will ensure consumers have choice, and mean smaller firms aren’t pushed out.”
#+end_quote

Why is everything in the language of business opportunity?

#+begin_quote
The code will seek to mediate between platforms and news publishers, for instance, to try to ensure they are able to monetise their content; it may also require platforms to give consumers a choice over whether to receive personalised advertising, or *force them to work harder to improve how they operate with rival platforms.*
#+end_quote

Interoperability would be good.

#+begin_quote
“Only through a new pro-competition regulatory regime can we tackle the market power of tech giants like Facebook and Google and ensure that businesses and consumers are protected."
#+end_quote

Pro-competition, such a poor framing.  [[id:9b63132d-58cc-449d-90b7-08e4af649e8d][User freedom]] might be a better one.

#+begin_quote
It’s time to address that and unleash a new age of tech growth.
#+end_quote

"Tech growth" - that's the last thing we need.
