:PROPERTIES:
:ID:       1948bc3b-3360-4cdd-8ede-337971bbb931
:mtime:    20211127120807 20210724222235
:ctime:    20210724222235
:END:
#+title: Privacy, censorship and social media- The Case for a Common Platform
#+CREATED: [2021-04-11 Sun 12:46]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://www.mutualinterest.coop/2021/03/privacy-censorship-and-social-media-the-case-for-a-common-platform
+ Authors :: [[id:a8dcc390-8d2b-49c4-8bd4-eabe3441ff57][Thomas Hanna]] / [[id:b008c798-e75f-40b3-bbfa-fc58fea991f2][Dan Hind]] / [[id:dd880076-8dd8-4bac-a6ed-82297c7b0c44][Mathew Lawrence]]
+ Publisher :: [[id:60a66ca5-1f56-42af-b2a5-b0d10202b9dd][Mutual Interest Media]]

#+begin_quote
The events of January 6th, and its aftermath requires us to step back and ask how we want to organise both the social media platforms, and the broader digital economy to which they belong.
#+end_quote

Specifically taking the way in which the storming of the capitol was incited as an example of a problem with social media platforms.

#+begin_quote
establish a digital sector that is substantially public and democratic in character.
#+end_quote

Public digital sector as the alternative.

#+begin_quote
three core institutional approaches that could be enacted independently according to specific political and social contexts, or jointly as part of a linked eco-system approach: a [[id:9289197d-a04c-420f-a4e8-02af17ed6cd1][Public Platform Accelerator]] (PPA), public “[[id:347fda9e-f9b1-41f8-a96a-37d037236655][data trusts]]”, and a network of [[id:8ac5b2b5-fc63-424f-af1a-8342a885ce02][Public Digital Cooperative]]s.
#+end_quote

^ The main pillars of this public digital sector.

#+begin_quote
We can, and must, build both the case for radical change and pieces of a new digital economy in the here and now. In this the new generation of co-operators are going to be crucial. *Every worker in the digital economy who refuses the siren call of the venture capitalists has it in them to make another world.*
#+end_quote

^ I like this rallying cry...

Summary: Interesting ideas. Public platform accelerator. Data trusts. Public digital cooperatives.

I wonder what each of these mean in practice exactly?  This is probably covered in [[id:9d3a969e-bb7d-44bd-be11-c73d93d774a2][A Common Platform:Reimagining Data and Platforms]].

I also wonder if it can be made more municipal.  It sounds a bit state-oriented from this article.  But I remember Dan Hind's podcast about the British Digital Cooperative having quite a municipal / local side to it.
