:PROPERTIES:
:ID:       baba3fae-8a67-4809-ba8c-21d272f3ca98
:mtime:    20211127120937 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Rediscovering the Small Web

-- [[https://neustadt.fr/essays/the-small-web/][Rediscovering the Small Web - Neustadt.fr]] 

* Modern gatekeepers
  
#+begin_quote
Today, most of the time spent on the web is either on a small number of very dominant platforms like Facebook and LinkedIn, or mediated through them. 
#+end_quote

#+begin_quote
There is so much "content" that is constantly pushed at you as a user that *very few of us actually venture out to browse and explore anymore*. We simply don't need to. But these platforms thrive on "user engagement"—likes, comments, clicks and shares—and their algorithms are more likely to give visibility to content that generates this behavior. *Instead of browsing, the web is for many an endless and often overwhelming stream of content and commentary* picked out by algorithms based on what they think you already like and will engage with. *It's the opposite of exploration.* 
#+end_quote

#+begin_quote
When you're not receiving information passively and instead actually actively looking for something, you most likely have the same singular point of entry as about 87% of all web users: Google.
#+end_quote

#+begin_quote
the smaller, amateur web gets hidden in the shadows of web professionals who design around specific keywords and audiences.
#+end_quote

* Commercial web

#+begin_quote
There has always been a place for commerce and marketing on the web. [...]  But today's web is mostly commercial. The smaller web of individuals has neither the resources nor the will to compete for visibility and audience the way the commercial web does. [...] Compared to the small web, this commercial web is tactical and predatory.
#+end_quote

* Product-oriented websites
  
 #+begin_quote
 But the web is not always "profit-oriented" and it certainly does not need to be "user-centric" (and I say this as a UX consultant).
 #+end_quote
  
#+begin_quote
It is worth remembering a website does not have to be a product; it can also be art. The web is also a creative and cultural space that need not confine itself to the conventions defined by commercial product design and marketing.
#+end_quote

* The Web as a Creative Space

#+begin_quote
A painter wouldn’t add more red to her painting or change the composition because market data showed that people liked it better. It’s her creative vision; some people might like it, others might not. But it is her creation, with her own rules. The question of "performance" is simply irrelevant. It's the same thing with the small web.
#+end_quote

Agreed, with the one exception of accessibility.  That's important for inclusivity.

#+begin_quote
If the commercial web is "industrial", you could say that the small web is "artisanal". One is not better than the other. 
#+end_quote
