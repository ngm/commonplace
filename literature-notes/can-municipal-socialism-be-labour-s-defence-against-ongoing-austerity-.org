:PROPERTIES:
:ID:       6b8c931e-ce5c-4b2d-9921-8f7ac84ec739
:mtime:    20211127120850 20210724222235
:ctime:    20210724222235
:END:
#+title: Can municipal socialism be Labour’s defence against ongoing austerity?
#+CREATED: [2021-04-10 Sat 18:02]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://www.mutualinterest.coop/2020/03/can-municipal-socialism-be-labours-defence-against-ongoing-austerity

#+begin_quote
Key to these models is the belief that community and worker, not state, ownership is the key to building a better economy. 
#+end_quote

#+begin_quote
the focus is the growth of the co-operative economy as a way to fight off the worst effects of austerity by helping improve the economy and increased local engagement through democratisation of the economy.
#+end_quote

#+begin_quote
A more exclusive focus on building the co-operative is being started in Greater Manchester where Metro Mayor Andy Burnham has launched the [[file:greater-manchester-cooperative-commission.org][Greater Manchester co-operative agency]].
#+end_quote

#+begin_quote
in [[file:emilia-romagna.org][Emilia Romagna]] in Italy co-operative enterprises generate close to 40% of GDP.
#+end_quote

#+begin_quote
Emilia Romagna is helped by a much better legal framework for the establishment and development of co-operatives. The [[file:marcora-law.org][Marcora Law]] gives workers the right to buy and convert a company should it plan to close, and has been credited to have saved around 14,500 jobs in the country. 
#+end_quote
