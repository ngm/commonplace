:PROPERTIES:
:ID:       6c878292-d5c5-4650-974d-74f25f45a3b9
:mtime:    20211127120851 20210724222235
:ctime:    20210724222235
:END:
#+title: Analytics

Some non-GA options:

- umami https://umami.is​
- matomo https://matomo.org/
- plausible https://plausible.io/
- fathom https://usefathom.com
- simpleanalytics https://simpleanalytics.com/
- cabin https://withcabin.com
- goatcounter https://www.goatcounter.com/ (h/t http://mtsolitary.com on Digital Gardeners Telegram)
