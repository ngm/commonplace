:PROPERTIES:
:ID:       e3268f3b-fabb-4ea6-80ea-b1246c648387
:mtime:    20230923165037
:ctime:    20230923165037
:END:
#+TITLE: The Nature of Technology
#+CREATED: [2023-09-23 Sat]
#+LAST_MODIFIED: [2023-09-23 Sat 16:51]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Author :: [[id:baa01c28-bbf3-4005-8c76-12988771095e][W. Brian Arthur]]

Handy overview of the book: [[http://www.the-vital-edge.com/the-nature-of-technology/][The Nature of Technology: What It Is and How It Evolves (My Notes)]]

Three pr8nciples:


- All technologies are combinations of elements.
- These elements themselves are technologies.
- All technologies use phenomena to some purpose
