:PROPERTIES:
:ID:       dbd42956-8186-4c7a-aa9c-fd82af37fdcb
:mtime:    20211127120958 20210724222235
:ctime:    20200321214219
:END:
#+TITLE: Airline bailouts

#+begin_quote
UK airlines are asking for a government bailout. The terms should be as follows: (1) money goes first to workers to cover lost wages; (2) purchase happens at discounted rate; (3) government gets a commanding share, so we can manage the industry in line with climate objectives
  SCHEDULED: <2020-03-17 Tue>



https://twitter.com/jasonhickel/status/1239506700190744576
#+end_quote
