:PROPERTIES:
:ID:       7fd75801-1a7e-433c-a465-7e7c141a2814
:mtime:    20221121155043 20221016165407 20211127120901 20210724222235
:ctime:    20210724222235
:END:
#+title: Anti-trust and big tech
#+CREATED: [2021-04-11 Sun 14:17]
#+LAST_MODIFIED: [2022-11-21 Mon 15:50]

[[id:eefd8420-fa47-4239-a0f3-ad6ee3d0ed7e][Anti-trust]] won't necessarily work in breaking up [[id:4f2e9822-8e1e-4599-92eb-e9fc5e948bd5][Big tech]].

#+begin_quote
simply increasing competition amongst largish for-profit corporations with the same surveillance capitalist business model is unlikely to meaningfully address the major underlying problems and outcomes of the platform economy. In fact, in some instances, around things like data collection, surveillance, and privacy, increased competition may only make things worse.

-- [[id:1948bc3b-3360-4cdd-8ede-337971bbb931][Privacy, censorship and social media- The Case for a Common Platform]]
#+end_quote

#+begin_quote
Antitrust reformism is particularly problematic because it assumes the problem of the digital economy is merely the size and “unfair practices” of big companies rather than digital capitalism itself.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote

#+begin_quote
Antitrust advocates argue that monopolies distort an otherwise ideal capitalist system and that what is needed is a level playing field for everyone to compete.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote
