:PROPERTIES:
:ID:       a4cbfb42-9cb9-4fee-81ed-981025b51653
:mtime:    20211127120924 20210724222235
:ctime:    20210724222235
:END:
#+title: Netocracy

#+begin_quote
the most valuable form of economic wealth is a network of good connections, through which flow information and influence. 

-- [[http://thoughtstorms.info/view/NetoCracy]] 
#+end_quote

Stumbled on via [[id:390b2d01-645f-49e4-9ce9-36755f6b54c3][Phil Jones]]. 

Describing some of the current state of the world dominated by the Internet. 

Philosophically inspired by [[id:bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa][Deleuze & Guattari]]?

Any relationship to McKenzie Wark [[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]] stuff?

