:PROPERTIES:
:ID:       9a48db2a-c31f-4a83-b931-e7c1c9d69a84
:mtime:    20230912191710
:ctime:    20230912191710
:END:
#+TITLE: The environmental impact of a PlayStation 4
#+CREATED: [2023-09-12 Tue]
#+LAST_MODIFIED: [2023-09-12 Tue 19:37]

+ URL :: https://www.theverge.com/2019/12/5/20985330/ps4-sony-playstation-environmental-impact-carbon-footprint-manufacturing-25-anniversary

"Deconstructing Sony’s video game console is a mind-bending trip into carbon-intensive global electronics"

Carbon-intensive global electronics.

#+begin_quote
it occurs to me that the PlayStation 4 has the most dazzling and problematic parts of global capitalism purring in unison.
#+end_quote

#+begin_quote
It is an exquisite, leanly designed machine pulsing with the exploitation of Earth and its people.
#+end_quote


#+begin_quote
In an effort to explore both the environmental and human impacts of Sony’s current video game console, I decided to take one apart. Under its plastic hood, I discovered a machine that spans continents and deep time, touches thousands of lives (for better and worse), and leaves an indelible, measurable stain on Earth and its atmosphere.
#+end_quote

- The case is made from [[id:1dffdd10-2b12-4774-a719-e20aeb89974d][ABS]] (so is Lego apparently)
- steel
- POM (cogs for the blue ray drive)
- aluminium (heatsink)
- gold (PCB)
- tin (PCB)
- copper (PCB)
- neodynium
- [[id:dbfb5e1f-60dd-446f-a2be-9711e755596e][lithium]] (battery in controller)

 #+begin_quote
 It’s quite possible that the tin-based solder in your PlayStation 4, which is the glue holding its vital computerized parts together, originated from such deep exploitation.
 #+end_quote

#+begin_quote
“You miniaturize the product, and you maximize your carbon footprint,” Barlow said. “Even though it’s very little material, the processing energy is absolutely huge, and the end of life is almost impossible. It’s basically single-use.”
#+end_quote

#+begin_quote
The equivalent of 89 kilograms of carbon dioxide is emitted into the atmosphere with the production and transportation of every PlayStation 4.
#+end_quote

#+begin_quote
Since the PlayStation 4’s release in 2013, approximately 8.9 billion kilograms of carbon dioxide have been generated and subsequently released into the atmosphere.
#+end_quote
