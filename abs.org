:PROPERTIES:
:ID:       1dffdd10-2b12-4774-a719-e20aeb89974d
:mtime:    20230912192151
:ctime:    20230912192151
:END:
#+TITLE: ABS
#+CREATED: [2023-09-12 Tue]
#+LAST_MODIFIED: [2023-09-12 Tue 19:23]

acrylonitrile butadiene styrene

#+begin_quote
Like almost every form of plastic on the planet, ABS is made from petrochemicals that are derived from petroleum, the fossil fuel we commonly refer to as crude oil. The substance materialized over millions of years as fossilized organisms like zooplankton and algae were covered by stagnant water and further layers of these dead animals and plants. Try to imagine not only how slow that process is (geologists call this “deep time”) but also the near-instantaneous speed at which the oil was extracted from the earth. Now consider its carbon residue just sitting in the atmosphere, slowly helping make the planet hotter. As I stared at the plastic, these head-spinning thoughts flashed through my mind.

-- [[id:9a48db2a-c31f-4a83-b931-e7c1c9d69a84][The environmental impact of a PlayStation 4]]
#+end_quote
