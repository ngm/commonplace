:PROPERTIES:
:ID:       f6e99c9b-6bcf-406e-a7de-bcc9a8f517a4
:mtime:    20211127121013 20210724222235
:ctime:    20210724222235
:END:
#+title: The virtual class
#+CREATED: [2020-12-20 Sun 10:37]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

#+begin_quote
Although companies in these sectors can mechanise and sub-contract much of their labour needs, they remain dependent on key people who can research and create original products, from software programs and computer chips to books and tv programmes. These skilled workers and entrepreneurs form the so-called 'virtual class': '...the techno-intelligentsia of cognitive scientists, engineers, computer scientists, video-game developers, and all the other communications specialists...'

-- [[https://www.metamute.org/editorial/articles/californian-ideology][The Californian Ideology]] 
#+end_quote
