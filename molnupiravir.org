:PROPERTIES:
:ID:       322c5c3a-a6e2-46bc-9e22-0c23ba0597e3
:mtime:    20211127120821 20211122185834
:ctime:    20211122185834
:END:
#+TITLE: molnupiravir 
#+CREATED: [2021-11-22 Mon]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ oral antiviral pill for Covid
+ a pill that can be taken twice daily at home
+ due for delivery from mid-November
+ priority to elderly Covid patients and those with e.g. weakened immune systems
+ a clinical trial in the US showed a five-day course of the pills halved the risk of hospitalisation or death for at-risk patients

[[id:15ecf8a8-1975-4096-8eb5-b6550708bac1][Coronavirus]]

- [[https://www.theguardian.com/world/2021/nov/04/uk-is-first-to-approve-oral-antiviral-pill-molnupiravir-to-treat-covid][UK first to approve oral antiviral molnupiravir to treat Covid | Coronavirus ...]]

