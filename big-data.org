:PROPERTIES:
:ID:       cdb654c4-3354-472c-bac0-e96a53668534
:mtime:    20221016163807 20220616104037 20211127120949 20210724222235
:ctime:    20210724222235
:END:
#+title: Big data
#+CREATED: [2021-07-07 Wed 17:32]
#+LAST_MODIFIED: [2022-10-16 Sun 16:39]

#+begin_quote
Understood as the basic resource of communicative capitalism, big data has the characteristic of being self-renewing. It is inexhaustible and co-extensive with the reproduction of social life. It reaches through and beyond work, even beyond the reproduction of workers, into the social substance itself.

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote

#+begin_quote
UPS installed sensors as well as GPS in its trucks in an effort to increase efficiency and control costs. Data on more than 200 elements is collected, including truck speed, number of times the truck is put in reverse, driver seat belt use, the length of time a truck is idling.

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote

#+begin_quote
Many such enterprises “track the time employees arrive, what they do at work, when they leave for breaks, the times they call in sick, schedule details, personal information and much more”, writes the author, Bill Barlow. Workforce analytics lets a company use this information “to optimize its labor force by scheduling the right mix of full-time, part-time and temporary labor on a variety of schedules”.

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote

#+begin_quote
Approached in terms of [[id:5c06724b-1bbe-42f7-b611-896c83222936][class struggle]], big data looks like further escalation of capital’s war against labor.

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote

#+begin_quote
If earlier waves of automation displaced industrial workers, big data portends the displacement of post-industrial or knowledge workers

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote

#+begin_quote
The value in and of big data is for capital, not for the people from whom it is expropriated.

-- [[id:bf1c3d4e-1e85-419e-bfa8-748787190927][Communicative Capitalism and Class Struggle]]
#+end_quote
