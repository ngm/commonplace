:PROPERTIES:
:ID:       cb5f0f63-1baa-4152-9e47-b431e79d2fea
:mtime:    20220318120341
:ctime:    20220318120341
:END:
#+TITLE: Markets before capitalism
#+CREATED: [2022-03-18 Fri]
#+LAST_MODIFIED: [2022-03-18 Fri 12:13]

[[id:8f4fed05-9449-4099-86ac-8cbe4e2585d3][Markets]] before [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].

#+begin_quote
Almost from the very beginning of human history, there were markets. As early as the Ice Age, long before the rise of cities with permanently settled populations, there were specialized meeting areas for ritual and trade between groups. When hunting and gathering bands began to settle on land to cultivate crops and domesticate animals, they created the conditions to produce something unprecedented: an economic surplus. By the Bronze Age, people had amassed sufficient “surplus food, oil, and wool,” as economic historian Michael Hudson writes, “to support a permanent superstructure of handicraft, mercantile and administrative occupations.” Temples became the first public institutions, functioning variously as storage facilities for the surplus resources of their communities, gathering places, trading depots, refuges from local feud justice, establishers of contract law, enforcers of trade obligations, and sponsors of standardized weights and measures.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
Temples also employed the labor of dependents: war widows, orphans, the blind and infirm, and others who could not function in normal family contexts. Housing the workshops where these dependents wove, Mesopotamian temples consigned textiles to merchants under instructions to trade them abroad for raw materials not found between the Tigris and Euphrates: metal, stone, and hardwood. To facilitate this export enterprise and regulate the markets it sponsored, Sumer developed most of the major instruments of modern commerce during roughly the third millennium BCE — money, credit, interest, contracts, and legacies — and established profit-seeking mercantile operations as far away as Anatolia and the Indus Valley. Thus, roughly 4,000 years before the emergence of capitalism, we were economizing our resources by haggling in markets which connected people thousands of miles apart.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
Deposit banking, insurance policies, corporations, municipal bond markets, and, what was crucial for all of these, double entry bookkeeping, were developed later on, but still well before capitalism, in medieval Venice between about the eleventh and fifteenth centuries CE. From there, merchants would acquire and peddle wares on an intercontinental scale: after Marco Polo extended the silk route to Venice, the lagoon nestled in the pit of the Adriatic Sea became the world’s chief hub of trade in commodities, bullion, and various financial instruments, with a market network connecting West Africa to Siberia, the South Pacific to England. As Wood points out, the feudal system was compatible not just with “advanced urban cultures, highly developed trading systems, and far-flung commercial networks” but indeed “profit-seeking middlemen, even highly developed merchant classes.” None of these, it follows, should be confused for, nor even considered evidence of, capitalism. Capitalism isn’t distinguished by its capacity to provide market opportunities, but by the imperatives the market places on its unique system of production.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote
