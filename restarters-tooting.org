:PROPERTIES:
:ID:       0e66332b-1d3c-442c-b43e-6add4c9f4704
:mtime:    20211127120801 20210921102740
:ctime:    20210921102740
:END:
#+TITLE: Restarters Tooting 
#+CREATED: [2021-09-17 Fri]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://therestartproject.org/groups/restarters-tooting/

Run Restart Parties in Tooting, London.  I used to volunteer sometimes when I lived in London.
