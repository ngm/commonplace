:PROPERTIES:
:ID:       abdb6222-bf1d-49ce-b472-fc33ae57b21a
:mtime:    20230719181214 20220703175325 20220702140306
:ctime:    20220702140306
:END:
#+TITLE: carbon footprint
#+CREATED: [2022-07-02 Sat]
#+LAST_MODIFIED: [2023-07-19 Wed 18:12]

#+begin_quote
They control us, they exclude us, and then – as in the ingenious concept of the ‘carbon footprint’, which is the equivalent of those “drink responsibly” warnings at the end of ads for beer – they scold and blame us.

-- [[id:eda04511-6a17-4173-98b1-b94b97cb259b][For a Red Zoopolis]]
#+end_quote

#+begin_quote
British Petroleum, the second largest non-state owned oil company in the world, with 18,700 gas and service stations worldwide, hired the public relations professionals Ogilvy & Mather to promote the slant that climate change is not the fault of an oil giant, but that of individuals.

It’s here that British Petroleum, or BP, first promoted and soon successfully popularized the term “carbon footprint" in the early aughts.

-- [[https://mashable.com/feature/carbon-footprint-pr-campaign-sham][The devious fossil fuel propaganda we all use | Mashable]]
#+end_quote


[[https://www.theguardian.com/commentisfree/2021/aug/23/big-oil-coined-carbon-footprints-to-blame-us-for-their-greed-keep-them-on-the-hook][Big oil coined ‘carbon footprints’ to blame us for their greed. Keep them on ...]]
