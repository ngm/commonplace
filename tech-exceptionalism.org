:PROPERTIES:
:ID:       cfc88600-65fa-434b-90fd-d962e3596d9e
:mtime:    20231022122407
:ctime:    20231022122407
:END:
#+TITLE: tech exceptionalism
#+CREATED: [2023-10-22 Sun]
#+LAST_MODIFIED: [2023-10-22 Sun 12:24]

#+begin_quote
“Tech exceptionalism” is the sin of thinking that the normal rules don’t apply to technology.

-- [[id:f613abf1-a680-410a-9be7-871f52a04c3e][The Internet Con]]
#+end_quote
