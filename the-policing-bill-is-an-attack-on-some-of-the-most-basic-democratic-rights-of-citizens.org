:PROPERTIES:
:ID:       5bffd88c-027d-4a45-874e-41bb1c5a5d72
:mtime:    20220113150826 20220113094224
:ctime:    20220113094224
:END:
#+TITLE: The policing bill is an attack on some of the most basic democratic rights of citizens
#+CREATED: [2022-01-13 Thu]
#+LAST_MODIFIED: [2022-01-13 Thu 15:08]

The [[id:6c4bdf5e-8cbd-45bb-bc2a-dec1ba0d428d][Policing bill]] is an attack on some of the most basic [[id:f99b4109-b369-4e97-a1f5-7e3d1cc7f3e5][democratic rights of citizens]].

For example: the right to peacefully assemble.  The right to speak freely.

#+begin_quote
More than 350 organisations, including human rights groups, charities and faith bodies, have written to Patel and justice secretary Robert Buckland this weekend complaining that the measures would have a “profound impact” on freedom of expression, and represent “an attack on some of the most basic democratic rights of citizens”.

-- [[https://www.theguardian.com/uk-news/2021/sep/12/patel-faces-widening-revolt-over-policing-bills-restrictions-on-protest][Patel faces widening revolt over policing bill’s restrictions on protest | Po...]] 
#+end_quote

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement vector :: [[id:353d8e39-495c-4be4-a1ba-38419377555f][Signs point to yes]]
