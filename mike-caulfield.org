:PROPERTIES:
:ID:       e566624e-5d3b-4b96-95df-5da966b24593
:mtime:    20211127121003 20210724222235
:ctime:    20210724222235
:END:
#+title: Mike Caulfield
#+CREATED: [2021-02-14 Sun 15:29]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

Works on [[id:72bae185-89a2-496d-a046-c90f18af5fb3][web literacy]].

Also is/was a proponent of a slower web than the social media streams.  See [[id:81340413-1481-4376-a906-b65e32f10fef][The Garden and the Stream]].

Posts on [[id:563ec48b-525a-4fc8-b548-f8a721fdfa6b][Federated wikis]] too.

[[id:4294a644-7c22-4985-9485-6dd45cbe7bc7][Wikity]] was how I found my way in to the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]].
