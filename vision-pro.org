:PROPERTIES:
:ID:       ba0f8dc4-a990-4ab1-960a-5d752c42a74c
:mtime:    20240216182904
:ctime:    20240216182904
:END:
#+TITLE: Vision Pro
#+CREATED: [2024-02-16 Fri]
#+LAST_MODIFIED: [2024-02-16 Fri 18:30]

"…you’ve more chance of walking to Venus than getting the Apple Vision Pro repaired if you damage it." -- [[https://www.techfinitive.com/why-vision-pro-is-a-repair-nightmare-and-apple-probably-doesnt-care/][Why Vision Pro is a repair nightmare | TechFinitive]]
