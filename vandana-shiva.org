:PROPERTIES:
:ID:       d3d0d697-882c-46e7-adb0-3c5248926e44
:mtime:    20220830222056
:ctime:    20220830222056
:END:
#+TITLE: Vandana Shiva
#+CREATED: [2022-08-30 Tue]
#+LAST_MODIFIED: [2022-08-30 Tue 22:20]

#+begin_quote
Vandana Shiva was an important leading public intellectual, combining defense of local land rights, indigenous knowledge, feminism, post-caste Hinduism, and other progressive programs characteristic of New India and the Renewal

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote
