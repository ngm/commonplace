:PROPERTIES:
:ID:       8e4a8afd-e205-40ff-9b44-a3a61d017552
:mtime:    20220119194658
:ctime:    20220119194658
:END:
#+TITLE: Households across the UK will see rising prices and stalling wages in 2022
#+CREATED: [2022-01-19 Wed]
#+LAST_MODIFIED: [2022-01-19 Wed 19:47]

#+begin_quote
It’s being called “[[id:7f291766-8cf9-4839-bc84-762942c5d90a][the year of the squeeze]]”: thanks to stagnating wages, rising costs of consumer goods, and increases in energy tariffs, plus changes in government measures, the cost of living is set to increase sharply in 2022. 
#+end_quote

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]
