:PROPERTIES:
:ID:       4b4ec932-eae5-4c19-af40-14c7d9f347bb
:mtime:    20211127120834 20210724222235
:ctime:    20200405012852
:END:
#+TITLE: Jurassic World

A capitalist film with an [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][anti-capitalist]] message and lots of product placement.
