:PROPERTIES:
:ID:       4ceb1daf-8e92-42d8-bacd-f9d7bacbdded
:mtime:    20220206211507 20211127120834 20210828192359
:ctime:    20210828192359
:END:

#+TITLE:All Watched Over by Machines of Loving Grace

+ [[id:ee51cdc7-aab2-4663-b3a1-12f48e7b9988][Poem]]
+ Author :: [[id:8c659965-8615-4c26-a145-a574c100d0de][Richard Brautigan]]
 
I originally came across the poem All Watched Over by Machines of Loving Grace by Richard Brautigan via the Adam Curtis documentary of the same name ([[id:ac7e3737-91f7-4e4e-b4fc-0ca0032392cc][All Watched Over by Machines of Loving Grace (Adam Curtis)]]).

It’s kind of fascinating. I like it.  I know it came from a period whose [[id:9934c2a1-d69a-4eee-828a-c1ce08d386bb][Technological utopianism]] certainly didn’t come to pass, and might have been a bit off-key in the first place, but its sweetly optimistic (…or bitingly critical, depending on what way you squint at it).

It was written in 1967.

#+begin_verse
*All Watched Over by Machines of Loving Grace*

I like to think (and
the sooner the better!)
of a cybernetic meadow
where mammals and computers
live together in mutually
programming harmony
like pure water
touching clear sky.

I like to think
(right now, please!)
of a cybernetic forest
filled with pines and electronics
where deer stroll peacefully
past computers
as if they were flowers
with spinning blossoms.

I like to think
(it has to be!)
of a cybernetic ecology
where we are free of our labors
and joined back to nature,
returned to our mammal
brothers and sisters,
and all watched over
by machines of loving grace.
#+end_verse

If it were written today it must surely be ironic. But I wonder if it was heartfelt back in the 60s?

I find what it paints, this harmony of nature and technology, to be kind of a mixture of pleasantly bucolic and desireable, and weird and creepy all at once. Not sure if I want it or not. I like the idea of a cybernetic ecology, where we are free of our labours, and joined back to nature. Not entirely so keen on being watched over by machines of loving grace. (Though the benevolent AIs in [[id:ad985a12-92cd-477e-81c8-5dda1ca11a93][Iain M. Banks]]’ Culture novels could be good role models if we did want machines of loving grace…)

The idea of technology being more in balance with nature is good.  Though the poem kind of has it backwards - a kind of [[id:d724ef4e-0aa6-4407-adc8-6c7ef3dd9048][accelerationism]] of the technology, rather than maybe a [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][degrowth]] to natural boundaries.

It’s interesting that the poem doesn’t really make a case for technology, other than the nod towards a kind of [[id:1d0fa725-6b14-4ecb-9705-283dd56b782e][fully automated luxury communism]] at the end. It just sort of assumes that tech is the route to liberation – I guess that’s the flavour of the time. I’m not a primitivist, but I’m not sure that an IoT meadow will have all that much better benefit than the analogue equivalent.

Obviously [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]] was hot stuff back then.  I would say that the poem misuses the term.  I don't think that cybernetics is necessarily about a conjoining of technology and nature.  It's the study of how organisations function and maintain themselves, and it just spans organisms/structures in technology and nature.

Notice that the poem increases in scale over verses, from meadow to forest to ecology:

- cybernetic meadow
- cybernetic forest
- cybernetic ecology

Curtis explores a lot of these ideas in [[id:ecbb88e5-9843-4ff7-8a97-33d92de1c9db][The Use and Abuse of Vegetational Concepts]] part of his documentary.
