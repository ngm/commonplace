:PROPERTIES:
:ID:       83e8775d-65b7-49bf-975d-e4473ea2d6a8
:mtime:    20220616102920
:ctime:    20220616102920
:END:
#+TITLE: Wide data
#+CREATED: [2022-06-16 Thu]
#+LAST_MODIFIED: [2022-06-16 Thu 10:42]

#+begin_quote
Wide Data sets are much smaller than Big Data sets, making them easier to manage, but the key defining characteristic is that wide data is made up of data that comes from many disparate and unconnected data sources.

The move to Wide Data is only just beginning, but technology research firm Gartner predicts that 70% of organisations will shift their focus from Big to Small and Wide Data by 2025.

https://charitydigital.org.uk/topics/topics/what-is-wide-data-10127
#+end_quote

I mean taking data from a wide variety of sources makes sense, but labelling it as 'Wide Data' (capitalised...) sounds like a bit of a marketing thing.
I don't think many charities (especially not small charities) ever worked with [[id:cdb654c4-3354-472c-bac0-e96a53668534][Big Data]] either, not the scale that 'Big Data' really is.
