:PROPERTIES:
:ID:       20a2a55f-842b-4103-8ac7-9eb1bd9813ac
:mtime:    20220122155613
:ctime:    20220122155613
:END:
#+TITLE: The government should invest in the NHS, not privatise it
#+CREATED: [2022-01-22 Sat]
#+LAST_MODIFIED: [2022-01-22 Sat 16:00]

[[id:1ac681b1-22c3-4e25-9731-df84b3bd7d14][NHS]].  [[id:2a4b9236-04dc-4361-b4f9-2d331cb60adf][Privatisation]].

[[https://www.theguardian.com/commentisfree/2022/jan/21/the-guardian-view-on-nhs-privatisation-the-wrong-treatment][The Guardian view on NHS privatisation: the wrong treatment | Editorial | The...]]

* Because

+ [[id:5e6a9696-c04d-4fdd-b2f9-0675908b5b2e][For-profit healthcare systems are a piece-of-shit]]

* But

+ I am still interested in commons-based / municipal forms of healthcare, as opposed to state health provision.

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:9a13c2e4-f670-4d0d-9f15-62cfa405f37d][Yes definitely]]

