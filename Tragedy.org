:PROPERTIES:
:ID:       9a612490-d3dd-4bf0-a431-3188dad01e1b
:mtime:    20211127120917 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Tragedy

#+begin_quote
Why does tragedy give pleasure? Why do people who are neither wicked nor depraved enjoy watching plays about suffering or death? Is it because we see horrific matter controlled by majestic art? Or because tragedy actually reaches out to the dark side of human nature?

--[[https://www.oxfordscholarship.com/view/10.1093/acprof:oso/9780198187660.001.0001/acprof-9780198187660][Why Does Tragedy Give Pleasure? - Oxford Scholarship]] 
#+end_quote
