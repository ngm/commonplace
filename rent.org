:PROPERTIES:
:ID:       dd0afcfc-e583-4aea-b03e-019b41d98808
:mtime:    20220610094615
:ctime:    20220610094615
:END:
#+TITLE: Rent
#+CREATED: [2022-06-10 Fri]
#+LAST_MODIFIED: [2022-06-10 Fri 10:12]

#+begin_quote
Rent, in other words, is not just an economic phenomenon: it’s a reflection of the prevailing political order, and the struggles that define it

-- [[id:627383d2-9115-4f09-9eac-23f7bd46e615][The Rent Is Too Damn High]]
#+end_quote

#+begin_quote
This ‘30% rule’ has its roots in American president Lyndon B. Johnson’s ‘Great Society’ programmes, when, following 1968’s Housing and Urban Development Act, legislation was passed capping rents in public housing at 25 per cent of residents’ income. This in turn was raised to 30 per cent in the 1980s

-- [[id:627383d2-9115-4f09-9eac-23f7bd46e615][The Rent Is Too Damn High]]
#+end_quote
