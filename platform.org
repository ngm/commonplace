:PROPERTIES:
:ID:       4957145c-c70f-4c02-8824-9c5e90b66371
:mtime:    20220206175611
:ctime:    20220206175611
:END:
#+TITLE: Platform
#+CREATED: [2022-02-06 Sun]
#+LAST_MODIFIED: [2022-02-06 Sun 17:56]

#+begin_quote
A platform is an online application or website used by individuals or groups to connect to one another or to organize services. 

-- [[https://platform.coop/][Platform Cooperativism Consortium]]
#+end_quote
