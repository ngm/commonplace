:PROPERTIES:
:ID:       c937a404-328a-4282-bc62-6a6f12838632
:mtime:    20221224213831 20221224195846
:ctime:    20221224195846
:END:
#+TITLE: 42. TECH FOR GOOD
#+CREATED: [2022-12-24 Sat]
#+LAST_MODIFIED: [2022-12-24 Sat 21:49]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://shows.acast.com/reasonstobecheerful/episodes/episode42.techforgood
+ Series :: [[id:bb2beb1f-e125-4fa6-97b5-0909a0effd46][Reasons to be Cheerful]]

+ ~00:09:43 Digital revolution has to be combined with democratic revolution
+ ~00:15:25  [[id:353ba7d9-04c3-4003-bcb1-b60eb5039684][Data commons]]
+ ~00:15:50  [[id:e447c8a9-81f1-4764-81fd-2e077d05b16c][Participatory democracy]] rather than just consultation
+ ~00:19:52  [[id:e76adc11-e466-434c-9aad-e3f84f0ba6da][Community mesh networks]] in [[id:477fe288-c36f-4b26-bf41-f1c3c541b15b][Red Hook, Brooklyn]] and in Detroit
+ ~00:22:26 Mesh was only network that worked in Red Hook during [[id:f3ce3d72-b8a1-4b48-a7f6-00f0de58e7ff][Hurricane Sandy]].
+ ~00:23:01  Red Hook mesh works in emergency and to help those who have no connection otherwise
+ ~00:36:30  [[id:5b21c57c-c8ba-4f47-8127-e20ac8c83551][Connectivity]] is fundamental to human life these days
+ ~00:37:22 The deep value of something like WiFi is in empowering people (same true of repair cafes?)
+ ~00:40:32  [[id:d6ddaef1-d7b5-416f-8ddf-7ae442d0779b][Bristol Approach]], damp sensing
