:PROPERTIES:
:ID:       8e8f5ac7-e338-43b5-9455-8764d5598713
:mtime:    20220204144903 20211127120910 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Arcology

* Idea

Ecological architecture.

* Software

[[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWebby]] blog generator built on [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]].

- [2021-06-26 Sat]: https://dev.arcology.garden/garden/arcology.html

- https://notes.whatthefuck.computer/1460753040.0-note.html
- https://code.rix.si/rrix/arcology 


