:PROPERTIES:
:ID:       4387c5c1-dbef-4d2f-b3b1-20e7eab31894
:mtime:    20250314095938 20250308153116
:ctime:    20250308153116
:END:
#+TITLE: Towards Socialism and the End of Capitalism: An Introduction
#+CREATED: [2025-03-08 Sat]
#+LAST_MODIFIED: [2025-03-08 Sat 15:32]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ Found at :: https://www.patreon.com/posts/towards-and-end-117054157
+ Part of :: [[id:b7b70123-178d-4155-8dab-b845c3a6867c][Upstream]]

[[id:d37c2ed3-4eb3-46c2-b528-79356cd3fa70][Towards socialism]] and [[id:ebbdfd84-67e0-4c40-9f65-099af8a01cd7][the end of capitalism]].

A reading of the introduction of the book.

Enjoyed this a lot. Good thoughts and the oral annotations by Robbie add a ton of understanding and value.
