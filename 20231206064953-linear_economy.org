:PROPERTIES:
:ID:       41b6fe96-d336-4ada-ab93-2bbaa6c55820
:END:
#+title: linear economy

#+begin_quote
A circular economy contrasts with a one-way, linear economy that depletes natural resources, transforms them into waste, and pollutes residual natural capital through resource extraction, production, and waste disposal practices

-- [[id:34e1d939-c7fc-4194-bd51-8fe3ed0b19b1][Degrowth, green energy, social equity, and circular economy]]
#+end_quote
