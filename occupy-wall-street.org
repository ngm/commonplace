:PROPERTIES:
:ID:       c7aee726-2e3a-4b2b-985f-77f5d525ec7d
:mtime:    20220212120151 20211127120945 20210724222235
:ctime:    20210724222235
:END:
#+title: Occupy Wall Street
#+CREATED: [2021-03-12 Fri 12:09]
#+LAST_MODIFIED: [2022-02-12 Sat 12:02]

[[id:2df7e386-5ea3-4c83-9f06-c89980b17753][Occupy]]

#+begin_quote
At the Occupy Wall Street encampment in 2011, reporters would arrive and be transfixed by the media centre – the nerve centre, the centre of power because it was media (Schneider 2013, 36). And media was powerful indeed, as it drew thousands upon thousands of people into what began as a small, precarious protest. Videos of police attacking activists, in particular, bred sympathy and participants, and a feeling that the movement might be on the brink of sparking some kind of revolution. At least at first. By the following year, the videos didn’t work the same way. As an activist mon- itoring the analytics data noticed at the time, “Riot porn is losing its luster for mass online consumption”

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote
