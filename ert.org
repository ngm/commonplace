:PROPERTIES:
:ID:       c94b1c37-7771-4bf7-8ce4-dd1500e813a4
:mtime:    20211127120946 20210724222235
:ctime:    20210724222235
:END:
#+title: ERT
#+CREATED: [2021-03-31 Wed 19:57]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Library for running tests against [[id:bdeca72d-e54b-4f87-8c69-f1cdf136763a][elisp]] code.

* Bookmarks
  - [[https://www.gnu.org/software/emacs/manual/html_node/ert/index.html]] 
  
