:PROPERTIES:
:ID:       09d0aee7-d205-45b2-a5c7-c08a28474491
:mtime:    20220918142229 20220911103653 20220812163823
:ctime:    20220812163823
:END:
#+TITLE: UK energy bill crisis
#+CREATED: [2022-08-12 Fri]
#+LAST_MODIFIED: [2022-09-18 Sun 14:22]

Part of the [[id:c499a402-92d0-48d3-ab33-f3b40dda401e][Cost of living crisis]].

* Timeline

** [2022-09-11 Sun]

[[id:69fe6b68-f6a1-4179-a819-b29d8b9bc47c][Energy price freeze]].

#+begin_quote
Britain will be plunged into an even worse energy crisis in a year’s time without an immediate plan to improve leaky homes and dramatically reduce demand for gas, ministers have been warned.

-- [[https://www.theguardian.com/money/2022/sep/11/britain-insulate-homes-energy-crisis-2023-heat-loss-houses-subsidising-bills][UK must insulate homes or face a worse energy crisis in 2023, say experts | E...]]
#+end_quote
