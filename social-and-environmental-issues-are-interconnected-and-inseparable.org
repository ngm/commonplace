:PROPERTIES:
:ID:       1d843238-d48a-4095-9324-d22d4e140063
:mtime:    20231025213049
:ctime:    20231025213049
:END:
#+TITLE: Social and environmental issues are interconnected and inseparable
#+CREATED: [2023-10-25 Wed]
#+LAST_MODIFIED: [2023-10-25 Wed 21:31]

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

[[id:e56699aa-a087-4e17-92ec-594d606eab10][Metabolic rift]] probably relates to this.

#+begin_quote
This ecological rift is the result of a social rift: the domination of humans over humans.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

Same as what [[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Bookchin]] says. Domination of humans over humans leads to domination of humans over nature. Interesting to hear that Marx said this also.  But yes capitalism could be described as the domination of humans over humans.

- [[https://globalizationandhealth.biomedcentral.com/articles/10.1186/s12992-021-00686-4][The critical intersection of environmental and social justice: a commentary |...]]
- [[https://globaldialogue.isa-sociology.org/articles/interconnected-challenges-of-the-21st-century][Interconnected Challenges of the 21st Century]]
- [[https://blog.pachamama.org/how-social-justice-and-environmental-justice-are-intrinsically-interconnected][How Social Justice and Environmental Justice Are Intrinsically Interconnected]]
