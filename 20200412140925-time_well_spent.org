:PROPERTIES:
:ID:       1ad18a6b-ec24-472c-a149-47b29a26b0a8
:mtime:    20211127120808 20210724222235
:ctime:    20200412140925
:END:
#+TITLE: Time well spent

Can tech help focus our intention rather than steal our [[id:15aa5a06-b7f5-4410-9432-9dd0b87f05fd][attention]].

