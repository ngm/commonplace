:PROPERTIES:
:ID:       fe739f4b-feff-466f-ac59-f7921c32d814
:mtime:    20220802231316
:ctime:    20220802231316
:END:
#+TITLE: Monopoly
#+CREATED: [2022-08-02 Tue]
#+LAST_MODIFIED: [2022-08-02 Tue 23:13]

#+begin_quote
How big tech uses monopoly power against long-lasting products 
• Apple trying for a monopoly on repairs 
• Amazon sells disposable tablets with no repair possible 
• Amazon removed small refurbishers of Apple products 
• Google blocks indie repair business from advertising

https://twitter.com/RestartProject/status/1288371183654707201
#+end_quote
