:PROPERTIES:
:ID:       c5601b9e-a3dd-4f91-8596-5c042af8de8a
:mtime:    20221008114535 20221007120911 20221007110855
:ctime:    20221007110855
:END:
#+TITLE: Updating to YunoHost 11
#+CREATED: [2022-10-07 Fri]
#+LAST_MODIFIED: [2022-10-08 Sat 11:45]

Updating to [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]] 11.

Going from Debian Buster to Debian Bullseye behind the scenes.

- https://yunohost.org/en/buster_bullseye_migration

- I ran the first migration from ~Tools > Migrations~.
  - It worked fine.
- However afterwards, I was getting the message "YunoHost API not responding.
  - I ssh'ed in to the server and ran ~sudo systemctl restart yunohost-api~.
  - This made that message go away.
- Next: another migration to fix python. "Repair Python app after bullseye migration"
  - apparently ~searx~ and ~calibreweb~ affected for me.
  - searx should be automatically resolved.
  - calibreweb I'll need to run ~sudo yunohost app upgrade -F calibreweb~
  - both appear to be fine after those actions.
- No major issues in the 'Diagnostics' section.
- Updating all the apps now then.
  - collabora ok
  - invidious failed (that's common though...)
  - searx failed (that's new)
    - seems to have been removed entirely for some reason.
    - installing it again has worked fine.
  - syncthing ok
  - nitter ok
  - nextcloud ok
  - rainloop ok
  - my_webapp ok
