:PROPERTIES:
:ID:       81e07c43-1e32-440f-817b-2c7edddfcc46
:mtime:    20220607210219
:ctime:    20220607210219
:END:
#+TITLE: Food strategy for England likely to be watered down
#+CREATED: [2022-06-07 Tue]
#+LAST_MODIFIED: [2022-06-07 Tue 21:12]

+ URL :: https://www.theguardian.com/environment/2022/jun/07/food-strategy-for-england-likely-to-be-watered-down

UK [[id:4b76b6f5-38fb-41df-8d72-943e8b3f61d6][National Food Strategy]] is going to be a bit rubbish.

#+begin_quote
Experts consulted on the strategy pushed for a reduction in intensive animal agriculture and mandatory reporting for retailers on how much [[id:94c6656b-ad96-49ad-9733-bc78aad31919][animal protein]], compared with [[id:5b93d72a-aa2e-4921-88a7-48cae6edf8c9][plant protein]], they sell.
#+end_quote

#+begin_quote
Greenpeace has called for a shift towards plant-based protein, and the Soil Association agrees, arguing that any meat should be produced in a regenerative system, with more land being used to grow crops for human consumption rather than to be fed to animals or used for intensive animal agriculture
#+end_quote

