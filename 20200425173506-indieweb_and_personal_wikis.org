:PROPERTIES:
:ID:       d5a8d1f7-57cb-426e-825c-5ea88414d6e1
:mtime:    20211127120954 20210724222235
:ctime:    20200425173506
:END:
#+TITLE: IndieWeb and personal wikis

IndieWeb sometimes feels pretty [[id:22aec571-2dcd-4fc8-abe5-6f7ab4083fc5][stream]] focused. 

However, [[id:363b6ef6-b565-450e-87f6-820daeca653e][FedWiki]] came out of one of the early IWCs, so I guess it's always been a thread! 

Plus I'm pretty sure it was through [[id:4294a644-7c22-4985-9485-6dd45cbe7bc7][Wikity]] somehow that I first heard about IndieWeb. 

* Whys
  
Lots of the IndieWeb [[https://indieweb.org/why][whys]] apply.

* Building blocks
  
Which of the [[https://indieweb.org/Category:building-blocks][building blocks]] apply to a personal wiki?  i.e. which apply to [[id:1316026d-4c00-413e-a825-e983ab9ddbbe][the garden]]?

They definitely apply to the stream for a lot of social stream stuff.  Microformats, webmentions.

I'm not convinced about the need for web actions to copy material from wiki to wiki. I'm doing manual til it hurts here, and it's pretty painless. Though doing a webmention for letting people do back links if they wanted could be interesting.

