:PROPERTIES:
:ID:       7bc51939-5c62-46ab-8a3e-c6533472f2da
:mtime:    20211127120859 20210724222235
:ctime:    20200615201521
:END:
#+TITLE: The End of Policing

#+ATTR_HTML: :width 100% :alt Front cover of the book.
[[file:2020-06-15_20-26-25_9781784782924-eee743ba813f726f0ef78c27fdea782c.jpg]]


There’s a pretty dark history to the origins of policing – intertwined with [[id:f1669171-61c2-4069-9d38-c6047534a247][colonialism]], [[id:d7b92b91-53f6-474c-8d15-0a462a2b8713][slavery]], and industrial [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].  That legacy still exists in the police today, as well as new issues , like the militarisation of the police force.  (Does the local police really need grenade launchers?)

#+begin_quote
The origins and functions of the police are intimately tied to the management of inequalities of race and class. 

— The End of Policing, Alex Vitale
#+end_quote

- [[https://soundcloud.com/upstreampodcast/alex-vitale][Alex Vitale - The End of Policing (In Conversation)]]
