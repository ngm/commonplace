:PROPERTIES:
:ID:       da869cda-8826-4b14-a7d5-aa9f981a95bc
:mtime:    20211127120957 20210724222235
:ctime:    20210724222235
:END:
#+title: 5tracks - week 46

[[id:7ad30c14-e6de-43bc-ba13-b542b8547d89][5tracks]] from Bandcamp to and enjoy and support the artistes.  Week 46: some kind of noise.

- [[https://cometsonfire.bandcamp.com/track/the-antlers-of-the-midnight-sun][Comets on Fire - The Antlers of the Midnight Sun]]
- [[https://mindflayer1.bandcamp.com/track/1999-animals-revenge][MINDFLAYER - 1999 ANIMALS REVENGE]]
- [[https://blackpus.bandcamp.com/track/dying-on-a-gorgeous-rug][Black Pus - DYING ON A GORGEOUS RUG]]
- [[https://thestooges.bandcamp.com/track/fun-house][The Stooges - Fun House]] 
- [[https://hella.bandcamp.com/track/you-dj-parents][Hella - You DJ Parents]] 
