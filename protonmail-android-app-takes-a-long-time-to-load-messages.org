:PROPERTIES:
:ID:       82f9940d-1558-413b-811d-41667373f80e
:mtime:    20220724100635 20220722215925 20220718091916
:ctime:    20220718091916
:END:
#+TITLE: ProtonMail Android app takes a long time to load messages
#+CREATED: [2022-07-18 Mon]
#+LAST_MODIFIED: [2022-07-24 Sun 10:07]

[[id:a4063baf-1950-4a48-bb43-23e1cb83f809][ProtonMail]] Android app takes ages to load up new emails.  Makes it kind of unusable. Frustrating, as this is an important service for me and one that I pay for.

Others seem to be having the same problem - https://github.com/ProtonMail/proton-mail-android/issues/165 - with no response from ProtonMail in over a month.

In the comments, someone suggests downgrading to an earlier version of the app.  https://github.com/ProtonMail/proton-mail-android/issues/165#issuecomment-1179731854

I'll try that when I get a minute.

OK, a few days later someone posted a fix: 
https://github.com/ProtonMail/proton-mail-android/issues/165#issuecomment-1186924180

#+begin_quote
proton team got back to me suggesting to turn off alternative routing, which resolved the issue for me.
#+end_quote

Works for me!
