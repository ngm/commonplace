:PROPERTIES:
:ID:       9da88741-d39d-49da-9f25-11c657fe2109
:mtime:    20211127120919 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-04-01

* [[id:9cc21418-ef8c-4e98-83cc-a4115fb4eec1][Public drafts]]

I posted that previous note with a link to a draft article in it mainly so it could be shown at the online HWC - but also it marks the point that I'm starting trying out working on draft articles 'in public'.  They are still tucked away in my wiki, so not really that public, but at least online somewhere in case (as sometimes happens) I never finish them - some of the nascent bits will at least still be there available to the world and possibly useful.

* 19:01

Working on a draft post about owning my event discovery and RSVPs:

[[id:243dc6e6-a3af-4ddb-8eba-be321da8e4a5][DRAFT: Indie events and RSVPs]]
