:PROPERTIES:
:ID:       1c9627fc-b18e-4016-b5d9-dc3f43a3225b
:mtime:    20211127120809 20210724222235
:ctime:    20210724222235
:END:
#+title: Your Priorities
#+CREATED: [2021-04-24 Sat 16:19]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
Your Priorities is a [[id:3dfe6feb-d9ae-447a-ab62-2f568ca19f21][citizen engagement]] platform, a progressive web app, and a [[id:ae013554-20ca-462b-8d1a-ce11d8d0cb4d][participatory social network]] that empowers groups of any size to speak with one voice and organize around ideas. 
#+end_quote
