:PROPERTIES:
:ID:       263ab5f7-3a67-4fef-bbc4-1438fe8a8591
:mtime:    20220421181321
:ctime:    20220421181321
:END:
#+TITLE: Fixing Factories: And we're off
#+CREATED: [2022-04-21 Thu]
#+LAST_MODIFIED: [2022-04-21 Thu 18:21]

+ URL :: https://therestartproject.org/news/brent-fixing-factory/ 

Launch of first [[id:6e5895f7-0c3d-4165-b3f1-f48461b8b848][Fixing Factory]] in Brent.   Saturday 23rd April 2022.

#+begin_quote
Visitors to the site will be able to donate devices, see how they’re repaired and find out about the project. Those keen to learn about [[id:36fdfa5d-bb7e-41c1-8442-054bea1915ec][repair]] will have a chance to volunteer on the site and get work experience. 
#+end_quote

#+begin_quote
We’ll also host regular weekend events with local partners, where visitors will be able to get involved in a more hands-on way. Ideas so far include workshops on [[id:87499474-f26a-4a34-b216-58d7aaeb48fe][laptop repair]], and an opportunity to bring your own laptop for fixing. 
#+end_quote
