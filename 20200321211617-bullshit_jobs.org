:PROPERTIES:
:ID:       3dd3f1c3-43d9-409b-b4c2-ab898788bc4a
:mtime:    20211127120827 20210724222235
:ctime:    20200321211617
:END:
#+TITLE: Bullshit jobs

Bullshit jobs... creation of meaningless bureaucratic jobs to fulfil capitalism.  

c.f. strikes of garbage workers vs strikes of bankers and which one causes the most disruption.

#+begin_quote
Reading about who still needs to show up for work in times of pandemic, reminds of @davidgraeber & Bullshit Jobs. We are experiencing how our societies would collapse if the nurses, waste collectors & teachers stopped showing up for work

-- https://twitter.com/LejlaSadiku/status/1241030820955148290
#+end_quote

#+begin_quote
We place [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]] in quarantine only to discover: 

- The paramount importance of all [[id:36fdfa5d-bb7e-41c1-8442-054bea1915ec][maintenance]] work: transport, logistics, cleaning, health work, etc. 
- The radical uselessness of financiers, consultants & vultures 
- The absolute centrality of informal/family/support networks

-- https://twitter.com/acorsin/status/1237821553703882752
#+end_quote

#+begin_quote
We no longer live. We aspire. We work to get richer. Paradoxically, we find ourselves working in order to have a “vacation.” We can’t seem to actually live without working. Capitalism has thus completely occupied social life. 

-- [[https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/][An Illustrated Guide to Guy Debord's 'The Society of the Spectacle']] 
#+end_quote
