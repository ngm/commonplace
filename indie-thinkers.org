:PROPERTIES:
:ID:       646d5371-69ca-4f8c-8afe-ebcb584ab17d
:ROAM_ALIASES: "Independent researcher"
:mtime:    20211127120847 20210724222235
:ctime:    20210724222235
:END:
#+title: Indie thinker

#+begin_quote
A new breed of influencers has emerged from the PKM market: indie thinkers, which some call thinkfluencers. From researchers to entrepreneurs and writers, these influencers rely on new dissemination tools to monetize their unique knowledge.

-- [[https://nesslabs.com/the-state-of-personal-knowledge-management][The state of personal knowledge management - Ness Labs]] 
#+end_quote

^ I think the term thinkfluencer is pretty awful - hope that doesn't take off.  I guess I just have a negative reaction to the modern notion of "influencing" others' behaviour.  But I suppose in some sense also I do wish to influence others' behaviour through what I share.

#+begin_quote
Producing work that makes other people think, and perhaps change their behavior, is the validation, and it’s enormously satisfying.

-- [[https://nadiaeghbal.com/independent-research][Nadia Eghbal | The independent researcher]] 
#+end_quote

'Monetizing' I also don't like, but on the flipside in the current absence of universal security, I do think it's fair enough for people to be recompensed for what they've worked on.  Certainly don't want it to end up as some winner-takes-all profit-driven competition of /thinkfluencing/ bullshit though.  

I guess I just hope the idea is that the indie stands more for 'independent', less for 'individual'.   I like the idea that the institution of the university need not be the only route for research.

#+begin_quote
There’s no reason that universities need to be the gatekeepers of exploring and developing new ideas.

  - [[https://nadiaeghbal.com/independent-research][Nadia Eghbal | The independent researcher]] 
#+end_quote

Perhaps even better would be if the indie stood for something like [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]]'s /inter/dependent thoughts.

* Indie Research webinar

I watched the webinar [[https://www.stream.club/e/indie][The Indie Researcher: Tools for thought and internet academia]].  It was more discursive than practical to be honest, but a few takeaways that I like:

- being an indie researcher is about being curious in public - not necessarily an expert in some domain with a position to defend (I think [[id:b37459d9-806e-431c-b595-01f4e65e5509][Anne-Laure]] said this)
- being 'indie' doesn't mean you just do it all yourself - it's very much about discussion and community, too
- it might free you to do some things that might not happen in an academic environment

* Some indie researchers

  Just based on my opinion of what it means. Being curious in public, possibly financially supported by the crowd, regularly producing 'research' outputs. 

  - [[id:b37459d9-806e-431c-b595-01f4e65e5509][Anne-Laure]]
  - [[id:e558d566-d664-4566-8f6a-eef13353ba85][Nadia Eghbal]]
  - [[id:75be7570-85d0-496d-9402-e1933e45171a][Chris Webber]]
  - [[id:d4c392f4-7115-4bb5-b5ee-708174b8debf][Andy Matuschak]]

* Links

- [[id:e558d566-d664-4566-8f6a-eef13353ba85][Nadia Eghbal]] has written about this:
  - [[https://nadiaeghbal.com/independent-research][Nadia Eghbal | The independent researcher]] 
  - [[https://nadiaeghbal.com/phd][Nadia Eghbal | Reimagining the PhD]]
- [[https://otherlife.co/calling-all-indie-thinkers-literally/][Calling all indie thinkers (literally) - Other Life]]
  - note: after subscribing to his newsletter, I suspect (perhaps unfairly, TBD) that this dude is a bit alt-right.
- [[https://davidklaing.com/personal-phd/][The personal PhD]] 
