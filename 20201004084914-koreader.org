:PROPERTIES:
:ID:       41b338c9-5783-4da9-aba8-ab4739ea3f02
:mtime:    20220610101752 20211127120829 20210724222235
:ctime:    20201004084914
:END:
#+title: koreader

+ URL :: https://github.com/koreader/koreader

An absolutely fantastic piece of [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][libre software]] for reading ebooks.

I have it installed on my [[id:8860073c-5152-4350-bdd7-cb90ba8c67cd][Kobo]].

The top features for me:

- Night mode.  White text on a black background for reading at night.  Amazingly you don't get this on default OS for the Kobo.
- Wallabag plugin.  You can download all your articles from Wallabag as epubs to read.
- OPDS catalog plugin.  You can connect to a remote OPDS catalog and download books from there.  I connect it to my [[id:e3180240-961b-4384-9f8a-5f23c5493af6][Calibre-Web]] instance.
- Highlights.  I use this a lot.  Nothing too fancy but it does the job.  It's quite nice the way it handles when a highlights spans multiple pages.  I use [[id:fc800120-c64b-4463-a304-8fe3a1ad9d24][KoHighlights]] to get the highlights on to my computer for processing.
- Book Map is also nice.
