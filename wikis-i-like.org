:PROPERTIES:
:ID:       7adb53eb-f11d-4ac1-a0f8-05a6a26b781a
:mtime:    20220627225143 20220216181631 20211229125123 20211127120858 20210724222235
:ctime:    20200411181240
:END:
#+TITLE: Wikis I like

See also [[id:b6e838d4-77ff-45e1-8e1d-a5397c881331][My garden circles]].

  - [[id:390b2d01-645f-49e4-9ce9-36755f6b54c3][Phil Jones]]'s [[http://thoughtstorms.info/view/RecentChanges][ThoughtStorms]].  Phil has been doing it for a long time. It has an old school look and I really like that.  In terms of content, it's precisely what I want to read in someone else's wiki - a hot mix of quick thoughts and long-evolved ideas to stumble through.  It's got wiki [[id:383691fb-2d07-4865-b69d-c1a479865056][personality]].
  - [[http://webseitz.fluxent.com/wiki/FrontPage][Bill Seitz]].  Bill's is chock full of history and content too.  It has a [[id:a0d7cf5a-5d56-4f39-a06d-e90943a26bb5][bliki]] feel (in fact, Bill calls it a WikiLog) - it has a [[id:22aec571-2dcd-4fc8-abe5-6f7ab4083fc5][stream]] of recent changes right on the landing page as an entry point to the rest of what's there.
  - [[https://www.zylstra.org/blog/wiki-frontpage/][Ton's wiki]].  Ton's has a very strong link to his blog (the wiki is embedded in it and mainly an index of posts from the blog).  I like to see [[id:ffbea278-7011-49d6-8c1e-c784d40abb6b][blogs and wikis]] intertwingled.  You could argue that Ton's whole site is a wiki or digital garden of sorts - most 'blog' posts will link to previous thoughts on the topic.
  - [[https://philosopher.life/#Root:Root%20%5B%5BLegal%20Notice%5D%5D][h0p3's wiki]]. I have not really delved deeply into h0p3's wiki, but there are a friend of Kicks.   Their wiki is some between '[[id:383691fb-2d07-4865-b69d-c1a479865056][wiki as personality]]' and wiki as performance art.
  - [[https://emsenn.github.io/index.html][Emsenn's Digital Garden]].  Their site has gone through a few incarnations, and the latest is a digital garden created and published via org-roam.  Lots of interesting stories to be found when taking a [[id:c88b2e27-cc2b-438c-b9ca-5d20e819051f][drift]] through their rhizomatous writing.
  - [[id:e558d566-d664-4566-8f6a-eef13353ba85][Nadia Eghbal]] . 'Learning in public' - I like that sentiment.  Not really a wiki (perhaps I should remove from here...).  I also like how Nadia's writing combines cultural references with other ideas.  I definitely want to make those same kind of links in my wiki / blog.
  - [[id:9d92104e-335b-4fb0-89c8-7b8d4973522a][Nick Sellen]]'s [[https://github.com/nicksellen/ponderings][ponderings]]. I like Nick's ponderings as an example of someone thinking out loud to themselves, in a way of interest to others.
  - [[http://tw.boffosocko.com/][Chris Aldrich's wiki]].  Chris has just started up a TiddlyWiki, and you can be certain he'll do something interesting with it.
  - Anne-Laure's [[https://www.mentalnodes.com/][Mental Nodes]].  Nice, clean design, and the backlinks and transclusion work really well for browsing around.  Plus I love the content.
  - [[https://gwern.net][gwern.net]]. A [[https://www.gwern.net/About#long-site][long site]] with [[https://www.gwern.net/About#long-content][long content]]. 
  - [[https://maggieappleton.com/garden][Maggie Appleton's Digital Garden]].  Full of lots of great long-form posts, often on PKM stuff.  Closer to blog/research than a rambling personal wiki though.
  - Weakty. https://weakty.com/
