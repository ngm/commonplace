:PROPERTIES:
:ID:       5d422c9b-0172-41df-8e83-39f6a6f9ec9a
:mtime:    20231026111854 20231025213301
:ctime:    20231025213301
:END:
#+TITLE: Capitalism is the root cause of the overshoot of planetary boundaries
#+CREATED: [2023-10-25 Wed]
#+LAST_MODIFIED: [2023-10-26 Thu 11:21]

[[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][Capitalism]] is the root cause of the [[id:1d5a84bb-9e9e-4bda-9458-44d017f2b617][overshoot of planetary boundaries]].

* Because

- The endless accumulation dynamics of capital are in contradiction with planetary boundaries.

#+begin_quote
The comprehensive ecological crisis is an expression of the contradiction between the planetary boundaries of growth and the endless accumulation dynamics of capital.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

- The capitalist mode of production practices a social metabolism with nature that pushes it to disrespect planetary boundaries.

#+begin_quote
capitalist mode of production practices a [[id:4f82d469-fdf9-4b85-9ad9-8267fca545b8][social metabolism]] with nature that pushes it to disrespect [[id:92d55d0f-2a21-4d82-a475-89becc54d3aa][planetary boundaries]]

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

- The treadmill of accumulation leads to a planetary overload.

#+begin_quote
The ‘treadmill’ of accumulation leads to a planetary overload and to an “overall break in the human relation to nature arising from an alienated system of capital without end.”

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

* Also

#+begin_quote
This ecological rift is the result of a social rift: the domination of humans over humans.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

Sounds very similar to [[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Bookchin]]. Interesting to hear that Marx said this also.  But yes capitalism could be described as the domination of humans over humans.
