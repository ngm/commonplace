:PROPERTIES:
:ID:       a50e7e68-5f75-43cc-8fcf-2f4d0bb23187
:mtime:    20231124171952
:ctime:    20231124171952
:END:
#+TITLE: Hotel Bar Sessions
#+CREATED: [2023-11-24 Fri]
#+LAST_MODIFIED: [2023-11-24 Fri 17:24]

+ A :: [[id:6ec161cb-6746-4bdb-8648-47ae56cd45d4][podcast series]]
+ Lives at :: https://hotelbarpodcast.com/

I like it.  Left-leaning philosophy chats with a Marxist twang and lots of reference to pop culture.
