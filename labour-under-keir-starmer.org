:PROPERTIES:
:ID:       9d13ba38-5b5f-4cb9-ad53-383ae6b331fc
:mtime:    20220119194947 20211127120918 20211024134037
:ctime:    20211024134037
:END:
#+TITLE: Labour under Keir Starmer 
#+CREATED: [2021-10-24 Sun]
#+LAST_MODIFIED: [2022-01-19 Wed 19:49]

[[id:d50fea56-389b-423a-954a-46a800b5e228][Keir Starmer]].

#+begin_quote
Labour’s proposed solution, however, was even more baffling. It proposed creating a “next-generation neighbourhood watch” by placing police hubs around the country and tapping technology like video doorbells and Whatsapp groups to help surveil communities.

--  [[https://mailchi.mp/techwontsave.us/amazon-is-always-watching][Amazon is always watching]] 
#+end_quote

#+begin_quote
Let’s be very clear about what this would mean: Amazon Ring cameras and apps like Neighbors, its neighborhood watch app, would become even more central to policing. It’s an incredibly dystopian proposal.

--  [[https://mailchi.mp/techwontsave.us/amazon-is-always-watching][Amazon is always watching]] 
#+end_quote
