:PROPERTIES:
:ID:       1bb4bed6-9699-4bd7-8c03-932c9fff904d
:mtime:    20211127120808 20210724222235
:ctime:    20210724222235
:END:
#+title: CTZN
#+CREATED: [2021-04-15 Thu 18:45]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Decentralised social network.  Something to do with [[id:9c6a4a14-1c63-4c49-b769-a241477e5767][Hypercore Protocol]].

+ https://github.com/pfrazee/ctzn
+ https://ctznry.com/

I created an account: I am neil@ctzn.one.  I like many things about [[id:9c6a4a14-1c63-4c49-b769-a241477e5767][Hypercore Protocol]] so interested to see how this works.  I am guessing it has more of a P2P model, similar to the Beaker chat, with the servers acting as 'pubs'.  That said, the idea of another chat silo doesn't fill me with joy, so I wonder where the data is stored.  I still think the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] (or [[id:1550b2cf-584d-4bf7-ba81-9fafea0af1c0][Solid]]) approach is the best philosophy (for me personally).

#+begin_quote
Decentralized hosting (anybody can create a server, and data syncs using the Hypercore Protocol).

-- [[https://github.com/pfrazee/ctzn][GitHub - pfrazee/ctzn: A distributed social network mad science experiment]] 
#+end_quote

* [[id:d5719694-9e43-43ab-a4c6-df858e513984][Servers should be background infrastructure]].
  
* Log
**  [2021-04-17 Sat] 
  + Super easy to get set up and posting
  + Really like cross server community interest groups, always missed this in Fediverse
  + Really intrigued by potential of  customisable [[id:9c7743a9-cac0-4f06-8a98-5c518377e612][reaction]]s - feels like it could avoid gamification while avoiding the [[id:9f9fcdc3-da94-451c-b0d0-e0d840f368b2][+1 problem]]
  + No clue where my data is being stored
