:PROPERTIES:
:ID:       1d15f2e5-c6b6-4efa-8d12-4c88a9506781
:END:
#+title: The silencing of climate protesters in English and Welsh courts

Latest in a long line of crackdowns on defences that climate protesters can use in the courts. One of which made it an offence to use the words 'climate change' in court.
