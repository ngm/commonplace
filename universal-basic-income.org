:PROPERTIES:
:ID:       50e8960f-1b4f-4f8d-8619-7fd64754d938
:mtime:    20221203171819 20220820171246 20220728212256 20211127120837 20210724222235
:ctime:    20200404123527
:END:
#+TITLE: universal basic income

#+begin_quote
Research has shown repeatedly that such programs—known as Universal Basic Income—are remarkably effective in improving quality of life in communities around the world, in both the Global North and South.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
Programs consistently report reduction in crime, child mortality, malnutrition, truancy, teenage pregnancy, and alcohol consumption, along with increases in health, gender equality, school performance—and even entrepreneurial activity.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

Some prefer [[id:7e288212-b1fc-4116-81f2-e2e6c6c88e2b][Universal basic services]] over [[id:50e8960f-1b4f-4f8d-8619-7fd64754d938][universal basic income]].  e.g. [[id:816019df-3008-46c4-82b3-3f00bdfc70a7][Universal Basic Income or Universal Basic Services?]]


#+begin_quote
As the name suggests, most UBI plans—and the variants are many—propose that the state furnish all of its citizens with some kind of sustaining stipend, regardless of means tests or other qualifications. Most versions propose a grant at least equal to the local poverty line, in theory liberating recipients from the worst of the want and gnawing fear that might otherwise beset them in a time of mass disemployment.

-- [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]]
#+end_quote

#+begin_quote
Held up to sustained inspection, the UBI can often seem like little more than a neoliberal giveaway.

-- [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]]
#+end_quote

#+begin_quote
Its proponents on the market right clearly anticipate it as a pretext to do away with existing benefits, siphoning whatever transfers are involved in it back into the economy as fees for a wide variety of educational, healthcare and family services that are now furnished via social provision.

-- [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]]
#+end_quote
