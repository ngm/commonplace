:PROPERTIES:
:ID:       5ed3ff81-abd0-4dd9-b355-a834c26ba67b
:mtime:    20211127120844 20210724222235
:ctime:    20200815144351
:END:
#+title: Beware of the collector's fallacy

#+begin_quote
Because ‘to know about something’ isn’t the same as ‘knowing something’.

-- [[https://zettelkasten.de/posts/collectors-fallacy/][The Collector’s Fallacy • Zettelkasten Method]]
#+end_quote
