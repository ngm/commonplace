:PROPERTIES:
:ID:       c2399b28-813a-4eb8-b30c-53825493bf2b
:mtime:    20211127120941 20211002093825
:ctime:    20211002093825
:END:
#+TITLE: Labour's green policies
#+CREATED: [2021-10-02 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ [[id:c333714a-075a-47c4-ac19-a6fc3c6965c2][green investment fund]]
+ mass retrofitting programme
+ pledge to decaronise steel
+ "net zero and nature test" for every policy

#+begin_quote
During its party conference in Brighton, Labour set out measures including an annual £28bn [[id:c333714a-075a-47c4-ac19-a6fc3c6965c2][green investment fund]], a mass retrofitting programme, a pledge to decarbonise steel and a “net zero and nature test” for every policy.

-- [[https://www.theguardian.com/politics/2021/oct/01/climate-experts-give-cautious-welcome-to-labours-green-policies][Climate experts give cautious welcome to Labour’s green policies | Labour con...]]
#+end_quote

[[id:f822ddda-a977-47f7-829f-4c451f017915][Labour Party]].
