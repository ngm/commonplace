:PROPERTIES:
:ID:       b20c16dc-3891-4c35-ae31-aa63755be990
:mtime:    20221121144033 20221113161806
:ctime:    20221113161806
:END:
#+TITLE: It is possible to create cutting-edge systems using technologies that are not state-of-the-art
#+CREATED: [2022-11-13 Sun]
#+LAST_MODIFIED: [2022-11-21 Mon 14:40]

#+begin_quote
The current market for electronic products depends on planned obsolescence: old products quickly become outdated and unfashionable. But extending the life of our electronic devices helps to address the e-waste problem. Project Cybersyn showed that it is possible to create a cutting-edge system using technologies that are not state-of-the-art. It demonstrates that the future can be tied to the technological past.

-- [[id:89b83bd4-58b4-41e1-bb73-c111bc29c0de][The Cybersyn Revolution]]
#+end_quote

#+begin_quote
Perhaps governability can be achieved by reconfiguring tools that already exist; perhaps it is necessary to make new ones. Tiziana Terranova (2014), who has pro- posed the complementary idea of a “red stack”, writes that insurgent stacks become “new platforms through a crafty bricolage of existing technologies, the enactment of new subjectivities through a detournement of widespread social media literacy”. Older technologies may be better suited to this than newer ones (Maxigas and Latzko-Toth 2020).

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote
