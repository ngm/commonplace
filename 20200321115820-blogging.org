:PROPERTIES:
:ID:       40e45410-e030-49ac-bf14-075f230f240b
:ROAM_ALIASES: Blogs
:mtime:    20211127120828 20210724222235
:ctime:    20200321115820
:END:
#+TITLE: Blogging

#+begin_quote
Blogs are, in many ways, the child of BBS culture and mailing lists. They are a unique innovation on that model, allowing each person to control their part of the conversation on their own machine and software while still being tied to a larger conversation through linking, [[id:f7edad20-2866-4a73-840d-657f1e73df33][backlinks]], tags, and RSS feeds.

-- [[https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/][Can Blogs and Wiki Be Merged? | Hapgood]] 
#+end_quote

#+begin_quote
Blogs value a separation of voices, the development of personalities, new posts over revision of old posts. 

-- [[https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/][Can Blogs and Wiki Be Merged? | Hapgood]] 
#+end_quote

#+begin_quote
They are read serially, and the larger meaning is created out of a narrative that expands and elaborates themes over time, becoming much more than the sum of its parts to the daily reader.

-- [[https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/][Can Blogs and Wiki Be Merged? | Hapgood]] 
#+end_quote

Kind of the idea that you are following a person, not a blog.  I'm sure Ton or someone else on the IndieWeb has mentioned that before.

#+begin_quote
Through reading a good blogger on a regular basis, one is able to watch someone of talent think through issues

-- [[https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/][Can Blogs and Wiki Be Merged? | Hapgood]] 
#+end_quote

Same.


- [[id:22aec571-2dcd-4fc8-abe5-6f7ab4083fc5][the Stream]].
[[id:0cf82905-ed96-4bb4-a374-32dab19d4317][- Blog tooling]].

* Blogging more
  
#+begin_quote
It strikes me there are a few flavors of epistemic uncertainty for the blogger
#+end_quote

[[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]] has talked before about the barriers to blogging and ways to navigate them in his post on [[https://www.zylstra.org/blog/2018/01/how-to-blog-more/][blogging more]].

* Blogging together
  
#+begin_quote
I think when we talk about “[[id:c2bd4607-581f-4aa3-9b35-ae79b16b5014][networked communities]]” that’s one of the ideas we’re getting at: that we can be part of multiple communities at once, with shared, partially overlapping sets of interests. And that we can do this while tending our own digital garden, without having to maintain accounts on a dozen different third-party platforms.

-- [[https://www.brendanschlagel.com/2019/09/01/weaving-a-public-web-or-why-dont-i-blog-more/][Weaving a public web, or, why don’t I blog more? – Brendan Schlagel]] 
#+end_quote

#+begin_quote
I’m hoping that starting up a few will lower the psychological barriers to publishing and give me some momentum to continue posting more regularly.

-- [[https://www.brendanschlagel.com/2019/09/01/weaving-a-public-web-or-why-dont-i-blog-more/][Weaving a public web, or, why don’t I blog more? – Brendan Schlagel]] 
#+end_quote

[[id:cedad0d8-8e72-452f-8dc3-b402a4a04339][Blogchains]] as a way of lowering barriers to [[id:40e45410-e030-49ac-bf14-075f230f240b][blogging]].

#+begin_quote
And networks can’t self-sustain without participation - so maybe the best argument for blogging is not because it’s good for you but because it’s good for… us?

-- [[https://tomcritchlow.com/2019/09/04/networked-communities-2/][Networked Communities 2 - Blogging as a Social Act]] 
#+end_quote

#+begin_quote
And this cross-domain blogchain is another way to experiment
#+end_quote

Blogchains and "blogging together".
