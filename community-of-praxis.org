:PROPERTIES:
:ID:       d57a6514-7b0e-4dc5-ba5c-974ae1cdd95a
:mtime:    20211127120954 20211008145539
:ctime:    20211008145539
:END:
#+TITLE: Community of praxis
#+CREATED: [2021-10-08 Fri]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Like a [[id:9708006a-52ad-4864-90c0-9f0f08616382][community of practice]] but more political and [[id:946c81bf-503a-4d6e-b591-d970730357c9][Praxis]]-oriented.
