:PROPERTIES:
:ID:       f5bbecd7-af72-43f2-a6ad-969181a4ae4f
:mtime:    20230722095544
:ctime:    20230722095544
:END:
#+TITLE: Abundance (think tank)
#+CREATED: [2023-07-22 Sat]
#+LAST_MODIFIED: [2023-07-22 Sat 09:57]

+ URL :: https://www.in-abundance.org/

[[id:101619b4-b87b-4771-aa2b-14d52edf62c1][Keir Milburn]], [[id:ddb998c8-abf9-47a0-b666-e17e441a7e6f][Kai Heron]].

Looks good. Seems to be focused on [[id:b6c98503-1b96-496f-a805-287e7cda1f46][public-commons partnerships]].
