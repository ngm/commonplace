:PROPERTIES:
:ID:       45c67c6c-79e7-47cf-aa46-22004de29bfc
:mtime:    20221022135049
:ctime:    20221022135049
:END:
#+TITLE: Fragment on Machines
#+CREATED: [2022-10-22 Sat]
#+LAST_MODIFIED: [2022-10-22 Sat 13:53]

#+begin_quote
In the “Fragment,” Marx seems to sketch a future of fully automated production, “an automatic system of machinery,” 34 perhaps even a [[id:1d0fa725-6b14-4ecb-9705-283dd56b782e][Fully Automated Luxury Communism]] driven by a “[[id:5701607d-aa70-4f8b-a8a4-6661cea4cf85][general intellect]]”—an assemblage of accumulated technical knowledge that could be an anticipation of the digital networks of the internet

-- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
#+end_quote
