:PROPERTIES:
:ID:       ffefbcf6-52c9-4f5a-a8ad-10a1772ead2c
:mtime:    20220402173109
:ctime:    20220402173109
:END:
#+TITLE: Metamodernity
#+CREATED: [2022-04-02 Sat]
#+LAST_MODIFIED: [2022-04-02 Sat 17:37]

I don't understand it yet.  But if it's kind of an attempt at some synthesis of [[id:96671bbd-9526-419b-a306-012963451a68][Modernism]] and [[id:2db89aa0-38bb-4eea-ae75-3fff2343b481][postmodernism]] that sounds worth looking in to more.

See [[id:84f65cfd-112c-42e4-822c-84a05fad1ff6][metamodernism]].

 #+begin_quote
The short answer is metamodernity is a systems perspective; "it is about seeing the world as a process and not as fixed circumstances, a world in which there are not isolated phenomena but where everything is interconnected and interdependent..."

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
  #+end_quote
 
#+begin_quote
More than a cultural trend (or 'ism'), metamodernity is a meaning-making code—one that encompasses cultural codes from every epoch of the human experience.

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

#+begin_quote
Andersen argues that "we need metamodern minds that can relate to the intimate indigenous, the existential premodern, the democratic & scientific modern, and the deconstructing postmodern simultaneously" (p. 128).

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

Interesting, so a synthesis of [[id:fd7012bc-d6cd-4e13-8e5c-a4124009fc86][Indigenous and traditional ecological knowledge]], [[id:96671bbd-9526-419b-a306-012963451a68][Modernism]] and [[id:2db89aa0-38bb-4eea-ae75-3fff2343b481][postmodernism]]?  And premodern as well?

#+begin_quote
It is only through this synthesis and adoption of the metamodern code, she stresses, that we'll have the capacity to make good decisions to guide the necessary changes to our current systems and institutions.

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

#+begin_quote
The 'hyper-modern' alternative is not a good one; think: much an exaggerated version of what will turn out to be mere glimpses of what we're seeing right now—such as rise in authoritarianism, surveillance society, extreme inequality, and of course, climate change. 

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

#+begin_quote
Metamodernity provides us with a framework for understanding ourselves and our societies in a more complex way.

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

#+begin_quote
Metamodernity is a way of strengthening local, national, continental, and global cultural heritage among all.

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote

#+begin_quote
It thus has the potential to dismantle the fear of losing one's culture as the global economy as well as the internet and exponential technologies are disrupting our current modes of societal organization and governance.

-- [[id:430de680-7f06-4bef-87ec-2e510ea7aa1c][Lene Andersen, Metamodernity, Meaning and Hope in a Complex World]]
#+end_quote
