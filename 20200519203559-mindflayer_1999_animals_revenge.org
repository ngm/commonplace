:PROPERTIES:
:ID:       cd9c2636-cd9f-4e4e-924e-ef02d3a2200d
:mtime:    20211127120949 20210724222235
:ctime:    20200519203559
:END:
#+TITLE: Mindflayer - 1999 Animals Revenge

[[https://invidious.snopyta.org/watch?v=NEClnx8q3Nc][Video]]

Patrick introduced me to [[id:be57c72b-6cf6-4dbd-9881-ddcf1fd0f229][noise rock]] way back when, through the likes of Lightning Bolt and similar.  Mindflayer are part of that Fort Thunder Providence crew.  This track is just hypnotic sludgy goodness.
