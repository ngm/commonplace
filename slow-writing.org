:PROPERTIES:
:ID:       dd8cb931-b7d4-4e7d-921e-88338470400b
:mtime:    20211127120958 20210724222235
:ctime:    20210724222235
:END:
#+title: Slow writing

#+begin_quote
In other words, the foraging, summarizing, and linking parts of the process are often separated by weeks.

- [[https://hapgood.us/2016/10/20/slow-writing-with-wikity/][Slow-Writing with Wikity | Hapgood]] 
#+end_quote

Kind of similar to [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]].

