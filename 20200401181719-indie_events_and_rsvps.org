:PROPERTIES:
:ID:       243dc6e6-a3af-4ddb-8eba-be321da8e4a5
:ROAM_ALIASES: De-Meetupifying
:mtime:    20211127120813 20210724222235
:ctime:    20200401181719
:END:
#+TITLE: Indiewebifying event discovery and RSVPs

Getting events out of the silos seems like a hot topic this year, with plenty of [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] and [[id:cb7f18be-33c1-4f0e-9fe5-e6304e5b3e99][Fediverse]] activity around it.  The COVID-19 pandemic might be changing the nature of events for the foreseeable, but there's still plenty happening online.

A couple of weeks ago at IndieWebCamp London, Jamie led a session about owning your RSVPs: https://indieweb.org/2020/London/OwnYourRSVPs

Inspired by the session, my day 2 hacking revolved around [[https://indieweb.org/Events][events]] and [[https://indieweb.org/rsvp][RSVPs]].   The plan was to try indie-fy my event discovery a bit, and also try and decouple myself from Meetup a little for events there.  I want to get a feed of upcoming events that I might be interested in in my social reader, and RSVP to them on my own site.  

I'd also quite like to have an 'agenda' page, where I publish the events that I'm going to.  If people want to, they can follow my events agenda, and it could be a helpful way to discover events that are going on.  Other IndieWeb peeps such as Calum, Jamie and Seb are already doing this.  (Not everyone might want to do this - there are obviously privacy implications).

I'd also like to have the events that I've decided to go to show up in my calendar.  

#+TOC: headlines 2

* Getting events into my reader
  
I don't really have a good way of finding out about events at the moment.  When I moved to a new town last year I sadly found that the silos of Meetup and Facebook were the best places to find out about events and what was going on.  

Though it might take a while for the world to move away from listing their events in these platforms, in the [[id:cb7ba722-2be4-4b40-a06c-36494ff26acf][interim]] I would like a way of finding out about interesting events without having to visit lots of disparate sites around the Internet to find them.  So a good place to try and get this would be in my [[https://indieweb.org/social_reader][social reader]].
  
** Meetup
   
One big source of events is [[https://indieweb.org/meetup.com][Meetup]].  The service is going down a slippery slope of sales to corporates (first to WeWork, [[https://www.reuters.com/article/us-wework-divestment-meetup/wework-sells-social-network-meetup-to-alleycorp-private-investors-idUSKBN21H2K6][recently to some private investors]]), so what better time to avoid as much as possible.

As Jamie discussed in the London session, [[https://www.jvt.me/posts/2019/08/31/microformats-meetup/][he has built a service]] that wraps Meetup's API and can give you any Meetup groups' list of events, or any particular individual event, in Microformats.  
  
To do that, very handily, you can just take any event or event list from Meetup and change the domain to his service at: https://meetup-mf2.jvt.me

So, say, I am interested in finding out about events from  https://www.meetup.com/Lancaster-Digital-Friday-Coffee/events/, so I just need to go to   https://meetup-mf2.jvt.me/Lancaster-Digital-Friday-Coffee/events/ and I get a feed of the events in MF2.  Nice!

Now I have the mf2 I can a feed into my reader.  I created a new *Events* channel in [[https://indieweb.org/Aperture][Aperture]] and subscribed to the link in this channel. 

Now they show up in my reader!  

** IndieWeb events

I can also subscribe to events from other feeds of events (e.g. [[https://events.indieweb.org][events.indieweb.org]]).  I don't think there's a way to subscribe to a Facebook event feed.  That would be rad though, to get that feed into my reader.

** Better display of event posts in readers

The social readers don't seem completely aware of the event type at present.  (Indigenous does allow you to RSVP to them).

One wrinkle at present is that the Meetup I am following, all of the events were created to be recurring about 3 years ago. So their published/updated date is 3 years ago, even though the event's start date might be next week.   The readers seem to mostly work off of published date at the moment. 

#+ATTR_HTML: :width 100%
 [[file:pulling_the_events_into_my_social_reader/2020-03-15_10-52-14_3tfH9i4.png]]

#+ATTR_HTML: :width 100%
 [[file:pulling_the_events_into_my_social_reader/2020-03-15_10-50-35_ylXBC2o.png]]
 
It could be good if readers might have a calendar view for events, like they have special Gallery or Map views. Or maybe just a simple 'sort by start date' would do it.

#+ATTR_HTML: :width 100%
 [[file:pulling_the_events_into_my_social_reader/2020-03-15_10-59-53_e2jVr0c.png]]

* Own your RSVPs: RSVPing and POSSEing
  
So when I find out about an interesting event, I want to RSVP to it.
  
RSVPing to IndieWeb events already works fine for me, thanks to the [[https://indieweb.org/Post_Kinds_Plugin][Post Kinds]] and [[https://indieweb.org/Wordpress_Webmention_Plugin][Webmention]] plugins for WordPress.  I can create an RSVP on my own site, linking to the page where the event is listed, and the RSVP gets sent over and recorded on the event page.

For something like Meetup, which doesn't accept webmentions for RSVPs natively, we have to do some work there ourselves.

But thanks to [[https://brid.gy][Bridgy]] and the [[https://www.jvt.me/posts/2020/02/17/meetup-bridgy-support/][recent Meetup integration that Jamie added to it]], we can do it!
   
** Setting up the Bridgy connection
   
First up you'll want to go to [[https://brid.gy][brid.gy]] and connect it to your Meetup account:
 
#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_11-54-45_n2fyxL8.png]]
 

#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_11-55-54_W6qM8OO.png]]

Once it's connected, if you create an RSVP post, you can send a webmention to Bridgy about it, and Bridgy will send that RSVP to Meetup.  Let's give it a go.

** Creating an RSVP post
   
For me in WordPress creating an RSVP post is all already taken care of by Post Kinds.

#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_12-01-58_VvzVnIC.png]]

One really nice feature of Post Kinds is it uses [[https://github.com/dshanske/parse-this][Parse This]] behind the scenes to pull in contextual information about the event you're RSVPing to.  Just chuck the URL in the URL box and it'll pull the data back.

#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_12-05-00_P2xWG7u.png]]

Save the post and you have your RSVP - all owned on your own site for your safekeeping and historical pleasure.

** Sending your RSVP to Bridgy
   
We usually want to tell the event host that we're going to their event.  We want to send our RSVP to them.  For Meetup, our RSVP goes via Bridgy.

You can manually tell Bridgy about a post you've made that you want it to do something with.  This is a handy way of testing it out, and that's what I did with my first RSVP:

#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_12-11-47_MDOuZPS.png]]

I hit Send there, got a success message, and... lo and behold...

#+ATTR_HTML: :width 100%
[[file:RSVPing_and_POSSEing_to_Meetup/2020-03-15_12-14-05_fJmF3IQ.png]]

I've found out about an event on Meetup, and RSVPed, without actually visiting!

** Smoother POSSEing
 
I would like to have the option to push my RSVP to Meetup without having to go to Bridgy and put the URL in manually each time.  And you can do that of course.  The magic happens via the Webmention and Syndication Links plugins.  You can turn it on in your IndieWeb settings in the WordPress dashboard.

#+ATTR_HTML: :width 100%
[[file:Own_your_RSVPs:_RSVPing_and_POSSEing/2020-04-03_18-53-51_IklUOEZ.png]]

Now when you create a new RSVP, you can have it sent to Bridgy automatically:

#+ATTR_HTML: :width 100%
[[file:Own_your_RSVPs:_RSVPing_and_POSSEing/2020-04-03_18-56-15_3WBfJ1q.png]]

Boom!

Some social readers (e.g. Indigenous) also support RSVPing from within the reader themselves.  

#+ATTR_HTML: :width 100%
[[file:Screenshot_2020-04-03-19-00-17.png]]

So you can just hit RSVP in your reader and create the RSVP there and then.

* Bonus features
  
A couple of nice-to-haves around events and RSVPs.

** Agenda page
   
Seb has an [[https://seblog.nl/agenda][agenda page]] - a page with a list of his RSVPs.  I think this is a nice way of helping with event discoverability.  You could follow the agendas of people you know, and see when they RSVP to interesting events.

This comes out-of-the-box with [[https://indieweb.org/Post_Kinds_Plugin][Post Kinds]] again.  Each post kind gets it's own archive page - so for example,  https://doubleloop.net/kind/rsvp/ is a list of my RSVPs.  I added a simple redirect from https://doubleloop.net/agenda to that page.

** iCal feed

Jamie has written about how he created an [[https://www.jvt.me/posts/2019/07/27/rsvp-calendar/][iCal feed of his RSVPs]].  I'd like to do this too, to pull the events I've RSVPed to into my calendar.  I haven't done it yet, but it should be fairly simple - just parse through my 'yes' RSVPs, pull out the dates from the associated event, and turn it into iCal markup.
