:PROPERTIES:
:ID:       95fb7bf1-e4ea-495a-bd64-40bfc07a2de6
:mtime:    20211231180232
:ctime:    20211231180232
:END:
#+TITLE: The commons enables people to foster togetherness without compulsion
#+CREATED: [2021-12-31 Fri]
#+LAST_MODIFIED: [2021-12-31 Fri 18:02]

A claim from [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Gut feeling :: 6
+ Confidence :: 4
