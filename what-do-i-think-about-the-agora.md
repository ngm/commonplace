
# Table of Contents

1.  [Longer version](#org9d19b40)

[Agora](agora.md) as in <https://anagora.org>

tl;dr: It's rad.


<a id="org9d19b40"></a>

# Longer version

I like the connecting of [digital garden](digital-garden.md)s.  To build a [knowledge commons](knowledge-commons.md) together while retaining [agency](20200725132152-agency.md) and [autonomy](20200721194732-autonomy.md).

I'm inspired by the way in which FedWiki is described in the section in [Free, Fair and Alive](free--fair-and-alive.md) on  [A Platform Designed for Collaboration: Federated Wiki](a-platform-designed-for-collaboration-federated-wiki.md).  That is, [Combining multiple personal wikis provisions a better commons than one central wiki](combining-multiple-personal-wikis-provisions-a-better-commons-than-one-central-wiki.md).

I feel Agora achieves this same principle, but in a different way. ([What are the similarities and differences between Agora and FedWiki?](what-are-the-similarities-and-differences-between-agora-and-fedwiki.md))  It appears to allow for a greater plurality of **ways of making** personal wikis (as long as you can get your notes in to markdown format in a git repo, you're good), resulting, I think, in even wider scope for provisioning the commons.

That said, I would still like to see explorations in [peer-to-peer](peer-to-peer.md) connection of gardens - something perhaps [IndieWeb](indieweb.md) and webmention based.

But even if that were so, I think there is an important place for an aggregator such as Agora.  You can see Agora as an easy<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> way to get your garden on the web, for those without the time or inclination to learn how to publish their own website, while still retaining complete ownership of the data.  And it also connects you to other people out-of-the-box - you see what people have written on topics similar to you.  In some sense I see Agora as doing for indie gardens what [micro.blog](micro-blog.md) does for indie streams.


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Well, easy-ish. You can make it in a program such as Obsidian, Foam, logseq, org, or your favourite text editor of markdown, and you need to get it in to a public git repository.
