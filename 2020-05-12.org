:PROPERTIES:
:ID:       c15cb2d6-df62-4aca-8433-4d4ed13a41a6
:mtime:    20211127120940 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-05-12

* Read [[https://medium.com/@jonjalex/johnsons-message-is-very-deliberate-and-very-dangerous-here-s-how-to-combat-it-d336cae96348][Johnson’s message is very deliberate and very dangerous: here’s how to combat it]]
  
This article says that the change in the government's framing of [[id:15ecf8a8-1975-4096-8eb5-b6550708bac1][coronavirus]] is moving the narrative to one of individual responsibility.

Previously the frame was that we were under threat from an enemy, and they knew best what to do to protect us.  That didn't work.

Jon suggests a framing around citizenship, the *citizen* narrative:

#+begin_quote
The right role for government in this story is to equip and enable us: to share as much information as possible and as much power as possible, so that we can work together where we live to find a new and sustainable normal.
#+end_quote

