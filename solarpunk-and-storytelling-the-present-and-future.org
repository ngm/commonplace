:PROPERTIES:
:ID:       056f8446-a4c1-4ea4-97ae-953c6bcf8ebd
:mtime:    20211127120756 20210724222235
:ctime:    20210724222235
:END:
#+title: Solarpunk and Storytelling the Present and Future
#+CREATED: [2021-07-09 Fri 20:30]
#+LAST_MODIFIED: [2021-11-27 Sat 12:07]

+ URL :: https://thefirethisti.me/2021/06/25/81-solarpunk-and-storytelling-the-present-and-future-with-phoebe-wagner/
+ Publisher :: [[id:cf597fd6-fcd7-404c-b39f-4f86f2ed8f89][The Fire These Times]]

[[id:c2eee50e-313d-49a0-9688-f4ada05c4182][Solarpunk]].
