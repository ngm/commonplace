:PROPERTIES:
:ID:       92aa8ec1-0032-46e7-8fae-8d055312fb3c
:mtime:    20211127120913 20211120204837
:ctime:    20211120204837
:END:
#+TITLE: Rely on Distributed Structures
#+CREATED: [2021-11-20 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
This means that the (infra)structures should enable peers, teams, and local nodes to interconnect and form semi-autonomous spheres of self-provisioning and -governance. Each part of the whole can then operate semi-autonomously, according to its own distinct rules and situational needs, while also coordinating with their other semi-autonomous peers.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
