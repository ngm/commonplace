:PROPERTIES:
:ID:       572f4c67-300e-46fb-a81d-b6b8f07c1303
:mtime:    20211127120839 20210724222235
:ctime:    20210724222235
:END:
#+title: CS Unplugged
#+CREATED: [2021-06-20 Sun 13:37]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://csunplugged.org/en/
+ Summary :: "CS Unplugged is a collection of free teaching material that teaches Computer Science through engaging games and puzzles that use cards, string, crayons and lots of running around."



