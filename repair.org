:PROPERTIES:
:ID:       36fdfa5d-bb7e-41c1-8442-054bea1915ec
:mtime:    20220702140208 20211127120823 20211008165002
:ctime:    20211008165002
:END:
#+TITLE:Repair

* community repair events
  
* culture change
 
* problems
  - products are increasingly harder to repair 
** e-waste 
 - is one of the fastest growing waste streams in the world
 - 
#+begin_quote
Fifty million tonnes of e-waste is produced on an annual basis and that could grow to 120m by 2050 if left unchecked.
#+end_quote
 - question: how big is it currently, relative to others?
 - the amount of household appliances failing within 5 years of their purchase has increased over time
 
** carbon emissions
- CO2 emissions associated with device manufacture
- "around 80% of a small electronic device’s carbon footprint — over the whole of its lifecycle — is emitted before it even reaches UK shores."
*** dodgy carbon accounting
       - "The UK is, however, very special. Not only for its mind-blowing historical carbon debt, but also for its current, very creative, carbon accounting"
       - "We must take responsibility for our emissions, wherever they occur."
* policy
   - [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]]
   - [[id:6598013b-cebd-4a4c-bee7-db8fb4352dc8][ecodesign]]
   - international repair day
* misc
- [[https://collapseos.org/why.html][Collapse OS]]
- Using products for longer is one of the simplest actions we can take towards climate justice
 
* articles
- [[https://www.opendemocracy.net/en/oureconomy/why-taking-responsibility-our-carbon-emissions-means-promoting-right-repair/][Why taking responsibility for our carbon emissions means promoting the Right to Repair]]
- [[https://www.ft.com/content/990c7846-e5cf-11e9-9743-db5a370481bc][Giving old tech a second life]]
  - Also reusues the skills of people previously manufacturing 

#+begin_quote
According to Pete Seeger, "If it can’t be reduced, reused, #REPAIRED, rebuilt, refurbished, refinished, resold, recycled or composted, then it should be restricted, redesigned or removed from production". We couldn't agree more. #RightToRepair #EWWR2019
#+end_quote

- [[id:9cf9d627-b74a-4e45-bd52-31badf3dbbea][19 broken laptops]]

#+begin_quote
That's because Apple has designed the MacBook Pro such that fixing even one key requires replacing the entire keyboard apparatus, as well as part of the metal enclosure and some other components
#+end_quote

#+begin_quote
It's not great for tech consumers that buying an expensive service plan is the only way to have peace of mind when buying a $2,500 device.
#+end_quote

* Ecodesign Directive                                                

- EU's plans to reduce the environmental impacts of products
- make products last longer, and are easier to repair and recycle

* Manchester Declaration

- UK community repair movement
- came together in October 2018
- asking UK legislators, decision-makers, product manufacturers and designers, to promote Right to Repair

* Ecodesign Core

- product design that allows replacing key componenets with the use of commonly available tools

- spare parts availability for 7 to 10 years

- access to technical and repair guides  for repairers, such as the wiring diagrams or exploded views of products

* Felipe Fonseca
  - platform coops for connecting actors in reuse and repair 

* [[id:1a531fe0-e86e-4daf-991c-4da2b2c3ee05][Repairability standard]]

* Bookmarks

+ [[https://electronicplanet.xyz/2021/05/05/new-report-electronics-repair-and-maintenance-in-the-european-union/][New Report – Electronics Repair and Maintenance in the European Union]]

#+begin_quote
Nor did we design the technologies that increasingly exclude us. A few decades ago, a broken television could potentially be fixed by unscrewing the back and looking for a blown transistor. Try opening your smartphone today: they’ve built it to stop you doing that. It is just that technologies are blackboxed, designed both to control us (through data) and keep us out. The entire political and economic system is increasingly blackboxed in the same way. If it weren’t, we would be having emergency meetings in town and city halls every week to work out how to fix this crisis

-- [[id:eda04511-6a17-4173-98b1-b94b97cb259b][For a Red Zoopolis]]
#+end_quote
