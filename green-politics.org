:PROPERTIES:
:ID:       e8d69e81-3f02-4785-9fd1-353d8d3a5ea3
:mtime:    20220820105925 20220819183826
:ctime:    20220819183826
:END:
#+TITLE: green politics
#+CREATED: [2022-08-19 Fri]
#+LAST_MODIFIED: [2022-08-20 Sat 11:23]

#+begin_quote
However, specifically Green Party politics, in some states, has seen a movement of former Marxist-Leninists towards a revisionist understanding of politics, with revolutionary objectives being discarded.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
movements are reflected in the work of a single individual. André Gorz, the French ecological theorist, acted paradoxically to promote a movement from red to green, and conversely from environmentalism to anti-capitalist commitment. Best known for his book Farewell to the Working Class, the former Marxist argued that class conflict was largely redundant and new social movements, including environmentalists, represented a force for potential change

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
The present Fourth International, from Ernest Mandel’s line, is explicitly ecosocialist in nature.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
Roberts’ argument was that a lack of democratic involvement including an absence of workers’ control, led to a frustrated demand for consumer goods. The less we participate and have the ability to shape our life experience, the more we compensate by consuming wasteful goods.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
This was based on an understanding that capitalism drives environmental destruction and thus green political economy inevitably demanded an articulation with anti-capitalism, if it was to provide a realistic chance of overcoming ecological problems.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
Kovel noted the distinction between 'use values' and 'exchange values', discussed by Marx in the first chapter of the first volume of Capital, was essential to creating an ecologically sustainable society. Thus by making goods to last longer and providing communal products for use, human prosperity could grow without the waste of capitalism.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

● right to repair

