:PROPERTIES:
:ID:       a5fc50a9-93f6-4eb8-b6b7-6796f8bc347d
:mtime:    20220221180420 20220220124943
:ctime:    20220220124943
:END:
#+TITLE: Center for a Stateless Society
#+CREATED: [2022-02-20 Sun]
#+LAST_MODIFIED: [2022-02-20 Sun 12:51]

+ URL :: https://c4ss.org/

"a left market anarchist think tank & media center"

Never quite sure what left market anarchism is.  So I don't know fully what C4SS advocates.  But have read a bunch of interesting things from [[id:fda9a645-cdfd-46ce-a6bc-d435f4b57934][Kevin Carson]] before.
