:PROPERTIES:
:ID:       14417d38-cb2f-4d8e-b85d-5cc48f578c07
:mtime:    20220430180329 20220321192819
:ctime:    20220321192819
:END:
#+TITLE: Following Twitter people via Nitter and Miniflux
#+CREATED: [2022-03-21 Mon]
#+LAST_MODIFIED: [2022-04-30 Sat 18:03]

So I use a self-hosted [[id:865e6075-25e1-4017-9941-c4e520230d95][Nitter]] and self-hosted [[id:d4d7efc2-c373-4a63-8da0-df8b81cd098d][Miniflux]] combo to follow these feeds.

Nitter is an alternative front-end to Twitter, and very helpfully gives you an RSS feed of any account.  Miniflux is a minimal RSS reader.  So I just plop the Nitter RSS feeds in to Miniflux and read the local stuff there.

Both of them are installed via [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]], which makes it a doddle to set up.

* Issues 

Feed fetching from Nitter fails periodically.  But I just have to force refresh it a few times and it sorts itself out.


* [2022-03-21 Mon] 

Get 404s and 429s returned from Nitter via Miniflux fairly frequently.  I wonder if that's Nitter actually returning that, or Twitter?

* [2022-03-19 Sat] 

Since the [[id:21f94069-f68f-426c-8bf1-c92adad26c8d][RSS-Bridge]] thing was never much of a success, I've installed [[id:865e6075-25e1-4017-9941-c4e520230d95][Nitter]] on my [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]] to get RSS feeds of people I want to follow on [[id:cdda8ae9-b985-4e45-8c8d-f7e94674c3fd][Twitter]].

I could add the RSS feeds to [[id:02426552-0f6a-44be-9a50-107f76200c34][Aperture]], but I would feel kind of bad adding a feed for every single Twitter user I want, it's more load on the Aperture server.  

So I've also installed [[id:d4d7efc2-c373-4a63-8da0-df8b81cd098d][Miniflux]] via YunoHost too, interested to give that a pop as an RSS reader.
