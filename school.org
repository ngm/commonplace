:PROPERTIES:
:ID:       92e66d78-e8b7-48c4-878a-4d320842688a
:mtime:    20211127120913 20211126145135
:ctime:    20211126145135
:END:
#+title: School

#+begin_quote
When children attend schools that place a greater value on discipline and security than on knowledge and intellectual development, they are attending prep schools for prison.

-- [[id:cdfacefa-8f06-431b-97b5-15dab032c508][Angela Davis]]
#+end_quote


#+begin_quote
Of course, complications arise and multiply as young children grow up. They learn that some people are not trustworthy and that others don’t reciprocate acts of kindness. Children learn to internalize social norms and ethical expectations, especially from societal institutions. As they mature, children associate schooling with economic success, learn to package personal reputation into a marketable brand, and find satisfaction in buying and selling.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
