:PROPERTIES:
:ID:       f1610aaf-83a6-47ea-a544-25fb161c8eb0
:mtime:    20211127121010 20211120204643
:ctime:    20211120204643
:END:
#+title: Creatively Adapt & Renew
#+CREATED: [2021-06-12 Sat 11:45]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

Pattern for [[id:5c17484b-fd1e-4efe-88ba-770f7a8ebde0][Provisioning Through Commons]] from [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].

#+begin_quote
In a commons, there is no imperative to constantly expand production and profit, and so creativity can be focused on what really matters — ameliorating quality, durability, resilience, and holistic stability. Innovation need not be linked to boosting market sales and ignoring planetary health.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].
#+end_quote
