:PROPERTIES:
:ID:       d6ddaef1-d7b5-416f-8ddf-7ae442d0779b
:mtime:    20221224214855 20211127120955 20210724222235
:ctime:    20210724222235
:END:
#+title: Bristol Approach
#+CREATED: [2021-07-07 Wed 21:51]
#+LAST_MODIFIED: [2022-12-24 Sat 21:48]

#+begin_quote
Between 2016 and 2017, KWMC and Ideas for Change tested the Bristol Approach, exploring the potential of a [[id:353ba7d9-04c3-4003-bcb1-b60eb5039684][data commons]] as a tool for social change

-- [[id:e41f8a67-173a-470f-bb94-2d50c4693154][Undoing Optimization: Civic Action in Smart Cities]]
#+end_quote
