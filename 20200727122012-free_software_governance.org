:PROPERTIES:
:ID:       683d64d2-f81f-4317-a0ff-8bef024a947f
:mtime:    20211127120849 20210724222235
:ctime:    20200727122012
:END:
#+title: free software governance

I like [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]]'s [[https://communityrule.info/][CommunityRule]] site - listing a few different [[id:683d64d2-f81f-4317-a0ff-8bef024a947f][models of governance]] that might be followed in a free software project.  

#+begin_quote
Like choosing a Creative Commons license, CommunityRule offers a palette of templates, from dictatorship to various flavors of democracy. 

-- [[https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng][How’s That Open Source Governance Working for You?]] 
#+end_quote

#+begin_quote
being explicit matters. It helps avoid that [[id:710ed987-7b38-43bd-9d12-fd335d355515][the tyranny of structurelessness]], ensuring that the lines of responsibility are clear. A Rule also serves as a mirror, encouraging community members to ask whether the current structure really fits the nature of the community. Any Rule should include provisions for how the community can evolve its governance as it matures, as any community must.

-- [[https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng][How’s That Open Source Governance Working for You?]] 
#+end_quote

See [[id:853dd384-0544-4745-9eab-58b46356f61e][implicit feudalism]].

* Examples

- [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]] has a really interesting governance structure, involving special interest groups, a council, and consensus for decision making.  https://yunohost.org/en/yunohost_project_organization 
  
* Bookmarks

+ [[id:874cb75d-eb8e-4f78-81d9-4afaed3ed986][Online Communities Are Still Catching Up to My Mother's Garden Club]]
+ [[https://socialhub.activitypub.rocks/t/what-would-a-fediverse-governance-body-look-like/1497][What would a fediverse "governance" body look like?]] 

 
