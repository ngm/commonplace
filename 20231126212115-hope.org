:PROPERTIES:
:ID:       74b0c002-7c85-44ce-8bb0-0a155f538c2b
:mtime:    20231201153339
:ctime:    20231126212115
:END:
#+title: Hope

On the podcast [[id:53c5e1fa-38aa-4bee-9174-4b3c4aab12d7][Building Lifeboats to the Emerging Futures with Sophia Parker of the Joseph Rowntree Foundation]] they mention some different types of hope: pre-tragic hope; tragic hope; and post-tragic (emergent?) hope.  They recommend some writings from [[id:3bc90268-b15d-4fe9-b571-2328ce7f8205][Rebecca Solnit]] on the idea.

I liked the definition of they give in the podcast, something like: Hope is not optimism. Hope leads to action.

#+begin_quote
It's the space of existence where you don't know that the actions you take are going to make the difference, but you believe that they might.

-- [[id:53c5e1fa-38aa-4bee-9174-4b3c4aab12d7][Building Lifeboats to the Emerging Futures with Sophia Parker of the Joseph Rowntree Foundation]]
#+end_quote
