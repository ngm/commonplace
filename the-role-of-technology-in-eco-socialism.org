:PROPERTIES:
:ID:       d0fefec3-43c2-48ef-b533-02bc65d97a31
:mtime:    20221211220921 20221207215523 20221122202232 20221122192141 20221121150548 20221121104128 20221119113835 20221118213445 20221118114608 20221117232750 20221117213219 20221113172335 20221113161432 20221111201836 20221111122756 20221106115322 20221105221901 20221104214253 20221104150904 20221104114208 20221029170509 20221029153955 20221028115529 20221028104042 20221027184321 20221025215840 20221019205701 20221018165304 20221016161952 20221016121545 20221012205351 20221008180753 20221008114937 20220925195015 20220925143331 20220924185351 20220918141536 20220911102014 20220910124508
:ctime:    20220910124508
:END:
#+TITLE: The role of technology in eco-socialism
#+CREATED: [2022-09-10 Sat]
#+LAST_MODIFIED: [2022-12-11 Sun 22:09]

A [[id:d5d2d46f-f558-4934-8256-ca08dc3ef4be][review article]].  I plan to give a critical overview of existing work on what ecosocialism is, why it is needed, what the various strands are.  Then: what are its programmes and what are the existing currents for a transition to it, and how could ICT support both of these.

* Outline

#+begin_src plantuml :file project-wbs.png :exports results
@startwbs
+ ecosocialism and ICT [150h]
++ background [50h]
+++ ecosocialism [30h]
++++ socialism [20h]
+++++ broad overview [10h]
+++++ what is transition? [10h]
++++ environmentalism [10h]
+++++ broad overview [10h]
+++++_ climate crisis
++++ exsiting [10h]
+++++_ doughtnut economics, etc
+++ ICT [20h]
++++ broad overview [10h]
++++ worker struggles [10h]
++ synthesis [100h]
+++ socialism and ICT [15h]
++++_ review
++++_ analysis
+++ environmentalism and ICT [15h]
++++_ review
++++_ analysis
+++ ecosocialism and ICT [70h]
++++ existing work [10h]
++++ enumeration [40h]
++++ critical reflection [20h]
@endwbs
#+end_src

#+RESULTS:
[[file:project-wbs.png]]

* Intro

What is the role of information and communications technology (ICT) in supporting a transition to ecosocialism?

Both the climate crisis and global inequality are pressing concerns of our time [cite:@portner]. Ecosocialism is a merging of socialist and environmental politics that strives for equity of resources and a fair standard of living for all, all while staying within planetary boundaries.

ICT underpins much of the infrastructure and operation of modern society.  As a sector of industry, it has a significant environmental impact of its own; it also supports other industries; and it has an ever-increasing influence on how society communicates and is organised.

The way in which society uses ICT will undoubtedly play a role in how we transition to a more sustainable and equal world. For example, during the global protests of 2011 (the Arab Spring, Occupy, Movement of the Squares, etc) much was made of the use of social media to both agitate and organise ("Twitter revolutions", etc).  These protests ultimately failed to change the dominant capitalist paradigm under which the majority of the world operates.  What role could ICT play to transition to an ecosocialist future?  How can ICT workers act to bring this transition about?

* Background

** Eco-socialism

To give context and a point of reference, you can situate ecosocialism within a wider space of the sustainable development debate.  Within this space are a number of different tendencies with varying levels of concerns regarding the environment and socio-economic wellbeing and equality. These trends fall in to different bands of 'Status Quo', 'Reform', and 'Transition'.  Those which are most eco-centred and most desiring of socio-economic well-being and equality, are [[id:02f15035-9754-43a7-aa2b-b81fcf2b2e6c][ecofeminism]], [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][ecosocialism]], [[id:999c7df1-4d1a-4b47-abfb-ae3e0fe284d0][social ecology]] and indigenous/'south' movements.  They all fall into the 'Transformation' band. [[[id:288c437a-0e5d-4e4c-a655-e2dec3e54daf][Sustainable development: mapping different approaches]]]

*** Socialism
*** Environmentalism
*** Climate crisis
- [[id:1c5821aa-f289-48b6-96e5-c55153fb2a47][climate crisis]]
- [[id:48947b6d-cce7-49e7-9613-5373e7e48d15][Biodiversity loss]]
- [[id:fa91f034-1b2a-4995-ab40-daa52579cbd8][sixth extinction]]

*** Overview of different strands of ecosocialism

- e.g. [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
- [[id:4ab7e432-2182-4651-bf7d-c0f0805ab970][Climate Change as Class War: Building Socialism on a Warming Planet]]
- [[id:59ac676d-71b1-4e8f-a50f-ee409d4d1e4b][Climate Leviathan]]
- [[id:999c7df1-4d1a-4b47-abfb-ae3e0fe284d0][Social ecology]]
- [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][Degrowth]]
- FALC
- [[id:38a872a6-1b07-40b6-8bdd-30cba08ee59c][Andreas Malm]]
- [[id:4e3f4b07-d308-44ec-8b54-8e5b9fb75c24][Biocommunism]]
- [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]] gives a bit of an overview
- etc

In terms of their ideas on:

- transition
  - strategy and tactics
- future
  - programme
  - governance structures

*** Resources    
- [[id:de0f8ce3-c1ad-44fb-ae8f-1cc0e13fd289][Red-Green Revolution]]
+ Overview :: on ecosocialism and technology
+ Relation to question :: has strand of what role technology plays throughout it.  (with ICT mentioned explicity a few times)
+ How to use it :: to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism


** Information and communication technology and society

*** Technology and society in general

- [[id:a0548c62-534b-4978-946d-43f5993091f9][ICT is a fundamental part of modern society]]
- General stance that [[id:3c57b47d-49ea-4391-8c63-994f993b132c][technology alone will not create a better world]].  Against [[id:8cdc6cde-0de0-4032-aec4-a7ea3399c2fa][technological determinism]]. [[id:eb568c3e-a8a1-4bcb-b4fb-36163281fb94][We need to be thinking in terms of systems rather than technological quick fixes]].
- Draw on some of the STS stuff in [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]

*** ICT definition

*** Capitalism and ICT

**** Concepts
- [[id:e1680617-e291-427f-ac3c-ebd8a12f890d][The Stack]]

**** Claims

**** Resources

- [[id:a0da7876-5d03-4efb-8f7f-4fbfb91ed234][The Stack as an Integrative Model of Global Capitalism]]

* Eco-socialism and ICT

** Socialism and ICT

*** Concepts
- [[id:5f449b36-4f96-468c-a39c-bcdc12868443][Liberatory technology]]
- [[id:9ff4d7a5-9c34-49c8-8024-98a5863e8f52][Technology Networks]]
- [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]]
- [[id:3862763b-409e-4c92-bf44-8c30a649ef83][Governable stacks]]
- [[id:b2da45d6-5d8c-4263-b82e-4c5930e54be0][Convivial tools]]
- [[id:a9463a48-2ee3-4305-ab4f-4ce596af25e7][Telecommunism]]
- [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][Libre software]]
- [[id:450a654c-4edc-4c0c-90ea-f26999820dfd][Digital socialism]]
- [[id:3e022f2c-d890-43a0-92e4-6e68fd946596][Digital Tech Deal]]

*** Possible claims
- [[id:acdbe26b-7858-45d3-8ac7-11051deecbed][Modern information technology is one of the main conditions for socialist revolution]]
- A socialist ICT will depend on governable stacks
  - Libre software facilitates governable stacks
    - "Governable stacks should prioritise community accountability alongside individual freedom" ([[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]])
     
*** Related
- [[id:6303642b-998b-4850-b9aa-20f60de3245b][How not to deal with big tech]]

*** Resources

- [[id:152562ee-a03c-43ba-b919-33791cff50e7][Information Technology and Socialist Construction]]
- [[id:43961e5f-42e4-4749-aa48-d690730e3a9f][Technology of the Oppressed]]
- [[id:5edca101-03d7-4ef1-a921-7b3167176c09][Towards a Liberatory Technology]]
- [[id:4dae6c1f-a51b-41b9-b887-4d9eae246a84][Tools for Conviviality]]
- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
- [[id:98c43aa1-2b96-401e-8987-1c8205e46816][Technology Networks for Socially Useful Production]]
- [[id:08160100-0655-47a4-bfa6-b4846ae64c22][The Telekommunist Manifesto]]
- [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]]
  - Relation to question :: ICT in practice for facilitating socialism.  The book also reflects on the STS questions of how technology relates to society.
  - How to use it :: Conclusion is actually good place to start.
- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
  - Overview :: Luddism over time and its relationship to Marxism.
  - Relation to question :: explores relationship between technology and capitalism, and how technology might be used in socialism.
  - How to use it :: pick out anything that suggests how we might use technology for anti-capitalist / pro-socialist purposes.

- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
  - Overview :: Digital self-governance as a push back against digital colonialism.
  - Relation to question :: Identifies governance of 'the stack' - a key part of ICT - as crucial in anti-colonial (and hence anti-capital, hence socialist) struggle
  - How to use it :: see how ICT workers may use/build governable stacks

- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
  - Overview :: Expansion and update of ideas from [[id:4ae309e4-0084-42b8-9206-16ad8c3238b3][A Hacker Manifesto]] e.g. [[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]] and [[id:6bf4c9fa-8c23-45f3-90ae-f784dd8af415][Hacker class]].
  - Relation to question :: [[id:6bf4c9fa-8c23-45f3-90ae-f784dd8af415][Hacker class]] is a class representation of ICT/knowledge workers?
  - How to use it :: see if it gives useful outline of how ICT workers can be part of the struggle, any suggestions of [[id:990e7dc0-ea59-4e5f-aa1a-c6ff27a162c4][Praxis for the hacker class]].


** Environmentalism and ICT
*** Concepts
- [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]]

*** Claims
- [[id:e2ac250c-e92b-4e78-8deb-8a10b63e7746][Extending the life of electronic devices helps to addresses the e-waste problem]].
- [[id:b20c16dc-3891-4c35-ae31-aa63755be990][It is possible to create cutting-edge systems using technologies that are not state-of-the-art]].
- 

*** Resources
- [[id:ce7acc0e-0fd7-468c-af6c-10a94b932cb7][Doing the Doughnut.tech]]
- [[id:3e8542f0-9634-4edf-b707-f5618c208ba6][Server Infrastructure for Global Rebellion]]

** Ecosocialism and ICT

What do the existing eco-socialist tendencies say about technology and specifically [[id:baef8183-00e6-46ee-8346-baef86c3c666][information and communication technology]] ([[id:4f2e9822-8e1e-4599-92eb-e9fc5e948bd5][big tech]] etc)

*** Various prongs

The material impacts prong has more coverage at present.  I want to focus more on the political/organisational impacts.

**** Material impacts
- The Materialist Circuits and the Quest for Environmental Justice in ICT’s Global Expansion
- [[https://www.thegreenwebfoundation.org/publications/report-fog-of-enactment/][Report: Fog of Enactment - The Green Web Foundation]] 

**** Control impacts, surveillance, hegemony

**** Organisational impacts
- how can we use it for organisation and transition?
- how does it get co-opted (e.g. cambridge analytica, trump, brexit, etc)

*** Concepts
- [[id:0337f84a-df36-4275-a9fa-1b70778a2cbf][Ecosocialist technology]]
- [[id:ef014409-4969-418b-8f4f-1c550369e17e][The Lucas Plan]], [[id:45368db7-3411-4fd1-85c5-d6f146a301bf][Just transition]], [[id:c22ba44c-7c08-4682-8fe5-6df24fd12ad4][Green New Deal]],
- [[id:3862763b-409e-4c92-bf44-8c30a649ef83][Governable stacks]]
- perhaps also looking at how technology might be used in some of the climate leviathan quadrants, as counterpoints

*** Claims


*** Resources
- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]

- [[id:a28ec79b-9982-407e-b6c9-c2b4e33dfe10][Technology and Ecosocialism]]
  - Overview :: comparison of ecosocialist and capitalist technology, sectoral overview of ecosocialist technologies.
  - Relation to question :: has strand of what role technology plays throughout it. 
  - How to use it :: to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism
  - Relation to question :: seems very relevant.

- [[id:de0f8ce3-c1ad-44fb-ae8f-1cc0e13fd289][Red-Green Revolution]]
  - Overview :: on ecosocialism and technology
  - Relation to question :: has strand of what role technology plays throughout it.  (with ICT mentioned explicity a few times)
  - How to use it :: to give overview of technology and ecosocialism specifically, and to get some initial thoughts on ICT and ecosocialism
  - Relation to question :: seems very relevant.

* Points to intervene

With regards to systems thinking.  What are the points to intervene in a system with regards to a transition to eco-socialism - earth system, political systems, economic systems?  Through the lens of technology, presumably.

- [[id:eb568c3e-a8a1-4bcb-b4fb-36163281fb94][We need to be thinking in terms of systems rather than technological quick fixes]]

* Misc

- Network technology (social media) was discussed heavily as part of the 2011 uprisings, though all of those ultimately did not bring a socialist outcome long-term. Another political approach needed, maybe another technology tool too?
- Perhaps self-governance is a part of that. c.f. [[id:3862763b-409e-4c92-bf44-8c30a649ef83][Governable stacks]].  Take back the stacks.
- "These instructions assume you are using Microsoft Word, but other word-processing software should work in a similar way."  we are pushed down a certain path.
- [[id:c2eee50e-313d-49a0-9688-f4ada05c4182][Solarpunk]] maybe

** Unsorted

- technology is a site of struggle
- focus on digital technology / ICT
- [[id:84656e58-0af4-4b7a-8dc5-632bffbbef84][digital activism]] will be part of a transition presumably
  - [[id:0a314a6f-e239-406d-8e31-a591a762303b][From Cyber-Autonomism to Cyber-Populism: An Ideological Analysis of the Evolution of Digital Activism]]
- [[id:451989b1-5163-461f-bf61-7fdfd80e96f7][Communicative capitalism]]
- [[id:4db92dd5-f837-4ecb-b0cc-f97cb3d6f5ce][Vectoralism]]
- Look at  existing overlaps of ICT and socialism/communism. Existing mergings of ICT and environmentalism. Mergings of socialism and environmental. 
- Matrix of different uses of ICT - e.g. in protest, in economic planning, in organisation, etc. Other can be  how governable, how libre, etc.
- Look at 3IR and 4IR for ideas of tech to look at - maybe via Coop Jackson
  - but critique Scwab's framing
    - see e.g. "The Impact of 4IR Digital Technologies and Circular Thinking on the United Nations Sustainable Development Goals"
- strongly critique much of the SDGs and technology papers
  - e.g. Unleashing the convergence amid digitalization and sustainability towards pursuing the Sustainable Development Goals (SDGs): A holistic review
  - [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]] provides good general critique of the type of technologies referenced
  - ICT and Sustainability: Looking Beyond the Anthropocene looks very good.

** Learning plan and log

*** Plan
*** Log
- [[id:a0da7876-5d03-4efb-8f7f-4fbfb91ed234][The Stack as an Integrative Model of Global Capitalism]]
  - 2022-11-06
  - 2h 
  - 3
  - Useful to learn about the idea of the Stack, i.e. big tech platforms and their control over whole vertical sections of ICT.  Additionally, provides some commons-based options as ways to intervene.

- [[id:2dcfe016-9477-475a-b970-56ec69493d68][First and Third World Ecosocialisms]]
  - Date read :: 2022-11-11
  - Time spent :: 1h 15m
  - Level :: 2
  - Reflection :: Useful for further understanding of the debate on different aspects of ecosocialism.  Peripheral mention of technology in the abstract, but not specifically much to do with ICT (one mention of repair, though)

- [[id:89b83bd4-58b4-41e1-bb73-c111bc29c0de][The Cybersyn Revolution]]

* Writing time log
:LOGBOOK:
CLOCK: [2022-11-13 Sun 16:14]--[2022-11-13 Sun 17:02] =>  0:48
:END:


