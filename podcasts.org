:PROPERTIES:
:ID:       d5aab574-a7f5-45e2-a5b6-01f496f847d2
:mtime:    20240302115628 20230814160259 20230814144109 20211127120954 20210724222235
:ctime:    20210724222235
:END:
#+title: Podcasts

[[id:03d11fc6-6fd6-4d26-a8f5-cff132e25a21][I love podcasts]].

Here's a list of [[id:6ec161cb-6746-4bdb-8648-47ae56cd45d4][podcast series]] that I'm subscribed to, periodically exported from AntennaPod: [[file:antennapod-feeds.html][html]] / [[file:antennapod-feeds.opml][opml]]

I think it makes more sense to recommend individual episodes, rather than particular series.  Perhaps see the backlinks in [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]] for that.  But regardless:

+ [[id:76505a4c-a0cb-4604-9154-e2a85f578823][Novara Media]]
+ [[id:2d9e72be-7f43-4aa7-81ec-46bc4f117875][Tech Won't Save Us]]
+ [[id:6dd39bf5-e60f-455c-a8a9-f58c63011f02][Revolutionary Left Radio]]
+ [[id:ee39ec72-1008-479a-bc90-6eb21d492370][General Intellect Unit]]
+ [[id:dd1a4c20-5762-4bd5-926e-bd7177801681][Philosophize This]]
+ The Partially Examined Life
+ [[id:bb2beb1f-e125-4fa6-97b5-0909a0effd46][Reasons to be Cheerful]]
+ [[id:d5d0ab90-54d9-412b-bc7f-38717a3bee50][Looks Like New]]
+ [[id:cf597fd6-fcd7-404c-b39f-4f86f2ed8f89][The Fire These Times]]
+ The Restart Project Podcast
+ [[id:d5682dcd-68e5-4da6-b260-87382b960781][Frontiers of Commoning]]
