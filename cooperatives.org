:PROPERTIES:
:ID:       ca6a27d0-3025-4fe1-a72a-264bf9587ffa
:ROAM_ALIASES: Co-ops
:mtime:    20221216152255 20221203173949 20211127120947 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Cooperatives

I'm very much into the idea of cooperatives.  I don't have any experience of working in one myself (yet), but have worked with some, and try to buy goods/services from them when I can.

#+begin_quote
A co-operative is an autonomous association of persons united voluntarily to meet their common economic, social, and cultural needs and aspirations through a jointly-owned and democratically-controlled enterprise.

-- [[https://www.ica.coop/en/cooperatives/cooperative-identity][Cooperative identity, values & principles | ICA]]
#+end_quote

#+begin_quote
A cooperative is an association chartered to provide a product or service, much like any other company. What sets cooperatives apart from more conventional enterprises is that they are entirely owned by the people who work for them—their members—and that their policies and growth strategies are steered by the membership itself, via some kind of democratic decision-making process. All the benefits and rights attendant on the ownership of capital are distributed equally within the organization.

-- [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]]
#+end_quote

* [[id:59c248eb-876c-4ce0-ad05-bb5a933dd9cf][Cooperative values]]

* [[id:232f76b5-2ae9-4c5a-b9d5-8aea675f1777][Coops and commoning]]
* [[id:0d541fd6-b5fa-49c7-9d52-e0da9edc171e][Coops and socialism]]
* Are they a challenge to capitalism?
  
#+begin_quote
…if cooperatives are islands in a sea of capitalism, we need better catamarans, bridges, and data lines to connect them to each other and to other transformative economies.

-- [[http://disco.coop/manifesto/][Manifesto – DisCO.coop]] 
#+end_quote

#+begin_quote
I think the same could be said about [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][free software]]. Both coops and free software are non capitalist, but in and of themselves aren't a strategy to end capitalism or even a real threat to the capitalist system. In the worst vein, they provide a strategy for a privileged few to avoid some exploitative aspects of capitalism. There is enormous potential to organize within these movements to actively struggle against capitalism. But I'm not sure how to do this.

--  [[https://social.coop/@jamiem/105650661094861507][@jamiem@social.coop]] 
#+end_quote

#+begin_quote
Though participants are often drawn to the cooperative movement by a concern for basic tenets of fairness and economic justice, there’s nothing in it that conflicts with private ownership, property rights or the market as a mechanism of resource allocation.

-- [[id:7b7e27e7-5902-4ad5-a426-d58449aa8abf][Radical Technologies]]
#+end_quote


#+begin_quote
Co-operatives should also strive not to silo themselves off from broader social struggle. Once in competition with other firms, there is a tendency to pursue goals that would advance the interests of the co-operative and to become detached from social issues. The platform co-operative movement works best alongside other forms of activism and political change. This would involve working through trade unions, municipal associations and political parties

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
change in ownership model is no guarantee that other forms of social power – from racism to sexism and ableism – will not continue to be reproduced within the space

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
Are co-operatives just a better business model or do they stand for an alternative way of organising the economy?

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
As Rosa Luxemburg saw, the issue with workers’ co-operatives is that they are a ‘hybrid form in the midst of capitalism’.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
This is why platform co-operativism cannot lose sight of its broader political struggle against capitalism and remain content to build small co-operative islands in a sea of corporate giants. If platform co-operativism is reduced to simply a better way of doing business for members it loses its emancipatory potential.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

* [[id:21f4ca5f-b0b2-4b3c-86cb-da206ab7ee08][Worker cooperatives]] 
* [[id:3bca3628-65e4-4436-98a8-56574054c555][Open cooperativism]]
* [[id:990186cd-5768-4371-9f1b-35a0c8cde353][Open-value cooperatives]]
* [[id:7f8ebfb1-3845-4b11-9aa1-5f6bf40b019b][Distributed Cooperative Organisations]]
* Copper lane cohousing

Novara Media https://youtu.be/yjbtCVTWCas​

#+begin_quote
Cooperatives formalize the practice of commoning and facilitate legally regulated operations in the marketplace.

-- [[id:130c45dd-dbec-4cba-b3af-3a426aedf3a7][The DisCO Elements]]
#+end_quote

#+begin_quote
Cooperatives worldwide have a combined turnover of US$3 trillion, which is similar to the aggregate market capitalization of Silicon Valley’s greatest players (Microsoft, Amazon, Google, Apple and Facebook).

-- [[id:130c45dd-dbec-4cba-b3af-3a426aedf3a7][The DisCO Elements]]
#+end_quote

* Links

  - [[https://www.nytimes.com/2020/12/29/business/cooperatives-basque-spain-economy.html][Co-ops in Spain’s Basque Region Soften Capitalism’s Rough Edges - The New Yor...]] 
  - [[https://github.com/hng/tech-coops/][GitHub - hng/tech-coops: A list of tech coops and resources concerning tech c...]]
  - [[https://ioo.coop/directory/][#PlatformCoop Directory – The Internet of Ownership]] 
- [[https://vidi.noodlemaps.net/watch?v=90FL_bBE4mw][What is a Co-operative? - Invidious]] 
