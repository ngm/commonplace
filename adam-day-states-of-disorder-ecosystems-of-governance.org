:PROPERTIES:
:ID:       1efc20b8-3607-433b-84ce-1ae16b7608a8
:mtime:    20220628215023 20220626202952 20220625215715
:ctime:    20220625215715
:END:
#+TITLE: Adam Day, States of Disorder, Ecosystems of Governance
#+CREATED: [2022-06-25 Sat]
#+LAST_MODIFIED: [2022-06-28 Tue 21:50]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://newbooksnetwork.com/states-of-disorder-ecosystems-of-governance

"Complexity Theory Applied to UN Statebuilding in the DRC and South Sudan"

Adam Day.

This is a very interesting discussion.  The book critiques the UN's traditional approach of 'state-building', i.e. treating what it deems 'failed states' as simple machines where you can simply remove the bad part and replace it with a good part and all will be well.  The author proposes treating them as complex systems, and ultimately as sites of self-governance, not world-building exercises from the outside.

[[id:1851e5d6-ad79-4b1e-a1e5-7e7026d1a4e9][complexity science]].  [[id:681c73ab-25b2-4935-9d23-78807c35a5fe][Democratic Republic of Congo]].  [[id:6d0f4a62-ef16-4d19-8af0-308a62a5c64e][South Sudan]]

[[id:d289bb52-ba89-4fd1-9913-aa8b993d143c][There is no such thing as an ungoverned space]]. Where there are people, there is governance. It just may not be in a form that appeals to the sensibilities of Western liberals.

~00:04:15  Gives a nice succinct description of [[id:c62c4292-85c2-4f77-8af2-5e56c360e8dc][complicated vs complex]] systems.

#+begin_quote
Pursuing a complexity-driven approach instead helps to avoid unintentional consequences, identifies meaningful points of leverage, and opens the possibility of transforming societies from within.
#+end_quote
