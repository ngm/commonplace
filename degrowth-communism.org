:PROPERTIES:
:ID:       a435c947-9263-444c-8714-b60eea7a068b
:mtime:    20230922104632 20230918182057
:ctime:    20230918182057
:END:
#+TITLE: Degrowth communism
#+CREATED: [2023-09-18 Mon]
#+LAST_MODIFIED: [2023-09-22 Fri 10:46]

[[id:ef443efc-54ed-44b9-8f65-4ecc8e0944a2][Kohei Saito]].

[[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][Degrowth]] [[id:9c3f0897-c60c-44a8-9714-943a2987ab23][Communism]].

Saito says something like: degrowth needs communism, communism needs degrowth.

#+begin_quote
So when I talk about degrowth communism, this is actually coming from Karl Marx. So I even argue that Marx became a degrowth socialist, a degrowth communist in the end of his life.

-- [[id:c6f7e802-f4d6-4bad-8fd4-4baab7350fdb][Kohei Saito on Degrowth Communism]]
#+end_quote
