:PROPERTIES:
:ID:       b5d620de-7468-42a1-a128-3e5678f65fe6
:mtime:    20221119120428 20220212154950
:ctime:    20220212154950
:END:
#+TITLE: B4RN
#+CREATED: [2022-02-12 Sat]
#+LAST_MODIFIED: [2022-11-19 Sat 12:05]

"Broadband for the Rural North"

+ URL :: https://b4rn.org.uk

A [[id:c310d42a-33f4-41b4-9d66-bbc3f839f148][community benefit society]] delivering fibre broadband to rural areas in the North of England.
