:PROPERTIES:
:ID:       f0da7c18-306a-4654-8aa2-1e4dd827050c
:mtime:    20220719220313
:ctime:    20220719220313
:END:
#+TITLE: Revisiting the Calculation Debate: A Call for a Multiscale Approach
#+CREATED: [2022-07-19 Tue]
#+LAST_MODIFIED: [2022-07-19 Tue 22:05]

+ URL ::
https://www.tandfonline.com/doi/full/10.1080/08935696.2022.2051374?scroll=top&needAccess=true

I'm interested in wha the multiscale approach is.

#+begin_quote
because local and macro initiatives are equally important and feed one another positively, advocates for adopting a multiscale approach.
#+end_quote

#+begin_quote
critically reviewing discussions of an [[id:50b848e1-c617-45ca-a876-cafba6943479][ecosocialist]] society and its economic organization through revisiting the “[[id:4ed9e584-c86c-440d-9d10-35d52ea3a2d8][calculation debate]]”
#+end_quote

#+begin_quote
It underlines the importance of repoliticizing the economic sphere to reembed the economy in society rather than society being subordinated to the economy.
#+end_quote

#+begin_quote
To that aim, it emphasizes the gravity of (1) planning our future, (2) coordinating our decisions before we embark on any action, (3) relying on knowledge in different forms and formats as articulated at both the individual and societal level, and (4) generating real democracy via participatory and deliberative mechanisms. 
#+end_quote
