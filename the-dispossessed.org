:PROPERTIES:
:ID:       73f4c8e5-1e5a-4934-9d08-6847d8a9e87d
:mtime:    20220531211413 20211127120855 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: The Dispossessed

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Author :: [[id:ef6ef327-9ecf-4040-930a-b54375f4aa08][Ursula K. Le Guin]]

#+begin_quote
Ursula Le Guin’s best-known utopian novel [...] which examines [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]], [[id:0818d39d-d31e-476c-a748-a1a4186cf67d][state socialism]], and [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]] in the planetary system of [[id:9cf2c4dc-b854-4f39-8e3e-700a45be910e][Tau Ceti]]

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

I *really* liked The Dispossessed. 

Here's what I wrote when I read it ([[https://doubleloop.net/2017/11/22/dispossessed-ursula-k-le-guin/][originally here]]).

----

I read The Dispossessed by Ursula K. Le Guin recently. It’s quite a few weeks since I finished it, so my recollections are now a little hazy, but I wanted to take the time to write something about it, as it was very good.

It was indeed a great book. Beautifully written. The story revolves around the life of [[id:b4f0fb3f-9ce5-40f3-b32c-f6406849e686][Shevek]], an inhabitant of the world of Anarres. The central premise is that Anarres is a world where anarchism is the predominant political system, founded by individuals who splintered away from the neighbouring world of Urras many years ago to start a different society. The life and travels of Shevek serve as the vessel for contrasting full-blown [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]] with full-blown [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]], as he visits and explores the country of A-Io on Urras. A-Io is patriarchy and individualism dialled up to 11. The book provides many moments of point and counterpoint on the merits and dismerits of individualism and communalism when both go to their extremes.

My understanding is that Le Guin was herself an anarchist, or sympathetic to anarchist tendencies, but I think she is not overly biased in her presentation. The excesses of A-Io are pretty bad, but not so far removed from where western society is heading, to be fair. On the spectrum of anarchism, of which I’m by no means an expert, I would guess that Le Guin is somewhere towards the communalist end of it; she certainly is not an anarcho-capitalist, at any rate. Anarres is very much a world of shared dormitories, shared labour and shared canteens.

I recently listened to [[id:e40bd2ce-c91b-4b8c-abac-8d23613931ca][Carne Ross]] discuss his views on anarchism, and he said that he couldn’t provide a picture of what an anarchist society might look like, just the things we should do to live in an anarchistic way, to remove boundaries and hierarchies. I think The Dispossessed gives a good thought experiment of one way that an anarchist society might play out. One thing though, the society on Anarres starts from quite a disadvantage, as it’s a remarkably barren planet – moonlike in geography. Whereas Urras is green and plentiful, much like Earth. It would be interesting to rerun the thought experiment of The Dispossessed with the anarchist society on a bountiful planet. (Perhaps Le Guin was suggesting that such bounty leads to coveting of it? Shevek certainly starts to enjoy his verdant surroundings. Maybe it’s easier to share and be fraternal when there’s nothing special and little to go around?)

One thing I liked is how it shows that when we grow up in a particular system, our minds can become attuned to that way of thinking, to the extent that it becomes very difficult to imagine a different system. There’s a few occasions in the book where Shevek and friends struggle to comprehend the behaviour of the capitalist system on A-Io. As children they are quite bemused (and their teacher quite disgusted) by the concept of [[id:98779669-2c6b-4c90-bf43-7b86bea2468d][prisons]] and emprisonment. I found this fascinating. I think I’m on the liberal side of things, and think that prison should be rehabilitation not punishment, etc, but the idea of no prisons at all was certainly outside of my bubble. I intuitively resisted, ‘but surely there has to be some way of dealing with those who transgress’. As a product of the current system I’m entrenched in it. It’s fascinating to be presented with completely different alternatives.

Likewise there’s parts in the book where I found the anarchist society felt quite alien. Not without some logic to it, but triggering an automatic reflex of ‘hmmm that feels a bit odd’. Like the heavily [[id:0c636db6-2593-4bb1-be4e-95ca2f2483e8][communal living]], or like every child being allocated a name by a computer, or the loosening of family relations.

I think one of my big take aways from the book is how much we are products of our  environment. Even those of us who might think we eschew the excesses of the modern individualist society, there’s many ways we’re still fully wrapped up in it. And perhaps ultimately there is no perfect system. Every one will always have cons to go with the pros.

Overall The Dispossessed presents a fascinating portrait of what an anarchist society might look like, with the excesses of capitalist individualism removed and replaced with a more egalitarian way of life. But it doesn’t shy away from outlining some of the changes and sacrifices that might be needed to live that way, and the loss of some of the perks of individualism.  For no small reason is the subtitle of the book “An Ambiguous [[id:24708b10-bb07-4045-8b11-778982d73927][Utopia]].”

Finished: November 2017 sometime.

