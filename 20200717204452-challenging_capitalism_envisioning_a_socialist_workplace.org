:PROPERTIES:
:ID:       a31e4cbd-ff4e-4d85-8900-a6a8f9cd6725
:mtime:    20211127120922 20210724222235
:ctime:    20200717204452
:END:
#+title: Challenging Capitalism: Envisioning a Socialist Workplace

Great episode of Rev Left on what it would mean and what benefits it would bring to have [[id:4cee5351-de5b-47bf-beff-2b327129a633][socialist workplaces]].

- https://revolutionaryleftradio.libsyn.com/socialist-workplace
