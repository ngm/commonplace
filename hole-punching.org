:PROPERTIES:
:ID:       306e8228-516a-405c-aa86-d0e08228ea79
:mtime:    20211127120820 20210724222235
:ctime:    20210724222235
:END:
#+title: Hole punching
#+CREATED: [2021-04-03 Sat 14:40]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
A number of techniques exist for ‘hole-punching’ [[id:69c67421-30cd-4727-9343-71d0135a9751][network address tables]], in order to allow connections to be made between two computers, neither of which are directly connected to the internet. These generally rely on having some sort of ‘rendez-vous server’ used in order for the NAT to allow the connection. That is to say, the technique only works with ‘help’ from a server which is not behind a NAT, which means that although the server’s role is minimal, it does not give us complete independence. 

-- [[id:d5d03a8b-5788-494a-8781-473b10babeaf][Seeding the Wild]]
#+end_quote
