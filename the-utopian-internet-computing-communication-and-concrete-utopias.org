:PROPERTIES:
:ID:       9b454afc-f3f3-4a4a-ada4-2abd6638e17f
:mtime:    20221010203249 20221009141427 20221009124136
:ctime:    20221009124136
:END:
#+TITLE: The Utopian Internet, Computing, Communication, and Concrete Utopias
#+CREATED: [2022-10-09 Sun]
#+LAST_MODIFIED: [2022-10-10 Mon 20:36]

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]]
+ URL :: https://www.triple-c.at/index.php/tripleC/article/view/1143
+ Author :: [[id:73e5fb02-42dd-4b71-99fa-bd3e1f7eb7ee][Christian Fuchs]]
+ Subtitle: Reading William Morris, Peter Kropotkin, Ursula K. Le Guin, and P.M. in the Light of Digital Socialism

[[id:450a654c-4edc-4c0c-90ea-f26999820dfd][Digital socialism]].

#+begin_quote
What can we learn from literary communist utopias for the creation and organisation of communicative and digital socialist society and a utopian Internet?
#+end_quote

#+begin_quote
The paper recommends features of concrete utopian-communist stories that can inspire contemporary political imagination and socialist consciousness.
#+end_quote

#+begin_quote
To provide an answer to this question, the article discusses aspects of technology and communication in utopian-communist writings and reads these literary works in the light of questions concerning digital technologies and 21st-century communication. The selected authors have written some of the most influential literary communist utopias. The utopias presented by these authors are the focus of the reading presented in this paper: William Morris’s [[id:95a17b04-906c-4dd1-8f62-75d731206aec][News from Nowhere]], Peter Kropotkin’s [[id:6dda1739-e32e-47b2-93ea-59b7f66f8bf6][The Conquest of Bread]], Ursula K. Le Guin’s [[id:73f4c8e5-1e5a-4934-9d08-6847d8a9e87d][The Dispossessed]], and P.M.’s [[id:bef17ef3-0b14-46a1-ada7-5325221baed6][bolo'bolo]] and [[id:0270a4f7-080a-490b-a689-d670ac87715a][Kartoffeln und Computer]] (Potatoes and Computers). These works are the focus of the reading presented in this paper and are read in respect to three themes: general communism, technology and production, communication and culture. ... The themes explored include the role of post-scarcity, decentralised computerised planning, wealth and luxury for all, beauty, creativity, education, democracy, the public sphere, everyday life, transportation, dirt, robots, automation, and communist means of communication (such as the “[[id:4989b556-1c27-4388-b989-8311b12c1fba][ansible]]”) in digital communism. The paper develops a communist allocation algorithm needed in a communist economy for the allocation of goods based on the decentralised satisfaction of needs. Such needs-satisfaction does not require any market. It is argued that socialism/communism is not just a post-scarcity society but also a post-market and post-exchange society.
#+end_quote
