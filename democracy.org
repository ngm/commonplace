:PROPERTIES:
:ID:       f3be050a-7f71-4040-a6b0-12ca4447efe1
:mtime:    20220123144151 20211127121012 20210724222235
:ctime:    20210724222235
:END:
#+title: Democracy
#+CREATED: [2021-04-18 Sun 15:16]
#+LAST_MODIFIED: [2022-01-23 Sun 14:41]

+ Division of powers
+ Respect for peaceful protest
+ An independent judiciary
+ Building cross-party consensus
+ Winning arguments
+ Being held to account
