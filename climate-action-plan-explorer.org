:PROPERTIES:
:ID:       0bf30a2e-1bde-4ffa-acca-cbc1921b085e
:mtime:    20220419221329
:ctime:    20220419221329
:END:
#+TITLE: Climate Action Plan Explorer
#+CREATED: [2022-04-19 Tue]
#+LAST_MODIFIED: [2022-04-19 Tue 22:27]

+ URL :: https://data.climateemergency.uk

#+begin_quote
A fully searchable database of UK local authority climate action plans, climate targets, and climate emergency declarations.
#+end_quote

Really excellent service for finding out what kind of [[id:486bda09-29ad-4b87-99a7-9878ac00ec1a][Climate Action Plan]]s different UK councils have.

By [[id:26a9da8f-b5a3-46f8-a0c2-f636f88e6c83][mySociety]] and [[id:664160bc-5702-40eb-a222-4e1ae907ceb4][Climate Emergency UK]].
