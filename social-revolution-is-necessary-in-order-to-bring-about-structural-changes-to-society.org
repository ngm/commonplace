:PROPERTIES:
:ID:       0399477c-a9ed-43fb-bce8-47e16904618f
:mtime:    20220226095430
:ctime:    20220226095430
:END:
#+TITLE: Social revolution is necessary in order to bring about structural changes to society
#+CREATED: [2022-02-26 Sat]
#+LAST_MODIFIED: [2022-02-26 Sat 09:58]

[[id:a27cc7b2-38fe-47ee-8549-fb76e73f584c][Social revolution]] is necessary in order to bring about structural changes to society.

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:353d8e39-495c-4be4-a1ba-38419377555f][Signs point to yes]]

