:PROPERTIES:
:ID:       76ae14f0-b901-4f7f-86df-d4ae8de54a09
:mtime:    20220205082733
:ctime:    20220205082733
:END:
#+TITLE: Seeing Like a State
#+CREATED: [2022-02-05 Sat]
#+LAST_MODIFIED: [2022-02-05 Sat 08:35]

+ URL :: https://theanarchistlibrary.org/library/james-c-scott-seeing-like-a-state

* Articles

+ [[https://www.ribbonfarm.com/2010/07/26/a-big-little-idea-called-legibility/][A Big Little Idea Called Legibility]]
