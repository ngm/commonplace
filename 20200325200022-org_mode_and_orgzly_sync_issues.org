:PROPERTIES:
:ID:       9bae8df3-73f4-4061-8b50-b662724ef39e
:mtime:    20211127120917 20210724222235
:ctime:    20200325200022
:END:
#+TITLE: org-mode and orgzly sync issues
  
I sync my [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org]] files between [[id:aad51368-24a7-460e-8944-255958dcf67f][Emacs]] on my desktop and [[id:22b57ec1-2c19-4628-a651-893d1034696b][orgzly]] on my phone using [[id:7a9ceae8-df63-43a3-b72b-22182471dc0a][syncthing]].

At the moment I'm finding they get out of sync reasonably frequently, and you get an error in orgzly saying 'Local and remote notebooks are different'.

My steps to resolve are:

- on desktop, copy `_GTD.org` to `_GTD.org.desktop`
- in orgzly, from the Notebooks view, choose to override remote notebook
- that means syncthing syncs `_GTD.org` back on to the deskop
- then in Emacs, do an ediff of `_GTD.org` and `_GTD.org.desktop` and pick out the bits you want from each
- things will sync OK from there
