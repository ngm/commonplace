:PROPERTIES:
:ID:       71e92b34-5da0-4dab-a42e-e2aeda4ed938
:mtime:    20240326101237
:ctime:    20240326101237
:END:
#+TITLE: Using org-mode for timeblocking
#+CREATED: [2024-03-26 Tue]
#+LAST_MODIFIED: [2024-03-26 Tue 10:20]

I used to use [[id:bc8c8a61-9693-40ef-b3e0-79605e61b350][Goalist]] for [[id:7a1cdf27-db16-4c2d-a47b-4b678b371c5f][timeblocking]] / [[id:5526e322-c7ab-4418-b437-dd03b97f980b][hyperscheduling]].

There's lot of unfortunate '10x your productivity' articles around the practice, but I just found it very useful to keep on top of things and for my peace of mind.

Goalist was really great in terms of functionality, but I got annoyed partly that it was proprietary, but mostly that I couldn't sync it and make use of it anywhere else other than on my phone.

So - I'm now trying to use org-mode for timeblocking.  I've had one attempt at this before, half-building my own Quasar app that works off an org-mode file (https://gitlab.com/ngm/org-day-plan).  Never finished it though.

Recently I'm [[id:d05d27c4-5577-438b-a208-3a600a56231c][trying out org-timeblock]] and [[id:456db841-dce0-4f2b-80d9-a53e4629cc6a][trying out calfw-blocks]].  There's also org-hyperscheduler which I might look at if the others don't pan out.

To be continued.
