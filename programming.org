:PROPERTIES:
:ID:       a435a914-ad10-4466-bc04-0cf40e3e5fce
:ROAM_ALIASES: Coding
:mtime:    20211127120923 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Programming

* Timeline

** Early stuff
- I have early memories coding with my Mum and my Grandad
- typing in BASIC to the [[id:5d870662-8083-4ee4-bb90-420c9ca38573][Amstrad]]
- making the screen background flash different colours
- making it print out "Hello, <name>" over and over
- these were from the instruction manual I'm pretty sure

#+DOWNLOADED: https://s14-eu5.startpage.com/cgi-bin/serveimage?url=http:%2F%2Fwww.gamebytes.co.uk%2Fuserdata%2FPRODPIC-4094.jpg&sp=bcb2021dd94a2136acbd17dc30a3eaea @ 2020-03-21 22:11:58
#+ATTR_HTML: :width 100%
[[file:2020-03-21_22-11-58_serveimage.jpg]]

- also I think we got these other books full of code from somewhere, where you just went through and typed a full program in line by line.  Often it didn't work, you had to go through line by line and find the error.
- I also remember making a program with my Grandad on his business Amstrad PC.  It was something to do with the Blitz, a game of dropping bombs on a city.

** College
   
- I learned Pascal at college in my Computing A-level

** The web
 
- I remember teaching myself HTML around late 1990s it must've been, from [[https://en.wikipedia.org/wiki/Webmonkey][Webmonkey]].  I was listening to [[id:67b8de57-d5f4-4c19-929e-7981d2db0f03][Tri Repetae]] by [[id:783755cc-4a51-48dc-ba0a-29d0020d19ea][Autechre]] a lot while doing it.  Fun times.


** Now

 I'd like to learn [[id:ffbc57b7-f07c-452a-b35f-e0ab2fc396cc][Lisp]].  We were doing a group learning via [[id:38e63831-9458-4772-87ca-33f5734c4dd9][SICP]] for a while at https://evalapply.space, but that has gone dormant for now.
 
Perhaps [[id:1535dba7-ce45-4a3a-a8bb-e0853353ad60][Clojure]].  
