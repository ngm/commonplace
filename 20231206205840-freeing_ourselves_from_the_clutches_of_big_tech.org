:PROPERTIES:
:ID:       8b662f9b-8e8c-4027-9edc-de5564a9a746
:END:
#+title: Freeing Ourselves From The Clutches Of Big Tech

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]]
+ Found at :: https://www.noemamag.com/freeing-ourselves-from-the-clutches-of-big-tech/
+ Written by :: [[id:2449228e-60cf-4c50-9948-91cb0595991b][Cory Doctorow]]
