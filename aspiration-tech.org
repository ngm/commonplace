:PROPERTIES:
:ID:       c4649644-295a-4b68-8a2b-228fc13869fd
:mtime:    20220311175704
:ctime:    20220311175704
:END:
#+TITLE: Aspiration Tech
#+CREATED: [2022-03-11 Fri]
#+LAST_MODIFIED: [2022-03-11 Fri 17:57]

+ URL :: https://aspirationtech.org
  
#+begin_quote
Aspiration's mission is to connect nonprofit organizations, free and open source projects, philanthropic funders and activists with strategic frameworks, technology solutions, digital practices and data skills that help them more fully realize their missions.
#+end_quote
