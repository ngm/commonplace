:PROPERTIES:
:ID:       979a9310-fb5e-4c7d-a573-c12862e36128
:mtime:    20220610112327
:ctime:    20220610112327
:END:
#+TITLE: PhotoPrism
#+CREATED: [2022-06-10 Fri]
#+LAST_MODIFIED: [2022-06-10 Fri 11:27]

+ URL :: https://photoprism.app/

#+begin_quote
PhotoPrism® is an AI-Powered Photos App for the Decentralized Web.

It makes use of the latest technologies to tag and find pictures automatically without getting in your way. You can run it at home, on a private server, or in the cloud.  
#+end_quote

I'd like to get it set up on YunoHost.

See:

- https://forum.yunohost.org/t/photoprism-as-google-photos-replacement-my-experience/13510
- https://blog.arkadi.one/p/photoprism-for-self-hosted-photo-management/
- https://github.com/YunoHost-Apps/photoprism_ynh
