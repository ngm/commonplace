:PROPERTIES:
:ID:       d30aa584-2ccd-4d15-869a-6e40bc60b87e
:mtime:    20211127120953 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: The Three Body Problem

- by Liu Cixin
- https://en.wikipedia.org/wiki/The_Three-Body_Problem_(novel)
 
sort of *SPOILER ALERTS!*

A couple chapters in and enjoying this so far.  It's quite compelling.  I don't know much about the [[id:38bd0e99-dc64-4ea2-8cda-e373fe91ef70][Cultural Revolution]].  This paints it in a pretty bad light - lots of brainwashed anti-science, and the communist characters are all pretty horrible thus far.

Started losing me a little bit of the way in - not something I can't wait to pick up each night - but it's still good.  The premise of the laws of physics not being constant is really interesting, and a fun way of exploring it through virtual reality.  A few snippets of history of communist China and the [[id:38bd0e99-dc64-4ea2-8cda-e373fe91ef70][Cultural Revolution]] is interesting, too.

Getting towards the end of this now.  The plot is a bit all over the place.  Like, what happened to that countdown?  Maybe it'll make a comeback I guess.  The idea of someone being so disillusioned with humanity that they invite its destruction is kind of interesting.

The actual three-body problem itself is interesting, too.  That you can't predict the positioning of three bodies at some point in the future, as the system is chaotic.

The ideas are interesting, but the trajectory of the plot is a bit chaotic itself.  There are two more books in the series - I don't think I'll be *rushing* to read them, but might at some point.

Finished: April 2020.
