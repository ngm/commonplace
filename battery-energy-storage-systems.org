:PROPERTIES:
:ID:       c7d9d1d0-8cb1-43e8-9df1-d51272f337f5
:mtime:    20221206212425 20221203163356
:ctime:    20221203163356
:END:
#+TITLE: Battery energy storage systems
#+CREATED: [2022-12-03 Sat]
#+LAST_MODIFIED: [2022-12-06 Tue 21:24]

#+begin_quote
Battery energy storage systems hold electricity generated from renewable sources such as wind turbines and solar farms before releasing it at times of high customer demand.

-- [[id:43f9dac0-e273-4f12-8bfc-ae746ac098aa][Cottingham: Europe's biggest battery storage system switched on]]
#+end_quote

