:PROPERTIES:
:ID:       73c50c95-37c4-4221-92b5-3cbcfcf885cf
:mtime:    20211127120855 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-06-03

* ReclaimFutures conference
  
Call for participants for [[https://www.reclaimfutures.org/][ReclaimFutures]] is running up until 14th June.
  
#+begin_quote
ReclaimFutures is a technology and culture conference around the broad subjects of *post-capitalist desire, utopian exploration, ecology and alternative computing*.
#+end_quote

Submissions encouraged from underrepresented groups.

#+begin_quote
RF is open to all individuals or non-commercial small groups to participate by presenting talks, artworks, workshops and performances and strongly encourages submissions from anyone who considers themselves to belong to an underrepresented group including but not limited to POCs, LGBTQIA+, non-binary folx, womxn, people with disabilities, working class, persons with no fixed address, immigrants, the undocumented and first-time speakers.
#+end_quote
