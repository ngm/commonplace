:PROPERTIES:
:ID:       2a4b9236-04dc-4361-b4f9-2d331cb60adf
:mtime:    20220816143533 20220815133416 20220809191744 20220122155657
:ctime:    20220122155657
:END:
#+TITLE: Privatisation
#+CREATED: [2022-01-22 Sat]
#+LAST_MODIFIED: [2022-08-16 Tue 14:37]

"Selling state-owned businesses to private investors."

* Why?

#+begin_quote
the same nonsensical line that we constantly hear about all privatisations—it will bring better management and financial investment. As is always the case, the opposite turned out to be true.

-- [[id:7da061b8-a9b0-4cd2-9389-b6b1750e5dd0][It's Time to Bring Energy into Public Ownership]]
#+end_quote
