:PROPERTIES:
:ID:       0dc24c55-4e63-41fd-aef6-1cbb39ab7c67
:mtime:    20211127120801 20210724222235
:ctime:    20200830170250
:END:
#+title: Big tech is a huge part of capitalism

[[id:4f2e9822-8e1e-4599-92eb-e9fc5e948bd5][Big tech]] is at the leading edge of contemporary [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].

#+begin_quote
The information technology sector broadly defined is now at the leading edge of the capitalist system.

-- [[https://thenextsystem.org/bdc][The British Digital Cooperative: A New Model Public Sector Institution]] 
#+end_quote

#+begin_quote
In the second quarter of 2019 the top five firms in the world by market capitalisation were Microsoft, Apple, Amazon, Alphabet and Facebook. 

-- [[https://thenextsystem.org/bdc][The British Digital Cooperative: A New Model Public Sector Institution]] 
#+end_quote
