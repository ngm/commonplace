;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((eval . (ngm/writing-mode))
              (eval . (setq org-roam-title-to-slug-function 'commonplace/slugify-title))
              (org-roam-directory . "/home/neil/commonplace/")
              (org-roam-db-location . "/home/neil/commonplace/org-roam.db"))))
