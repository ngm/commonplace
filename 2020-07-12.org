:PROPERTIES:
:ID:       c6369cb5-26bb-41b5-b326-760514dfce85
:mtime:    20211127120944 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-07-12

* Reading [[id:08160100-0655-47a4-bfa6-b4846ae64c22][The Telekommunist Manifesto]]
  
Starting to read [[id:08160100-0655-47a4-bfa6-b4846ae64c22][The Telekommunist Manifesto]]:
  
 #+begin_quote
The Manifesto covers the [[id:40cd8dcb-9f30-4011-b2b8-a3d3be94371e][political economy]] of network topologies and cultural production respectively.
#+end_quote

#+begin_quote
Based on an exploration of [[id:edafb934-0a8f-419a-b3ff-b4b6405394fd][class conflict]] in the age of international telecommunications, global migration, and the emergence of the information economy. 
#+end_quote

#+begin_quote
the work of Telekommunisten is very much rooted in the [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][free software]] and [[id:821b2786-7847-4bff-a10d-f3272c332148][Free culture]] communities. 
#+end_quote

#+begin_quote
This text is particularly addressed to politically motivated artists, hackers and activists
#+end_quote

^ I'm sure it will have its flaws, but can't deny that it sounds pretty up my street.
