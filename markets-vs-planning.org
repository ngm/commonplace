:PROPERTIES:
:ID:       c6aaf865-bb7e-4930-94dc-1195e6e2b39a
:mtime:    20220604163421 20220318105951
:ctime:    20220318105951
:END:
#+TITLE: Markets vs planning
#+CREATED: [2022-03-18 Fri]
#+LAST_MODIFIED: [2022-06-04 Sat 16:36]

#+begin_quote
Schoolchildren in the US are commonly taught to conceive of the broad variety of political-economic systems, both those extant and those possible, as divisible into two essential and opposing categories: “[[id:8f4fed05-9449-4099-86ac-8cbe4e2585d3][markets]]” and “[[id:31c25452-2977-4054-ae2e-8bc09704a9ce][planning]].”

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
“Markets,” in this formulation, offer opportunities for commerce which make people free, while “planning” oppresses people through inefficient resource rationing.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
It is taken for granted that “markets” and “[[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]]” are synonymous; likewise “planning” and “[[id:1574c602-e123-4f31-afc4-31b92e874e49][socialism]].” The problems with this formulation are legion, but particularly egregious is its utter ahistoricity: inconveniently for the schoolteachers formulation, markets predate capitalism by thousands of years.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

See also [[id:4ed9e584-c86c-440d-9d10-35d52ea3a2d8][socialist calculation debate]].

I would (surprise surprise) prefer something that has a little bit of each - e.g. [[id:eae4078f-002e-402d-8fc2-ed9cef5326b5][P2P accounting]].
