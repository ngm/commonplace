:PROPERTIES:
:ID:       e73ff9d7-75c3-44b9-9ce5-4396272c0ab5
:mtime:    20211127121004 20210724222235
:ctime:    20210724222235
:END:
#+title: Magit performance on minified JS and CSS
#+CREATED: [2021-03-29 Mon 17:11]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

For a long, long time, [[file:../magit.org][Magit]] has been super slow for me when I have any minified files in the diff for staging.  It was one of those annoyances that I just kind of lived with for a while, but I revisited it today.

Someone else [[https://emacs.stackexchange.com/questions/37219/magit-hunk-with-long-lines-set-default-hidden][here]] was having the same problem.  They put it down to long lines.

There's a big discussion on that [[https://emacs.stackexchange.com/questions/598/how-do-i-prevent-extremely-long-lines-making-emacs-slow][here]].  It's been an Emacs problem for a while.

For me, the solution from there was the so-long.el package, which is included in Emacs >27.1 (Which I recently updated to - [[file:../installing-emacs-from-source.org][Installing Emacs from source]]).

After adding:

#+begin_src elisp
(global-so-long-mode 1)
#+end_src

to my init file, the problem has pretty much gone.

It's still a teeny bit slow when there's a few minified files in magit status, but way, way better now.
