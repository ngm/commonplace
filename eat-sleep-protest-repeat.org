:PROPERTIES:
:ID:       b19c9a9f-5e7e-470b-aa63-0b4f5dc5a9cf
:mtime:    20230521204509
:ctime:    20230521204509
:END:
#+TITLE: Eat Sleep Protest Repeat
#+CREATED: [2023-05-21 Sun]
#+LAST_MODIFIED: [2023-05-21 Sun 20:47]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://novaramedia.com/2023/04/14/eat-sleep-protest-repeat/
+ Series :: [[id:cc1365eb-25b7-4c4e-afed-6452000cfdee][Novara FM]]

Discussion of tactics, strategies, actions in [[id:6cc6be6a-0503-4455-b805-21dcc26ebb4f][climate action]].

[[id:76de8387-98bb-4d16-b7c2-de62e936fbcf][Extinction Rebellion]]. [[id:e4546311-4619-43b0-b504-bb55f5545877][Just Stop Oil]].  [[id:15114012-0243-42f2-9816-7cbcc463147d][Ende Gelände]].
