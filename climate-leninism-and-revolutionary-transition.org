:PROPERTIES:
:ID:       4f55d021-f092-42e1-860a-75677db167c5
:mtime:    20220918112334 20220819114227 20220818213340 20220811211000 20220811200930 20220811181104 20220731213607
:ctime:    20220731213607
:END:
#+TITLE: Climate Leninism and Revolutionary Transition
#+CREATED: [2022-07-31 Sun]
#+LAST_MODIFIED: [2022-09-18 Sun 11:23]

+ URL ::  https://spectrejournal.com/climate-leninism-and-revolutionary-transition/
+ Authors :: [[id:ddb998c8-abf9-47a0-b666-e17e441a7e6f][Kai Heron]] / [[id:86dba301-4116-4681-af37-ab7d61fe584c][Jodi Dean]]

They do a good overview of what various left climate proposals are right now.  They say specifically that a lot of them fall short of saying what we need to do in the very here and now in order to [[id:7503a87b-5490-4e1b-bf73-82f5c078a0a6][transition]] from where we are to where we need to be.  i.e. how the [[id:8e7a6330-0600-418b-8c90-457dac0a42c7][revolution]] actually comes about.  [[id:3ddc4c92-47dc-4e82-ab5b-c1bc1fa36e34][Revolutionary transition]].

[[id:9b82533b-416e-4e2a-bedf-0c67fdab160c][Climate Leninism]] is their proposal.  So then, they call for a [[id:7b9f2c10-9b27-4da1-89b6-b81cbd18ec5e][revolutionary party]].  They would quite clearly then fall into the [[id:111de04c-7bff-4034-ad09-254532b4aa60][Climate Mao]] quadrant.

Lots of good stuff but ultimately unsatisfying.  Unless I've missed it, beyond a call for a COP of anti-imperialists, I see little concrete in how to actually form this Climate Leninist party. Given that lack of immediate concreteness is their criticism of other movements, it feels lacking.  The clear statement that the climate movement needs a [[id:7b9f2c10-9b27-4da1-89b6-b81cbd18ec5e][revolutionary party]] is something you can at least sink your teeth into.  But how would you form it and what would it do?

* Highlights

#+begin_quote
We need a powerful Left capable of using state power to confront and redress the grossly and globally unequal impacts of climate change, but we don’t have time to build one.
#+end_quote

#+begin_quote
The elements necessary for a transition to a post-capitalist, communist, future are there, even in the imperialist COP26.
#+end_quote

#+begin_quote
Since the earth’s temperature is already over a centigrade above pre-industrial levels and planned cuts are insufficient to reduce carbon emissions to the needed levels, the only appropriate response is the nationalization, regulation, and prohibition of fossil fuels within a global framework wherein the imperialist countries accept responsibility for climate change and provide all the necessary financial support poor countries require
#+end_quote

- Social upheaval resulting from climate breakdown

#+begin_quote
Revolutionary social upheaval will result from the mass migration of people fleeing floods, fires, and droughts, rioting for food, shelter, and energy, and seizing what is rightfully theirs
#+end_quote

#+begin_quote
The question is the direction revolutions will take: toward the abolition of eco-apartheid and the establishment of equitable and livable societies or toward the entrenchment of authoritarianism, fascism, and neofeudalism. That this is the question makes political transition the primary issue confronting us on the Left.
#+end_quote

- [[id:1fee24cb-3418-4c4b-b69c-f83675838f05][Eco-apartheid]]

- Behaviour change

#+begin_quote
Absent changes in production and policy, efforts focused on voluntary changes to consumption will remain inadequate.
#+end_quote

#+begin_quote
It’s a fantasy to think that capitalism can manage a transition from fossil fuels to so-called “renewables” in a way that will not spell death and catastrophe for many millions of human and non-human lives
#+end_quote

Why do they keep saying 'so-called' renewables?

- [[id:59e05717-5c9d-441f-90ce-e430f9cd09eb][Green imperialism]]
- Without a revolution, best case scenario is that we will end up with [[id:65403284-7dac-40de-a041-c1b958a5596c][green capitalism]].
- [[id:38a872a6-1b07-40b6-8bdd-30cba08ee59c][Andreas Malm]]. They seem OK with Malm's critique of anarchism, and proposal of eco-Leninism, but suggest that there's no solid proposal from Malm of transition.

#+begin_quote
From our perspective, Malm’s proposal evades the problem of revolutionary transition. War communism is a plan for what comes after a revolutionary movement has taken power or after social movements have implausibly persuaded capitalist states to act via a coordinated campaign of mass civil disobedience and sabotage (as Malm’s argument in [[id:b293b39d-eda4-46c8-8bdd-1c4f1ef0c1db][How to Blow Up a Pipeline]] suggests)
#+end_quote

#+begin_quote
we need a politics that works from the material conditions of struggle that confront us, not one that takes a distance from them. We need a politics of revolutionary transition.
#+end_quote

- [[id:23524716-5d70-44c8-97f0-1d111b2ad9af][Disaster communism]].  They seem to respect, but again say is missing revolutionary transition.

#+begin_quote
Yet this familiar genuflection to the fact that revolution produces its own forms of struggle puts revolution at a distance from us, as if we were observers rather than participants in the struggles of our time.
#+end_quote

- [[id:4b4a49a4-26eb-43f0-b217-73a2bbb60266][Base-building]].  To the authors, this is more aware of the need for transition, but fuzzy as to how base-building actually makes it happen.

#+begin_quote
Base-building correctly sees the limitations of jumping over the problem of transition.
#+end_quote

#+begin_quote
However, as important as this work is, base-builders are decidedly fuzzy on the question of how meeting the immediate material needs of workers and communities within capitalism transitions into revolutionary struggle
#+end_quote

#+begin_quote
Teresa Kalisz of the now defunct Marxist Center notes that base-building as a tactic is not intrinsically revolutionary; it is a strategic task “all healthy political organizations must take on, whether they are communist, socialist, or anarchist; even liberal groups often engage in base building
#+end_quote

#+begin_quote
The problem is that “by not going beyond these tactics and connecting them to a political vision,” the Marxist left “runs the very real risk of presenting ourselves and engaging our organizing in an apolitical way.”
#+end_quote

- [[id:9b82533b-416e-4e2a-bedf-0c67fdab160c][Climate Leninism]].
#+begin_quote
The “we” necessary for an anti-imperialist approach to climate change, for a just, a communist, transition, has to be conscious of itself as a “we.”
#+end_quote

#+begin_quote
this consciousness must be linked to a shared understanding of where we are and where we need to be, and a recognition that we can only get where we need to be through organized, collective action.
#+end_quote

#+begin_quote
We come together because that’s the only way we can win. And we must win – the flourishing of people and the planet depends on our meeting the challenge of a just transition.
#+end_quote

#+begin_quote
Global climate politics faces problems of scale and coordination. The dimension of scale is easy to see: we need forms of struggle that are more than assemblies of locals and experimental communities of resistance. We need organizational approaches that operate at national and international scales, that can adopt national and international perspectives and strategies.
#+end_quote

#+begin_quote
How do we make decisions about strategies, tactics, and priorities, at a national and international scale? What assumptions guide our deliberations at these larger scales? This is where shared values and common principles matter enormously. This is where the question of our politics comes in: what is the line that we hold in common, the principles to which we are committed to fight?
#+end_quote

#+begin_quote
We all know that as the climate catastrophe intensifies, so will ethno-nationalisms. 
#+end_quote

#+begin_quote
We need now to establish an irrevocable anti-imperialist international commitment that prioritizes the regions and peoples most immediately and heavily impacted by climate change. This of course includes welcoming climate refugees and providing all necessary material and financial support for a just tradition.
#+end_quote

#+begin_quote
If climate change is not to intensify oppression and accelerate extinction, we have to build and join organizations adequate to the challenge of transitional thinking and acting.
#+end_quote

#+begin_quote
The imperative of the party form arises from an analysis of our conjuncture: how can we endure, scale, and strategize? How can we win?
#+end_quote

I prefer [[id:2e46c603-b3cf-4633-ba54-21b568ffeec9][Rodrigo Nunes]]' [[id:96a37fb7-282b-44a5-bb88-d5332d49c581][Neither Vertical Nor Horizontal]] more so than this strict adherence to the party form.

#+begin_quote
We cannot expect mass demonstrations to exert pressure sufficient to get governments to enact the changes necessary for a just transition. Demonstrations may push governments to do something, but that something will protect the property and profits of the ruling classes and promote the interests of the imperialist powers.
#+end_quote

#+begin_quote
If nothing else, Malm and the Zetkin Collective are correct when they emphasize that the coming period will be one of ever-intensifying polarization and confrontation.
#+end_quote

#+begin_quote
The fact of this conflict means we must prepare for a chaotic, uncertain, and revolutionary transition.
#+end_quote

#+begin_quote
What follows from these acts other than the immediate escalation of state violence and repression? Will citizens, observers, immediately reject the state’s use of force or will they be swayed by decades of anti-terrorist propaganda?
#+end_quote

^ re: direct action of the [[id:b293b39d-eda4-46c8-8bdd-1c4f1ef0c1db][how to blow up a pipeline]] sort.

#+begin_quote
The array of tactics familiar to movement actors – blockades, occupations, marches, rallies – becomes a means for recruiting party cadres, building coherent alliances, and weaving a red thread through the movements. 
#+end_quote

#+begin_quote
Likewise experiments in farming, urban gardening, and similar such survival oriented micro-initiatives can be expanded into the repertoire of party practices, treated as opportunities for building skills and camaraderie. In each instance, previously separate activities – a blockade here, a mutual aid arrangement there – become consciously integrated into a larger theory and plan for building the power necessary to effect a just transition.
#+end_quote

#+begin_quote
Political, economic, energy, and social transition require [[id:4d234874-5fa3-419a-ba13-6dc499b591ec][centralized planning]].
#+end_quote

#+begin_quote
A [[id:45368db7-3411-4fd1-85c5-d6f146a301bf][just transition]], one that is [[id:57f38468-73e4-4dd7-86e7-dc594cd84f7d][anti-imperialist]] and oriented toward the struggles of the oppressed, demands even more coordination and planning: we have a capitalist enemy to defeat and its [[id:82c173bb-89d5-4350-975a-6072338b5f64][hegemony]] to unravel.
#+end_quote

#+begin_quote
In sum, the party is a form for building long-term alliances and training cadre, requirements for any politics of climate change that recognizes the actuality of revolution.
#+end_quote

- . #push [[roam:2022-08-26]] review

* Top of head summaries

What do I remember about it off the top of my head, without re-reading notes?

** [2022-08-19 Fri] 
They do a really nice overview of existing leftist climate outlooks, branches of eco-socialism etc.  Andreas Malm is in there.  I would like to tease out those a bit more - that overview.  They outline what they say they see as the limitations of these.  I can't remember exactly what they are.  The authors have a strong focus on revolutionary transition - that is, what actually needs doing to kickstart the revolution.  And their suggestion for this is to have a strong revolutionary party.  A vanguard party for eco-socialism, I guess.  I'm still not clear how this achieves the transition though.  What is a revolutionary climate party actually?
