:PROPERTIES:
:ID:       c549f6c5-7dda-480f-a725-7a3514684472
:mtime:    20220326145345 20211127120943 20210724222235
:ctime:    20200507204341
:END:
#+TITLE: Use of Weapons

+ Author :: [[id:ad985a12-92cd-477e-81c8-5dda1ca11a93][Iain M. Banks]]

Reading more Iain M. Banks. Use of Weapons. It’s good. His writing style has gone from super linear and rip roaring to totally nonlinear and introspective in the space of a couple of books. I’ve enjoyed all of them so far. This one is much more comical too. The Culture is getting more morally ambiguous by the book as well, which I like. Love a bit of moral ambiguity.
