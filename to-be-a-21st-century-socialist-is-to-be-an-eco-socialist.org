:PROPERTIES:
:ID:       7ce5b8bf-a6b3-4696-81d5-eb0819ac6187
:mtime:    20231025212307 20220702133451 20220114203112
:ctime:    20220114203112
:END:
#+TITLE: To be a 21st century socialist is to be an ecosocialist
#+CREATED: [2022-01-14 Fri]
#+LAST_MODIFIED: [2023-10-25 Wed 21:23]

To be a 21st century [[id:9b1da676-96c5-4613-a12e-2cc5ec1d253e][socialist]] is to be an [[id:50b848e1-c617-45ca-a876-cafba6943479][ecosocialist]].

* Because

(presumably) 

+ Climate breakdown is the dominant issue of our times

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]
+ Source (for me) :: [[id:16cc5364-20fd-449e-b4a7-f975051fc0c2][Breht O'Shea]] [[id:7457e62f-23f1-41ad-ac05-bb763b40bbc3][Socialist States & the Environment w/ Salvatore Engel-Di Mauro]]

Probably without a doubt, just need to double-check I understand the intent here.  
