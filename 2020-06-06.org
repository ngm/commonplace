:PROPERTIES:
:ID:       02420adf-f3b6-4d5c-a5f5-5b250d81724e
:mtime:    20211127120755 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-06-06

* The alternative to the current money system
 https://www.lowimpact.org/alternative-money-system-tim-jenkin-matthew-slater-dil-green/

* Listened: [[https://revolutionaryleftradio.libsyn.com/post-structuralism-postmodernism-and-metamodernism][Revolutionary Left Radio: Post-Structuralism, Postmodernism, and... Metamodernism?]] 
  
Interesting they make a bit of a linkage here between [[id:5950aafc-ae52-453e-920e-2a0eb823ea23][post-structuralism]] and [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]] and [[id:763cb241-eb6c-4b29-945d-6e06aad17233][horizontalism]].

And explores if there's some tension between [[id:5950aafc-ae52-453e-920e-2a0eb823ea23][post-structuralism]] and [[id:dea564a6-cfcb-4937-bfdd-3d678e3e0106][Marxism]], in its presentation of a scientific approach to history.

Also makes an interesting distinction in the definition of [[id:2db89aa0-38bb-4eea-ae75-3fff2343b481][postmodernism]] - of it not so much being an ideology of some kind, and more a description of the 
state of the world.  And people like [[id:bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa][Deleuze & Guattari]], [[id:c16f89c2-7777-42f9-b70d-e69fed6df129][Baudrillard]], are describing that state of the world and what it means, but they're not "postmodernists".  I think he said that they're best described as post-structuralists.

* Listened: [[https://revolutionaryleftradio.libsyn.com/postmodernism-the-cultural-logic-of-late-capitalism][Revolutionary Left Radio: Postmodernism, or, The Cultural Logic of Late Capitalism]] 

[[id:db2f97b7-2680-446e-a226-6c4182a53e82][Fredric Jameson]]'s [[id:e6abd35c-3bfe-4c95-93ba-cd16ca82adae][Postmodernism, or, The Cultural Logic of Late Capitalism]].

[[id:d701985a-67fe-48a3-8658-578511ec4008][Capitalist Realism]] is an extension of Jameson's thesis.
