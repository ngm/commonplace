:PROPERTIES:
:ID:       dbc09ad9-2035-426f-ab40-8ad51abe3bda
:mtime:    20220602104312
:ctime:    20220602104312
:END:
#+TITLE: Market anarchism
#+CREATED: [2022-06-02 Thu]
#+LAST_MODIFIED: [2022-06-02 Thu 11:23]

#+begin_quote
market anarchism is advocacy of replacing the state with civil society while pointing to free market economics to explain the workability and/or desirability of such.

-- [[https://c4ss.org/what-is-market-anarchism][Center for a Stateless Society » What is market anarchism?]]
#+end_quote

Never quite sure what to make of it.  

#+begin_quote
Some market anarchists label their views as “[[id:f8e1e4d1-288d-443e-8534-c2449974c032][anarcho-capitalism]],” while others prefer to identify with “anti-capitalism” or “libertarian socialism.” Still others reject both the labels “capitalism” and “socialism” as too hopelessly distorted in the public consciousness to be used meaningfully in reference to what they advocate.

-- [[https://c4ss.org/are-market-anarchists-for-or-against-capitalism][Center for a Stateless Society » Are market anarchists for or against capital...]]
#+end_quote
