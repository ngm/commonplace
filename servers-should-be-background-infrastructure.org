:PROPERTIES:
:ID:       d5719694-9e43-43ab-a4c6-df858e513984
:mtime:    20211127120954 20210724222235
:ctime:    20210724222235
:END:
#+title: Servers should be background infrastructure
#+CREATED: [2021-04-17 Sat 18:38]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

I believe this.  Servers should not infer any privileges over the admins of a server.  They should just provide an extra piece of (essentially redundant) infrastructure in a network.

#+begin_quote
A big goal is to avoid leaning on servers whenever possible. Servers naturally gain a lot of importance as hosts and, like a system of government with multiple branches, we want to constrain that importance to hosting and disconnect it from content moderation. As soon as we ask the server for a list of all its communities, it becomes the server’s job to moderate that list. That makes server choice more important than it should be; they really should be background infrastructure.

-- https://ctznry.com/pfrazee@ctzn.one/ctzn.network/comment/ff080bc405a1bf50
#+end_quote

