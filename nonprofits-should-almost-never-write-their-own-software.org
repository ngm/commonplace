:PROPERTIES:
:ID: 551fffd0-ec03-4b06-aff9-1a53e6ad6389
:mtime:    20240413092615
:ctime:    20240409003547
:END:
#+TITLE: Nonprofits should (almost) never write their own software

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

From the [[id:0d5ba1ab-2007-470f-92f0-3e54357bffdf][Aspiration Manifesto]].

Being the tech lead for a non-profit that develops our own software - I fully agree. We are currently going through a process of trying to divest as much bespoke code as possible to pre-existing (FLOSS) software.

One alternative I see, where no other software exists for the desired purpose, is for non-profits to perhaps be incubators for the software, but always with an intention to [[id:6e0eb5f0-5506-4d64-a6ea-4c205f38fb3e][exit to community]] / exit the software to cooperative.
