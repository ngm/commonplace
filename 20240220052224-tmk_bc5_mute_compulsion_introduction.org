:PROPERTIES:
:ID:       af33f55c-e7dc-4e25-95a8-36a4813dc527
:END:
#+title: TMK BC5: Mute Compulsion, Introduction

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ Found at :: https://soundcloud.com/thismachinekillspod/unlocked-275-tmk-bc5-mute-compulsion-introduction
+ Part of :: [[id:f8e35c03-c235-435a-add8-04c26272ee12][This Machine Kills]]

#+begin_quote
We kick off the TMK book club with the excellent new book – [[id:cdba187a-6ed3-4e7b-beb4-bafc2f37946a][Mute Compulsion]] by [[id:bf48fb30-4f0d-4b26-bbbb-982c96faa20e][Søren Mau]] – which offers a foundational analysis of power, value, capital, and social reproduction. Mau’s book is written with a real analytical clarity that advances our critical, theoretical understanding of the relations and operations of those things in society and our lives. We set the context for the book and our approach before discussing the Introduction chapter, which established the book’s motivating questions and overarching argument about the need to pay special attention to the economic power of capital.
#+end_quote

Recognising [[id:7d5e4cd4-cf96-4ab9-aa30-24ae08f33da6][economic power]] means we can better articulate what needs to be done. Otherwise if we only see coercion we think we need only challenge the state, if we only see ideology we think we need only change our minds. But if we see economic power we challenge it and it's role in [[id:0b89dd21-ed5e-4e32-86c9-f32010507732][social reproduction]].
