:PROPERTIES:
:ID:       01713016-d00f-4a63-9215-483659e70939
:mtime:    20211127120754 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:From Apple to Anomaly

An exhibition at the Barbican.  Really good.  Visually very powerful, also thought-provoking with regard to [[id:84384c14-99db-4abb-ba9c-1a646e73bed4][algorithmic bias]].

Based around [[id:4ae973d6-bf20-4bc2-ad9f-7c9f9d8e1c55][ImageNet]], a project of 14 million photos used as a training set for machine learning.  Those 14 million photos have been categorised by people on Mechanical Turk.

The piece highlights the inherent biases in that initial categorisation.  You see 30,000 physical prints of photos arranged around certain terms, and it starts off very factual ('apple', 'soil') and moves into terms more laced with interpretation ('criminal', 'alcoholic').  

It looks fantastic.
