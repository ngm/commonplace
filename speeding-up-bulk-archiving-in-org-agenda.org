:PROPERTIES:
:ID:       431f683a-c596-4c46-ba92-be0762f8d8ea
:mtime:    20240427121729 20211127120830 20210724222235
:ctime:    20210724222235
:END:
#+title: Speeding up bulk archiving in org-agenda
#+CREATED: [2021-03-25 Thu 20:21]
#+LAST_MODIFIED: [2024-04-27 Sat 12:18]

Speeding up bulk archiving in [[id:d02dc796-2ec8-47a5-9de7-90d4cfa019e2][org-agenda]].

Marking items in bulk in the agenda, then bulk archiving them, had gotten really slow for me.  Turns out it was because my _GTD.org_archive file had gotten so big (>30,000 lines).

I copied _GTD.org_archive to _GTD.org_archive-2021-03-25, then emptied _GTD.org_archive.  Now it's nice and fast to archive in bulk again.

