:PROPERTIES:
:ID:       9b9a3c62-ace7-4def-a6c2-0df2eb0f6fbc
:mtime:    20220816121551
:ctime:    20220816121551
:END:
#+TITLE: We should have public ownership of water
#+CREATED: [2022-08-16 Tue]
#+LAST_MODIFIED: [2022-08-16 Tue 12:53]

We should have [[id:52a0b7a0-b65c-486c-8166-969d9b273279][public ownership]] of the UK [[id:a2faa857-3f94-4b51-b06e-22509fd07ee3][water industry]].

* Because

- [[id:6a7a698b-c817-4dbc-9919-1bb44fd0f0a2][Water privatisation]] has been bad for the UK water industry
  - Lack of investment in infrastructure
  - Profits over utility
- Public ownership would improve the UK water industry

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:9a13c2e4-f670-4d0d-9f15-62cfa405f37d][Yes definitely]]
