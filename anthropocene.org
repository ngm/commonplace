:PROPERTIES:
:ID:       3153be76-ec41-42ec-9d61-292dc72fa9e7
:mtime:    20220828145210 20220807162238
:ctime:    20220807162238
:END:
#+TITLE: Anthropocene
#+CREATED: [2022-08-07 Sun]
#+LAST_MODIFIED: [2022-08-28 Sun 14:56]

#+begin_quote
In addition to [[id:3dedbefd-4f37-481a-9597-9b05e84083ed][global warming]], [[id:48947b6d-cce7-49e7-9613-5373e7e48d15][biodiversity loss]], [[id:8fd07205-dcba-4b90-a7b0-9479833ede53][ocean acidification]], and land-use change through deforestation, nitrogen and phosphorus inputs into the biosphere and atmosphere have already reached or even exceeded carrying capacity limits.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

[[id:c50654a1-5098-44ab-9400-a547cf5b94c5][nitrogen and phosphorus pollution]].

#+begin_quote
For more than 200 years, the capitalist mode of production has cultivated a [[id:4f82d469-fdf9-4b85-9ad9-8267fca545b8][social metabolism]] with nature that has changed the Earth system to such an extent that at least since the great acceleration after the Second World War, the Earth has entered a new Earth-historical epoch, what Earth system scientists are calling the Anthropocene.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote
