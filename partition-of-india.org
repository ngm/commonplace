:PROPERTIES:
:ID:       b51ba8d5-3412-4601-98f1-cf78ccb2df7a
:mtime:    20220812160101 20220712212140
:ctime:    20220712212140
:END:
#+TITLE: partition of India
#+CREATED: [2022-07-12 Tue]
#+LAST_MODIFIED: [2022-08-12 Fri 16:06]

#+begin_quote
India’s bloody partition in August [[id:a62c3828-af82-4b96-a989-610bf87eadd5][1947]], [...] led to the deaths of at least 1 million Indians and the displacement of around 15 million

-- [[https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy][‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen...]]
#+end_quote

#+begin_quote
The borders for two independent states were drawn on religious lines: Hindu-majority India, and Muslim-majority West Pakistan and East Pakistan (now [[id:6f5972d8-1514-447e-87ae-ce34bdf4855a][Bangladesh]]).  In just a few months, thousands of years of cultural exchange and co-existence between India’s Hindu, Muslim and Sikh communities, nightmarishly unravelled into panic, then terror, with millions rushing for the hastily established new borders as violence erupted.

-- [[https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy][‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen...]]
#+end_quote

#+begin_quote
In what was to become the British Raj’s swan song after two centuries of colonial rule, Cyril Radcliffe, a British judge who had never visited colonial India before, was appointed in July 1947 to carve through the ancient land within weeks.

-- [[https://www.theguardian.com/world/2022/aug/11/a-sikh-soldier-pulled-me-out-of-the-rubble-survivors-recall-indias-violent-partition-and-reflect-on-its-legacy][‘A Sikh soldier pulled me out of the rubble’: survivors recall India’s violen...]]
#+end_quote

