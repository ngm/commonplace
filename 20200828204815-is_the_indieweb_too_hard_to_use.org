:PROPERTIES:
:ID:       2a8f3eb4-d387-450e-b550-83992bd0c570
:mtime:    20211127120816 20210724222235
:ctime:    20200828204815
:END:
#+title: Is the IndieWeb too hard to use?

#+begin_quote
Diversity is absolutely a problem in tech, but IndieWeb folks are, from my experience, absolutely doing what they can to rectify that; bringing in people from all sorts of backgrounds, trying to boost the minority voices, and being supportive of everyone who is trying to make the world, or at least the Internet, a better place. 
#+end_quote
  
This is a really good article by Fluffy on the state of the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] and making it more accessible for wider adoption.  Just because we're not there yet, doesn't mean that we're not trying.

https://beesbuzz.biz/blog/3876-Incremental-progress

