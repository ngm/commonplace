:PROPERTIES:
:ID:       c106204f-5bdc-4e7a-9243-a27b2f03bda5
:mtime:    20211127120940 20211103203045
:ctime:    20200503132315
:END:
#+TITLE: social reader

I use [[id:02426552-0f6a-44be-9a50-107f76200c34][Aperture]] for the backend bits, and [[id:842344f3-aa78-42ac-b2ae-9027543bb833][Indigenous]] and [[id:34c542bb-4e3b-41e8-8180-4fe5d657fe74][Together]] for the frontend bits.

* Twin pages
  
- https://indieweb.org/social_reader
