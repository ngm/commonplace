:PROPERTIES:
:ID:       af8a6f36-b061-4d11-8db6-55de810b2ebe
:mtime:    20230624090227
:ctime:    20230624090227
:END:
#+TITLE: Western Maoism
#+CREATED: [2023-06-24 Sat]
#+LAST_MODIFIED: [2023-06-24 Sat 09:09]

[[id:7cb62bd4-f694-4bfa-8d74-d8903eace888][Maoism]] in the West.

#+begin_quote
Western Maoism was not just a movement of white students, as the stereotype holds, and the example of China as a beacon of ‘Third World’ socialism held appeal to many Black and Brown radicals in Britain. Mao’s China was a key reference point for [[id:d8a72bc9-a738-421d-9f34-ea835bee709d][Claudia Jones]] and the militant [[id:4523192b-f355-45be-bb78-cd3a77678bbd][Caribbean Workers' Movement]], several British Black Power organisations, and the great [[id:93c6f51e-5bad-4641-b8b9-ea12c1e6ec34][Indian Workers Association]].

-- [[id:b7717dd4-9d86-451c-bfae-aa7df4a49c17]['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]
#+end_quote
