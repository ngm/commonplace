:PROPERTIES:
:ID:       5e14b868-7ffa-4540-9cd5-86e34d1a8c1e
:mtime:    20211127120843 20211126151424
:ctime:    20211126151424
:END:
#+TITLE: Dadaism

"magnificent madmen"

#+begin_quote
Nearly a half century ago, while Social-Democratic and Communist theoreticians babbled about a society with"work for all," the Dadaists, those magnificent madmen, demanded unemployment for everybody

-- [[id:5edca101-03d7-4ef1-a921-7b3167176c09][Towards a Liberatory Technology]]
#+end_quote

Hah [[id:cb9c581d-b6e4-4d78-bea8-2230e1a0e09d][Unemployment for all]].  An early [[id:1d0fa725-6b14-4ecb-9705-283dd56b782e][fully automated luxury communism]]?
