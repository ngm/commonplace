:PROPERTIES:
:ID:       1daf6c28-f585-497f-8d0c-b6b2b9e68750
:mtime:    20211127120809 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-07-03

* [[id:ef812d1c-1f50-45b3-b525-0cbc9f606c24][Resist the feudal internet]]
  
Via [[https://twitter.com/pfrazee][@pfrazee]]'s [[https://infocivics.com/][article]] on [[id:f27371b3-b837-49cb-a8df-30bfd9355b03][information civics]], came across this old [[http://en.collaboratory.de/w/Power_in_the_Age_of_the_Feudal_Internet][article of Bruce Schneier]]'s on what he calls the [[id:9058362a-63b6-4c0b-b438-cab30d10bde3][feudal internet]].  

In his analogy, we're the peasants who have traded in freedom for some convenience and protection.

#+begin_quote
Users pledge allegiance to more powerful companies who, in turn, promise to protect them from both sysadmin duties and security threats.
#+end_quote

He sees the two big power centres of the feudal lords as *data* and *devices*.

#+begin_quote
On the corporate side, power is consolidating around both vendor-managed user devices and large personal-data aggregators. 
#+end_quote

We no longer have control of our data:

#+begin_quote
Our e-mail, photos, calendar, address book, messages, and documents are on servers belonging to Google, Apple, Microsoft, Facebook, and so on. 
#+end_quote

I see the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]], [[id:08f20186-b4e0-4070-be89-9082b2a2b2d0][Beaker]], etc as means of resisting this.

And we're no longer in control of our devices:

#+begin_quote
And second, the rise of vendor-managed platforms means that we no longer have control of our computing devices. We’re increasingly accessing our data using iPhones, iPads, Android phones, Kindles, ChromeBooks, and so on. 
#+end_quote

I see the [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]] as a means of resisting this.  Allowing us to do what we wish with our own devices - including putting whatever software on them that we want.

One big omission from the article I find is that Schneier focuses on the disbenefits to the *users* of these devices and platforms - the [[id:917308da-99cd-4a0f-a836-e750898da789][manufactured iSlaves]], in Jack Qiu's terminology.   He doesn't mention (at least in this particular article) those exploited in the creation and upkeep of these - the [[id:b159e971-3528-4ab7-9558-6216850442b6][manufacturing iSlaves]].  That's just as big, if not bigger, a reason for challenging these power structures.
