:PROPERTIES:
:ID:       004877dd-e274-4837-b4b8-511f5c6c8dd1
:mtime:    20221010205005 20220918120134 20220902213556 20211127120754 20210724222235
:ctime:    20200717192229
:END:
#+title: Socialism: Utopian and Scientific

[[id:75982453-a0d6-460e-b0d3-a56f5970ed99][Friedrich Engels]].

A critique of [[id:72c29bc6-6095-4b77-b69e-bf038a5bbb61][utopian socialism]] in favour of [[id:5e4bc086-7fdd-4549-acf7-c7b8e4069ad0][scientific socialism]].

#+begin_quote
In Socialism: Utopian and Scientific, Engels saw the emergence of 19th-century utopian socialism, signalled by the work of Saint-Simon, Fourier and Owen, as a reaction to the defeated aspirations of the [[id:deca4bbf-e7ce-4f12-b970-e1ca3f4f8383][French Revolution]].

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

#+begin_quote
In Socialism: Utopian and Scientific, Engels (1880, 290) writes that utopian socialists are “drifting off into pure phantasies”. He argues that utopian socialism is moralistic and lacks a scientific analysis of capitalism and its contradictions: “The Socialism of earlier days certainly criticized the existing capitalistic mode of production and its consequences. But it could not explain them, and, therefore, could not get the mastery of them. It could only simply reject them as bad” (1880, 305). Engels thinks that Marx’s works, Marx’s approach of historical materialism, and the notion of surplus-value helped to turn socialism into a science:

-- [[id:9b454afc-f3f3-4a4a-ada4-2abd6638e17f][The Utopian Internet, Computing, Communication, and Concrete Utopias]]
#+end_quote

#+begin_quote
Industrial production was barely developed, and the proletariat, wrote Engels, appeared to these radicals as ‘incapable of independent political action’ – ‘an oppressed, suffering order’ which required help from outside.

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

#+begin_quote
In these conditions, the utopian socialists attempted in idealist fashion to evolve the solution to social problems ‘out of the human brain'

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

#+begin_quote
Engels argued that ‘scientific’ – that is, self-critical, rigorously conceptualized and empirically tested – socialism must be rooted in an investigation of historical development: ‘the process of evolution of humanity.’

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

#+begin_quote
Georg Lukács argues that although classical utopian-communist literature [in its] step beyond Capitalism follows fantastic paths, its critical-historical basis is nonetheless linked – especially in the case of Fourier – with a devastating critique of the contradictions of bourgeois society. In Fourier, despite the fantastic nature of his ideas about Socialism and of the ways to Socialism, the picture of Capitalism is shown with such overwhelming clarity in all its contradiction that the idea of the transitory nature of this society appears tangibly and plastically before us

-- [[id:9b454afc-f3f3-4a4a-ada4-2abd6638e17f][The Utopian Internet, Computing, Communication, and Concrete Utopias]]
#+end_quote
