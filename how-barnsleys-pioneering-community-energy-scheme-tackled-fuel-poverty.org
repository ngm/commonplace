:PROPERTIES:
:ID:       694b0ed7-6a71-41ff-8aa4-0d1d29a810ba
:mtime:    20220415193904
:ctime:    20220415193904
:END:
#+TITLE: How Barnsley's pioneering community energy scheme tackled fuel poverty
#+CREATED: [2022-04-15 Fri]
#+LAST_MODIFIED: [2022-04-15 Fri 19:41]

+ URL :: https://takeclimateaction.uk/climate-action/how-barnsleys-pioneering-community-energy-scheme-tackled-fuel-poverty

[[id:b75f6046-81b8-4b12-8e92-3988d00af257][Community energy]].  [[id:d1f5eaa0-d555-4444-8fef-34389a19eed9][Barnsley]].
  
  
