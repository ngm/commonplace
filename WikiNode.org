:PROPERTIES:
:ID:       2a635dae-f7db-40cd-ae62-2f916506b1f8
:mtime:    20211127120816 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: WikiNode

Hi - this is the WikiNode page for Neil's Noodlemaps.

Here you can find a bit of info about this site, and links to neighbouring nodes.

* Summary
  
- [[id:34eb592e-0a9b-4f37-ab9c-e37918abe1ed][About this site]]

* Neighbour Nodes

- [[http://thoughtstorms.info/view/WikiNode][PhilJones:WikiNode]]
- [[http://webseitz.fluxent.com/wiki/WikiNode][BillSeitz:WikiNode]]
