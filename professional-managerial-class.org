:PROPERTIES:
:ID:       f1e20e95-e8a3-4041-aa65-b55583092669
:mtime:    20211127121011 20211005144041
:ctime:    20211005144041
:END:
#+TITLE: Professional-managerial class
#+CREATED: [2021-10-05 Tue]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

#+begin_quote
The term professional–managerial class (PMC) refers to a social class within capitalism that, by controlling production processes through superior management skills, is neither proletarian nor bourgeois. 

-- [[https://en.wikipedia.org/wiki/Professional%E2%80%93managerial_class][Professional–managerial class - Wikipedia]] 
#+end_quote

#+begin_quote
From what I've seen from this presenter previously, I sense there's a professional appropriation of the commons ethos going on. Better architecture, more equity. Beware the professional-managerial class! It's good though, to see commoning being situated in a context of Black equity and property relations. Of course it's relevant :) Power to the commons
Just - never entirely trust a professional. *Yours, in and against the professional-managerial class* 🙄  Vernaculars of the world, unite!
 
+ https://social.coop/@mike_hales/107013946335060605
#+end_quote

I think I am also in and against the professional-managerial class.
