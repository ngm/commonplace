:PROPERTIES:
:ID:       36167d73-aef7-4de7-a43d-5c040b528ff8
:mtime:    20211127120823 20210724222235
:ctime:    20200801130605
:END:
#+title: Review: Future Histories

There's a lot to chew on in [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]]. Thematically it is right up my street, in that it is linking leftist ideas from history to modern issues around digital technology and [[id:5b0fd875-1e61-4c91-823f-ae6b8aeb13eb][technology capitalism]].  It is ultimately about how [[id:98822387-9731-4f34-bb97-5971f627b1a2][technology should be liberatory]], while warning against [[id:3071e42b-56cc-422e-b4fd-a422e09cdf8f][techno-utopianism]].

#+begin_quote
As the planet slides further toward a potential future of catastrophic climate change, and as society glorifies billionaires while billions languish in poverty, digital technology could be a tool for arresting capitalism’s death drive and radically transforming the prospects of humanity. But this requires that we politically organize to demand something different.
#+end_quote

#+ATTR_HTML: :width 50%
[[file:images/future-histories.jpg]]

[[id:d8f82c66-07e1-4947-b845-d77ed4db8643][Fanon]] and his work on [[id:f1669171-61c2-4069-9d38-c6047534a247][colonialism]] are used as a frame for [[id:62eebbf1-7f44-4be1-bdf1-ad7ec8118dbc][digital self-determination]].  The historial commons is linked to the [[id:3ff62201-7514-4b55-9325-cf70a137ae6b][digital commons]].  [[id:48bf106b-3fea-4726-bb14-ac55082cd96d][Thomas Paine]] is a jumping off point for [[id:50e8960f-1b4f-4f8d-8619-7fd64754d938][universal basic income]] and [[id:7e288212-b1fc-4116-81f2-e2e6c6c88e2b][services]].  And lots of other interesting juxtapositions.

It's full of ideas and statements that I agree with.  It's so choc full of stuff that I'm not sure that I've come away from it with a coherent idea of what is to be done - it's more of a manifesto than a handbook.  Each chapter does have broad strokes of ideas, just more long-term legislative or policy demands than immediate opportunities for praxis. But definitely good jumping off points.  For example, [[id:46ebd2a2-812f-42e5-9d3c-ebee6a1ef1d7][decentralisation]], [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][libre software]] and [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] adjacent ideas (e.g. [[id:1550b2cf-584d-4bf7-ba81-9fafea0af1c0][Solid]]) are mentioned for digital self-determination, although you'll be left to your own devices as to how you do something practical with those ideas.

Anyway, it's something I will definitely return to when I circle round to particular ideas again.

