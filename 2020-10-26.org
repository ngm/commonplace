:PROPERTIES:
:ID:       81f3c031-52e2-4638-a756-773bd94c815c
:mtime:    20211127120902 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-10-26

* spacemacs php layer

  The [[id:b529d37d-becd-495d-be37-dd91a4dc039b][spacemacs]] [[https://develop.spacemacs.org/layers/+lang/php/README.html][php layer]] bundles together lots of nice little features. I'm not sure which bits comes from where, but currently just the basic usage of ~SPC m g g~ to jump to definition is super handy already for spelunking around in vendor libraries.

* PHPUnit command/package is not installed 

  Was getting this error in [[id:aad51368-24a7-460e-8944-255958dcf67f][Emacs]] when trying to run [[id:6bb9df20-dedc-40cb-8287-493808dafe2b][PHPUnit]] tests via phpunit.el.  It's possible to change the location of the phpunit executable via ~phpunit-set-dir-local-variable~, but I wondered why it wasn't working out of the box (when it was on another project).  Turns out that a long time ago this particular project had the ~bin-dir~ setting in ~composer.json~ set to something else, rather than the standard ~vendor/bin~ (see https://getcomposer.org/doc/articles/vendor-binaries.md).

  I just changed it back to the standard location, and phpunit worked fine again.

* [[id:a38f29bd-275f-4102-84c6-56dd7c869a0c][Speeding up org-publish]]

* [[id:80d25053-9a4e-4e44-9744-03cf4feb8021][5tracks - week 44]]
