:PROPERTIES:
:ID:       dd307774-66c8-4e71-a1c9-4c3b4ec3baf4
:mtime:    20230324112546
:ctime:    20230324112546
:END:
#+TITLE: Billions Will Die If They Don’t Listen to Us. Aaron Bastani Meets Roger Hallam.
#+CREATED: [2023-03-24 Fri]
#+LAST_MODIFIED: [2023-03-24 Fri 11:27]

+ URL :: https://novaramedia.com/2023/03/20/billions-will-die-if-they-dont-listen-to-us-aaron-bastani-meets-roger-hallam/

Interesting discussion. [[id:9e455ffd-10db-4d1c-86ca-4723c01aa417][Roger Hallam]] very focused on [[id:946c81bf-503a-4d6e-b591-d970730357c9][praxis]], and fair enough.  ("meeting point of theory and practice").  Came across generally dismissive of theory though.
