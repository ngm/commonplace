:PROPERTIES:
:ID:       6b5548ec-51c0-430d-9598-dd30545b58d1
:END:
#+title: We should socialise the platforms

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

See e.g. [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]],
[[id:9d3a969e-bb7d-44bd-be11-c73d93d774a2][A Common Platform: Reimagining Data and Platforms]]
