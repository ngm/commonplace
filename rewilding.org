:PROPERTIES:
:ID:       f19b404a-a0d4-4733-86c2-baf4c5e5e93f
:mtime:    20220526123357 20211127121011 20210724222235
:ctime:    20210724222235
:END:
#+title: Rewilding
#+CREATED: [2021-04-05 Mon 16:47]
#+LAST_MODIFIED: [2022-05-26 Thu 12:33]

#+begin_quote
conservation efforts aimed at restoring and protecting natural processes and wilderness areas. 

-- [[https://en.wikipedia.org/wiki/Rewilding_(conservation_biology)][Rewilding (conservation biology) - Wikipedia]] 
#+end_quote

#+begin_quote
Rewilding means not only allowing natural forests and grasslands of native species to replace pasture but also returning wild animals to these ecosystems

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
