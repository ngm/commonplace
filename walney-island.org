:PROPERTIES:
:ID:       a2938006-2d4b-414e-a88c-20ab462e0b9d
:mtime:    20220712222614 20220710213334
:ctime:    20220710213334
:END:
#+TITLE: Walney Island
#+CREATED: [2022-07-10 Sun]
#+LAST_MODIFIED: [2022-07-12 Tue 22:26]

#+ATTR_HTML: :width 100%
[[file:photos/walney-coast.jpg]]

You can see the epic [[id:207656c6-5a98-470c-88eb-4631db3e4a18][Walney Wind Farm]] and the [[id:75706a57-c847-4aac-9494-8f4b300b6766][Isle of Man]].

#+ATTR_HTML: :width 100%
[[file:photos/black-combe-from-walney-island.jpg]]

[[id:b2a1f374-946c-4260-bce4-5805ec9c8902][Black Combe]] in the distance.
