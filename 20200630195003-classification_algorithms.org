:PROPERTIES:
:ID:       ffd13a1d-e550-41c4-a1ed-53d82e198d32
:mtime:    20211127121018 20210724222235
:ctime:    20200630195003
:END:
#+title: Classification (algorithms)

Picking a category.

* examples

- advertising, putting people into a particular category that might be receptive to certain ads
- classifying content on e.g. YouTube
- labelling holiday photos
- optical character recognition (classifying handwriting into letters)

