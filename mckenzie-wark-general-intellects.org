:PROPERTIES:
:ID:       a43405f7-29ef-49ff-bfc1-270c9cf5d107
:mtime:    20230522220633
:ctime:    20230522220633
:END:
#+TITLE: McKenzie Wark, General Intellects
#+CREATED: [2023-05-22 Mon]
#+LAST_MODIFIED: [2023-05-22 Mon 22:07]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://newbooksnetwork.com/mckenzie-wark-general-intellects-twenty-one-thinkers-for-the-twenty-first-century-verso-2017
+ Featuring :: [[id:cbcdc3c9-42c2-4159-9682-a3192f720b61][McKenzie Wark]]

