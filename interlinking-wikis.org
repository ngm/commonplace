:PROPERTIES:
:ID:       ffd70794-e498-4e5e-a74c-e8351dc0538a
:mtime:    20220207181635 20211227210710 20211127121018 20210919211752
:ctime:    20210919211752
:END:
#+TITLE: Interlinking wikis

How could you connect [[id:92f8694f-8482-4541-9e6f-8ce03f59e01f][digital gardens]] together?  And why would you? 

* Why?

[[http://reganmian.net/blog/][Stian Håklev]] posted an interesting question and on the Digital Gardeners telegram group:

#+begin_quote
I'm curious how people feel about comments and interaction? And also interactivity between digital gardens in general (like paths connecting the parks of a city? :)). 
#+end_quote

[[id:62cdf680-b517-47a6-b81a-4fba91aee329][Chris]] has talked enthusiastically about interlinking wikis before (e.g. during the [[id:e0f80607-a244-45c0-847b-c26118a14741][Gardens and Streams IndieWeb session]]), so I'm sure there's something to it.  For me, I think because I already get the interactivity goodness on my [[id:22aec571-2dcd-4fc8-abe5-6f7ab4083fc5][stream]] and my articles, it's something I haven't generally been that interested in for my wiki notes thus far.  

I can definitely see the appeal of backlinks between wikis, but only in an abstract sense at the moment.  

The utility is in the [[id:a322a380-4fe5-49c7-868f-1869341ccef1][networked thought]].
I guess for me it comes down to whether I see the utility in all of this connectivity on specifically my [[id:2446d0dd-825d-44a7-b620-0daacdd73a35][evergreen notes]], as opposed to my stream posts.

[[id:56a7b34e-cf41-4d2c-996e-7759008d7775][A Platform Designed for Collaboration: Federated Wiki]] has lots on this.

 #+begin_quote
 This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration. But in fact the effect is quite the opposite: *giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis results in a richer, more robust commons*.

 -- [[id:56a7b34e-cf41-4d2c-996e-7759008d7775][A Platform Designed for Collaboration: Federated Wiki]]
 #+end_quote
 
[[id:5ce43ee0-8fcb-4285-b417-776326c81ea6][How does linking gardens facilitate a richer commons?]]

 Drilling down. Commons in FFF being a shared governed resource. What's the actual value of the output of a commons of joined gardens?


* What?
  
** Comments
   
Stian has some use cases for which he would like the interactivity:

#+begin_quote
I know comments have gotten a bad rep on the internet, attracting spam or trolls etc, but on the other hand I feel really frustrated when I can't leave comments on Andy Matuschak's notes... 
#+end_quote

I think Webmentions would work well here.  You would write a comment as a post on your own site, and then this will notify Andy.  He can choose to do whatever he wants with this comment (display the comment, display it as a backlink, ignore it completely, not display it at all, if he prefers).  This way you can write a comment on whatever you want and the receiver chooses what to do with it.

#+begin_quote
Or another example - I just looked at Salman's site about Deliberate rest (https://notes.salman.io/deliberate-rest), and thought that I just took some notes about attention restoration therapy from Deep Work  - https://notes.reganmian.net/deep-work... Of course I could tell him here (I am :)) but that "doesn't scale"... 
#+end_quote

Webmentions would work for this too - as just a simple 'mention', not necessarily a comment.  Salman would be notified automatically that your note references his note. Salman could choose to display it as a backlink, if he liked.

#+begin_quote
Short-term, I am looking at adding at least page-level comments to my blog, using a Gatsby plugin and probably externally hosted comments. 
#+end_quote

Adding webmention support to receive comments could work here.

** Annotations

#+begin_quote
Also interested in experimenting with annotations, for example embedding Hypothes.is directly in the pages... 
#+end_quote

Kartik Prabhu has a [[https://kartikprabhu.com/articles/marginalia][nice article]] about receiving annotations on his posts via webmentions.

** Backlinks
   
#+begin_quote
Long-term, I'm interesting in thinking about more structured ways of interlinking digital gardens - whether it looks more like interwiki links, blog backlinks, or something else, I'm not sure. I have some notes I'll publish once I organize them a bit more.
#+end_quote

** Combining content

Sharing blocks of content easily between individual gardens.

* How?

** 'Multiplayer'
 Roam is now [[https://twitter.com/Conaw/status/1307107745397604354][talking about 'multiplayer']]. I'm presuming this means linking between individuals' wikis.  I like the idea of [[id:ffd70794-e498-4e5e-a74c-e8351dc0538a][interlinking wikis]], but imagining it's going to be pretty closed in Roam.  

** WikiWeb
  
 Bill Seitz has lots of ideas about a more open [[http://webseitz.fluxent.com/wiki/WikiWeb][WikiWeb]]. 

 #+begin_quote
 aka Wiki Sphere; parallel to BlogWeb, the universe of (clusters of) WikiSpaces
 #+end_quote

** FedWiki
  
 FedWiki is obviously doing great things in this space.  

 However, a downside of FedWiki (as far as I understand it) is that it is one platform, one tool, that facilitates the federation.  A [[https://indieweb.org/monoculture][monoculture]].

** Agora
 I would prefer a more distributed, protocol-based approach.  [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]] works in this sense as each individual can write their garden whichever way they see fit, and just needs to fit in to some accepted formatting protocols to be aggregated.

** IndieWeb
 See also the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] approach, where the onus is less on aggregation and more on a certain protocol of communication.  

 FedWiki does seem to offer much simpler actions for copying content from one wiki to another.  Agora doesn't have this built in as such.  IndieWeb maybe more so (retweets, quotes, likes, etc, are catered to through webmentions)

** [[id:4a0b39ed-8827-4a2c-9752-170a4a6bee64][Collective Knowledge Management]]

What Athens Research is calling this.

#+begin_quote
CKM is a way for groups of people to create, connect, and compound knowledge, from research and documentation to ideas and conversations.

-- [[https://athensresearch.ghost.io/season-2/][Season 2 of Athens — A Collective Vision]] 
#+end_quote

** massive.wiki

   https://massive.wiki/

   Seems similar to Agora in some ways - combining individual collections of markdown files in to something bigger.

** bopwiki
   https://floss.social/@Valenoern/104748723877868120
   
   If I understand, the intent is to allow users of ActivityPub easily add things from their stream in to a wiki.

* Links

  - [[id:8b51c392-8f11-4666-9072-99575271845f][Commons P2P wiki requirements]]
  - [[id:5f2fb62e-0725-4d6e-819c-a672d3907138][Distributed wikis on solid]]
  - [[https://github.com/IndyVerse/indywiki][GitHub - IndyVerse/indywiki: Web3Native Tinkerable Wiki]]
  - [[id:e68dca42-5fad-42c6-88a5-f74f261d2e64][Federated Education: News Directions in Digital Collaboration]] 
  - [[id:56a7b34e-cf41-4d2c-996e-7759008d7775][A Platform Designed for Collaboration: Federated Wiki]]

* Log
** [2021-05-16 Sun]   
   I guess there is a distinction between:
   - federated wikis
   - aggregated wikis
   - distributed wikis
   perhaps?  Similarities, overlaps, possibly they're the same.  Would be good to differentiate.
   
** [2021-12-27 Mon] 

Increasingly convinced that the distributed/federated digital garden approach is the way to go for knowledge commoning. I say this because I've been doing it for a while and it is working well in practice.

Plenty to crib from the ActivityPub model, I think - where you have local, global, and 'those-you-follow' timelines. Similarly, you could have local, 'those-you-follow', and global gardens. I'd in fact prefer it if this model worked more on some kind of liquid democracy style, where there's some rippling out effect that lives between the following and global timelines. I guess perhaps there's some repost/boost equivalent?

And as with ActivityPub, where it's kind of absent, I'd want for there to be a 'group' concept that isn't based on infrastructure, just on interest groups.

As with streams, my ideal is probably some middleground of Indieweb and ActivityPub approaches.



