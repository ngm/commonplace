:PROPERTIES:
:ID:       04103922-6e31-4d71-a285-880f55e08924
:mtime:    20211127120755 20210724222235
:ctime:    20210724222235
:END:
#+title: Little Red Songbook
#+CREATED: [2021-02-28 Sun 14:10]
#+LAST_MODIFIED: [2021-11-27 Sat 12:07]

"songs to fan the flames of discontent"

first published by the [[id:225d6ec1-c3d0-4092-a383-6e0f74742fbb][Industrial Workers of the World]] in 1909. 

- [[https://libcom.org/library/little-red-song-book][The little red song book]]

