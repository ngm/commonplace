:PROPERTIES:
:ID:       970ecf50-1272-48b6-8888-153388200b92
:mtime:    20211127120915 20210918010247
:ctime:    20210918010247
:END:
#+TITLE: Gaian Systems: Lynn Margulis, Neocybernetics, and the End of the Anthropocene
#+CREATED: [2021-09-18 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://newbooksnetwork.com/gaian-systems
+ Publisher ::  [[id:65048c6b-870b-4b3e-84d3-e7275d141dfd][Systems and cybernetics]]

[[id:18bd9889-fad4-4db4-a080-0028f8d006b2][Gaia hypothesis]].
