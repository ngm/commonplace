:PROPERTIES:
:ID:       1c1f0a2f-c416-4b5c-988a-6b37c25118f4
:mtime:    20220123122118
:ctime:    20220123122118
:END:
#+TITLE: All you need is links
#+CREATED: [2022-01-23 Sun]
#+LAST_MODIFIED: [2022-01-23 Sun 12:31]

+ URL :: https://subconscious.substack.com/p/all-you-need-is-links
+ Author :: [[id:910a5921-4de6-462c-837b-34133ad5cc93][Gordon Brander]]

Article.  Also a [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]] I guess.

Basically saying that you can get a long way with just links.

The can provide functionality for:

+ [[id:20210326T232652.811020][tags]]
+ [[id:4cc02aaa-9e88-41dc-a527-0f455a4768f7][folders]]
+ [[id:1616ad34-df38-4aa4-beeb-447b8cbfdfaa][interactions]] (likes, stars, etc)
+ [[id:5d2c40dd-2bab-4070-9c18-a5f28d2f5f7d][comments]]
+ [[id:aae977ab-3513-43fa-9357-afc2c1e7d405][outlines]]
+ [[id:f6e641c1-b0d5-415d-b563-42de9e0e6725][semantic triples]]
+ [[id:00e1daf1-32bf-426d-8fdd-4f8aecfddd69][topic modeling]]
