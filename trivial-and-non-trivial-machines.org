:PROPERTIES:
:ID:       4604ffe5-f1e8-4fd3-af2b-e3c587d99228
:mtime:    20211127120831 20210819003644
:ctime:    20210819003644
:END:
#+TITLE: Trivial and non-trivial machines
#+CREATED: [2021-08-19 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

[[id:9c23aaf5-05ed-4786-9ac9-6e9fef8a8dcd][Heinz von Foerster]].

 #+begin_quote
 A trivial machine is a machine whose operations are not influenced by previous operations. It is analytically determinable, independent from previous operations, and thus predictable. For non-trivial machines, however, this is no longer true as the problem of identification, i.e., deducing the structure of the machine from its behavior, becomes unsolvable.

-- [[https://www.univie.ac.at/constructivism/HvF.htm][The Heinz von Foerster Page]]  
 #+end_quote
