:PROPERTIES:
:ID:       34eb592e-0a9b-4f37-ab9c-e37918abe1ed
:mtime:    20211127120822 20210724222235
:ctime:    20200927194249
:END:
#+title: About this site

This is my [[id:3e95dc16-444f-4683-aa90-a859b92de533][hyper commonplace garden wiki]].  I started it (in this format) in [[id:a4efd750-9e13-4e79-9b1e-71ccfaea4aa2][October 2019]].

It is a companion to my [[https://doubleloop.net][blog]].  They are [[id:ffbea278-7011-49d6-8c1e-c784d40abb6b][the Garden and the Stream]].

Please feel free to click around here and explore.  Don't expect too much in the way coherence or permanence... it is a lot of half-baked ideas, badly organised.  The very purpose is for snippets to percolate and  morph and evolve over time, and it's possible (quite likely) that pages will move around.  

That said, I make it public in the interest of info-sharing, and occassionally it is quite useful to have a public place to refer someone to an idea-in-progress of mine.

Some more info on the [[file:wikis.org][whats and the whys]].
