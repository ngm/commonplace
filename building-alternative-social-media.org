:PROPERTIES:
:ID:       0c891a34-68e2-40d6-b811-2069824dfe4f
:mtime:    20220313135926 20211127120800 20211126214009
:ctime:    20211126214009
:END:
#+TITLE: Building alternative social media
#+CREATED: [2021-09-12 Sun]
#+LAST_MODIFIED: [2022-03-13 Sun 14:00]

[[id:c1682a80-e58d-4c1b-9ea6-1e683847a092][Alternative social media]].

* As per Anarchist Cybernetics
  
There's a chapter called exactly this, "Building Alternative Social Media" in [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]] on this.

With a list of desirable features:

- Forums for conversation
- Direct messaging
- Sharing content with meme/viral potential
- News feed with curated and user-generated content
- Public relations function
- Linking alternative and mainstream platforms
- Central resource hub
- Noise and overload filters
- Overuse addiction warning system
- General assembly function
- Pad function
- Memory / narrative
- File sharing and knowledge exchange
- Procrastination
- Online decision making embedded in offline structures
- Sections open to non-members
- Architecture for [[id:8def3efd-6f04-4254-b918-d0745396a587][collective autonomy]]
- Flexible engagement options
- Responsibility and commitment
- Privacy, security and data
- Easy backup of information

I like this list.  Is it solely social media though?  What explicitly *is* [[id:6807fcc6-8104-4e03-bf09-b4fd04469f6f][social media]]?

Anyway, I would probably advocate to attain the above in a patterny /  Unixy way I feel. i.e. bridge and glue together the different pieces, avoid a behemoth platform.

I suppose a behemoth matters less if it's libre. But it feels like the pieces are already there to glue together, so don't reinvent the wheel.
