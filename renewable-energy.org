:PROPERTIES:
:ID:       2f0cc071-0606-495f-8194-535d09cae40c
:mtime:    20220828152306 20220526123440 20211127120819 20210914231852
:ctime:    20210914231852
:END:
#+TITLE: Renewable energy
#+CREATED: [2021-09-14 Tue]
#+LAST_MODIFIED: [2022-08-28 Sun 15:24]

#+begin_quote
What would a completely renewable energy system look like in terms of land use? Energy expert Vaclav Smil estimates that such a system would take up 25 to 50 per cent of the US land-mass, while rich and densely populated countries like the UK would have a ratio approaching 100 per cent

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
renewable energies are extremely resource-intensive, and building an infrastructure for renewable energies will itself continue to consume huge amounts of fossil energy.

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

#+begin_quote
order to keep the prices of raw materials low enough so that the prices of renewables do not exceed those of fossil fuels, an imperialist race is already underway to control and develop raw material deposits. A “green” capitalism is therefore, necessarily an imperialist one

-- [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]
#+end_quote

[[id:65403284-7dac-40de-a041-c1b958a5596c][green capitalism]]
