:PROPERTIES:
:ID:       3eaaa2ac-d87d-4844-85bc-268be30d1bc1
:mtime:    20211127120827 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-06-30

* Reading [[id:26bfd69b-6760-4a52-a6fc-28093ab1e046][Hello World]]
  
Reading Hello World at the moment.  Subtitled "being human in the age of algorithms".

It's good so far.  Clear and making its point well, drawing on plenty of examples of the problems with some present uses of decision-making [[id:710635c6-d4a2-42a0-9951-101dafe9a82f][algorithms]].  It's being framed as 'dilemmas', so, the idea that there's good as well as bad in what's going on.

I wonder what the overall thesis will be though.  Will there be some call to action as to what needs to be done?  Or will it just be left that there is good and bad, and we need to be aware of that.  Hoping for the former, something with some teeth.
