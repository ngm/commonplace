:PROPERTIES:
:ID:       2f8ae047-fb30-4a92-8c68-bc466810ee6f
:mtime:    20211127120819 20210724222235
:ctime:    20200718194845
:END:
#+title: Spanish omelette

- source: https://hurrythefoodup.com/quick-spanish-omelette/

1. Grate the potato (peel the skin if it looks manky).
1. Dice the onion.
1. Heat the oil in the pan on medium heat and add the potato and onion with a pinch of salt.
1. Stir every now and then, cooking for about 7 minutes until you have a nice golden brown edge to it.
1. In the meantime, beat the eggs in a mixing bowl, again with a pinch of salt.
1. When the potato-onion mix is golden brown, add it to the eggs in the mixing bowl. This starts to cook the egg.
1. Put it all back into the pan and cook on a low heat with a lid on for 5 minutes.
1. After this, take the lid off and get a plate that is wider than the pan. Place the plate face down onto the pan. With a firm grip on the handle and the plate, flip it. You can also use the pan's lid for this.
1. Now slide the omelette back into the pan the other way up. If you’re not sure how to do it we urge you to check out this video. It’ll make life easier.
1. Cook for another 5 minutes on low heat.
1. Awesome, your omelette is then ready. Serve with a sauce of your choice, and enjoy!
1. Que aproveche!
