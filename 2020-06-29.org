:PROPERTIES:
:ID:       afb07510-b401-410a-818c-eb7f76ce6c49
:mtime:    20211127120930 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-06-29

* [[id:f723adc7-128c-411f-ad96-820b210ee373][Facebook only cares about money]]
  
[[id:fe2273c1-dd14-4aa2-9323-cafa2ab70ef5][Facebook]] will make some changes around its policy on hateful content, but only from the threat of lost ad revenue.  Not from actually caring about the victims of it.

#+begin_quote
“Let’s be honest,” said Moghal, "these tech platforms have generated income and interest from this divisive content; they won’t change their practices until they begin to see a significant cut to their revenue."
#+end_quote

Sucks that only big companies pulling out can have an effect on FB.  But props to Stop Hate for Profit for putting pressure on companies.

https://www.theguardian.com/technology/2020/jun/29/how-hate-speech-campaigners-found-facebooks-weak-spot  (thanks [[id:54789e69-0048-4be4-b193-1cc50616313c][Ellie]] for the link!)
