:PROPERTIES:
:ID:       c732747f-e14e-40de-8e17-ea5bc83481e0
:mtime:    20230424204919
:ctime:    20230424204919
:END:
#+TITLE: Internet Enclosure w/ Ben Tarnoff
#+CREATED: [2023-04-24 Mon]
#+LAST_MODIFIED: [2023-04-24 Mon 20:53]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://tribunemag.co.uk/2022/09/95-internet-enclosure-w-ben-tarnoff

#+begin_quote
On this week's podcast, Grace speaks to [[id:cdadc98c-109f-480b-a044-d4b72a470051][Ben Tarnoff]], author of [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]]. They discuss the history of the web's enclosure and privatisation – and how we could build a different model for the future.
#+end_quote

Nice discussion around Ben's outline of a program for [[id:2f0d168a-4af5-4e96-a3e1-5c12263cf4fc][deprivatisation of the internet]].

Ben talks a bit about avoiding separating [[id:8cd5d652-6206-4141-b9d5-e053fd6cefb3][forces of production]] and [[id:a9e90189-a8cf-4ea7-8a1d-697353097842][relations of production]].  Says they are entwined.  So simply nationalising Facebook, for example, wouldn't necessarily make it any better.
