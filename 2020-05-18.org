:PROPERTIES:
:ID:       281cc74b-745d-40a0-99a3-a748cf057d2e
:mtime:    20211127120815 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-05-18

* The culture industry
  
Been listening to The Partially Examined Life [[https://partiallyexaminedlife.com/2016/03/28/ep136-1-adorno/][episode about Adorno on the Culture Industry]].  The [[id:844cedb3-da62-4fba-9435-73bbc1363e4b][culture industry]] being the mass production of films etc for the purposes of rendering people docile and perfect for the consumption of unnecessary goods.

They say [[id:c5533c49-9c21-427d-8eac-5090bb51cb95][Adorno]] (/Horkheimer) very explicitly wanted it referred to as a culture *industry*, to make clear it was driven from above and imposed.  Not a natural representation of the desires of the masses.  

Can definitely see the parallels with the [[id:e2e10174-fcd7-4d4e-a294-fb38f50aeff7][social industry]] and guess this is probably why Richard Seymour used that name in [[id:d4276054-35f6-4108-b4fa-85df76206caa][The Twittering Machine]].
