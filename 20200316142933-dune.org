:PROPERTIES:
:ID:       2aa957e5-873c-44b8-b1ce-a51dd87a73ea
:mtime:    20211127120817 20210724222235
:ctime:    20200316142933
:END:
#+TITLE: Dune

-- Frank Herbert

- tags: [[id:5f333662-2e97-4ede-88bc-b0e312fc7b6a][books]] 

Read this for the first time in Feb/[[id:14ac9fbd-4342-4bbb-bb73-fd71f6941420][March 2020]].  

I started off really enjoying it. 

#+begin_quote
It feels a bit like a fantasy book, but with little hints of sci-fi thrown in here and there. I like the way it’s written. I like how the characters have these little thoughts as asides now and then.

Feuding dukes doesn’t really get me going plot wise, but some of this CHOAM company stuff seems like it could be interesting.
#+end_quote

About two thirds through my interest was waning a bit.

#+begin_quote
I’m about two thirds of the way through Dune.  Still well written but all this stuff about the machinations of Fremen culture and Paul going off his nut and having visions all the time, I’m finding a bit less compelling plot-wise.  Also I don’t like things that are underground or in caves.  Makes me feel claustrophobic.
#+end_quote

I finished it off overall enjoying it, but not *loving* it, like a few people seem to.  I'd probably read the next in the series - maybe you get more out of it as a series - but I'm not rushing to.  I'm keen to see the film though.

#+begin_quote
Finished Dune off the other night. It was a bit Shakespearian, duelling families, noble characters, sweeping arcs of history and all that. Soliloquys. I did enjoy it and finished it off quickly. But on reflection I can’t say I really actually *liked* any of the characters very much. Interesting yes, do I care what happens to them, not so much.

On the plus side I can watch the David Lynch film now.
#+end_quote

