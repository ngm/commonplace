:PROPERTIES:
:ID:       4a2c1a8d-4537-4b9f-9a88-58f820edb5e0
:mtime:    20220830211020
:ctime:    20220830211020
:END:
#+TITLE: Effecting change
#+CREATED: [2022-08-30 Tue]
#+LAST_MODIFIED: [2022-08-30 Tue 21:10]

#+begin_quote
One-eight-billionth wasn’t a very big fraction, but then again there were poisons that worked in the parts-per-billion range, so it wasn’t entirely unprecedented for such a small agent to change things.

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote
