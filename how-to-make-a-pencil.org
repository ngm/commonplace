:PROPERTIES:
:ID:       3c8b35c0-76c4-4433-bdc9-220c573f397b
:mtime:    20220530210125
:ctime:    20220530210125
:END:
#+TITLE: How to Make a Pencil
#+CREATED: [2022-05-30 Mon]
#+LAST_MODIFIED: [2022-05-30 Mon 21:32]

+ URL ::  https://logicmag.io/commons/how-to-make-a-pencil/
+ Author :: [[id:93a9a7f1-8704-4a47-8c95-dab1c30ec92a][Aaron Benanav]]

[[id:4f0437ba-340d-4ec6-9c8e-507f629b9875][Socialist economy]].  [[id:31c25452-2977-4054-ae2e-8bc09704a9ce][Planning]].  [[id:286a25df-a49d-408d-920d-d23da4151d54][Otto Neurath]].

#+begin_quote
The digital socialist focus on algorithms presents a serious problem. It risks constraining the decision-making processes of a future socialist society to focus narrowly on optimization: producing as much as possible using the fewest resources
#+end_quote

#+begin_quote
After all, the societies of the future will want to do more than just produce as much as possible using the fewest resources. They will have other goals, which are more difficult to quantify, such as wanting to address issues of justice, fairness, work quality, and sustainability—and these are not just matters of optimization
#+end_quote

#+begin_quote
This is where planning protocols come in. They streamline decision-making by clarifying the rules by which decisions are made. Deployed in concert with algorithms, protocols enable a range of considerations—besides those available to an optimization program—to enter into the planning process
#+end_quote

#+begin_quote
Putting both algorithms and protocols to work, people can plan production with computers in ways that allow their practical knowledge, as well as their values, ends, and aims, to become integral to production decisions. The result is something that neither capitalism nor Soviet socialism allowed: a truly human mode of production
#+end_quote

[[id:4ed9e584-c86c-440d-9d10-35d52ea3a2d8][Socialist calculation debate]]

#+begin_quote
Of course, the prices that pencil makers use to make production decisions are not just random numbers. They are expressions of a living market society, characterized by decentralized decision-making, involving large numbers of producers and consumers
#+end_quote

#+begin_quote
As Mises’s student [[id:7502b890-f087-4914-ab61-2e9a5afa0159][Friedrich Hayek]] later emphasized, an economy is not a set of equations waiting to be solved, either with a capitalist price system or a socialist computer. It is better understood as a network of decision-makers, each with their own motivation, using information to make decisions, and generating information in turn. Even in a highly digitally mediated capitalist economy, those decisions are coordinated through market competition. For any alternative system to be viable, human beings still need to be directly involved in making production decisions, but coordinated in a different way
#+end_quote

#+begin_quote
Mises and Hayek were correct to observe that people’s participation in decision-making will remain essential for any economy to function. Yet their vision also sets strict limits on who has the opportunity to exercise this agency
#+end_quote

#+begin_quote
In a socialist society, however, the entire population would control production. Decision-making power would be democratized, and this would almost certainly lead to different kinds of decisions being made
#+end_quote

#+begin_quote
Efficiency, whether calculated in terms of energy use, resource consumption, or labor time, would remain a concern, but it would no longer be the sole concern. It would simply be one of many. Other considerations—dignity, justice, community, sustainability—would also enter the picture
#+end_quote

#+begin_quote
We need to be able to make planning decisions on the basis of multiple, incommensurable criteria, and to coordinate these decisions across society. To do this, we must have agreed-upon procedures for making such decisions collectively—protocols
#+end_quote

#+begin_quote
During World War I, masses of workers joined militant rank-and-file movements demanding workplace democracy, including the [[id:225d6ec1-c3d0-4092-a383-6e0f74742fbb][Industrial Workers of the World]] in the US, the [[id:0af25c51-118c-4887-886c-ea04c16981f2][Shop Stewards Movement]] in the UK, the councilists in Germany, and the anarcho-syndicalists in Spain, France, and Italy
#+end_quote

#+begin_quote
We do not want software to substitute for the price mechanism
#+end_quote

#+begin_quote
To function at all, a society that replaces the single-minded focus on cost control with multi-criteria decision-making must use algorithms to help clarify the choices to be made and protocols to help structure the way it makes these choices
#+end_quote
