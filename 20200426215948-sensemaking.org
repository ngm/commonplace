:PROPERTIES:
:ID:       98923c43-0c65-4cee-98c9-f48a9dc50b73
:mtime:    20211127120916 20210724222235
:ctime:    20200426215948
:END:
#+TITLE: sensemaking

#+begin_quote
A question that came up for me, musing about the conversation is what it is I am trying to automate or reduce friction for? If I am trying to automate curation (getting from crumbs to articles automagically) then that would be undesirable. Only I should curate, as it is my learning and agency that is involved. Having sensemaking aids that surface patterns, visualise links etc would be very helpful. Also in terms of timelines, and in terms of shifting vocabulary (tags) for similar content.

-- [[https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/][On Wikis, Blogs and Note Taking – Interdependent Thoughts]]
#+end_quote
