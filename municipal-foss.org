:PROPERTIES:
:ID:       4826376d-fe4a-4a35-a01b-872897b36d13
:mtime:    20220206113342 20211127120832 20210724222235
:ctime:    20210724222235
:END:
#+title: Municipal FOSS
#+CREATED: [2021-04-23 Fri 20:22]
#+LAST_MODIFIED: [2022-02-06 Sun 11:33]

- [[id:ae24dcc9-3fd8-4f7e-bef5-b191da524faa][Free software becomes a standard in Dortmund, Germany]]
- [[id:bd707b81-ec79-46b2-a582-b142eadde95d][How a local government migrated to open source]]

 Local governments should make use of free and open source software.  And should follow [[id:10332554-6fe5-49be-b28d-4dedc24d1cfc][Public Money, Public Code]].

 How does it fit in to [[id:9250b263-a25e-4d6c-996c-3bcfd65cbce4][Municipal socialism]]?  e.g. where would FOSS fit in to [[id:174ed8ef-0f25-4a92-bdf3-c06dcaf6493c][The Preston Model]]?

[[id:86a4ffcb-1c36-4b78-8a38-f3181e3ae8aa][How would you include FOSS in community wealth building?]]

[[id:76981cbf-4c71-4aba-a792-63c4f7815eb5][public sector free software]]
