:PROPERTIES:
:ID:       c9799533-c66b-450e-84cc-2bd8325dd0ea
:END:
#+title: What’s the Value of Data? (ft. Salomé Viljoen)

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ Found at :: https://www.patreon.com/posts/305-whats-value-95039111
+ Part of :: [[id:f8e35c03-c235-435a-add8-04c26272ee12][This Machine Kills]]

+ [[id:53bca401-ec5f-4cc5-96bd-a4df28bd2f44][Social data]].
+ Has [[id:a899f494-03df-4c87-b91d-4af1584b1f41][predictive value]].
+ Three ways to extract [[id:37ed5add-aaac-42e9-b7d6-597cf3d13af9][surplus value]] with it:
  + 1. Just sell it on, e.g. data broker
  + 2. Use it to exploit people based on knowledge from the data
  + 3. Use it to exert power (e.g. Uber's [[id:302590a5-0cd0-4cb3-bb11-a81f9e829750][Greyball]] program)
