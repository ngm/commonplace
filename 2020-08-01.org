:PROPERTIES:
:ID:       44ddc132-2dde-49ad-b305-26684f6fa776
:mtime:    20211127120830 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-08-01

* Reading The Twittering Machine and Small is Beautiful
  
I'm on a bit of a non-fiction kick at the moment.  I've started reading [[id:d4276054-35f6-4108-b4fa-85df76206caa][The Twittering Machine]] and [[id:6d207ece-0e7b-4d05-940b-ee91ee722faf][Small is Beautiful]].

The Twittering Machine is about the [[id:e2e10174-fcd7-4d4e-a294-fb38f50aeff7][social industry]] and its effects.  Though I guess at a broader level, it is about writing, and how we're in an era where we're all writing more than ever - but looking at the results of that.

Small is Beautiful is E. F. Schumacher's collection of essays from the 70s that outlins  his critique of Western economics, and describes ideas like [[id:7ee5a1b6-9c3b-41a3-8683-e1812b02b192][appropriate technology]] and [[id:6c6a8325-757f-4937-9c61-475411f01775][Buddhist economics]].  


* Reposted: [[https://werd.io/2020/raising-the-alarm][Raising the alarm]] by Ben Werdmüller

#+begin_quote
It’s impossible to ignore.

Federal agents dressed in military gear have been bundling protesters into unmarked vehicles (allegedly rented from Enterprise) in Portland, with additional reports from Chicago and San Diego. The White House has announced that they’ll be rolling these troops out nationwide...
#+end_quote

A troubling assessment on the current and potential future state of America.
