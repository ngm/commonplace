:PROPERTIES:
:ID:       46ebd2a2-812f-42e5-9d3c-ebee6a1ef1d7
:mtime:    20220307193650 20211218124126 20211127120831 20210724222235
:ctime:    20200718193005
:END:
#+title: Technological decentralisation

Generally a fan.

#+begin_quote
The concept is boringly normal in some areas: your phone can call any other phone, your bank lets you transfer money to customers of any other bank, and likewise for email you can choose your preferred provider and app.

-- [[https://redecentralize.org/redigest/2022/02/][Redecentralize Digest — February 2022 — Redecentralize.org]] 
#+end_quote

#+begin_quote
But nowadays, much of online communication only works between users of the exact same app and same platform, because tech companies create them with this restriction; a practice that we could call monopoly-by-design

-- [[https://redecentralize.org/redigest/2022/02/][Redecentralize Digest — February 2022 — Redecentralize.org]] 
#+end_quote

#+begin_quote
This confusion stems from the use of the overly-general application of the word “centralized.” While the ability to host is not consolidated on the Web, the functions of Web applications (identity, networking, configuration, data storage) are often consolidated within the hosts. As a result, it might not always be accurate to say that the Web is centralized, but rather that the application functions are.

-- [[https://infocivics.com/][Information Civics]] 
#+end_quote
  
- [[id:e2e10174-fcd7-4d4e-a294-fb38f50aeff7][Social industry]]
- [[id:87e1d161-34f4-42f6-ba59-bc4aaab94e8e][Blockchain]] - blockchain!  blockchain!
- [[id:32ce1568-3e39-41cf-aa77-7ca96e49944b][fabrication]]

Early peer to peer stuff like [[id:20afb7bf-40a5-4295-93ee-fac69a18a3bb][Napster]], [[id:16403781-6ae5-40d8-a186-31dbf6c577f5][BitTorrent]].
 
Fascinating sprawling discussion on counter anti-disintermediation, decentralized social media, decentralized infrastructure, politics of coops, mesh networking.

https://social.coop/web/statuses/99324980275573606

Great article on decentralising the internet and the successes and failures of federation. Suggests some new approaches and some policy support (but not a [[id:5bab184b-3d5f-449b-9427-15a59e4f92ad][StateBook]]) are needed for decentralisation that actually empowers. [[id:91581ce2-3841-4234-92de-452f1ae9e408][Platform coops]] play a role. We need [[id:b549ff48-ac5b-4788-93e8-4fb0bd06cfad][counterantidisintermediation]] to prevent platform lock-in. I think the ‘centralised services win (partly) through better UX’ argument needs examining though. 

https://newsocialist.org.uk/decentralising-the-internet/

#+begin_quote
Decentralisation advocates roleplay as antagonists for change, but they have yet to truly threaten incumbent power. Instead, the de-prioritisation of privacy by design – regardless of its justification – enables its behaviour and offers it new scope for surveillance and control. 

-- [[id:8a41f43e-71f4-447e-83d0-14d9158861a9][This is Fine: Optimism and Emergency in the Decentralised Network]] 
#+end_quote

#+begin_quote
We need to lay aside our delusions that decentralisation grants us immunity – any ground ceded to the commons will be met with amplified resistance from those who already own these spaces.

-- [[id:8a41f43e-71f4-447e-83d0-14d9158861a9][This is Fine: Optimism and Emergency in the Decentralised Network]] 
#+end_quote

#+begin_quote
The re-decentralization of the web may be a technological design issue, but it will only be achieved if we understand it as a political objective.

-- [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]] 
#+end_quote

* Problems with centralised platforms
  
#+begin_quote
Dominant network effects, walled gardens, and high barriers for competition have produced de facto monopolies, where we overwhelmingly go to YouTube for videos or to Airbnb for apartment hosting. The race for traffic to support ad sales has promoted clickbait content and swarms of bots. Recommendation algorithms encourage extreme polarization and systemic bias. The micro-targeting of propaganda is weakening our democracies. 

What’s more, large centralized platforms are precarious repositories of the data they collect, and have been shown to willingly collaborate in state global surveillance.

- [[https://thereboot.com/toward-a-digital-economy-thats-truly-collaborative-not-exploitative/][Toward a Digital Economy That’s Truly Collaborative, Not Exploitative - The R...]] 
#+end_quote

#+begin_quote
The centralized nature of these platforms and the disparity of power between platform owners and user communities invariably leads to the concentration of resources in the hands of the owners. 

- [[https://thereboot.com/toward-a-digital-economy-thats-truly-collaborative-not-exploitative/][Toward a Digital Economy That’s Truly Collaborative, Not Exploitative - The R...]] 
#+end_quote

* ActivityPub vs. blockchain based decentralization:

#+begin_quote
Apps may demand real-time “global state”, and this is the key tragedy of Blockchain-like decentralized thinking. Trying to have decentralization but also demand global consistency. That is not needed in federation: decentralization, with a window into “the world” and yet acknowledgement there’s more out there that we don’t know. Perhaps a little too philosophical.

-- https://socialhub.activitypub.rocks/t/s2s-create-activity/1647/15​
#+end_quote
 
* Bookmarks

- [[https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/][Breaking Tech Open: Why Social Platforms Should Work More Like Email]] 
- [[https://decentpatterns.xyz/library/][Patterns | Decentralization Off The Shelf]]

