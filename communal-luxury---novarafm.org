:PROPERTIES:
:ID:       237998d4-8b6b-4530-82c5-7ccfba7cb295
:mtime:    20211127120812 20210724222235
:ctime:    20210724222235
:END:
#+title: Communal Luxury - NovaraFM
#+CREATED: [2021-04-18 Sun 18:13]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://novaramedia.com/2015/05/22/communal-luxury/
+ Publisher :: [[id:76505a4c-a0cb-4604-9154-e2a85f578823][Novara Media]]
+ Interview with :: [[id:5b05ba03-edee-479b-ac94-77e81c37353f][Kristin Ross]]

  #+begin_quote
  On this week’s show James Butler and Aaron Bastani are joined by Kristin Ross as they discuss her new book “Communal Luxury: The Political Imaginary of the Paris Commune”.
  #+end_quote

[[id:dab602b4-c6a8-4947-aaf3-3bfb70f40546][Paris Commune]].

It didn't just spontaneously appear - it had history in debating clubs etc.

Article excerpt from the book - [[id:af67a97b-f534-4de7-8ac9-63a13325f8a2][Birth of the Commune]].


