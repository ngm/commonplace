:PROPERTIES:
:ID:       5b33aa82-3d2b-4b4d-9217-14c2cf6d2f02
:mtime:    20211127120841 20210724222235
:ctime:    20200819104130
:END:
#+title: Permalinks and forgetting

Not sure how I feel about '[[https://www.w3.org/Provider/Style/URI][Cool URIs don't change]]' as it pertains to personal content.  I like the act of forgetting.   

I guess that article is more focused on how and why to not break links to things that you *want* to stay available, which is fair enough.

But I'm thinking more of things that you may wish to not be permanent.  It's not really my desire and I feel not my responsibility to make my past thoughts permanently accessible by anyone else.  If I want to change them and forget them, I should be able to.  I suppose you could keep the URI in place, but let people know that you've pulled that bit of info out of the public sphere. 

Why share it at all online then?  Many reasons.  For learning, evoloving dialogue and discussion, finding people with similar ideas.  But not per se as a permanent record.
