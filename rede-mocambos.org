:PROPERTIES:
:ID:       eac16a94-2b35-452f-84a7-9627c44aa61a
:mtime:    20230915143030 20230915105027
:ctime:    20230915105027
:END:
#+TITLE: Rede Mocambos
#+CREATED: [2023-09-15 Fri]
#+LAST_MODIFIED: [2023-09-15 Fri 14:30]

+ URL :: https://mocambos.net/tambor/pt/home

#+begin_quote
organizes quilombola, indigenous, artist and artisan communities

-- [[id:0d508ceb-88f1-4773-aef0-2cde0193afde][Decentralized and rooted in care: envisioning the digital infrastructures of the future]]
#+end_quote
