:PROPERTIES:
:ID:       d56318fc-6596-446e-b736-d659a58d212c
:mtime:    20211127120954 20210724222235
:ctime:    20200410200959
:END:
#+TITLE: the potential of crossposting

This is really interesting from CJ Eller on [[https://blog.cjeller.site/from-blog-to-blocks][the potential of crossposting]].  Interesting to think about how it fits in with the IndieWeb ideas of owning your own content and POSSEing (publishing on your own site, syndicating elsewhere).

I've thought of POSSE before more as a means of transitioning away from the big platforms (the [[id:652e1530-7570-4a01-9582-38d6d4c1c0bc][bit tyrants]]) while they still have the network effects.  But this is more about your stuff existing in various locations as a means to enable new creative uses of it.
