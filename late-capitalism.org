:PROPERTIES:
:ID:       e2a2a98f-bf7e-4f07-b870-585d66875340
:mtime:    20231124171721
:ctime:    20231124171721
:END:
#+TITLE: Late capitalism
#+CREATED: [2023-11-24 Fri]
#+LAST_MODIFIED: [2023-11-24 Fri 17:18]

The current (and final?) stage of [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].
