:PROPERTIES:
:ID:       b8665c5e-5bbf-4608-961c-750ef47c0803
:mtime:    20220206075606 20220129172539
:ctime:    20220129172539
:END:
#+TITLE: Boris Johnson is a liar
#+CREATED: [2022-01-29 Sat]
#+LAST_MODIFIED: [2022-02-06 Sun 07:57]

[[id:f0382cdc-bb8a-476a-bd6a-95004a3088d7][Boris Johnson]].

[[https://www.theguardian.com/politics/2021/dec/10/lies-accusations-boris-johnson-full-list-dishonesty-christmas-party][Lies, damned lies: the full list of accusations against Boris Johnson | Boris...]]

#+begin_quote
Johnson is no stranger to misinformation. He has made liberal use of it in the past – the Leave campaign he chaired made claims that the UK Statistics Authority later ruled “a clear misuse of official statistics”, while as prime minister he has repeatedly spread false information, lying about the implications of his Brexit deal for Northern Ireland. The UK Statistics Authority said his claim made in the House of Commons last Monday that crime had fallen by 14% was wrong.
#+end_quote

"Johnson is no stranger to misinformation": nice way of putting it.

#+begin_quote
And his relationship with the truth is now so loose that it is unclear how voters are supposed to distinguish between government announcements that are true or false statements designed to distract from the disintegration of his premiership.
#+end_quote

"loose relationship with the truth": he's a liar mate.

* Because

Some documented lies:

+ Christmas partygate
+ Wallpapergate
+ Brexit spending
+ Misleading the Queen
+ Hillsborough disaster
+ Extramarital affair
+ Journalism
+ New hospitals

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:9a13c2e4-f670-4d0d-9f15-62cfa405f37d][Yes definitely]]

Seems to be a lot of incontrovertible evidence in the public domain.
