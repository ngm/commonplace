:PROPERTIES:
:ID:       86a4ffcb-1c36-4b78-8a38-f3181e3ae8aa
:mtime:    20211127120906 20210724222235
:ctime:    20210724222235
:END:
#+title: How would you include free software in community wealth building?
#+CREATED: [2021-04-23 Fri 20:35]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

How would you include FOSS in [[id:8572adc8-6416-4f24-bb33-f74bdb0ff88b][Community wealth building]]?

Maybe hire local tech coops to work on implementing / improving FOSS for the council.  But with the rule that improvements are always contributed back to the upstream project.

Municipal sponsorship of FOSS could be a good financial model too.

Think of all the councils in the world, imagine if they all used something like [[id:af5b6882-d778-4aeb-ae84-021ffa4bb13d][LibreOffice]].  Some of their IT budget (i.e. public money) goes on using and improving libre software.  Maybe they work with local tech coops to work on the codebase.  Money gets invested locally, code improvements can be used globally.  That amount of distributed labour, the libre offerings would easily surpass the proprietary offerings.

Heh, this is totally mentioned in DiscCO!

#+begin_quote
The development of [[id:19c72914-a9b8-4dc5-a4ec-758655015c3e][commons-public partnerships]] as an alternative to the established public-private, following the Preston and Evergreen models, with DisCOs facilitating:

a. Municipal service provision for local economies.
b. FLOSS-built and open licensed, common-pool civic knowledge resources.
c. Practical workplace education on feminist economics, the commons, decentralized technology and the ethical market sectors offered by participating DisCOs.

-- [[id:130c45dd-dbec-4cba-b3af-3a426aedf3a7][The DisCO Elements]]
#+end_quote

* What kinds of software does a municipality need?

  - [[id:a2bcafe7-e390-455b-bf92-dcef1fe6501d][citizen participation platform]]s
  - maybe a search engine that favours local business / news / organisations?  And doesn't track. e.g. a custom [[id:19a27213-8906-49d1-8f26-7de7798a58da][Searx]] of some kind

 General civic tech stuff. A ton of resource management stuff presumably.  
    
**    See commercial offerings...
    - [[https://www.councilwise.com.au/][CouncilWise]]
      - finance; asset management; property and rating; records management; citizen services
    - [[https://www.edgeitsystems.com/advantedge/][AdvantEDGE]] (management solutions for town and parish councils)
      - Admin (agendas and minutes)
      - Allotments
      - Asset management
      - Cemetery management
      - Facilities
      - Finance
      - Inspections
      - Planning
      - Playgrounds
      - Services

* Bookmarks
    - [[https://opensource.com/article/20/8/linux-government][How a local government migrated to open source | Opensource.com]]
     
