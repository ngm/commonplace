:PROPERTIES:
:ID:       a2f3ed90-0e35-45f9-b82b-0cff49b6ba90
:mtime:    20211127120922 20210724222235
:ctime:    20210724222235
:END:
#+title: There are different strands of cybernetics
#+CREATED: [2021-05-09 Sun 11:03]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Interesting to learn about the different strands of [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]] from [[id:d7825ec8-401a-462f-a3c9-21875c252f1f][Cybernetic Revolutionaries]].

There is/(was?) a kind of top-down militaristic command and control strain (Wiener-ish?), and a more holistic, distributed complex systems strain (Beer-ish?).
