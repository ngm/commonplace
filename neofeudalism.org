:PROPERTIES:
:ID:       84b88c67-00e0-4616-be50-a1d74419bbbd
:mtime:    20231125003514 20211127120905 20210724222235
:ctime:    20210724222235
:END:
#+title: Neofeudalism
#+CREATED: [2020-12-05 Sat 11:55]
#+LAST_MODIFIED: [2023-11-25 Sat 00:35]

#+begin_quote
Over the past decade, “neofeudalism” has emerged to name tendencies associated with extreme inequality, generalized precarity, monopoly power, and changes at the level of the state.

-- [[id:32cecf94-c813-49ba-9f15-35bc20d2c5af][Neofeudalism: The End of Capitalism?]]
#+end_quote

- Extreme inequality
- Monopoly power
- Generalized precarity
- Changes at the level of the state
