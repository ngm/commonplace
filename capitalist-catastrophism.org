:PROPERTIES:
:ID:       3311c88a-59b8-42ce-9010-2bc81c38c051
:mtime:    20220923101610
:ctime:    20220923101610
:END:
#+TITLE: Capitalist Catastrophism
#+CREATED: [2022-09-23 Fri]
#+LAST_MODIFIED: [2022-09-23 Fri 10:20]

+ Author :: [[id:ddb998c8-abf9-47a0-b666-e17e441a7e6f][Kai Heron]]

.
+ [[id:d701985a-67fe-48a3-8658-578511ec4008][Capitalist Realism]]
+ [[id:d00fd4f7-1036-41c9-9b77-1a5728c44cab][End of history]]
+ Capitalist catastrophism is a mutation of capitalist realism.

#+begin_quote
Unless there is a radical break from capitalism — a revolution — what will supplant capitalist realism is not the ability to imagine and fight for a post-capitalist future as Mason, Uetricht and Milburn had hoped, but something more ambiguous and perhaps ultimately worse. I call this something worse “capitalist catastrophism.”
#+end_quote

#+begin_quote
Capitalist catastrophism is what happens when capitalist realism begins to fray at the edges. It describes a situation in which capitalism can no longer determine what it means to be “realistic,” not because of the force of movements assembled against it but because capital’s self-undermining and ecologically destructive dynamics have outstripped capitalism’s powers to control them.
#+end_quote
