:PROPERTIES:
:ID:       e2c44e61-5c0a-456d-83ea-af8f1f9ed89e
:END:
#+title: Iterative Inquiry

A method for [[id:e044a716-096c-438e-b69a-9a2f177d781b][systems thinking]]
from [[id:ac147134-ce80-4027-ad74-53dbf8306388][Design Journeys through Complex Systems]].
