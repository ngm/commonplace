:PROPERTIES:
:ID:       0e1618fe-3960-4a83-85d4-247e8384be7f
:mtime:    20240520102614 20240519112303 20231025213202 20231015181907 20230923171314 20230722100404 20230610152704 20230507103621 20230410172634 20221210191334 20221121171952
:ctime:    20221121171952
:END:
#+TITLE: Ecosocialism
#+CREATED: [2022-11-21 Mon]
#+LAST_MODIFIED: [2024-05-20 Mon 10:30]

[[id:e3e8baa3-60c8-4c99-bfb0-0ff8b43b5d8c][I am an ecosocialist]].

* What is ecosocialism?

A combo of [[id:1574c602-e123-4f31-afc4-31b92e874e49][socialism]] with [[id:e8d69e81-3f02-4785-9fd1-353d8d3a5ea3][green politics]], [[id:9bdc71a4-aed8-44eb-920a-01968a5550fb][ecology]] and [[id:3311a70e-783d-45e6-8d5d-90c4759562f0][alter-globalisation]].
A baseline of fulfillment of human needs while staying within [[id:92d55d0f-2a21-4d82-a475-89becc54d3aa][planetary boundaries]]. Ecosocialism sees [[id:1d843238-d48a-4095-9324-d22d4e140063][social and environmental issues as interconnected and inseparable]]. Ecosocialism is [[id:86858785-24ab-4b45-83fd-0570404a7283][anti-capitalist]], seeing capitalism as the root cause of both social inequity and the overshoot of planetary boundaries.

#+begin_quote
Loosely defined, ecosocialism describes a classless socioeconomic system in which humans live in balance with nature.

-- [[id:4f2eb92a-67cf-406a-9198-2e4d8129bcd4][Jackson Rising Redux]]
#+end_quote

#+begin_quote
an ideology merging aspects of socialism with that of green politics, ecology and alter-globalization or anti-globalization.

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

- [[id:64e5b991-269d-4676-afeb-aa3a4039dc3a][Capitalism is the root cause of social inequity]].
- [[id:5d422c9b-0172-41df-8e83-39f6a6f9ec9a][Capitalism is the root cause of the overshoot of planetary boundaries]].


* What is the political economy of ecosocialism?

#+begin_quote
market-based solutions to ecological crises (ecological economics, environmental economics, green economy) are rejected as technical tweaks that do not confront capitalism's structural failures.

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

What is its [[id:40cd8dcb-9f30-4011-b2b8-a3d3be94371e][political economy]] then?

#+begin_quote
Eco-socialists advocate dismantling capitalism, focusing on common ownership of the means of production by [[id:11cbc6ff-f5d8-498a-a140-c89f17ab5694][freely associated producers]], and [[id:679c2c25-8db1-46d4-a940-3357cd30c32a][restoring the commons]].

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

[[id:0cb7cb18-cbb2-4060-bf7b-4ef6497dc375][Commons]].

What's the *specifically* eco bit?

#+begin_quote
Exchange value would be subordinated to use value by organiszing production primarily to meet social needs and the requirements of enviornmental protection and ecological regeneration.

-- [[id:4f2eb92a-67cf-406a-9198-2e4d8129bcd4][Jackson Rising Redux]]
#+end_quote

#+begin_quote
To build an ecologically rational society along these lines requires the collective ownership of the means of production, democratic planning to enable society to define the goals of production and invemestment, and a new technological production structure that meshes with society's plans and stays within the Earth's ecological carrying capacity.

-- [[id:4f2eb92a-67cf-406a-9198-2e4d8129bcd4][Jackson Rising Redux]]
#+end_quote

* Various flavours of ecosocialism

There are various [[id:457f02dd-2549-48b6-aff1-0ed59431293f][types of ecosocialism]].

#+begin_quote
The discursive terrain of ecosocialist thinking is vast, and there is no single way to define ecosocialism. However, we can identify three broad principles that would likely be shared across these varieties: 1) the priority of use-value over exchange-value; 2) collective ownership and planning to shape and constrain markets; and 3) “contraction and convergence” in consumption levels between the global north and south

-- [[id:bc44751c-0a2a-40a8-9ca9-973824d246cc][Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]
#+end_quote

#+begin_quote
But at core these movements share a commitment to struggling for a socioecological transition beyond capitalism by democratizing the means of production, subjecting markets to more ecologically rational planning, and subordinating private profit to social use-value and ecocentric production.

-- [[id:bc44751c-0a2a-40a8-9ca9-973824d246cc][Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]
#+end_quote

* Why not just eco?

#+begin_quote
One might argue that the climate situation is so serious, we should prioritise it over everything; if we don’t get this right there’s no room for any human society, equitable or otherwise.  But then, I would say, the changeover to sustainability is an enormous undertaking, requiring the energies of society as a whole: hence we must therefore address the disempowerment of the vast majority.  This is why I proceed from a ‘red-green’ perspective, which is socially radical as well as environmentally conscious.

-- [[id:0289a45d-68bd-47c0-932e-468d3b97ab7c][How systems theory can help us reflect on the world]]
#+end_quote

* Governance

- participatory planning
- democratic control of resources
- collective ownership and management of the means of production
- decentralisation and regional self-sufficiency

* History

#+begin_quote
[[id:47483b89-ec0c-4ced-bc5b-cbbb502417c9][William Morris]], the English novelist, poet and designer, is largely credited with developing key principles of what was later called eco-socialism

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

#+begin_quote
The "pre-revolutionary environmental movement", encouraged by the revolutionary scientist Aleksandr Bogdanov and the Proletkul't organisation, made efforts to "integrate production with natural laws and limits" in the first decade of Soviet rule, before Joseph Stalin attacked ecologists and the science of ecology and the Soviet Union fell into the pseudo-science of the state biologist Trofim Lysenko, who "set about to rearrange the Russian map" in ignorance of environmental limits

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

* Future

#+begin_quote
The eco-socialist future is female

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
