:PROPERTIES:
:ID:       5b32eaa9-8810-4aab-97f8-687c11e9335e
:mtime:    20220219164834
:ctime:    20220219164834
:END:
#+TITLE: Admins, mods, and benevolent dictators for life: The implicit feudalism of online communities
#+CREATED: [2022-02-19 Sat]
#+LAST_MODIFIED: [2022-02-19 Sat 16:56]

+ URL :: https://osf.io/gxu3a/?view_only=11c9e93011df4865951f2056a64f5938
+ DOI :: https://doi.org/10.1177%2F1461444820986553
+ Author :: [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]]

[[id:853dd384-0544-4745-9eab-58b46356f61e][implicit feudalism]].
[[id:8086d14e-26ee-40bb-8706-0e2277944a5f][Feudalism]].

#+begin_quote
By implicit I mean that while platforms may not explicitly proclaim or seek to practice feudal ideology—to the contrary, many claim democratic ideals—a feudalism lurks latent in the available tools that guide and limit user behavior
#+end_quote

#+begin_quote
I do not use feudalism in a historically precise sense, as there is much to distinguish online communities from the medieval European regime of land tenancy and its lord-vassal relations. Rather, I use the word metaphorically to describe concurrent communities across a network, each subject to a power structure that is apparently absolute and unalterable by those who lack such power.
#+end_quote
