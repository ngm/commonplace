:PROPERTIES:
:ID:       5c53f22c-683b-4496-ba97-6bacbfcb4d3b
:mtime:    20220904103051
:ctime:    20220904103051
:END:
#+TITLE: systems mapping
#+CREATED: [2022-09-04 Sun]
#+LAST_MODIFIED: [2022-09-04 Sun 10:46]

#+begin_quote
Broadly speaking, systems mapping is the creation of visual depictions of a system, such as its relationships and feedback loops, actors and trends. Systems mapping is intended to provide a simplified conceptual understanding of a [[id:d201b97a-3067-4a7c-adb3-61d0fd08932d][complex system]] that, for collective action purposes, can get partners on the same page

-- [[https://rmi.org/systems-mapping-a-vital-ingredient-for-successful-partnerships/][Systems Mapping: A Vital Ingredient for Successful Partnerships - RMI]] 
#+end_quote

[[id:e044a716-096c-438e-b69a-9a2f177d781b][Systems thinking]].
