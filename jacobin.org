:PROPERTIES:
:ID:       4c8422ed-6330-49dd-97da-e9e7d80a5ce5
:mtime:    20220829105324 20220531204146
:ctime:    20220531204146
:END:
#+TITLE: Jacobin
#+CREATED: [2022-05-31 Tue]
#+LAST_MODIFIED: [2022-08-29 Mon 10:53]

* Jacobin Club

A [[id:7a07c0c9-7746-41dc-b3ff-6dc891bd8f97][political club]] in the [[id:deca4bbf-e7ce-4f12-b970-e1ca3f4f8383][French Revolution]].
