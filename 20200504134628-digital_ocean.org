:PROPERTIES:
:ID:       4b918007-1bb5-4231-9d69-8c77280e2efd
:mtime:    20211127120834 20210724222235
:ctime:    20200504134628
:END:
#+TITLE: Digital Ocean

* ssh access stopped working to a server
  
- Server was initially setup with ssh key access only, no root password.
- All of a sudden getting Permission denied (publickey)
- What happened?
- Creating a copy droplet from backup is very handy.  I did that, and was able to log in fine.  It happened to be from the day before.  So something on the server end, not my end.
- Has the user been banned?
- Has it been hacked?  Has it gotten corrupted somehow?
- As far as I can tell, fail2ban only bans users for 10minutes.  So, it's not that.  Something must have buggered with the ssh access.

  to try: 

- [ ] reset password
- [ ] reboot
- [ ] check website OK
- [ ] if not, log in with new password and restart services

Resetting the root password was very quick.  Does it reboot completely?  Everything was all started up again straight after.

OK, so my IP address was banned!  And for more than 10 minutes?

Not sure why banned.

Wrong key on the server??? One of my other keys?? Makes no sense!!!  Very confused as to what happened.
