:PROPERTIES:
:ID:       0351dcc0-ae9c-4de3-b212-3efe250d1f88
:mtime:    20220318113153
:ctime:    20220318113153
:END:
#+TITLE: Labour markets must be socialized
#+CREATED: [2022-03-18 Fri]
#+LAST_MODIFIED: [2022-03-18 Fri 11:37]

#+begin_quote
There are basically two strategies for [[id:1202a2ab-11b0-493d-bc68-18409c6a248c][de-marketing labor]], and they work best together.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
The first is the aggressive encouragement of [[id:21f4ca5f-b0b2-4b3c-86cb-da206ab7ee08][worker co-ops]], including buying out (not bailing out) failing firms and leasing them to workers, giving workers the right to buy out their shops as an alternative to closure, and providing public financial and technological support for start-up co-ops. *The more workers can become owners, the less they’ll have to work for wages, thus shrinking the labor market.*

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
The second strategy is to provide the option to exit the job market by filling out the [[id:04d7101b-af58-456b-83a2-f38cf8093b79][welfare state]]: public health care, education, and “last resort” guaranteed employment, capped off with a [[id:1977fafc-17f9-4bcb-b591-2eea79dd2e81][basic income]] to subsidize culture- and community-production. The ability to survive without submitting to the dictates of the job market would incapacitate the capitalist imperative to compete with everybody else.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote
