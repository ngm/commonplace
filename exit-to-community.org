:PROPERTIES:
:ID:       6e0eb5f0-5506-4d64-a6ea-4c205f38fb3e
:mtime:    20211127120852 20211109113932
:ctime:    20211109113932
:END:
#+title: Exit to community

#+begin_quote
Giving startup businesses an alternative to selling out to private investors or nasty big companies ; they can users or a local community to purchase them.

-- David Bollier, Stir to Action Issue 30
#+end_quote

- [[id:ce976166-fa88-48dc-90cd-7378f32c5b0d][Open Collective]]: [[https://blog.opencollective.com/exit-to-community/][Early musings on "Exit to Community" for Open Collective]]
