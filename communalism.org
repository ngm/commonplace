:PROPERTIES:
:ID:       4129c1a1-caae-43c1-a8ed-a11b7248f6f9
:mtime:    20220702133208 20211127120829 20210724222235
:ctime:    20200719181553
:END:
#+title: Communalism

A vision and political strategy, chief proponent [[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Murray Bookchin]].

[[id:47c9474c-9954-48f8-9950-de52a4325c5a][Directly democratic]], [[id:f3c496db-5884-4c22-99d4-60c0fe9166bd][anticapitalist]], [[id:9bdc71a4-aed8-44eb-920a-01968a5550fb][ecological]], and opposed to domination.  Governance through [[id:0ff426cb-44c4-4662-8c27-749b386c149d][popular assemblies]] bound together in confederation.

#+begin_quote
While originally conceived as a form of Social anarchism, he later developed Communalism into a separate ideology which incorporates what he saw as the most beneficial elements of Anarchism, Marxism, syndicalism, and radical ecology.

-- [[https://en.wikipedia.org/wiki/Eco-socialism][Eco-socialism - Wikipedia]] 
#+end_quote

#+begin_quote
The excitement and solidarity on the ground has yet to coalesce into a political praxis capable of eliminating the current array of repressive forces and replacing it with a visionary, egalitarian—and importantly, achievable—new society. [[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Murray Bookchin]] directly addresses this need, offering a transformative vision and new political strategy for a truly free society—a project that he called “Communalism.”

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
[[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Bookchin]]’s Communalism circumvents the stalemate between the state and the street—the familiar oscillation between empowering but ephemeral street protest and entering the very state institutions designed to uphold the present order.

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
Communalism moves beyond critique to offer a reconstructive vision of a fundamentally different society—directly democratic, [[id:f3c496db-5884-4c22-99d4-60c0fe9166bd][anticapitalist]], [[id:9bdc71a4-aed8-44eb-920a-01968a5550fb][ecological]], and opposed to all forms of domination—that actualizes freedom in popular assemblies bound together in confederation. Rescuing the revolutionary project from the taint of authoritarianism and the supposed “end of history,” Communalism advances a bold politics that moves from resistance to social transformation.

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
Communalist politics suggests a way out of the familiar deadlock between the anarchist and Marxist traditions

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
Bookchin instead returns to the recurrent formation arising in nearly every revolutionary upsurge: [[id:0ff426cb-44c4-4662-8c27-749b386c149d][popular assemblies]]

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
As [[id:8932abbe-de62-4538-b7cc-fd3068991576][David Harvey]] observed in his book [[id:79b21be2-4b51-4569-a1c0-35ec54c2e6fe][Rebel Cities]], “Bookchin’s proposal is by far the most sophisticated radical proposal to deal with the creation and collective use of the commons across a wide variety of scales.”

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote

#+begin_quote
From Marxism, it draws the basic project of formulating a rationally systematic and coherent socialism that integrates philosophy, history, economics, and politics. Avowedly dialectical, it attempts to infuse theory with practice. From anarchism, it draws its commitment to antistatism and confederalism, as well as its recognition that hierarchy is a basic problem that can be overcome only by a libertarian socialist society.

   -- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote

#+begin_quote
Communalism constitutes a critique of hierarchical and capitalist society as a whole. It seeks to alter not only the political life of society but also its economic life. On this score, its aim is not to nationalize the economy or retain private ownership of the means of production but to municipalize the economy.

-- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote

#+begin_quote
Finally, Communalism, in contrast to anarchism, decidedly calls for decision-making by majority voting as the only equitable way for a large number of people to make decisions. Authentic anarchists claim that this principle—the “rule” of the minority by the majority—is authoritarian and propose instead to make decisions by consensus. [[id:774c6cc4-8322-4914-be48-6772e810e706][Consensus]], in which single individuals can veto majority decisions, threatens to abolish society as such. 

-- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote

#+begin_quote
Communalists would see their program and practice as a process. Indeed, a transitional program in which each new demand provides the springboard for escalating demands that lead toward more radical and eventually revolutionary demands

-- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote

#+begin_quote
Significantly, Communalists do not hesitate to run candidates in municipal elections who, if elected, would use what real power their offices confer to legislate [[id:0ff426cb-44c4-4662-8c27-749b386c149d][popular assemblies]] into existence.

-- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote
