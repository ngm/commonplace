:PROPERTIES:
:ID:       951b206c-d417-455d-98c2-67bb80be81c1
:mtime:    20220522192448
:ctime:    20220522192448
:END:
#+TITLE: energy development
#+CREATED: [2022-05-22 Sun]
#+LAST_MODIFIED: [2022-05-22 Sun 19:25]

#+begin_quote
Energy development is the field of activities focused on obtaining sources of energy from natural resources. These activities include production of [[id:2f0cc071-0606-495f-8194-535d09cae40c][renewable]], nuclear, and fossil fuel derived sources of energy, and for the recovery and reuse of energy that would otherwise be wasted.

-- Wikipedia
#+end_quote
