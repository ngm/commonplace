:PROPERTIES:
:ID:       965af473-d198-400b-8407-dd007a7f381b
:mtime:    20211127120914 20211104200856
:ctime:    20211104200856
:END:
#+TITLE: Industrialisation
#+CREATED: [2021-11-04 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
To many people there seemed to be a direct, one-to-one relationship between technological advances and social progress; a fetishism of the word "industrialization" excused the most abusive of economic plans and programs

-- [[id:5edca101-03d7-4ef1-a921-7b3167176c09][Towards a Liberatory Technology]]
#+end_quote
