:PROPERTIES:
:ID:       0917c9c7-034c-4e05-ba76-12ebdd106813
:mtime:    20231201181411
:ctime:    20231201181411
:END:
#+TITLE: org-mode inline src blocks not publishing
#+CREATED: [2023-12-01 Fri]
#+LAST_MODIFIED: [2023-12-01 Fri 18:14]

I'd like to know why the outputs of my little bits of executable code blocks aren't getting correctly generated when my garden gets published to the web.

e.g. I have one on [[id:a2ad6cbd-9a8f-48cc-b574-698d7cbe0711][Neil's Digital Garden]] that's supposed to list how many nodes in the garden.

#+begin_example
As of src_sh[:results raw]{date +"%A, %B %e, %Y"}, I have src_bash{echo -n $(sqlite3 ./org-roam.db "select count(*) from nodes")} nodes in my garden.
#+end_example

They work if I run them locally, just not during the remote build on my server.

I have ~(setq org-confirm-babel-evaluate nil)~ which means no prompting to confirm evaluation should occur.

In the logs for index.org I have
"Executing Emacs-Lisp unknown at position 153"
