:PROPERTIES:
:ID:       ceae4ada-14e7-446f-96ee-4ebae2b815c3
:END:
#+title: The Jakarta Method Comes to Latin America (Review)

+ Found at :: https://nacla.org/news/2020/05/27/jakarta-method-review

About the book by [[id:8bca7a96-a290-46c4-9d69-9e07c633ad9a][Vincent Bevins]]

The Jakarta Method.

CIA involvement in brutally violent suppression of communist societies.

#+begin_quote
Bevins shows that the [[id:7631f757-f30f-4aa8-afa4-dfecec8edcbf][Indonesian Communist Party]] resisted calls from [[id:cb63d675-00d5-4659-8f55-b9fc1a709a55][Mao]] and others to arm and prepare for insurrectionary resistance. The mass murder in Indonesia not only annihilated the communist movement in the country—it served as a warning to other leftist movements about the dangers of remaining unarmed
#+end_quote
