:PROPERTIES:
:ID:       879c7ddc-c823-41f5-ac63-f286b61d603a
:mtime:    20211127120906 20210724222235
:ctime:    20210724222235
:END:
#+title: Smoke tests
#+CREATED: [2021-02-23 Tue 12:53]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Quick run through of main features to check things look OK.

#+begin_quote
In computer programming and software testing, smoke testing (also confidence testing, sanity testing,[1] build verification test (BVT)[2][3][4] and build acceptance test) is preliminary testing to reveal simple failures severe enough to, for example, reject a prospective software release. Smoke tests are a subset of test cases that cover the most important functionality of a component or system, used to aid assessment of whether main functions of the software appear to work correctly.
#+end_quote
