:PROPERTIES:
:ID:       4c6b73d3-df53-4cb7-9c9a-43d6e0903c3f
:mtime:    20211127120834 20210724222235
:ctime:    20210724222235
:END:
#+title: Strategy
#+CREATED: [2021-07-10 Sat 21:59]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
strategy is concerned with both what is happening in the moment inside the organisation and how best to regulate it to achieve set goals as well as what is happening outside in the external environment and with respect to the possible futures of the organisation.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote
