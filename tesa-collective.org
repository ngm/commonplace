:PROPERTIES:
:ID:       f421f2ac-4b20-444f-9b08-bd88bcf9d998
:mtime:    20211127121012 20210724222235
:ctime:    20210724222235
:END:
#+title: Tesa Collective
#+CREATED: [2021-04-27 Tue 21:24]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

https://store.tesacollective.com/

[[id:21f4ca5f-b0b2-4b3c-86cb-da206ab7ee08][Worker cooperatives]] that make board games for "games for changing the world".
