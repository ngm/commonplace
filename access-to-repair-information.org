:PROPERTIES:
:ID:       855fc3c9-d96d-46f9-be15-71f9225bf7c1
:mtime:    20230922174043 20220402094813
:ctime:    20220402094813
:END:
#+TITLE: access to repair information
#+CREATED: [2022-04-02 Sat]
#+LAST_MODIFIED: [2023-09-22 Fri 17:40]

[[id:2fb5bdb2-1001-4ea3-a9cc-0203bbd0153f][Access]] to [[id:d94f0c52-8367-453b-a61d-5915e837b570][repair information]].

"the information necessary to repair"

To everyone, including consumers, non profit repair initiatives and independent repairers.

Promote safe self repair by requiring manufacturers to provide consumers with adequate information on product disassembly and reassembly.
