:PROPERTIES:
:ID:       5fe249b4-7286-425b-af9a-a33eda8c4c1c
:mtime:    20211127120844 20210911161236
:ctime:    20210911161236
:END:
#+TITLE: Moloch
#+CREATED: [2021-09-05 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

* a Biblical thing

#+begin_quote
is a name or a term which appears in the Hebrew Bible several times, primarily in the book of Leviticus. The Bible strongly condemns practices which are associated with Moloch, practices which appear to have included child sacrifice.

-- [[https://en.wikipedia.org/wiki/Moloch][Moloch - Wikipedia]] 
#+end_quote
  
* a poem by [[id:9db53d58-569b-4d4d-b897-dcc60d2e4c04][Allen Ginsberg]] (part of [[id:240e7d27-c285-472b-bbb5-22000977ada0][Howl]])
 
#+begin_quote
'Moloch' has been figuratively used in reference to a person or a thing which demands or requires a very costly sacrifice.

-- [[https://en.wikipedia.org/wiki/Moloch][Moloch - Wikipedia]] 
#+end_quote

#+begin_quote
sphinx of cement and aluminum
#+end_quote

Something has been sacrified to Moloch.  Modern materials but an old god.

#+DOWNLOADED: https://static.tvtropes.org/pmwiki/pub/images/nightmare-factory_metropolis_8423.png @ 2021-09-11 16:10:58
[[file:images/moloch.png]]

Moloch in [[roam:Metropolis]].

#+begin_quote
whose mind is pure machinery!
#+end_quote

Anti-technology?

#+begin_quote
whose blood is running money!
#+end_quote

[[id:86858785-24ab-4b45-83fd-0570404a7283][Anti-capitalist]]?

#+begin_quote
whose fingers are ten armies!
#+end_quote

Anti-war?

There's a lot of exclamation marks and angry energy.

#+begin_quote
Moloch who entered my soul early!
#+end_quote

[[id:82c173bb-89d5-4350-975a-6072338b5f64][Hegemony]].

#+begin_quote
Moloch who frightened me out of my natural ecstasy!
#+end_quote

Yearning for nature?

#+begin_quote
Moloch whom I abandon! Wake up in Moloch! Light streaming out of the sky!
#+end_quote

^ [[id:fc18dd57-b043-4ad4-b62e-4e7d62588dbe][The Matrix]] vibes.

After a first read through, I'm thinking Ginsberg was seriously angry about over-consumption and technology in the pursuit of money, at the expense of nature (or something more 'natural').  I don't know who he thinks has sold out and made this sacrifice.  People in general?  He himself maybe.  I should read the other parts.

** Words to describe Moloch

 + Solitude!
 + Filth!
 + Ugliness!
 + Nightmare
 + Loveless
 + Heavy judger
 + Incomprehensible prison
 + Crossbone soulless jailhouse
 + Congress of sorrows
 + etc
