:PROPERTIES:
:ID:       5ec25e66-451c-458a-885b-472db157efd1
:mtime:    20220601222906 20220601205345 20211127120844 20210724222235
:ctime:    20210724222235
:END:
#+title: Logic Magazine
#+CREATED: [2021-04-03 Sat 10:04]
#+LAST_MODIFIED: [2022-06-01 Wed 22:29]


* Themes

#+begin_quote
Like you, we are both insiders and outsiders. Luckily, this is exactly the position you need to be in to observe and describe a system. In the social sciences they call it “logic”: the rules that govern how groups of people operate. For engineers, “logic” refers to the rules that govern paths to a goal. In the vernacular, “logic” means something like being reasonable. Logic Magazine believes that we are living in times of great peril and possibility. We want to ask the right questions. How do the tools work? Who finances and builds them, and how are they used? Whom do they enrich, and whom do they impoverish? What futures do they make feasible, and which ones do they foreclose?

We’re not looking for answers. We’re looking for logic.

[[https://logicmag.io/intelligence/disruption-a-manifesto/][Disruption: A Manifesto]]
#+end_quote

* Pitching

https://logicmag.io/pitch-us/
