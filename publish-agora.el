;; just an experiment at the moment.
;; see https://commonplace.doubleloop.net/ox-agora

(require 'package)

(package-initialize)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;(unless package-archive-contents
  (package-refresh-contents);)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package htmlize)
(use-package org-roam
  :init
  (setq org-roam-v2-ack t))
(use-package ox-hugo) ; needed for ox-blackfriday in ox-agora.el
(use-package s)

; https://gitlab.com/ngm/ox-agora
(load "~/.emacs.d/private/ox-agora/ox-agora.el")
; https://gitlab.com/ngm/commonplace-lib/
(load "~/.emacs.d/private/commonplace-lib/commonplace-lib.el")

(require 'ox-publish)
(require 'org-roam)
(require 's)
(require 'ox-agora)
(require 'find-lisp)
(require 'commonplace-lib)

(defun get-title (file)
  "For a given file, get its TITLE keyword."
  (with-current-buffer
      (get-file-buffer file)
    (cadar (org-collect-keywords '("TITLE")))))

(defun commonplace/slugify-export-output-file-name (output-file)
  "Gets the title of the org file and uses this (slugified) for the output filename.
This is mainly to override org-roam's default filename convention of `timestamp-title_of_your_note` which doesn't work well with Agora."
  (let* ((title (get-title (buffer-file-name (buffer-base-buffer))))
         (directory (file-name-directory output-file))
         (slug (commonplace/slugify-title title)))
    (concat directory slug ".md")))

(advice-add 'org-export-output-file-name :filter-return #'commonplace/slugify-export-output-file-name)

; https://github.com/alphapapa/unpackaged.el#export-to-html-with-useful-anchors
(defun unpackaged/org-export-get-reference (datum info)
  "Like `org-export-get-reference', except uses heading titles instead of random numbers."
  (let ((cache (plist-get info :internal-references)))
    (or (car (rassq datum cache))
        (let* ((crossrefs (plist-get info :crossrefs))
               (cells (org-export-search-cells datum))
               ;; Preserve any pre-existing association between
               ;; a search cell and a reference, i.e., when some
               ;; previously published document referenced a location
               ;; within current file (see
               ;; `org-publish-resolve-external-link').
               ;;
               ;; However, there is no guarantee that search cells are
               ;; unique, e.g., there might be duplicate custom ID or
               ;; two headings with the same title in the file.
               ;;
               ;; As a consequence, before re-using any reference to
               ;; an element or object, we check that it doesn't refer
               ;; to a previous element or object.
               (new (or (cl-some
                         (lambda (cell)
                           (let ((stored (cdr (assoc cell crossrefs))))
                             (when stored
                               (let ((old (org-export-format-reference stored)))
                                 (and (not (assoc old cache)) stored)))))
                         cells)
                        (when (org-element-property :raw-value datum)
                          ;; Heading with a title
                          (unpackaged/org-export-new-title-reference datum cache))
                        ;; NOTE: This probably breaks some Org Export
                        ;; feature, but if it does what I need, fine.
                        (org-export-format-reference
                         (org-export-new-reference cache))))
               (reference-string new))
          ;; Cache contains both data already associated to
          ;; a reference and in-use internal references, so as to make
          ;; unique references.
          (dolist (cell cells) (push (cons cell new) cache))
          ;; Retain a direct association between reference string and
          ;; DATUM since (1) not every object or element can be given
          ;; a search cell (2) it permits quick lookup.
          (push (cons reference-string datum) cache)
          (plist-put info :internal-references cache)
          reference-string))))

(defun unpackaged/org-export-new-title-reference (datum cache)
  "Return new reference for DATUM that is unique in CACHE."
  (cl-macrolet ((inc-suffixf (place)
                             `(progn
                                (string-match (rx bos
                                                  (minimal-match (group (1+ anything)))
                                                  (optional "--" (group (1+ digit)))
                                                  eos)
                                              ,place)
                                ;; HACK: `s1' instead of a gensym.
                                (-let* (((s1 suffix) (list (match-string 1 ,place)
                                                           (match-string 2 ,place)))
                                        (suffix (if suffix
                                                    (string-to-number suffix)
                                                  0)))
                                  (setf ,place (format "%s--%s" s1 (cl-incf suffix)))))))
    (let* ((title (org-element-property :raw-value datum))
           (ref (url-hexify-string (substring-no-properties title)))
           (parent (org-element-property :parent datum)))
      (while (--any (equal ref (car it))
                    cache)
        ;; Title not unique: make it so.
        (if parent
            ;; Append ancestor title.
            (setf title (concat (org-element-property :raw-value parent)
                                "--" title)
                  ref (url-hexify-string (substring-no-properties title))
                  parent (org-element-property :parent parent))
          ;; No more ancestors: add and increment a number.
          (inc-suffixf ref)))
      ref)))

(advice-add #'org-export-get-reference :override #'unpackaged/org-export-get-reference)

(defun commonplace/configure (project-dir publish-dir make-sitemap)
  (setq commonplace/project-dir project-dir)
  (commonplace/configure-org-publish project-dir publish-dir make-sitemap)

  ;; for babeling
  (with-eval-after-load 'org
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((sqlite . t)
       (shell . t))))

  ;; this is important - otherwise org-roam--org-roam-file-p doesnt work.
  (setq org-roam-directory project-dir)
  ;; to be able to find id links during publish
  (setq org-id-extra-files (find-lisp-find-files org-roam-directory "\.org$"))
  (setq org-roam-db-location (concat project-dir "/org-roam.db")))

(defun commonplace/agora-configure-local ()
  (interactive)
  (commonplace/configure "/home/neil/commonplace" "/home/neil/commonplace-agora" nil))

(defun commonplace/publish-for-agora-local ()
  (commonplace/configure "/home/neil/commonplace" "/home/neil/commonplace-agora" nil)
  (call-interactively 'org-publish-all))

(defun commonplace/publish-for-agora-remote ()
  (setq org-confirm-babel-evaluate nil)
  (setq org-publish-list-skipped-files nil)
  (commonplace/configure (file-truename ".") "../commonplace-agora" nil)
  (org-roam-db-sync t)

  (advice-add 'org-publish :around #'org-publish-ignore-mode-hooks)

  ; current-prefix-arg is to force republish.
	(let ((current-prefix-arg 4))
    (rassq-delete-all 'web-mode auto-mode-alist)
    (fset 'web-mode (symbol-function 'fundamental-mode))
    (call-interactively 'org-publish-all)))

(defun commonplace/configure-org-publish (project-dir publish-dir make-sitemap)
  (setq org-publish-project-alist
        `(("commonplace"
           :components ("commonplace-notes" "commonplace-static"))
          ("commonplace-notes"
           :base-directory ,project-dir
           :base-extension "org"
           :publishing-directory ,publish-dir
           :publishing-function org-agora-publish-to-agora
           :recursive t
           :headline-levels 4
           :with-toc nil
           :exclude "node_modules\\|recentchanges.org")
          ("commonplace-static"
           :base-directory ,project-dir
           :base-extension "png\\|jpg\\|gif\\|svg\\|svg\\|pdf"
           :publishing-directory ,publish-dir
           :exclude "node_modules"
           :recursive t
           :publishing-function org-publish-attachment))))
