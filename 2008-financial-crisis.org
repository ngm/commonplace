:PROPERTIES:
:ID:       1ec31179-dd24-4436-be2a-7e03778566bd
:mtime:    20220212224135
:ctime:    20220212224135
:END:
#+TITLE: 2008 financial crisis
#+CREATED: [2022-02-12 Sat]
#+LAST_MODIFIED: [2022-02-12 Sat 22:42]

#+begin_quote
The world economic crisis that started in 2008 was the result of decades of [[id:f4a0bc4b-8b79-42e8-8c6e-561244d3c6b7][neoliberal capitalism]].

-- [[id:7b19a0e3-0ab5-4fc0-a7e7-eea5f901acd0][Social Media: A Critical Introduction]]
#+end_quote

