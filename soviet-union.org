:PROPERTIES:
:ID:       6a118895-9bdc-4027-a8b2-2cab1f48e562
:mtime:    20220326161749 20220326120201 20220316194939 20220114202626
:ctime:    20220114202626
:END:
#+TITLE: Soviet Union
#+CREATED: [2022-01-14 Fri]
#+LAST_MODIFIED: [2022-03-26 Sat 16:24]

Union of Soviet Socialist Republics. [[id:a0815245-f892-4899-be12-428a94a35255][1922]] - [[id:75dd6857-062e-4813-80a8-b482cd1da84e][1991]].

Comprised of [[id:4a96c83a-2c11-4701-b243-cafae95c0ea8][Russia]], Estonia, Latvia, Lithuania, Belarus, Ukraine, Moldova, Georgia, Armenia, Azerbaijan, Kazakhstan, Uzbekistan, Turkmenistan, Kyrgyzstan, Tajikistan.

#+begin_quote
But that system died at the end of [[id:75dd6857-062e-4813-80a8-b482cd1da84e][1991]], replacing a world of [[id:9ba3630e-7003-407f-8f85-c5859bbd856b][shortages]] (full pockets, empty shops) with a world of poverty and [[id:b568e6bc-1a14-441c-a580-64415b2be6ad][hyperinflation]] (empty pockets, full shops) overnight

-- [[https://www.theguardian.com/books/2010/aug/08/red-plenty-francis-spufford][Red Plenty by Francis Spufford | Fiction | The Guardian]]
#+end_quote

#+begin_quote
We realised there were saboteurs and enemies among us, and we caught them, but it drove us mad for a while, and for a while we were seeing enemies and saboteurs everywhere, and hurting people who were brothers, sisters, good friends, honest comrades. Then the Fascists came, and stamping on them was bloody, nobody could call what we did then sweetness and light, wreckage everywhere, but what are you going to do when a gang of murderers breaks into the house? And the Boss didn’t help much. Wonderful clear mind, but by that time he was frankly screwy, moving whole nations round the map like chess pieces, making us sit up all night with him and drink that filthy vodka till we couldn’t see straight, and always watching us: no, I don’t deny we went wrong, in fact if you recall it was me that said so. But all the while we were building. All the while we were building factories and mines, railroads and roads, towns and cities, and all without any help, all without getting the say-so from any millionaire or bigshot. We did that. We taught people to read, we taught them to love culture. We sent tens of millions of them to school and millions of them to college, so they could have the advantages we never had. We created the boys and girls who’re young now. *We did the dirty work so they could inherit a clean world*.

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

^ [[id:1a42cfcb-98be-4254-a3a5-218551cb946f][Khrushchev]]'s words. Reminds me a bit of [[id:a4da160e-d766-452a-bc7b-054a01de8959][To Posterity - Bertolt Brecht]].

#+begin_quote
Some comrades seemed to think that fine words and fine ideas were all the world would ever require, that pure enthusiasm would carry humanity forward to happiness: well excuse me, comrades, but aren’t we supposed to be materialists? Aren’t we supposed to be the ones who get along without fairytales? If communism couldn’t give people a better life than capitalism, he personally couldn’t see the point

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

[[id:1a42cfcb-98be-4254-a3a5-218551cb946f][Khrushchev]] again.

#+begin_quote
Politics gave the orders, in the economy of the USSR, and economists were allowed to find reasons why the orders already given were admirable

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote
