:PROPERTIES:
:ID:       ca4c2277-c977-4ffd-93cd-1ce5c1e618a0
:mtime:    20220526114727 20211127120947 20210724222235
:ctime:    20200719135938
:END:
#+title: care work

#+begin_quote
Jobs that are often done by women, so-called ‘pink-collar’ jobs in health and education, not only represent some of the strongest segments of the labour movement today but also foreshadow the shift to a zero-carbon economy that prioritizes care work over extractive labour.

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
