:PROPERTIES:
:ID:       987068d5-f90a-4de9-96ad-c2fa66313b14
:mtime:    20211127120916 20210724222235
:ctime:    20201003113235
:END:
#+title: Open Media Network

A reboot of [[id:222ff150-64a5-430a-a2ef-5c3272c91825][Indymedia]], using current technologies and learning from mistakes.

#+begin_quote
The news part of the Fediverse

- [[https://conf.tube/videos/watch/953de898-74dc-4665-95fb-313042f66cc6][The reboot of the indymedia project - ConfTube]] 
#+end_quote
