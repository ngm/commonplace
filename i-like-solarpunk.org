:PROPERTIES:
:ID:       82f6c74b-4436-4e67-875c-69f54f815de2
:mtime:    20220108214257
:ctime:    20220108214257
:END:
#+TITLE: I like solarpunk
#+CREATED: [2022-01-08 Sat]
#+LAST_MODIFIED: [2022-01-08 Sat 21:45]

[[id:2f5935ea-a42a-49c4-b0e0-b4f178be79c0][I like]] [[id:c2eee50e-313d-49a0-9688-f4ada05c4182][solarpunk]].

* Because

+ [[id:b22a3a44-4077-4801-8443-fe4cdb9bf56d][Solarpunk is about technologies that de-abstract human relationships with the material world]]
+ ...

* Epistemic status

+ Type :: [[id:c4b32a97-6206-4649-8218-39c25526c3a8][feeling]]
+ Strength :: 6

I definitely do, I just haven't figured out yet how much it's a fad, a trend, a lifestyle thing, how much it can be genuine praxis.

I think it's more than an aesthetic.  Though to be fair if it's just an aesthetic that's fine, so long as it doesn't *claim* to be more.

