:PROPERTIES:
:ID:       fae438e7-8766-4938-8626-6aea161ba38d
:mtime:    20211127121016 20210724222235
:ctime:    20200712202108
:END:
#+title: peer-to-peer networks

#+begin_quote
In peer-to-peer networks, we ‘seed’ for one another. We spread our bytes like weeds, criss-crossing fenced-in property and open meadows alike, in our effort to grow digital abundance.

-- [[id:d5d03a8b-5788-494a-8781-473b10babeaf][Seeding the Wild]]
#+end_quote

#+begin_quote
the peer-to-peer model turns addicted ‘users’ into active peers, giving people more control over the systems they use.

-- [[id:d5d03a8b-5788-494a-8781-473b10babeaf][Seeding the Wild]]
#+end_quote

#+begin_quote
Peer-to-peer applications cannot be monetised in the same way traditional client-server applications can. 

-- [[id:d5d03a8b-5788-494a-8781-473b10babeaf][Seeding the Wild]]
#+end_quote

^ why not?
