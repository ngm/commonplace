:PROPERTIES:
:ID:       cdfacefa-8f06-431b-97b5-15dab032c508
:mtime:    20220727181445 20220726204300 20220531211317 20211127120949 20210724222235
:ctime:    20201024080347
:END:
#+title: Angela Davis

[[id:7c273007-864c-4d2d-acf8-578f8a7f97c0][Feminist]], [[id:1e6dbe7e-7463-4506-9288-cdc18aead1d8][Marxist]], [[id:eea96993-f380-4c96-ad5a-7ee04add0985][activist]].

#+begin_quote
[[id:e65b15cc-7e36-478b-a354-8eb8aa2c339f][Marcuse]]’s most famous student

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
not only a tireless advocate of the great utopian project of today – [[id:1a42fbbb-9b95-48ac-a1e2-0959945aaf95][prison abolition]] – but also a [[id:86659224-9876-4041-83c6-46fd646fc8ac][vegan]]

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

