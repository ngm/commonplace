:PROPERTIES:
:ID:       cf664375-dfb7-4c73-a2a5-33b6b24765f6
:mtime:    20211127120950 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-09-21

* Finished: A Closed and Common Orbit
  
I finished [[id:3c81cfeb-73c4-4800-92d6-4905576d21be][A Closed and Common Orbit]] by [[id:561262ce-b075-4b76-a8a1-86c4997b53bc][Becky Chambers]] recently.  I loved it.  There's something very lovely about Becky Chambers' sci-fi - it's much more touching and emotional than a lot that I've read.  I felt quite emotional at the ending of this one - it's so full of heart and compassion.

* WikiWeb and Roam multiplayer

Roam is now [[https://twitter.com/Conaw/status/1307107745397604354][talking about 'multiplayer']]. I'm presuming this means linking between individuals' wikis.  I like the idea of [[id:ffd70794-e498-4e5e-a74c-e8351dc0538a][interlinking wikis]], but imagining it's going to be pretty closed in Roam.  

Bill Seitz has lots of ideas about a more open [[http://webseitz.fluxent.com/wiki/WikiWeb][WikiWeb]]. 

#+begin_quote
aka Wiki Sphere; parallel to BlogWeb, the universe of (clusters of) WikiSpaces
#+end_quote


