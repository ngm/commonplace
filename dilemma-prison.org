:PROPERTIES:
:ID:       f3b61e02-9e94-40a2-b626-26c4620293a4
:mtime:    20220101165357
:ctime:    20220101165357
:END:
#+TITLE: Dilemma Prison
#+CREATED: [2022-01-01 Sat]
#+LAST_MODIFIED: [2022-01-01 Sat 16:54]

From [[id:84ddc129-b9cc-4069-966b-b2b1afd37004][The Quantum Thief]] - a prison that puts the inmates through the [[id:70b09d08-0856-46bd-8cb3-cb60b2337807][iterated prisoner's dilemma]], until they learn to cooperate. 
