:PROPERTIES:
:ID:       5503f8ab-eedb-4522-ae1d-7d6a6ad909c6
:mtime:    20220807161200 20220807095327 20220806192953
:ctime:    20220806192953
:END:
#+TITLE: How Degrowth Will Save the World with Jason Hickel
#+CREATED: [2022-08-06 Sat]
#+LAST_MODIFIED: [2022-08-07 Sun 16:19]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.upstreampodcast.org/conversations
+ Featuring :: [[id:9b7c7b5b-4a6b-4d06-85ad-c8642172932c][Jason Hickel]]

Great interview. Challenging the idea that economies need to continuously grow.  Hickel calls that outlook [[id:5859b765-2844-4c2e-b3ab-04fbe84ab963][growthism]].  Believes we need [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][degrowth]].  Presents a comprehensive view of the problems and solutions that seems to go beyond just how you might think of the debate on 'degrowth'.  See [[id:242ce2c9-d80f-452e-8cf3-3c6587967a79][Less is More]] for more.

~00:00:00  What do we want the economy to actually achieve?

~00:06:31  The inputs to production are labour and nature.

~00:06:53  [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][Capitalism]] must take more from [[id:76c3d510-e8b8-4d3c-a66f-eb2b34a903d6][labour]] and [[id:e24422de-7415-4779-a757-5ef731d166bd][nature]] than it gives back. This is its core logic.

~00:13:04  Capitalism and growth is like an aeroplane that has to keep accelerating in order to stay in the air.

~00:21:01  Productivity gains should result in shorter working week.

~00:30:36  Describes [[id:32cbb06f-0bbd-4ad7-a116-6260f3e1bbe0][green growth]], degrowth, [[id:02bb9d07-8804-4a42-90ac-cf1e79907f37][post-growth]], [[id:d5efa178-57a7-4b84-9097-80e953712220][steady-state economy]].

~00:40:40 Jason likes [[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]].  DE presents itself as somewhat growth-agnostic, and Kate Raworth doesn't like the term degrowth.  He says that where degrowth comes in is that developed countries need degrowth to get back in the doughnut.  Developing countries need growth to get into the doughnut.  Then, in both cases, the aim would be to stay steady within the doughnut.

~00:39:34  GDP does not bring happiness or other useful social outcomes.

~00:40:45  [[id:7e288212-b1fc-4116-81f2-e2e6c6c88e2b][Universal basic services]] and distribution of income more fairly are the key drivers of positive social outcomes. Also democratic equality.

~00:43:14  [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][Right to repair]] as part of degrowth.  Good general bunch of policy proposals.

~00:50:11  Upstream, you can trace a lot of problems back to [[id:e1d03516-7c22-465e-a88b-d9331e68bed2][dualism]] and a mechanistic rather than an animistic worldview.  [[id:5f6ff8ab-baaf-4c9d-8c37-c2619ac28586][Animism]].

