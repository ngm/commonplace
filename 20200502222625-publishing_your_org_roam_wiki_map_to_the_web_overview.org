:PROPERTIES:
:ID:       b8e81bcf-7704-49e7-b412-29177b0f285e
:mtime:    20211127120936 20210724222235
:ctime:    20200502222625
:END:
#+TITLE: Publishing your org-roam wiki map to the web: overview

I want my wiki to be a [[id:98923c43-0c65-4cee-98c9-f48a9dc50b73][sensemaking]] aid - after collecting the dots, to help me then [[id:053ee741-a4e1-4021-a992-6a326734603c][connect them]] and see the [[id:00ac9b2b-f466-48d2-9967-8133e874c14c][constellations]].

One sensemaking tool in Roam and [[id:20210326T232003.148801][org-roam]] is the [[id:c5b2143a-75b9-438a-a02f-b158909305b5][wiki graph]] - a graph (in the nodes and edges sense) of all of the notes in your wiki and their links to each other.  Not quite a mind map.  More of a hypertext map.  I've been playing around with publishing this map to my public site.

#+ATTR_HTML: :width 100%
[[file:org-roam_graph_publish_web_version/2020-05-02_22-08-47_screenshot.png]]

* Does it make sense?

While fun, I'm not yet 100% convinced with these simple graphs as sensemaking aids.  The full map is pretty messy (org-roam's at least, I haven't seen Roam's).  From browsing around it, I don't feel that enlightened about what I'm thinking.  It *did* make me realise I have a few orphaned pages, but you could do that pretty easily with just a list.

That said, it definitely is interesting browsing around it - I come across pages I forgot about, and do see the beginnings of clusters of thoughts.  I think visualisation, when done right, could possibly bring unexpected insights, and if nothing else, provide an alternative way of navigating around.  These graphs are not there yet, but worth experimenting with.

* Publishing org-roam's map to the web

So anyway, with all that, for my [[https://indieweb.org/2020/Pop-ups/GardenAndStream][Garden and Stream]] post-session [[https://indieweb.org/2020/Pop-ups/Demos][hack and demo]], I had a bit of a play with org-roam, and got it so that I can publish my wiki graph to my site, and hacked it so that you can click each node in the graph to take you to the corresponding page.

You can find it [[https://commonplace.doubleloop.net/graph.svg][here]], and there's also a link back to it at the bottom of every page.

A couple of ways I would improve it if I carry on with it:

- make it more visually appealing and navigable 
- break it up into a map per page, so you get a local map, too.  To be seen whether this is a useful sensemaking aid or not.  Maybe it would just be another useful navigational aid.

I'll make a separate post shortly with the technical details of how I got it to work (I'm experimenting a bit with [[id:04845f29-d619-4459-aa3f-4b89c5b88c48][separating]] the overview from the technical with these things).
