:PROPERTIES:
:ID:       613bdb50-f958-410e-80e2-277c517a630e
:mtime:    20220726185100
:ctime:    20220726185100
:END:
#+TITLE: An Essay on Liberation
#+CREATED: [2022-07-26 Tue]
#+LAST_MODIFIED: [2022-07-26 Tue 18:52]

+ URL ::  https://www.marxists.org/reference/archive/marcuse/works/1969/essay-liberation.htm
+ Author :: [[id:e65b15cc-7e36-478b-a354-8eb8aa2c339f][Herbert Marcuse]]

First of all saying that simply just the alienation of the working class, the proletariat was not enough.

Right now, it's not enough to bring about revolution, plenty of ways in which still alienated.  Culture industry maybe. This is from the Frankfurt School, so probably something related to that. 

One was any kind of response has to be rhizomatic, you have a narrative that speaks to a number of different elements within the working class.

So, a big part of that kind of revolution is not it's not enough kind of depression and grief. That's something along the lines of changing people's sensibilities has to do with.

Yes, some of the awareness raising was through arts and creativity. This changing the biology people to think differently.

Finally, solidarity.
