:PROPERTIES:
:ID:       7d93efd1-4a45-4fd4-8acb-20d8961e5279
:mtime:    20211127120900 20210724222235
:ctime:    20210724222235
:END:
#+title: Metabase
#+CREATED: [2021-02-18 Thu 10:03]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

- https://www.metabase.com/

We use Metabase for ad-hoc business analytics.

It's open-source and very handy.

Always worth evaluating alternatives though:

- [[https://www.holistics.io/blog/metabase-limitations-and-top-alternatives-bi/][Metabase Limitations and Top 4 Alternatives (BI)]]
