:PROPERTIES:
:ID:       06aa3744-682c-4a2a-a437-0df373c7ebef
:mtime:    20230924110649 20230821212535 20211127120756 20211110213105
:ctime:    20211110213105
:END:
#+TITLE: #ACFM
#+CREATED: [2021-11-10 Wed]
#+LAST_MODIFIED: [2023-09-24 Sun 11:06]

+ A :: [[id:6ec161cb-6746-4bdb-8648-47ae56cd45d4][podcast series]]
+ URL :: https://novaramedia.com/category/audio/acfm/

#+begin_quote
The home of the weird left. [[id:7cecfe3c-f363-4242-9981-05c7dd4b8746][Nadia Idle]], [[id:50764f9f-fdb4-4538-bc71-95ed7e36d8ed][Jeremy Gilbert]] and [[id:101619b4-b87b-4771-aa2b-14d52edf62c1][Keir Milburn]] examine the links between left-wing politics, culture, music and experiences of collective joy.
#+end_quote

I really enjoy #ACFM podcasts.  They take fairly everyday things and look at them through a leftist lens, and throw a bit of music in too.  They seem to like [[id:9250b263-a25e-4d6c-996c-3bcfd65cbce4][municipal socialism]].
