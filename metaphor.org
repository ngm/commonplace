:PROPERTIES:
:ID:       e58e2540-f36e-43d6-811a-41b7500a7543
:mtime:    20220718203449
:ctime:    20220718203449
:END:
#+TITLE: Metaphor
#+CREATED: [2022-07-18 Mon]
#+LAST_MODIFIED: [2022-07-18 Mon 20:35]

#+begin_quote
Metaphor is a conceptual device without any real-world existence, the value of which might be measured by the purpose it serves – in other words, it has instrumental rather than intrinsic value.

-- [[id:f9e7173e-4659-4d10-859d-161c412b7989][Nature Matters: Systems thinking and experts]]
#+end_quote
