:PROPERTIES:
:ID:       c6c16385-52bc-4413-918f-06b44e63f82d
:mtime:    20220120211832
:ctime:    20220120211832
:END:
#+TITLE: Community washing
#+CREATED: [2022-01-20 Thu]
#+LAST_MODIFIED: [2022-01-20 Thu 21:41]

Handy phrase for the bullshit Uber, Airbnb do/did around pretending they facilitate community relations.

First heard on [[id:0ba91345-9289-47d0-a120-2be0c8e27135][Platform Socialism and Web3]].
