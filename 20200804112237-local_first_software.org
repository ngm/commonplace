:PROPERTIES:
:ID:       90c87855-2880-4c1e-bf3f-d80d087837a5
:mtime:    20211127120911 20210724222235
:ctime:    20200804112237
:END:
#+title: Local-first software

See: [[https://www.inkandswitch.com/local-first.html][Local-first software: You own your data, in spite of the cloud]].

Local-first software:

#+begin_quote
prioritizes the use of local storage (the disk built into your computer) and local networks (such as your home WiFi) over servers in remote datacenters.
#+end_quote

#+begin_quote
The data synchronization need not necessarily go via the Internet: local-first apps could also use Bluetooth or local WiFi to sync data to nearby devices
#+end_quote

* Why?

- [[id:2d9a3fbe-b88d-481e-ade3-70f5c6fb93f2][Cloud software has benefits]], but [[id:1b47a1ae-73b7-41d4-93d1-3666db6df222][Cloud apps are problematic]].  
- [[id:6122eb2b-bc8b-4378-84e4-bf169069fea5][Centralised applications are authoritarian]].  
- Local-first software gives you agency and ownership of your data.  
- [[id:71f4ba5f-bc73-4837-8216-5a53185d88d0][You shouldn't need permission to access your own files]].
- [[id:36e4fcde-2666-4864-98dd-30d4105aff45][Servers should provide a supporting role, not a central role]].

#+begin_quote
Live collaboration between computers without Internet access feels like magic in a world that has come to depend on centralized APIs.
#+end_quote

* Challenges

#+begin_quote
In local-first apps, our ideal is to support real-time collaboration that is on par with the best cloud apps today, or better. Achieving this goal is one of the biggest challenges in realizing local-first software, but we believe it is possible:
#+end_quote

#+begin_quote
The developers of mobile apps are generally experts in end-user app development, not in distributed systems. We have seen multiple app development teams writing their own ad-hoc diffing, merging, and conflict resolution algorithms, and the resulting data sync solutions are often unreliable and brittle.
#+end_quote


* Examples

#+begin_quote
This is the case because a Git repository on your local filesystem is a primary copy of the data, and is not subordinate to any server.
#+end_quote

