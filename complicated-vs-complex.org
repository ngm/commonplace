:PROPERTIES:
:ID:       c62c4292-85c2-4f77-8af2-5e56c360e8dc
:mtime:    20211224174726
:ctime:    20211224174726
:END:
#+TITLE: Complicated vs complex
#+CREATED: [2021-12-24 Fri]
#+LAST_MODIFIED: [2021-12-24 Fri 17:48]

#+begin_quote
These reciprocal feedback loops are not just [[id:a2236149-b7bc-434c-8895-4f19f7df3e55][complicated]]—they're also [[id:c85b1f24-3cc6-4595-9781-a6b7e006ba2f][complex]]. In everyday language, we tend to use these two words interchangeably, but, in the world of [[id:529cfe2d-8a59-41a3-8015-db13e863226e][systems theory]], they're very different. A system can be complicated but not complex, no matter how large, if each of its components and the way they relate to each other can be completely analyzed and given an exact description. A jumbo jet, an offshore oil rig, and a snowflake are all examples of complicated systems. A complex system, on the other hand, arises from a large number of nonlinear relationships between its components with feedback loops that can never be precisely described. Any living thing, or system comprising living things, is complex: a bacterium, a brain, an ecosystem, a financial market, a language, or a social system.

-- [[id:2db61895-7843-4f76-a591-1d579a7c9545][The Patterning Instinct]]
#+end_quote
