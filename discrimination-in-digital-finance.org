:PROPERTIES:
:ID:       1886f1ff-2d93-48dd-80b2-26294a5cc1a6
:mtime:    20211127120807 20211021204814
:ctime:    20211021204814
:END:
#+TITLE: Discrimination in digital finance
#+CREATED: [2021-10-21 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

I don't know much about [[id:2e4d8f16-c5e3-468b-ae2a-d1273eb3978f][digital finance]].  My first thoughts are that it refers to [[id:5997aec1-99d7-4793-b1e5-2cdf3229beb1][cryptocurrency]] but it's probably more than that.

In general what I've seen of fintech (admittedly not that much), it comes across as brotech and not [[id:5f449b36-4f96-468c-a39c-bcdc12868443][Liberatory technology]].  I wonder if there is [[id:6b982d1b-e0f6-4a03-b7ee-fac24c8cc76b][liberatory fintech]]. 

I'll read [[id:e5ff6c38-37b3-497b-80ab-b18d7184f6e3][vera]]'s notes on this node to get started...

They mention cashapp and venmo too - I don't know too much about these.

Does online banking count as digital finance?  I can't remember the last time I went in to an actual branch of my nominally physical bank to be honest.

Added some definitions: [[id:2e4d8f16-c5e3-468b-ae2a-d1273eb3978f][digital finance]].

But yeah in general seems to have a wide range of description, from online banking to distributed ledgers.

How does it discriminate? One obvious act of discrimination I guess is the exclusion of those who aren't comfortable in the use of digital technology.  People may prefer to interact in person in a real branch, not fuck around with shitty bank websites.

On the other end, stuff like [[id:e5b7bbac-3bf7-4618-a713-24f319761b3e][bitcoin]] say, you need to be pretty tech savvy to get started with it.  And to mine it in the first place, you have to have capital for hardware.
