:PROPERTIES:
:ID:       2303cd69-7ddf-4398-b9d2-e695933b7358
:mtime:    20211127120812 20210724222235
:ctime:    20210724222235
:END:
#+title: Consul
#+CREATED: [2021-04-24 Sat 16:26]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

https://consulproject.org/en/

#+begin_quote
Free software for citizen participation.
#+end_quote

Grew out of Decide Madrid, I think.

+ Debates
+ Proposals
+ [[id:04c1c125-b6a3-4269-a1ec-309f1ab074a2][Participatory budgeting]]
+ Voting
+ [[id:8d3e22e6-bc4f-4617-ac07-3872f2dcabf9][Collaborative legislation]]

  #+begin_quote
  Decide Madrid is an open source civic tech platform developed by the Madrid city council in 2015 and licensed under AFFERO GPL 3. It received the UN's prestigious public service award in 2018 and has been implemented in more than 100 cities, including Paris, New York, and Porto Alegre, Brazil. The platform was designed to be as easy to use as possible.

--  [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
  #+end_quote

  #+begin_quote
  Madrid's implementation is unique because it gives executive powers to its citizens: any proposal that receives 1% of citizens' votes is automatically placed on the government's agenda. Because of this, Decide Madrid remains one of the most consequential open source projects to date and an important experiment in combining representative and direct democracy in the context of entrenched disenchantment with democracy.
  
-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
  #+end_quote
  
#+begin_quote
A better indicator of a civic platform's success than participation numbers might be its direct effects. In this context, that is how many citizen proposals received enough votes to be sent to the city council. Here, the picture is less rosy: only two citizen proposals were voted onto the agenda. The conversion rate between visits on the site and voting for proposals was too low.

-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
#+end_quote
