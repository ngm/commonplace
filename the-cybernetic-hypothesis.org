:PROPERTIES:
:ID:       e7603dc6-eb62-4415-8bc5-7dee24dc3bf1
:mtime:    20211127121005 20210724222235
:ctime:    20210724222235
:END:
#+title: The Cybernetic Hypothesis
#+CREATED: [2021-06-25 Fri 22:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

[[id:14a34560-c4f6-4ce8-9896-b47c7ffa06cc][Tiqqun]].

An [[id:c59fb5a0-da7a-4e01-9295-2a038231bab7][anarchist]] critique of [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]].

* Links
  
- [[http://www.richard-hall.org/2017/07/07/notes-on-the-cybernetic-hypothesis/][notes on the cybernetic hypothesis | Richard Hall's Space]]
