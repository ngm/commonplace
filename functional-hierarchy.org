:PROPERTIES:
:ID:       5c68f419-da2a-4af5-9b24-15dd7491f7aa
:mtime:    20211127120843 20210724222235
:ctime:    20210724222235
:END:
#+title: Functional hierarchy
#+CREATED: [2021-07-10 Sat 22:37]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
[[id:c7aee726-2e3a-4b2b-985f-77f5d525ec7d][Occupy Wall Street]] illustrates one way in which a functional hierarchy can be maintained in an organisation which aims to have a nonhierarchical (in terms of structural or anatomical hierarchy) structure.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

#+begin_quote
According to McEwan, the concept of anatomical hierarchy refers to what we normally understand by the term ‘hierarchy’: different levels in an organisation with a chain of command running between then and with lower levels subordinate to higher ones. This is hierarchy within the structure or anatomy of an organisation, hence McEwan calling it anatomical. Functional hierarchy, on the other hand, concerns a situation where ‘there are two or more levels of information structure operating in the system’

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

