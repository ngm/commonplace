:PROPERTIES:
:ID:       6e8c2e1c-5e53-4062-86c4-2dde869dc78c
:mtime:    20211127120852 20210724222235
:ctime:    20200508100509
:END:
#+TITLE: Invidious

- https://github.com/omarroth/invidious

This is a lightweight web wrapper around YT. It also is libre software, so there are multiple hosted instances of it, and you could host it yourself if you wanted. It has a bunch of nice features, such as no ads, audio-only mode, doesn’t require JS, and no tracking.

* Installation notes

Deb 10.

- https://github.com/omarroth/invidious/issues/881
- 
