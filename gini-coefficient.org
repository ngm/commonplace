:PROPERTIES:
:ID:       810a61da-eb8a-448a-bf02-7d3e11d7a208
:mtime:    20220830211053
:ctime:    20220830211053
:END:
#+TITLE: Gini coefficient
#+CREATED: [2022-08-30 Tue]
#+LAST_MODIFIED: [2022-08-30 Tue 21:11]

#+begin_quote
Gini coefficient, devised by the Italian sociologist Corrado Gini in 1912, is a measure of income or wealth disparity in a population

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote

Has problems.

#+begin_quote
The Gini figures for Bangladesh and for Holland are nearly the same, for instance, at 0.31; but the average annual income in Bangladesh is about $2,000, while in Holland it’s $50,000.

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote

#+begin_quote
While discussing inequality, it should be noted that the Gini coefficient for the whole world’s population is higher than for any individual country’s, basically because there are so many more poor people in the world than there are rich ones, so that cumulatively, globally, the number rises to around 0.7.

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote
