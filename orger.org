:PROPERTIES:
:ID:       f5cad8f1-db48-473a-9b1c-9f50012ecb81
:mtime:    20211127121013 20210724222235
:ctime:    20210724222235
:END:
#+title: orger
#+CREATED: [2021-04-02 Fri 10:54]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

#+begin_quote
Mirror your personal data as org-mode for instant access and search 
#+end_quote

+ https://github.com/karlicoss/orger
+ https://beepb00p.xyz/orger.html
