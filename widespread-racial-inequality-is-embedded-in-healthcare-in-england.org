:PROPERTIES:
:ID:       d4e944a2-22d1-4366-a67f-c6cc468498e4
:mtime:    20220216182031
:ctime:    20220216182031
:END:
#+TITLE: Widespread racial inequality is embedded in healthcare in England
#+CREATED: [2022-02-16 Wed]
#+LAST_MODIFIED: [2022-02-16 Wed 18:23]

Widespread [[id:c59ba162-f2dd-4b4e-8f13-a75f3d46ebcf][racial inequality]] is embedded in [[id:2a0fef7a-5ef4-4b06-8a90-b2c044280cb8][healthcare in England]]

https://www.theguardian.com/society/2022/feb/13/radical-action-needed-to-tackle-racial-health-inequality-in-nhs-says-damning-report

#+begin_quote
“ *Ethnic inequalities in health outcomes are evident at every stage throughout the life course, from birth to death*,” says the review, the largest of its kind. Yet despite “clear”, “convincing” and “persistent” evidence that ethnic minorities are being failed, and repeated pledges of action, no “significant change” has yet been made in the NHS, it adds.

-- [[https://www.theguardian.com/society/2022/feb/13/radical-action-needed-to-tackle-racial-health-inequality-in-nhs-says-damning-report][Radical action needed to tackle racial health inequality in NHS, says damning...]]
#+end_quote

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]

