:PROPERTIES:
:ID:       6e31407d-632a-4486-99aa-8e423fc3371f
:mtime:    20241102164514
:ctime:    20241102164507
:END:
#+title: making it easier to open the session drawer in Termux in right-handed one-handed mode

[[id:32be7697-0e4f-4bf9-9426-38dcfb15306a][Termux]] has a session drawer - a place to switch between open terminal sessions. You access it by swiping right from the left of the screen.

I was finding it hard to open the session drawer when using the phone in right-handed one-handed mode.

Termux has this notion of extra-keys - a layer of extra keys useful for operations in a terminal.

Helpfully, the keys in that layer are configurable, and also helpfully, one special key that you can add is DRAWER, which opens up the session drawer.

The extra keys layer is already quite full of keys, and I couldn't figure which one to get rid of. So helpfully again, you can stack keys on top of each other, accessible with a swipe up from the base extra key.

You need to edit ~/termux/termux.properties.

Here's my config for that below:

#+begin_src
### Two rows with more keys
extra-keys = [['ESC','/','-','HOME','UP','END', {key: PGUP, popup: ESC}], \                     ['TAB','CTRL','ALT','LEFT','DOWN','RIGHT', {key: PGDN, popup:DRAWER}]]
#+end_src

I've added DRAWER to PGUP, and
I've also added ESC as a swipe from PGUP too, to get easier access to that too.

* References

- https://github.com/termux/termux-app/issues/1325
