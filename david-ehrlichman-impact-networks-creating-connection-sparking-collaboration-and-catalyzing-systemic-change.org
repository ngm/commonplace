:PROPERTIES:
:ID:       b78606b1-369a-49ff-99a2-a20db612a2b3
:mtime:    20220821110035 20220820231659
:ctime:    20220820231659
:END:
#+TITLE: David Ehrlichman, "Impact Networks: Creating Connection, Sparking Collaboration, and Catalyzing Systemic Change"
#+CREATED: [2022-08-20 Sat]
#+LAST_MODIFIED: [2022-08-21 Sun 11:07]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://newbooksnetwork.com/impact-networks

.

+ Praises [[id:3f7e9252-25c2-4171-9e57-e359ee16059e][Donella Meadows]] and her work on where to intervene in a system.
+ Relationship between networks and systems.
+ Impact networks - it's all about coordination.
+ Networked organisation.  Tension between the parts and the whole.
+ Only through divergence do we find convergence.
+ Planning for emergence. (Perhaps relates to Social tipping points?)
+ [[id:d162ad23-1179-4df8-842c-33bd04fdccc8][Network weaving]].
+ Infusing energy in a network.
+ Pockets of possibility.
+ Building a network is like cultivating a garden.
+ Creating systemic change is a marathon not a sprint.
+ David is working in the Web3 / DAO space lately.  He sees it as a new system to challenge the old.
+ One option is to focus locally, grow pockets of possibility, try to weave networks.  Web3 can give tools and infrastructure to allow people to coordinate at larger scales than before.  Trustless systems - coordinating when we don't know anything about each other.
+ Some of it feels a bit incrementalist.

 #+begin_quote
 Solving complex problems like climate change or homelessness demands intense collaboration between diverse organizations and individuals. In his book, David argues that a network approach combines the strategic rigor and agility of modern organizations with the deep connection and shared purpose of communities.
 #+end_quote

#+begin_quote
Drawing on his experience working with over fifty impact networks over the past decade, David describes how to cultivate a network mentality. He then goes deeply into the five Cs of creating impact networks: * clarify purpose and principles * convene the people * cultivate trust * coordinate actions * collaborate for systems change. Given the increasing urgency of the issues we face, impact networks have never been more essential.
#+end_quote

#+begin_quote
What I love about this book—and what I enjoyed so much about our conversation—is the opportunity for exploring the potential of human networks (and networks of networks!) to bring about significant systemic change. On the relationship between systems and networks, David writes that "the networks that underlie systems—organizational, social, planetary—have a huge influence on how healthy and effective these systems are". I enjoyed getting to ask David about his thoughts on network leadership—and what it means for this work to be grounded in the wisdom of living systems. 
#+end_quote
