:PROPERTIES:
:ID:       2fc3b32c-658e-455d-9bbc-3b64b0ab7904
:mtime:    20211127120819 20210724222235
:ctime:    20200411204455
:END:
#+TITLE: Github

#+begin_quote
While git is a distributed version control system, GitHub created a centralised collaboration platform around it, with such popularity that for many people they have become synonymous. People manage their software projects on GitHub, no longer just because it is a helpful platform, or because it is gratis, but also because they effectively have to be there for the rest of the developer world to discover and contribute to their project — a network lock-in, like with other social networks

-- [[https://redecentralize.org/redigest/2020/03][Redecentralize Digest — March 2020 — Redecentralize.org]] 
#+end_quote
