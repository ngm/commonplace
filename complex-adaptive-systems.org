:PROPERTIES:
:ID:       ff07e5fd-e5c2-4de3-9464-6751aecb3189
:mtime:    20211127121018 20210825111447
:ctime:    20210825111447
:END:
#+TITLE: complex adaptive systems
#+CREATED: [2021-08-25 Wed]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

I did a Masters degree in [[id:0b43d410-b347-4e06-851c-ce9a2abde212][Evolutionary and adaptive systems]] which included complex adaptive systems.

These days I am very interested in their application to leftist politics.  [[id:e2f662e0-b532-4f7e-9b19-96ab2d2dbfd4][Complexity and the left]].

#+begin_quote
Complex adaptive systems are special cases of [[id:d201b97a-3067-4a7c-adb3-61d0fd08932d][complex systems]] that are adaptive in that they have the capacity to change and learn from experience. Examples of complex adaptive systems include the stock market, social insect and ant colonies, the biosphere and the ecosystem, the brain and the immune system, the cell and the developing embryo, the cities, manufacturing businesses and any human social group-based endeavor in a cultural and social system such as political parties or communities.

-- [[https://en.wikipedia.org/wiki/Complex_system][Complex system - Wikipedia]] 
#+end_quote

