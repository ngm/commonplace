:PROPERTIES:
:ID:       f572eb4c-5bcd-4d2b-a315-1355a21c68f6
:mtime:    20211127121013 20210724222235
:ctime:    20200516174221
:END:
#+TITLE: Better Call Saul

I've been really enjoying Better Call Saul lately (early 2020).  I didn't get into it that much when it first aired - maybe it was too soon after having just finished [[id:6324e19c-844c-4d71-af24-e36c1dcfb904][Breaking Bad]], and it didn't seem as good.  But with a bit of time having passed, I'm really into it.  

It's cool to get some of the back history filled of the characters from Breaking Bad.  I don't think it's /quite/ as well scripted and shot etc as well as Breaking Bad.  But that's a tough comparison to meet, and it's pretty good.
