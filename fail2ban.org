:PROPERTIES:
:ID:       abed1e84-d47c-4e4a-b3cc-941c24599f2f
:mtime:    20211127120928 20210724222235
:ctime:    20210724222235
:END:
#+title: fail2ban
#+CREATED: [2021-02-01 Mon 16:32]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

* Checking if someone is in jail

You have multiple jails - the one I'm usually interested in is =sshd=:

#+begin_src shell
sudo fail2ban-client status sshd
#+end_src
  
- [[https://serverfault.com/questions/841183/how-to-show-all-banned-ip-with-fail2ban][How to show all banned IP with fail2ban? - Server Fault]]


* Removing from jail

#+begin_src 
sudo fail2ban-client -i
#+end_src

then in interactive mode:

#+begin_src 
status sshd
set sshd unbanip XXX.XXX.XXX.XXX
#+end_src

- [[https://serverfault.com/a/760324][firewall - How to Unban an IP properly with Fail2Ban - Server Fault]] 
