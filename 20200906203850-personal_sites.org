:PROPERTIES:
:ID:       bd7b6c2b-ebca-4f78-b3d0-25374dc9e38d
:mtime:    20211127120938 20210724222235
:ctime:    20200906203850
:END:
#+title: Personal sites

#+begin_quote
My personal site is a repository for my memories, experiences, feelings, recipes, tips, photos, and more. [...] it is an ever-growing extension of myself that I have total control over, my mirror and memory aid. I want to be able to look back at this when I’m eighty and thank my past self for surfacing things that I otherwise would have forgotten.

--  [[https://piperhaywood.com/on-personal-sites-adios-analytics/][On personal sites, and adios analytics — Piper Haywood]]
#+end_quote

Hmm, reading this and also Amy Hoy's post recently ([[https://stackingthebricks.com/how-blogs-broke-the-web/][How the Blog Broke the Web]]) is making me think a bit different about how I refer to my site(s). Think I'll think of it a bit more as having a [[id:bd7b6c2b-ebca-4f78-b3d0-25374dc9e38d][personal site]], rather than framing it as I have a 'blog' or a 'wiki'.  Both of which are great technologies, but I want to be a little bit freer about how I think about what my home on the web is and how I structure it.
