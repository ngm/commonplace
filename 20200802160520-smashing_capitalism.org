:PROPERTIES:
:ID:       6de37992-0039-4b86-b3cb-a794d7d75e99
:mtime:    20211127120852 20210724222235
:ctime:    20200802160520
:END:
#+title: Smashing capitalism

#+begin_quote
This is the classic revolutionary road to socialism. It assumes a seizure of power by a cadre of radicals, typically achieved by violent means, but also potentially through elections. Its defining element is not so much reliance on revolution, but what happens after — that it suppresses the counterrevolution by force and then rapidly builds new socialist institutions.

-- A Blueprint for Socialism in the Twenty-First Century
#+end_quote

* Pros 
#+begin_quote
periodically there will be intense capitalist economic crises in which the system becomes vulnerable and ruptures become possible.
-- [[https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/][How to Be an Anticapitalist Today]] , Erik Olin Wright
#+end_quote
   
#+begin_quote
The idea that capitalism can be rendered a benign social order in which ordinary people can live flourishing, meaningful lives is ultimately an illusion because, at its core, capitalism is unreformable.

-- [[https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/][How to Be an Anticapitalist Today]] , Erik Olin Wright
#+end_quote

* Cons
#+begin_quote
It is one thing to burn down old institutions; it is quite another to build emancipatory new institutions from the ashes.

-- [[https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/][How to Be an Anticapitalist Today]] , Erik Olin Wright
#+end_quote

#+begin_quote
Give up the fantasy of smashing capitalism. Capitalism is not smashable, at least if you really want to construct an emancipatory future.

-- [[https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/][How to Be an Anticapitalist Today]] , Erik Olin Wright
#+end_quote

Why not?   Idea seems to be that defending such a revolution would be necessarily violent.  Capitalism won't take it lying down.  See: [[id:a4da160e-d766-452a-bc7b-054a01de8959][To Posterity - Bertolt Brecht]]. 

A pro-smash article: [[https://regenerationmag.org/goodbye-revolution/][Goodbye Revolution? - Regeneration Magazine]] 

