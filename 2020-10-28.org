:PROPERTIES:
:ID:       5fc9a5df-4624-41b8-baaf-2d6a2fc8f448
:mtime:    20211127120844 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-10-28

* org-mode to micropub
  
I tracked down the ~org-html-convert-region-html~ method in the org source.  Thusly, I am going to try posting HTML formatted notes from Emacs to my site via my micropub elisp thingy, and have them also syndicate to Mastodon via Bridgy.  
