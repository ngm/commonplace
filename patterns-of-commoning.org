:PROPERTIES:
:ID:       8b29662f-5254-4691-86a8-26d6bf932fd3
:mtime:    20211211172326 20211127120908 20210724222235
:ctime:    20210724222235
:END:
#+title: Patterns of Commoning
#+CREATED: [2021-04-28 Wed 20:27]
#+LAST_MODIFIED: [2021-12-11 Sat 17:23]

It's the name of a book, but I prefer to link to it as the pattners outlined in [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]] - the [[id:de8a6325-ea45-4a30-99e9-b43cae27ffe0][Spheres of commoning]].

+ http://patternsofcommoning.org/
