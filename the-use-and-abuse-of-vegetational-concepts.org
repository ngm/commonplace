:PROPERTIES:
:ID:       ecbb88e5-9843-4ff7-8a97-33d92de1c9db
:mtime:    20211127121008 20210828192411
:ctime:    20210828192411
:END:
#+TITLE: The Use and Abuse of Vegetational Concepts
#+CREATED: [2021-08-28 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

One of the parts of [[id:ac7e3737-91f7-4e4e-b4fc-0ca0032392cc][All Watched Over by Machines of Loving Grace (Adam Curtis)]].
