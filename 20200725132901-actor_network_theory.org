:PROPERTIES:
:ID:       89b79f72-09ab-40ee-9a7c-7cb5d8f29348
:ROAM_ALIASES: ANT
:mtime:    20211127120908 20210724222235
:ctime:    20200725132901
:END:
#+title: Actor-network theory

#+begin_quote
ANT sees agency as a distributed achievement, emerging  from  associations  between human and non-human entities (the actor-network).

-- [[https://rgs-ibg.onlinelibrary.wiley.com/doi/full/10.1111/tran.12117][Assemblage thinking and actor‐network theory: conjunctions, disjunctions, cross‐fertilisations]] 
#+end_quote

[[id:5950aafc-ae52-453e-920e-2a0eb823ea23][Post-structuralism]]?

