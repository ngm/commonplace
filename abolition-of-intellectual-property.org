:PROPERTIES:
:ID:       72e997cd-8326-4b4a-92da-02abd5d84a93
:mtime:    20230508173621
:ctime:    20230508173621
:END:
#+TITLE: abolition of intellectual property
#+CREATED: [2023-05-08 Mon]
#+LAST_MODIFIED: [2023-05-08 Mon 17:36]

See  [[id:326d2c48-0cb2-4f20-a888-b26228e1b9be][We should phase out intellectual property]].

#+begin_quote
Intellectual property, especially in the form of copyrights and patents, give corporations control over knowledge, culture and the code that determines how apps and services work, allowing them to maximize user engagement, privatize innovation and extract data and rents. Economist Dean Bakerestimates that intellectual property rents cost consumers an additional $1 trillion per year compared to what could be obtained on a “free market” without patents or copyright monopolies. Phasing out intellectual property in favor of a commons-based model of sharing knowledge would reduce prices, widen access to and enhance education for all and function as a form of wealth redistribution and reparations to the Global South.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote
