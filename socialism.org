:PROPERTIES:
:ID:       1574c602-e123-4f31-afc4-31b92e874e49
:mtime:    20241111061600 20241102124209 20221027184752 20221024223634 20221024181118 20221018212121 20221016170048 20220830213923 20220526121138 20211127120805 20210724222235
:ctime:    20200308231547
:END:
#+TITLE: Socialism

Socialism is a set of political and economic ideas for how to run society in a fair way.

Socialism's primary goals are an equitable distribution of wealth and resources, and for social rather than private ownership of the means of production.

There are different variations of socialism in practice, for example democratic vs revolutionary, utopian vs scientific. But common elements are usually some form of economic planning, worker self-management and provision of social welfare.

Key points:
- [[id:f6087375-3989-43e4-b021-0861334cc98c][social ownership]] of the [[id:ea8987bf-afa4-4ddb-b021-b33f15180606][means of production]], as opposed to private ownership
- fair distribution of the fruits of labour

I am a socialist.

Plenty of different flavours as to how you achieve a socialist society in practice e.g. [[id:4998a12f-0133-4c39-8be9-65f435119aaf][democratic socialism]], [[id:eb441741-1cd9-44ff-aef4-8b3554a47faf][revolutionary socialism]] ([[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][Anarchism]], [[id:9c3f0897-c60c-44a8-9714-943a2987ab23][Communism]]).

[[id:2961fa2a-b747-400b-a1fc-6fe5825d8e20][Communism, anarchism, socialism]]

#+begin_quote
Marx and Engels saw socialism as the movement for a society that is based on the principles of equality, justice, and solidarity. 

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

Another definion:

#+begin_quote
The socialist goal is to bring about a society free of class divisions.

-- [[id:de0f8ce3-c1ad-44fb-ae8f-1cc0e13fd289][Red-Green Revolution]]
#+end_quote


* Property and production

#+begin_quote
In a socialist system, property is held in common. The means of production are directly controlled by the workers themselves through worker coops, and production is for use and need rather than exchange, profit and accumulation.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote

#+begin_quote
A key characteristic of a socialist society is that the means of production are the common property of the producers

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

* Socialism and communism

#+begin_quote
Marx (1875) spoke of a first and second phase of communism. In the first phase, private property and capital cease to exist and the ownership of the means of production is socialised, but wage-labour, money, the state and exchange continue to exist. In the second phase, wages, wage-labour, money, the state, exchange-value and all forms of alienation cease to exist. But Marx did not call the first phase socialism and the second phase communism. He rather spoke of two stages or phases of communism.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
They distinguish different types of socialism, of which communism is one, while reactionary socialism, bourgeois socialism, and critical-utopian socialism are others.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
In the Manifesto of the Communist Party, Marx and Engels (1848) speak of communism as a type of socialist movement. Besides communism they identify reactionary socialism, bourgeois socialism, and critical-utopian socialism as types of socialism. When one speaks of communism, one therefore means a type of socialism that aims at the abolition of class society and a democratic, worker-controlled economy within a participatory democracy. 

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
socialism, Marxists often use the terms socialism and communism interchangeably. Strictly speaking, socialism is broader than communism.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
In the 19th century, the socialist movement experienced a split between reformist revisionists and revolutionary socialists. On the one side, the revisionists believed in the evolutionary transition to socialism through victories in elections and an automatic breakdown of capitalism. On the other side, revolutionary socialists stressed the importance of class struggle, street action, mass political strikes, and fundamental transformations of society in order to establish a free society. In the Second International (1889-1916), the various factions of socialism were part of one organisation. The Second International collapsed during the First World War. Socialists were split between those who supported the War and those who radically opposed it.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
Marx saw communism as a radical, democratic movement and form of socialism.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
For Marx and Engels, socialism is not an abstract idea, but has as its precondition the development of productive forces and the socialisation of work, is grounded in class antagonisms that it wants to overcome, and can only become real through class struggles that aim at abolishing class society and establishing a classless society.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

#+begin_quote
Socialism’s conditions are class society’s antagonisms, its method is communist class struggle, and its goal is a classless, socialist society.

-- [[id:b989e413-1066-4114-82f1-ca669074494c][Communicative Socialism/Digital Socialism]]
#+end_quote

Common ownership.

* Socialism and ecology

- [[id:14f2c22a-0efd-4ab5-8743-35c501c0691f][Socialism and Ecology]]
- [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][Eco-socialism]]
- [[id:999c7df1-4d1a-4b47-abfb-ae3e0fe284d0][Social ecology]]

* Misc

#+begin_quote
If capitalism is a society characterized by unconscious control, then socialism must be the restoration of human consciousness as a historical force. In practice, this means that the market must be replaced by planning.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
