:PROPERTIES:
:ID:       1520e2e6-0a3e-4e47-b78d-399771b40baa
:mtime:    20211127120805 20210724222235
:ctime:    20200515200839
:END:
#+TITLE: The Goose and the Common

A protest rhyme from the 17th century, decrying the [[id:9ab1a71d-1c09-4fcc-8ec6-4c4d73452949][enclosure of the commons]].

#+begin_verse
They hang the man and flog the woman
Who steals the goose from off the common
Yet let the greater villain loose
That steals the common from the goose
#+end_verse

http://unionsong.com/u765.html


