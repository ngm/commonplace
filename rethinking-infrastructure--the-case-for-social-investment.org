:PROPERTIES:
:ID:       4dea6bc2-af1f-4b01-963b-d4453b558c1b
:mtime:    20211127120835 20210724222235
:ctime:    20210724222235
:END:
#+title: Rethinking infrastructure: the case for social investment
#+CREATED: [2021-04-24 Sat 12:30]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://www.cheerfulpodcast.com/rtbc-episodes/rethinking-infrastructure
+ Publisher :: [[id:bb2beb1f-e125-4fa6-97b5-0909a0effd46][Reasons to be Cheerful]]

#+begin_quote
President [[id:744e12b2-c778-47ff-be72-e096ac235be7][Joe Biden]] sparked a major debate when he described spending on [[id:168a5951-ad3c-4444-922d-1d6f13024816][social care]] and [[id:51c85bde-963a-48ed-befb-306429ca64cf][childcare]] as an investment in infrastructure. We’re exploring why the question of what counts as ‘[[id:4ee19c5a-62af-45b2-b430-da0bbc515e36][infrastructure]]’ really matters. US policy expert Julie Kashen talks us through the US infrastructure debate then Professor Sue Himmelweit from the Women’s Budget Group explains why it could have far-reaching implications around the world.
#+end_quote

[[id:9c431f2c-7144-4f9a-af06-ffb1fb54ed1d][Care work is infrastructure]].
