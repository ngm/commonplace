:PROPERTIES:
:ID:       b31b39ed-dda5-4dfd-a84a-7aa393c8b68b
:mtime:    20211127120933 20210724222235
:ctime:    20210724222235
:END:
#+title: Checking for TLS 1.2 support on a server
#+CREATED: [2021-03-01 Mon 15:18]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:19168612-9bb6-4f5e-b6b0-7d2a5526cfa3][Sysadmin]]

I was doing this to check that Mailgun would continue to work for some of our apps following their deprecation of TLS 1.2.

From what I glean from Mailgun's [[https://www.mailgun.com/blog/tls-version-1-0-and-1-1-deprecation/][blog post]], you need to check that wherever your app is that sends mail via Mailgun, that that server supports TLS 1.2.

You can do this by scanning it with `nmap`:

#+begin_src shell
nmap --script ssl-enum-ciphers -p 443 domain.example
#+end_src

I guess it does a check on the [[https://en.wikipedia.org/wiki/Cipher_suite][cipher suites]] that the server is providing.

It will also tell you if any of the ciphers are vulnerable to attacks (and ergo that you need to update the server).
