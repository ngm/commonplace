:PROPERTIES:
:ID:       0c15256b-5e36-4acd-b18b-5f3211f9ee66
:mtime:    20211127120800 20210724222235
:ctime:    20210724222235
:END:
#+title: Building orgzly
#+CREATED: [2021-03-20 Sat 20:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

* Why?

  It's not had a release for nearly a year, but a bunch of features have been added in that I'd like.  Specifically that of relative paths working as they do in Emacs, so that they work across both.  Also webdav sync could be handy.

* Building

- [[https://github.com/orgzly/orgzly-android#building--testing][GitHub - orgzly/orgzly-android: Outliner for taking notes and managing to-do ...]] 

  Seems I need gradle, which seems to then need SDKMAN!
- [[https://sdkman.io/][Home - SDKMAN! the Software Development Kit Manager]] 

  #+begin_src bash
  source "/home/neil/.sdkman/bin/sdkman-init.sh"
  sdk install gradle
  #+end_src

#+begin_src bash
cd Code/
git clone https://github.com/orgzly/orgzly-android
cd orgzly-android/
./gradlew build
  #+end_src

  #+begin_quote
  > SDK location not found. Define location with an ANDROID_SDK_ROOT environment variable or by setting the sdk.dir path in your project's local properties file at '/home/neil/Code/orgzly-android/local.properties'.
  #+end_quote

  https://developer.android.com/studio
  Download command line tools only.
  
 #+begin_src bash
 unzip commandlinetools-linux-6858069_latest.zip 
 mkdir ~/android
 mv cmdline-tools/ ~/android
 cd android/cmdline-tools
mkdir latest
mv latest/ cmdline-tools/
cd Code/orgzly-android/
./gradlew build
ANDROID_SDK_ROOT=/home/neil/android/ ./gradlew build
scp app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk dloop:
scp app/build/outputs/apk/fdroid/debug/app-fdroid-debug.apk dloop:
 #+end_src

 New settings:

 - Root for absolute links (e.g. =file:/readme.txt=)
 - Root for relative links (e.g. =file:readme.txt=)

   Files now open properly!  I open them either in orgro, or images open in the file manager.
