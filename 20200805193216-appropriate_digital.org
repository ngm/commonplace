:PROPERTIES:
:ID:       e9e1398e-70cf-47fd-8af9-3291ee570d24
:mtime:    20211127121006 20210724222235
:ctime:    20200805193216
:END:
#+title: Appropriate digital

Like [[id:7ee5a1b6-9c3b-41a3-8683-e1812b02b192][appropriate technology]], but specifically for digital services (mainly software).  [[id:b008c798-e75f-40b3-bbfa-fc58fea991f2][Dan Hind]] talks about something like it in his interview on [[id:2d9e72be-7f43-4aa7-81ec-46bc4f117875][Tech Won't Save Us]]. 
