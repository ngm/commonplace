:PROPERTIES:
:ID:       9281ef41-3ae5-4f01-afd4-ae8a5fe4d921
:mtime:    20220221181157
:ctime:    20220221181157
:END:
#+TITLE: The Tyranny of Openness: What Happened to Peer Production?
#+CREATED: [2022-02-21 Mon]
#+LAST_MODIFIED: [2022-02-21 Mon 18:14]

+ URL :: https://mediarxiv.org/ecfpj/
+ Author :: [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]]

  About [[id:59b8356d-2a7e-45c4-aad4-4adec40e4593][Free software licensing]], [[id:829e4083-d410-4ef3-8aac-703b31826061][Free software economics]].
