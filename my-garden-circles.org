:PROPERTIES:
:ID:       b6e838d4-77ff-45e1-8e1d-a5397c881331
:mtime:    20220220123100 20220122124238 20220120225355 20220118195422 20220113141418 20220108085711 20211231120736 20211229124205
:ctime:    20211229124205
:END:
#+TITLE: My garden circles
#+CREATED: [2021-12-29 Wed]
#+LAST_MODIFIED: [2022-02-20 Sun 12:33]

For various topics, people that I have some existing mental model of, that I would look in their garden for some useful thoughts.  Not necessary that I agree with.  Being in one of these lists assumes the person has a fairly easily searchable [[id:cc35ef6f-c123-4779-9ba6-58c64ab69d6b][digital garden]] somewhere, not just a social media stream.

See [[id:023fb7e2-0f76-41bd-b8dd-b5e0adeabd50][How I take notes]]. [[id:eb453cc7-33fb-42a7-9c7e-23475c5b7708][Garden circle]].

[[https://github.com/MaggieAppleton/digital-gardeners][MaggieAppleton/digital-gardeners]] is probably a useful list to look at for new gardens to peruse on topics. And https://tiny.cc/digital-gardeners

As it grows, I'm noticing that (perhaps unsurprisingly) a list-based approach to this isn't working.  People tend to write on multiple things that I'm interested in, so pop up in multiple lists.  Some kind of tagging or wikilinking would make more sense.  

* Knowledge management

+ [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]] (https://www.zylstra.org/blog/wiki-frontpage/)
+ [[id:390b2d01-645f-49e4-9ce9-36755f6b54c3][Phil Jones]]
+ [[id:999d15b3-84ff-4b10-8146-9f7b575b0215][Bill Seitz]]
+ [[id:a248536c-5132-45a6-b2ed-593060601cd7][Maggie Appleton]]
+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]]
+ [[id:d4c392f4-7115-4bb5-b5ee-708174b8debf][Andy Matuschak]]
+ [[id:910a5921-4de6-462c-837b-34133ad5cc93][Gordon Brander]]
+ [[id:4239b262-c4a5-40ed-bda2-e0fd92bf0649][Thompson Morrison]]

* Decentralised technology

+ [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] Wiki
+ [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]] (a book, but with a well defined pattern language)
+ [[id:1d7ad45e-2f5e-4f40-8f3f-b829e2dc79ec][P2P Foundation Wiki]]

* Commoning / political organisation

+ [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]] (a book, but with a well defined pattern language)
+ [[id:83490ac8-79eb-4360-a18c-a509e10a7766][patternlanguage.commoning.wiki]] (wikification of FFA)
+ [[id:3f95d064-1072-4115-9942-3e42532aa4ee][Mike Hales]]
+ [[id:ef3c7554-d765-4645-9631-129c22673656][Lean Logic]] (newly discovered as of Dec 2021, still don't have good mental model but seems interesting)
+ [[id:1d7ad45e-2f5e-4f40-8f3f-b829e2dc79ec][P2P Foundation Wiki]]
  
* Complex system

+ [[id:4239b262-c4a5-40ed-bda2-e0fd92bf0649][Thompson Morrison]]

* Politics, governance 

+ [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]] - not quite a garden, but easy enough to search and huge respect for Nathan's work https://nathanschneider.info/open-work/
 
* Revolution

+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]]

* Theology

+ [[id:f0f2e065-5717-46ba-ace3-b78f53efb540][Maya]]

* Coding

+ [[id:390b2d01-645f-49e4-9ce9-36755f6b54c3][Phil Jones]]
+ [[id:e5ff6c38-37b3-497b-80ab-b18d7184f6e3][vera]]
+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]]
