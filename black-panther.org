:PROPERTIES:
:ID:       b51c0bea-c88b-4ba3-8600-0555c640e18f
:mtime:    20220731115804 20220731105512
:ctime:    20220731105512
:END:
#+TITLE: Black Panther
#+CREATED: [2022-07-31 Sun]
#+LAST_MODIFIED: [2022-07-31 Sun 11:58]

+ A :: [[id:4a164472-ace9-4df6-84f4-da8c1642ceeb][film]]

[[id:dd2c8f3b-44e5-450b-b26d-0a93209e7bd5][Marvel Cinematic Universe]].

Themes: [[id:98f53dc4-e01d-4f4c-aea5-b1f6e47ba385][Empire]].  [[id:f1669171-61c2-4069-9d38-c6047534a247][Colonialism]].  [[id:43abb9f7-1334-49aa-9e47-e3c50a57a91f][Imperialism]].  [[id:5ecec057-7c9d-44b3-8e7e-71b8018d95f1][Oppression]].

Wakanda has a [[id:c2eee50e-313d-49a0-9688-f4ada05c4182][Solarpunk]] vibe.  Though it is based on the mining of a finite resource.
