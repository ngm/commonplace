:PROPERTIES:
:ID:       7ade0847-a936-4cdc-aecd-c91816fbca97
:mtime:    20211127120858 20210822211010
:ctime:    20210822211010
:END:
#+TITLE: public-private partnerships
#+CREATED: [2021-08-22 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Contrast with [[id:19c72914-a9b8-4dc5-a4ec-758655015c3e][commons-public partnerships]].

#+begin_quote
PPPs are typically vehicles for developing infrastructure for water supply or sewage management, or building roads, bridges, schools, hospitals, prisons, or public facilities such as swimming pools and playing fields.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
These are often good-faith attempts to address pressing social problems through contract-based collaborations between businesses and government.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
PPPs are based on fundamentally incompatible objectives — the state’s obligation to protect the public good and private businesses’ desire to maximize profits. In practice, many public-private collaborations function less as partnerships than as disguised giveaways.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
PPP can let a company acquire equity ownership of public infrastructure such as roads, bridges, and public facilities for a long period — fifteen, thirty, even ninety-nine years — and then manage them as a private market asset.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
The state and the corporate sector both pretend that PPPs are a healthy, wholesome arrangement that benefits everyone and solves the lack of public funds. In truth, a great many PPPs amount to a marketization of the public sector that extracts more money from citizens, surrenders taxpayer assets to businesses, and neutralizes public accountability and control.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
