:PROPERTIES:
:ID:       0d1e88e5-b752-4fb0-a13f-1cd512a3d12c
:mtime:    20211127120801 20210724222235
:ctime:    20210724222235
:END:
#+title: Problem: org-preserve-local-variables error when refiling
#+CREATED: [2021-03-17 Wed 09:27]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Whenever I get an =org-preserve-local-variables= error (usually when refiling an item, and usually after a spacemacs update), this is the way to solve it:

#+begin_src bash
cd ~/.emacs.d/elpa
find org*/*.elc -print0 | xargs -0 rm
#+end_src

NOTE: where the elpa directory might be in a different location depending which version/branch you’re on.  e.g. =~/.emacs.d/elpa/26.3/develop/=.

And then run =M-x spacemacs/recompile-elpa=.

Basically it's removing all of the compiled org files and then compiling them again.

source: https://github.com/syl20bnr/spacemacs/issues/11801
