:PROPERTIES:
:ID:       e42ecec4-23d4-4cff-aeaf-afd1df59135f
:mtime:    20211127121002 20210724222235
:ctime:    20200923161500
:END:
#+title: Discussion of Roam alternatives

There's a good discussion on Roam alternatives at Darius' toot here: https://friend.camp/@darius/104898659326177512

Touches on some of the nice features of [[id:0ba0c8ce-613b-4145-bbb3-e952c4fa41e8][Roam]] too - such as [[id:912ce554-5ec6-470a-bb7e-35eb130925b1][fully-transcluded backlinks]] (plus filtering) and [[id:e08fbeb5-6101-4c65-98c2-ded4029a61c5][block-level transclusion]].  It's good as it's making me wonder if I can do them and would find them useful in org-roam too.


