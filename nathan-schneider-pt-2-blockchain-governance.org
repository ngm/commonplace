:PROPERTIES:
:ID:       7201f56a-f196-4c31-b70b-b0dd002862b8
:mtime:    20220123120534 20220120231259
:ctime:    20220120231259
:END:
#+TITLE: Nathan Schneider, Pt. 2 (Blockchain Governance)
#+CREATED: [2022-01-20 Thu]
#+LAST_MODIFIED: [2022-01-23 Sun 12:05]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://publicinfrastructure.org/podcast/52-nathan-schneider-pt-2/ 

  * DAO space is doing experimenting with voting systems and governance :wikify:

[[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]], podcast with Ethan Zuckerman.

[[id:f6b5f16b-8004-4c3f-a687-d05f23a2fbf6][Quadratic voting]].

