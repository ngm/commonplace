:PROPERTIES:
:ID:       19cf7e5f-896c-46b6-ad9f-43fe61f48d79
:mtime:    20230317152712
:ctime:    20230317152712
:END:
#+TITLE: Agroecology and open-source software
#+CREATED: [2023-03-17 Fri]
#+LAST_MODIFIED: [2023-03-17 Fri 15:28]

[[id:fdd64cf0-e6fa-481d-b4d1-807686bfe3f0][Agroecology]] and [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][Open-source]].

#+begin_quote
jeff - Looking forward to this new book by Dorn Cox and Courtney White:

The Great Regeneration:
Ecological Agriculture, Open-Source Technology, and a Radical Vision of Hope

...explores the critical function that #OpenSource tech can have in promoting healthy #agroecological systems, through data-sharing and networking....

brought together, there is potential to revolutionize how we manage food production... decentralizing and deindustrializing [long dominant] structures..."

https://www.chelseagreen.com/product/the-great-regeneration/

-- https://social.coop/@jeff/109558583982281354
#+end_quote
