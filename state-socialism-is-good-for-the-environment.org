:PROPERTIES:
:ID:       57e77c40-1df8-413a-bf07-0e16c79c4660
:mtime:    20220115172352
:ctime:    20220115172352
:END:
#+TITLE: State socialism is good for the environment
#+CREATED: [2022-01-15 Sat]
#+LAST_MODIFIED: [2022-01-15 Sat 17:29]

[[id:0818d39d-d31e-476c-a748-a1a4186cf67d][State socialism]] is good for the [[id:4c186278-137a-439d-a123-b27932c4e008][environment]].

* Because

Kind of an empirical argument.

+ State policies in [[id:38b8ccd8-e2d9-4e60-8917-1d29bb18cca5][China]], [[id:94a8b80f-e5c4-44f3-ab3b-fc16becf1baa][Cuba]] and the [[id:6a118895-9bdc-4027-a8b2-2cab1f48e562][Soviet Union]] were beneficial for the environment
 
* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:c292cbe9-0fd0-4eda-887b-c2022b1ea472][Concentrate and ask again]]
+ Source (for me) :: [[id:6cc6c063-6cf7-4c1c-baf1-c4924bb0c8d6][Salvatore Engel-Di Mauro]]

Not sure.   Salvatore seems fairly convinced, but I don't know enough.  Not necessarily a fan of state socialism either.  But hey if the claim is true, then that's a good thing.
