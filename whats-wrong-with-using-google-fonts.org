:PROPERTIES:
:ID:       763a3e36-57b8-42da-8f5f-0e771e490f70
:mtime:    20240419111728
:ctime:    20240419111728
:END:
#+TITLE: What's wrong with using Google Fonts?
#+CREATED: [2024-04-19 Fri]
#+LAST_MODIFIED: [2024-04-19 Fri 11:23]

It's a [[id:2fea36aa-f5f0-4c68-9e6a-3d3a4f980ca8][GDPR]] violation apparently, when served from Google's servers.

* Resources

- [[https://termageddon.com/google-fonts-violates-gdpr/][Usage of Google Fonts violates GDPR - Termageddon]] 
- [[https://www.lexology.com/library/detail.aspx?g=8546d90b-61f5-4c96-8bd0-86d1676863c2][Google fonts on your website? Do not share IP addresses with Google because o...]]
