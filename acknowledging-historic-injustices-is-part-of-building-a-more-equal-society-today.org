:PROPERTIES:
:ID:       da3cbec3-18e9-4c7b-97ef-87657a3dc885
:mtime:    20220108191828 20220108130659
:ctime:    20220108130659
:END:
#+TITLE: Acknowledging historic injustices is part of building a more equal society today
#+CREATED: [2022-01-08 Sat]
#+LAST_MODIFIED: [2022-01-08 Sat 19:20]

Articulated here: [[https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down][The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The...]] 

* Because

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Acceptance level :: [[id:9a13c2e4-f670-4d0d-9f15-62cfa405f37d][Most likely]]

I'm sure it must be, I just don't have any supporting evidence as of yet.
