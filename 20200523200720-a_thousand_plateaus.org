:PROPERTIES:
:ID:       7e696126-293c-44f9-892d-afe84d03527e
:mtime:    20211127120900 20210724222235
:ctime:    20200523200720
:END:
#+TITLE: A Thousand Plateaus

[[id:bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa][Deleuze & Guattari]].

#+begin_quote
Write, form a [[id:8042b857-d223-4947-840e-bf5bbbb29e73][rhizome]], increase your territory by [[id:37a8cb79-d66b-44b9-be62-40ce6e9ab6e5][deterritorialization]], extend the [[id:110093a1-7524-4da4-bf28-107b995105c0][line of flight]] to the point where it becomes an abstract machine covering the entire plane of consistency.
#+end_quote
