:PROPERTIES:
:ID:       e9b9723c-884a-49c5-ba95-13d96b06a469
:mtime:    20211220224943
:ctime:    20211220224943
:END:
#+TITLE: Shaun Chamberlin on David Fleming's Vision of Post-Capitalist Life
#+CREATED: [2021-12-20 Mon]
#+LAST_MODIFIED: [2021-12-20 Mon 22:51]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://david-bollier.simplecast.com/episodes/shaun-chaberlin-on-david-flemings-vision-of-post-capitalist-life
+ Series :: [[id:d5682dcd-68e5-4da6-b260-87382b960781][Frontiers of Commoning]]
+ Featuring :: [[id:6da90d6e-f2eb-4a49-b5bf-2c35c88f257c][Shaun Chamberlin]] / [[id:b5173915-bea2-4036-b353-b3617c7f3263][David Bollier]]

[[id:19b41473-b79c-40aa-b9eb-d9c2b0bc5d79][David Fleming]]. [[id:ef3c7554-d765-4645-9631-129c22673656][Lean Logic]].
