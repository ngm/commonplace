:PROPERTIES:
:ID:       0768dd60-e972-4e97-a475-c21c010b89fa
:mtime:    20211127120757 20210724222235
:ctime:    20210724222235
:END:
#+title: Hierarchical note-taking

See e.g. [[https://www.dendron.so/][Dendron]].

Vs [[id:47f5252f-cfcb-429a-9b2e-9bd49992a25e][Non-hierarchical note-taking]].

See [[id:abcd71aa-9a1f-4df3-b637-7c05ab1416fb][note-taking]].

#+begin_quote
I see Dendron adds hierarchy by using naming conventions. That doesn't appeal to me at all, smacks of the Dewey decimal system, and ultimately everything ending up in '999miscellaneous', or the alt. usenet category. Whereas to me, as per Weinberger, everything *starts as* misc and its place in the scheme of things is emergent.

-- [[https://m.tzyl.nl/@ton/105135732686588264][Ton Zijlstra]] 
#+end_quote

* Resources

- [[https://www.kevinslin.com/notes/3dd58f62-fee5-4f93-b9f1-b0f0f59a9b64.html][A Hierarchy First Approach to Note Taking - Kevin S Lin]] 
