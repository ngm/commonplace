:PROPERTIES:
:ID:       67f07e28-d01e-4569-a48d-e2c6483852a8
:mtime:    20240309171356
:ctime:    20240308213258
:END:
#+title: Miners’ Strike 1984: The Battle For Britain

Documentary about the [[id:3cfc2d79-f25d-4f0a-9936-86eba1438310][Miners' strike]].

Three different episodes.

* [[id:37810e3a-df28-4a0b-8a70-643097cdff31][Shirebrook]]

First one is about the division between strikers and those that crossed the picket line.  Focused on the mining town of [[id:37810e3a-df28-4a0b-8a70-643097cdff31][Shirebrook]].

Feelings are still raw, 40 years later.

* Orgreave

The [[id:9622904b-9b94-4b39-a1e4-361bb844532b][Battle of Orgreave]].

[[id:1d2141d3-3692-4ca7-ab20-534197186470][Police brutality]]. [[id:5e29ef2f-0773-46e3-8ae0-eb0361264d4e][State violence]].

Shocking, the violence that was meted out by the police on the miners.  Seemingly premeditated.

Then perjury.

All seemingly coming directly from the wishes of Margaret Thatcher.

The media apparently complicit, a piece of state apparatus.
