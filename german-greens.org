:PROPERTIES:
:ID:       b27fabf3-de4e-4e78-b652-3001e02df9a6
:mtime:    20220820110705
:ctime:    20220820110705
:END:
#+TITLE: German Greens
#+CREATED: [2022-08-20 Sat]
#+LAST_MODIFIED: [2022-08-20 Sat 11:07]

#+begin_quote
The story of the German Greens has been told many times: briefly, those on the left, involved in social movements, joined a platform to fight elections. Those who had been involved in the student movement, and some sympathetic to the Baader-Meinhof gang, joined environmentalists. At first the Greens were, in the words of an early leader figure Petra Kelly, ‘the anti-party party’ (Emerson, 2011: 55). Given the openness of the German electoral system, co-option was perhaps close to inevitable. Greens were elected on radical platforms but eventually joined coalition regional governments with the SPD, and the party over the decades has moved broadly to the centre right.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote
