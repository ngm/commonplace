:PROPERTIES:
:ID:       8ac5b2b5-fc63-424f-af1a-8342a885ce02
:mtime:    20211127120908 20210724222235
:ctime:    20210724222235
:END:
#+title: Public Digital Cooperative
#+CREATED: [2021-04-11 Sun 14:20]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
Finally, we propose a network of Public Digital Cooperatives that would establish digital development labs in disinvested communities. Each would be overseen by citizens assemblies with a mandate to ensure that the projects funded develop the infrastructure of a more complete social, economic and political democracy. The labs would work with other public institutions, from the post office and the central bank to the universities and the library system, to develop new, commonly held resources. In this way, we as citizens will have eyes on the shape and direction of the digital economy,, and will be able to take a far more active role in the conscious planning of our shared future

-- [[id:1948bc3b-3360-4cdd-8ede-337971bbb931][Privacy, censorship and social media- The Case for a Common Platform]]
#+end_quote

The [[id:a12bb960-7e1e-4435-9220-da919213d7c3][British Digital Cooperative]] fleshes this out.
