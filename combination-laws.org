:PROPERTIES:
:ID:       82681daf-f9b1-431f-bf40-485873475abe
:mtime:    20221022134203
:ctime:    20221022134203
:END:
#+TITLE: Combination Laws
#+CREATED: [2022-10-22 Sat]
#+LAST_MODIFIED: [2022-10-22 Sat 13:42]

#+begin_quote
The new Combination Laws, which outlawed [[id:14be0045-2209-4ea0-829a-58659f2624f0][trade union]] activity, severely limited collective action by textile weavers.

-- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
#+end_quote
