:PROPERTIES:
:ID:       1dc58554-5352-4b1e-a10f-2985522575ea
:mtime:    20220502153334
:ctime:    20220502153334
:END:
#+TITLE: FOLLOW THE (CITY) LEADER: the power of local action on the climate crisis
#+CREATED: [2022-05-02 Mon]
#+LAST_MODIFIED: [2022-05-02 Mon 15:58]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.cheerfulpodcast.com/rtbc-episodes/follow-the-city-leader
+ Series :: [[id:bb2beb1f-e125-4fa6-97b5-0909a0effd46][Reasons to be Cheerful]]

The power of [[id:2d8e1629-ec78-4a99-a331-16d7cdf73b3a][local action]] on the [[id:1c5821aa-f289-48b6-96e5-c55153fb2a47][climate crisis]].

Some good examples of [[id:a59010da-0ade-42f6-b80a-d96c0900c660][municipalism]].  Cities and local authorities as drivers of climate action, rather than national governments.

~00:16:09 [[id:ca45bcac-5827-4722-90f7-bc151a6cc973][C40 Cities]]. Have been the drivers behind some of the big climate targets. I think they said around 700 million population represented by C40? And a substantial economic amount.

~00:34:36 [[id:dd37e88a-9336-4717-aebc-5bccef3bd3c6][UK100]].  "It supports decision-makers in UK towns, cities and counties in their transition to Net Zero."

~00:38:54  [[id:ba17bc61-2862-46ee-9b83-80f4b711cc60][Planning permission]] and [[id:d9f77bd4-9129-410b-a8c2-d19b1eb63b59][public procurement]] are things local authorities already have plenty of say over.

~00:50:16  [[id:c0a1f806-12be-4ee5-9d2d-f627b66862c1][15-minute city]].  Everything you need should be 15 minutes or less walk away.
