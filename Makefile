# Originally sourced from https://opensource.com/article/20/3/blog-emacs

# Makefile for commonplace

.PHONY: all publish publish_no_init

all: graph republish rsync

graph: publish.el
	@echo "Graphing..."
	emacs --batch --load publish.el --funcall commonplace/build-graph-json
	@chmod 755 graph.svg

publish: publish.el
	@echo "Publishing..."
	emacs --batch --load publish.el --funcall commonplace/publish

republish: publish.el
	@echo "Republishing all files..."
	emacs --batch --load publish.el --funcall commonplace/republish

publish_remote: publish.el
	@echo "Publishing..."
	emacs --batch --load publish.el --funcall commonplace/publish-remote

rsync:
	@echo "rsyncing published site to hosting..."
	rsync -chavz /var/www/html/commonplace/ dloop:/var/www/commonplace/

agora: publish-agora.el
	@echo "Converting to Agora-compatible Markdown files..."
	emacs --batch --load publish-agora.el --funcall commonplace/publish-for-agora-local

agora_remote: publish-agora.el
	@echo "Converting to Agora-compatible Markdown files..."
	emacs --batch --load publish-agora.el --funcall commonplace/publish-for-agora-remote

publish_no_init: publish.el
	@echo "Publishing... with --no-init."
	emacs --batch --no-init --load publish.el --funcall org-publish-all

clean:
	@echo "Cleaning up.."
	@rm -rvf *.elc
	@rm -rvf public
	@rm -rvf ~/.org-timestamps/*
