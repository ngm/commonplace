module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
    purge: [
        '/var/www/html/commonplace/index.html',
        '/var/www/html/commonplace/clougha-hike.html',
        '/var/www/html/commonplace/the-garden-and-the-stream.html',
        '/var/www/html/commonplace/20200821142536-how_i_publish_my_wiki_with_org_publish.html'
    ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
