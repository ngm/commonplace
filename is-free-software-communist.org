:PROPERTIES:
:ID:       30c67bcd-4d33-48cc-adff-1f6a2ec52d1f
:mtime:    20211231122554
:ctime:    20211231122554
:END:
#+TITLE: Is free software communist?
#+CREATED: [2021-12-31 Fri]
#+LAST_MODIFIED: [2021-12-31 Fri 12:38]

[Does this even make sense as a question?]

Depends on your definition of [[id:9c3f0897-c60c-44a8-9714-943a2987ab23][communism]] and your definition of [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][free software]].

Free software at least fulfils the tenet "[[id:f113b886-928d-4197-a048-9c0e52f88d17][From each according to their ability, to each according to their need]]".

Multiple takes on this here: https://www.quora.com/Is-free-open-source-software-communist

* Epistemic status

+ Type :: [[id:4dc58a85-d764-44ea-be65-0ba08647a552][question]]
