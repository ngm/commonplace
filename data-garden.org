:PROPERTIES:
:ID:       71ce2d60-93c2-40d2-8217-eda8d9f3f507
:mtime:    20211127120854 20210724222235
:ctime:    20210724222235
:END:
#+title: Data Garden
#+CREATED: [2021-06-19 Sat 12:14]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://cyrus.website/data-garden
  
#+begin_quote
By storing data nature’s way, in the DNA of plants, this work discusses the potential for truly green, carbon absorbing data storage, owned by the public rather than monopolistic corporations.
#+end_quote

Art project, not necessarily reality anytime soon, but I love this as an idea and a provocation.

[[id:c2eee50e-313d-49a0-9688-f4ada05c4182][Solarpunk]].
