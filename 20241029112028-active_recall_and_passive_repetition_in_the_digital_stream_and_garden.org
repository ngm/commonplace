:PROPERTIES:
:ID:       969ae38f-4537-4732-9fc4-4095d541fda6
:mtime:    20241029172108 20241029151146 20241029112032
:ctime:    20241029112028
:END:
#+title: Active recall and passive repetition in the digital stream and garden

[[id:a4b78734-7103-4168-b46b-f896129c383e][Passive repetition]] can result in an [[id:de9f3ab2-b646-47fe-8989-e5fdc0479f23][illusion of knowing]]. Better to do [[id:eae52647-d84d-4e3b-ade5-6bffea571fef][active repetition]] when you can.

One of the key uses of a digital stream and digital garden for me is as a personal educational tool - helping me to learn. One way they can help with this is by encouraging active recall and repetition. Writing in a digital garden or on social media can be a form of active repetition.

Not by default, though - you can quite easily also do passive repetition in a digital garden. Just copying and pasting text from elsewhere would be largely passive repetition. Similar to just underlining or highlighting.

I think the journal pages of a digital garden is a good space for active repetition. Somewhere to summarise ideas in your own words, and for "blurting".

I suppose you can do active recall in both the stream and the garden. But the stream is your working area for it, and the garden where you store what sticks long term.

Traditional social media / microblogging is also great for this.  Particularly as dialogue and group discussion is an excellent prompt for active recall.  However for me they also have a huge problem - distraction. I can't go on the Fediverse without it ending up as a bit of a mindless scroll fest. (Which, admittedly of often a useful tool for information discovery...)
