:PROPERTIES:
:ID:       616dab19-d70e-48c8-b10c-e1826b4e6b3f
:mtime:    20211127120845 20210724222235
:ctime:    20200705162023
:END:
#+title: economic policy in response to Big Tech must go beyond data

Particularly in the context of AI.

#+begin_quote
economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst. It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers. If we want to take back control over Big Tech, we need to pay attention to more than just data.

-- [[https://www.adalovelaceinstitute.org/data-compute-labour/][Data, Compute, Labour | Ada Lovelace Institute]] 
#+end_quote

