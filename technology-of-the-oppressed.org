:PROPERTIES:
:ID:       43961e5f-42e4-4749-aa48-d690730e3a9f
:mtime:    20220712212703 20220624142640
:ctime:    20220624142640
:END:
#+TITLE: Technology of the Oppressed
#+CREATED: [2022-06-24 Fri]
#+LAST_MODIFIED: [2022-07-12 Tue 22:04]

+ URL :: https://mitpress.mit.edu/books/technology-oppressed
+ Author :: [[id:faecd671-34a1-4432-becd-aaf64a37c690][David Nemer]]
+ Subtitle ::  Inequity and the Digital Mundane in Favelas of Brazil 
  
#+begin_quote
Brazilian favelas are impoverished settlements usually located on hillsides or the outskirts of a city. In Technology of the Oppressed, David Nemer draws on extensive ethnographic fieldwork to provide a rich account of how favela residents engage with technology in community technology centers and in their everyday lives. Their stories reveal [[id:df4dd7c1-42cd-40ed-88fd-5b2f85341e6d][the structural violence of the information age]]. But they also show how those oppressed by technology don't just reject it, but consciously resist and appropriate it, and how their experiences with digital technologies enable them to navigate both digital and nondigital sources of oppression—and even, at times, to flourish.
#+end_quote

#+begin_quote
Nemer uses a decolonial and intersectional framework called [[id:5af3c435-c2bc-42bb-8eb6-780cb41c494a][Mundane Technology]] as an analytical tool to understand how digital technologies can simultaneously be sites of oppression and tools in the fight for freedom. Building on the work of the Brazilian educator and philosopher [[id:4808a722-0e7f-4211-988c-9daeb2d5ba94][Paulo Freire]], he shows how the favela residents appropriate everyday technologies—technological artifacts (cell phones, [[id:fe2273c1-dd14-4aa2-9323-cafa2ab70ef5][Facebook]]), operations ([[id:36fdfa5d-bb7e-41c1-8442-054bea1915ec][repair]]), and spaces (Telecenters and Lan Houses)—and use them to alleviate the oppression in their everyday lives. He also addresses the relationship of misinformation to radicalization and the rise of the new far right.
#+end_quote

- [[id:4d2e3c96-95d1-45eb-94d7-9aed000c2a18][Even with access to technology, marginalized people face numerous sources of oppression]].

#+begin_quote
Contrary to the simplistic techno-optimistic belief that technology will save the poor, even with access to technology these marginalized people face numerous sources of oppression, including technological biases, racism, classism, sexism, and censorship. Yet the spirit, love, community, resilience, and resistance of favela residents make possible their pursuit of freedom. 

-- [[id:43961e5f-42e4-4749-aa48-d690730e3a9f][Technology of the Oppressed]]
#+end_quote
