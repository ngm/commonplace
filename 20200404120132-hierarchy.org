:PROPERTIES:
:ID:       f17f1321-8d55-4c32-bdd9-3db4a55b5808
:mtime:    20230909171229 20211127121011 20210724222235
:ctime:    20200404120132
:END:
#+TITLE: hierarchy

#+begin_quote
Hierarchy is emergent, not imposed from above. This is why we see hierarchy in nature—central nervous systems, keystone species, cells, organs, and organisms—even though there is no boss of nature.

However, a hierarchy, once established, tends to find ways to perpetuate itself.

-- [[id:dea4b226-2d36-4ef2-b15c-5a38d6bf5b9c][Fragments: vertebrate technology]]
#+end_quote
