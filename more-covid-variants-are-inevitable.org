:PROPERTIES:
:ID:       69703597-f311-41bd-b56b-680ab1a662d4
:mtime:    20220113202503
:ctime:    20220113202503
:END:
#+TITLE: More Covid variants are inevitable
#+CREATED: [2022-01-13 Thu]
#+LAST_MODIFIED: [2022-01-13 Thu 20:33]

[[id:15ecf8a8-1975-4096-8eb5-b6550708bac1][Coronavirus]]

[[https://www.theguardian.com/world/2022/jan/13/what-lies-on-the-other-side-of-the-uks-omicron-wave][What lies on the other side of the UK’s Omicron wave? | Omicron variant | The...]]

* Because

+ The key to reducing the risk of new variants is driving down case rates globally
+ But to date, [[id:b34caf64-7e36-411c-ba68-ec0f71aa19f4][vaccine hoarding]] and the widespread use of boosters in wealthier countries has left vast swathes of the world under-vaccinated

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]
