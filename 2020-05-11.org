:PROPERTIES:
:ID:       095e3c15-8b0e-4c09-802f-d54b6527d44e
:mtime:    20211127120758 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-05-11

* Facebook stock
I said that Facebook has no [[id:6b686538-e398-497b-96e1-5a5484d1877d][stock]], but [[id:54789e69-0048-4be4-b193-1cc50616313c][Ellie]] pointed out that Facebook does have some stock - its Pages pages builds up a collection of your photos, videos, and events over time.

Thanks Ellie!  Ellie has a great page at https://www.facebook.com/pg/storiesinpaper/ (and a great website at [[https://storiesinpaper.com/][Stories In Paper]]). 

* Spectacular
  
A couple of nice resources on [[id:8529cc43-f4ea-42a0-81c5-5bc53d4362fa][the Society of the Spectacle]]:

- [[https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/][An Illustrated Guide to Guy Debord's 'The Society of the Spectacle']]
- [[https://partiallyexaminedlife.com/2017/08/14/ep170-1-debord/][Partially Examined Life Episode 170: Guy Debord’s “Society of the Spectacle”]] 

I've found them both pretty handy for getting a grasp on the book and the [[id:ec236732-f776-452f-8af4-394ff125e259][spectacle]].

* [[id:e2e10174-fcd7-4d4e-a294-fb38f50aeff7][The online spectacle]] 
  
#+begin_quote
If [[id:2972a0df-d0ec-43ca-bd16-9a4a71108c1b][Debord]] were alive today, he would almost certainly extend his analysis of [[id:ec236732-f776-452f-8af4-394ff125e259][the spectacle]] to the Internet and [[id:e2e10174-fcd7-4d4e-a294-fb38f50aeff7][social media]]. Debord would no doubt have been horrified by social media companies such as Facebook and Twitter, which monetize our friendships, opinions, and emotions. Our internal thoughts and experiences are now commodifiable assets. Did you tweet today? Why haven’t you posted to Instagram? Did you “like” your friend’s photos on Facebook yet?

-- [[https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/][An Illustrated Guide to Guy Debord's 'The Society of the Spectacle']] 
#+end_quote
