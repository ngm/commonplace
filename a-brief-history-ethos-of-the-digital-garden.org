:PROPERTIES:
:ID:       884c0bb2-6f8e-4ada-a141-14ec0a8b0e2c
:mtime:    20211127120907 20210724222235
:ctime:    20210724222235
:END:
#+title: A Brief History & Ethos of the Digital Garden
#+CREATED: [2021-05-29 Sat 13:30]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ URL :: https://maggieappleton.com/garden-history
+ Author :: [[id:a248536c-5132-45a6-b2ed-593060601cd7][Maggie Appleton]]

* The Six Patterns of Gardening

 1. Topography over timelines
 2. Continuous growth
 3. Imperfection & learning in public
 4. Playful, personal and experimental
 5. Intercropping & content diversity
 6. Independent ownership
