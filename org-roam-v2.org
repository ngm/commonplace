:PROPERTIES:
:ID:       9a9cca39-43bc-4bd1-96b9-3f2b8d79a0fc
:mtime:    20211127120917 20210724222235
:ctime:    20210724222235
:END:
#+title: org-roam v2
#+CREATED: [2021-06-25 Fri 15:56]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:20210326T231455.451131][Preparing for org-roam v2]]

* Links

  - [[https://org-roam.discourse.group/t/the-org-roam-v2-great-migration/1505][The Org-roam v2 Great Migration - Development - Org-roam]] 
  - spacemacs: [[https://org-roam.discourse.group/t/the-org-roam-v2-great-migration/1505/19][The Org-roam v2 Great Migration - #19 by nickanderson - Development - Org-roam]] 
