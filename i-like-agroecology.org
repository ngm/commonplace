:PROPERTIES:
:ID:       aeff9698-9dd3-473b-9aca-ffdf7d215b21
:mtime:    20230723104651 20220122173641
:ctime:    20220122173641
:END:
#+TITLE: I like agroecology
#+CREATED: [2022-01-22 Sat]
#+LAST_MODIFIED: [2023-07-23 Sun 10:47]

[[id:2f5935ea-a42a-49c4-b0e0-b4f178be79c0][I like]] [[id:fdd64cf0-e6fa-481d-b4d1-807686bfe3f0][agroecology]].

* Because

+ Agroecology helps humanity relink itself with nature
+ Agroecology rebuts the predatory logic of nature's use as a commodity and production resource
+ Agroecology is not just alternative to green capitalism, but an alternative to capitalism itself
+ Agroecology is what allows movements of struggle for land to exist
+ Any serious struggle movement must have agroecology as its mode of production

the above claims come from [[id:78e1c71c-2340-40c2-9031-b2fa47de671e][Jayu]]'s subnode: https://anagora.org/@Jayu/agroecology

* And

+ dazinism likes it (https://social.coop/@dazinism/107628616319792941)
+ There's no agroecology without community
+ There's no agroecology without food sovereignty
+ There's no agroecology without preserving nature
+ [[id:f6d09757-ff62-49a3-bb13-158930a61ecc][Agroecology is an example of systems thinking]]


* Epistemic status

+ Type :: [[id:c4b32a97-6206-4649-8218-39c25526c3a8][feeling]]
+ Strength :: 5

Only 5 because it's new to me and I have no personal experience of it.  I like it with my mind but not with my heart yet.

Also I cannot backup the claims yet but the fact dazinism likes it gives me good gut feeling.
