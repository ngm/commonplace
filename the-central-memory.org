:PROPERTIES:
:ID:       5725dcc1-01d6-474c-9b82-9423beee38a3
:mtime:    20211127120839 20210724222235
:ctime:    20210724222235
:END:
#+title: The Central Memory
#+CREATED: [2020-11-29 Sun 19:48]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Thoroughly enjoying [[https://www.cyberneticforests.com/music/][The Central Memory]] by The Organizing Committee - quite a work of art.  AI-assisted 'Cyborg Pop' with references to [[id:3176ea46-0b18-4abe-be7c-cc8b7a441c90][Allende]], [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]], [[id:7f194828-b798-46d2-872b-24459a13bf51][Stafford Beer]], [[id:bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa][Deleuze and Guattari]], [[id:4652ebb9-6d7a-463b-9b20-b7109d0c576e][Situationism]], and it all sounds a bit like a Stereolab album.  (Found [[https://www.kickscondor.com/the-organizing-committee/][via]] [[id:640cd983-ba1e-4ab5-b7da-d860cdf75af1][Kicks Condor]])
