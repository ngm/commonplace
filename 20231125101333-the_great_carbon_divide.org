:PROPERTIES:
:ID:       87c7ed1d-2c39-459e-a5da-879868ee8b07
:END:
#+title: The great carbon divide

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]]
+ Found at :: https://www.theguardian.com/environment/ng-interactive/2023/nov/20/the-great-carbon-divide-climate-chasm-rich-poor

[[id:4e08f86e-d777-4e0c-a671-80bc58e34745][Carbon inequality]].

#+begin_quote
Income gaps – and therefore carbon gaps – may have narrowed between countries but they have widened within them. Responsibility for the ongoing climate crisis is becoming more concentrated, while its impacts are spreading.
#+end_quote

#+begin_quote
the richest 1% of the population produced as much carbon pollution in one year as the 5 billion people who make up the poorest two-thirds
#+end_quote

#+begin_quote
inequality between people has increasingly become a structural impediment to climate justice and climate action.
#+end_quote

#+begin_quote
the climate crisis worsens inequality and inequality worsens the climate crisis
#+end_quote

[[id:0899a9ab-5fb7-4f33-b1a8-6dfd2dfbd3e8][polluter elite]]
