:PROPERTIES:
:ID:       51927c09-259b-493d-8ca2-dc2926e7f56f
:mtime:    20220128214534 20211228170201 20211127120837 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Xanadu

[[id:305ae5b8-a3c4-4aca-bd38-0512fe0649dd][Ted Nelson]]'s [[id:0e6d2f7b-9fcb-48dd-bf81-f2a5046dbf9e][hypertext]] project.

* Attempts at implementation

Interesting thread from John Ohno on attempts at implement Xanadu concepts - https://threadreaderapp.com/thread/1232381856294875137.html

[[id:0ba0c8ce-613b-4145-bbb3-e952c4fa41e8][Roam]] and [[id:7082252f-074e-4552-aa16-7e535f1b2587][wikis]] get namechecked as examples of regularly occurring attempts to rebuild Xanadu concepts, badly..
I suppose you could argue, at least they actually exist :)

I like [[id:a248536c-5132-45a6-b2ed-593060601cd7][Maggie Appleton]]'s interpretation in [[id:945f638c-c5e8-4f13-a4b4-f2c34fa30f83][The Pattern Language of Project Xanadu]] that we can should perhaps see Xanadu as an increasingly more utilised [[id:887b6439-f0f0-4a99-9260-b82be3551a05][pattern language]], and stop just thinking of it as a failed software project.

* Twin Pages
  
- http://thoughtstorms.info/view/Xanadu
- http://webseitz.fluxent.com/view/Xanadu
- https://en.wikipedia.org/wiki/Project_Xanadu
