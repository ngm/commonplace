:PROPERTIES:
:ID:       b492d4a4-dd5d-4f06-9e50-7f4dc38694b9
:mtime:    20211127120933 20211126151556
:ctime:    20211126151556
:END:
#+TITLE: State power
#+CREATED: [2021-11-26 Fri]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
going beyond” the nation-state doesn’t mean “without the nation-state.” It means that we must seriously alter state power by introducing new operational logics and institutional players.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
