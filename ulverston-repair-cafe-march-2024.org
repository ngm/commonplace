:PROPERTIES:
:ID:       57c0604e-328f-44d9-a342-332b20dfbc2d
:mtime:    20240313094317
:ctime:    20240313094317
:END:
#+TITLE: Ulverston Repair Cafe, March 2024
#+CREATED: [2024-03-13 Wed]
#+LAST_MODIFIED: [2024-03-13 Wed 10:42]

[[id:fc14def2-3a67-4d4c-982b-a88061562fe7][Ulverston Repair Cafe]].

Second month back after being away for a few.

Looked at a couple of slow laptops.

* Laptop 1

A Dell Vostro. A bit of a long-term project for one of the other volunteers - today just backing up important files, which took about 2 hours running in the background.

* Laptop 2

An Asus running Windows 10.

** Reported issue
+ Reported as had been slowing down for a while.
+ But had recently started really creaking.

** Work carried out
+ At startup, Teams reports not having enough free space.
+ This message also appeared at startup "instruction at ... referenced memory at xxxx. The required data was not placed into memory because of an I/O error status of xxx  Click on OK to to terminate the program".
+ First thing, based off the messages, looked at available drive space.
  + Windows Explorer -> right-click C: -> Properties.
  + There was only 9Mb available.
  + Full drive space only 60Gb.
+ Have a look for where the space is being used up.
  + Settings -> System -> Storage ([[https://pureinfotech.com/whats-taking-up-space-hard-drive-windows-10/][How to see what's taking up space on a hard drive on Windows 10 - Pureinfotech]])
+ Mostly apps and features.  A few Gb on temporary files (mostly downloads folder).  User documents only about 5Gb.
+ Temporary files -> Cleared temporary files.
+ Apps & features -> Uninstalled a few apps.  (3 of which were different virus checkers.)
+ After this, had 9Gb free.
+ Installed AVG Free. (You have to pay for Windows Defender now? Oh - the free version is actually called Windows Security on Windows 10.  [[https://support.microsoft.com/en-gb/windows/stay-protected-with-windows-security-2ae0363d-0ada-c064-8b56-6a39afb6a963][Stay protected with Windows Security - Microsoft Support]]).
+ Turned off some apps starting at startup.
  + Settings -> Apps -> Startup ([[https://www.howtogeek.com/74523/how-to-disable-startup-programs-in-windows/][How to Disable Startup Programs in Windows]])
+ Restarted.
  + Was forced to install some Windows updates which was annoying and wasted some time.
+ Much faster at startup!
+ Still not the speediest machine.  Said that installing more memory and an SSD could possibly help, but that they were not easy to access.
+ Wrote down instructions for checking space and removing unneeded files, and how to check startup apps periodically.  Advised to run AVG scan regularly.
