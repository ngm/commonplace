:PROPERTIES:
:ID:       709bbf74-8275-4c43-ac68-c3f0f548d9f6
:mtime:    20240314174910 20240314101333
:ctime:    20240314101333
:END:
#+TITLE: Reframing and simplifying the idea of how to keep a Zettelkasten
#+CREATED: [2024-03-14 Thu]
#+LAST_MODIFIED: [2024-03-14 Thu 17:51]

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]]
+ Written by :: [[id:62cdf680-b517-47a6-b81a-4fba91aee329][Chris Aldrich]]
+ Found at :: https://boffosocko.com/2022/06/10/reframing-and-simplifying-the-idea-of-how-to-keep-a-zettelkasten/

[[id:af67b576-41a0-4378-8b53-e026c7dc6200][Commonplace books]], [[id:0fd15ffa-f702-47e8-a8a4-c6f6e5022221][zettelkasten]].

Really nice summary of the minimum of what you need to do to keep a 'zettelkasten', cutting through a lot of the complexity that has appeared around this.

- start with a commonplace book
- add a way to find things again (could be an index, could be search)
- keep reference / bibliography notes
- summarise and rewrite ideas in your own words
- link individual notes together
- answer a question by finding your notes on the topic, following the links you've made, and pulling together your summaries

Similar in spirit to e.g. [[id:0eac196c-2cbb-4309-bdde-34647ea76bde][Collector to Creator]].  But I like that Chris frames collector as a perfectly valid and minimal friction starting point.

#+begin_quote
collect interesting passages, quotes, and ideas as you read.
#+end_quote

No stress to create just yet.
