:PROPERTIES:
:ID:       10275349-74ce-4545-bbf3-ae9ffa5fe59b
:mtime:    20220212223450
:ctime:    20220212223450
:END:
#+TITLE: Commons-based Internet
#+CREATED: [2022-02-12 Sat]
#+LAST_MODIFIED: [2022-02-12 Sat 22:37]

[[id:0cb7cb18-cbb2-4060-bf7b-4ef6497dc375][Commons]]-based [[id:2fa03c01-e849-4ca3-8cdb-224f13717ae5][Internet]].

#+begin_quote
A commons-based Internet requires commons-based design principles and a commons-oriented society (Fuchs 2011b, chapters 8 and 9).

-- [[id:7b19a0e3-0ab5-4fc0-a7e7-eea5f901acd0][Social Media: A Critical Introduction]]
#+end_quote

#+begin_quote
Struggles for a commons-based Internet need to be connected to struggles for [[id:1574c602-e123-4f31-afc4-31b92e874e49][socialism]].

-- [[id:7b19a0e3-0ab5-4fc0-a7e7-eea5f901acd0][Social Media: A Critical Introduction]]
#+end_quote
