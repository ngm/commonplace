:PROPERTIES:
:ID:       98861578-e61f-4ee1-b3ce-8977f940f868
:mtime:    20220219162406
:ctime:    20220219162406
:END:
#+TITLE: How's That Open Source Governance Working for You?
#+CREATED: [2022-02-19 Sat]
#+LAST_MODIFIED: [2022-02-19 Sat 16:44]

+ URL :: https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng
+ Author :: [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]]

Discusses  [[id:683d64d2-f81f-4317-a0ff-8bef024a947f][free software governance]]: [[id:853dd384-0544-4745-9eab-58b46356f61e][implicit feudalism]], [[id:50c724cd-f882-4817-9300-2cf86330d3a6][Benevolent Dictator For Life]], [[id:710ed987-7b38-43bd-9d12-fd335d355515][The Tyranny of Structurelessness]].

[[id:cba713bf-4fe3-4187-8226-871729b43a7e][There is a layer of governance infrastructure missing from the web]].

See [[id:13f0b618-c94c-44a6-9fce-7cd3626e469d][CommunityRule]].   [[id:ee51e69e-87dc-4afd-9bbb-2a74a2f71c9b][No Servers!  No Admins!]]

#+begin_quote
Under the rhetoric of openness and meritocracy, an entrenched and disguised hierarchy reigns.
#+end_quote

#+begin_quote
Regardless, take a moment to notice the governance layers of the communities and projects you are part of. What is stated, and what is not? How would you make explicit the ways they are operating with now, and what do you wish their shared agreements would say?
#+end_quote
