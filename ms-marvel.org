:PROPERTIES:
:ID:       0be2cf19-1f0a-4038-80f6-8f94518db73a
:mtime:    20220714090000 20220712212109 20220712191611
:ctime:    20220712191611
:END:
#+TITLE: Ms. Marvel
#+CREATED: [2022-07-12 Tue]
#+LAST_MODIFIED: [2022-07-14 Thu 09:00]

+ A :: [[id:9720a5e1-6d61-401c-af01-27be7e6afea2][TV show]]

Touches on the [[id:b51ba8d5-3412-4601-98f1-cf78ccb2df7a][partition of India]].  And being Muslim in modern-day America.

Also has a banging soundtrack.
