:PROPERTIES:
:ID:       f86b9b23-14cc-433c-aece-0877619042de
:mtime:    20221024214939
:ctime:    20221024214939
:END:
#+TITLE: The Fossil Politics of Texas
#+CREATED: [2022-10-24 Mon]
#+LAST_MODIFIED: [2022-10-24 Mon 21:51]

+ URL :: http://www.gndmedia.co.uk 

In the US, solar and wind jobs are dangerous, don't pay well, and aren't unionised.
