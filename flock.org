:PROPERTIES:
:ID:       555611be-76b4-4c72-bd62-26e1c29c7d87
:mtime:    20211127120838 20210724222235
:ctime:    20210724222235
:END:
#+title: Flock

Flock is the name I'm giving my personal wiki / notes / stream / garden / flow / stock setup.  (My [[id:56bb1da4-32da-4505-8b67-f96f721e14cd][Personal learning environment]]?)  It's a Rube Goldberg device of various other systems, so it's handy to have a word to refer to it all with.

Flock as a portmanteau of flow and stock, and also because I like flocks of birds and [[id:430fb5f7-df03-4417-ad97-9dd414b1817f][agent-based systems]].  I like the idea of groups of people doing similar stuff to this, autonomously but connected, producing emergent phenomena.

* Parts

- [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]]
- [[id:20210326T232003.148801][org-roam]]
- [[id:22b57ec1-2c19-4628-a651-893d1034696b][orgzly]]
- [[id:bc94cd9c-f9f6-4029-a351-71d4f415af70][org-publish]]
- [[id:ade5b88f-9996-4c37-a248-52cb690e67fe][WordPress]] with [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] plugins

* [[id:04e1ff79-a3c4-4957-8070-039fe7f8f0c4][Fleeting notes]]

I take these with org-capture if I'm at my desktop, orgzly if I'm somewhere with my mobile.

* [[id:ed8bcbeb-0b32-4fcd-9f30-6625da8af21f][Permanent notes]]
  
Write them in org-roam.  Link them from my stream.

* Garden / stock
  
Same as permanent notes?  This is org-roam, anyway.

* Stream / flow
  
I write this in org-roam first with org-roam-daily.  Most stuff I'll then publish to my WordPress site, by publishing to html from org-roam and then copy/pasting into WordPress.

I do this as on WP I have feeds / webmentions set up, so I can get a bit of interactivity with my friends through this.  As well as POSSE, etc.  All the IndieWeb goodness.

* Articles
  
org-roam, but then publish on WordPress.

* Principles
  
- [[id:90c87855-2880-4c1e-bf3f-d80d087837a5][Local-first]].  I don't want to be dependent on connectivity, at least not for the basics of capturing notes and writing.
- 

* Itches
  
- [ ] Rather than copy/paste my notes from published org-roam to WordPress, push them via micropub.
  - Would require the content to be converted to HTML first, otherwise it would still be marked up in org-mode.  [[https://www.w3.org/TR/micropub/#new-article-with-html][Micropub accepts html]].
  - I could probably use org-html-export-as-html or org-html-convert-region-to-html (elpa/26.3/develop/org-plus-contrib-20201019/ox-html.el:3775)
- [ ] Or, set up webmentions etc as some layer on top of published org-roam, so WP no longer needed
- [ ] With my current setup, I can't really post from my phone. I mean I can post via micropub to WP, but that I have to PESOS back to my org-roam version.  So what I do is just take it as a note in orgzly, and post it later.  I'm kind of OK with that - I have no need for things to be instant.  That said, it could be nice to bring back micropub into the equation, just because I like it.
- [ ] a good way for getting notes from koreader on my kobo into my garden
- [ ] add a wordcloud
  - e.g. https://www.idkrtm.com/creating-word-clouds-with-python/
- [X] change the capture template to not include timestamp
- [X] setup purge on tailwind ([[id:6afa0673-61b0-40a9-92d6-e0260adfbc35][Purging unused CSS from my org-publish]])
- [ ] reloading a page with stacked notes sometimes gives a 'page not redirecting correctly' error.
- [ ] some links from subfolders (e.g. recent changes) don't work
- [ ] the 'root this page' link doesn't from pages in a subfolder
- [ ] Add some basic night mode styling
- [ ] Use or take some inspiration from [[id:adeea700-86fd-4d13-aa06-3ac9e9f40989][Tufte CSS]]


* Changelog

- 2021-02-21: [[id:17b479fa-6231-4bf7-a4cc-8409a3a699ac][Publishing org-roam via GitLab CI]]
- 2020-11-28: [[id:b17ebf15-bf64-429b-b573-1119245ba4c9][Made myself a logo with PlantUML]]
- 2020-11-21: [[id:cd8d8f7d-317b-4e71-94a9-ca26a04620ef][Updating to org-roam 1.2.3]]
- 2020-10-24: [[id:0e419875-6ba6-40d1-89d4-39139f9fe962][Updating to org-roam 1.2.2]] (to enable wikilink autocompletion)
- 2020-10-24: [[id:b6453d37-1a45-4c75-8f45-9ee0eead07aa][Adding a 404 page with .htaccess]] 
- 2020-10-19: [[id:1746fa27-8c0d-412f-833c-3b6304d2d2e1][Making a recent changes page on my wiki]]
- ...
- 2020-07-20: [[file:2020-07-20.org::*It's Miller time][Add Miller columns view]]
- 2020-03-08: [[https://gitlab.com/ngm/commonplace/-/commit/62c1aa137a3d8a4914398386d0ff7c0ba96fe3af][Started using org-roam]]
- 2019-10-05: [[https://gitlab.com/ngm/commonplace/-/commit/1382224b55b4e6f6cb818bb02afe546982ecdfc4][Started using org-mode and org-publish with intent]]
