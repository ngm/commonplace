:PROPERTIES:
:ID:       54baa5f8-f7b1-412a-ac98-59d6a5c3b7e3
:mtime:    20220816120148 20220816101639 20220816091554 20220815103713
:ctime:    20220815103713
:END:
#+TITLE: UK water shortages 2022
#+CREATED: [2022-08-15 Mon]
#+LAST_MODIFIED: [2022-08-16 Tue 12:03]

In 2022, the UK is experiencing [[id:397830b5-b8f7-4348-8baf-19daef539e8a][drought]]s and [[id:b4179523-aaaf-418d-9454-68eb269c6a71][water shortage]]s.

There has been low rainfall.  [[id:6a7a698b-c817-4dbc-9919-1bb44fd0f0a2][Water privatisation]] in the 1980s has led to a lack of investment in the infrastructure of the [[id:26bf42d3-135f-4407-8e1a-80d4eecc2a82][water supply system]] (e.g. reservoirs and pipes), meaning that we don't collect enough and we leak away a huge amount.

* Why?

** Lack of rainfall

 #+begin_quote
 “Whilst it is not unusual to have periods of low rainfall, we have seen an *extended period of below average rainfall*, particularly in the south-east of England where it was the *third driest November to July on record (from 1836)*. Far from being relieved, the dry conditions intensified in July, with less than 10% of the usual July rainfall recorded across much of the south-east of England (Anglian, Thames and Southern regions each saw their driest July on record, from 1836). The situation has continued into August, with south-east England receiving no rainfall so far this month.”

 -- [[https://www.theguardian.com/uk-news/2022/aug/09/uk-braced-for-drought-conditions-in-october][UK braced for drought conditions to last until October | UK weather | The Gua...]] 
 #+end_quote

** Lack of investment in infrastructure

  #+begin_quote
No substantial reservoir has been constructed in England since the Kielder Water dam was built in 1981.  Thus, our capacity to store fresh water has remained static as demand for it has risen steadily with the growth of the population.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
  #+end_quote

*** Inability to collect water

#+begin_quote
An early impact of climate change has meant that the period 2011-2020 was 9% wetter, in terms of rainfall, than the period 1961-1990. So there is no shortage of water we can collect. Unfortunately, it seems we just lack the urge to do so.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
#+end_quote

*** Inability to store water

3 billion litres lost *daily*

#+begin_quote
For the whole of England and Wales, the daily loss – from leaks and other losses – from all of the two nations’ main water companies is *3bn litres of water*, a fifth of their total supply.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
#+end_quote

* Root cause

[[id:6a7a698b-c817-4dbc-9919-1bb44fd0f0a2][Water privatisation]], essentially.

#+begin_quote
Even worse, analyses indicate that, while water bills in England have risen noticeably over the past three decades, spending on improved infrastructure by water companies has, at best, flatlined or declined, depending on how you break down the figures. Hence the intensity of our current drought, it is argued.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
#+end_quote

#+begin_quote
At the same time, huge dividends have been paid out to water company shareholders while their chief executives have been generously rewarded for their work.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
#+end_quote

#+begin_quote
These company chiefs have been the main beneficiaries of the privatisation of England’s water companies, which was imposed by the Conservative government in 1989.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
#+end_quote

* Side effects

** Affecting groundwater sources
   #+begin_quote
   First, it has forced us to pump ever more water from groundwater sources. This has stressed aquifers, lowered the water table in many areas and threatened chalk streams. This last hazard is particularly vexing because chalk streams are some of the planet’s rarest habitats – and the vast majority are in England.

-- [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
   #+end_quote

** Hosepipe bans

Water companies are asking citizens to reduce their water usage to alleviate the water shortages.
