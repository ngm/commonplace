:PROPERTIES:
:ID:       eeb51d4f-e015-4895-949f-6bf5c1248335
:mtime:    20220821223138 20220526114510
:ctime:    20220526114510
:END:
#+TITLE: climate modelling
#+CREATED: [2022-05-26 Thu]
#+LAST_MODIFIED: [2022-08-21 Sun 22:31]

#+begin_quote
Climate models are planetary climate forecasts that span decades, or more. They use equations to represent the processes and interactions driving our climate across oceans, land masses, ice-covered regions and within the atmosphere. Crucially, they try to predict the outcomes that different types of human activity will have for global heating trends.

Today’s climate models are often run on massive super-computers, some capable of cumulatively generating up to 14,000 trillion calculations per second.

-- Down to Earth newsletter
#+end_quote
