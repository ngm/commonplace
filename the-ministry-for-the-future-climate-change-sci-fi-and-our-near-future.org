:PROPERTIES:
:ID:       fa1a35f2-a65e-410b-9b69-30a5c9672cb2
:mtime:    20220918025350 20220806100953
:ctime:    20220806100953
:END:
#+TITLE: The Ministry for the Future: Climate Change, Sci Fi, and our Near Future
#+CREATED: [2022-08-06 Sat]
#+LAST_MODIFIED: [2022-09-18 Sun 02:54]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://revolutionaryleftradio.libsyn.com/ministry-for-the-future

[[id:0d39a319-c863-45b2-9403-2918bb40ded8][Kim Stanley Robinson]] and [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]].

#+begin_quote
We are joined by Kim Stanley Robinson, the renowned science fiction author, to discuss his NYT bestselling book "The Ministry for the Future". We discuss [[id:d5e4abd3-fcd6-48eb-a6aa-303498ada7a5][climate change]], [[id:68b920fd-0052-447c-96ec-7f58493f0720][geoengineering]], [[id:db2f97b7-2680-446e-a226-6c4182a53e82][Fredric Jameson]], anti-dystopian fiction, political terrorism, [[id:d701985a-67fe-48a3-8658-578511ec4008][Capitalist Realism]], [[id:4998a12f-0133-4c39-8be9-65f435119aaf][democratic socialism]], the boomer/millennial divide, and so much more!
#+end_quote

+ ~00:07:15  Nice quick political bio of Robinson.
+ ~00:09:18  Any mid to near future science fiction has to take climate change in to account.
+ ~00:11:59  Capitalist realism and science fiction.

