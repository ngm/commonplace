:PROPERTIES:
:ID:       2fea36aa-f5f0-4c68-9e6a-3d3a4f980ca8
:mtime:    20240601111407 20240419112314
:ctime:    20240419112314
:END:
#+TITLE: GDPR
#+CREATED: [2024-04-19 Fri]
#+LAST_MODIFIED: [2024-06-01 Sat 11:14]

* Do you think GDPR and other data regulations around the globe are enough? What collective rights over data can you propose?

No, individual-focused regulations such as GDPR are not enough.  They are a step in the right direction, as they begin to move the needle away from corporate ownership of our data.  But they focus on individual data and individual consent, and do not cover aggregate data.  As stated in the reading materials: "the pitfalls of such a logic is that it renders data into private property and obscures the structural crisis of data capitalism by reducing it to a “choice” to share/not share one’s own data." (https://digitalcapitalismcourse.tni.org/mod/book/view.php?id=32&chapterid=58)

Data regulations should also be pushing for public, collective ownership of our data.  Initiatives such as data commons and data trusts should be supported.
