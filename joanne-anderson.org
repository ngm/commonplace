:PROPERTIES:
:ID:       8575b934-2382-4f55-801b-bb71958d2652
:mtime:    20211127120905 20210724222235
:ctime:    20210724222235
:END:
#+title: Joanne Anderson
#+CREATED: [2021-05-08 Sat 11:50]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

UK’s first directly elected black female mayor (in 2021).
Socialist.
[[id:cd47c6e3-a67a-439d-a479-4a3020513fe0][Liverpool]].

- [[https://www.theguardian.com/politics/2021/may/07/liverpool-chooses-uks-first-directly-elected-black-female-mayor][Liverpool chooses UK’s first directly elected black female mayor | Elections ...]] 

