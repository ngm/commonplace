:PROPERTIES:
:ID:       c76405a5-850b-46c7-87a0-1ca7a26152db
:mtime:    20230915143412 20230915111627
:ctime:    20230915111627
:END:
#+TITLE: Design Justice Network
#+CREATED: [2023-09-15 Fri]
#+LAST_MODIFIED: [2023-09-15 Fri 14:35]

+ URL :: https://designjustice.org/

[[id:642b4b21-c46d-45d5-8dee-30877198d0dc][design justice]]

#+begin_quote
The Design Justice Network is a home for people who are committed to embodying and practicing the Design Justice Network Principles.
#+end_quote

#+begin_quote
We wield our collective power and experiences to bring forth worlds that are safer, more just, more accessible, and more sustainable. We uplift liberatory experiences, practices, and tools, and critically question the role of design and designers.
#+end_quote

#+begin_quote
Rooted in a sense of abundance, possibility, and joy, we provide connection, care, and community for design justice practitioners.
#+end_quote
