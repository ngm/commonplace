:PROPERTIES:
:ID:       86d7e16f-e11b-40d0-b5ae-7fd50c3a6257
:mtime:    20211127120906 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Century of the Self

Watching Century of the Self, first time I have heard of the Kent State shootings. Astonishing – the US state firing on an unarmed student protest. According to the documentary, it had a suffocating effect on the protest movement.

Final episode of Century of the Self. The culmination of psychoanalysis and public relations inserting themselves into politics, with Clinton and Blair basing most of their policies on feelings-based focus groups of swing voters. Selfish desires trumping feelings of altruism, denigration of welfare, individualism over society. The populace treated as needy consumers, not engaged citizens.

Really good documentary by Adam Curtis. Entertaining and informative.
