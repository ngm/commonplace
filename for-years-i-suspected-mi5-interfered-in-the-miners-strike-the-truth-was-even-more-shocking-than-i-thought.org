:PROPERTIES:
:ID:       649aeb64-102e-4ffe-8429-9ad279394cb0
:END:
#+title: For years, I suspected MI5 interfered in the miners’ strike. The truth was even more shocking than I thought

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]]
+ Found at :: https://www.theguardian.com/commentisfree/2024/mar/07/mi5-miners-strike-national-archives-security-service-government

[[id:3cfc2d79-f25d-4f0a-9936-86eba1438310][Miners' strike]].

#+begin_quote
A document buried in the National Archives reveals how the security service abused its power to help the government win
#+end_quote
