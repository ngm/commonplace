:PROPERTIES:
:ID:       ba734016-9be0-4ee0-b19c-776d995eee38
:mtime:    20211127120936 20210724222235
:ctime:    20210724222235
:END:
#+title: Primitive accumulation

Comes as this point of transition to [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]] from late [[id:8086d14e-26ee-40bb-8706-0e2277944a5f][feudalism]], when land is enclosed etc. Land property wealth into hands of capitalist class, that's what made them capitalists.  Violence.

See [[id:9ab1a71d-1c09-4fcc-8ec6-4c4d73452949][enclosure of the commons]].  An artificial construction of an urban workforce to support capitalism.

See [[id:155ce756-bc6c-4bc2-a7ed-35ec2226a92c][Caliban and the witch]].

* Resources

  - [[https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy][The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy]] 
