:PROPERTIES:
:ID:       41715769-6f94-4083-bee6-7e8edad7e55e
:mtime:    20221008120539
:ctime:    20221008120539
:END:
#+TITLE: Alternatives for the Internet: A Journey into Decentralised Network Architectures and Information Commons
#+CREATED: [2022-10-08 Sat]
#+LAST_MODIFIED: [2022-10-08 Sat 12:05]

+ URL :: 
https://triple-c.at/index.php/tripleC/article/view/1201/1379
+ Journal :: [[id:c336b15e-2d44-44f8-a47a-2f5cfe4ec020][Communication, Capitalism & Critique]]
