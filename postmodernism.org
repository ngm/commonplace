:PROPERTIES:
:ID:       2db89aa0-38bb-4eea-ae75-3fff2343b481
:mtime:    20211219165939 20211127120818 20210724222235
:ctime:    20200601191127
:END:
#+TITLE: postmodernism

Bit of an overloaded and slippery term.  Seems to be used in lots of different ways.  In some uses it seems like a decent riposte to modernity and rationalism.  Othertimes it's [[id:e6abd35c-3bfe-4c95-93ba-cd16ca82adae][The cultural logic of late capitalism]]. Another conception of it seems to be that its about irony and a lack of sincerity.

Maybe it's all of these, depending on context (it would probably quite like that).

#+begin_quote
A series of critiques by a school of French philosophers coalesced into a movement known as postmodernism, which attacked the notion that objective truths could be applied universally under the rubric of such capitalized abstractions as Truth, Science, Reason, and Man. Included in this attack was the tradition of “cultural essentialism,” by which Northrop and those who followed him had sought to ascribe a particular set of universal characteristics to the Orient, the West, or, for that matter, any racial or cultural stereotype.

-- [[id:2db61895-7843-4f76-a591-1d579a7c9545][The Patterning Instinct]]
#+end_quote

#+begin_quote
In contrast to the modernist view of the world, which had emerged with the Scientific Revolution, the postmodernists proposed that reality is constructed by the mind and can never therefore be described objectively. Each culture, they argued, develops its own version of reality that arises from its specific physical and environmental context. 

-- [[id:2db61895-7843-4f76-a591-1d579a7c9545][The Patterning Instinct]]
#+end_quote

#+begin_quote
The postmodernists accused Westerners who had attempted to do so of engaging in a form of [[id:f7181203-8997-47ab-9509-66d974334c01][cultural imperialism]], seeking to appropriate what seemed valuable in other cultures for their own use while ignoring its historical context.

-- [[id:2db61895-7843-4f76-a591-1d579a7c9545][The Patterning Instinct]]
#+end_quote

#+begin_quote
A more useful investigation, according to the postmodernist critique, would be to recognize the multiplicity of discourses created by various cultures and, rather than try to distill some essential meaning from them, trace how certain social and political groups used these discourses to maintain or enhance their own power relative to others.

-- [[id:2db61895-7843-4f76-a591-1d579a7c9545][The Patterning Instinct]]
#+end_quote


#+ATTR_HTML: :width 100% :alt One of Andy Warhol's Diamond Dust Shoes.  Flattened, brightly coloured women's shoes.
[[file:2020-06-07_18-23-54_diamond-dust-shoes.jpg]]


#+begin_quote
rejection of the "universal validity" of binary oppositions, stable identity, hierarchy, and categorization.

-- [[https://en.wikipedia.org/wiki/Postmodernism#Manifestations][Postmodernism - Wikipedia]] 
#+end_quote

#+begin_quote
[[id:53675183-4947-47e8-b328-fac299313391][Jane Jacobs]]' 1961 book The Death and Life of Great American Cities was a sustained critique of urban planning as it had developed within Modernism and marked a transition from modernity to postmodernity in thinking about urban planning

-- [[https://en.wikipedia.org/wiki/Postmodernism#Manifestations][Postmodernism - Wikipedia]] 
#+end_quote

