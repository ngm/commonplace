:PROPERTIES:
:ID:       7f2450d7-d883-4844-bab0-c0f3734adae8
:END:
#+title: French repair index

Been having fun looking at [[id:e9c05fc2-f19c-4d61-9dc6-84b2c58dec1a][repairability scores]] from the [[id:7f2450d7-d883-4844-bab0-c0f3734adae8][French repair index]] as displayed on Amazon.fr.

Impressive how prominent they are (being mandated to be placed next to the price).

Important to bear in mind the caveats around France’s index as it currently stands (e.g. here and here).

I see that Fairphone 4 gets a 9.3 (Amazon link), Samsung Galaxy S24 gets an 8.5 (Amazon link), iPhone 15 Pro gets a 7.6 (Amazon link). Based on that I’m not sure that the repair scores as they stand would be enough of a signal to compel or deter a purchase of a particular item.

As mentioned in the above article on repair.eu:

Overall, most displayed grades are quite good (between 6 and 8/10). In order to reflect the true repairability of products, HOP recommends a revision of the scoring system by questioning the weighting of the various criteria. Indeed, while a very low score on a criterion such as disassembly, availability or cost of spare parts means repair is almost impossible, the current grading system allows a product to still get a decent grade, despite performing poorly on the criteria above.

One year on, has the French repair index kept its promises?

A stronger repair index displayed so prominently would be amazing
