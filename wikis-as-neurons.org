:PROPERTIES:
:ID:       26ed32b8-7988-4ba5-bf85-7b1e70613e7f
:mtime:    20240428082949 20211127120815 20211016154757
:ctime:    20211016154757
:END:
#+TITLE: Wikis as neurons 
#+CREATED: [2021-10-16 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

When you 'copy' the idea from another [[id:db75b03e-2c1a-4eac-a896-64eeb2a7e95e][wiki]], and have it as a bi-link, that's like a connection between the two.  A bridge where someone might jump from one to the other.

I had this thought as I had copied antistatic gardens concept.

Made me think of synaptic gaps.

Wikis that fire together, wire together?

Interwiki bidirectional links as the synaptic connections?

Networks of wikis, like a neural network?

Bit tenuous, but fun to think on.
 
Parallels a lot of what I thought previously about Wikis being neuronal. 

I wonder if maybe just another avenue of approaching that is the Wikis being rhizomatic connections of Wikis we need connections of wiki sites, blogs, blikis, wiki logs whatever you want to call it, but to facilitate those connections between them is important, very important. 
