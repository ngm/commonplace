:PROPERTIES:
:ID:       ac7e3737-91f7-4e4e-b4fc-0ca0032392cc
:mtime:    20220830201828 20211127120928 20210825115842
:ctime:    20210825115842
:END:
#+TITLE:All Watched Over by Machines of Loving Grace (Adam Curtis)

Watched the first part of [[id:6437a525-85b3-4ae7-bf9e-beae5e72de85][Adam Curtis]]’ All Watched Over by Machines of Loving Grace documentary.

Very enjoyable. I have to say that a lot of times in Curtis’ documentaries I feel like if it was a Wikipedia article it would say ‘citation needed’. And I’m definitely picking up on certain Curtis tropes the more of his documentaries that I see. (“They thought it was doing XYZ…….. But it wasn’t.” Discordant music, long shot on someone’s face. Quick cut to silly music and image. etc etc)

All that said, it’s entertaining, and I’m sure there’s something of merit to all of his theses, and it definitely makes you think about the broad strokes of recent history and how they link together. And the soundtrack’s great.

So the theme in AWObMoLG pt.1 is [[id:1babd5b7-0800-45d7-b2aa-46bb01ac6ed2][individualism]] I guess, like Century of the Self. Main protagonists so far being [[id:be965ab7-d497-4c13-973d-9c9f69cbd828][Ayn Rand]], Alan Greenspan, and the early Silicon Valley tech utopians. I guess the thread is that Randian heroic selfishness bleeds into both finance and tech, leading to an overconfidence in algorithms, to free marketeers in positions of power, and the belief that weird financial shit like risks and hedging is all good for the healthy pursuit of one’s money. It leads to the various market fuck-ups of the recent decades, along with state bailouts of banks, paid for by citizens around the world.

Bill Clinton makes a bit of a cameo, appearing generally useless, and responsible for letting the money changers into the temple.

I briefly ended up feeling.. sorry (..or perhaps pity) for Ayn Rand, which I certainly wasn’t expecting to happen. Not that she would want that, anyway.

The funniest/weirdest bit is Curtis heckling Barbara Branden with a shout of “That’s altruism!” when she reveals she let Rand have an affair with her husband (Mr. Self-Esteem Nathaniel Branden) because she felt sorry for Rand. And Barbara Branden gets defensive, but it seems more at the accusation of altruism than at the marital complexities. Odd stuff.
