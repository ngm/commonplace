:PROPERTIES:
:ID:       c2aade63-3331-40d5-b136-ceccae87da34
:mtime:    20231008103107
:ctime:    20231008103107
:END:
#+TITLE: Technofeudalism: What Killed Capitalism?
#+CREATED: [2023-10-08 Sun]
#+LAST_MODIFIED: [2023-10-08 Sun 10:32]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Author :: [[id:12357f53-8bb2-497f-86f6-00b6353a44ac][Yanis Varoufakis]]

On [[id:ac0e4d66-7b99-444b-86b4-4b4d986c833c][technofeudalism]].
