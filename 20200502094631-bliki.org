:PROPERTIES:
:ID:       a0d7cf5a-5d56-4f39-a06d-e90943a26bb5
:mtime:    20211127120920 20210724222235
:ctime:    20200502094631
:END:
#+TITLE: Bliki

I learned ([[https://desmondrivet.com/2020/05/01/wikis-and-blogs][via Desmond]]) that Martin Fowler has a [[https://martinfowler.com/bliki/WhatIsaBliki.html][post about the link between blogs and wikis]], from 2003(!). 

He calls [[id:ffbea278-7011-49d6-8c1e-c784d40abb6b][the blog and wiki combo]] his bliki.

He wants more than a blog: 

#+begin_quote
Beyond the name, however, there's the very ephemeral nature of blog postings. Short bursts of writing that might be interesting when they are read - but quickly age. I find writing too hard to want to spend it on things that disappear. 
#+end_quote

*"I find writing too hard to want to spend it on things that disappear"* - I love that as a little epigram for why you might want a [[id:92f8694f-8482-4541-9e6f-8ce03f59e01f][digital garden]]. 

#+begin_quote
Like a blog, it allows me to post short thoughts when I have them. Like a wiki it will build up a body of cross-linked pieces that I hope will still be interesting in a year's time.
#+end_quote

As a word, I'm not so keen on 'bliki' (although back then Martin didn't like the word 'blog', and well here we are now, I don't give it a second thought).  

But blikis as a concept - I'm all in.

#+begin_quote
Yeah. That's a massive set of issues ... one thing that I've been thinking about a lot is exactly the way the web moved from stocks to flows. And to increasingly fast, fragmented streams ... and how wiki needs to evolve to cope.

I had a debate back 15 years ago with Bill  about what in those days we called "bliki" (mixture of blogs and wikis) Bill's software combines blog-like journal with wiki. And I think it's been very successful. 

My argument against him at the time was that addressing knowledge by "topic" (which is what wiki pagenames are) vs. addressing it by date were two incompatible things. And that addressing by topic was better (because you could guess at meaningful concrete pagenames to make links rather than have to look them up (or do complicated "typeahead" searching which wasn't much thought of back in the early 2000s)) ...

Today I think I was half right and half wrong ... Bill has successfully captured a lot of time-related stuff in his bliki, and I never managed to capture it in my wiki. (Because it was always put "temporarily" in blogs and social media)

At the same time, despite how temptingly easy they are ... I think time / journal based information DOES eventually just get lost and forgotten. That's what we have all experienced in social media like Facebook and Twitter and (let's be ready for it) Telegram. Chat apps are fantastically immediate to capture information but terrible to search.

So we are still looking for the holy grail of easy to capture + easy to retrieve / edit. Wiki has one profile with strengths and weaknesses. Blogs had another. We need more good ideas. 

My main contention continues to be that the hard problem is overcoming the inconsistent / incompatible addressing conventions of the different chunks.

-- Phil Jones, Digital Gardeners telegram
#+end_quote

#+begin_quote
I still believe in OneSoup to maximize fluidity.

When I remember to do MorningPages style Journaling, I still keep it smooshed in my Weekly page.

But if I have a thought I want to stick with, I give it its own page, even if it's only 1 paragraph. And I link as usual, so it will be refindable forever via backlinks.

Gardening a thought often involves scanning multiple backlinks lists, ending up with 10 pages open (so I prefer separate browser windows vs the inline subwindow fashion).

-- Bill Weitz, Digital Gardeners telegram
#+end_quote
