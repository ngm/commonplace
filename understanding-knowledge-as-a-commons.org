:PROPERTIES:
:ID:       8224200c-f25c-4586-bad1-8c06d07f2208
:mtime:    20211211162353
:ctime:    20211211162353
:END:
#+TITLE: Understanding Knowledge as a Commons
#+CREATED: [2021-12-11 Sat]
#+LAST_MODIFIED: [2021-12-11 Sat 16:24]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ URL :: https://mitpress.mit.edu/books/understanding-knowledge-commons
