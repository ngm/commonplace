:PROPERTIES:
:ID:       2205f3df-42fa-42b8-94e4-50cb22e088c7
:mtime:    20240506064600
:ctime:    20240506064600
:END:
#+TITLE: data
#+CREATED: [2024-05-06 Mon]
#+LAST_MODIFIED: [2024-05-06 Mon 06:55]

#+begin_quote
Data is the foundational resource of the modern economy

-- [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]]
#+end_quote

#+begin_quote
UNCTAD projections from 2019 suggesting that data-intensive businesses will account for 70 percent of the new value generated in the global economy over the next decade

-- [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]]
#+end_quote

Data is made through [[id:ae7a1d00-ea43-461c-adde-53d3b75d4fae][datafication]].

 #+begin_quote
 While many lenses and frames exist to conceptualize data, framing data as a form of capital allows us to understand the ways in which value can be derived from data, thus helping us unpack the motives behind its use by corporations and why data extraction within has become central to the economy4. Data as a form of capital is distinct from, but has roots in economic capital. Much like social and cultural capital, data capital is convertible, under certain conditions to economic capital.

 -- [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]]
 #+end_quote
