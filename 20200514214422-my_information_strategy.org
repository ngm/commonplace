:PROPERTIES:
:ID:       f1c5f02b-5e94-4cb4-8910-defd2daa63c7
:mtime:    20211127121011 20210724222235
:ctime:    20200514214422
:END:
#+TITLE: My information strategy

[[id:922a74d8-b8d5-4238-b26b-216e79dd6c0b][information strategy]]

I find most content to read via the big stream.

* Social reader / big stream
The big stream is basically a bunch of people that I follow.  

I don't use Twitter for my reader.  I use a social reader for this.  I follow people on their personal websites, or on Twitter, and read all these posts in the reader.

I wrote a bit more about how I organise feeds in my reader [[https://doubleloop.net/2019/10/05/discovery-strategy/][here]].

* Replying / storing

Usually what I see in the big stream are either ideas from someone that I might want to write a reply to, or a link to an article I might want to read later.  I save articles in to Wallabag.  A short reply I'll write there and then, a longer one I will probably save the post into my note-taking app.

* [[id:abcd71aa-9a1f-4df3-b637-7c05ab1416fb][Note-taking]]
  
My fleeting notes I take with either [[id:c905ea35-21b7-44bb-ba78-094d3e02ccb9][org-capture]] or [[id:22b57ec1-2c19-4628-a651-893d1034696b][orgzly]], depending where I am.  With my literature notes I write in [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]] and try to do the [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]] approach.  Permanent notes I use [[id:20210326T232003.148801][org-roam]].
