:PROPERTIES:
:ID:       f88c4224-2e05-4a39-b0c2-5d638b4c4a54
:mtime:    20220524083749
:ctime:    20220524083749
:END:
#+TITLE: Public-civic collaboration, coop incubators and stewarding digital infrastructure as a commons
#+CREATED: [2022-05-24 Tue]
#+LAST_MODIFIED: [2022-05-24 Tue 08:48]

https://forum.meet.coop/t/session-8-discussion/1003

Sophie Bloemen from [[id:134c274d-b534-4d5a-bb66-b33379611566][Commons Network]].

[[id:d1fce7db-383b-4e56-8678-197bd50cad1d][Coop incubators]].
[[id:0698b5ab-74ea-4165-b707-4ad0bfccf76a][NGI Forward]].
[[id:1e8c78e7-b5cc-4a93-b281-27851affc1a8][Generative interoperability]].

Individual -> community
Extractive -> regenerative
Competition -> cooperation
Linear -> ecosystem

Commons / cooperative economy.

Need governmental support at all levels.

[[id:56127945-35f5-473f-8000-597498da2ad4][Public-collective partnerships]] 
