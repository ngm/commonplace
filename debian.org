:PROPERTIES:
:ID:       d819826b-1a39-437a-a59d-8aa61fdbc63b
:mtime:    20211127120956 20210724222235
:ctime:    20210724222235
:END:
#+title: Debian
#+CREATED: [2020-12-13 Sun 13:41]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:35b859d8-0ff2-4614-89e8-257f5a2511ec][GNU]]/[[id:121e6c4d-e58b-46ca-8c02-42ab7a84d2f1][Linux]].

* Values
  
+ [[https://www.debian.org/devel/constitution][Debian Constitution]]   
+ [[https://www.debian.org/intro/diversity][Diversity Statement]]
+ [[https://www.debian.org/social_contract][Debian Social Contract]]
+ [[https://www.debian.org/code_of_conduct][Debian Code of Conduct]] 
