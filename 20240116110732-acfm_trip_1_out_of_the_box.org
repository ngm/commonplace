:PROPERTIES:
:ID:       ac959887-5013-4366-8a7c-d4882ef7c14a
:END:
#+title: #ACFM Trip 1: Out of the Box

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ Found at :: https://novaramedia.com/2018/11/02/trip-1-out-of-the-box/
+ Part of :: [[id:06aa3744-682c-4a2a-a437-0df373c7ebef][#ACFM]]

  [[id:1fa6ce8a-e3c5-48da-bc79-84d0ef966f0f][Acid communism]].
