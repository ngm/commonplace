:PROPERTIES:
:ID:       6037c537-e916-4ccf-a334-a07e1c98e15e
:mtime:    20220115170628
:ctime:    20220115170628
:END:
#+TITLE: Cuba is world-leading in agroecology
#+CREATED: [2022-01-15 Sat]
#+LAST_MODIFIED: [2022-01-15 Sat 17:08]

[[id:94a8b80f-e5c4-44f3-ab3b-fc16becf1baa][Cuba]] is world-leading in [[id:fdd64cf0-e6fa-481d-b4d1-807686bfe3f0][Agroecology]].

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:a7800287-cff2-458c-abdd-873f2c05c944][Ask again later]]
+ Source (for me) :: [[id:6cc6c063-6cf7-4c1c-baf1-c4924bb0c8d6][Salvatore Engel-Di Mauro]] [[id:7457e62f-23f1-41ad-ac05-bb763b40bbc3][Socialist States & the Environment w/ Salvatore Engel-Di Mauro]]

Don't see why not, but I would like to fact check a bit before espousing.  I also don't *really* know what agroecology is.
