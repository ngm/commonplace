:PROPERTIES:
:ID:       c843ca7a-d8ae-4454-a4af-b1f16f3ac387
:mtime:    20220828221945
:ctime:    20220828221945
:END:
#+TITLE: Cuba is the most sustainably developed country in the world
#+CREATED: [2022-08-28 Sun]
#+LAST_MODIFIED: [2022-08-28 Sun 22:28]

[[id:94a8b80f-e5c4-44f3-ab3b-fc16becf1baa][Cuba]] is the most [[id:2b4c5fa8-9bc6-4580-9ec5-f779b988a3bc][sustainably developed]] country in the world.

According to the [[id:edd2c228-2f2d-4f49-bca3-85f26851ff18][Sustainable Development Index]].
Via [[https://morningstaronline.co.uk/article/w/cuba-found-to-be-the-most-sustainably-developed-country-in-the-world-new-research-finds?fbclid=IwAR36RqEUws0RyRcis1IgRbTkjDV9QF_GYsogziiW0RvKEcrAmPID60wGQ-c][Cuba found to be the most sustainably developed country in the world, new res...]].

Btu that was reported in 2020.  If you look at the SDI rankings though in 2022, it isn't currently.  It's in fifth, still not bad.  Costa Rica is top.

United States, which has subjected Cuba to a punitive six-decades-long economic blockade

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:353d8e39-495c-4be4-a1ba-38419377555f][Signs point to yes]]
