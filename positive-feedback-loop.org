:PROPERTIES:
:ID:       2785bd79-d56b-4e91-9cad-e6ad38a1288c
:mtime:    20230624211245 20220730101227 20220729214540
:ctime:    20220729214540
:END:
#+TITLE: positive feedback loop
#+CREATED: [2022-07-29 Fri]
#+LAST_MODIFIED: [2023-06-24 Sat 21:12]

* Positive feedback loops in the environment

#+begin_quote
When rising temperatures alter the environment in a way that intensifies the rate of overheating, a positive feedback loop is created. For instance, the melting of permafrost in Arctic tundra may trigger a release of methane from frozen hydrates. Methane is a highly potent greenhouse gas and its increased concentration in the atmosphere will likely speed the rate of global heating – so releasing more methane. 

-- Down to Earth newsletter
#+end_quote

^ Positive as in reinforcing.  *Not* positive in general.

#+begin_quote
Positive feedback is any process where the result magnifies the cause: an example is the screech which comes from a sound system when the signal from the speaker is itself picked up by the mic, which feeds it through to the speaker, forming a continuous loop.

-- [[id:27e0ccb4-eea7-48f5-87ff-2ad9af25ae4a][The Entropy of Capitalism]]
#+end_quote
