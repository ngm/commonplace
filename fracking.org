:PROPERTIES:
:ID:       3e1c8344-fa1e-4432-bba0-2a22de61adab
:mtime:    20220708113315
:ctime:    20220708113315
:END:
#+TITLE: Fracking
#+CREATED: [2022-07-08 Fri]
#+LAST_MODIFIED: [2022-07-08 Fri 11:33]

#+begin_quote
Fracking is also deeply unpopular with the public and given that any shale gas extracted would have to be sold at international market prices, it would have no impact on UK fuel bills

[[https://www.theguardian.com/commentisfree/2022/mar/06/observer-view-on-ukraine-and-climate-emergency][The Observer view on Ukraine and the climate emergency | Observer editorial |...]]
#+end_quote

