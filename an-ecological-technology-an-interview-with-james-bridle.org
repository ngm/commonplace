:PROPERTIES:
:ID:       a4ad3c2e-4efd-47e6-8c3f-a91247a0b04e
:mtime:    20230428140048
:ctime:    20230428140048
:END:
#+TITLE: An Ecological Technology: An Interview with James Bridle
#+CREATED: [2023-04-28 Fri]
#+LAST_MODIFIED: [2023-04-28 Fri 14:16]

+ URL :: https://emergencemagazine.org/interview/an-ecological-technology/

Transcript of an interview with [[id:e3ec9035-76a2-4df2-a7e0-31ef08b51c12][James Bridle]].

Mainly around topics covered in [[id:60a7b7ec-18fa-430a-bb2f-4b731ce257d1][Ways of Being]].  But a little bit from [[id:fdbad94e-bcc1-4d04-88ad-4c8e454e88ab][New Dark Age]] too.

Although I am surprisingly uninterested in [[id:d4cf17cd-08ef-4676-8747-75d52f2e0edf][artificial intelligence]] as a topic these days, maybe it's just the general discourse around it.  Ways of Being does sound it'll be more up my street - comes across much more in line with an [[id:0b43d410-b347-4e06-851c-ce9a2abde212][Evolutionary and adaptive systems]] vibe - i.e. importance of [[id:270c7185-942c-4873-9c78-9ebc12ee72a4][embodiment]], environment, [[id:c1efafbe-920d-4c76-b1c8-19f6c86fab29][relationality]].

Interested to learn more what Bridle means by [[id:9447b3f9-6ef1-4131-91ce-8acab49b14a4][Ecology of technology]].
