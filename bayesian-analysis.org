:PROPERTIES:
:ID:       d80a493d-bd00-4aaa-98be-8f8518fac0da
:mtime:    20211229102045
:ctime:    20211229102045
:END:
#+TITLE: Bayesian analysis
#+CREATED: [2021-12-29 Wed]
#+LAST_MODIFIED: [2021-12-29 Wed 10:22]

#+begin_quote
derives degrees of certainty which are interpreted as a measure of subjective psychological belief. 

-- [[https://en.wikipedia.org/wiki/Certainty][Certainty - Wikipedia]] 
#+end_quote
