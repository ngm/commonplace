:PROPERTIES:
:ID:       83bf4242-4f91-4adc-b592-99c523ed76cf
:mtime:    20220116202115
:ctime:    20220116202115
:END:
#+TITLE: There will be a fresh wave of Omicron cases in the early summer
#+CREATED: [2022-01-16 Sun]
#+LAST_MODIFIED: [2022-01-16 Sun 20:28]

So sayeth Sage in The Guardian.

So sometime around June presumably.

* Because

+ People will resume social habits
+ Immunity will be waning 

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]
+ Source (for me) :: [[id:7f5a7ae4-7af4-44e8-876f-d1d83fb4c43d][The Guardian]] / [[id:f4331945-97ef-4d0a-ace9-8fff079d210d][Sage]] / [[https://www.theguardian.com/world/2022/jan/14/expect-another-covid-omicron-wave-in-early-summer-sage-says][Expect another Omicron wave in early summer, Sage says | Omicron variant | Th...]] 

