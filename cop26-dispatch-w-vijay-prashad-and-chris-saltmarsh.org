:PROPERTIES:
:ID:       07d0bcfe-c154-4190-818a-27a6902636c3
:mtime:    20211127120757 20211122220636
:ctime:    20211122220636
:END:
#+TITLE: COP26 Dispatch w/ Vijay Prashad and Chris Saltmarsh
#+CREATED: [2021-11-22 Mon]
#+LAST_MODIFIED: [2021-11-27 Sat 12:07]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://guerrillahistory.libsyn.com/cop26-dispatch-w-vijay-prashad-and-chris-saltmarsh
+ Series :: [[id:53d6a7e6-eff1-469d-b842-56cb7a13f5fe][Guerrilla History]]
+ Featuring :: [[id:a4a66be7-506d-41e2-9165-f659a0950322][Vijay Prashad]]

[[id:28c04890-c035-49ce-b120-17c43260e95f][COP 26]].

Not finished it yet but [[id:a4a66be7-506d-41e2-9165-f659a0950322][Vijay Prashad]] is an engaging speaker.  Gives interesting history of Glasgow and slavery and how the main COP was held in the Docklands part.
