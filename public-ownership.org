:PROPERTIES:
:ID:       52a0b7a0-b65c-486c-8166-969d9b273279
:mtime:    20220816144424 20220603162112
:ctime:    20220603162112
:END:
#+TITLE: public ownership
#+CREATED: [2022-06-03 Fri]
#+LAST_MODIFIED: [2022-08-16 Tue 14:49]

An alternative of [[id:2a4b9236-04dc-4361-b4f9-2d331cb60adf][privatisation]].

Often equated with state ownership and [[id:fc1b15d2-f3be-4735-a436-6f07482dd594][nationalisation]].

That's one option - also municipal and community-based ownership is a type of public ownership.  The commons.
