:PROPERTIES:
:ID:       698c9f4c-c33f-4262-bec1-86e61eea368f
:mtime:    20230430151848
:ctime:    20230430151848
:END:
#+TITLE: word counts in org-mode
#+CREATED: [2023-04-30 Sun]
#+LAST_MODIFIED: [2023-04-30 Sun 15:39]

Not that great of an experience out of the box, as far as I could tell.

I tried a few different things.

The org-wc package, and the wc-mode package.

* org-wc

org-wc is good because it has put some effort into only counting the relevant words in an org file.  e.g. it will ignore subheadings marked as COMMENT, or it'll ignore things in #+begin_comment blocks.  This is exactly what I need.

It also can display word count per headline, which is also really useful.

But it feels a bit flaky in the activation of these word counts.  org-wc-display is handy, but I found it a bit annoying that it is not persistent.  Also I'd like to see these counts in the imenu sidebar.  org-wc-count-subtrees doesn't seem to work at all for me.

* wc-mode

This displays a nice little running word count in your mode line.  However, it naively counts all words.

Someone [[https://github.com/tesujimath/org-wc/issues/16#issuecomment-473281085][here]] says you can overwrite the count function to use the one from org-wc (org-word-count-aux), but it didn't work for me as is.

In principle it should definitely be possible: https://github.com/bnbeckwith/wc-mode#counting-functions

It's something about using it with org-word-count-aux that is the problem. I hacked around a bit and got it to partially work by hardcoding a value for rstart, but then it stopped working again and I got fed up.

* Tweaks

So then I thought I'd try running org-wc-display on a timer, so as to keep it permanently displayed at least.  I did that, but it is set to clear itself everytime you make a new insert, which resulted in an annoying flickering interaction between that clearing and my timer running.

So in the end, I just set a keybinding for org-wc-display so I can run it easily whenever I want.

#+begin_src elisp
(spacemacs/declare-prefix "o" "own-menu")
(spacemacs/set-leader-keys "ow" 'org-wc-display nil)
#+end_src
