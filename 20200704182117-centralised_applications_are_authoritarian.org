:PROPERTIES:
:ID:       6122eb2b-bc8b-4378-84e4-bf169069fea5
:mtime:    20211127120845 20210724222235
:ctime:    20200704182117
:END:
#+title: Centralised applications are authoritarian

#+begin_quote
Centralized applications emerge in the “Thin Client / Thick Server” model of networking. 

-- [[id:f27371b3-b837-49cb-a8df-30bfd9355b03][information civics]] 
#+end_quote

In thin client/thick server models, the server does all the work and has all the power.  Any authority for the user to do something is borrowed from the server (which can also remove it at any point).

#+begin_quote
The society of users in thick-server applications route their actions through the server. When they message each other, they are updating entries in the server’s database. When they publish files, they are writing those files to the server’s disks. The server has ultimate authority over those systems. Users can access the interfaces provided by the server to control them, but the server may override a user’s choice at any time. All authority is borrowed from the server, and so the users possess no authority of their own. As a result we must describe these services as authoritarian.
#+end_quote

#+begin_quote
Unfortunately, cloud apps are problematic in this regard. Although they let you access your data anywhere, all data access must go via the server, and you can only do the things that the server will let you do. In a sense, you don’t have full ownership of that data — the cloud provider does.

-- [[https://www.inkandswitch.com/local-first.html][Local-first software: You own your data, in spite of the cloud]] 
#+end_quote

#+begin_quote
October 2017, several Google Docs users were locked out of their documents because an automated system incorrectly flagged these documents as abusive. This incident served as a reminder that with cloud apps, the service provider, not you, has the ultimate say on what you are allowed to do.

-- [[https://www.inkandswitch.com/local-first.html][Local-first software: You own your data, in spite of the cloud]] 
#+end_quote
