:PROPERTIES:
:ID:       a18cf441-2a06-493f-b8c7-86da2bbafda3
:END:
#+title: We should socialise ICT infrastructure

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

Seen in [[id:3e022f2c-d890-43a0-92e4-6e68fd946596][Digital Tech Deal]], [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]], [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]].

#+begin_quote
Physical infrastructure such as cloud server farms, wireless cell towers, fiber optic networks and transoceanic submarine cables benefit those who own it. There are initiatives for community-run internet service providers and wireless mesh networks which can help place these services into the hands of communities. Some infrastructure, such as submarine cables, could be maintained by an international consortium that builds and maintains it at cost for the public good rather than profit.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote
