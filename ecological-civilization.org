:PROPERTIES:
:ID:       049fadad-c1bf-4003-9886-809f8823ac61
:mtime:    20220728211037 20211219165233
:ctime:    20211219165233
:END:
#+TITLE: ecological civilization
#+CREATED: [2021-12-19 Sun]
#+LAST_MODIFIED: [2022-07-28 Thu 21:31]

A vision for a future society.  [[id:cbee2565-88fe-4217-844c-9a2df29a3093][Jeremy Lent]]'s idea, I think. I think it's a good one.

#+begin_quote
This is the fundamental idea underlying an ecological civilization: using nature’s own design principles to reimagine the basis of our civilization.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
An ecological civilization would incorporate government spending and markets, but—as laid out by visionary economist Kate Raworth—would add two critical realms to this framework: households and the commons.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
Technological innovation would still be encouraged, but would be prized for its effectiveness in enhancing symbiosis between people and with living systems, rather than minting billionaires.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
Manufacturing would be structured around circular material flows, and locally owned cooperatives would become the default organizational structure.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
Online networks with scale, such as Facebook, would be turned over to the commons, so that rather than manipulating users to maximize advertising dollars, the internet could become a vehicle for humanity to develop a planetary consciousness.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
Governance would be transformed with local, regional, and global decisions made at the levels where their effects are felt most (known as [[id:48de8bb5-ffc7-43d1-956f-37db499e0524][subsidiarity]]).

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote

#+begin_quote
While much decision-making would devolve to lower levels, a stronger global governance would enforce rules on planetwide challenges such as the climate emergency and the sixth great extinction.

-- [[id:f507389c-8889-463b-990c-1a05b000f68a][What Does An Ecological Civilization Look Like?]]
#+end_quote
