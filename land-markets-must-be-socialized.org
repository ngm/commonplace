:PROPERTIES:
:ID:       475df36d-ff84-4982-a334-cb11a08a040b
:mtime:    20220318113311
:ctime:    20220318113311
:END:
#+TITLE: Land markets must be socialized
#+CREATED: [2022-03-18 Fri]
#+LAST_MODIFIED: [2022-03-18 Fri 11:45]

[[id:4562c609-54fc-4646-8614-011ae5fe1f6c][de-marketing land]]

#+begin_quote
Lastly, there are also already existent vehicles for democratic land management and development that can allow us to undo the initial act of capitalism and de-market land.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
Foremost, in recognition of the fact that the price of a location is determined by the desirability of the surrounding community, the government can impose a fee on land-owners equivalent to the ground rent they derive. Especially elegant would be to have these fees provide the initial investment into the public fund paying out the basic income, thus compensating everyone for their exclusion from the locations in question.

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
Additionally, the government should devote public financing and its power of eminent domain to energetically foster [[id:84330e6d-1d4b-4992-b58a-516933094231][Community Land Trusts]].

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote

#+begin_quote
Perhaps most importantly, the government should massively grow the public housing stock so that it does not merely provide poor people undignified living conditions, but rather houses major swaths of large cities in modest, comfortable units. 

-- [[id:30164122-0a72-4e52-8731-8028b8d85a98][Markets in the Next System]]
#+end_quote
