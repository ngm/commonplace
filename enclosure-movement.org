:PROPERTIES:
:ID:       82db8b70-f14f-4905-8705-d106b8ce6b3f
:mtime:    20211127120903 20211017145645
:ctime:    20211017145645
:END:
#+TITLE: Enclosure Movement
#+CREATED: [2021-10-17 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:03f7f762-f450-41fa-83d4-3c0faecf1be2][Enclosure]]

#+begin_quote
The forcible expropriation of peasants alienated humanity from the natural world, and led to a “metabolic rift,” as Marx described it, beginning a process that has driven us today to the brink of planetary destruction

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote
