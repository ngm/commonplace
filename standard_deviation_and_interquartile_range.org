:PROPERTIES:
:ID:       bebe0cec-7950-4016-b850-9853acf9719f
:mtime:    20211127120939 20210724222235
:ctime:    20210724222235
:END:
#+title: Standard deviation and interquartile range
#+CREATED: [2021-02-18 Thu 09:32]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

- [[https://statistics.laerd.com/statistical-guides/measures-of-spread-range-quartiles.php][Measures of Spread | How and when to use measures of spread | Laerd Statistics]]
- [[https://discuss.codecademy.com/t/what-are-some-important-differences-between-standard-deviation-and-interquartile-range/359402][What are some important differences between standard deviation and interquart...]]
- [[https://www.khanacademy.org/math/ap-statistics/summarizing-quantitative-data-ap/measuring-spread-quantitative/v/mean-and-standard-deviation-versus-median-and-iqr][Mean and standard deviation versus median and IQR (video) | Khan Academy]] 
