:PROPERTIES:
:ID:       28962782-ed6c-4a39-b3ec-747b8489ea3a
:mtime:    20231029215921
:ctime:    20231029215921
:END:
#+TITLE: planetary sovereign
#+CREATED: [2023-10-29 Sun]
#+LAST_MODIFIED: [2023-10-29 Sun 21:59]

From [[id:59ac676d-71b1-4e8f-a50f-ee409d4d1e4b][Climate Leviathan]], the idea of a global 'state' of some kind, to coordinate response to climate crisis (and polycrisis in general).
