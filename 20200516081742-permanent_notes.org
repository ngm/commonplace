:PROPERTIES:
:ID:       ed8bcbeb-0b32-4fcd-9f30-6625da8af21f
:mtime:    20211127121008 20210724222235
:ctime:    20200516081742
:END:
#+TITLE: permanent notes

#+begin_quote
Once a day (ideally), go through the notes you created in the first two steps, and turn them into permanent notes. These are more detailed, and carefully written to capture your exact thought or idea. It’s an atomic process: one index card should correspond to one idea and one idea only.

-- [[https://nesslabs.com/how-to-take-smart-notes][How to take smart notes]]
#+end_quote
