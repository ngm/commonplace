:PROPERTIES:
:ID:       e14ffa57-ce98-46e6-a52d-7ab99abebd2f
:mtime:    20220710193039 20220710162906
:ctime:    20220710162906
:END:
#+TITLE: Weeknotes W27 2022
#+CREATED: [2022-07-10 Sun]
#+LAST_MODIFIED: [2022-07-10 Sun 19:30]

Seem to have mostly consumed things about socialism and ecology.

Plus Borish Johnson resigned - now wondering who will be least worse for the environment (as well as everything else) from the leadership contenders.

* Sunday

+ [[id:198c7f10-179b-45d7-bac7-334118e5c951][Technics]]

+ Listened: [[id:1eb52c92-0c17-4341-b4cb-1b36c3def652][Turning the Earth into Money w/ John Bellamy Foster]]  

+ [[id:abdb6222-bf1d-49ce-b472-fc33ae57b21a][carbon footprint]] is bunk
 
+ Environmentalism is presently primarily the preserve of the [[id:f1e20e95-e8a3-4041-aa65-b55583092669][professional-managerial class]].  It needs to be inherently valuable to the [[id:6466d5e2-6e8c-4dec-bc57-cca4d5e72a50][working class]].
  + [[id:f0d2a10e-2ceb-4e0a-9777-0c088e2d3325][Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"]]

+ Decarbonise electricity, then electrify everything.

+ [[id:0e134276-136c-4f95-b0fa-68139d4f0aa9][Climate sadism]]

* Monday

+ [[id:11cbc6ff-f5d8-498a-a140-c89f17ab5694][Free association of producers]] is a goal of socialism/communism.

+ [[id:66201aa6-0753-433c-a022-7e1473c6b5b1][Net zero policies would reduce the cost of living]]. That's an incredibly important line of enquiry to pursue.  Talk of [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][degrowth]] isn't appealing if you're already on the breadline.

+ 'The cost of living' is a wretched phrase.

+ Reading: [[id:e9d04f1b-bab0-4d59-a570-2df5491afbb7][Marx's Vision of Sustainable Human Development]]

* Tuesday

+ Watched: [[id:a8790583-9de5-452f-ab88-b3a09617cf57][Armageddon]].  Pretty terrible.  [[id:fd44499e-47e5-45ba-9873-e36e7c4d4295][Don't Look Up]] is better...

* Wednesday

+ I need to renew my passport.  This is massively over subscribed and delayed in the UK right now.  So I'm following a Twitter bot some guy made that tweets whenever new appointments are available.  World beating service from the UK gov once again!

* Thursday

+ [[id:f0382cdc-bb8a-476a-bd6a-95004a3088d7][Boris Johnson]] is going.  This is good.  ([[id:b8665c5e-5bbf-4608-961c-750ef47c0803][Boris Johnson is a liar]] etc).
+ [[id:032eab29-4b3a-46d5-8b80-570f0df02149][Steve Baker]] is in the news.  This is not good.

+ Listened: [[id:61249a4b-792f-44cc-9832-9745ab433e6e][Adrienne Buller, "The Value of a Whale: On the Illusions of Green Capitalism"]]

* Friday

+ Looking at different [[id:a6a49fc1-7358-4296-b432-f769ea950f28][options for publishing an org-roam-based digital garden]] because mine is currently horrendously slow.  Nothing ticking all the boxes yet though.
