:PROPERTIES:
:ID:       3b3c216c-5435-498a-bc9f-f8c122085ba4
:mtime:    20241116234933
:ctime:    20241116234926
:END:
#+title: stocks and flows

#+begin_quote
Stocks and flows are the basic elements of any system: things that can get built up or run down – just like water in a bath, fish in the sea, people on the planet, trust in a community, or money in the bank. A stock’s levels change over time due to the balance between its inflows and outflows. A bathtub fills or empties depending on how fast water pours in from the tap versus how fast it drains out of the plughole. A flock of chickens grows or shrinks depending on the rate of chicks born versus chickens dying. A piggy bank fills up if more coins are added than are taken awa

-- [[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]]
#+end_quote
