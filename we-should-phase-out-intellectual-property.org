:PROPERTIES:
:ID:       326d2c48-0cb2-4f20-a888-b26228e1b9be
:mtime:    20231110180457
:ctime:    20231110180457
:END:
#+TITLE: We should phase out intellectual property
#+CREATED: [2023-11-10 Fri]
#+LAST_MODIFIED: [2023-11-10 Fri 18:10]

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

* Because

Kwet ([[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]) says that phasing out intellectual property in favour of a commons-based model of sharing knowledge would:

- reduce prices
- widen access to and enhance education for all
- function as a form of wealth redistribution and reparations to the Global South

#+begin_quote
Phasing out intellectual property in favor of a commons-based model of sharing knowledge would reduce prices, widen access to and enhance education for all and function as a form of wealth redistribution and reparations to the Global South.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote

* How?

- [[id:a4da230c-16ee-4d61-a2fd-4e903f52a033][Intellectual property markets must be socialized]]
