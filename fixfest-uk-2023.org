:PROPERTIES:
:ID:       7949653b-7666-4f87-b556-ef96e40b9cd4
:mtime:    20231022173108
:ctime:    20231022173108
:END:
#+TITLE: Fixfest UK 2023
#+CREATED: [2023-10-22 Sun]
#+LAST_MODIFIED: [2023-10-22 Sun 17:35]

Fixfest UK event in Cardiff in 2023.

[[https://fixfest.therestartproject.org/news/this-was-fixfest-uk-2023/][This was Fixfest UK 2023 - Fixfest]]

I was there as part of [[id:bac1368b-df71-478f-9f9f-43ea13e7d449][The Restart Project]].
