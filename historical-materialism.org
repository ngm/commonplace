:PROPERTIES:
:ID:       218dbc25-03ab-4e9e-8bd3-0821cb761168
:mtime:    20211127120812 20210724222235
:ctime:    20210724222235
:END:
#+title: Historical materialism

#+begin_quote
The claim that history is the result of [[id:de16f952-4d1f-4c40-9159-d84429842274][material conditions]] rather than ideas or the machinations of 'great men'

-- [[https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy][The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy]] 
#+end_quote

#+begin_quote
Historical materialism is, simply stated, the theory that human societies develop according to how the “forces of production” are ordered, and that the features of a society will, ultimately, relate back to the ordering of the forces of production. 

--  [[https://jacobinmag.com/2018/12/marxism-socialism-class-struggle-materialism][What It Means to Be a Marxist]] 
#+end_quote

In the episode of Rev Left in the resources, they talk about it in contrast to idealism.  And refer to it more in terms of material conditions, whereas the above quote is 'forces of production'.

* Resources

  - [[https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy][The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy]] 

* Flashcard
   :PROPERTIES:
   :ANKI_DECK: Default
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1596559179302
   :END:

** Front
   What is historical materialism?

** Back
the theory that human societies develop according to how the “forces of production” are ordered, 

and that the features of a society will, ultimately, relate back to the ordering of the forces of production. 
