:PROPERTIES:
:ID:       7d2cd083-eb57-4325-b670-6107ad73a9a2
:mtime:    20220212120730 20211127120859 20210724222235
:ctime:    20210724222235
:END:
#+title: Modern Times
#+CREATED: [2021-04-18 Sun 15:10]
#+LAST_MODIFIED: [2022-02-12 Sat 12:07]

#+begin_quote
the classic satire of mechanized [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]], Modern Times

-- [[id:3c3aa000-7d00-4f2d-9a33-97526d3d10c0][Vanguard Stacks: Self-Governing against Digital Colonialism]]
#+end_quote
