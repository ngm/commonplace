:PROPERTIES:
:ID:       8ca6ac68-ab53-489c-bfe5-b1517bc485be
:mtime:    20211127120909 20210724222235
:ctime:    20210724222235
:END:
#+title: The Year 200

Cuban science fiction book.  Heard about it in [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]].

https://restlessbooks.org/bookstore/the-year-200
