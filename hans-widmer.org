:PROPERTIES:
:ID:       d22334b2-eff3-40af-bdf8-7c439154aabe
:mtime:    20221010204245 20211127120952 20210912164103
:ctime:    20210912164103
:END:
#+TITLE: Hans Widmer
#+CREATED: [2021-09-12 Sun]
#+LAST_MODIFIED: [2022-10-10 Mon 20:46]

#+begin_quote
P.M. is the pseudonym of the Swiss author Hans Widmer (born in 1947). Besides writing utopian novels, other literature, theatre performances, and radio plays, P.M. has also been an activist in autonomous and [[id:50b848e1-c617-45ca-a876-cafba6943479][eco-socialist]] projects and movements. [[id:bef17ef3-0b14-46a1-ada7-5325221baed6][bolo'bolo]] is P.M.’s most wellknown book. Reminiscent of Kroptokin, bolo’bolo outlines how a future grassroots communist society without capital and the state could look. [[id:0270a4f7-080a-490b-a689-d670ac87715a][Kartoffeln und Computer]] (Potatoes and Computers) is an update of bolo’bolo’s vision written almost thirty years later.
#+end_quote
