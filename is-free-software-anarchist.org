:PROPERTIES:
:ID:       69cad721-c654-4d8b-94e1-8ed2bdcb9367
:mtime:    20211231123429
:ctime:    20211231123429
:END:
#+TITLE: Is free software anarchist?
#+CREATED: [2021-12-31 Fri]
#+LAST_MODIFIED: [2021-12-31 Fri 12:38]

[Does this even make sense as a question?]

Depends on your definition of [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]] and your definition of [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][free software]].


+ https://events.ccc.de/congress/2005/fahrplan/attachments/571-SoftwAnarchyLong.pdf
