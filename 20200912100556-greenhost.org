:PROPERTIES:
:ID:       4040fafc-8e29-46d4-b81d-51eb8b199677
:mtime:    20211127120828 20210724222235
:ctime:    20200912100556
:END:
#+title: GreenHost

When my sites server suddenly went AWOL on cloudvault.me, I chucked everything over to Digital Ocean to get it back up again. But was keen to get off - I moved from there to GreenHost.

GreenHost has stellar environmental and ethical creds, and really not that far off the cost of a basic DO droplet. 

I've generally been using Ubuntu server, which GreenHost has available for their VPSes, so went with Ubuntu 20.04.  (The options are Debian 10, Ubuntu 18.04 and 20.04, and CentOS 7.)

A droplet at Digital Ocean is $5, 1 core, 1GB, 25GB, 1000GB transfer.
GreenHost: 1 core, 1GiB, 5GiB, 5.75 euro. (6.50 euro if you go 10GiB disk), 5TB data limit.

You can turn on server backups - not 100% sure how you restore at GreenHost yet.
