:PROPERTIES:
:ID:       8e1586f3-da61-4e1d-9fbb-8eaa3b9d157b
:mtime:    20211127120910 20210724222235
:ctime:    20200622182115
:END:
#+TITLE: schizoanalysis

#+begin_quote
schizoanalysis: a narration of the history of desire, as a productive and impersonal world-creating force.

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

#+begin_quote
Enter the schizoanalyst, somewhere between a psychoanalyst and a political agitator. The role of this figure was to decode the unconscious processes of desire and identify their revolutionary potential.

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote


