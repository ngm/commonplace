:PROPERTIES:
:ID:       c0a85cca-14d2-425d-807b-f1636beecc3a
:mtime:    20220605182825
:ctime:    20220605182825
:END:
#+TITLE: World Social Forum
#+CREATED: [2022-06-05 Sun]
#+LAST_MODIFIED: [2022-06-05 Sun 18:29]

#+begin_quote
a self-conscious effort to develop an alternative future through the championing of counter-hegemonic globalization

--  [[https://en.wikipedia.org/wiki/World_Social_Forum][World Social Forum - Wikipedia]]
#+end_quote
