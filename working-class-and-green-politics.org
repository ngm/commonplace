:PROPERTIES:
:ID:       17d539e4-17c5-4fe1-a526-a7cc529c4dd2
:mtime:    20220820112432
:ctime:    20220820112432
:END:
#+TITLE: Working class and green politics
#+CREATED: [2022-08-20 Sat]
#+LAST_MODIFIED: [2022-08-20 Sat 11:24]

#+begin_quote
Climate change and other severe environmental problems demand working class solutions. The productivity and creativity of workers is vital to ecological alternatives.

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

#+begin_quote
Workers produce and can produce alternative sustainable futures, the concept of workers’ plans for ecological production is important

-- [[id:b0eea04f-ed32-4036-98e9-925f0f48e24c][Imperialism is the Arsonist: Marxism's Contribution to Ecological Literatures and Struggles]]
#+end_quote

