:PROPERTIES:
:ID:       4ad96999-4e32-4de1-ac5d-8791ebc7e70c
:mtime:    20230923162034 20230922175231
:ctime:    20230922175231
:END:
#+TITLE: The information necessary for repair should be easily accessible
#+CREATED: [2023-09-22 Fri]
#+LAST_MODIFIED: [2023-09-23 Sat 16:22]

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

Because relevant repair information is a big part of completing a successful repair.

[[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]]. [[id:855fc3c9-d96d-46f9-be15-71f9225bf7c1][access to repair information]].
