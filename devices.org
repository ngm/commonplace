:PROPERTIES:
:ID:       7d6c8c82-84e6-4805-960a-7c6b5d9aa03e
:mtime:    20211127120900 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:My Devices

* Current

** Lenovo Thinkpad T450s

I got this is May 2019 from a second hand shop in Elephant and Castle.  Refurbished.  £240.

Really liking Thinkpads so far.

- Processor: Quad-Core Intel Core i5-5300U CPU @ 2.30GHz
- Memory: 8 GB
- OS: [[id:40d8289c-ebd9-460e-a3b0-ab707a16f03d][Linux Mint]]

More details: [[id:33bdd380-c871-4fe4-b7cd-60459afe39db][Lenovo Thinkpad T450s]]

** [[id:2ba44773-1234-49c1-bd5e-397fe79b6f1a][Samsung Galaxy S5]]
   
* History
  
** Computers

*** [[id:5d870662-8083-4ee4-bb90-420c9ca38573][Amstrad CPC 464]]


*** RM Nimbus
    
 My primary school had one of these.  I think probably the 80186 version.  So technically better than the Amstrad but I remember the Amstrad being way cooler.  I remember some game that in my head is called Puff the Magic Dragon, but I think it must actually be Through the Dragon's Eye?  

Possibly we did turtle graphics on this at school?
 
*** Amiga 1200
   
    - CPU: Motorola 68EC020 @ 14 MHz 
    - Memory: 2 MB
    - OS: AmigaOS 3.0/3.1
    - https://en.wikipedia.org/wiki/Amiga_1200
  
 My Mum got it on credit card and was in debt paying it off for a long time.

 I remember Deluxe Paint.  Cannon Fodder.  Sensible Soccer.  Chaos Engine.  Syndicate.  Lemmings.  

Oh and Frontier: Elite II!

 I don't remember doing any coding on this one?

*** Family PC
We got a family PC at some point.  Maybe around the time I went to college?  I think it was from Evesham but no clue which one.

*** 1st Laptop
    
    I got one before going to university in 2001.  Possibly from Hi-Grade?  I worked for a long time to save up for it I recall.  I'm convinced it cost over £1000 at the time, although I have no proof.

*** Hi-Grade Notino 4400

My second laptop I think.

- Processor: don't know
- Memory: 512 Mb
- Storage: 30 Gb HDD

I know I had it in Brighton during my Masters degree.  So perhaps I got it in 2006 before starting?
    
Think it was replaced with the Asus that I got around 2012/2013.
  
I gave it away in 2019 via Freegle.
    
*** Asus Vivobook S200
    I got an Asus Vivobook S200 on sale from Comet when it was closing.  So I guess around the end of 2012.  I think it was £400.
    
I replaced the power supply for this once, and had to replace the battery once.

** Consoles
*** Master System
    My brother had a Master System.
   
*** MegaDrive
    I got a MegaDrive for Christmas one year.

*** N64
    I played Goldeneye a lot.

** Mobile devices
*** Nokia N800
    No a phone but an 'Internet tablet'.  It ran Maemo.
   
*** Samsung Galaxy Portal
    My first smartphone.  I think maybe 2009/2010.

*** Motorola Atrix
    My second smartphone.  Got it second hand from Computer Exchange.  Maybe 2013?
    
I replaced the screen on this after I smashed it while shooing away a bee.

*** Sony Xperia Z1 Compact
    My third smartphone.  Second hand from Computer Exchange.  I'm going to guess [[id:87c58065-2399-4868-9588-30cc8bff88b7][2016]].
   
I replaced the battery and the back panel on this over the span of its life.

OK phone, but it had a locked and encrypted bootloader on it, which was massively shit, and meant I couldn't install any custom ROMs.  I didn't know this was a thing until getting this phone.
 
It went kaput in [[id:a4efd750-9e13-4e79-9b1e-71ccfaea4aa2][October 2019]] going into infinite boot loops.
  
*** Nokia Lumia ?? (930??)
    
I used this for a bit.  Mike gave it to me as the OS for it was being end-of-lifed.  Kind of crappy, a very dead app store where even of all of the big apps lay pretty unmaintained.  But it did the job as a loaner.
  
