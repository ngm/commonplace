:PROPERTIES:
:ID:       b17522e0-c28c-4320-b185-9cba7436a5d3
:mtime:    20211127120931 20210724222235
:ctime:    20210724222235
:END:
#+title: Infinite Detail
#+CREATED: [2021-01-21 Thu 18:47]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

by Tim Maughan.

https://www.mcdbooks.com/books/infinite-detail

Enjoying this so far.  Fun writing, and I like the nods to [[roam:Temporary Autonomous Zones]], [[id:bf300569-4788-4de9-b53a-632d883427ac][Decentralisation]], [[id:0c84c589-cc3d-46c0-8899-0c458ef346eb][supply chains]], etc.
