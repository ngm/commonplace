:PROPERTIES:
:ID:       6caa3ad7-a866-4bce-8979-b775fc6ff498
:mtime:    20221022175120
:ctime:    20221022175120
:END:
#+TITLE: Sabotage
#+CREATED: [2022-10-22 Sat]
#+LAST_MODIFIED: [2022-10-22 Sat 17:51]

#+begin_quote
“Sabotage” at this time could mean any number of subversive techniques employed by a worker that interfered with production, including absenteeism, work slowdowns, “working to rule” by following only explicit requirements of the job, even striking. It could refer to disrupting equipment— “the displacement of parts of machinery or the disarrangement of a whole machine”—but it could also mean creating better-quality products than owners intended.

-- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
#+end_quote
