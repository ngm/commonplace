:PROPERTIES:
:ID:       1df140dd-85f5-4ff7-8321-b155ccbceee3
:mtime:    20220225213328 20211127120809 20210724222235
:ctime:    20210724222235
:END:
#+title: Spanish Revolution
#+CREATED: [2021-03-12 Fri 12:21]
#+LAST_MODIFIED: [2022-02-25 Fri 21:33]

[[id:80eed59b-5347-47b0-956c-9df020e08321][Spanish Civil War]].


#+begin_quote
In Barcelona in July 1936, when the generals initiated a coup to impose fascism across Spain, it was the resistance of poorly armed militias in Barcelona set up by anarchist union the CNT and the Marxist party, the POUM, that persuaded a section of the army, the Assault Guards, to join the resistance.

-- [[id:fd09cb49-a70e-476c-a3ac-bcc0b725a12c][Revolution in the 21st century - Chris Harman]]
#+end_quote
