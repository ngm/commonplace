:PROPERTIES:
:ID:       d16e395d-f87e-4fd1-9974-53dd606b93ad
:mtime:    20230722095121
:ctime:    20230722095121
:END:
#+TITLE: The Common Place
#+CREATED: [2023-07-22 Sat]
#+LAST_MODIFIED: [2023-07-22 Sat 09:52]

Was a social centre in Leeds, now closed.

- [[https://theculturevulture.co.uk/conversations/speakerscorner/the-common-place-is-closing/][The Common Place is closing | the CULTURE VULTURE]]
