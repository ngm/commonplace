:PROPERTIES:
:ID:       fbd23090-f807-4a00-94b8-d89ab00bf581
:mtime:    20211127121016 20210724222235
:ctime:    20200322104407
:END:
#+TITLE: The Librarian

From [[id:0ecb1650-ca8a-4ef3-9302-1e8432c45ac3][Snow Crash]].  A [[id:d00ead5b-8c0d-4f17-b6c6-ed90bde2ba78][daemon[[https://www.researchgate.net/publication/251011743_Agent_of_Civility_The_Librarian_in_Neal_Stephenson's_Snow_Crash]]]] of sorts?

#+begin_quote
In Neal Stephenson's 1992 cyberpunk novel Snow Crash, hacker Hiro Protagonist, aided by a virtual Librarian, works to save the world from a cybernetic and biological virus that will enslave the information elite (hackers and programmers). The text proposes questions for an information culture like ours facing technological and informational calamity. What will become of the gap between the information rich and poor? What is the future of public access to information? Can librarians in the digital library (cybrarians) be successful educators? If librarians embrace technology, will it be at the expense of humanism? Can the cybrarian continue to act as an agent of culture and enlightenment, as in the past? This article examines the external world that makes living in an internal virtual world necessary, then explores the issue of whether the Librarian is a harmful or beneficial creation, and finally considers the Librarian as a force for civility.

-- [[https://www.researchgate.net/publication/251011743_Agent_of_Civility_The_Librarian_in_Neal_Stephenson's_Snow_Crash]]
#+end_quote

