:PROPERTIES:
:ID:       982e5671-7b36-4a1b-8b3f-d66e76429080
:mtime:    20231103142930
:ctime:    20231103142930
:END:
#+TITLE: Using SSH keys in Termux for Git
#+CREATED: [2023-11-03 Fri]
#+LAST_MODIFIED: [2023-11-03 Fri 14:38]

[[id:32be7697-0e4f-4bf9-9426-38dcfb15306a][termux]]. ssh. git.

I need to install a couple of packages.

#+begin_src sh
pkg install openssl-tool
pkg install openssh
#+end_src

Set up your ssh key.  I use passphrases.

To have gitlab.com in your git config be recognised by ssh, and to use the particular key for gitlab:

#+CAPTION: ~/ssh/config
#+begin_src
Host gitlab.com
        HostName gitlab.com
        User git
        Port 22
        IdentityFile ~/.ssh/gitlab
#+end_src

To make sure you don't have to keep entering it:

#+CAPTION: ~/.ssh/config
#+begin_src
Host *
        AddKeysToAgent yes
#+end_src

And at the start of new Termux session:

#+begin_src
eval $(ssh-agent)
ssh-add
#+end_src
