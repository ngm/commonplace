:PROPERTIES:
:ID:       0b471656-4030-411e-ba9b-957a25c91447
:mtime:    20230430154146 20230430111759 20230402183856 20230402110351
:ctime:    20230402110351
:END:
#+TITLE: Moving my WordPress installation to YunoHost
#+CREATED: [2023-04-02 Sun]
#+LAST_MODIFIED: [2023-04-30 Sun 15:43]

I moved my WordPress install from [[id:4040fafc-8e29-46d4-b81d-51eb8b199677][GreenHost]] VPS to a WordPress install on my [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]] server.

GreenHost are a great company, but I was almost permanently getting the 'Error establishing a database connection' error on the install.  Not sure why.

I briefly dallied with a fresh VPS at Hetzner just for the purpose of the WordPress install, but it felt a bit overkill, and also when going through some of the steps of [[id:4da07512-a8cf-4b92-b649-7aeed68cf55d][Ubuntu 20.04 basic server setup]] (but for latest Ubuntu) it all just felt a bit of a faff these days.  Why not let YunoHost take care of the fiddly bits of the server setup (including security aspects like fail2ban, firewall, etc) and just get my a WordPress install to work with?

So I did that.

- point my doubleloop.net domain at the YunoHost IP
- add a new domain in YunoHost
  - self-signed cert to begin with
- set up SSL with Let's Encrypt on the domain
  - it wouldn't let me for a while, but I think adding the CAA record for Let's Encrypt helped, or maybe my DNS records just took a little time to propagate
- Install WordPress app to the new domain in YunoHost
- Once installed, follow the steps here for migrating a WordPress install: https://www.snel.com/support/how-to-duplicate-a-wordpress-site/
  - I usually just use [[id:a1501bf3-b7e3-498f-a857-865302c68e60][Duplicator]] for this, but because the MySQL connection was constantly failing, it wasn't working
- Location of new app in YunoHost was /var/www/wordpress
- Had to chown -R wordpress:www-data ./* on all the new files

And it's up and running, relatively painless all told.  Also it's so much faster now.

* Log

** [2023-04-23 Sun]

I notice that the SSOwat single sign on doesn't work into Wordpress - I have to log in again with my WP details.  Which makes sense, as I restored from the backup which has different login details to Yuno.  But I wonder if by design it would work out of the box for a fresh install, and whether I can fix it for my restored WP.

** [2023-04-30 Sun]

- The upgrade from 6.1 to 6.2 happened via yunohost system update screen, rather than within wordpress itself - interesting.
- Upgrading from 6.1 to 6.2, noticed that it's running on MariaDB rather than MySQL.  Probably fine, but worth being aware of.
- Doing a general updates on Yunohost, that knocked out the website briefly, and ungracefully, with the ol classic 'Error establishing a database connection'.  It came back up again after the updates.  It was probably PHP or MariaDB being upgraded, I suppose.
