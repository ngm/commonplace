:PROPERTIES:
:ID:       8807efff-51a6-43c9-9ce6-fbc2352849d3
:mtime:    20220413205035
:ctime:    20220413205035
:END:
#+TITLE: mining
#+CREATED: [2022-04-13 Wed]
#+LAST_MODIFIED: [2022-04-13 Wed 20:50]

#+begin_quote
Mining is an industry characterised around the world by extractivist violence, environmental destruction and worker exploitation

-- [[id:209016b0-9bfa-492f-b225-92c7244f142f][Can urban mining help to save the planet?]]
#+end_quote
