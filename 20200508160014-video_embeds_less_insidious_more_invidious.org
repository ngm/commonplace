:PROPERTIES:
:ID:       3fd1661d-189c-42ef-a5b0-4cd2714d9219
:mtime:    20211127120828 20210724222235
:ctime:    20200508160014
:END:
#+TITLE: Less insidious, more Invidious: Improving my video embeds

Prompted by a chat with [[id:62180b97-a71d-419f-997e-f3e81291e4f6][Panda]], I've revisited how I'm embedding videos on my site.  This tends to be mostly for my occasional [[https://doubleloop.net/kind/jam][jam]] posts.  In a nutshell: back to Invidious but with a couple of tweaks.

* The problem with YouTube

YouTube is obviously the home of most videos.  As such that's the source of much video content, particularly the music videos I want to link to. 

But I have two qualms about embedding YouTube videos:

- In general I'd prefer not to feed traffic and data to the beast (and its parent beast).  
- If you embed a video from YouTube, this adds tracking cookies to your own site.

* YT alternatives

For a brief period I used HookTube to link to videos.  It's just a lightweight wrapper around YT.  However, I can't remember what it was, but I got a weird vibe from it, like it was some alt-right den of inequity.  I can't back that up, so apologies to however runs it if it isn't true.  (But maybe try to limit the Fox News and truther vidoes that seem to appear on the homepage).

Then I started using [[id:6e8c2e1c-5e53-4062-86c4-2dde869dc78c][Invidious]].  This is also a lightweight web wrapper around YT.  It also is libre software, so there are multiple hosted instances of it, and you could host it yourself if you wanted.  It has a bunch of nice features, such as no ads, audio-only mode, doesn't require JS, and no tracking.

* Invidious media loading issues
   
So I used that for a while.  But quite regularly I would encounter an error:

 #+begin_quote
'The media could not be loaded, either because the server or network failed or because the format is not supported.
 #+end_quote

And you can't play the video.  So I got lazy and started just embedding YouTube again.

But chatting with Panda made me realise I want to strip out the YouTube embeds again.  Panda also pointed out an alternative instance of Invidious that seems to have better performance than the main one.
 
* Improved embedding

So I've tweaked the [[https://gitlab.com/ngm/doublescores/-/blob/master/functions.php#L170][function]] in my theme that overrides the embeds, to make it so that any YouTube or Individious link is embedded via [[https://invidious.snopyta.org/][invidious.snopyta.org]].  Also underneath it will include a link to both Invidious and YouTube to watch the video there.  The YouTube link being for if the Invidious one isn't working.  As it is a link, there's no tracking, and it empowers the visitor to make the decision as to whether they wish to be tracked by YouTube.  I've set rel="nofollow noreferrer" as well.


#+ATTR_HTML: :width 100%
[[file:Improved_embedding/2020-05-08_16-29-47_screenshot.png]]


(While I was at it, I added [[https://github.com/drdogbot7/tailwindcss-responsive-embed][tailwindcss-responsive-embed]] to get the embeds to display better on smaller displays).

* To embed or not to embed?

Finally, I'm also tempted not to use embeds at all, and just grab the cover image for a video and display that, along with the links to watch the actual video.  I don't think anyone *really* cares about playing the video inline on my site.  I just like the visual that comes with embedding a video.

Perhaps even better, I'd like to find a good solution for [[id:ef4c6a0c-f6f1-436b-8859-d0e442d72568][linking to songs on the web]] that doesn't favour any particular platform - ideally the visitor could listen to it wherever they want.

