:PROPERTIES:
:ID:       7c31036f-47ec-456b-ba40-94384c29fa80
:mtime:    20211127120859 20210912101730
:ctime:    20210912101730
:END:
#+TITLE: Code Red for Humanity: the IPCC Report 2021
#+CREATED: [2021-09-12 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]].
+ Series :: [[id:38c52a54-52ce-4e39-9a49-905d9a13fa24][Red Menace]]
+ URL :: https://redmenace.libsyn.com/ipcc

They do a good recap on the [[id:58ffc41a-dde4-42b8-bccf-8ce0d1010852][IPCC Sixth Assessment Report]].

They talk about how [[id:2f2900eb-a328-4f77-b251-c47acfc6491b][climate anxiety]] should not become [[id:c7c5f1fc-ea0f-4b33-a827-6f3a01ac70bb][climate apathy]] and the need to organise even in the face of almost certainly pushing beyond 1.5 and probably 2 degrees and things falling apart.

Because capitalism will not simply collapse along with the climate.  It will likely accelerate and goose step towards its latent fascistic tendencies.

Also, interesting discussion on the merits of individual behaviour in the midst of an obvious need for large structural change.  [[id:a323f464-f813-4ef5-98bc-923d9d8da3d8][Horizontalism vs verticalism]]. Given they tend towards [[id:7d53d9ad-4f31-4b36-8a3a-2b7c912b1c97][vanguardism]] and the need for a vertical party, good to hear some constructive thoughts on the need for [[id:55f4dec4-a979-4bac-86b9-49be08c6527a][prefiguration]] too.
