:PROPERTIES:
:ID:       820870a1-9f5b-4103-9ee9-66ffc7597fe9
:mtime:    20220326144445 20220326115905
:ctime:    20220326115905
:END:
#+TITLE: Soviet economy
#+CREATED: [2022-03-26 Sat]
#+LAST_MODIFIED: [2022-03-26 Sat 14:49]

* Earlier
#+begin_quote
the Soviet economy of the 1930s was exceptional in the degree to which it reinvested, rather than consuming, its production.   

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

* Later

[[id:061c873a-f66a-4b9d-9114-e282b0e54acc][planned economy]].

#+begin_quote
Indeed, there was a philosophical issue involved here, a point on which it was important for Soviet planners to feel that they were keeping faith with Marx, even if in almost every other respect their post-revolutionary world parted company with his

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

#+begin_quote
By counting actual bags of cement rather than the phantom of cash, the Soviet economy was voting for reality, for the material world as it truly was in itself, rather than for the ideological hallucination

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

#+begin_quote
Instead of calculating [[id:9433d84e-d1c7-4889-bec2-09cb219ce5a2][Gross Domestic Product]], the sum of all the incomes earned in a country, the USSR calculated [[id:720bc47e-4341-45ab-b916-c1004a5c5ee7][Net Material Product]], the country’s total output of stuff – expressed, for convenience, in roubles

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

#+begin_quote
This kind of ‘extensive’ growth (as opposed to the ‘intensive’ growth of rising productivity) came with built-in limits, and the Soviet economy was already nearing them

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote

#+begin_quote
In effect, they were spraying Soviet industry with the money they had so painfully extracted from the populace, and wasting more than a third of it in the process

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote
