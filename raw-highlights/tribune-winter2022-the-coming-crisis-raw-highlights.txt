--------------------------------------------------------------------------------
Tribune - The Coming Crisis
--------------------------------------------------------------------------------
Page 330 [2022-06-02 23:55:46]:
opposite approach to dystopia can be found in the Liverpudlian socialist Olaf Stapledon’s still incredible 1930 First and Last Men, a boundlessly optimistic, Hegelian account of human evolution

Page 332 [2022-06-03 01:02:13]:
Vagabonds, a long novel by Hao Jingfang (whose

Page 273 [2022-06-03 08:00:00]:
youngsters were swallowed up by something else — namely, video rooms, scattered across the whole country by the beginning of the nineties. Some people got their own VHS recorders, and collective viewings of rented films from the West were a new and exciting activity

Page 274 [2022-06-03 08:01:39]:
A new type of leisure facility built by the Soviet government, Houses of Culture (or ‘clubs’) were based on the already existing system of People’s Houses established in Imperial Russia to distract workers from drinking. Installed into the structure of the city, Houses of Culture allowed every district to have its own ‘cultural leisure’ place to meet — in the absence of drinking facilities, such as pubs in the UK

Page 280 [2022-06-03 08:04:19]:
Like the current trend for mid-century modern design, the idea (perhaps more than the material reality) of new towns is attractive, because they’re associated with a perceived utopianism and optimism of the post-war period in Britain

Page 280 [2022-06-03 08:04:39]:
Proposed by Sir Patrick Abercrombie in his reconstruction plans of 1944–45, new towns were a bold solution to overcrowding and poor housing through decentralisation of inhabitants from London and the other big cities

Page 280 [2022-06-03 08:05:14]:
Attlee’s Labour government passed the 1946 New Towns Act among the other key pieces of legislation framing the welfare state. Stevenage was the first new town to be chosen

Page 281 [2022-06-03 08:06:31]:
Stevenage was the first pedestrian town centre in England. Not even the nearby garden cities of Letchworth and Welwyn, with their carefully planned green space, prioritised the pedestrian shopper over the car user

Page 283 [2022-06-03 08:08:08]:
Other than a discussion of the leisure activities, there is little recognition of the people who migrated to live in Stevenage, east-enders from blitzed London, and the Irish builders who physically built the houses, churches, and community centres, and then stayed put

Page 283 [2022-06-03 08:08:25]:
The radical novelty of the post-war new towns programme lay in the centralised control of planning and finance through public development corporations

Page 6 [2022-06-03 08:10:53]:
The crisis impacting working people isn’t a result of blind economic forces — it is the result of a class war waged from above

Page 7 [2022-06-03 08:11:13]:
The year 2022 will, in all likelihood, be the toughest one for working people since the immediate aftermath of the 2008 Financial Crash

Page 8 [2022-06-03 08:13:07]:
In its response to rising inflation, The Bank of England was quick to lay the blame on workers, calling for wage restraint. But this crisis hasn’t been caused by an increase in demand. The proximate cause is a crisis in supply, chiefly of energy but also in the structure of the supply chain itself, against which the only protection that workers have is the fight for higher wages

Page 8 [2022-06-03 08:13:54]:
In the end, prices are not set by workers or by consumers, but by firms. This is one of the entitlements afforded to capital by the private ownership of the economy. So, when those prices go up, a decision is being made. In the case of Tesco, for example, Britain’s largest retailer, their decision is to increase the cost of food while sitting on an annual operating profit of £2.6 billion

Page 9 [2022-06-03 08:14:30]:
Much the same is the case in the energy sector, where the so-called Big Six raked in over £1 billion in profits on the eve of a truly astronomical hike in bills for their customers. British Gas alone saw a 44 per cent jump in its profits

Page 9 [2022-06-03 08:16:30]:
When a crisis like the one we’re experiencing emerges, there is always a question: who pays for it? If the cost of production rises, will workers and consumers bear the brunt or will profits be squeezed? In an economy dominated by private ownership, this is in reality not much of a question at all. Profits will be protected; everything else is a lower-order concern

Page 10 [2022-06-03 08:17:11]:
The only answer to the cost-of-living crisis is fundamental economic change. A government that truly represented the public interest would today be intervening in any number of ways to prevent living standards from dramatically deteriorating

Page 13 [2022-06-03 08:21:25]:
This situation puts a particular focus on extra-parliamentary politics, from trade unions to social movements and campaigns. These are now the only ways that pressure felt in working-class communities across the country can be expressed in an organised form. And, in turn, the only way that pressure can be directed against the business interests profiting from this crisis and the politicians doing their bidding

Page 14 [2022-06-03 08:22:21]:
This means that government by consent is fading and, in its place, government by force is becoming more commonplace. As the cost-of-living crisis deepens this year, and millions of people who were already struggling realise that their living standards will decline even further, the anger will be difficult for the government to contain. And so, they are providing themselves with the tools necessary for a crackdown

Page 15 [2022-06-03 08:23:32]:
Like steam, even great quantities of anger can soon dissipate. It is only when that steam is gathered in a structure that the pistons move and the wheels of history can begin to turn

Page 15 [2022-06-03 08:24:36]:
We must set out our alternative of wage rises, price caps, and public ownership, and of a fundamentally different political and economic system which serves a different set of interests

Page 16 [2022-06-03 08:25:00]:
A great wave is about to come crashing down on this country, and the only thing which can protect us from it is class struggle

Page 21 [2022-06-03 08:53:10]:
As the politically bankable public joy over the success of the Covid vaccine programme began to wane, the latest scandal was a reminder of who had been steering the country through this generational crisis: a prime minister who fundamentally did not believe he had any responsibility to the rules he set, and a man whose entire political career has been built on impunity

Page 22 [2022-06-03 08:54:02]:
North. There’s no cast-iron cohort of loyal MPs because there’s no such thing as a Johnsonite — only fair-weather fans

Page 26 [2022-06-03 08:55:45]:
Like many industries, the answer lies in the advent and development of neoliberalism throughout the 1980s. The dogma pushed by Margaret Thatcher and her successors in government (including New Labour) was one of ‘value for money’, with private sector ‘entrepreneurship’ being the main guarantor of efficiency in public services

Page 29 [2022-06-03 08:57:48]:
Those that worked through a pandemic and were lauded as key workers are now acutely aware of their importance to society and have no intention of letting the politicians and bureaucrats who decanted them into the hands of private companies off the hook

Page 177 [2022-06-05 00:50:29]:
Crypto advocates promise to democratise the internet by decentralising power, but the real path to digital democracy is publicly-owned infrastructure

Page 180 [2022-06-05 00:52:10]:
But the true genius of these new crypto technologies is how they have coded libertarian and pro-market ideas into our understanding of the internet. By promoting the idea that a truly democratic internet would be one governed by individuals, not collectives, and that this would be organised on the basis of private property, they have done more than any recent politician to take the Thatcherite mantra that ‘there’s no such thing as society’ into the twenty-first century

Page 180 [2022-06-05 00:52:35]:
It’s not a coincidence that some of the more coherent suggestions for how crypto technology might be applied include ‘smart contracts’, which lock you out of the home you’re living in if you miss a rent payment, or a store of wealth that cannot be touched by ‘oppressive’ governments (for example, if they’re asking you to pay tax

Page 181 [2022-06-05 00:53:49]:
The fundamental issue socialists have with these platforms isn’t the service they provide, it’s the fact they do it for private profit rather than in the public interest

Page 182 [2022-06-05 00:55:34]:
For a more democratic and egalitarian alternative, we need a level of funding to build a parallel, state-managed, and nationalised infrastructure on a scale that cannot be bootstrapped or crowdfunded

Page 183 [2022-06-05 00:56:21]:
Creating infrastructure on a scale that could wrest control of the services that millions of people across Britain use every day from the wealthiest men ever to have lived would require billions of pounds in funding, and economies of scale that cannot be reached through decentralisation or distributed apps. In fact, it requires the opposite: central planning

Page 186 [2022-06-05 01:01:42]:
The importance of a ‘People’s internet’ to socialist politics goes beyond the need to expropriate big tech or curb their power over our lives. The year 2021 was dogged by supply chain issues, causing food, fuel, and consumer goods shortages — and leading to the cost of living squeeze. Unfortunately, these problems are predicted to worsen as the climate crisis unfolds. The market can’t coordinate the resources necessary to overcome them and wouldn’t do it in the public interest even if it could. This problem can only be solved in the long term with central planning

Page 187 [2022-06-05 01:02:52]:
The internal decisions made by these corporations have a significant impact on our economy. They allocate resources on a vast scale, decide on investment, dictate production, and organise the pay and conditions of workforces across the world. As Leigh Phillips and Michal Rozworski argue in their book The People’s Republic of Walmart, all of this is planning — but none is subject to democratic control or carried out in the public interest. It is planning for profit

Page 253 [2022-06-05 01:07:24]:
The Northern Roots of Modernist Sci-Fi
By Alex Niven
In the dying days of industry, northern England supplied the crucial animating backdrop to classic sci-fi in its formative stages

Page 255 [2022-06-05 08:10:33]:
More than anything else, modern science fiction in Britain — and indeed Europe as a whole — was an imaginative record of the passage from industrial to post-industrial phases of capitalist development. And perhaps inevitably, given the roots of the Industrial Revolution, it was northern England in the dying days of industry that supplied the crucial animating backdrop to classic sci-fi in its formative stages

Page 260 [2022-06-05 08:15:45]:
Written in 1920–21, and often viewed as a veiled critique of the new Soviet Union by a disillusioned Bolshevik, Zamyatin’s We is really best viewed as an attempt to render the capitalist, industrial landscape of 1910s Newcastle in imaginative, allegorical prose

Page 261 [2022-06-05 08:18:26]:
One of the first attempts to create an English counterpart to Zamyatin’s seminal dystopian novel, though with a much more explicitly consumer-capitalist emphasis, Aldous Huxley’s Brave New World was directly inspired by its author’s visit in the late 1920s to the recently built Imperial Chemical Industries (ICI) factory in Billingham, in the Teesside portion of County Durham

Page 263 [2022-06-05 08:20:48]:
Published in 1962, as Britain teetered on the brink of a transformative white-heat modernity, Anthony Burgess’s A Clockwork Orange condensed an array of disparate influences into a caustic sci-fi novella which argued for the importance of theological free will in an increasingly rationalistic epoch

Page 265 [2022-06-05 08:23:54]:
Blade Runner into the world. Released in 1982, as the final, most aggressive phase of northern deindustrialisation was kicking into gear, this visionary film was the swansong of a specific tradition in twentieth-century art, which used northern industrial landscapes as a prompt for hallucinating evocative near futures

Page 110 [2022-06-05 08:34:14]:
Rising inflation is driving the cost of living crisis, but it isn’t an act of God. It’s the result of policy decisions that favour the rich — and socialists need to have an alternative

Page 111 [2022-06-06 00:26:40]:
Attitudes towards inflation have long been seen as a dividing line between left and right. Most Keynesians saw rising prices as a small price to pay for an otherwise healthy economy, given that tackling inflation tended to require higher interest rates, which curtail investment, output, and employment over the short term

Page 112 [2022-06-06 00:26:55]:
Neoliberals, on the other hand, saw inflation as public enemy number one. First, rising prices eroded the real value of assets — if consumer prices are rising while house prices are stagnant, your house is worth less in real terms. Second, keeping interest rates low reduces returns from lending — important for profits in the finance sector — and low unemployment made it harder to discipline labour

Page 113 [2022-06-06 00:28:55]:
The question we should therefore be asking is not ‘is inflation bad?’ The questions we should be asking are ‘which prices are rising, why are they rising, and who is bearing most of the impact

Page 114 [2022-06-06 01:02:09]:
The Consumer Price Index (CPI) is the official metric of inflation used by the government

Page 117 [2022-06-06 01:17:14]:
there is reason to believe that inflation is running at even higher rates for poorer households than it is for wealthier ones

Page 118 [2022-06-06 01:19:53]:
The most noticeable effect on the rich world was the novel phenomenon of ‘stagflation’. According to the Keynesian consensus of the post-war period, a stable relationship was supposed to exist between inflation and employment. When the economy was operating at full capacity, prices would increase because all available resources (including labour) were being used in production, pushing up prices (and wages). The inverse was thought to be true when production and growth slowed.
But during the 1970s, unemployment and inflation were both high at the same time. Looking back, the reason for this — the formation of OPEC — is quite obvious. But the chaos and confusion of the time provided neoliberal economists with an opportunity to push a new theory of inflation: monetarism

Page 128 [2022-06-08 23:07:18]:
bringing social housing that was sold off under Right-to-Buy back into the public sector.
Thousands of tenants all over the country have found themselves paying extortionate rents to private landlords in homes that were once built and managed by the state — effectively a state-backed handout to the rich. Investing in social housing is the only way to solve our housing crisis

Page 132 [2022-06-08 23:10:14]:
Rent, in other words, is not just an economic phenomenon: it’s a reflection of the prevailing political order, and the struggles that define it

Page 132 [2022-06-08 23:10:30]:
This ‘30% rule’ has its roots in American president Lyndon B. Johnson’s ‘Great Society’ programmes, when, following 1968’s Housing and Urban Development Act, legislation was passed capping rents in public housing at 25 per cent of residents’ income. This in turn was raised to 30 per cent in the 1980s

Page 134 [2022-06-08 23:12:11]:
Between 1946 and 1979, 50 per cent of all houses completed in the UK were council houses. Between 1979 and 2020, that figure drops to 5 per cent

Page 139 [2022-06-09 01:25:20]:
In England in 1949 close to 140,000 council houses were built. In 1999 this number was fifty

Page 143 [2022-06-09 02:52:56]:
As home secretary, Priti Patel has been explicit that the draconian anti-protest provisions of the Police, Crime, Sentencing and Courts Bill are directed against new environmental and racial equality movements in particular.

Page 150 [2022-06-09 02:58:52]:
civil liberties campaign called Kill the Bill.
A response to far-right culture war hostility to both racial justice and climate protection activism, this partnership of Black Lives Matter, Extinction Rebellion, and a number of grassroots and civil society activist coalitions may be the most prominent social movement of recent times. That it is peaceful, inclusive, and has for the most part

Page 154 [2022-06-09 03:01:46]:
Nationality and Borders Bill, which is being voted on tonight, is the harshest crackdown on migrant rights in decades

Page 163 [2022-06-10 06:09:07]:
Donna McLean, who was deceived into a relationship as part of the Spycops scandal, speaks to Tribune about the deliberate nature of abuse by undercover police, the wider web of corruption exposed by recent revelations — and why she feels the public inquiry process offers little justice

Page 196 [2022-06-10 06:15:54]:
Ultimately, the business model of BetterHelp is not only the product of but also works to entrench the very economic system that contributes so much to the mental health crisis that creates its customer base

Page 198 [2022-06-10 06:16:46]:
Since her election in December 2019, Zarah Sultana has been a political phenomenon. A high-profile socialist whose regular interventions against capitalism and war give voice to perspectives rarely heard in the halls of power, she has become one of Westminster’s most effective communicators — building platforms of hundreds of thousands of followers acrosssocial media platforms

Page 217 [2022-06-10 06:49:16]:
Finally, a question that has been on my mind. I agree with you that there are a lot of people of my age and your age who remain committed to fundamental political change. Many of them would call themselves socialists, but even many who don’t would agree with the policies we support. And yet, the only way they have to express these politics, often times, is social media

