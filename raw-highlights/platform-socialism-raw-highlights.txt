--------------------------------------------------------------------------------
James Muldoon - Platform Socialism
--------------------------------------------------------------------------------
Page 13 [2022-01-24 00:11:28]:
technological determinism

Page 13 [2022-01-24 00:12:22]:
In the absence of any bold ideas from politicians, the tech world has claimed ownership over the future tense

Page 15 [2022-01-25 00:05:01]:
It has become easier for us to imagine humans living forever in colonies on Mars than exercising meaningful democratic control over digital platforms

Page 15 [2022-01-25 00:05:49]:
By creating the digital infrastructure that facilitates online communities, platform companies have inserted value capture mechanisms between people seeking to interact and exchange online

Page 15 [2022-01-25 00:06:14]:
Digital platforms are tools that enable a more sophisticated business model for exploiting our social interactions and connections with others

Page 16 [2022-01-25 00:08:02]:
We need to shift our focus from ‘privacy, data and size’ to ‘power, ownership and control

Page 17 [2022-01-25 00:09:57]:
How do we imagine an alternative that is neither private oligarchy nor unaccountable state bureaucracy – an alternative outside of rule by Big Tech or Big State

Page 17 [2022-01-26 09:44:37]:
I call this idea platform socialism – the organisation of the digital economy through the social ownership of digital assets and democratic control over the infrastructure and systems that govern our digital lives

Page 17 [2022-01-26 09:45:06]:
Platform socialism describes both an ideal and a process

Page 18 [2022-01-26 09:49:54]:
Rather than just trying to fix Facebook, we should start to imagine what better alternatives could take its place

Page 18 [2022-01-26 09:53:19]:
platform socialism is about reclaiming a long-term counter-hegemonic project for challenging capitalist control over technology

Page 18 [2022-01-26 09:53:54]:
This movement is not a quest for an ideal or harmonious society but is driven by antagonistic practices and a resistance to commodification and exploitation. It gestures beyond piecemeal reforms and the bland crisis management and troubleshooting that characterises much of our present response to Big Tech

Page 19 [2022-01-26 09:55:04]:
Marx and Engels declined to write ‘recipes’ for the ‘cook-shops of the future’ and concentrated on a detailed analysis of the capitalist economy. They opposed their own scientific socialism to the ‘utopian socialism’ of those who imagined societies of the future, but who failed to base their theories on the movement of existing social forces

Page 19 [2022-01-26 09:55:31]:
We have good reason to doubt the cogency of what has been called Marx’s ‘utopophobia

Page 20 [2022-01-26 09:57:57]:
Without a clear vision of the future and an alternative to the ideological framework of ‘capitalist realism,’ it can be difficult to imagine how another world could be possible

Page 20 [2022-01-26 09:58:17]:
It is strategically unsound to always be on the defensive, waiting to protest the latest round of capitalist tech innovation

Page 21 [2022-01-26 09:59:54]:
First, platform socialism is concerned with expanding the realm of human freedom by enabling communities to actively participate in their own self-governance

Page 21 [2022-01-26 10:00:36]:
It is about creating new digital platforms in which citizens can take back control over their services and public spaces

Page 22 [2022-01-26 10:07:48]:
Before the dominance of a liberal understanding of negative liberty, emancipatory groups strived for a conception of freedom as collective self-determination

Page 22 [2022-01-26 10:08:09]:
Freedom in this sense is understood as an ongoing collective struggle and must be practised rather than enjoyed as a passive condition

Page 22 [2022-01-26 10:09:09]:
Second, it strives for social ownership over digital assets – the critical infrastructure, software and organisations of the digital economy

Page 23 [2022-01-26 10:11:39]:
Social ownership is neither pure state ownership nor worker ownership. Centralising all property in the instrument of the state risks it devolving into a new bureaucracy, whereas pure worker ownership discriminates against the many people who do not engage in full-time paid labour and creates tensions between workers in different parts of the economy

Page 23 [2022-01-26 10:12:11]:
broad ecology of social ownership acknowledges the multiple and overlapping associations to which individuals belong and promotes the flourishing of different communities from mutual societies to platform co-operatives, data trusts and international social networks

Page 23 [2022-01-26 10:12:53]:
Third, platform socialism enacts community control over the governance of digital platforms

Page 24 [2022-01-26 10:14:40]:
The move from shareholder primacy over appointing the board of a company to multi-stakeholder governance structures changes the purpose of digital platforms from maximising profit to creating social value

Page 25 [2022-01-26 10:15:04]:
Fourth, platform socialism seeks to ensure that the social and economic benefits of digital technology are shared more equitably throughout society

Page 25 [2022-01-26 10:18:19]:
In addition, it puts forward an idea of data not as a commodity but as a collective resource to be held in common and used to empower citizens and help them solve shared problems

Page 25 [2022-01-26 10:18:47]:
Fifth, an emancipatory movement should aim to combat power inequalities based on social hierarchies

Page 26 [2022-01-26 10:21:09]:
Big Tech is global in scope, but also fundamentally colonial in character

Page 27 [2022-01-26 10:30:37]:
Sixth, it fosters a culture of collaboration, solidarity and hope in which a spirit of innovation and invention is harnessed for socially useful ends

Page 27 [2022-01-26 10:30:42]:
The problem-solving and tinkering culture which has long been part of the technology world can only be fully realised when technology is liberated from capitalism

Page 28 [2022-01-26 10:32:49]:
this book proposes a series of concrete institutional reforms to recode how the digital economy operates

Page 29 [2022-01-26 10:34:33]:
The world’s largest digital platforms present an important case study of what is happening at the cutting edge of capitalist development. The largest and most profitable companies in the world are now mostly American tech companies

Page 31 [2022-01-26 10:36:58]:
The approach advocated here is a threefold strategy of resisting, regulating and recoding existing digital platforms

Page 31 [2022-01-26 10:39:46]:
A thriving ecosystem of well-established alternative models also weakens the power of major tech companies to control how we envision the future

