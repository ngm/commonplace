--------------------------------------------------------------------------------
Hadas Thier - A People's Guide to Capitalism: An Introduction to Marxist Economics
--------------------------------------------------------------------------------
Page 9 [2021-10-10 00:23:48]:
The unthinkable scale of the tragedy is the result of a capitalist perfect storm.
First, the increasing number of novel viruses is linked to the rise of factory farming, city encroachment on wildlife, and an industrial model of livestock production.3 Second, budget cuts and a systematic undermining of health care systems across the world—at varying levels of crisis—have left countries incapable of handling a public health emergency. Finally, as coronavirus rips through our communities, the dark reality of class inequality is laid bare: who will be most vulnerable to infection, who will receive treatment, and who will be left to die? Millions of frontline workers—from nurses to grocery clerks to delivery persons to the homeless largely unprotected and unable to stay home—will bear the brunt of the death toll.

Page 15 [2021-10-10 20:45:56]:
Nevertheless, as Jacobin editor Bhaskar Sunkara wrote: “The core of the system he described is little changed. Capitalism is crisis-prone, is built on domination and exploitation, and for all its micro-rationality has produced macro-irrationalities in the form of social and environmental destruction

Page 15 [2021-10-10 20:46:36]:
Marx’s economic theory and political method of analysis are critical tools for activists today. Marxism is a means to understand and dismantle the world of the 1 percent, a world that exploits, disenfranchises, oppresses, and dispossesses the many for the sake of the few; a world that may not make it to the next century with our planet and our humanity intact. I wrote this book for those of us who are attempting to make sense of the world in order to change it.

Page 16 [2021-10-10 20:47:51]:
Capital’s three volumes were written to provide a theoretical arsenal to a workers’ movement for the revolutionary overthrow of the system—and to do so on the most scientific foundation possible

Page 17 [2021-10-10 20:49:10]:
At its core, capitalism was defined by Marx as a social relation of production. He meant that profits are not the result of good accounting or the inventive ideas of the superrich, but are instead the outcome of an exploitative relationship between two classes of people: bosses and workers

Page 24 [2021-10-10 23:33:09]:
money, according to Augier, “comes into the world with a congenital bloodstain on one cheek,” capital comes dripping from head to foot, from every pore, with blood and dirt

Page 25 [2021-10-10 23:34:44]:
other words, if human history took place over the course of a single day, capitalism only unfolded three minutes before midnight

Page 27 [2021-10-10 23:39:39]:
The advent of agriculture opened up the possibility of producing a surplus of food items, which could be stored for future need. Rather than the precariousness of hand-to-mouth living, the safety of that surplus on hand meant that communities could settle and grow, and some members of the group could specialize in the production of non-food items

Page 27 [2021-10-10 23:41:48]:
classes”—groups of people with different relationships to the production, ownership, and distribution of wealth and goods

Page 28 [2021-10-10 23:45:08]:
Feudalism essentially divided monarchic lands among local lords. These lords then ruled over the inhabitants of their respective estates. Tied to the lord’s estates, serfs (unfree peasants) were put to work. They had to pay their lords with money, or with a portion of their harvest, or by tilling the lord’s fields for a certain number of days per week.

Page 28 [2021-10-10 23:46:03]:
CAPITALISM’S “ROSY DAWN

Page 29 [2021-10-11 20:48:32]:
Rather than a peasantry violently coerced to turn over goods to their lords, capitalism created a new underclass of wageworkers—a class of people theoretically free to work where and how they pleased, but who would in practice be compelled—by economic necessity—to produce a surplus for someone else nonetheless

Page 30 [2021-10-11 20:50:36]:
Classical economists like Adam Smith argued that capital came to be through a gradually evolving division of labor, where some people became traders, and some of these traders would eventually—through thriftiness or hard work—save enough wealth to build factories and employ workers

Page 31 [2021-10-11 20:50:58]:
The division of society into haves and have-nots did not gently come to pass, and certainly not through the frugalness and intelligence of a small elite. It was the outcome of a violent upheaval, which forced large swaths of the population from their lands and traditional means of self-sufficiency

Page 31 [2021-10-11 20:51:44]:
The violence, coercion, legislation, and upheavals necessary for the birth of this new system evince just how unnatural and vicious the road to capitalism was

Page 31 [2021-10-11 20:51:56]:
WHERE WEALTH ACCUMULATES, AND MEN DECAY

Page 31 [2021-10-11 20:53:43]:
England, where capitalism gained its first foothold, it did so on the basis of what’s referred to as the “Enclosure Movement.”13 Millions of acres of common land were violently confiscated and turned into privately-owned plots during several centuries. Traditional rights to use common land for farming or grazing livestock were revoked, land was fenced in (enclosed) and restricted to private owners—whether through payment, theft, or law

Page 33 [2021-10-14 00:04:11]:
The BOURGEOISIE—or capitalists—are the class of people who own the means to produce (land, factories, tools, and materials), and employ laborers to do the work of production

Page 36 [2021-10-14 00:11:55]:
PRISONERS OF STARVATION: DISCIPLINING THE NEW WORKING CLASS

Page 38 [2021-10-15 23:56:10]:
A STATE OF THEIR OWN

Page 38 [2021-10-15 23:56:20]:
other words, at capitalism’s dawn, the rising bourgeoisie depended heavily on the power of the state to enforce its collective will

Page 40 [2021-10-15 23:59:40]:
A “PRIMITIVE ACCUMULATION” OF WEALTH

Page 40 [2021-10-16 00:09:49]:
American historian Howard Zinn described the capture and journey of Africans to slavery

Page 42 [2021-10-17 06:52:32]:
Nearly all of Britain’s growing towns and cities flourished as a result of slavery—from seaport towns like Liverpool, centered on trade of enslaved people, to manufacturing towns like Lancashire and Manchester.
Thus it wasn’t the case

Page 43 [2021-10-17 06:54:09]:
The slave plantations and the colonial settlements were built upon over a billion and a half acres of land inhabited by Native American tribes. Their brutal dispossession and ethnic extermination were nothing short of genocidal

Page 44 [2021-10-17 06:55:08]:
CONCLUSION

Page 44 [2021-10-17 06:55:36]:
capitalism is not an eternal system embedded in our nature. It has a history and an origin, and therefore, it can have an end

Page 44 [2021-10-17 06:56:36]:
context. So long as families could produce food and clothing for themselves, they did not have to work for a wage. Once the vast majority of people lost access to their lands, the organizing principles of society would change. Private ownership

Page 44 [2021-10-17 06:56:55]:
capitalism is not simply an economic or a political structure, but a system of social relationships, whose foundation is the expropriation of masses of people from the land

Page 44 [2021-10-17 06:57:16]:
The power of the few to extract labor and profits from the many is based on a relationship of economic dependence, which is historically conditioned

Page 45 [2021-10-17 06:59:08]:
CAPITALISM AND SOIL

Page 46 [2021-10-17 06:59:32]:
The forcible expropriation of peasants alienated humanity from the natural world, and led to a “metabolic rift,” as Marx described it, beginning a process that has driven us today to the brink of planetary destruction

Page 47 [2021-10-17 07:05:26]:
the original sources of all wealth—the soil and the worker

Page 49 [2021-10-17 10:48:50]:
THE LABOR THEORY OF VALUE

Page 50 [2021-10-17 11:02:06]:
THE CELLULAR STRUCTURE OF CAPITALISM

Page 50 [2021-10-17 11:03:50]:
Simply put: a commodity is defined as something that was made through human labor, satisfies a demand, and is produced for the purpose of exchange. If I bake a loaf of bread to eat, then it’s just bread. But if I bake it in order to sell it, then the loaf becomes a commodity

Page 51 [2021-10-17 11:08:07]:
Nor did Steve Jobs live in a castle of iPads. Apple products are manufactured for sale on the market.

Page 51 [2021-10-17 11:08:15]:
The separation of production from consumption is unique to capitalism and was inconceivable in earlier times because needs were so immediate
● really? The separation of production from consumption is unique to capitalism and was inconceivable in earlier times because needs were so immediate

Page 51 [2021-10-17 11:09:18]:
The producer of commodities no longer lives directly on the products of his own labor: on the contrary, he can live only if gets rid of these products

Page 52 [2021-10-17 11:11:18]:
THE DUAL CHARACTER OF A COMMODITY

Page 52 [2021-10-17 11:12:05]:
the defining aspect of commodities is that they are produced for exchange

Page 53 [2021-10-17 11:13:36]:
How are they exchanged? What determines their values in relation to other commodities

Page 53 [2021-10-17 11:15:07]:
An item’s USE-VALUE is how it is used. The use-value of bread is that it provides nourishment. The use-value of a chair is that it can be sat upon. And so on

Page 53 [2021-10-17 11:15:48]:
Regardless of whether that item is sold on a market, it still has a use-value if it is useful to someone

Page 54 [2021-10-17 11:17:33]:
The changing and often subjective “utility” of commodities is one of the reasons why it cannot be the determinant of how much something is worth on the market

Page 54 [2021-10-17 11:18:55]:
Even accounting for average needs, or in cases where the hierarchy of utility is more fixed, it makes no sense as a measure of exchange-value. If it were, bread would be more expensive than cars, and certainly more so than diamonds

Page 56 [2021-10-17 11:32:48]:
Their quantitative exchange relation is at first determined purely by chance. They become exchangeable through the mutual desire of their owners to alienate them. In the meantime, the need for others’ objects of utility gradually establishes itself. The constant repetition of exchange makes it a normal social process. In the course of time, therefore, at least some part of the products must be produced especially for the purpose of exchange
● eg the spectacle? Their quantitative exchange relation is at first determined purely by chance. They become exchangeable through the mutual desire of their owners to alienate them. In the meantime, the need for others’ objects of utility gradually establishes itself. The constant repetition of exchange makes it a normal social process. In the course of time, therefore, at least some part of the products must be produced especially for the purpose of exchange

Page 56 [2021-10-17 11:36:40]:
While you or I are primarily concerned with the use-value of items, whether we can sit on a chair or on a loaf of bread, a capitalist does not care what an item’s use is, so long as it will make him money

Page 56 [2021-10-17 11:36:46]:
Use-values only matter to capitalists insofar as they know they must produce something that can be sold, and useless things can’t be sold

Page 57 [2021-10-17 11:38:51]:
Put another way: the use-value of grain and whether there are people that need to eat is not the system’s main concern, rather its exchange-value is what matters—whether and for how much grain can be exchanged.10 And so capitalism is responsible for such grotesque events as dumping grain reserves into the ocean rather than selling at a loss in order to feed hungry people

Page 57 [2021-10-17 11:41:48]:
The question of how exchange-value is determined is, therefore, critical to understanding what makes capitalism tick

Page 58 [2021-10-17 11:42:02]:
The one property that all commodities have in common, and through which their “value” can be determined, is that each is a product of human labor

Page 58 [2021-10-17 11:42:25]:
Commodities can exchange according to the relative amount of labor-time that it takes to produce them

Page 58 [2021-10-17 11:43:31]:
One type of value does not cause the other (how useful an item is does not determine its value on the market; nor does its value on the market determine how useful an item may be to us

Page 59 [2021-10-17 11:45:02]:
In fact, it’s precisely the fact that an item has no use-value to its producer that allows it to be an exchange-value

Page 59 [2021-10-17 11:46:19]:
Simply put, you can either live in your house, or you can sell your house. You can’t have your cake and eat it too.
● at least for rivalrous goods... Simply put, you can either live in your house, or you can sell your house. You can’t have your cake and eat it too.

Page 60 [2021-10-17 12:40:13]:
The predominant explanation of value in mainstream economics today is the neoclassical theory of “marginalism.” It assumes that a commodity’s worth is determined by laws of supply and demand, mediated by what they term “marginal utility

Page 60 [2021-10-17 12:40:29]:
This theory argues that value is established on the one hand by individuals maximizing the amount of “utility” (or benefit) that they get from purchasing a good, and on the other hand, a corporation maximizing the amount of profit they can achieve through producing and selling those goods. Where these two values meet determines the cost of a product

Page 61 [2021-10-17 23:41:47]:
The marginal cost then is the cost of producing one more unit of a commodity

Page 64 [2021-10-17 23:56:04]:
Marx and Engels dismissed marginal utility as just a fancy way of saying that value is determined by supply and demand (prices go up when demand increases and go down when supply increases

Page 64 [2021-10-17 23:56:50]:
NOT BY GOLD OR BY SILVER, BUT BY LABOR

Page 69 [2021-10-19 00:15:35]:
SOCIALLY NECESSARY LABOR-TIME

Page 71 [2021-10-19 00:18:27]:
That’s why mainstream economists today are known as neoclassical, rather than classical, economists. They profess dedication to the likes of Smith and Ricardo but have discarded the troubling labor theory of value. After all, if the producers of wealth are laborers and not the bosses, then the popular slogan of May 1968, “The boss needs you; You don’t need the boss,” is not abstract hyperbole, but a concrete roadmap for the future

Page 72 [2021-10-19 00:19:50]:
The socially necessary labor-time is the average amount of time that it takes to produce a commodity “under the conditions of production normal for a given society and with the average degree of skill and intensity of labor prevalent in that society.”29 That is to say: what is the average time, using the common tools and technology within society, that it will take to construct a table? If the average is two hours—then this determines the table’s exchange-value

Page 74 [2021-10-20 00:04:28]:
GENERATIONS OF LABOR

Page 77 [2021-10-20 00:10:29]:
MONEY AND FETISHISM

Page 77 [2021-10-20 00:10:46]:
Value—which is just a crystallization of abstract labor—is represented by money

Page 77 [2021-10-21 00:08:00]:
Money conveniently stores value over time, which its owner can dispense of as he or she sees fit.

Page 78 [2021-10-22 01:02:13]:
The real social relations of production and exchange are therefore hidden behind a veil of what appears to be a relationship between money and commodities. Instead of human relationships, we have economic relationships between goods

Page 79 [2021-10-22 01:03:30]:
The thinkers of the Enlightenment promoted science and logic, yet had no problem with a warped reality in which things are powerful and valuable, while human beings relate to each other through the exchange of those things

Page 79 [2021-10-22 01:05:34]:
COMMODITY FETISHISM

Page 81 [2021-10-22 01:10:59]:
One extreme example is known as the Great Pacific Garbage Patch, a floating island of debris in the Pacific Ocean so large it is visible from space. This drifting stew of junk stretches across an area larger than the state of Texas

Page 84 [2021-10-22 01:15:17]:
To presume that markets and market signals can best determine all allocative decisions is to presume that everything can in principle be treated as a commodity

Page 85 [2021-10-22 01:16:00]:
MONEY

Page 85 [2021-10-22 01:17:29]:
THE MAN BEHIND THE CURTAIN

Page 86 [2021-10-22 23:47:02]:
Money, or the “money-form,” as Marx explained, is simply a particular physical format representing the development of “value” in market-based societies

Page 86 [2021-10-22 23:49:27]:
Having a universal measure of value is critical to any society that depends on trade and exchange—even relatively simple barter economies

Page 88 [2021-10-23 11:37:39]:
money plays several simultaneous roles: Within the circulation process, as we’ve discussed, it measures and reflects the values of commodities by providing a price tag. It then eases the process of circulation of those commodities by functioning as a convenient and easily portable medium of exchange. Finally, money can also be removed from the circulation process to serve as a store of value over time

Page 88 [2021-10-23 11:38:09]:
UNIVERSAL EQUIVALENT

Page 89 [2021-10-23 11:39:50]:
RELATIVE VALUE, and the coat is its EQUIVALENT VALUE

Page 93 [2021-10-23 11:48:55]:
The “things” that get used as money have changed over time, and “modern” people often chuckle when they hear about some of them. The Romans used salt (from which we get the world “salary”), South Sea Islanders used shark’s teeth, and several societies actually used cows. The “Three Wise Men” brought gold, frankincense, and myrrh, each of which was money in different regions at the time

Page 94 [2021-10-23 11:49:40]:
If money does not exist, or is in short supply, it will be created. In POW camps, where guards specifically outlaw its existence, prisoners use cigarettes instead. In the American colonies, the British attempted to limit the supply of British pounds, because they knew that by limiting the supply of money, they could hamper the development of independent markets in the colonies… To overcome this problem, the colonists began to use tobacco leaves as money

Page 94 [2021-10-23 11:49:54]:
SWEATING MONEY

Page 96 [2021-10-23 11:57:49]:
The circulation of commodities differs from the direct exchange of products

Page 97 [2021-10-23 11:59:14]:
A STORE OF VALUE

Page 99 [2021-10-23 12:03:16]:
GLITTERING INCARNATION

Page 100 [2021-10-23 12:04:35]:
reality, while gold is itself a commodity and has always carried its own value—equal to the socially necessary labor-time required to find, mine, and smelt it—its Wizard of Oz-like power grew out of its role as the accepted universal equivalent

Page 100 [2021-10-23 12:05:11]:
Countries around the world had developed their own systems and currencies, but the need for a universal equivalent that could be used across countries grew as capitalism became a worldwide system

Page 101 [2021-10-23 12:06:37]:
If late 17th century England laid the basis for what was to become the world monetary system (the gold standard and banknotes), this can only be understood in terms of the establishment of England’s power, especially her international and maritime power, which was then taking place

Page 102 [2021-10-23 12:11:47]:
The gold standard eventually crumbled under the pressure of the Great Depression and transformed itself for the needs of a new world order. After World War II, it was resuscitated in a different form. Allied nations met through the United Nations Monetary and Financial Conference and agreed to a system that became known as the Bretton Woods Agreement. Bretton Woods set up the International Monetary Fund (IMF) and required all currencies to be tied to the US dollar at fixed rates of exchange. The dollar, in turn, was pegged to gold

Page 103 [2021-10-23 12:12:55]:
FROM GLITTERING GOLD TO IMAGINED ELECTRONIC BITS

Page 104 [2021-10-23 12:17:52]:
Since the end of the gold standard, money became more complex and intangible, representing “value” in its own right, though it is made up of no particular substance, with no intrinsic worth

Page 107 [2021-10-23 12:21:00]:
As a result, new mining computers, which each cost several thousand dollars, have been becoming obsolete in a matter of months

Page 110 [2021-10-23 12:57:56]:
Thus a hangar which used to hold actual physical goods is now warehousing thousands of computers solving meaningless riddles, at great environmental cost, for the purpose of producing nothing but fodder for risky gambles. The magic of capitalism is at it again

Page 110 [2021-10-23 12:58:30]:
PRICE AND CURRENCY

Page 110 [2021-10-23 12:58:54]:
Price, Marx argued, is not the same thing as value. While value is determined by socially necessary labor-time, prices tend to fluctuate above and below this value, and sometimes wildly so

Page 111 [2021-10-23 13:00:09]:
At its base, PRICE is the ratio between a given quantity of a commodity and its equivalent in money

Page 113 [2021-10-23 13:05:57]:
Finally, the price of a commodity is also determined by the value of money itself

Page 116 [2021-10-23 13:10:50]:
the important point to stress here is that while it is tempting to get hung up on fluctuations in currencies and their impact on prices (indeed, this is the focus of much of mainstream economics), the deeper question lies in the determination of the values of commodities—a value which money merely reflects

Page 116 [2021-10-23 13:13:14]:
Yet the unquestioned authority that society bestows onto money masks the social relationships that determine these underlying values

Page 117 [2021-10-23 13:13:32]:
WHERE DO PROFITS COME FROM

Page 117 [2021-10-23 13:14:18]:
REAL WORLD EXCHANGE

Page 120 [2021-10-24 00:26:12]:
The point to note is that the goal of such an exchange is qualitative (gaining new use-values) rather than quantitative (making money). The purpose is to procure a different item, which you did not possess before.
The development of professional traders transformed the goal of exchange from the procurement of like items for use to the accumulation of money

