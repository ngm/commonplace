:PROPERTIES:
:ID:       1aa5d602-2495-49ff-b4e4-67a21946b455
:END:
#+title: Ulverston Repair Cafe, November 2023

I saw three things - didn't actually fix anything, but did some good diagnosis on them.

 - CD player. Sony CFD-S70.  Problem reading CDs. Error 'noCD' appearing. Cleaned the lens with microfibre and IPA. Issue remains. Cleaned the CD. Issue remains. Researched online - turns out the a bunch of items of that model had a [[https://www.sony.com/electronics/support/audio-video-boomboxes/cfd-s70/articles/S1Q0530][problem that Sony offered to fix for free]] - until 2021. No info on the actual fix though. Tried to open but access to screws was too fiddly. Owner happy to keep it just as radio.
 - Laptop. Asus Vivobook K3400P with blue power light and orange battery light stuck on permanently, but no signs of life otherwise. Holding down power button for over a minute does nothing.  Probably just a case of disconnecting and reconnecting battery. Access to battery not straightforward. As less than 2 years old advised owner to check if still in warranty first, then try local repair shop, then bring it back if still no joy.
 - Android Phone (trying to pair to Windows laptop). Coming up with Couldn't Connect message on the phone. But my phone could pair to the laptop. Their phone could pair to my laptop. Phone paired successfully on Linux booted from USB. So not a hardware problem - something wrong on Windows 10. Similar-ish issue [[https://eu.community.samsung.com/t5/galaxy-a-series/a52s-5g-pairs-but-does-not-connect-through-bluetooth-on-windows/td-p/4712104][here]].  Ran out of time, but they'll try to upgrade to Windows 11 and see what happens.
