:PROPERTIES:
:ID:       286e7abb-1b29-4e8c-abcd-98f865356591
:mtime:    20211127120815 20210724222235
:ctime:    20200719105224
:END:
#+title: Wikis as hypertexts

Kicks calls wikis a type of [[id:386f8dd5-dbba-4a6a-abee-ee9d5f065b91][hypertexting]]. I don't feel I am actively building a hypertext, like I'm not intentionally focusing on that kind of non-linear hypertextual navigation.  But it is definitely full of hyperlinks and they're a big part of the utility of it. I guess a personal wiki *is-a* hypertext, with its own special focus.  

