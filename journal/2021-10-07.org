:PROPERTIES:
:ID:       59410894-5c4e-4554-882a-d676966e980d
:mtime:    20211127120840 20211007095837
:ctime:    20211007095837
:END:
#+title: 2021-10-07

+ Your [[id:db53fbe3-e54c-4c63-8f24-fa259c61b08f][iPhone 13]] doesn't belong to you.
  + [[https://www.zdnet.com/article/your-new-iphone-13-doesnt-belong-to-you/][Your new iPhone 13 doesn't belong to you | ZDNet]] 
+ The [[id:58f83736-5212-4f2f-984f-41e7c8b66d77][Circular economy]] is unclear and lacks substance.
  + [[https://www.circularonline.co.uk/news/circular-economy-is-unclear-and-lacks-substance-new-report-finds/][Circular economy is ‘unclear and lacks substance’, new report finds]].
