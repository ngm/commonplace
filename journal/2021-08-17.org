:PROPERTIES:
:ID:       d8aaf670-5195-4e55-b434-3e5643725665
:mtime:    20211127120956 20210818013910
:ctime:    20210818013910
:END:
#+title: 2021-08-17

+ I'd like to have a bit of a hobbyist play around with [[id:430fb5f7-df03-4417-ad97-9dd414b1817f][agent-based modelling]].  I did it a long time ago with object-oriented languages, but a language with strong [[id:29fd8980-73c5-49a5-a679-e903859bb85d][actor model]] creds seems like a good place to look nowadays.  I'd like something where visualisation of the world and its agents is fairly simple.  I'm up for learning something new.  Any thoughts / advice? Two possible things that seem pretty relevant are [[id:40bb3268-f1d3-445f-a301-db8fbc5698d4][Elixir]] or [[id:df940750-e74d-4337-8dc4-7075ca65f320][Spritely Goblins]].  

+ @therealraccoon@octodon.social recommended [[id:2bea5ba6-5176-4f4c-b45f-cca48ccc2d56][NetLogo]], which is an awesome recommend.
  + I'd come across NetLogo in the past and forgotten about it, didn't realise it was still going so strong.  Part of me kind of wanted an excuse to learn some fancy new language and understand the actor model, but really as the actual goal is to play around with [[id:e044a716-096c-438e-b69a-9a2f177d781b][systems thinking]] and easily visualise the systems, NetLogo actually looks *perfect*.
