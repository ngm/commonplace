:PROPERTIES:
:ID:       18c6b446-6e5f-438f-a34f-38b7e1de2481
:mtime:    20240423175454 20240423093717
:ctime:    20240423093717
:END:
#+title: 2024-04-23

+ Read: [[id:302b3665-d6d0-4b93-a8c6-a9ac728b9a8e][What a waste: our study shows almost half of electricals sent for recycling could be reused]]
  + [[id:bac1368b-df71-478f-9f9f-43ea13e7d449][The Restart Project]] set up in a Household Waste Recycling Centre (in Brent, London).
  + Assessed the reusability of products being brought in for recycling.
  + Shocking results: almost 50% could have an easy second life (either fully functional as is, or in need of only minor repair).
  + There needs to be less focus on recycling, and much more on reuse and repair.
  + Filtering and diverting initiatives should be mandatory.
