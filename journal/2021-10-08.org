:PROPERTIES:
:ID:       c811e3ac-cab2-42be-b90b-d544e2e60a59
:mtime:    20211127120945 20211008214517
:ctime:    20211008214517
:END:
#+title: 2021-10-08

+ I hope Agora can facilitate [[id:841c19cc-578a-464f-81d7-d9838bfc4c95][communities of praxis]].
+ [[id:36fdfa5d-bb7e-41c1-8442-054bea1915ec][Repair]]
  + Bookmarked: [[https://electronicplanet.xyz/2021/05/05/new-report-electronics-repair-and-maintenance-in-the-european-union/][New Report – Electronics Repair and Maintenance in the European Union]]
+ The risk of [[id:4183228a-f253-4914-ae50-04de1347cd83][Power failure]] in the UK this winter has increased.
+ Britons have cut [[id:07b0404a-18d4-4214-a130-d7a0ac6d9e7f][Meat consumption]] by 17% over the past decade.
+ Number of [[id:a5dfb8bc-6130-4ac4-9e9d-ffccd1452364][Butterflies]] in the UK at a record low.
+ [[id:823d75df-ce83-495c-99e8-80f27f5db629][Eco-anxiety]]: the chronic fear of environmental doom
+ Listened: [[id:16721e8b-d2b6-445b-9bbc-6a992b029e96][CLIMATE, CAPITAL, AND THE STATE: An interview with Geoff Mann]]
+ [[id:fd7012bc-d6cd-4e13-8e5c-a4124009fc86][Indigenous and traditional ecological knowledge]]
+ Noding [[id:744d45d5-ea33-4203-9274-c7bd1828e70f][Permaculture]] for [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]].
