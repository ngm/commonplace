:PROPERTIES:
:ID:       d083cd8d-5ddb-4997-88b0-2fe068722fb2
:END:
#+title: 2024-02-19

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:90ec7f8a-2ae9-42be-84f1-d24b26b807ed][The Art and Science of Communism, Part 1 (ft. Nick Chavez, Phil Neel)]]
  + More good stuff.
  + Description of working in some distribution warehouse was pretty wild.
  + They dislike [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]].

+ Read: [[id:cdba187a-6ed3-4e7b-beb4-bafc2f37946a][Mute Compulsion]]
  + After recommendation in [[id:831702a8-0348-49bb-ab60-fd8f164b5a19][Forest and Factory]], and [[id:05bc848b-8f26-4d67-8e11-0d2b5a3b3269][Jathan Sadowski]] raving about it.
  + In addition to coercion ([[id:dce3101d-1da9-4ef6-b175-563eca68fa0b][violence]]) and consent ([[id:22a870fe-cbf2-4a18-b3a2-bda221227c50][ideology]]), there is the mute compulsion of [[id:7d5e4cd4-cf96-4ab9-aa30-24ae08f33da6][economic power]].

+ Listened: [[id:af33f55c-e7dc-4e25-95a8-36a4813dc527][TMK BC5: Mute Compulsion, Introduction]]
  + Very handy chat about [[id:cdba187a-6ed3-4e7b-beb4-bafc2f37946a][Mute Compulsion]].

+ [[id:0b89dd21-ed5e-4e32-86c9-f32010507732][Social reproduction]].
