:PROPERTIES:
:ID:       103b0208-efab-42e7-825c-570c5b269e4c
:mtime:    20230129114400
:ctime:    20230129114400
:END:
#+title: 2023-01-29

+ My [[id:19a27213-8906-49d1-8f26-7de7798a58da][Searx]] instance keeps on coming back with "Engines cannot retrieve results: duckduckgo (blocked), startpage (unexpected crash)" for a while now.  Need to figure out how to fix that...
