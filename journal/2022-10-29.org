:PROPERTIES:
:ID:       041da575-0c89-47b6-b513-17c5e566e68a
:mtime:    20221029173707 20221029152655
:ctime:    20221029152655
:END:
#+title: 2022-10-29

+ To read: [[id:152562ee-a03c-43ba-b919-33791cff50e7][Information Technology and Socialist Construction]]
+ Bookmarked:  [[id:8efe928f-0633-4ef3-9ba2-487e3a6e8d18][EcoSocialism and the Technoprogressive Perspective]]
  + Looks to have a nice overview of [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][ecosocialism]] contrasted with [[id:1118361a-1e06-40f8-9b12-b4a5d7b033b5][ecomodernism]].
+ Working on: [[id:d0fefec3-43c2-48ef-b533-02bc65d97a31][The role of technology in eco-socialism]]
