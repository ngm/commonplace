:PROPERTIES:
:ID:       4fa256be-e128-4ea3-bb3a-ac92aeba06d4
:mtime:    20211127120836 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-04-05

- Read [[id:a5007415-8638-4cb3-b31b-0c6d0622f879][404 Page Not Found]].
  - Good article on what we lost when web 1.0 became web 2.0.  How the bit social media platforms killed creativity and agency.
- Kicked off an emergent outline - [[id:4839284d-48e9-4216-ab52-ee6ae466d5b4][Reweirding the web]].
- [[id:945f638c-c5e8-4f13-a4b4-f2c34fa30f83][The Pattern Language of Project Xanadu]].
- What I do when I'm [[id:965d9de8-f01f-40c1-aaf0-056e85ecf4bf][Getting a phone]].
