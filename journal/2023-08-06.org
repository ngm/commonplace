:PROPERTIES:
:ID:       73392dc3-211d-460e-9ff6-40cbd1f2cd56
:mtime:    20230806120651 20230806095928
:ctime:    20230806095928
:END:
#+title: 2023-08-06

+ Listened: [[id:30941957-be6a-4838-8fa8-6a56a5827230][Microdose: Californian Capitalism]]
  + More interesting stuff in there.  Some of the later history around Palo Alto, the internet, militarism, Leftist protests, possible liberatory potential (or not) of the internet.

+ Writing: [[id:98037a1a-d552-4cd4-a210-92c57bf8bd2d][Reclaiming the stacks: August 2023 roundup]]

+ Read: [[id:01b01114-c64b-4767-abc2-3ae62d4aa3ec]['A certain danger lurks there': how the inventor of the first chatbot turned against AI]]
  + "Perhaps his most fundamental heresy was the belief that the computer revolution, which Weizenbaum not only lived through but centrally participated in, was actually a counter-revolution. It strengthened repressive power structures instead of upending them."
