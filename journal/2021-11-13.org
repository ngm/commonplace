:PROPERTIES:
:ID:       87f7526b-77b5-4024-a5c9-2d71afaff63a
:mtime:    20211127120907 20211113121542
:ctime:    20211113121542
:END:
#+title: 2021-11-13

+ Hey, it's [[id:58f63844-45b4-4f30-a540-6ee3854946af][Aaron Swartz Day]] today.

+ Some good talks/projects at the hackathon (https://www.aaronswartzday.org/)

  + [[id:20a351d8-41d2-40e5-a987-83729c575fc5][Bad Apple]] - holding law enforcement accountable and putting an end to police misconduct
  + [[id:3de8bb56-84b6-4110-979d-b2fda115cde1][Open Library]] - an open, editable library catalog, building towards a web page for every book ever published
  + [[id:d97ed81b-1ae6-4709-933f-9953b60e49e5][Internet Archive]] seem to be talking about decentralisation of IA
  + [[id:2449228e-60cf-4c50-9948-91cb0595991b][Cory Doctorow]] talking about various things including the [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]]
  + [[id:75d4a14d-010b-4e3c-ab9b-e9a5b80196f9][SecureDrop]] - whistleblower submission system to securely accept documents from anonymous sources
