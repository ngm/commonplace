:PROPERTIES:
:ID:       9e635d48-df5a-41ce-871e-17a7b9707487
:mtime:    20220807135803 20220806224936 20220806201431 20220806191422 20220806095655
:ctime:    20220806095655
:END:
#+title: 2022-08-06

+ Holy heck, the first chapter of [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]] is brutal.  A vision of hell when temperatures persist above 40 degrees.

+ [[id:f1e20e95-e8a3-4041-aa65-b55583092669][Professional-managerial class]]
  + Listened: [[id:613d646d-aa9a-425d-b0ca-eb581da4e34c][The Case Against the Professional Managerial Class with Catherine Liu]]

+ [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][Degrowth]]
  + Listened: [[id:5503f8ab-eedb-4522-ae1d-7d6a6ad909c6][How Degrowth Will Save the World with Jason Hickel]]

+ [[id:fc13c18d-5e16-4d84-b0d5-a97c51635015][Black Panther Party]]
  + Watching: [[id:559f3014-7362-4798-a7db-365219545e30][The Black Panthers: Vanguard of the Revolution]]
  + Black panther will not attack.  The first time it is attacked, it will retreat.  If it is continually attacked, it will fight back.

+ Walked along a bit of the [[id:9cac3aab-41a8-4dd7-80e7-bccf1e6ea5c6][England Coast Path]] up to [[id:51395020-8ce8-4d8f-8c58-1598e47ebf25][North Walney Nature Reserve]].
  + Saw:
    + [[id:84664ba2-55c5-4d36-abca-3e9dceeb53d6][Meadow Brown]].
    + Probably [[id:4c9469dc-af6a-4003-9f24-8b8e49f90748][Common Darter]].
    + [[id:0ff0752b-3095-47ac-815c-d4713da224cd][Sand martin]]s I think (possibly swallows)
