:PROPERTIES:
:ID:       c490b48e-4443-4bb9-9aa2-e7d58bd483d6
:mtime:    20220810232419 20220810190332
:ctime:    20220810190332
:END:
#+title: 2022-08-10

+ [[id:9b8c3b97-87c0-4008-9fc9-0791076ad7be][Food]]
  + Listened: [[id:b6f3748c-0311-49e9-855b-7b08ffe29549][THE FUTURE OF FOOD: How to achieve radical change with George Monbiot]]

+ [[id:85bb9033-ecfa-4f81-a8c8-2152e6e13f71][Climate fiction]]
  + Listened: [[id:7b9555fb-8282-4726-a6b2-03cbad9fd2fe][GND Summer Book club]]
