:PROPERTIES:
:ID:       fbb8a7db-4354-45ea-a70c-576582ed80b0
:mtime:    20220419223403 20220419213331 20220419103149
:ctime:    20220419103149
:END:
#+title: 2022-04-19

+ Read: [[id:b682ee63-e028-40e8-8f72-28bf5503b897][Inside the Real Repair Shop 7]]
  + Discusses some [[id:6cb036c5-68d9-4ed8-88a3-9e05ab12211b][barriers to repair]].

+ [[id:a03e102b-dfb4-4754-a3fa-1de0d8ed5e2e][The Global E-waste Monitor 2020]]

+ The [[id:0bf30a2e-1bde-4ffa-acca-cbc1921b085e][Climate Action Plan Explorer]] is really, really excellent.  For finding out what UK councils are doing with regards to the [[id:1c5821aa-f289-48b6-96e5-c55153fb2a47][climate crisis]].

+ Read: [[id:b8581c71-30a8-418b-9c3d-fd5ff4f2d56d][Cumbria councils to be replaced by two authorities]]
  + [[id:1e6d1d81-20ec-4c2b-8a7a-2d6b93184f90][Changes to councils in Cumbria]]
