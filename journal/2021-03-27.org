:PROPERTIES:
:ID:       1722d19b-d58e-4734-a2d7-79f2043c6233
:mtime:    20211127120845 20210724222235 20211127120806
:ctime:    20210724222235
:END:
#+TITLE: 2021-03-27

* [2021-03-27 Sat 14:36]  
  :PROPERTIES:
  :ID:       6101cae6-12ac-4c1e-b9c2-497cc471f2e2
  :END:
  Got around to [[id:2b351cd7-d4fd-4663-aef8-1cf6d6e50065][Installing Emacs from source]], so now on 27.2.50.  Surprisingly few problems so far...  let's see how we get on...
