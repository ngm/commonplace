:PROPERTIES:
:ID:       c557fea9-b2a6-4e12-be64-ca073681ea28
:mtime:    20220120230913 20220120211722 20220120174726
:ctime:    20220120174726
:END:
#+title: 2022-01-20

+ [[id:f0f2e065-5717-46ba-ace3-b78f53efb540][Maya]] has an article related to [[id:6c7318f2-3fe8-48c8-a1d7-69ad2ba1cf77][Oblique Strategies]] and [[id:392c31d7-4951-4678-bcc2-831886ff22f9][tarot for thought]] - [[https://lesser.occult.institute/introducing-randomness-into-chaos-culture-oblique-strategies-and-tarot][introducing randomness into chaos: culture, oblique strategies, and tarot for...]] (shared by Maya in hypothesis annotation to my journal from yesterday, thanks Maya!)

+ I wonder out loud if there is any linkage between [[id:2305f4a9-4fe4-4501-aa7d-d237cb4ab6ce][Law of Requisite Variety]] and the [[id:86e3bfb5-85f2-4ad0-bb80-d75ca1f6ee46][pluriverse]], [[id:80318426-c892-4d63-ac87-9b8f085be6db][Nested-I]], [[id:fe97deca-0989-4cf0-aca8-04c1701f66b5][Ubuntu Rationality]] stuff from [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].
  + e.g. if I think of the [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]] / [[id:363b6ef6-b565-450e-87f6-820daeca653e][FedWiki]] and [[id:b76e011d-2975-4351-a7a6-9c778cd1c7ca][Wikipedia]] contrast, and the claim that [[id:7b4a7663-3b97-402a-ba38-e9d8d6c856c9][Multiple voices are better than one]], then I think that multiple voices is more variety, and then more able to provide the variety needed to adequately navigate complex questions of coordination.  And the flattening of voices in to one is an unhelpful reduction of variety.
  + On the flip side, too much variety leads to a [[id:9097ceaf-db12-4de6-9cd8-3f0b08d3110a][cacophony of voices]].
  + I can not claim to know enough about either space to know if this is bollocks or not. Gut says worth exploring.

+ Listening: [[id:0ba91345-9289-47d0-a120-2be0c8e27135][Platform Socialism and Web3]].  Great stuff (the platform socialism more of interest to me than the web3, but chat around that is interesting too.)

+ Listening: [[id:7201f56a-f196-4c31-b70b-b0dd002862b8][Nathan Schneider, Pt. 2 (Blockchain Governance)]]
  + [[id:d2816eb0-6119-4816-b32d-71d877635553][New technologies are more likely to amplify the inequalities and the injustices of a society than to undo them]]
  + [[id:1134b13c-cdc2-4cbc-b1f6-8031a43682cc][Structurally decentralized technologies do not guarantee decentralized power]].
