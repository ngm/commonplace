:PROPERTIES:
:ID:       9b08a5d2-d58a-444b-b399-c84b6e078b87
:mtime:    20241201063142 20241130224959 20241130214554 20241130150032 20241130135046 20241130103955
:ctime:    20241130103955
:END:
#+title: 2024-11-30

+ [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]]
  + [[id:a1378280-8431-4538-9433-1ac4cd803ad2][Food waste]] and [[id:c95aea5c-7423-4c53-a409-70efd3e87e2a][gleaning]].

+ Writing prose in [[id:aad51368-24a7-460e-8944-255958dcf67f][Emacs]] with [[id:32be7697-0e4f-4bf9-9426-38dcfb15306a][Termux]] is a little weird.
  + It's because you have to workaround the fact that terminal emulators don't generally work well with touch keyboards.
  + Termux solves this cleverly with a text input view you can access by swiping left on the extra keys row.
  + It's neat, but not optimal for longer-form writing.
  + Options:
    + Use orgzly for longer text input.
    + Try Emacs native Android build again.
    + Stick with it, it's not that bad.

+ Going all in on [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][libre software]] and [[id:8501c18f-1c10-4b2c-a70c-53a167068694][open hardware]] is a key part of [[id:e3868d08-bfb3-4d5e-aeaf-784cc7543e0d][digital ecosocialism]].
  + That said, use of proprietary alternatives may be necessary during transition.

+ Provisioning the [[id:8b592e91-4798-425e-babb-b7062a165d57][knowledge commons]] with info on what software is good, how to use it, how to switch to it, etc, also very important.

+ [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]]
  + [[id:22533ae2-5105-4b8f-8c9e-a6a686070889][Soil]], [[id:2ea0fb65-be39-49c9-b59b-4df6d91291f2][composting]], and a surprise mention of [[id:e56699aa-a087-4e17-92ec-594d606eab10][metabolic rift]].
  + It's a well-written book - well paced, informative, humorous.
  + [[id:478f0df6-dcef-40ab-be7f-892a3386395c][Biogas]].

+ [[id:8ec029ab-9f58-4fbd-8a24-cda751bf841b][Waste]] and what you do with it is an important part of any [[id:899721b6-aa67-4b89-8b39-eec67fd65f16][system]].
  + This is part of why I'm finding [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]] so interesting.
    + Here the system is society.
  + You can try and reuse it circulate it back into your system. Or you sink it somewhere in the environment.
    + It seems though, nothing is ever truly a sink - it always comes back one way or another...
  + Or you try and reduce how much of it there is in the first place.
