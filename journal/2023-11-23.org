:PROPERTIES:
:ID:       36c962d5-9d4c-460e-bb53-d34758b063de
:mtime:    20231123095025
:ctime:    20231123095025
:END:
#+title: 2023-11-23

- Reading: [[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]]
  - I like the emphasis on an economics that is distributive by design and regenerative by design.
  - Also like the occasional references to [[id:a55b4567-7ed7-48ff-b76c-528e16be0738][biomimicry]]. Not convinced yet how applicable to economics it is - but I just have a general interest in it from [[id:0b43d410-b347-4e06-851c-ce9a2abde212][Evolutionary and adaptive systems]] days.

- Listened: [[id:4156f681-4f24-4e53-842d-ad6ae3cceaa5][Hotel Bar Sessions: Late Capitalism]]

- Today at work I:
  - Responded to a personal message from a community member.
    - We have a community and friends within it, and sometimes personal messages come via my work channels.
  - Scheduled in some things for when I'm away.
  - Did the daily inbox trawl.
  - 
