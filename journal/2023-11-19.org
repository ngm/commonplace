:PROPERTIES:
:ID:       b9eaa5a9-a97c-4b15-8f64-a0d08cfcab5e
:mtime:    20231124173057
:ctime:    20231124173057
:END:
#+title: 2023-11-19

- We had another play of [[id:698c982d-3d17-40f2-86d0-a64e42a9528b][Space Cats Fight Fascism]] today.
  - Love these [[id:f421f2ac-4b20-444f-9b08-bd88bcf9d998][Tesa Collective]] games.

- We spend a not insignificant chunk of our lives just on the upkeep of our household.
  - If it was a system, how would you describe it?
  - What are the stocks and flows? What are the processes? What system archetypes does it exhibit and what are the leverage points to make it function better?
  - I feel like ours has a few too many input flows of things and a blockage at the output which mean it gets easily cluttered.
