:PROPERTIES:
:ID:       458efdf3-1b4e-4f6f-b7bc-5ecf15b442fd
:mtime:    20240326095923
:ctime:    20240326095923
:END:
#+title: 2024-03-26

+ Further to [[id:d05d27c4-5577-438b-a208-3a600a56231c][trying out org-timeblock]], I'm now [[id:456db841-dce0-4f2b-80d9-a53e4629cc6a][trying out calfw-blocks]].
  + As part of a general attempt to be able to do timeblocking in org-mode. ([[id:71e92b34-5da0-4dab-a42e-e2aeda4ed938][Using org-mode for timeblocking]]).
  + I wouldn't say it's going swimmingly... but I'm learning plenty about spacemacs layers, doom config, and use-package.  So that's something.
