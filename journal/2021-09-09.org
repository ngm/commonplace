:PROPERTIES:
:ID:       7be22c78-820e-4eb1-a4d1-f2f3e5a942e4
:mtime:    20211127120859 20210909220528
:ctime:    20210909220528
:END:
#+title: 2021-09-09

+ The browser extension for [[id:ad0043b2-0454-413e-9fbf-0d5a9a931b12][KeePassXC]] is super handy.  It works pretty smoothly.  I was missing browser integration a bit from 1password.
