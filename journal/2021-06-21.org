:PROPERTIES:
:ID:       c79d9373-dc4c-4eee-9aed-60709ac7b94a
:mtime:    20211127120945 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-06-21

+ Read: [[id:d4e389b1-f52c-4d34-82c5-2dd20cdec7f2][Choral Explanations and OER: A Summary of Thinking to Date]]
  + On the topic of [[id:563ec48b-525a-4fc8-b548-f8a721fdfa6b][federated wikis]].  It made me think a lot of [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]].
  + I like the phrase [[id:3ec28a37-7f99-46f2-8a44-52a9c268bbc2][Stigmergic Production Using Choral Explanations]].
