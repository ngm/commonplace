:PROPERTIES:
:ID:       3cb312e9-8c25-4cd7-a3d0-5231809bb863
:mtime:    20211127120826 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-06-17

+ [[id:0ead3e7e-6ea9-44a6-92aa-a2527043e17c][We are Commoners]]
  + This exhibition invites you to become or recognise yourself as a ‘Commoner’. Featuring UK and international artists, the projects exhibited in this exhibition represent ideas and resources to inspire acts of commoning.
