:PROPERTIES:
:ID:       46c0d674-83b3-4784-b87e-3ebb67efe0fd
:mtime:    20221023111724 20221022220025 20221022185317 20221022104108
:ctime:    20221022104108
:END:
#+title: 2022-10-22

+ Reading [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]

+ [[id:1adbde58-5a6b-4e5d-8efe-70e6de8ddbbe][How are the Mahsa Amini protests being organised?]]
  + [[id:f3ed16c4-17cf-4a7a-b56f-c2cdcfc0b6fd][The Bold Tactics That Have Kept Iran Protests Going]]
  + [[id:93b0254d-ca3b-409b-8371-c73724817777][Reporting in Iran could get you jailed. This outlet is doing it anyway.]]
  + [[id:3737338d-5d54-4eae-98e1-9511dd56fabe][Hacktivists seek to aid Iran protests with cyberattacks and tips on how to bypass internet censorship]]
