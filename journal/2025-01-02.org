:PROPERTIES:
:ID:       643be55e-8f52-4e4a-9968-76b632d8dd7a
:mtime:    20250102135247 20250102120719
:ctime:    20250102120719
:END:
#+title: 2025-01-02

- Planning to update spacemacs to latest.
  - As noted recently, usually something breaks in this process.
  - So I'll look at ways to do this with minimal disruption.
  - I think trying to do it using multiple config directories seems a good approach.
    - That'll be useful for if I ever want to run spacemacs and Doom side-by-side, too, for example, or my own vanilla Emacs.
  - One issue to resolve first - you need Emacs 29 for the --init-directory flag.  I'm still on 28.1.
    - OK, I'll try and tackle that first.
- [[id:f9c24e7f-8469-4740-bf5e-7d5667a5ec43][Updating to Emacs 29 on Linux Mint]]
