:PROPERTIES:
:ID:       df136210-7e15-4e85-90ca-69a4619cf417
:mtime:    20211127120959 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-03-31

- I have joined an [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]]!  https://anagora.org/@neil
- Need to fix up my slugify function a bit.  I will use this as a reason to try out using [[id:c94b1c37-7771-4bf7-8ce4-dd1500e813a4][ERT]] (elisp testing library), too.
- Read: [[id:4e9bb0b2-fd07-470b-8cda-8b6da6dce31f][The Judgement of Paris: Facebook vs. the Communards]] by [[id:c2ccadb7-581f-4d00-9d58-42cc16783dbb][Lizzie O'Shea]].
 
  #+begin_quote
“Solidarity grows through increasing liberty, not through constraint or obligation,” writes Ross. “Personal autonomy and social solidarity do not oppose each other, but instead reinforce each other.” In an age in which online spaces feel more divisive and polarized than ever, perhaps it is time to ponder how we can create conditions of personal autonomy that give rise to greater social solidarity.

- [[https://thebaffler.com/salvos/the-judgment-of-paris-oshea][The Judgment of Paris | Lizzie O’Shea]] 
#+end_quote
  
  - I like Lizzie O'Shea's writing, e.g. [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]]. I like the link to left-wing movements from the past. I do find them a little lacking in practical application though.  There are a lot of 'what if' questions, what if we did this the way the Communards did it, etc, without really fleshing out what that means in practice in the present.

