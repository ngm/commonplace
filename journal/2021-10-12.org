:PROPERTIES:
:ID:       7d332e68-37ef-4f08-8735-bb3a31b6b683
:mtime:    20211127120859 20211012230751
:ctime:    20211012230751
:END:
#+title: 2021-10-12

+ We just watched [[id:62367c2d-0525-4beb-819c-ada11f8f7591][Sisters With Transistors]] and it was brilliant.
  + It's a documentary on some of the early electronic music pioneers - I knew [[id:ab9a8187-c3e1-477a-bdfc-a4e1ff204d87][Delia Derbyshire]] and [[id:458eb584-afa8-406d-8cd0-c08c81e4c6d1][Wendy Carlos]] already but got introduced to some amazing others.
  + For example [[id:e87af23d-f23d-4dc7-9080-6093fb1ef257][Daphne Oram]], [[id:34babfd1-b9c1-4949-855a-d0597256db4f][Suzanne Ciani]] and [[id:160428fd-0a32-471e-8e45-836e4316f973][Laura Spiegel]].

+ @bhaugen@social.coop's post (https://social.coop/@bhaugen/107088836725318619) made me think of the description of moving from network to [[id:9708006a-52ad-4864-90c0-9f0f08616382][community of practice]] in Margaret Wheatley and Deborah Frieze's paper - https://www.margaretwheatley.com/articles/
