:PROPERTIES:
:ID:       b0254637-c5ee-406e-8e8d-cd7141168d2b
:mtime:    20211127120930 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-02-04

Really enjoyed the [[id:bf1529d7-260c-427c-9202-29e620b9daf9][When Preston Meets the Doughnut]] webinar.  Inspiring to hear more about what's going on in [[id:174ed8ef-0f25-4a92-bdf3-c06dcaf6493c][Preston]], and to see how Kate Raworth has been working on adaptations of [[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]] to the city-level.
