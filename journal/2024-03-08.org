:PROPERTIES:
:ID:       f602ebcf-945e-4a10-8be7-e276197f5b0f
:END:
#+title: 2024-03-08

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:cdb3d4a3-0783-4b64-a5ae-ea382473b37e][Jeremy Hunt’s election budget for big earners and big owners]]
  + [[id:b544ab29-d3d6-4309-9a42-c1420aba953c][Spring Statement 2024]].
  + This is a budget for big earners and big owners.
  + It's not good for public services.

+ Don't think I'll be able to do a [[id:a2d442cd-fba1-4b1c-a958-abb3a0ebd753][new connections]] page.
  + At least not easily.
  + It would require amending org-roam to allow for link annotations.
  + See chat at  https://org-roam.discourse.group/t/recording-the-date-that-connections-were-made/3379
  + [[id:bcd55d9e-ef0c-4591-beac-40f90e5f23f4][Adding a new connections page to my garden]]

+ Listened: [[id:ed6474d7-b6c2-4a8d-826f-e41318871421][What if we became better Protopians?]]
  + With Monika Bielskyte.
  + Good discussion, but it seemed more utopian than protopian.
  + Lots of nice things listed for how the future could be. But, not much discussion on how to get there. Even in protopian increments.

+ [[id:e71bfecc-feda-4dc9-802f-59d180f00b32][Watched]]: [[id:67f07e28-d01e-4569-a48d-e2c6483852a8][Miners’ Strike 1984: The Battle For Britain]]
