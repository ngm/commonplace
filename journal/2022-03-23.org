:PROPERTIES:
:ID:       de8dfe3d-011b-48c6-b3c2-ff289bd74684
:mtime:    20220323205924
:ctime:    20220323205924
:END:
#+title: 2022-03-23

+ [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]] keeps going from strength to strength in its awesomeness.
  + I installed [[id:5713b69f-da53-47c4-8f1d-f554a546bf7a][Collabora Online]] and connected it to [[id:6171108a-fd08-48ca-885c-75c7576c8d13][NextCloud]].
  + Now I can edit spreadsheets online, locally in LibreOffice, and on my phone with Collabora Office and it's all synced up!

+ I wrote an email to my MP about the [[id:d8314eb6-f8d1-4325-9a22-549428ba9a75][UK energy supply strategy]].
