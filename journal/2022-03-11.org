:PROPERTIES:
:ID:       8d7b3689-1da1-4022-a2ff-0329daa072fc
:mtime:    20220311174859
:ctime:    20220311174859
:END:
#+title: 2022-03-11

+ Participated in a roundtable on [[id:489f1cef-dc6a-4e2b-9975-5ce7b4bf4202][citizen science]] and [[id:43eed9db-b26f-4002-9a9c-4ee3273b0d9b][digital action]] run by [[https://www.geog.ucl.ac.uk/research/research-centres/excites][UCL Extreme Citizen Science]] lab, as part of [[id:98acdb66-0e54-4a0e-9eb1-de4c228cdb29][Restart]].
 
+ Put in for a place on the [[https://aspirationtech.org/programs/leadership/data][Data Leadership Cohort]] that [[id:c4649644-295a-4b68-8a2b-228fc13869fd][Aspiration Tech]] is running, related to our work on [[id:ff75469a-76bb-4711-84f1-83f4377e46b9][open repair data]].  Got me thinking about [[id:353ba7d9-04c3-4003-bcb1-b60eb5039684][data commons]].

+ I've noticed that I like writing [[id:e82b2aab-967a-4b71-a3b5-b0402801c258][technical documentation]].
  + I've always had a predilection for [[id:2fabb236-4996-4d38-be0a-f5f50e7e75f3][maintenance]] rather than the creation of shiny new things.  I feel like writing docs is in keeping with that affinity.
