:PROPERTIES:
:ID:       c127d5e0-0e07-423d-930a-7863c35a15e2
:mtime:    20211127120940 20211105174035
:ctime:    20211105174035
:END:
#+title: 2021-11-05

+ [[id:cb7ba722-2be4-4b40-a06c-36494ff26acf][Build the new web in the shell of the old]]

+ Fixed the issue with redirects on my stacked notes.  Was super simple in the end.  Just took me months to get around to it :D

+ Had a bit of a play with [[id:363b6ef6-b565-450e-87f6-820daeca653e][FedWiki]] at the farm [[id:e5ff6c38-37b3-497b-80ab-b18d7184f6e3][vera]] set up.  I'm at http://neil.wiki.anagora.org.

  + It is pretty wild, let me tell you.  It kind of feels like magic, the way you can navigate links between wikis, and see other pages in your neighbourhood.

  + I feel like I will have some galaxy brain moment when it properly settles in.

  + I could honestly imagine using it as my daily driver for the kind of federation features it offers.  But I'm far too tied to local-first editing in org-roam right now.  I think it'll take me a while to move from this paradigm.

  + I think this is what [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]] does for me - some of that federated wiki goodness, but just layered on top of what I'm already using - not needing to move elsewhere.

  + FedWiki is far more advanced, but Agora grows all the time and may gain some of these federated features too.
