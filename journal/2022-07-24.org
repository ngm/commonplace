:PROPERTIES:
:ID:       6c467c66-06a1-4d98-b735-3e2a38da409f
:mtime:    20220724185330 20220724130346 20220724092742
:ctime:    20220724092742
:END:
#+title: 2022-07-24

+ [[id:ebbc4b97-7502-42de-8256-9cdd4fa94550][Energy storage]] and [[id:9f0a7882-2078-4533-82e7-8ec525a3a4b2][grid balancing]].

+ [[id:365106ea-6de6-4a42-9192-6ede6cea96a7][Associational socialism]]

+ The [[id:92762ed5-8c8d-49dc-827c-e91bca24c63b][Iceberg Model]]

+ The [[id:2b682013-5a18-4ac6-8fa3-76c3057171f1][water cycle]] is a complex system.
