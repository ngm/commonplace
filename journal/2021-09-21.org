:PROPERTIES:
:ID:       a947e0e3-0dd1-4965-b633-5465a46c8c5e
:mtime:    20211127120926 20210921145152
:ctime:    20210921145152
:END:
#+title: 2021-09-21

+ [[id:451989b1-5163-461f-bf61-7fdfd80e96f7][Communicative capitalism]]
  + [[id:86dba301-4116-4681-af37-ab7d61fe584c][Jodi Dean]]

+ [[id:46dcfc8e-a7a2-4277-ac16-e3af2584d319][The Rocky Road To a Real Transition]]

+ Noded [[id:fccc983b-8ee2-4364-82ea-bb828d4d29bd][Transition town]] and [[id:b1eca78f-bc3a-43ea-baaf-23e41d68bda1][What do I think about the Agora?]] for [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]].
