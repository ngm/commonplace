:PROPERTIES:
:ID:       e8806049-670b-4a50-931a-a07293332b55
:mtime:    20240313094542 20240312201758 20240312185939 20240312123258 20240312094747
:ctime:    20240312094747
:END:
#+title: 2024-03-12

+ Kickstarted the [[id:b529d37d-becd-495d-be37-dd91a4dc039b][spacemacs]] / spacemacs packages update dance.
  + Because of the [[id:8a2659b7-15d7-44a3-bc90-bc8bf70f9847][error "Invalid type in command series" from org-super-agenda]].
  + As always, kind of wish I hadn't.  [[id:420e1199-14af-48ae-aa42-b07f32ed36a8][Updating spacemacs 2024-03-12]].

+ I created a [[id:42c51fd9-5397-43fc-a58a-207b24e3283d][quick function to help extract bold sections from text into bullet points]].
  + Most narrative text is usually just verbose prose around a few relevant points.
  + So when I'm parsing some text, I bold the relevant bits.
  + Then I pull those out to review them as bullets.
  + This function helps quickly pull the bold text into bullets.
  + Love the fact you can so easily configure Emacs to do this kind of thing!
