:PROPERTIES:
:ID:       e0d1c2bb-cbb0-44f9-a49d-b48299877f80
:mtime:    20220404191334 20220404165015 20220404153939
:ctime:    20220404153939
:END:
#+title: 2022-04-04

+ Chatted with Alan about [[id:092629e6-3b4d-4509-9681-969469b31856][using solar panels for the home]].
  + [[id:7e5e9381-1f38-48b9-a09a-da1ac33b147d][Microgeneration]]
 
+ Chatted with Chris the other day about [[id:153c0229-1615-42e5-a00f-51c5671bb37b][AC and DC]]

+ We saw [[id:b28b34fe-965f-4484-a9f9-4ae71b68484e][marsh harrier]]s and [[id:0b899aa3-f384-4961-bf97-5fad129f416b][red kite]]s and [[id:eae67b7b-75dc-49cf-8726-59208a2f5a90][cormorant]]s.
