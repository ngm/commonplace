:PROPERTIES:
:ID:       40f3f083-86cf-4ba1-bf40-efbb6a18e208
:mtime:    20230505084734
:ctime:    20230505084734
:END:
#+title: 2023-05-05

+ I can't seem to settle on a book at the moment.  I'm making my way through [[id:bbd17ac9-3f51-45b2-bf43-a1144c6c9797][Ours to Hack and To Own]], and it's good stuff, but not page-turning bedtime reading.  Not many non-fiction books are, to be fair.  But I don't get much opportunity to read in the day, so I'm mostly reading non-fiction at night.  I started reading [[id:155e8dd2-72fc-4c45-abc0-80b4b0a7e4a1][Aramis, or the Love of Technology]] last night and enjoyed the beginning of that.  Maybe '[[id:6b4e60d3-4114-4383-a1a3-6809fe7cf4c4][scientifiction]]' is the way to go...
