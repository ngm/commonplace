:PROPERTIES:
:ID:       3526ba34-f039-4272-85d2-c4733a59e52d
:mtime:    20230409100511 20230408195710
:ctime:    20230408195710
:END:
#+title: 2023-04-08

+ Read: [[id:bc44751c-0a2a-40a8-9ca9-973824d246cc][Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers]]

+ Listened:  [[id:9c4d2964-9882-4a85-b92e-2dc3a3177dac][The Week in Green Software: Netflix, Refurbishment and Anti-Greenwashing Laws]]
  + Fun podcast. Enjoyed the discussion. Bits on recent EU right to repair legislation. And distributing stored energy in/from batteries. And the environmental benefit of buying 2nd hand.

+ Listened: [[id:c7d99736-668b-4601-93d6-021df00d273d][Trebor Scholz on Platform Cooperativism]]
  + Lots of examples of [[id:91581ce2-3841-4234-92de-452f1ae9e408][platform coops]] given. [[id:91818eb4-ea47-40dc-a0d1-b05108d32abd][Kerala]] was mentioned.
  + I enjoyed the discussion on [[id:91e8486d-cfdf-4a1f-9edc-a7353b12be08][scale]]. And particularly how coops tend to scale via federation. Some discussion of exploration of DAOs for distributed governance at scale, but with a healthy bit of scepticism.
