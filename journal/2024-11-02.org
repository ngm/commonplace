:PROPERTIES:
:ID:       830d4196-06ec-40ad-b5ff-5657172661c5
:mtime:    20241102170541 20241102054448
:ctime:    20241102054448
:END:
#+title: 2024-11-02

+ Everybody blurts, sometimes
  + [[id:66bbcffc-6d66-4487-a19b-598f693589d5][my blurts]]

+ More one handed mode configuration for Termux:
  - [[id:6e31407d-632a-4486-99aa-8e423fc3371f][making it easier to open the session drawer in Termux in right-handed one-handed mode]]
