:PROPERTIES:
:ID:       e42ec1ff-105c-46fe-be58-e605e0ce4776
:mtime:    20240512214305 20240428181907 20240428125353 20240428085925
:ctime:    20240428085925
:END:
#+title: 2024-04-28

+ [[id:9bdc71a4-aed8-44eb-920a-01968a5550fb][Ecology]]
  + [[id:a9f3d678-c933-4b7e-aff2-f550ca7ee12c][Keystone species]]

+ I should put more images in my garden.
  + I stumbled on the page on [[id:c16f89c2-7777-42f9-b70d-e69fed6df129][Jean Baudrillard]], which has no text. But the picture is fun.

+ Finally finished watching [[id:67f07e28-d01e-4569-a48d-e2c6483852a8][Miners’ Strike 1984: The Battle For Britain]].
  + National Working Miners Committee. David Hart.
  + Arthur Scargill. Sequestration of funds. Connection to Al-Zulfikar and Colonel Gaddafi for financial support.
  + Ultimately that negative press seemed to be the final nail in the coffin of the strike.

+ Read [[id:887440a5-e633-4564-8745-5bbbefdcb92f][Palestine speaks for everyone]]
