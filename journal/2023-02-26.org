:PROPERTIES:
:ID:       0fda9a0b-0996-4835-98c5-0a4131354cb0
:mtime:    20230227085956 20230226180919
:ctime:    20230226180919
:END:
#+title: 2023-02-26

+ Spent some time on [[id:c8784e29-ead7-4c90-881c-1e062c7b8e17][Ways to reclaim the stacks]]
  + looking at [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]], [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]], [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]] today
  + trying to get my head around some way of usefully pooling all these together

+ Got a half hearted setup on Boox with termux and Emacs so I can edit commonplace. Wait actually it's vim for now.  (Ironically, couldn't exit Emacs - keeps on trying to save a file when quitting.)
