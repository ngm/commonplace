:PROPERTIES:
:ID:       ac1a358e-661d-468d-83ef-d6826092f655
:END:
#+title: 2024-03-15

+ [[id:62d9a821-3335-4738-95c2-dc4a15090338][What would AI for the people look like?]]
  + My biggest problem with AI are its environmental impact and the fact that it is in the hands of a cabal of big tech firms using it to turn a profit.
  + Is there a way it could be retained? Publicly owned, democratically governed, socially useful, and existing within planetary boundaries?

+ Watched: [[id:67f07e28-d01e-4569-a48d-e2c6483852a8][Miners’ Strike 1984: The Battle For Britain]]
  + Episode 2. Focuses on the [[id:9622904b-9b94-4b39-a1e4-361bb844532b][Battle of Orgreave]].
  + [[id:1d2141d3-3692-4ca7-ab20-534197186470][Police brutality]]. [[id:5e29ef2f-0773-46e3-8ae0-eb0361264d4e][State violence]].

+ Read: [[id:f3dbd0d9-534a-41c6-836c-6db29f25c47a][A Modern Anarchism (Part 1): Anarchist Analysis]]
