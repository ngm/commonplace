:PROPERTIES:
:ID:       555661b5-7b30-42ca-a5b3-4424e5993df9
:mtime:    20211221222935 20211221172857
:ctime:    20211221172857
:END:
#+title: 2021-12-21

+ Listening: [[id:1d84358c-680c-4583-9b9d-16355cd0671a][Dancing with radical economics at the DisCO: A feminist cooperative alternative to DAOs]]

+ [[id:0a7091d9-2441-4cd3-977b-789df58e3afc][Reductionism]] and [[id:738f965a-d12b-4f71-819a-2fe73e591efc][Rationalism]] in [[id:ef3c7554-d765-4645-9631-129c22673656][Lean Logic]]

+ Saw a [[id:c18e97e1-2207-4729-992b-7203f5bed750][Cloud inversion]] over the weekend, which was incredible, and then got told about [[id:610488aa-29a5-45a0-960f-595d70f39fc7][Brocken spectre]]s, which seem absolutely wild! 
