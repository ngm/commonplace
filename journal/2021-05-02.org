:PROPERTIES:
:ID:       f38fed01-dd89-4a75-86b6-bfc857cc78f6
:mtime:    20211127121012 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-05-02

+ I'd quite like a more automated way of copying my notes here in these daily logs more easily in to some kind of [[id:22aec571-2dcd-4fc8-abe5-6f7ab4083fc5][stream]] where people see it and reply.  I've still not quite cracked that nut in a way that I would like, so I just periodically manually copy stuff in to my stream.  (Mostly the Mastodon stream at the moment.  I'd prefer to push it to my website stream first, but that's even more friction.)  It's not the end of the world, but would be nice to have it all synced up.

+ [[id:f4d90ff4-0957-46b9-b753-c12a29323010][Citizen Shift]].
  + I saw Jon Alexander talk on this at TICTeC one year.  I liked the idea - make it easy to regularly exercise [[id:d8086088-c89c-48fe-a460-92b4ae576980][civic muscle]].  Not just every 4 to 5 years of an election cycle.

+ Listened: [[id:a4b11969-3a88-4cdd-a55f-621efde3dce4][Nathan Schneider - Cooperatives, the Commons and Ownership]]    
