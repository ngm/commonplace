:PROPERTIES:
:ID:       e997eb34-a97c-4692-a353-3239546666ac
:mtime:    20211127121006 20210912210207
:ctime:    20210912210207
:END:
#+title: 2021-09-12

+ Listened: [[id:7c31036f-47ec-456b-ba40-94384c29fa80][Code Red for Humanity: the IPCC Report 2021]]
  + They do a good recap on the [[id:58ffc41a-dde4-42b8-bccf-8ce0d1010852][IPCC Sixth Assessment Report]].
  + They talk about how [[id:2f2900eb-a328-4f77-b251-c47acfc6491b][climate anxiety]] should not become [[id:c7c5f1fc-ea0f-4b33-a827-6f3a01ac70bb][climate apathy]] and the need to organise even in the face of almost certainly pushing beyond 1.5 and probably 2 degrees and things falling apart.
  + Because capitalism will not simply collapse along with the climate.  It will likely accelerate and goose step towards its latent fascistic tendencies.
  + Also, interesting discussion on the merits of individual behaviour in the midst of an obvious need for large structural change.  [[id:a323f464-f813-4ef5-98bc-923d9d8da3d8][Horizontalism vs verticalism]]. Given they tend towards [[id:7d53d9ad-4f31-4b36-8a3a-2b7c912b1c97][vanguardism]] and the need for a vertical party, good to hear some constructive thoughts on the need for [[id:55f4dec4-a979-4bac-86b9-49be08c6527a][prefiguration]] too.

+ [[id:b9f8bf2a-d082-482e-a691-7b16b420d1cf][The commodification of social activities]].

+ [[id:0c891a34-68e2-40d6-b811-2069824dfe4f][Building alternative social media]]

+ What happened to [[id:29c504b5-c186-482a-86fe-52ef94dd3fdb][Lorea]]?
  + "The basic aim of Lorea, n-1, and RedDry is to create free, federated, self-managed social networks. Social networks for the people who work on them."
  + "Lorea aims to become a network of federated networks, “a garden of shared knowledge.”"
  + https://2011.fcforum.net/en/outcomes11/ 

+ Reading (a bit of) [[id:bef17ef3-0b14-46a1-ada7-5325221baed6][bolo bolo]] for the Agora [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]].

+ Now listening to [[id:bef17ef3-0b14-46a1-ada7-5325221baed6][bolo bolo]] instead via [[id:bd3b230b-a857-4fa1-93eb-f44e7920f574][Immediatism]] which is handy while I cycle and do chores.
