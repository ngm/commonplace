:PROPERTIES:
:ID:       9f46a5d3-cfe7-44c3-86fa-1e97e25d884d
:mtime:    20230319205507
:ctime:    20230319205507
:END:
#+title: 2023-03-19

+ Yesterday had a great time at another gig put on by [[id:b6408353-6673-4b9f-957c-84e11e889c52][Full of Noises]]. [[id:e66b7e2e-c1d7-42a8-b860-2e5f0d6dd87c][Lee Patterson]]. He did a live performance and a bit of an artist talk.  Loved the live performance, amazing sounds from [[id:8558d726-3197-4d2d-81eb-5e161100269f][contact microphones]] and some kind of [[id:3e79be4c-3870-4744-b364-430cb60fcd21][photosensitive microphones]].  No effects or digitalisation, just amplification, and it made the most incredible sounds.  Springs sounded amazing through the contact mics. He also played some [[id:20ce06af-d386-4beb-9bb9-51225f0507bb][field recordings]] of audio from inside ponds from homebrew [[id:50a5087a-ec35-436b-862d-a6eb0b6bd034][hydrophones]] which were also really incredible.  Crazy throbs and pulses and sirens from little water bugs.

+ Been experimenting with being [[id:ec4050cc-ffc4-40f0-a94d-839d3423e55e][offline by default]] a little bit.
