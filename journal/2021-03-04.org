:PROPERTIES:
:ID:       0c81dd14-ca64-43c2-a5e4-fa3285e49387
:mtime:    20211127120800 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-03-04

* [2021-03-04 Thu 09:58]

  Can't quite put my finger on why, but having my garden [[id:17b479fa-6231-4bf7-a4cc-8409a3a699ac][publish automatically]] has given me a bit of a kick to note in it more often again.  And also tweak it a bit more often.

  I guess it removed some friction.  For me it's generally good to remove as much friction as possible, when it comes to writing and publishing (even tiny things).

* [2021-03-04 Thu 11:35] 

  For work purposes, I'm recently using my [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]] and [[id:20210326T232003.148801][org-roam]] as a kind of dashboard - shortcuts to the various Jira views, Google Docs, etc, that I need to work on.  So I don't bookmark them in my browser, I just flip to the right page in org-roam and hit enter on the link to where I need to go.
