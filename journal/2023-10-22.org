:PROPERTIES:
:ID:       02def429-6e64-4e1e-94be-279e448c1352
:mtime:    20231022171702 20231022121841
:ctime:    20231022121841
:END:
#+title: 2023-10-22

+ Listened: [[id:cd1622b9-4822-43e2-b427-005a0e0172b8][Degrowth and Ecosocialism | Jason Hickel]]

+ Occurs to me that technology-focused ideas around alternatives to Big Tech, that are not explicitly tied to a broader political programme, are themselves a form of [[id:cfc88600-65fa-434b-90fd-d962e3596d9e][tech exceptionalism]].  Hence I think [[id:e3868d08-bfb3-4d5e-aeaf-784cc7543e0d][digital ecosocialism]] is important.

+ Listened: [[id:53a92be2-9257-4fd0-887b-cf3db5d046cd][Culture, Power and Politics: Ecosocialism and Degrowth]]

+ Think the theme of the discursive part of my roundup this month can be around [[id:7f36f5ae-b024-4b95-bd99-37d78b141be9][ecosocialism and degrowth]], extending that a little bit to an exploration of [[id:ccde652c-45a4-4518-9451-9296d87566a5][digital degrowth]].

+ Yesterday was [[id:3199308f-3742-48bf-bda7-b41682f51a2d][Repair Day]] and it went great.  Biggest number of events we've ever listed - pulled in events from quite a few different networks, in particular the [[id:31c95933-5c09-4393-9c76-052eae89a004][Journées Nationales de la Réparation]] in France brought in a huge amount.

+ We launched the [[id:b8273205-45e0-4f95-b5b5-bf5ba19805ad][UK Repair and Reuse Declaration]].  Asking UK policymakers to introduce repair-friendly legislation.

+ Listened: [[id:db6a2be6-e717-4623-9b92-5dd1f24d70e1][Voices of Fixfest UK 2023]]

+ Read: [[id:9865729b-2c1a-4c74-b937-a370547de336][Most Brits bin electrical items if they break. These 'magic' Repair Cafés are trying to change that]]
