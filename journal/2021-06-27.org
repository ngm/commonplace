:PROPERTIES:
:ID:       121fa0cf-3b8f-46ba-bf83-52e578930c0c
:mtime:    20211127120803 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-06-27

+ [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]

+ Listening: [[id:37d1e6bf-2558-43d7-865f-4838b649f003][NO ONE WAY WORKS: Political Organisation for the 21st Century]]
  + On the topic of [[id:9d79cc54-da5c-4932-8e0a-1cee8adffbce][Political organisation]]... an interview with Rodrigo Nunes about his new book [[id:96a37fb7-282b-44a5-bb88-d5332d49c581][Neither Vertical Nor Horizontal: A Theory of Political Organisation]].
