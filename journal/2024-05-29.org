:PROPERTIES:
:ID:       355975e7-4a0f-440d-8e8b-e56ab4c86373
:mtime:    20240529213416
:ctime:    20240529213416
:END:
#+title: 2024-05-29

- [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:037f8c3e-8f2c-4b58-98fc-476bdce907f6][How the North Plunders the South w/ Jason Hickel]]
  - Some of the discussion around counties reclaiming agency over their trade overlaps with the material on the [[id:26995b34-9334-4637-9764-1b183bd93710][digital trade]] agenda in the [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]].
