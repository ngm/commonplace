:PROPERTIES:
:ID:       fcab1094-d040-4b34-9c4a-cc38131d9918
:mtime:    20240429175510
:ctime:    20240429175510
:END:
:PROPERTIES:
:ID:       8c141ded-1a33-447e-9999-dfed514a74da
:mtime:    20240427191417 20240427113158
:ctime:    20240427113158
:END:
#+title: 2024-04-27

+ [[id:2e0bf126-a00f-4ab0-b128-c5ca31a93fa4][Digital weeding and watering]]
  + Adding a page for keeping track of little bits of content cleanup that need doing on my digital garden.
  + This has been very useful already.
  + I've found:
    + pages that were using old file: links (e.g. Praxis)
    + duplicate nodes (e.g. Philsophize This).
    + pages that I'm interested in, but just had never had time to write anything about (e.g. climate action)
    + mistakenly created pages
  + And it has been fun to revisit various older pages in the process.

+ I am really looking forward to the release of [[id:05bc848b-8f26-4d67-8e11-0d2b5a3b3269][Jathan Sadowski]]'s new book.
  + [[id:6ab270d7-7079-4e83-b0fe-e18469b416ae][The Mechanic and the Luddite]]

+ [[id:30b65b04-0410-414d-9e14-e6ceafbe5994][Repairing a Dell Latitude E7450]]
