:PROPERTIES:
:ID:       e724edef-a522-4127-990b-6458789e5956
:mtime:    20231103103930
:ctime:    20231103103930
:END:
#+title: 2023-11-01

- Listened: [[id:bb27e031-e6b9-4326-85ef-9e1c3cc8d50f][Today in Focus: ‘We’re totally isolated’: inside Gaza as Israel’s war intensifies]]
  - They speak to a journalist in Gaza who is trapped there along with his family.You can hear the airstrikes happening during the interview.

- [[id:b7dd7f40-7186-4997-9205-56622765b256][MermaidJS]] seems to have come a long way recently. Might start using it in conjunction with [[id:29e41c76-8b82-45ea-a971-535c5111df2f][PlantUML]].
