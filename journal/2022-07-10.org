:PROPERTIES:
:ID:       11755f2b-25e2-4b9c-9ba1-ced54f9148d5
:mtime:    20220710210535 20220710195133 20220710134433
:ctime:    20220710134433
:END:
#+title: 2022-07-10

+ [[id:019f1dfa-d3ab-4639-b8c7-45dbc3c65652][Open Referral]] has been adopted as a UK standard.
  + https://openreferral.org/uk-government-endorses-open-referral-uk/

+ [[id:1850b6f2-77a2-460b-bc3c-91db84f473da][Open Green Map]]

+ Watched: [[id:407f047f-ae0c-4ec5-a744-6202c85c4b8a][Cloud Atlas]].
  + I thought it was really good.  Entertainingly done and plenty of thoughts about human nature to cogg on.

+ Oh yeah, a couple of days back we watched [[id:e0d76de3-9356-4e88-b894-419efd6a5d42][Wonder Woman]].

+ [[id:01dd0d80-d734-41a6-a441-bbb6d7ca953a][Political theory and film]].

+ I did my first [[id:1f5ce0a9-3956-4d9f-8322-1ab95d641bac][parkrun]] in about 2.5 years yesterday.  I got a new personal worst, but still pleased to be doing it again...

+ We visited [[id:a2938006-2d4b-414e-a88c-20ab462e0b9d][Walney Island]].
