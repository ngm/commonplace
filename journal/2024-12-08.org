:PROPERTIES:
:ID:       5636c057-623a-465a-abbf-283907ccf1cd
:mtime:    20241209000922 20241208201518 20241208122649
:ctime:    20241208122649
:END:
#+title: 2024-12-08

+ [[id:b565aab2-5ec8-40ca-8faa-ca6b4a19e653][Read]]: [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]]
  + In the epilogue, a bit of reflection on how we actually reduce waste.
  + Looks at [[id:c4e3f798-c375-474a-a10f-fa3edafd838a][ethical consumption]], [[id:b4e6230e-61be-4d20-b6fc-92eb6a50493d][zero waste]] and the [[id:58f83736-5212-4f2f-984f-41e7c8b66d77][circular economy]] with a critical eye. Them having been coopted by corporations.
  + Ultimately, his conclusion seems to be: [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][degrowth]]. Consume less, produce less.
    + That doesn't really address the problems of industrial waste though.
