:PROPERTIES:
:ID:       47e8a727-a3e5-493f-8d94-0b7c08e57871
:mtime:    20211127120832 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-04-18

+ Read:  [[id:3c3aa000-7d00-4f2d-9a33-97526d3d10c0][Vanguard Stacks: Self-Governing against Digital Colonialism]].  Really good.
+ Listened: [[id:237998d4-8b6b-4530-82c5-7ccfba7cb295][Communal Luxury - NovaraFM]]
+ Bookmarked: [[id:af67a97b-f534-4de7-8ac9-63a13325f8a2][Birth of the Commune]]
+ Lancaster City Council is looking for a [[id:8572adc8-6416-4f24-bb33-f74bdb0ff88b][Community wealth building]] officer - cool to see.
