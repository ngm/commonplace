:PROPERTIES:
:ID:       c7edb9e1-97a8-4adb-91f2-8deddd084bf6
:mtime:    20211127120945 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-06-01

+ Listened: [[id:aa0bb6a5-40d8-4bda-8c24-a07f8cf748fa][‘The government failed’: Dominic Cummings takes aim at No 10’s Covid response]] 
  + Hard to know how much of Dominic Cummings to actually believe.  But some incredible stuff if true.  Including the deputy cabinet secretary apparently in March(!): "there is no plan; we're in huge trouble. [...] I think we're absolutely fucked."
  + https://www.theguardian.com/politics/2021/may/26/dominic-cummings-timeline-of-coronavirus-crisis
