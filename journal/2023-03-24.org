:PROPERTIES:
:ID:       a9eb6798-f9f5-48ac-8ebb-892b6eca3c7d
:mtime:    20230324211228 20230324140126 20230324111821
:ctime:    20230324111821
:END:
#+title: 2023-03-24

+ Starting reading [[id:2bedad4c-2f7f-4e3e-bfe0-140bfdd74b51][The Care Manifesto]] and it's very good. About putting the notion of care (back?) at the centre of society. Care taking various forms. It has a feminist, anti-colonial and ecosocialist perspective.
+ Also recently read a bit of [[id:ef214884-ae3d-4567-82b2-c20b9ecbd5b9][A People's Green New Deal]]. Interesting. Overview of various propositions for a Green New Deal and critique of them, along with a proposition for a more ecosocialist GND.
+ [[id:0e1618fe-3960-4a83-85d4-247e8384be7f][Ecosocialism]] cropping up in a few places of late.
+ Listened: [[id:dd307774-66c8-4e71-a1c9-4c3b4ec3baf4][Billions Will Die If They Don’t Listen to Us. Aaron Bastani Meets Roger Hallam.]]
+ [[id:78d3acbd-b4e3-479b-878e-c1b9cd1ebfbd][AR6 synthesis report]].
  + Act now or it's too late.  How to make that message make any difference this time?
+ Listening: [[id:0a21e4d8-184f-43bf-ad7b-b8ee6fa9b117][The Week in Green Software: Green Software Legislation]]
+ Solved: [[id:1ed0cb4a-8e8c-4e46-9c3b-602659d28c96][miniflux issue: dial tcp 127.0.0.1:5432: connect: connection refused]]
+ Trying out [[id:93a8eca0-32fc-42f4-bf6e-f16a7772eba7][using the News app from f-droid to connect to Miniflux on Android]]
+ Very cool to see [[id:ddb998c8-abf9-47a0-b666-e17e441a7e6f][Kai Heron]] swapping London for  Lancaster.  I did the same and it was a fantastic move.
  + https://twitter.com/KaiHeron/status/1623038462940090369
