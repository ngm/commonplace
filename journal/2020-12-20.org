:PROPERTIES:
:ID:       c2e5ec7d-4cca-481f-8635-bf28e5ba4e4c
:mtime:    20211127120942 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-12-20

-  A widely available self-hosting service is really needed.  

I'd kind of like something in between TBL's Solid and the ideas of Dan Hind's [[id:a12bb960-7e1e-4435-9220-da919213d7c3][British Digital Cooperative]] (https://www.common-wealth.co.uk/reports/the-british-digital-cooperative-a-new-model-public-sector-institution).

Like a state-funded kickstart of infrastructure for every citizen to have their own personal data pod, with apps that can work on it, that's not too top-down and is contextual to each municipality, with legislation on interoperability so you can pick and choose your service providers if you want.
