:PROPERTIES:
:ID:       bd633bfd-ffb1-4a87-9871-594b7fe37515
:mtime:    20211127120938 20211101211607
:ctime:    20211101211607
:END:
#+title: 2021-11-01

+ Remembered the phrase [[id:80b494b6-1d6d-4c9a-908b-f6982b0e0f71][Designing for intermittency]].  After seeing a comment on indieweb-chat about how the SSB model (of expecting to be offline sometimes) should be an important part of [[id:ca05c4e7-c03a-4aea-a301-5758e379de7b][Web 3.0]].

+ [[id:910a5921-4de6-462c-837b-34133ad5cc93][Gordon Brander]]'s post [[id:c8d3de76-b153-470d-9cd5-9fe2c47f9c43][Building a Second Subconscious]] is nice.  His properties for what might make what you could call [[id:46205b32-8fdc-4f2b-a08e-73ceb6af87bd][Convivial tools for thought]] are all pretty [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]]by.
  + https://subconscious.substack.com/p/second-subconscious

+ [[id:9e3d6c2c-9136-4121-a9bc-ac2476ce1e45][John Brown]]
  + via [[id:f6f33400-7331-428f-a160-e02233a214a6][Celebrate People's History: The Poster Book  of Resistance and Revolution]]
  + https://justseeds.org/product/john-brown/
