:PROPERTIES:
:ID:       07aa3bb4-b2b5-46ef-8e89-c03586f01cc9
:mtime:    20220412144203
:ctime:    20220412144203
:END:
#+title: 2022-04-12

+ Read: [[id:a85dd7df-299d-4a86-bb33-abf029366c54][The era of fixing your own phone has nearly arrived]]
+ Read: [[id:914c01dd-e5de-4a8e-8b6e-90ec34f060cb][Samsung is working on a Galaxy self-repair program with iFixit]]
