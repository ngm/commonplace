:PROPERTIES:
:ID:       ffa1763c-2cd9-42c1-b323-7324801bc27f
:mtime:    20241129233201 20241129053322
:ctime:    20241129053322
:END:
#+title: 2024-11-29

+ Watched [[id:6373a35f-e657-442e-a273-a47c31eb94a3][Why Spaced Repetition Doesn't (Always) Work]]
  + Clickbaity title, but good points about [[id:71f52e42-6a89-4528-92a9-160aa920b4f6][spaced repetition]].

+ [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]]
  + [[id:2b1c0029-8bc0-45b2-8ab6-1fd2981e935f][Cholera]]. [[id:51c51a95-8051-4cc0-8482-2b6a742e4b76][Sewers]].
