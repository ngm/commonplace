:PROPERTIES:
:ID:       c77a0039-2701-45fb-9e0d-5667d9d6c00b
:mtime:    20240202101342
:ctime:    20240202101342
:END:
#+title: 2024-02-02

+ Read: [[id:5e54980d-c493-4b46-a0fd-59fb91359308][Red Enlightenment: On Socialism, Science and Spirituality]]
  + Enjoying it.  I am sympathetic to the idea of a [[id:762e7048-abdc-4ad0-bf5f-29164d24ee81][secular spirituality]] of some kind.
  + Jones is continuing his referencing to [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]] and [[id:ff07e5fd-e5c2-4de3-9464-6751aecb3189][complex adaptive systems]] that was present in [[id:eecb229e-a687-4d35-9b7a-dfd9a9e5cea3][The Shock Doctrine of the Left]], so I'm enjoying it on that level too.

+ Listened: [[id:959d89af-088f-4018-99b9-e814854fc576][The Missing Revolution w/ Vincent Bevins]]
  + On the uprisings of the 2010s and their failure to manifest as leftist revolutions in the long-term.
  + On the problems of [[id:763cb241-eb6c-4b29-945d-6e06aad17233][horizontalism]].  Bit of [[id:a323f464-f813-4ef5-98bc-923d9d8da3d8][Horizontalism vs verticalism]].
  + Interview is a bit one-sided, Marxist-Leninist, missing the nuance of [[id:96a37fb7-282b-44a5-bb88-d5332d49c581][Neither Vertical Nor Horizontal]] I think.
  + Still, really interesting, and based on on the ground accounts of what happened, so definitely to take heed of.
