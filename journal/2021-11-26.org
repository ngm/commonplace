:PROPERTIES:
:ID:       5bbd2c96-a3cc-4766-a379-f349ead16f6e
:mtime:    20211127120841 20211126171926
:ctime:    20211126171926
:END:
#+title: 2021-11-26

+ [[id:758ac459-4686-4270-a34c-8b4621827d77][Modular Politics]] looks really interesting, related to [[id:17ad2816-fec5-4d3f-a8b0-c55e5c9512e9][Metagov]] and based on [[id:19cfe585-b4dc-4cdb-abac-c12e4a0e15db][Elinor Ostrom]]'s [[id:45d81a8a-83e3-46c8-afcf-9af41f680388][Institutional Analysis and Development]] framework.

+ Read: [[id:fbd770af-7e1f-45e2-b27b-649d34a6a36f][Beyond the shouting match: what is a blockchain, really?]] 
