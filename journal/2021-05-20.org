:PROPERTIES:
:ID:       f337d401-d734-40c0-9ad4-cc83c0a52b50
:mtime:    20211127121012 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-05-20

+ Listened: [[id:b884f271-fd39-4aef-aea5-22d3d93ca77b][The Internet as a Super-Commons]]

+ Bookmarked: [[id:26a6a3c9-5a51-4c0d-91b3-a619e86cf3d3][Anarchism Triumphant: Free Software and the Death of Copyright]]
