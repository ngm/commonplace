:PROPERTIES:
:ID:       380506eb-abac-44f7-acd1-5e8f45f03c36
:mtime:    20220522190019
:ctime:    20220522190019
:END:
#+title: 2022-05-22

+ [[id:2f33e9c9-9be7-4ad5-bea5-03da72312b17][REA accounting]]
 
+ Enjoying [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]] a lot.  Very nicely written.  Keen to learn more at [[id:286a25df-a49d-408d-920d-d23da4151d54][Otto Neurath]].  They talk about [[id:c3210887-3870-4bd0-88bd-e5c38ea75cc0][Leonid Kantorovich]], [[id:ed597c35-e6aa-4469-861e-954f58691831][Linear programming]], and are going to go on to talk about [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]] I believe.

+ Listening:  [[id:463005b0-054f-4d78-beb7-61a9392ea18a][Trip 24: Technology]]
