:PROPERTIES:
:ID:       eb0d85af-6f8e-4093-ad1e-4cb39413e7e5
:mtime:    20230503210450
:ctime:    20230503210450
:END:
#+title: 2023-05-03

+ Listened: [[id:8725241b-956b-4966-9a92-3f52bbf3174f][Reclaiming Time with Oliver Burkeman (In Conversation)]]

+ I started reading the intro of [[id:27a1eb6a-f39b-4d51-a821-5d3c2a8a78ec][Governing the Commons]] last night.  It was actually very readable - for some reason I thought it would be really academic.

+ [[id:4f2eb92a-67cf-406a-9198-2e4d8129bcd4][Jackson Rising Redux]] arrived, after lots of delays to the publication date.

+ [[https://www.theguardian.com/politics/2023/apr/29/keir-starmer-labour-party-home-ownership-leader-voters][Keir Starmer: 'I want Labour to be the party of home ownership']]
  + Not my [[id:f822ddda-a977-47f7-829f-4c451f017915][Labour Party]]. I want a party of abundant, long-term [[id:6d9069f7-097d-4ebe-8c75-cdaf25e32743][social housing]].

+ [[id:c6d3c2c5-600e-4d53-b812-5e38d6d1497a][Sun Thinking]] is nice 

+ [[https://www.theguardian.com/technology/2023/apr/29/could-ai-save-amazon-rainforest-artificial-intelligence-conservation-deforestation][Could AI save the Amazon rainforest?]]
  + "Conservationists in the Brazilian Amazon are using a new tool to predict the next sites of deforestation – and it may prove a gamechanger in the war on logging"
  + Just seems like the wrong way around.  "Could humans stop destroying the rainforest?" would be better.

+ [[https://www.canarymedia.com/podcasts/the-carbon-copy/whats-driving-the-surge-in-opposition-to-renewables][What’s driving the surge in opposition to renewables?]]

+ [[id:b33caad4-9711-4224-b0ea-ddd353b8f0ba][Ecosocialism 2023]] conference
