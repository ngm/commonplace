:PROPERTIES:
:ID:       5736179c-fd9e-4a98-ad4e-7131a623e13c
:mtime:    20231007215929 20231007174221 20231007120701
:ctime:    20231007120701
:END:
#+title: 2023-10-07

- Read: [[id:11693185-4562-450a-96db-faa697bd4fe3]['Capitalism is dead. Now we have something much worse']]: Yanis Varoufakis on extremism, Starmer, and the tyranny of big tech.

- Listened: [[id:88d0e0db-8cae-4c2b-b484-45703bdad062][What Does Class Mean Now?]]

- Read: [[id:8cf3f09a-398f-4bee-9207-2a88ee516d05][On Technology and Degrowth]]

- Re: [[id:0350f187-03a1-4562-836a-53c073f8ac38][Reclaim the stacks]]. Varoufakis talks about a "[[id:0f0b0b00-f676-4241-93ed-cd878de9fbe0][cloud rebellion]]".  Doctorow talks about [[id:538ac4f2-b16c-4911-a4d5-9ebc9ac5b672][seizing the means of computation]].

- Listened: [[id:5c0bd2de-9dc4-403d-965c-d4fd684bff5e][Cory Doctorow on Why the Internet Broke and How to Fix It]]
