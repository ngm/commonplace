:PROPERTIES:
:ID:       9955e9c0-bd33-4d83-b9fd-2a53bd2aae4e
:mtime:    20211127120916 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-05-17

+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]] asks a good question on the social.coop tech channel - why does [[id:57910eea-4f81-42a1-a4eb-6e0076c3dc92][social.coop]] not use [[id:e710d724-14ef-4e5a-9ed1-00d1a10f503e][Mastodon]] to coordinate [[id:ee227d4a-4ef2-436f-a68e-9877f741c0d9][governance]]?
  + In my opinion because Mastodon is not very good for governance.  It only has polls and even that is fairly new.
  + In fact I don't find Mastodon very good for even just conversation.
  + [[id:cba713bf-4fe3-4187-8226-871729b43a7e][There is a layer of governance infrastructure missing from the web]].
  + That said, having two/more platforms to coordinate things is definitely a huge piece of friction to involving a community in governance.
  + h's mockups from way back when, are interesting: https://social.coop/@h/1868600 https://social.coop/@h/1870490
 
+ Bookmark: [[id:939fb06d-aa44-4030-af88-074f287f7790][The Web of Life]]
  + Would like to read it as it gives an intro to [[id:169a5d13-b85e-4afa-a340-36d90a259885][Humberto Maturana]], [[id:34a1749a-6914-430d-a576-29a340e0c5a9][Francisco Varela]], and [[id:be976f82-2b84-434b-ab4f-b07f483b8e7c][autopoesis]].
