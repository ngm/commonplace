:PROPERTIES:
:ID:       0fae6507-7f13-4735-a5fe-ff7a1f537e80
:mtime:    20211127120802 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-11-08

- I've been reading through [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]]'s articles on his PKM system.  [[id:703b1d84-d999-430f-9331-85d39a653ae4][Ton's PKM]].  It's of great interest to me because I really respect Ton's views, and I'm keen to see where it might help me reflect on my own system. Ton tries to avoid silos and lock-in, and has recently been able to move much of his system to plain-text and [[id:2a8f07c8-8c33-44b2-8abb-37ccbf601ef4][Obsidian]], and I think many of the methods would translate to org-mode, if I wanted them to.  Thanks Ton for sharing it in such detail.  https://www.zylstra.org/blog/2020/10/100-days-in-obsidian-pt-1/
  - Will take me a while to read and digest all of it, but I think there's many tips I can pick up.
- I want to rekindle my graphical weeknotes, after [[id:640cd983-ba1e-4ab5-b7da-d860cdf75af1][Kicks]] made a mention of them.  I really liked the end result of those.  It was just hard work keeping it up.
  - I either need to streamline the process, or make it a dedicated labour of love.  I don't have much time for labours of love right now, so streamlining the process might be needed.
  - Pipeline: recent changes, day logs -> textual week log -> diagram.
  - For the diagram, I'm thinking I might try to use something like PlantUML or mermaid.
    - Cons: It'll look kind of shitty, regimented and not so fun. 
    - Pros: It'll (probably) be quicker than mucking about in LibreOffice.
      - PlantUML is something that'll be useful for me to get familiar with for work.
  - Something like impress.js might be alright too.

- I kind of see something like Ton's public sharing of the details of his [[id:703b1d84-d999-430f-9331-85d39a653ae4][PKM]] as part of a body of artifacts for the [[id:6bf4c9fa-8c23-45f3-90ae-f784dd8af415][Hacker class]].
  - That perhaps sounds a bit grandiose, but hey if [[id:4db92dd5-f837-4ecb-b0cc-f97cb3d6f5ce][Vectoralism]] is about appropriation of knowledge work and [[id:f709307b-b37e-4eb1-b6e3-6bf5036cc8d1][commodification of information]], then open descriptions and sharing of how we do knowledge work without [[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]] tools is a small counter to that.

- [[id:9abafc20-b2a5-466a-92db-5846a015e21e][Glitch Feminism]] sounds good.  "Our software and our wetware are constantly glitching. How could it be otherwise? Rather than try for perfect order, let’s embrace the glitch and find out how else it all could play out." https://www.versobooks.com/books/3668-glitch-feminism

- I like [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]]'s terminology of notes and notions.  Notes being a bit more reference/factual, notions being ones own personal ideas. [[https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-4-writing-notes/][100 Days in Obsidian Pt 4: Writing Notes – Interdependent Thoughts]]
  - for reading through articles and taking literature notes, I think I will have a subfolder.

- Also the idea of [[id:a05a73f1-11fb-4b1f-9c5e-86c856a67bfe][emergent outlines]] for creating new content.  (Speculative outlines in Sonke Ahrens terminology).
