:PROPERTIES:
:ID:       0b0b402c-84f3-4e7a-a82d-2b4389ed5449
:mtime:    20241029220725 20241029205958 20241029172643 20241029161634 20241029150545 20241029111634 20241029074036
:ctime:    20241029074036
:END:
#+title: 2024-10-29

+ [[id:f5658bdf-d634-45ef-a96d-ddf30c693177][Human physiology]] - the processes and functions of living organisms.  Rather than, say, the structure of the body or evolutionary history.

+ [[id:a4b78734-7103-4168-b46b-f896129c383e][Passive repetition]] can result in an [[id:de9f3ab2-b646-47fe-8989-e5fdc0479f23][illusion of knowing]].
  + Better to do [[id:eae52647-d84d-4e3b-ade5-6bffea571fef][active repetition]] when you can.
  + Writing in a digital garden or on social media is a form of active repetition.

+ Actually, you could also do passive repetition in a digital garden.
  - Just copying and pasting text from elsewhere would be largely passive repetition. Similar to just underlining or highlighting.
  - I think the journal aspect of a digital garden is good for active repetition.
  - A space to summarise ideas in your own words, and for "blurting".
  - So really, that's more the stream than the garden?
  - I suppose you can do active recall in both. But I'd say the stream is your working area for it, and the garden where you store what sticks long term.

+ One of the key uses of a stream and garden for me is active recall and repetition. So worth thinking about it a bit more.
  - [[id:969ae38f-4537-4732-9fc4-4095d541fda6][Active recall and passive repetition in the digital stream and garden]]

+ Trying [[id:794b2017-ae3d-46da-8cc5-717545895c90][HeliBoard]].
  - Mainly to avoid the annoying display of 'Passwords' that Gboard does when in [[id:32be7697-0e4f-4bf9-9426-38dcfb15306a][Termux]].
  - Hey, turns out HeliBoard does it too. Must be a Termux issue.
  - Still, I like that this is fully open source and available via [[id:6b8d841f-e621-4b01-b187-07581e996e5c][F-Droid]].

+ [[id:47c0e8bd-e9ce-4c9c-92fe-399517f9820d][Learning blurt]]
  - [[id:22608921-c827-4659-9a6e-593eff019302][Amino acids]] are the building blocks of [[id:628e7dff-5d38-4475-8806-8aaef6e943bb][life]].
  - They are what constitute [[id:cbd58a3f-09f1-4472-9617-1d934459968d][proteins]].
  - [[id:6f00124c-b1ea-4416-b34b-d4c6f401ffd9][Biochemistry]]

+ Traditional social media / microblogging can also be great for active recall.
  - Particularly as dialogue and group discussion is an excellent prompt for active recall.
  - However for me they also have a huge problem - distraction.
  - I can't go on the Fediverse without it ending up as a bit of a mindless scroll fest. (Which, admittedly of often a useful tool for information discovery...)

+ [[id:47c0e8bd-e9ce-4c9c-92fe-399517f9820d][Learning blurt]]
  - [[id:3d74a0af-2e12-44da-bb04-b42d331cfa80][Bacteria]]
    - Humans are teeming with them.
    - Trillions in the gut alone.
    - We couldn't exist without them. They could happily exist without us.
    - [[id:3c23de69-2cf2-4929-84c5-d99f9814fec1][Microbiology]]

+ I listened to a good ACFM episode on the gut microbiome recently: [[id:be76610c-9fa8-488e-beaf-54ce1599d7f2][ACFM Trip 41: Trust Your Gut]]

+ I think I'll explore "[[id:c80ed4fa-cb15-463a-a8d1-770b965ddd44][microblurting]]" as a thing.
  - Is microblurting a useful way to do active recall? Should one blurt in public spaces? We'll find out.
