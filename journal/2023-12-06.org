:PROPERTIES:
:ID:       768449cc-c0ac-4957-b22d-cea752fa7e38
:mtime:    20231207124430
:ctime:    20231207124430
:END:
#+title: 2023-12-06

+ Read: [[id:34e1d939-c7fc-4194-bd51-8fe3ed0b19b1][Degrowth, green energy, social equity, and circular economy]]

+ Read: [[id:8b662f9b-8e8c-4027-9edc-de5564a9a746][Freeing Ourselves From The Clutches Of Big Tech]]

+ Finally built my little electro synth kit today.
  + (It was a Christmas present last year...)
  + It was loads of fun to do.
  + Great soldering practice for a soldering newbie.
    + Lots of different components to solder together in various different ways.
  + The [[id:4d62da74-9867-4fb7-baf3-d29f6420bdde][Pinecil]] worked great.
  + [[id:7adee684-569b-4270-b787-61d579216590][Putting together the EIGHT Electro Synth]]
