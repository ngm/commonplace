:PROPERTIES:
:ID:       4a54849f-121a-4966-bf43-6f2d41bfdd41
:mtime:    20211127120833 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-02-13

- [[id:1909193b-7a8e-4516-810c-53ee2a4e7877][Issue when updating spacemacs: Failed to checkout 'evil-impaired']] 

- Trying out [[https://www.reddit.com/r/emacs/comments/ld7mpr/new_doom_themes_homagewhite_and_homageblack/][homage-white theme]] for [[id:b529d37d-becd-495d-be37-dd91a4dc039b][spacemacs]] look'n'feel.  Quite nice.

- [[id:880870c4-bd1d-4186-8e56-511c1933727a][Nextcloud issue: "Error creating conflict dialogue!"]]
