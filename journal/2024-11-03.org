:PROPERTIES:
:ID:       a11bad54-4642-49ef-bad0-b8057c4fb331
:mtime:    20241104053106 20241103215841 20241103141609
:ctime:    20241103141609
:END:
#+title: 2024-11-03

+ [[id:22f7d385-bcab-4e52-885e-904291a7dabc][Some small experiments in 'microblurting']]

+ Finished [[id:1cefb40a-ade5-48ac-9bd0-44c5876d405a][A Short History of Nearly Everything]] (audiobook)
  + Fun and informative and very wide ranging on various science topics.
  + My immediate takeaway: the Universe is sublime, Earth is amazing, life is improbable and astonishing; human intelligence is incredible, yet we are astoundingly terrible stewards of life and the planet.
    + And we need to resolve that last issue immediately.
  + Listened via [[id:cd927ac9-9b2a-4677-a304-b6730b01cc7a][libro.fm]].
