:PROPERTIES:
:ID:       d455a009-7908-4688-98a9-59f7fd6d38f4
:mtime:    20230305165519 20230305155450 20230305105008
:ctime:    20230305105008
:END:
#+title: 2023-03-05

+ [[id:353ba7d9-04c3-4003-bcb1-b60eb5039684][data commons]]
+ [[id:d193af8e-15c8-411f-b03b-e5c53a6f7e12][Is Open Street Map a data commons?]]

+ [[id:6e296dfa-7ff6-4924-8acc-622ff74f9281][Pomodoros]]
  + p0: reading [[id:a3fc1549-7835-484a-8255-40a5d9952492][YXM830]] stage 4 materials
  + p1: completing [[id:a3fc1549-7835-484a-8255-40a5d9952492][YXM830]] EMA preparation session form
  + p2: completing [[id:a3fc1549-7835-484a-8255-40a5d9952492][YXM830]] EMA preparation session form
  + p3: completing [[id:a3fc1549-7835-484a-8255-40a5d9952492][YXM830]] EMA preparation session form; learning log
  + Music
    + Agnes Obel - Avantine
    + East Forest - Held
