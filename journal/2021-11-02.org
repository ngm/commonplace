:PROPERTIES:
:ID:       7312458c-867e-4d01-8728-49c9c8133f52
:mtime:    20211127120854 20211102195333
:ctime:    20211102195333
:END:
#+title: 2021-11-02

+ How long until there's a [[id:cf29f92b-9bfc-44a8-8276-9bc5d4cca539][GitHub Copilot]] but for your [[id:0fd15ffa-f702-47e8-a8a4-c6f6e5022221][zettelkasten]].  Autocompleting your thoughts.

+ This week for [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]] I'll be noding [[id:5f449b36-4f96-468c-a39c-bcdc12868443][Liberatory technology]].  Starting from [[id:5b3b55a8-4043-4263-8f8b-db5e09816108][Murray Bookchin]]'s take on it I think.
  + https://anagora.org/node-club
