:PROPERTIES:
:ID:       69f0fdde-263f-439c-bcd6-0d4dd052e5d1
:mtime:    20220510205244
:ctime:    20220510205244
:END:
#+title: 2022-05-10

+ Been reading [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]] in the evenings of late.  Still great.  The promised plenty of the planned economy goes sour over time.  You get insights into the ways in which trying to plan everything can have unforeseen consequences.  And the social ramifications of that.  The chapters on the factory that deliberately breaks some of its machinery in order to get a replacement; on the 'pusher' who greases the wheels between different parts of the planned economy; and the horror of a psychoprophylactic childbirth regime (due to shortages of medicine, according to the author); they are all excellent.

+ Wouldn't you know it, there's an Adam Curtis doc that looks at [[id:8f9b791c-5fd6-4a72-ad8a-be048be8310f][Gosplan]].  Should be a fun watch.  [[id:109e9829-1e05-42cb-80c6-02b79d93e85f][The Engineers' Plot]].
