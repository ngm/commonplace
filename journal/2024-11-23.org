:PROPERTIES:
:ID:       a0e8da0e-f5f4-4f7e-9995-8023ed04f6b2
:mtime:    20241123170900 20241123154553
:ctime:    20241123154553
:END:
#+title: 2024-11-23

+ [[id:7a6da115-ce89-4bfa-8638-c2733f34e652][Wasteland]].
  + When Blue Planet had its episode on plastic waste in the oceans, the anti-plastic public reaction gave the price of recycled plastic a huge boost.
    + Interesting to think how television programmes can still have such a system changing effect.
    + See also the documentary about the [[id:dc9c5afe-cece-4a47-878c-c8e87bae6cd7][British Post Office scandal]].
  + Apparently a plastic bag was found at the bottom of the Mariana Trench.

+ I hope to find the time to start participating in the [[id:ce0cf63a-4bbb-47e8-b304-563b552c63b6][IndieWeb Carnival]].
  + I like it as a concept for nudging people to write in their blogs.
    + It is an example of the [[id:50496209-00c2-457c-830a-ebab62e54741][Ritualize Togetherness]] pattern of commoning, too.
      + Which reminds me... would be good to keep [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][Node Club]] going.
