:PROPERTIES:
:ID:       88877b35-5d7c-42e5-95d0-98d433841622
:mtime:    20211127120907 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-06-15

+ I think I have broken my h-card.
 
+ Revisited https://evalapply.space to remove the link to Freenode.  Happy memories, and 'tis a nice site running on [[id:b1bbfe4c-bde0-4f6b-8985-8b67b3b191a1][Haunt]] - we're keeping it up for now.

+ [[id:1f9d5ab7-a6ea-42eb-a06d-dc5c8efe5f05][Understory]] has a really very tasty looking website.
  + "Grow your mind".  "Own your data".  "Link your ideas".
  + An experimental digital garden-y social platform thing, built on [[id:1550b2cf-584d-4bf7-ba81-9fafea0af1c0][Solid]].  But from the blurb, without needing to faff about coding.
