:PROPERTIES:
:ID:       1adae584-9a8d-481b-8790-21a821f3284b
:mtime:    20220717151913 20220716123041 20220716104548
:ctime:    20220716104548
:END:
#+title: 2022-07-16

+ Reading: [[id:7c0b2f53-f07e-447b-be24-3d153e3cc72e][A Wizard of Earthsea]]
  + Can definitely see the climate change / don't interfere with nature subtext.
  + As a piece of writing, not enjoying it quite so much as [[id:73f4c8e5-1e5a-4934-9d08-6847d8a9e87d][The Dispossessed]] or [[id:fb2d9823-15fb-4fe6-84a0-b65c75807c34][The Left Hand of Darkness]], but still good.

+ The [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]] authors really don't like [[id:68b920fd-0052-447c-96ec-7f58493f0720][geoengineering]], particularly [[id:0d572ff3-ca3a-48d6-ab20-5ea88e05ac54][solar radiation management]].

+ Did the [[id:1f5ce0a9-3956-4d9f-8322-1ab95d641bac][parkrun]] again.  A bit faster than last week; stopped less times.
