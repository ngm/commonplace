:PROPERTIES:
:ID:       2065de2d-5db8-4061-a8a9-d77e08d4d2a7
:mtime:    20211127120811 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-11-07

- Thinking about my [[id:213af944-a976-4a91-9079-73846038d330][music listening strategy]] a bit today.  Finding and listening to music in a way that supports artists gives the finger to the big corps.  What's yours?

- If I wanted to convert my org-roam files for use in [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]], I think the main issue would be lack of wikilinks for internal links.  I could make use of a [[https://orgmode.org/manual/Advanced-Export-Configuration.html][filter]] function on links to resolve that, I imagine.  (I've already got a script for [[id:cabf77d5-445e-44db-a8c5-bc0f9ee61d5a][converting a whole folder of org files to markdown]]).

- I think I like the bulleted day log style that for example [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]] [[https://anagora.org/node/2020-11-07][uses]].  A bullet for each log, not a new heading.  I will try this for a while.  To be determined however, how this maps up to the [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] notion of notes and which ones I publish on my stream.

- I have both a private journal and wiki, as well as this public one.  They are two completely separate [[id:20210326T232003.148801][org-roam]] databases.  There's a slight disconnect between the two, which introduces some friction when I'm thinking about where I should be writing something.

- [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]] [[https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-6-final-observations/][mentions]] that he does not like the idea of publishing his notes to the web.  "To me that is unthinkable: my notes are an extension of my thinking and a personal tool. They are part of my inner space. Publishing is a very different thing, meant for a different audience (you, not me), more product than internal process".  This is very interesting, I'd like to spend some time to reflect on it.

- Also trying out [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]]'s method of tagging people as [[id:9a560d90-a67a-427b-9823-9e67597a475d][person]].  Not sure where that will get me, but interested to see.

- I watched the webinar [[https://www.stream.club/e/indie][The Indie Researcher: Tools for thought and internet academia]] yesterday.  It was interesting, more discursive than really actionable advice, but a few takeaways that I liked:

  - being an [[id:646d5371-69ca-4f8c-8afe-ebcb584ab17d][indie researcher]] should be about being curious in public - not necessarily being an expert in some domain with a position to defend (I think [[id:b37459d9-806e-431c-b595-01f4e65e5509][Anne-Laure]] said this)
  - being 'indie' doesn't mean you just do it all yourself - it's very much about discussion and community, too
  - it might free you to do some things that might not happen in an academic environment

- Feeling really happy that Trump lost / Biden won.  Biden might not be about to bring in a socialist revolution, but holy fuck at least he's not Trump.  It feels good for there to be a positive piece of news for once.

- Experimenting with [[id:863e959e-4774-4f3c-b448-8fed7f7584a4][placing my daily logs in a journal subfolder]].  They would then be ready for being incorporated in to an [[id:5c3a4a75-8faa-41c2-8ac3-8994dfe9b076][Agora]].  I think it'll be nicer that way just from a general tidyness perspective, too.
