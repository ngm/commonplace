:PROPERTIES:
:ID:       f6d5e266-1fb4-47fb-b310-5b694980604f
:mtime:    20240325140218 20240325110004 20240325093945
:ctime:    20240325093945
:END:
#+title: 2024-03-25

+ Although in general it feels the same (possibly slower? because I didn't compile it myself?), one thing that is *much* faster in Emacs 28 is the parsing of my huge Tasks.org file for work.  Thumbs up.
  + [[id:1ed196b6-38ea-49ad-9467-bf22798874bb][What's new in Emacs 28?]]

+ I'd like to tweak my garden a bit such that I have 'planted' and 'last tended' dates on each page.
  + I already have 'This page last updated: ...' at the bottom of every page.
  + But I'd prefer it right at the top.  Not too prominent/distracting, but I have some pretty old pages knocking around now and I'd like people to be aware that they might be outdated.

+ [[id:7219dd4c-d5b2-41d4-974f-c095f9ac9526][org-timeblock]] looks pretty good and like it'd fill my desire for a timeblocking tool for org-mode.
  + I used to use [[id:bc8c8a61-9693-40ef-b3e0-79605e61b350][Goalist]] on Android and it was great, but I got annoyed that I couldn't sync it and make use of it anywhere else.
  + So... [[id:d05d27c4-5577-438b-a208-3a600a56231c][trying out org-timeblock]].  However, hitting a bunch of issues from the beginning.
