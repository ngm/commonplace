:PROPERTIES:
:ID:       32c1582d-6a1b-45eb-b3bf-7a6c365f2bbe
:mtime:    20220326183238 20220326150008 20220326104911
:ctime:    20220326104911
:END:
#+title: 2022-03-26

+ I've been reading [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]] by [[id:a243f63f-60f7-4521-bb88-46d695e2afd3][Francis Spufford]].  It's very good.

+ Lately life circumstances have meant I'm watching a lot more 'mass appeal' film and TV and reading less.  Which is fine, it's entertaining and convivial to relationships...  but sometimes feels like time not particularly well spent as far as my own interests go.  Maybe if I whack in a bit of a pop [[id:52d20ac1-d626-472c-8240-f8c92a09e74e][cultural studies]] lens I'll get more out of it.  Possibly horribly pretentious?  Maybe, but hey ho, gotta be true to thine ownself.

+ What are alternatives to the [[id:c332e251-5499-43f6-b382-233cd8ecaf17][Hero's Journey]]?

+ Cleaning the house time is also podcast time.
  + Listened: [[id:10fa0333-875e-43b6-a596-a462f92d62d7][Sunak Comes Unstuck]]
  + Listened: [[id:85012ded-9cd1-4b4a-a296-76018624b83e][Trip 22: Democracy]]
