:PROPERTIES:
:ID:       f00a0f5c-7823-4728-8577-047811478336
:mtime:    20240320081242
:ctime:    20240320081242
:END:
#+title: 2024-03-20

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:6df90561-01d1-44ac-9959-2788e49fb44e][Black Box: Episode four – Bing and I]]

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:f651fb9a-8e06-42dd-9e6e-9dbcfeccfb73][Black Box: Episode 5 – The white mask]]

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:4ee525e6-688d-4c00-a022-75e5f53fa096][Hotel Bar Sessions: Immediacy (with Anna Kornbluh)]] 

+ [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:08750b92-06c3-487d-bf35-dc7acac71ae9][Novara FM: Not Westminster’s Whipping Boys w/ Andy Burnham and Steve Rotheram]] 
