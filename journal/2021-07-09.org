:PROPERTIES:
:ID:       0181235f-506e-4bfd-b27f-be2529d25196
:mtime:    20211127120754 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-07-09

+ [[id:cf597fd6-fcd7-404c-b39f-4f86f2ed8f89][The Fire These Times]]  I realised I was subscribed to pretty much zero podcasts that aren't presented from a wholly Western perspective.  This one is good and isn't: https://thefirethisti.me/
  
  Any other suggestions?
 
+ [[id:056f8446-a4c1-4ea4-97ae-953c6bcf8ebd][Solarpunk and Storytelling the Present and Future]].  Nice wee podcast on some [[id:c2eee50e-313d-49a0-9688-f4ada05c4182][solarpunk]] stuff.

+ [[id:8c3b49c6-1c5c-412b-8d70-3ecb370149d1][adrienne maree brown]]'s [[id:d2eae429-1b00-48d4-91e6-89f720a965f3][Emergent Strategy]] is "inspired by [[id:1b7a416b-1bc2-4a0b-b79e-e3ac14c1bb06][Octavia Butler]]'s explorations of our human relationship to change".  [[id:ecf3b6b0-fd84-4c43-b39a-9b963a13897d][God is Change]]!  I feel like I should read this soon.
