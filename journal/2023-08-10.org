:PROPERTIES:
:ID:       a33737dd-27b6-467c-bc99-5f311023fb5b
:mtime:    20230810220143 20230810203841
:ctime:    20230810203841
:END:
#+title: 2023-08-10

+ At Tuesday's repair cafe: [[id:9fa5748e-1efb-447e-8a1f-38c5f6fc3fde][Repairing a Canon Pixma TS3150]]

+ Listened: [[id:20490ec2-3b6a-42dd-b9d7-bb499187e7f1][Cory Doctorow, "The Internet Con: How to Seize the Means of Computation"]]

+ Listened: [[id:da856068-503f-4b67-802a-e4f84bea0194][Frank Jacob, "Wallerstein 2.0: Thinking and Applying World-Systems Theory in the 21st Century"]]
