:PROPERTIES:
:ID:       a4cbebb0-4524-49f3-950b-ce75a25fd2d0
:mtime:    20220108212017 20220108191113 20220108161530 20220108124247 20220108112313 20220108085132
:ctime:    20220108085132
:END:
#+title: 2022-01-08

+ [[id:a8264e89-3b0c-412e-a7eb-d368084a7756][Flancia Meet 2022-01-08]]
+ [[id:f0f2e065-5717-46ba-ace3-b78f53efb540][Maya]] has collected our chat on [[id:87164d04-c137-444e-ad22-22a0e7a5c619][Wheel of the Year]] and related stuff here - https://maya.land/fragments/neil-wheel-of-the-year/

+ Reading the news, various claims considered:
  + [[id:c547ad9e-1ce9-48a9-990c-38a96bed4b4a][Democracies are inherently unstable and susceptible to ruin by aggressive, skilled demagogues]]
  + [[id:8d92e14a-7dad-4f07-83b8-6dae6a31f4ff][There is a latent fascist waiting to emerge in all humans]]
  + [[id:60702f21-f1dc-466a-9f25-8d40faad3b0d][The menace of Trumpism is darker now than it ever was before]]
  + [[id:a8bc2cef-369a-4d8d-9738-71bccf87dfa9][Britain is better off without Bristol's monument to Colston]]

+ Continuing the addition of [[id:7f02b574-a532-4294-b356-472ce589526f][epistemic disclosure]] to notes in my digital garden, I've been adding an 'agreement vector' to any claims that I make.
  + I started using the possible answers from Magic 8-Ball for this.  e.g. "[[id:cca69ddf-2626-4c72-a23a-1077ddd610ca][Without a doubt]]", "[[id:a7800287-cff2-458c-abdd-873f2c05c944][Ask again later]]", "My sources say no".  
  + Because: a) it's funny; b) they contain both direction and magnitude of agreement, which is handy; c) I prefer to have backlinks to a phrase rather than a number.

+ [[id:2dc06c2e-169e-4cb7-be97-c4f5dd0ad9fa][Querying org-roam]]

+ [[id:c5a1b00c-ee30-473a-adef-bef1bb0a942f][There is no such thing as knowledge]]

+ Listening: [[id:91c2544f-b048-4fb5-9cb0-5c972d5a562b][The Political Economy of Solarpunk (with Andrew Dana Hudson)]]
  + [[id:b22a3a44-4077-4801-8443-fe4cdb9bf56d][Solarpunk is about technologies that de-abstract human relationships with the material world]]
  + Yass.

+ [[id:2638ee85-4432-438a-8b0a-558b61666965][Reality is identical with divinity]]
