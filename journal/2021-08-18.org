:PROPERTIES:
:ID:       54698ae3-e32b-4266-9e0a-2935f765eb1d
:mtime:    20211127120838 20210819002543
:ctime:    20210819002543
:END:
#+title: 2021-08-18

+ I enrolled on the [[id:d52cd62f-d595-4db7-9233-7a4a7bba257e][Introduction to Complexity]] course from the [[id:d6d55d81-5ec6-44e3-838d-951271d5c354][Santa Fe Institute]].  It teaches about [[id:d201b97a-3067-4a7c-adb3-61d0fd08932d][complex systems]] and uses [[id:2bea5ba6-5176-4f4c-b45f-cca48ccc2d56][NetLogo]].

+ Yes, I could be making better use of references and citations in org-roam, I think.
  + I am doing this in theory, I believe, in that I have note files on particular references.  I'm just not giving these files formal cite links, and also not linking these to a bibliography.  I don't think that matters so much for online articles, but perhaps it would be better for books and journal articles.
  + I'm confused as to whether I should be using [[id:9027b4c7-4f3c-49a1-900a-90703c97d0a8][org-cite]] or [[id:8b56e825-7492-44b9-b50e-7f535fa8f7d9][org-ref]] though.
 
+ Anyway, I think I'll set up [[id:7648d7ec-9894-458e-8340-724caad40b46][Zotero]] for at least capturing the PDFs that I want.  I do note that [[id:d6469aef-4f03-456b-b7db-2fc8831667f8][Leo Vivier]] in one of his videos seemed to be using Calibre to store his PDFs.  Given I have my normal books in Calibre, maybe I want to do that too?
  + This here [[https://forums.zotero.org/discussion/81905/zotero-vs-calibre-or-a-workflow-for-using-both][thread]] seems to think having Calibre for books and Zotero for papers is fine.

+ OK.  So perhaps org-ref is actually how you manage your bibliography file?  So kind of a Zotero alternative.  And org-cite is just how you cite things in your org files.  maybe?  Not sure...  [[https://org-roam.discourse.group/t/has-anyone-migrated-away-from-zotero-mendeley-to-just-an-org-based-solution-probably-org-ref/772][Has anyone migrated away from zotero/mendeley to just an org-based solution (...]]  

+ Enjoying the first unit of [[id:d52cd62f-d595-4db7-9233-7a4a7bba257e][Introduction to Complexity]].

+ [[https://rgoswami.me/posts/org-note-workflow/][An Orgmode Note Workflow :: Rohit Goswami — Reflections]] 

+ [[id:18bd9889-fad4-4db4-a080-0028f8d006b2][Gaia hypothesis]]
  + [[id:f4efd614-fafe-4351-8ad0-5f9f37d87084][Daisyworld]]

+ Oh my, [[id:2bea5ba6-5176-4f4c-b45f-cca48ccc2d56][NetLogo]] is amazing.
  + There's so many rich examples in the Model Explorer.
  + Including some sound-based ones!

+ [[id:2f8f252e-a2ec-4392-9a11-130d3c391598][Effective freedom]]

+ [[id:cd6a81fb-258b-4a9b-87fd-99e7e0bfe053][Paul Pangaro]]
