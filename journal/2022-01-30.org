:PROPERTIES:
:ID:       7abce827-7d1a-41f8-8961-599c642c8346
:mtime:    20220130103623
:ctime:    20220130103623
:END:
#+title: 2022-01-30

+ Saw [[id:8b592e91-4798-425e-babb-b7062a165d57][knowledge commons]] referred to as "the mutualization of productive knowledge" ([[https://wiki.p2pfoundation.net/Copyfair][here]]), I like the sound of that as a specific way to pin it down as to what it is.
  + Though I don't fully know what [[id:8f696bb1-df5b-4b78-ba95-c2093816f69d][mutualization]] means, so that needs a dig.
  + [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]] has some stuff on it of course!
