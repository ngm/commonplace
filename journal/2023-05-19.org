:PROPERTIES:
:ID:       9953896b-cf84-4c94-980e-b4f643b8cbbf
:mtime:    20230522221510 20230519190122
:ctime:    20230519190122
:END:
#+title: 2023-05-19

+ Yesterday I submitted my final output for [[id:a3fc1549-7835-484a-8255-40a5d9952492][YXM830]]!
  + [[id:074245d4-50df-4ae5-952e-422a8df158cd][Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]] was the final version.
  + Enjoyed the course and pleased to have gotten final submission submitted.  Learned a great deal along the way.  Still *lots* of work needed on the idea of reclaiming the stacks.

+ Reading [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]] (again, didn't finish last time) and loving it.  I really like [[id:cbcdc3c9-42c2-4159-9682-a3192f720b61][McKenzie Wark]]'s writing style in this.  I'm finding the argument about there now being an information-based [[id:4db92dd5-f837-4ecb-b0cc-f97cb3d6f5ce][Vectoralism]] - something even worse than capitalism - quite compelling, though I know many disagree.

+ Used the borrowed orbital sander to sand down garden table and chairs that are a bit weather beaten.
