:PROPERTIES:
:ID:       08245fd6-ade7-4d1d-93f1-b13d14667376
:mtime:    20220223212333
:ctime:    20220223212333
:END:
#+title: 2022-02-23

+ This is great.   [[https://daily.bandcamp.com/lists/upcycling-musicians-list][Musicians Turning Trash into Musical Treasure | Bandcamp Daily]].  I'm loving Congotronics.  h/t James!
  + "Using everything from old milk boxes or beat-up Volvos, here are six musicians or collectives embracing the act of upcycled instrumentation."
