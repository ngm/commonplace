:PROPERTIES:
:ID:       c1a24dd1-7891-4b2a-83f4-0d8a5d22415b
:mtime:    20230210165132
:ctime:    20230210165132
:END:
#+title: 2023-02-10

+ Reading: [[id:2137fca1-2f70-4500-8a35-017ecdb4139d][Thinking in Systems]]
+ Having a go at [[id:424f452c-859f-4d49-b74c-2c7c1055f25d][Systems thinking diagrams in PlantUML]]
