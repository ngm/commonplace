:PROPERTIES:
:ID:       f59a5318-bd35-46ae-9622-e75eb419d564
:mtime:    20220816210553 20220816171546 20220816142201 20220816121539 20220816094404
:ctime:    20220816094404
:END:
#+title: 2022-08-16

+ [[id:54baa5f8-f7b1-412a-ac98-59d6a5c3b7e3][UK water shortages 2022]]
  + Read: [[id:9f7795d7-267b-4bfd-9a5a-b4202739936e][The Observer view on the woeful state of England's water industry]]
  + There has been low rainfall.  Privatisation in the 1980s has led to a lack of investment in infrastructure (reservoirs and pipes), meaning that we don't collect enough and we leak away a huge amount.

+ A goal of my digital garden is to (discover and) document and share claims that I believe.  Or disagree with.  A work in progress (always), but you can find some at [[id:9a13c2e4-f670-4d0d-9f15-62cfa405f37d][Yes definitely]] and [[id:cca69ddf-2626-4c72-a23a-1077ddd610ca][Without a doubt]].

+ [[id:52a0b7a0-b65c-486c-8166-969d9b273279][Public ownership]]
  + [[id:9b9a3c62-ace7-4def-a6c2-0df2eb0f6fbc][We should have public ownership of water]].
  + [[id:be72358f-b6d2-4dbb-ac61-2bba3fbd88e0][We should have public ownership of energy]].
    + Read: [[id:7da061b8-a9b0-4cd2-9389-b6b1750e5dd0][It's Time to Bring Energy into Public Ownership]]
  + Listened: [[id:437fd660-a01c-4ff7-96a7-4801d1335901][Public ownership 2.0]]

+ [[id:50e8960f-1b4f-4f8d-8619-7fd64754d938][Universal basic income]], [[id:7e288212-b1fc-4116-81f2-e2e6c6c88e2b][Universal basic services]]
  + Listened: [[id:816019df-3008-46c4-82b3-3f00bdfc70a7][Universal Basic Income or Universal Basic Services?]]

+ Using [[id:356d7839-20aa-4a8c-91a5-cc513aa85d46][Pl@ntNet]] again for identifying plants.
