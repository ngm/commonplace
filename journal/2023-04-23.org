:PROPERTIES:
:ID:       ff943acb-0917-4c3d-a1c9-8d7abd5b6107
:mtime:    20230424210457 20230423172728 20230423155056 20230423031122
:ctime:    20230423031122
:END:
#+title: 2023-04-23

+ Listened: [[id:d7b12e7c-596f-4c68-9e96-d538c55b0cf3][Soviet Cybernetics and the Promise of Big Computer Socialism]]

+ [[id:8ed16d27-e6a6-47f4-898e-ca3808ef316c][Oddly Influenced]] podcast looks fun.

+ Writing up thoughts on [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]].

+ Really enjoyed the [[id:b7a3875f-03ec-475d-896f-6a6bddbbc550][Ecological Radio Workshop]] put on by [[id:b6408353-6673-4b9f-957c-84e11e889c52][Full of Noises]] yesterday.

+ Read: [[id:0a610a7e-5943-497f-878b-ea16b4e72a86][Chile's president aims to nationalize world's largest lithium supply]]
  + Not quite so radical as Allende (it'll happen when current contracts expire).
