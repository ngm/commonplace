:PROPERTIES:
:ID:       30e62361-c5df-49f1-a21b-3c13b4157430
:mtime:    20211127120820 20211103202402
:ctime:    20211103202402
:END:
#+title: 2021-11-03

+ I've been using my [[id:c106204f-5bdc-4e7a-9243-a27b2f03bda5][social reader]] a bit more again recently.  It is very cool especially when set up with [[id:2182c572-83c5-4b8d-9d4c-2889d618f2dd][brid.gy]], I can follow people on Twitter and interact with them there while not actually really being on Twitter.
