:PROPERTIES:
:ID:       44dd3b90-d724-40f5-ab29-e289857dfc36
:mtime:    20211127120830 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-12-06

- Listened to another [[id:ee39ec72-1008-479a-bc90-6eb21d492370][General Intellect Unit]] episode, this on [[id:5d291c71-c3ec-4e47-8423-ffe36cfd6194][Post Open Source]].  A bit of a downer this one, to be honest - it's about FOSS being dead.  [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][Free software]] having died a while back, and [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][open-source]] being recently departed.  There are many valid points. It's based on this [[https://www.boringcactus.com/2020/08/13/post-open-source.html][article]].  Need more time to digest it and fully read the article.

- Went for a very nice hike up to the [[id:fae17700-94f3-4de8-a26f-61b9e595942c][The Cragg]] yesterday, via [[id:0379311c-bcf0-400b-b0c2-291eac2f1cbf][Williamson Park]].

#+ATTR_HTML: :width 100%
[[file:/home/shared/commonplace/photos/williamson-park-towards-lake-district.jpg]]

#+ATTR_HTML: :width 100%
[[file:/home/shared/commonplace/photos/the-cragg-towards-bowland.jpg]]
