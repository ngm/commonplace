:PROPERTIES:
:ID:       1e4095e0-3ef2-491c-94b6-85fdab4c1a23
:mtime:    20220821161300 20220821112733
:ctime:    20220821112733
:END:
#+title: 2022-08-21

+ August is [[id:8e7a6330-0600-418b-8c90-457dac0a42c7][revolution]] month on [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]].

+ Reading: [[id:7d511dbb-e148-4793-9db5-2cf0160451b2][Revolutionary Strategies on a Heated Earth]]

+ Listening: [[id:fd9746c9-76a1-4234-a60f-f3f5dd415b37][Nick Dyer-Witheford on Biocommunism]]
