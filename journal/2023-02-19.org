:PROPERTIES:
:ID:       c36181a8-e6f6-4f5f-ab56-5496120527cd
:mtime:    20230219202541 20230219175010 20230219113834 20230219102508
:ctime:    20230219102508
:END:
#+title: 2023-02-19

+ [[id:f40ca283-5a14-4bc6-9d7e-b43213180c43][Synchronized pomodoros]]
  + [[id:6e296dfa-7ff6-4924-8acc-622ff74f9281][Pomodoros]]
    + [X] p0: setup and start listing practical actions - [[id:c8784e29-ead7-4c90-881c-1e062c7b8e17][Ways to reclaim the stacks]] looking at [[id:9b8ea846-9247-47b3-b775-082ba1a26630][Digitalization and the Anthropocene]]
      + Music: [[id:8c274340-79da-4235-9998-fc4a9900611c][The Black Dog - Brutal Five To One Mix V2]] - via [[https://stream.resonate.coop/artist/8177/release/brutal-five-to-one-mix-v2][resonate]] 
    + [X] p1: [[id:c8784e29-ead7-4c90-881c-1e062c7b8e17][Ways to reclaim the stacks]], looking at [[id:9b8ea846-9247-47b3-b775-082ba1a26630][Digitalization and the Anthropocene]] & [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]] & [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
      + Music: [[id:8c274340-79da-4235-9998-fc4a9900611c][The Black Dog - Brutal Five To One Mix V2]] - via [[https://stream.resonate.coop/artist/8177/release/brutal-five-to-one-mix-v2][resonate]] 
    + [X] p2: figuring out the leverage points in [[id:9b8ea846-9247-47b3-b775-082ba1a26630][Digitalization and the Anthropocene]] a bit better
      + Music: [[id:38a0575f-afb2-4088-828e-b5fd76c79016][The Black Dog - Dubs: Volume 4]] - via [[https://stream.resonate.coop/artist/8177/release/dubs-volume-4][resonate]]
    + [X] p3: logging data from February [[id:fc14def2-3a67-4d4c-982b-a88061562fe7][Ulverston Repair Cafe]]
      + Music: Syntheme - Lasers 'n' Shit - via [[https://stream.resonate.coop/artist/716/release/lasers-n-shit][resonate]]
