:PROPERTIES:
:ID:       83a3509e-672f-40d9-abdc-a46b2c761709
:mtime:    20250107111412
:ctime:    20250107111412
:END:
#+title: 2025-01-07

- This year, I am celebrating having a 10 year old laptop.
  - My [[id:33bdd380-c871-4fe4-b7cd-60459afe39db][Lenovo Thinkpad T450s]].
  - The release date for the T450s was 2015.
  - Though I got it second-hand in 2019.
