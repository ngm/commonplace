:PROPERTIES:
:ID:       e5353718-5015-4f83-a3e8-2e6b3b8d01c9
:mtime:    20220603224147 20220603161253 20220603150525 20220603093049
:ctime:    20220603093049
:END:
#+title: 2022-06-03

+ Starting taking [[id:657be294-5647-409a-bdc5-168d39422665][voice notes]] (again).
  + Faffed around for a while with Simple Voice Recorder app from Fdroid and syncing via Nextcloud.  SVR app is fine - but getting the sync working was annoyingly non-trivial.  Settled on just using Matrix Chat for now, which has a voice record feature, and takes care of all the syncing.  But now am looking now for an easy way to transcribe them.  Lo and behold an article on doing this already exists from Maya! https://maya.land/monologues/2021/08/05/matrix-bot-transcribe-speech-audio-messages.html  But a bit of setup involved.  Maybe I should just use otter.ai for now while I'm getting in to the habit, otherwise the friction might kill my habit.

+ Been reading the Winter 2022 issue of [[id:746f5d3c-8423-4930-a9b1-e8099ee870ba][Tribune]] on my Kobo.  [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]].
  + My brother got me a subscription to Tribune as a late birthday present.  Got some good stuff in there.  Editorial on [[id:c499a402-92d0-48d3-ab33-f3b40dda401e][Cost of living crisis]].  Article about [[id:8d7a4e3b-22a0-4ee5-b489-7940c43e9918][New Towns]].  Article about socialist science-fiction - some good recommendations in there.  Article on successful [[id:14be0045-2209-4ea0-829a-58659f2624f0][trade unionism]], specifically in refuse workers sector - some organising to drive worker conditions back up.  Also something about [[id:ca9c0ab3-1b20-437e-a15a-9f5b33684374][Partygate]] being hopefully the end for Boris Johnson.  I think this article is a few months old, doesn't seem to have happened yet though.

+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]] suggested a [[id:8d849577-05ea-4cf5-97f9-7b765bc7dd78][node club]] on [[id:72c29bc6-6095-4b77-b69e-bf038a5bbb61][utopian socialism]].
  + I'm up for that.  I've started with a transcribed voice note.

+ Listened: [[id:40493f1b-c60b-42c9-b5df-a389db276082][Is the UK heading for a recession?]]

+ Listening:  [[id:ce37f757-fb49-49db-8ca2-2f28a5581ce5][How to feed the world without destroying it]] 

+ [[id:657893cf-3ca0-409a-bc94-1329f997df7b][Appropedia]]
