:PROPERTIES:
:ID:       4185b0e1-9e39-4e98-9463-bf636f68668d
:mtime:    20221222200336 20221222154241 20221222125220
:ctime:    20221222125220
:END:
#+title: 2022-12-22

+ Read: [[id:2ba43d8e-2d67-4ab6-bcf7-7765f4f18ada][Open Climate Then and Now]].
  + A very nice overview of areas in which open practices (open science, hardware, software, knowledge, infrastructure) can be applied to [[id:6cc6be6a-0503-4455-b805-21dcc26ebb4f][climate action]].

+ Listened: [[id:b8fed578-038c-4928-9535-c22353ab5782][The Week in Green Software: Disintegration vs Integration]]

+ Read: [[id:8d332665-05d7-449e-95a4-0192d9bc86d4][The EU battery directive will make it easier to replace batteries]]
