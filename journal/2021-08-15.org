:PROPERTIES:
:ID:       059e5ad7-130e-4340-8aa3-1e124854b9be
:mtime:    20211127120756 20210815204208
:ctime:    20210815204208
:END:
#+title: 2021-08-15

+ Read: [[id:9fa4608d-afda-4e27-a023-4be399f7c006][The Hammer - Bill Gates wants your money]]
  + [[id:363fabf6-5ad2-4466-a9fe-bab61b45de12][Bill Gates]] and his climate fund - it's not a benevolent gift, it's an investment with an expected return.
  + There is a [[id:f934bbf5-c8d7-4453-bdf3-e42bbaaf4f6f][Bezos Earth Fund]]!?

+ Picking out some choice quotes from [[id:fb2d9823-15fb-4fe6-84a0-b65c75807c34][The Left Hand of Darkness]].

+ Read: [[https://www.zylstra.org/blog/2021/08/working-around-post-kinds-plugin-lock-in/][Working Around Post Kinds Plugin Lock-In – Interdependent Thoughts]]
  + I need to do something like this, too.

+ Perhaps obvious but a nice little lightbulb flash for me, was that [[id:e044a716-096c-438e-b69a-9a2f177d781b][systems thinking]] appears to generally be a recent incarnation / labelling of (a particular part of?) [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]].  Makes reading on cybernetics feel less like learning a dead language, and makes systems thinking feel a bit more weighty.

+ Whenever I get that stab of feeling like there's too many ideas to explore, too many books to read, too little time to do it in, I remind myself not to turn to productivity systems, efficiency improvements and the like, but to [[id:d255aca7-ed06-4ccc-a5f5-c63a2322370b][knowledge sharing]] and [[id:fe25641f-7676-44ab-be19-4ab0c7b0f62b][communities of practice]], and remind myself that you don't need to know it all, knowing things is not just an individual pursuit but a collective endeavour.

+ [[id:1e1abdef-d723-4024-8798-7fbe350533b6][Biology of love]]
  
+ Listened: [[id:35bd3466-38f8-4472-85ea-30f4042c9d45][Ray Ison and Ed Straw, "The Hidden Power of Systems Thinking: Governance in a Climate Emergency"]].
  + Politics is failing our home, the [[id:3613cf01-ae18-42c7-8353-f651d6f36ca9][biosphere]], massively.  The biosphere and climate change needs to be at the centre of [[id:ee227d4a-4ef2-436f-a68e-9877f741c0d9][governance]].  [[id:e044a716-096c-438e-b69a-9a2f177d781b][Systems thinking]] is a means to do this.
  + Enjoyed this. Sounds like a good book.  

+ I wonder if I should be using [[id:9027b4c7-4f3c-49a1-900a-90703c97d0a8][Org Cite]] in some way.
