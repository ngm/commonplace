:PROPERTIES:
:ID:       d6f7c019-f2e5-4d82-a218-ffb175fdad76
:END:
#+title: 2023-11-06

- I like the phrase [[id:311338a6-badb-4f8c-85aa-cf909a782f5a][To be ready for anything without planning for everything]]. (From [[id:092d15fc-87f7-4caa-aeb7-1bd8b2aec8c8][Anthony Hodgson, "Ready for Anything: Designing Resilience for a Transforming World"]]).

- [[id:84b90ab9-e7a3-4c93-bac7-6fd2833ebdca][Listened]]: [[id:f4bea82f-c14b-483c-8f7a-1db6a8d6125d][The Material Power That Rules Computation (ft. Cory Doctorow)]]
