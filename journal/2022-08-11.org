:PROPERTIES:
:ID:       3bf054ff-f882-4a86-9f3c-dafb62ab815e
:mtime:    20220811195711 20220811181405
:ctime:    20220811181405
:END:
#+title: 2022-08-11

+ [[id:8e7a6330-0600-418b-8c90-457dac0a42c7][Revolution]], [[id:3ddc4c92-47dc-4e82-ab5b-c1bc1fa36e34][Revolutionary transition]], [[id:9b82533b-416e-4e2a-bedf-0c67fdab160c][Climate Leninism]]
  + Read: [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]]

+ I'll retroactively class the previous action as two [[id:6c30be0c-e4e2-4583-8a3f-79271d23c0d5][pomodoro for the revolution]]
  + [X] Read: [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]]
  + [X] Read: [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]]
