:PROPERTIES:
:ID:       dc13536f-9c7a-43a3-82db-65fe7db6a5a2
:mtime:    20211127120958 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2021-05-23

+ Listened: [[id:d1c5746d-9785-4f80-a8e4-1211b9aa2b61][Life inside Gaza during 11 days of bombardment]]
  + https://www.theguardian.com/news/audio/2021/may/21/life-inside-gaza
  + Discusses the impact bombardment has had within [[id:de51089a-7bd7-47b1-8688-6ff6a658d201][Gaza]].
  + Also [[id:2c8dff77-23e0-4b08-96cb-e5799a4a9b63][Fatah]], [[id:ec35edad-242d-4bb1-85d8-dab7bf4d8aa5][Hamas]], [[id:18ef2766-678a-4680-bb22-30e447f0f4aa][Palestinian Liberation Organization]], and the [[id:2a214475-813a-403a-b7a4-8ae3960b9892][Palestinian Authority]].

+ Pulled my =slugify-title= function out into a mini-library so I can use it from multiple places, including dir-locals.el for commonplace.  Still feel a bit ropey on how all of this works.  Used =provide= and =require=.

+ [[id:993c2cb7-1378-4b71-a809-4621255d665f][Workflow for taking notes from podcasts]]
