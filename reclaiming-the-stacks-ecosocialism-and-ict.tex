% Created 2023-06-03 Sat 14:43
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Neil Mather}
\date{\today}
\title{Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism}
\hypersetup{
 pdfauthor={Neil Mather},
 pdftitle={Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2.50 (Org mode 9.5.4)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents

\begin{abstract}
This research investigates the role that information and communication technology workers can play in a transition to an environmentally stable and socially equitable society.  Against a context of ICT's role in society, and the dominance of the capitalist mode of production in ICT, I explore alternative approaches in ICT and how they can contribute to an ecosocialist political programme.  I provide evaluative criteria for how ICT workers can engage in and support initiatives to help transition to these alternatives.
\end{abstract}

\section{Introduction}
\label{sec:orgfe0681e}

In the final synthesis of the sixth assessment report from the Intergovernmental Panel on Climate Change, the world's scientists presented a stark final warning to humanity: we are destroying the planet, and we need 'deep, rapid, and sustained' reduction in emissions in order to avoid irreversible loss and damage to nature and people (\citeprocitem{12}{IPCC, 2023}). Along with the report, UN Secretary-General António Guterres stated the need for climate action 'by every country and every sector and on every timeframe'(\citeprocitem{32}{UN Press, 2023}). For those of us that work in the ever-growing sector of information and communication technology (ICT) we face the question: what action should we take?

The IPCC is unequivocal as to the cause of the crises of our times: human activity (\citeprocitem{12}{IPCC, 2023, p. 4}). We are altering the planet to such a degree that the change has been proposed as its own geological epoch: the Anthropocene (\citeprocitem{28}{Syvitski \textit{et al.}, 2020}). Others hone this further, asserting that it is not the presence of humans per se that is the cause, but the very particular configuration of our recent social history: capitalism and growth (\citeprocitem{9}{Hickel, 2020}). We are in the Capitalocene, and only an alternative to capitalism will get us out of it (\citeprocitem{22}{Moore, 2017}).

In all of this, one of the sectors with a major role to play is ICT. Computing and communication devices are integral to our society - from our phones to our computers to the servers and global infrastructure that networks them together. ICT has a significant environmental impact of its own through the lifecycle of its physical devices (\citeprocitem{6}{Freitag \textit{et al.}, 2021}), but also an enabling and structural effect on the impact of other sectors and society in general (\citeprocitem{10}{Hilty and Aebischer, 2015}).

With such impact, ICT has the potential to be a liberatory technology at scale. Yet, much of ICT has been captured for capitalist ends. The `Big Tech' corporations are some of the largest capitalist structures in the world. They represent the majority of companies to have ever attained over \$1 trillion in market capitalization (\citeprocitem{21}{Monica, 2021}).  The result is "an intensification of labour and environmental exploitation on a planetary scale" (\citeprocitem{17}{Likavčan and Scholz-Wäckerle, 2022})\footnote{Wark (\citeprocitem{33}{2019}) proposes that digitalization and ICT has in fact led to a stage of information-based power consolidation even beyond and worse than capitalism.}. The paradigm of digital capitalism needs to be undone if we are to stand a chance of combatting climate change. What can challenge its dominance?

This research investigates what an ecosocialist programme for ICT could look like, and how ICT workers can contribute to this programme.

\section{Sustainable development?}
\label{sec:orga8ded5b}

Sustainable development and the sustainable development goals are well-known organising principles that aim to put people and planet first (\citeprocitem{31}{United Nations, 2015}). Sustainable development is an approach to economic activity that explicitly recognises the interdependence of environmental and socio-economic issues, and the need to move beyond an economy that grows without considering the limits of the planet.

Hopwood, Mellor and OʼBrien (\citeprocitem{11}{2005}) acknowledge the positive direction of sustainable development's core messages, but criticise the lack of programmatic definition of how to achieve its goals. They map out various existing approaches to organising society and the economy, assessing their strength on the dual axes of environmental concerns and socio-economic equity. Depending on the strength for each of these concerns, the approaches are grouped into three areas of sustainable development - Status Quo, Reform and Transformation.  Figure \ref{fig:sdmap} shows this mapping.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{Ecosocialism_and_ICT/2023-05-08_20-32-56_screenshot.png}
\caption{\label{fig:sdmap}Mapping of sustainable development approaches (from \citeprocitem{11}{Hopwood, Mellor and O’Brien, 2005}).}
\end{figure}

\emph{Status Quo} approaches ignore the urgency of the crises, asserting that a strategy of `business as usual' is sufficient, and that a continued pursuit of economic growth and technological development through markets will eventually resolve them. \emph{Reform} approaches believe that change is needed, but that such change can happen slowly over time, through gentle reforms to markets and government policies. \emph{Transformation} approaches are more radical, asserting that the solution lies only within deep societal transformation away from growth and capitalism.

Hopwood, Mellor and OʼBrien (\citeprocitem{11}{2005}) argue that status quo approaches have caused the current crises, and that reform alone will not be effective in the short window of time that we have. They argue that approaches based on transformation are essential, backed by pragmatic short-term reforms to facilitate transition.\footnote{In the language of André Gorz, "non-reformist reforms." (\citeprocitem{5}{Engler and Engler, 2021})}  There is a substantial body of work making the detailed case for transformation (e.g. \citeprocitem{14}{Klein, 2014}; \citeprocitem{9}{Hickel, 2020}), with the global consensus of the IPCC being the latest, most mainstream addition to this.

\subsection{Transformative alternatives}
\label{sec:orgb47fd8c}

What transformative approaches to sustainable development do we have? Hopwood, Mellor and OʼBrien (\citeprocitem{11}{2005}) situate a number of approaches within the transformation group: anti-capitalist and environmental justice movements; social ecology; ecofeminism; ecosocialism; and indigenous movements (top right of figure \ref{fig:sdmap}). All contain important elements of transformative practice, and between them “there is a constant interchange of ideas and cross-fertilization” (\citeprocitem{11}{Hopwood, Mellor and O’Brien, 2005, p. 10}).  Albert (\citeprocitem{1}{2022}) describes ecosocialism as the foremost present post-capitalist alternative, situating ideas such as degrowth and ecofeminism within it.  As such, in this research I will focus on ecosocialism, while recognising that similar transformative tendencies exist in and around it.

Ecosocialism is a political programme that melds socialist and environmental politics, with a strong anti-colonial and anti-capitalist sentiment.  Its tenets move beyond private profit to build forces and relations of production that are both ecologically-minded and socially-useful (\citeprocitem{3}{Brownhill, 2022}). 

\section{ICT, capitalism, and sustainability}
\label{sec:org8b19f83}

ICTs throughout history have had a significant impact on society, affecting politics, economy, labour, production, consumption and resource use (\citeprocitem{4}{Creutzig \textit{et al.}, 2022}).  This impact continues to this day, with the present incarnation of digital ICT ("digitalization") having a huge impact on the world (\citeprocitem{30}{The World in 2050, 2019}).

ICT is an industry that spans many layers of society. In a traditional, technical sense, it is a combination of software, hardware and communications infrastructure stacked upon one another. Broader definitions of ICT range philosophically further, viewing it as an ecology of design, materials, humans, nonhumans, values, politics, and more (e.g. Bratton (\citeprocitem{2}{2015})). ICT facilitates the activities of much of modern society, even when viewed in a purely technical sense.  Viewed in the wider sense, ICT and digitalization is all-encompassing of everyday life.

\subsection{Capitalist ICT}
\label{sec:org8976e89}

Like any technology, embedded in society as they are, the structures of ICT can never be value-neutral (\citeprocitem{8}{Hare, 2022}).  They are imbued with a particular political economy by a particular social context.

Over the years, digital ICT has moved from a starting position of public good to one of private ownership (\citeprocitem{29}{Tarnoff, 2022}).  This enclosure has continued to such an extent that the consolidation of the ICT stack by Big Tech corporations represents the current apex of capitalism (\citeprocitem{33}{Wark, 2019}; \citeprocitem{17}{Likavčan and Scholz-Wäckerle, 2022}).  ICT companies represent the majority of companies to have ever attained over \$1 trillion in market capitalization (\citeprocitem{21}{Monica, 2021}).  Google, Amazon, Facebook, Apple and Microsoft control vast tracts of society through software, hardware, networks, supply chains and manufacturing. They "span much of the terrain of ordinary experience" (\citeprocitem{7}{Greenfield, 2017, p. 275}), "mediate and monetize everyday life to the maximum possible extent" (\citeprocitem{7}{Greenfield, 2017, p. 238}).  They have resulted in “an intensification of labour and environmental exploitation on a planetary scale” (\citeprocitem{17}{Likavčan and Scholz-Wäckerle, 2022, p. 12}).  Capitalistic forms of ICT currently dominate the world.

If we accept that the stacks of Big Tech dominate capitalism, and that capitalism is negatively impacting nature and society, then as concerned ICT workers we should try to reclaim the stacks - engaging with and promoting alternatives to capitalist ICT that respect society and the environment.

\subsection{ICT for sustainability}
\label{sec:org642f294}

The field of ICT for sustainability (ICT4S) explores the principles of sustainable development as applied to ICT (\citeprocitem{10}{Hilty and Aebischer, 2015}). Yet, as with sustainable development as a whole, there are various approaches to ICT4S, and the majority either sustain the status quo or offer only mild reforms (\citeprocitem{25}{Santarius and Wagner, 2023}). They tend to focus on a notion of digital 'efficiency' (focusing on the operating efficiency of ICT, while still allowing its growth overall to continue) and ignore the concept of digital 'sufficiency' (the reduction of demand), and to ignore higher-level questions of structural transformation (\citeprocitem{25}{Santarius and Wagner, 2023, p. 2}).  Much more transformative strategies are needed to make the changes that we need in the timeframe that we have (\citeprocitem{11}{Hopwood, Mellor and O’Brien, 2005}).  What could an ecosocialist ICT look like?

\section{An overview of ecosocialist ICT}
\label{sec:orga8c4484}

A number of contemporary works have presented programmes for an ecological or socialist ICT (\citeprocitem{13}{Kleiner, 2010}; \citeprocitem{20}{Medina, 2014}; \citeprocitem{16}{Likavčan and Scholz-Wäckerle, 2018}; \citeprocitem{18}{Lovink, 2020}; \citeprocitem{26}{Schneider, 2022}; \citeprocitem{4}{Creutzig \textit{et al.}, 2022}; \citeprocitem{23}{Muldoon, 2022}; \citeprocitem{29}{Tarnoff, 2022}; \citeprocitem{15}{Kwet, 2022}; \citeprocitem{27}{Smith, 2022})).  In this section I review these through the lens of ecosocialism, looking for common principles, example initiatives and evaluative criteria that are contained within them.

An initiative is an existing, real-word example of an ecosocialist ICT that an ICT worker could support today. By outlining example initiatives, and identifying different criteria with which they can be evaluated, as ICT workers we can identify initiatives which are the most opportune for us to contribute to.

\subsection{Criteria for evaluating initiatives}
\label{sec:orgd2a714a}

We can evaluate initiatives for an ecosocialist ICT against a number of different criteria.  Knowing: the broad \emph{domain of ecosocialism}; the \emph{layer of the stack}; the \emph{type of action}; and the level of \emph{radicalism} involved; can help identify which initiatives fit best with a particular ICT worker's skillsets and capacity.

\subsubsection{Domain of ecosocialism}
\label{sec:org02dead9}

Creutzig \textit{et al.} (\citeprocitem{4}{2022}) outline three domains: planetary stability; social equity; and agency. \emph{Planetary stability} refers to issues such as energy, emissions, material use, land demand, and in general, the recognition of planetary boundaries and the need to stay within them. \emph{Social equity} refers to the fair distribution of benefits (and disbenefits) associated with digital technologies. Similarly, Smith (\citeprocitem{27}{2022}) references the concepts of social foundations and planetary boundaries in an ICT-focused interpretation of the well-known framework of Doughnut Economics (\citeprocitem{24}{Raworth, 2017}).  \emph{Agency} refers to ownership and control and the ability to participate. The stewards of a technology should be the people who use and are affected by the technology. Schneider (\citeprocitem{26}{2022}) identifies sovereignty and democracy as key aspects of agency.

\subsubsection{Layer of the stack}
\label{sec:org0bf8d2c}

Difference initiatives relate to different layers of the technological stack.
For example, 'down the stack', at the layer of the network, Tarnoff (\citeprocitem{29}{2022}) advocates for publicly and cooperatively owned networks such as community broadband and community mesh networks.  'Up the stack', at the layer of software and platforms, Tarnoff advocates for the protocolization of social media, highlighting alternative social media initiatives such as the Fediverse\footnote{\url{https://fediverse.info/}} and for platform cooperativism - the worker control and ownership of platforms. Creutzig \textit{et al.} (\citeprocitem{4}{2022}) discuss initiatives at the hardware layer, such as legislation against planned obsolescence.

\subsubsection{Type of action / point of leverage}
\label{sec:org1a01b41}

Initiatives fall into different types of action and the leverage points that they work with. Muldoon (\citeprocitem{23}{2022}) outlines the notions of \emph{resist}, \emph{regulate} and \emph{recode}.   Resistance is acts that directly confront the status quo, such as unionisation, strikes, protest and direct action.  Regulation is the advocacy for laws introduced by a state actor.  Recoding is the active building of alternatives, referencing the notion of Erik Olin Wright's concept of 'real utopias' (\citeprocitem{34}{Wright, 2010}).  Similarly, Kwet (\citeprocitem{15}{2022}) discusses a mixture of alternative-building, use of regulation, and direct action. Creutzig \textit{et al.} (\citeprocitem{4}{2022}) observe that digitalisation can be well applied to the key leverage points in a system (rules and feedback, structures, goal setting and mindset shifts) as outlined by Meadows and Wright (\citeprocitem{19}{2008}).

\subsubsection{Level of radicalism}
\label{sec:orgb655334}

The various programmes discuss initiatives with differing degrees of radicalism (the desire to break with the continuity of existing institutions). Some ICT workers may be more comfortable participating in initiatives that promote (non-reformist) reforms to existing structures, whereas others may be willing/able to participate in more radical action.

As seen, Hopwood, Mellor and OʼBrien (\citeprocitem{11}{2005}) give us a spectrum of status quo, reform and transformation.  Schneider (\citeprocitem{26}{2022}) describes a criteria for governable stacks of "insurgency" - a direct defiance against attempts at rule. Kwet (\citeprocitem{15}{2022}) promotes an explicitly anti-colonialist digital ecosocialism, and calls for direct actions such as boycott, divestment and sanctions. Likavčan and Scholz-Wäckerle (\citeprocitem{16}{2018}) call for technology appropriation.

\subsection{General principles}
\label{sec:org66870d0}

Across the programmes, there are a number of general principles repeated.  These are \emph{open knowledge} (such as libre software and creative commons); \emph{cooperativism} (such as platform coops and tech coops); \emph{commoning} (such as data commons and knowledge commons); \emph{degrowth} (such as concepts of digital sufficiency); \emph{socially useful / ecologically safe production}; \emph{adversarial interoperability}; \emph{municipal support} (such as community wealth building and public-commons partnerships); \emph{innovation from below} (such as design justice); and \emph{socialisation / deprivatisation} (of physical and digital ICT infrastructure). These general principles can be used to identify an ICT initiative as ecosocialist.

\section{What can ICT workers do?}
\label{sec:org069aa53}
As outlined by Albert (\citeprocitem{1}{2022}), ecosocialists must go beyond simply defining a wish list of things that we want to achieve, and must critically discuss how we might go about achieving them.  In our domain, 
it is important to focus on steps of transition from digital capitalism to digital ecosocialism.

For the ICT worker, one approach to this is participating in praxis that supports initiatives of ecosocialist ICT.  Based on the criteria above, an ICT worker can find initiatives that lie close to intersection of their expertise and interests. The classic socialist slogan is to "educate, agitate and organise."  For any given initiative, an ICT worker can support it by learning about it; sharing it and teaching it to others; and directly contributing skills or financial support to the initiative.

\subsection{Example initiatives}
\label{sec:org93f6761}

For example, an ICT worker might identify the problem of early obsolescence and short upgrade cycles as a problem they wish to challenge, where devices such as laptops, tablets and smartphones are consumed at an unsustainable rate, due to aggressive policies of manufacturers in pursuit of growth, leading to e-waste, carbon emissions and labour exploitation.

This problem falls into the domain of \emph{Planetary stability}, at the \emph{Hardware} layer.  Various initiatives might contribute to an ecosocialist response to this problem.  Campaigning for right to repair policy\footnote{\url{https://repair.eu}} aims for non-reformist \emph{Reform} through acts of \emph{Regulation}.  Initiatives such as Fairphone\footnote{\url{https://www.fairphone.com/en/}} or Framework\footnote{\url{https://frame.work/gb/en}} are attempts to \emph{Recode} the way in which devices are produced, with fairer supply chains and repairable designs.  The reverse engineering and distribution of schematics of devices to enable repair would be a more \emph{Transformative} act of \emph{Resistance}.

Another ICT worker might be concerned at the practices of social media platforms, driving addiction through the attention economy in the pursuit of advertising revenue.  Alternative social media initiatives such as the Fediverse aim to \emph{Recode} social media giving users more agency.  Initiatives such as Nitter\footnote{\url{https://nitter.net/about}} \emph{Resist} the hegemony of the big tech social media platforms through adversarial interoperability.  Campaigns to enforce network interoperability are a radical form of \emph{Regulation}, whereas anti-trust regulation is reformist (\citeprocitem{15}{Kwet, 2022}).

Dependent on their skillsets, the ICT workers can choose to support these initiatives as best fits them.

\section{Conclusion}
\label{sec:org9fd5410}

In order to prevent social and environmental disaster, all sectors of society need to engage in a rapid change towards sustainability.  ICT, with both a direct and indirect effect on emissions and societal activity, is integral in this.  Ecosocialist ICT is an alternative to capitalist ICT, and as concerned ICT workers we can contribute to the necessary transition by supporting ecosocialist ICT initiatives.

A programme of ecosocialist ICT combines resistance and regulation with the building of alternatives. Many building blocks and blueprints are here already: initiatives such as libre software and creative commons; platform coops and tech coops; federated and democratically governed social media platforms; data commons and data trusts; strike actions, worker empowerment and campaigns against exploitative practices; the deconstruction of intellectual property laws; regulation against monopolistic practices and early obsolescence; and many more.

We as ICT workers can contribute to a transition by building and supporting these initiatives.  In this research I have reviewed existing programmes and presented a set of criteria for ICT workers to pick out initiatives that they can best support.  Big Tech will not be challenged by maintaining the status quo; as concerned ICT workers we need to urgently agitate, organise and educate to foster transformative initiatives for an ecosocialist ICT.

\subsection{Future work}
\label{sec:orgc1d2ac0}

This research has looked at initiatives in relative isolation from one another.  It is important to also recognise the interconnectedness of these initiatives and to investigate how they interact in a broader strategy.  Future work could engage with systems thinking to do this.

A struggle for ecosocialist ICT is only one facet of an ecosocialist strategy.  Future work should investigate the connection points of ecosocialist ICT to the wider strategy of ecosocialist transition - for example, how it could support and be part of an ecosocialist Green New Deal.

\section{References}
\label{sec:org413f2c4}

\hypertarget{citeproc_bib_item_1}{Albert, M.J. (2022) “Ecosocialism for Realists: Transitions, Trade-Offs, and Authoritarian Dangers,” \textit{Capitalism nature socialism}, pp. 1–20. Available at: \url{https://doi.org/10.1080/10455752.2022.2106578}.}

\hypertarget{citeproc_bib_item_2}{Bratton, B.H. (2015) \textit{The stack: On software and sovereignty}. Cambridge, Massachusetts: MIT Press (Software studies).}

\hypertarget{citeproc_bib_item_3}{Brownhill, L. (ed.) (2022) \textit{The Routledge handbook on ecosocialism}. London ; New York: Routledge, Taylor \& Francis Group.}

\hypertarget{citeproc_bib_item_4}{Creutzig, F. \textit{et al.} (2022) “Digitalization and the Anthropocene,” \textit{Annual review of environment and resources}, 47(1), pp. 479–509. Available at: \url{https://doi.org/10.1146/annurev-environ-120920-100056}.}

\hypertarget{citeproc_bib_item_5}{Engler, M. and Engler, P. (2021) “André Gorz’s Non-Reformist Reforms Show How We Can Transform the World Today.” https://jacobin.com/2021/07/andre-gorz-non-reformist-reforms-revolution-political-theory.}

\hypertarget{citeproc_bib_item_6}{Freitag, C. \textit{et al.} (2021) “The real climate and transformative impact of ICT: A critique of estimates, trends, and regulations,” \textit{Patterns}, 2(9). Available at: \url{https://doi.org/10.1016/j.patter.2021.100340}.}

\hypertarget{citeproc_bib_item_7}{Greenfield, A. (2017) \textit{Radical technologies: The design of everyday life}. London ; New York: Verso.}

\hypertarget{citeproc_bib_item_8}{Hare, S. (2022) \textit{Technology is not neutral: A short guide to technology ethics}. London: London Publishing Partnership (Perspectives).}

\hypertarget{citeproc_bib_item_9}{Hickel, J. (2020) \textit{Less is more: How degrowth will save the world}. London: William Heinemann.}

\hypertarget{citeproc_bib_item_10}{Hilty, L.M. and Aebischer, B. (2015) “ICT for Sustainability: An Emerging Research Field,” in L.M. Hilty and B. Aebischer (eds.) \textit{ICT Innovations for Sustainability}. Cham: Springer International Publishing, pp. 3–36. Available at: \url{https://doi.org/10.1007/978-3-319-09228-7-1}.}

\hypertarget{citeproc_bib_item_11}{Hopwood, B., Mellor, M. and O’Brien, G. (2005) “Sustainable development: Mapping different approaches,” \textit{Sustainable development}, 13(1), pp. 38–52. Available at: \url{https://doi.org/10.1002/sd.244}.}

\hypertarget{citeproc_bib_item_12}{IPCC (2023) “AR6 Synthesis Report: Summary for Policymakers Headline Statements.” https://www.ipcc.ch/report/ar6/syr/resources/spm-headline-statements.}

\hypertarget{citeproc_bib_item_13}{Kleiner, D. (2010) “The Telekommunist Manifesto.”}

\hypertarget{citeproc_bib_item_14}{Klein, N. (2014) \textit{This changes everything: Capitalism vs. the climate}. First Simon \& Schuster hardcover edition. New York: Simon \& Schuster.}

\hypertarget{citeproc_bib_item_15}{Kwet, M. (2022) “Digital Ecosocialism: Breaking the power of Big Tech.” https://longreads.tni.org/digital-ecosocialism.}

\hypertarget{citeproc_bib_item_16}{Likavčan, L. and Scholz-Wäckerle, M. (2018) “Technology appropriation in a de-growing economy,” \textit{Journal of cleaner production}, 197, pp. 1666–1675. Available at: \url{https://doi.org/10.1016/j.jclepro.2016.12.134}.}

\hypertarget{citeproc_bib_item_17}{Likavčan, L. and Scholz-Wäckerle, M. (2022) “The Stack as an Integrative Model of Global Capitalism,” \textit{Triplec: Communication, capitalism \& critique. open access journal for a global sustainable information society}, 20(2), pp. 147–162. Available at: \url{https://doi.org/10.31269/triplec.v20i2.1343}.}

\hypertarget{citeproc_bib_item_18}{Lovink, G. (2020) “Principles of Stacktivism,” \textit{Triplec: Communication, capitalism \& critique. open access journal for a global sustainable information society}, pp. 716–724. Available at: \url{https://doi.org/10.31269/triplec.v18i2.1231}.}

\hypertarget{citeproc_bib_item_19}{Meadows, D.H. and Wright, D. (2008) \textit{Thinking in systems: A primer}. White River Junction, Vt: Chelsea Green Pub.}

\hypertarget{citeproc_bib_item_20}{Medina, E. (2014) \textit{Cybernetic revolutionaries: Technology and politics in Allende’s Chile}. First MIT Press paperback edition. Cambridge, Mass: MIT Press.}

\hypertarget{citeproc_bib_item_21}{Monica, P.R.L. (2021) “The race to \\$3 trillion: Big Tech keeps getting bigger | CNN Business,” \textit{Cnn} [Preprint]. https://www.cnn.com/2021/11/07/investing/stocks-week-ahead/index.html.}

\hypertarget{citeproc_bib_item_22}{Moore, J.W. (2017) “The Capitalocene, Part I: On the nature and origins of our ecological crisis,” \textit{The journal of peasant studies}, 44(3), pp. 594–630. Available at: \url{https://doi.org/10.1080/03066150.2016.1235036}.}

\hypertarget{citeproc_bib_item_23}{Muldoon, J. (2022) \textit{Platform socialism: How to reclaim our digital future from big tech}. London: Pluto Press.}

\hypertarget{citeproc_bib_item_24}{Raworth, K. (2017) \textit{Doughnut economics: Seven ways to think like a 21st-century economist}. London: Random House Business Books.}

\hypertarget{citeproc_bib_item_25}{Santarius, T. and Wagner, J. (2023) “Digitalization and sustainability: A systematic literature analysis of ICT for Sustainability research,” \textit{Gaia - ecological perspectives for science and society}, 32(1), pp. 21–32. Available at: \url{https://doi.org/10.14512/gaia.32.S1.5}.}

\hypertarget{citeproc_bib_item_26}{Schneider, N. (2022) “Governable Stacks against Digital Colonialism,” \textit{Triplec: Communication, capitalism \& critique. open access journal for a global sustainable information society}, 20(1), pp. 19–36. Available at: \url{https://doi.org/10.31269/triplec.v20i1.1281}.}

\hypertarget{citeproc_bib_item_27}{Smith, H. (2022) “What is the digital tech doughnut?,” \textit{Doing the doughnut tech} [Preprint].}

\hypertarget{citeproc_bib_item_28}{Syvitski, J. \textit{et al.} (2020) “Extraordinary human energy consumption and resultant geological impacts beginning around 1950 CE initiated the proposed Anthropocene Epoch,” \textit{Communications earth \& environment}, 1(1), pp. 1–13. Available at: \url{https://doi.org/10.1038/s43247-020-00029-y}.}

\hypertarget{citeproc_bib_item_29}{Tarnoff, B. (2022) \textit{Internet for the people: The fight for our digital future}. London ; New York: Verso.}

\hypertarget{citeproc_bib_item_30}{The World in 2050 (2019) “The Digital Revolution and Sustainable Development: Opportunities and Challenges. Report prepared by The World in 2050 initiative.” Available at: \url{https://doi.org/10.22022/TNT/05-2019.15913}.}

\hypertarget{citeproc_bib_item_31}{United Nations (2015) “Transforming our world: The 2030 Agenda for Sustainable Development.”}

\hypertarget{citeproc_bib_item_32}{UN Press (2023) “Secretary-General Calls on States to Tackle Climate Change `Time Bomb’ through New Solidarity Pact, Acceleration Agenda, at Launch of Intergovernmental Panel Report | UN Press.” https://press.un.org/en/2023/sgsm21730.doc.htm.}

\hypertarget{citeproc_bib_item_33}{Wark, M. (2019) \textit{Capital is dead}. London ; New York: Verso.}

\hypertarget{citeproc_bib_item_34}{Wright, E.O. (2010) \textit{Envisioning real utopias}. London ; New York: Verso.}\bigskip
\end{document}