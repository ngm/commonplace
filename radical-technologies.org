:PROPERTIES:
:ID:       7b7e27e7-5902-4ad5-a426-d58449aa8abf
:mtime:    20221217112227 20221211225956 20221211182111 20221203171610 20221126111733 20221120163603 20221118213742 20211127120859 20210724222235
:ctime:    20200412133357
:ROAM_REFS: [cite:@greenfield2017]
:END:
#+TITLE: Radical Technologies

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Author :: [[id:8883b8e9-a359-43e0-bde8-737473245eb2][Adam Greenfield]]
+ URL :: https://www.versobooks.com/books/2742-radical-technologies
+ Publisher :: [[id:80c65a84-467c-4f11-9e4d-de966f99a93b][Verso]]

Really a fantastic book.  An absolute banger.  Gorgeously written, erudite and thoughtful on modern technologies.

Thorough, fair critique of the technologies you see bandied around as part of the ‘Fourth Industrial Revolution’ – smartphones, IoT, AR/VR, blockchain, digital fabrication, automation, ML/AI.

Looks for the positives but mostly damning - the power structures behind the technologies very rarely allow them to be liberatory. All still right on point 5 years since publication.

The last two chapters on the Stacks, possible futures and tactics are gold.
https://www.versobooks.com/books/2742-radical-technologies

The technologies covered:

- [[id:a633b350-228d-43f5-b0db-0df337879196][smartphones]]
- [[id:611df3a2-9afe-4652-b171-8869a9b9001d][Internet of Things]]
- [[id:4b6a92d9-a29d-436c-b958-695186b31f03][augmented reality]]
- [[id:051689e2-ef7c-453d-97db-bf8269377c50][digital fabrication]]
- [[id:5997aec1-99d7-4793-b1e5-2cdf3229beb1][cryptocurrency]]
- [[id:87e1d161-34f4-42f6-ba59-bc4aaab94e8e][Blockchain]]
- [[id:df0b8083-6335-4038-8bed-d12cec8acde8][automation]]
- [[id:1dc0ad51-bc4e-411d-9a3f-83788c847faa][machine learning]]
- [[id:d4cf17cd-08ef-4676-8747-75d52f2e0edf][artificial intelligence]]

* Digital fabrication

Adam is very positive about the potential of decentralised means of production possible through digital fabrication.  However, also very aware of the barriers that need overcoming before we get there.

* Cryptocurrency and blockchain

Re-reading in 2022, still gives the best intro to cryptocurrency for me - where it came from, why it was created, the political / economic motivations behind the creators, what are its problems, etc.  And with blockchain also - what the promises made of second-generation blockchains for governance and democracy will likely not live up to the hype.

* Automation

* Raw notes

#+begin_quote
Insight into their functioning is distributed unequally across society, as is the power to intervene meaningfully in their design.
#+end_quote

Technology needs to be more democratic.  Governable.

#+begin_quote
And we’ll pay particular attention to the ways in which these allegedly disruptive technologies leave existing modes of domination mostly intact, asking if they can ever truly be turned to liberatory ends.
#+end_quote

Page 48 [2022-11-15 05:46:08]:
Everyone with a smartphone has, by definition, a free, continuously zoomable, self-updating, high-resolution map of every part of the populated surface of the Earth that goes with them wherever they go, and this is in itself an epochal development.

Page 48 [2022-11-15 05:46:23]:
Most profoundly of all—and it’s worth pausing to savor this—they are the first maps in human history that follow our movements and tell us where we are on them in real time.

Page 48 [2022-11-15 05:47:43]:
It’s dizzying to contemplate everything involved in that achievement. It fuses globally dispersed infrastructures of vertiginous scale and expense—
● mapping and geolocation is a megastack.vi feel like most stacks bottom out with vast state or corporate ownership.

Page 50 [2022-11-15 05:51:20]:
the networked condition

Page 50 [2022-11-15 05:52:02]:
we become reliant on access to the network to accomplish ordinary goals.

Page 51 [2022-11-15 05:52:37]:
the performance of everyday life as mediated by the smartphone depends on a vast and elaborate infrastructure that is ordinarily invisible to us.
● agency is being able to function while disconnected and offline. or at least p2p. reclaiming the stack.

Page 51 [2022-11-15 06:03:30]:
the moment-to-moment flow of our experience rests vitally on the smooth interfunctioning of all the many parts of this infrastructure—an extraordinarily heterogeneous and unstable meshwork, in which cellular base stations, undersea cables, and microwave relays are all invoked in what seems like the simplest and most straightforward tasks we perform with the

Page 51 [2022-11-15 06:03:50]:
The very first lesson of mapping on the smartphone, then, is that the handset is primarily a tangible way of engaging something much subtler and harder to discern, on which we have suddenly become reliant and over which we have virtually no meaningful control.

Page 52 [2022-11-15 06:04:59]:
many users will continue to experience the technics of everyday life as bewildering, overwhelming, even hostile.

Page 53 [2022-11-15 06:05:35]:
our sense of the world is subtly conditioned by information that is presented to us for interested reasons, and yet does not disclose that interest.

Page 54 [2022-11-15 06:07:10]:
the seamless, all-but-unremarked-upon splicing of revenue-generating processes into ordinary behavior,

Page 54 [2022-11-15 06:08:07]:
For in the world as we’ve made it, those who enjoy access to networked services are more capable than those without.

Page 55 [2022-11-15 06:09:11]:
impossible to use the device as intended without, in turn, surrendering data to it and the network beyond

Page 58 [2022-11-15 06:11:38]:
not, we are straightforwardly trading our privacy for convenience

Page 59 [2022-11-15 06:13:10]:
turn, that data will be captured and leveraged by any number of parties, including handset and operating system vendors, app developers, cellular service providers, and still others; those parties will be acting in their interests, which may only occasionally intersect our own; and it will be very, very difficult for us to exert any control over any of this
● libre alternatives less likely to do so.

Page 60 [2022-11-15 06:15:50]:
The individual networked in this way is no longer the autonomous subject enshrined in liberal theory, not precisely. Our very selfhood is smeared out across a global mesh of nodes and links; all the aspects of our personality we think of as constituting who we are—our tastes, preferences, capabilities, desires—we owe to the fact of our connection with that mesh, and the selves and distant resources to which it binds us.

Page 61 [2022-11-15 06:18:40]:
This is one of the costs of having a network organ, and the full-spectrum awareness it underwrites: a low-grade, persistent sense of the world and its suffering that we carry around at all times, that reaches us via texts and emails and Safety Check notices.

Page 62 [2022-11-15 06:19:22]:
network connectivity now underwrites the achievement of virtually every other need on the Maslovian pyramid, to the extent that refugees recently arriving from warzones have been known to ask for a smartphone before anything else, food and shelter not excluded.

Page 63 [2022-11-15 06:23:35]:
It remains to be seen what kind of institutions and power relations we will devise as selves fully conscious of our interconnection with one another, though the horizontal turn in recent politics might furnish us with a clue.

Page 65 [2022-11-15 06:30:19]:
The internetof things
A planetary mesh ofperception and response

Page 66 [2022-11-15 06:32:23]:
the colonization of everyday life by information processing.

Page 67 [2022-11-15 06:32:55]:
the ambition to raise awareness of some everyday circumstance to the network for analysis and response

Page 68 [2022-11-15 06:34:28]:
the primary scales at which it appears to us: that of the body, that of the room, and that of public space in general

Page 68 [2022-11-16 00:21:37]:
The quest to instrument the body, monitor its behavior and derive actionable insight from these soundings is known as the “quantified self”; the drive to render interior, domestic spaces visible to the network “the smart home”; and when this effort is extended to municipal scale, it is known as “the smart city.”

Page 72 [2022-11-16 00:40:56]:
Soylent4, the flavorless nutrient slurry

Page 82 [2022-11-16 06:21:26]:
I don’t think it’s unfair to say that at this moment in history, internet-of-things propositions are generally imagined, designed and architected by a group of people who have completely assimilated services like Uber, Airbnb and Venmo into their daily lives, at a time when Pew Research Center figures suggest that a very significant percentage of the population has never used (or even heard of) them.12 And all of their valuations get folded into the things they design. These propositions are normal to them, and so become normalized for everyone else as well.
● ict workers should ensure denocracy in democratic choices

Page 94 [2022-11-17 04:59:32]:
The temperature in Amazon’s fulfillment center, passing-out hot in the summer months because leaving the doors open to let in a breeze would also admit some possibility of theft. The prevalence of miscarriages and cancers among workers in the plant where the gallium arsenide in the LED was made.
● intnl solidaity with workers is required

Page 96 [2022-11-17 05:03:14]:
“smart city.” This is a place where the instrumentation of the urban fabric, and of all the people moving through the city, is driven by the desire to achieve a more efficient use of space, energy and other resources.

Page 104 [2022-11-17 05:14:34]:
We might think of it as an unreconstructed logical positivism, which among other things holds that the world is in principle perfectly knowable, its contents enumerable and their relations capable of being meaningfully encoded in the state of a technical system, without bias or distortion.

Page 110 [2022-11-18 00:12:26]:
In urban planning, the idea that certain kinds of challenges are susceptible to algorithmic resolution has a long pedigree.
● interedting to compsre to docialidt democratic plsnning

Page 111 [2022-11-18 00:19:48]:
Quite simply, we need to understand that the authorship of an algorithm intended to guide the distribution of civic resources is itself an inherently political act.

Page 112 [2022-11-18 00:22:02]:
“systems analysis” or “operations research,” was first applied to New York in a series of studies conducted between 1973 and 1975, in which RAND used FDNY incident response-time data to determine the optimal distribution of fire stations.

Page 117 [2022-11-18 03:21:30]:
The internet of things in all of its manifestations so often seems like an attempt to paper over the voids between us, or slap a quick technical patch on all the places where capital has left us unable to care for one another.

Page 123 [2022-11-18 03:36:02]:
Augmentedreality
An interactive overlayon the world

Page 128 [2022-11-18 03:37:05]:
AR has its conceptual roots in informational displays developed for military pilots in the early 1960s, when the performance of enemy fighter aircraft first began to overwhelm a human pilot’s ability to react to the environment in a sufficiently timely manner. In the fraught regime of jet-age dogfighting, even a momentary dip of the eyes to a dashboard-mounted instrument cluster could mean disaster. The solution was to project information about altitude, airspeed and the status of weapons and other critical aircraft systems onto a transparent pane aligned with the field of vision: a “head-up display.”

* Critical review

** what's the main argument?

That we need to look at technologies with a very critical eye.  That they are never neutral, and particularly in a neoliberal hegemony they will rarely end up being used for liberatory ends.

** what are the author's underlying assumption and implicit arguments?

There's an underlying assumption that capitalism is bad, and that technology that supports it is to be avoided.

** What theoretical perspectives do they use?

"What system or school of ideas based on critical analysis of previous theories and research?"

There were a good few references in the book.  I would say that politically Adam draws on anarchism / municipalism politically; comes from a critical STS viewpoint with regards to views of technology.  
