:PROPERTIES:
:ID:       989ec1af-795d-4955-98bf-fb68120e2330
:mtime:    20211127120916 20210724222235
:ctime:    20210724222235
:END:
#+title: Long-term computing
#+CREATED: [2020-12-19 Sat 11:18]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

:: tags: [[id:89446ec8-bf0d-4df8-9f17-8fc4638de8dd][Sustainable tech]].

Steve Lord talks about the [[id:666a5daa-3d3f-411a-886c-52ea903b8130][heirloom computer]].
