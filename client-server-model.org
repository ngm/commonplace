:PROPERTIES:
:ID:       ae9867f1-076c-4ee6-9700-ed14897081f3
:mtime:    20211127120929 20210724222235
:ctime:    20210724222235
:END:
#+title: client-server model
#+CREATED: [2021-04-03 Sat 14:49]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
It may be called the “client-server” model, but most of us clients aren’t really looking to connect to a “server.” What we want is to connect to a website. More accurately, we want to connect to friends or content. This desire for connection however is currently owned by platform giants rolling out huge server farms that harvest the world for data, while we are held captive, force-fed through feeds.

-- [[id:d5d03a8b-5788-494a-8781-473b10babeaf][Seeding the Wild]]
#+end_quote

