:PROPERTIES:
:ID:       af5b6882-d778-4aeb-ae84-021ffa4bb13d
:mtime:    20211127120930 20210916211038
:ctime:    20210916211038
:END:
#+title: LibreOffice
#+CREATED: [2021-04-25 Sun 11:49]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ Timeline: https://www.libreoffice.org/about-us/libreoffice-timeline/

+ [[https://patternsofcommoning.org/converting-proprietary-software-into-a-commons-the-libreoffice-story/][Converting Proprietary Software into a Commons: The LibreOffice Story]] 
