:PROPERTIES:
:ID:       bbf3ec5b-4604-46ae-bad1-45598576e035
:mtime:    20230520103739
:ctime:    20230520103739
:END:
#+TITLE: Left melancholia
#+CREATED: [2023-05-20 Sat]
#+LAST_MODIFIED: [2023-05-20 Sat 10:37]

#+begin_quote
left-melancholia, that eternal sadness about eternal capitalism

-- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
#+end_quote
