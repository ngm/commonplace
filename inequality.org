:PROPERTIES:
:ID:       29e21482-9612-480b-bebf-ad6fcbf96151
:mtime:    20220802230700
:ctime:    20220802230700
:END:
#+TITLE: Inequality
#+CREATED: [2022-08-02 Tue]
#+LAST_MODIFIED: [2022-08-02 Tue 23:07]

#+begin_quote
In ONE day 7 billionaires ⬆️'d their wealth by $30 billion

⬆️$13 bn Jeff Bezos
⬆️$5.74 bn Elon Musk
⬆️$4.58 bn M Bezos
⬆️$2.93 bn Steve Ballmer
⬆️$1.99 bn L Page
⬆️$1.89 bn S Brin
⬆️$1.25 bn Mark Zuckerberg

While world's poorest countries pay $2.8 bn/month in debt repayments 

https://twitter.com/Oxfam/status/1286254934841794561
#+end_quote
