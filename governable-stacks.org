:PROPERTIES:
:ID:       3862763b-409e-4c92-bf44-8c30a649ef83
:mtime:    20221121134255 20220213110736 20220212165306 20220212150949 20220113140927 20220113121118
:ctime:    20220113121118
:END:
#+TITLE: Governable stacks
#+CREATED: [2022-01-13 Thu]
#+LAST_MODIFIED: [2022-11-21 Mon 13:42]

[[id:823c1ed3-7a83-45b7-b761-ef465167205c][Governable]] [[id:f453d171-859b-462a-ae38-97373d0954e8][stack]]s.

A term coined by [[id:c4699fb0-f29b-4525-a0ec-44382aca4fb6][Nathan Schneider]] in the article [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]].

As far as I read it, in a nutshell, a governable stack is a set of tools/system that affords self-governance to its members across the multiple layers that build up the system.

#+begin_quote
Stacks: collections of tools and social practices that become the infrastructure of our lives.

Governable: the quality of being accountable to the people affected by something and whose labor produces it

https://twitter.com/ntnsndr/status/1481497350321283075
#+end_quote

+ https://twitter.com/ntnsndr/status/1481493320060657664

It makes a lot of sense to me - the governable part sounds very similar to [[id:a7ee13af-296f-478b-bbb3-3d7f60547b33][commoning]].  The stack part makes perfect sense - for example, what would be the point of a democratically-owned social media site, if the server it runs on is owned by Amazon, and the device you access it on is locked down by Apple or Google.

#+begin_quote
Governable stacks are cyborg assemblages of inter-operating technology, in symbiosis with human relationships (Haraway 1991; Puar 2012)

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
governable stacks invite the people who use them to change their relationship with technologies, to imagine different sorts of technologies, and to be changed themselves

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
The lifeblood of the governable stack is not any claim to innovation but the [[id:96c00d67-eb2f-4bd3-a37a-111536d83c91][self-governance]] that flows through it. What emerges from there is the point.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
There can be no one governable stack – only many, whose archipelagos of commoning enable each other and give rise to more.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

* Libre software

#+begin_quote
Technologists seeking alternative visions have often gravitated to the Free Software and Open Source movements, which employ creative licensing to enable the sharing of accessible and modifiable code. These movements have been successful in terms of the sheer volume of widely used software in their commons. But their emphasis on the freedoms of individual users, as well as of corporations, has privileged those with the technical know-how to take advantage.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
Governable stacks should prioritise community accountability alongside individual freedom.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

* Strategy

** Sovereignty 

#+begin_quote
What makes technology sovereign is when its stewards are the people who depend on it, protected from outside control by any legal or extra-legal means available. The data, the algorithms, and the interfaces are for their users, rather than acting surreptitiously against them.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

** Democracy

[[id:e447c8a9-81f1-4764-81fd-2e077d05b16c][Participatory democracy]] is the guarantor and the everyday practice of sovereignty.

#+begin_quote
Organisational designs that work well could become part of a [[id:012b7696-53c3-4f90-8636-0eb8ef1fff88][governance commons]], enabling other groups to adopt, adapt, and share them back into the common pool (Schneider et al. 2021). 

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
The more we demand and practice the arts of self-governing, the harder we are for someone else to govern.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

** Insurgency

* Examples

+ [[id:9bf42d1b-2a72-4480-8d99-0303924b9b43][May First Movement Technology]]
+ [[id:6a0b38ec-8e87-45b0-9edc-19411f8b463a][Community broadband]] 
+ koreader

Basically reading information. 

Koreader, epub, etc the software is fairly governable

Hardware a little bit (at least I can install my own software). Not sure how repairable it is.

Network stack - no control at all. Not my router, not my connectivity.

