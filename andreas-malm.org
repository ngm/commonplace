:PROPERTIES:
:ID:       38a872a6-1b07-40b6-8bdd-30cba08ee59c
:mtime:    20220819120902 20220807162534
:ctime:    20220807162534
:END:
#+TITLE: Andreas Malm
#+CREATED: [2022-08-07 Sun]
#+LAST_MODIFIED: [2022-08-19 Fri 12:09]

#+begin_quote
In Corona, Climate, Chronic Emergency, Andreas Malm suggests that neither anarchist horizontalism nor social democracy is capable of decarbonizing society fast enough to avoid the dire consequences of ecological collapse. Rehearsing a familiar Marxist critique of anarchism, Malm finds the tradition to be too decentralized, too opposed to programs, discipline, and the state’s potential as an instrument of revolutionary transition.

-- [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]]
#+end_quote

See discussion in [[id:96a37fb7-282b-44a5-bb88-d5332d49c581][Neither Vertical Nor Horizontal]].

#+begin_quote
If neither anarchism nor social democracy are up to the task, then what are we left with? Malm’s answer intends to provoke: eco-Leninism and [[id:25af3708-5994-46b5-a267-5e730efda006][war communism]]

-- [[id:4f55d021-f092-42e1-860a-75677db167c5][Climate Leninism and Revolutionary Transition]]
#+end_quote
