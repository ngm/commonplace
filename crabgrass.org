:PROPERTIES:
:ID:       1cb5d268-95b1-4abb-ae81-a7241b5c2e59
:mtime:    20211127120809 20210912122148
:ctime:    20210912122148
:END:
#+TITLE: Crabgrass
#+CREATED: [2021-08-07 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
Crabgrass is a web application designed for activist groups to be better able to collaborate online. Mostly, it is a glorified wiki with fine-grain control over access rights.

-- [[https://0xacab.org/liberate/crabgrass][liberate / crabgrass · GitLab]] 
#+end_quote
