:PROPERTIES:
:ID:       07b0404a-18d4-4214-a130-d7a0ac6d9e7f
:mtime:    20220702135937 20211127120757 20211008200618
:ctime:    20211008200618
:END:
#+TITLE: Meat consumption
#+CREATED: [2021-10-08 Fri]
#+LAST_MODIFIED: [2022-07-02 Sat 13:59]

Meat production is a major contributor to global heating.

#+begin_quote
About 300 million metric tonnes of meat were consumed in 2018. Meat and dairy alone account for 14.5% of all greenhouse gas emissions, but they also account for most deforestation

-- [[id:eda04511-6a17-4173-98b1-b94b97cb259b][For a Red Zoopolis]]
#+end_quote
