:PROPERTIES:
:ID:       993c2cb7-1378-4b71-a809-4621255d665f
:mtime:    20211127120916 20210724222235
:ctime:    20210724222235
:END:
#+title: Workflow for taking notes from podcasts
#+CREATED: [2021-05-23 Sun 21:47]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]



* Making notes
  
I don't have a very good workflow for taking notes from podcasts.  I listen to them in [[id:d3ce0fb4-d851-488e-ae24-86e199091057][AntennaPod]] usually while either going for a walk, or doing some household chores.  I'd like a way to capture when a particular comment or point in the podcast resonates with me.

I suppose MVP would be just to be able to bookmark that timestamp in the podcast.  That's not too difficult to do with AntennaPod - I can share to orgzly.

Better is to make a note with it as well.

The trouble with both of those is that often my hands are full.  I wonder if I can try voice commands for it.  I'd prefer not to use google voice stuff, but worth a shot.

* Finding podcasts

listennotes is good for finding podcasts on a particular topic.  But a silo.

* See

- [[https://networkedthought.substack.com/p/learning-effectively-with-podcasts][Learning effectively with podcasts - Networked Thought and Learning]] 
