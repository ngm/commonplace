:PROPERTIES:
:ID:       65ddc3d6-149c-4043-922d-44f92751ba73
:mtime:    20221214210132 20220123182109
:ctime:    20220123182109
:END:
#+TITLE: Guild socialism
#+CREATED: [2022-01-23 Sun]
#+LAST_MODIFIED: [2022-12-14 Wed 21:22]

[[id:dee08041-41d9-4c0c-acab-e081ee653b16][G. D. H. Cole]].

#+begin_quote
We find in Cole’s writings an intriguing theory of [[id:b68ece14-eba2-4950-b327-ec209eff6af3][democratic associationalism]], in which cooperation rather than force is seen as the mechanism that enables people to build organisations and work towards shared goals.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
The driving force of Cole’s guild socialism was the desire to institute what he called a ‘functional democracy’ in different domains of social life. This was an idea of democracy understood not simply as a set of abstract rights but in terms of real participation and involvement in the most important associations that governed individuals’ everyday lives. 

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
He called for a participatory democracy in which multiple and overlapping producer, consumer and municipal associations coordinated social life. In Cole’s sketch in Guild Socialism Restated, he envisioned cultural councils organising art galleries, museums and libraries; education guilds organising schools, universities and other tertiary institutions; health guilds; collective utilities councils; industrial councils; civil service councils; consumer councils; and co-operative councils. 

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
This vision of a radical democratic future entailed a greater emphasis on distributed forms of governance, a participatory political culture and citizens exercising meaningful democratic control over their lives.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

- distributed forms of governance
- participatory political culture
- citizens exercising meaningful democratic control over their lives

#+begin_quote
Cole’s theory required a decentralisation of power throughout society. He argued that the power and authority of the state should be drastically reduced to a coordinating agency – one small element in a larger social system of internally democratic associations led by active citizens.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

- decentralisation of power throughout society
- state should be reduced to a coordinating agency

#+begin_quote
Cole represented what Mark Bevir has called a ‘distinctive socialist tradition of pluralism’ within early twentieth-century British politics which challenged the state’s centralisation of power and the tendency of socialists to think of socialisation exclusively in state terms.8 The pluralist agenda of this associationalism posed a radical alternative to Fabianism and the various forms of state socialism of the era.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

- pluralism

#+begin_quote
For Cole, the central concept was ‘community control’ – how interconnected individuals working towards a common cause could regain collective self-determination over their economic and political institutions.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote
