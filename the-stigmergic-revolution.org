:PROPERTIES:
:ID:       d32acec6-5e52-48fa-8952-aa2104226f28
:mtime:    20220227172245 20220221175347 20220220183104 20220220125533
:ctime:    20220220125533
:END:
#+TITLE: The Stigmergic Revolution
#+CREATED: [2022-02-20 Sun]
#+LAST_MODIFIED: [2022-02-27 Sun 17:22]

+ URL :: https://c4ss.org/content/8914
+ Author :: [[id:fda9a645-cdfd-46ce-a6bc-d435f4b57934][Kevin Carson]]
+ Publisher :: [[id:a5fc50a9-93f6-4eb8-b6b7-6796f8bc347d][Center for a Stateless Society]]

[[id:01e5d548-31c9-4034-a50d-8f61bcad34d9][Stigmergy]].  [[id:8e7a6330-0600-418b-8c90-457dac0a42c7][Revolution]].

I always like political metaphors to [[id:0b43d410-b347-4e06-851c-ce9a2abde212][Evolutionary and adaptive systems]] / [[id:d201b97a-3067-4a7c-adb3-61d0fd08932d][complex systems]] stuff.  (That said: similar to [[id:abb53f00-a3b3-457e-9faa-53247fbd9d7b][Fractals]], they can be fairly easily shoehorned in to topics.)

Fascinating / sad seeing the level of hope (hubris?) around [[id:2df7e386-5ea3-4c83-9f06-c89980b17753][Occupy]] at the time it was happening. We know now that it didn't bring revolution long term, which is always one of the main criticisms levelled at it.

Perhaps parallels could be drawn how stigmergy (in nature) is not always enough. How some elements of the vertical might be required.

What would the vertical be for (natural) stigmergy?

Surprisingly I have still never read [[id:3af8c064-5d40-43fe-98f8-94b32ec840ee][The Cathedral and the Bazaar]].  Probably should, despite issues with Eric Raymond.
