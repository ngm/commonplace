:PROPERTIES:
:ID:       b4af43df-a369-48f5-9e06-ddceb3998550
:mtime:    20211127120933 20210824153102
:ctime:    20210824153102
:END:
#+title: The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons
#+CREATED: [2021-05-16 Sun 15:06]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

A section in the book [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].

#+begin_quote
Commoning has a different orientation to the world because its actions are based on a deep relationality of everything. It is a world of dense interpersonal connections and interdependencies. Actions are not simply matters of direct cause-and-effect between the most proximate, visible actors; they stem from a pulsating web of culture and myriad relationships through which new things emerge.
#+end_quote

[[id:b85a9356-9e1f-468c-a8ee-2e72184522e9][Relational ontology]].
[[id:fe97deca-0989-4cf0-aca8-04c1701f66b5][Ubuntu Rationality]]. [[id:80318426-c892-4d63-ac87-9b8f085be6db][Nested-I]].
