:PROPERTIES:
:ID:       24386b57-5a4c-4259-ba95-b85581c481b2
:mtime:    20211127120813 20210724222235
:ctime:    20210724222235
:END:
#+title: Vaporwave
#+CREATED: [2021-04-05 Mon 15:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
It’s an ironic, embittered genre that asks: What if the utopian innocence of those early Geocities websites had survived, and what if we all lived in chill, pastel, communal harmony? What makes vaporwave so distinct, other than its dubiously Marxist undertones, is that it is utopian and therefore against the grain of the modern mania for dystopian thought. 

-- [[file:404-page-not-found.org][404 Page Not Found]]
#+end_quote


 #+begin_quote
 the visual remnants of vaporwave have long outlasted its radical ideological underpinnings. Almost immediately, its pastel, geometric, softcore aesthetics were gobbled up by media platforms, in particular the image-driven platforms Tumblr and Instagram. The pastiche compositions of Arizona Iced Tea cans and old Windows desktops were very quickly made available on all these commercial interfaces, which were not only feeding on a countercultural art movement—they were likewise consuming the ghosts of an internet they had long since murdered.

 -- [[file:404-page-not-found.org][404 Page Not Found]]
 #+end_quote
