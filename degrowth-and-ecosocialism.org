:PROPERTIES:
:ID:       ab37e11d-9068-4477-a256-e482a7c4c90e
:mtime:    20231022123001 20230924104922
:ctime:    20230924104922
:END:
#+TITLE: Degrowth and ecosocialism
#+CREATED: [2023-09-24 Sun]
#+LAST_MODIFIED: [2023-10-22 Sun 12:30]

See [[id:7f36f5ae-b024-4b95-bd99-37d78b141be9][Ecosocialism and degrowth]].
