:PROPERTIES:
:ID:       613d646d-aa9a-425d-b0ca-eb581da4e34c
:mtime:    20220806191447
:ctime:    20220806191447
:END:
#+TITLE: The Case Against the Professional Managerial Class with Catherine Liu
#+CREATED: [2022-08-06 Sat]
#+LAST_MODIFIED: [2022-08-06 Sat 19:20]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.upstreampodcast.org/conversations

[[id:f1e20e95-e8a3-4041-aa65-b55583092669][Professional-managerial class]].

~00:02:29  PMC is technically a stratum of the working class but kind of aligned with capitalist class.

~00:21:07  Idea that PMC tend to think that they are more virtuous than other strata of working class.

~00:53:28  [[id:1b90c292-7b49-46fb-be11-18ae333078c0][Taylorism]] and PMC.

~00:58:46  [[id:170a031b-b42b-448e-a950-388c78f1ec49][New Left]] was dominated by PMC.

~01:06:19  PMC always favours [[id:fab3929d-fafd-47fd-8890-98d3da2fbe33][incrementalism]] over [[id:8e7a6330-0600-418b-8c90-457dac0a42c7][revolution]].

