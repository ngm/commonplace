:PROPERTIES:
:ID:       d7825ec8-401a-462f-a3c9-21875c252f1f
:mtime:    20221028110214 20211127120956 20210819010324
:ctime:    20200404101731
:END:
#+TITLE: Cybernetic Revolutionaries

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Author :: [[id:5cdbc099-5e4c-4af4-89a3-b3ecf268bc81][Eden Medina]]

Primarily on the topic of [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]].  With plenty of good [[id:9f6bede9-8f2d-4f44-aa40-4ed1bf8eb1b4][STS]] reflections along the way.

#+begin_quote
this connection between cybernetics and Chilean socialism came about, in part, because Beer and Popular Unity, as Allende’s governing coalition was called, were exploring similar concepts, albeit in the different domains of science and politics
#+end_quote

#+begin_quote
Chile was not able to implement its political dream of democratic socialism or its technological dream of real-time economic management
#+end_quote

#+begin_quote
major theme in Beer’s writings was finding a balance between centralized and decentralized control, and in particular how to ensure the stability of the entire firm without sacrificing the autonomy of its component parts.
#+end_quote

#+begin_quote
how do you create a system that can maintain its organizational stability while facilitating dramatic change, and how do you safeguard the cohesion of the whole without sacrificing the autonomy of its parts
#+end_quote

#+begin_quote
brought together ideas from across the disciplines—mathematics, engineering, and neurophysiology, among others—and applied them toward understanding the behavior of mechanical, biological, and social systems.23
#+end_quote

#+begin_quote
for Beer, cybernetics became the “science of effective organization
#+end_quote

#+begin_quote
Keller instead argues that the cybersciences also emerged as a way to embrace complexity and “in response to the increasing impracticality of conventional power regimes
#+end_quote

#+begin_quote
He embraced complexity, emphasized holism, and did not try to describe the complex systems he studied, biological or social, in their entirety
#+end_quote

#+begin_quote
To put it another way, Beer was more interested in studying how systems behaved in the real world than in creating exact representations of how they functioned
#+end_quote

+ [[id:a2f3ed90-0e35-45f9-b82b-0cff49b6ba90][There are different strands of cybernetics]]

