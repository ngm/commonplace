:PROPERTIES:
:ID:       e1e50dce-7727-4f48-8939-8ec843e8f0a0
:mtime:    20220918113446 20220903182034
:ctime:    20220903182034
:END:
#+TITLE: Matthew Huber
#+CREATED: [2022-09-03 Sat]
#+LAST_MODIFIED: [2022-09-18 Sun 11:35]

Huber is a proponent of socialist [[id:1118361a-1e06-40f8-9b12-b4a5d7b033b5][eco-modernism]].

Author of [[id:4ab7e432-2182-4651-bf7d-c0f0805ab970][Climate Change as Class War: Building Socialism on a Warming Planet]].
