:PROPERTIES:
:ID:       be4ba7ad-b327-4dd0-8d21-7126b0f7b63c
:mtime:    20230521204259 20230521190158 20230520183722 20230520104342
:ctime:    20230520104342
:END:
#+TITLE: unionization of creatives at Vice Media
#+CREATED: [2023-05-20 Sat]
#+LAST_MODIFIED: [2023-05-21 Sun 20:44]

+ An :: [[id:9559bed4-4bf5-4e55-bd1f-bc687d5ad2c4][initiative]] / [[id:f9890d93-64c3-4c4d-9e31-52916c24a56c][resist]]

* Resources

- [[https://variety.com/2017/digital/news/vice-media-unionization-writers-guild-editors-guild-1202565117/][Vice Media Unionizes With Writers Guild East and Editors Guild - Variety]]

