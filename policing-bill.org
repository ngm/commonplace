:PROPERTIES:
:ID:       6c4bdf5e-8cbd-45bb-bc2a-dec1ba0d428d
:mtime:    20220610094546 20220113144445 20220113093608 20211127120851 20211005212206
:ctime:    20211005212206
:END:
#+title: Policing bill
#+CREATED: [2021-06-23 Wed 20:39]
#+LAST_MODIFIED: [2022-06-10 Fri 09:45]

- https://bills.parliament.uk/bills/2839

[[id:5e1c0b25-51ee-4bcd-afaa-7b65cff081ac][I strongly dislike the policing bill]].

#+begin_quote
The bill includes powers for the home secretary to ban marches and demonstrations that might be “seriously disruptive” or too noisy; a criminal offence of obstructing infrastructure such as roads, railways, airports, oil refineries and printing presses; jail sentences for attaching or locking on to someone or something; bans on named individuals from demonstrating or going on the internet to encourage others; police stop-and-search powers to look for protest-related items; and up to 10 years’ prison for damaging memorials or statues

-- [[https://www.theguardian.com/world/2022/jan/13/thursday-briefing-a-man-without-shame][Thursday briefing: ‘A man without shame’ | | The Guardian]]
#+end_quote

  #+begin_quote
  Tucked away in the government’s 300-page police, crime, sentencing and courts bill, are various clauses which will have serious implications for the right to protest. The bill seeks to quietly criminalise “serious annoyance”, increase police powers to restrict protests, and give the home secretary discretion over what types of protests are allowed.

-- [[https://www.theguardian.com/commentisfree/2021/aug/09/police-bill-not-law-order-state-control-erosion-freedom][The police bill is not about law and order – it’s about state control | Joshu...]] 
  #+end_quote

#+begin_quote
A new policing bill that will be debated this week risks deepening racial and gender disparities in the justice system while forcing professionals to betray the trust of vulnerable people, hundreds of experts and a report have warned.

-- [[https://www.theguardian.com/uk-news/2021/sep/13/policing-bill-will-deepen-racial-and-gender-disparities-say-experts][Policing bill will deepen racial and gender disparities, say experts | Police...]] 
#+end_quote

#+begin_quote
As home secretary, Priti Patel has been explicit that the draconian anti-protest provisions of the Police, Crime, Sentencing and Courts Bill are directed against new environmental and racial equality movements in particular.

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote
