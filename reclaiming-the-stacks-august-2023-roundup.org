:PROPERTIES:
:ID:       98037a1a-d552-4cd4-a210-92c57bf8bd2d
:mtime:    20230806113325 20230806101754 20230805153715 20230805122435 20230805110216 20230805100211 20230728171909 20230723203415 20230723183005 20230722155019 20230722103823 20230721184614
:ctime:    20230721184614
:END:
#+TITLE: Reclaiming the stacks: August 2023 roundup
#+CREATED: [2023-07-21 Fri]
#+LAST_MODIFIED: [2023-08-06 Sun 12:31]

Welcome to the latest monthly roundup of my explorations at the intersection of ecosocialism and ICT.   I look at the problems of [[id:c36a0334-b4a0-4f89-a2c8-ba4cad680ec7][digital capitalism]] and positive actions that embody [[id:e3868d08-bfb3-4d5e-aeaf-784cc7543e0d][digital ecosocialism]], along with a look at the systemic side of things and how we can transition from (digital) capitalism to (digital) ecosocialism.

It all comes from the angle of [[id:ef81f2f9-7d41-4ff4-a731-0ab54416193d][reclaiming the stacks]] - expropriating information and communications technology from [[id:4f2e9822-8e1e-4599-92eb-e9fc5e948bd5][Big Tech]] and returning it to the people.

* Systems

"[[id:af940e79-9c10-4d76-ae72-45190ac3ce62][It's not about your footprint, it's about your point of leverage]]" says Simon Sharpe.  It's been clear for a while that [[id:abdb6222-bf1d-49ce-b472-fc33ae57b21a][carbon footprint]] isn't a good frame for how individuals can contribute to a climate transition.  In his article Sharpe reiterates that idea, and suggests that its better to think about one's [[id:29e973a9-1671-46f5-b7a7-1afb3d3d509e][leverage points]] instead.

Sharpe talks about leverage at a few different levels : the individual, the organisational, and the national, and, in another article, discusses leverage points at the sectoral level:

#+begin_quote
In any individual sector, we can identify leverage points: actions that are relatively low cost or low difficulty but that have a high impact in accelerating the transition.

-- [[id:460225de-db8e-4ad8-bff6-1a8da26a318e][Super-leverage points]]
#+end_quote

He also talks about *super*-leverage points:

#+begin_quote
In the whole system, we can identify super-leverage points: we define these as actions that are high leverage in the sectors where they are taken, and that influence transitions in other sectors in a way that is positive in direction, high in impact, and reasonably high in probability.

-- [[id:460225de-db8e-4ad8-bff6-1a8da26a318e][Super-leverage points]]
#+end_quote

Leverage points are a key part of [[id:e044a716-096c-438e-b69a-9a2f177d781b][systems thinking]].

#+begin_quote
how do we change the structure of systems to produce more of what we want and less of that which is undesirable?

-- [[id:2137fca1-2f70-4500-8a35-017ecdb4139d][Thinking in Systems]]
#+end_quote

What are the leverage points in ICT?  What are the super-leverage points?
How do we change the structure of ICT to produce more of what we want and less of that which is undesirable?

From an ecosocialist standpoint, we want more digital ecosocialism and less digital capitalism. We want to identify actions in ICT which can have a positive, impactful, and highly probable influence on agency, social equity and planetary stability.  I started to review work on this in [[id:074245d4-50df-4ae5-952e-422a8df158cd][Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism]].   Some sources of inspiration:

- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform Socialism]]
- [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]]
- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
- [[id:9b8ea846-9247-47b3-b775-082ba1a26630][Digitalization and the Anthropocene]]
- [[id:73529ed3-e9b9-4ba8-a0c7-912bc67d2d94][Leveraging Digital Disruptions for a Climate-Safe and Equitable World: The D^2S Agenda]]

Together, these contain quite a number of actions and initiatives and possible leverage points[fn:1].  There's a few common themes that stuck out - things like [[id:2f0d168a-4af5-4e96-a3e1-5c12263cf4fc][deprivatisation]]/(re)[[id:bfb6a061-a72a-4612-a7d5-ef8bc6b02a88][socialisation]] of infrastructure and platforms; [[id:8b592e91-4798-425e-babb-b7062a165d57][knowledge commons]], [[id:a7ee943a-32fb-4078-8bc0-6bdaa2b99091][tech cooperatives]] and [[id:b6c98503-1b96-496f-a805-287e7cda1f46][public-commons partnerships]]; libre and open software, data and access.  And the notion of [[id:82ae3aaf-9ec6-48d8-ac5c-ff8e0328311b][resist, regulate, recode]] as a triumvarate of types of leverage.  But it's not clear which of these, if any, are key leverage points, or even super-leverage points.  A systemic analysis of how they all connect together could help.

I recently came across "[[id:130c8726-4f8f-4cba-9625-07f32e183eb5][A leverage points analysis of a qualitative system dynamics model for climate change adaptation in agriculture]]", which has an interesting methodology for surfacing leverage points within a sector.  It would be interesting to see how the same could be applied to ICT.  It all starts off with a [[id:2fc9f4bb-74c5-4151-be4d-0ccc45c3ff45][qualititative system dynamics model]], so that will be where to look next.  The references above each contain their own informal, qualitative system dynamics model of ICT, so a starting point will be to look through them again with that lens.

* Problems

/Some problems from digital capitalism recently in the news.  To help map them out, I'm tagging them with some of the criteria I [[id:074245d4-50df-4ae5-952e-422a8df158cd][defined]] in my OU research./

- [[id:caeb9553-f857-47a8-8558-29988f846a44][ChatGPT Will Command More Than 30,000 Nvidia GPUs: Report]]. AI is driving the manufacture of a shit-ton of hardware.  And the negative environmental impact associated with that.  (A problem of [[id:2e1f998d-e505-4f42-8d13-3400a023a514][AI]], [[id:5859b765-2844-4c2e-b3ab-04fbe84ab963][growthism]] and [[id:2821287c-79b4-447d-a9ac-4c0cf2a5b7a5][overconsumption]], counter to [[id:5380f9c6-e7a4-49de-8a9f-72b3f97100df][planetary stability]], at the hardware layer of the stack).
- [[id:2fb1c33a-eb25-4868-b9fe-50139c86782d][The Generative AI Race Has a Dirty Secret]].  Not really a secret - LLMs use a vast amount of energy.  And this is set to increase as they get integrated into search engines.  (A problem of [[id:2e1f998d-e505-4f42-8d13-3400a023a514][AI]], [[id:81842fd4-cd8e-401a-ba27-548b1e856d36][data centres]] and [[id:fcc12795-2b14-4a8e-b1fe-d569c1bf7075][unnecessary technology]], counter to [[id:5380f9c6-e7a4-49de-8a9f-72b3f97100df][planetary stability]], at the software layer of the stack).
- [[id:76eb3296-bbf2-4f36-bd50-32db8c9cb4b2][Meta report shows the company causes far more emissions than it can cover with renewable energy]]. Similarly to Microsoft ([[id:c52f1af6-c648-44a0-9d74-90591861e4c7][Microsoft's dirty supply chain is holding back its climate ambitions]]), Meta has huge emissions, most of which are scope 3 emissions from its ecosystem and supply chain, and apparently it doesn't know what to do about them.  Here's a couple of ideas: (a) consider degrowth; (b) use your considerable clout on your supply chain, rather than pretending that you are helpless to do anything about it.  (A problem of [[id:cd90a972-b079-4e1c-b02b-21d9e4009144][carbon emissions]], [[id:0c84c589-cc3d-46c0-8899-0c458ef346eb][supply chains]] and [[id:29367dfa-0a47-497f-b979-77108dfd797c][scope 3 emissions]], counter to [[id:5380f9c6-e7a4-49de-8a9f-72b3f97100df][planetary stability]], at the software and hardware layers of the stack).

* Actions

#+begin_quote
Philosophers have only interpreted the world, in various ways; the point, however, is to change it.

-- Karl Marx
#+end_quote

/Some latest news on concrete actions that are part of an ecosocialist ICT movement./

- [[id:8a5f1b53-52d3-4d88-a76c-0a152e011af8][Extreme heat prompts first-ever Amazon delivery driver strike]].  (A [[id:523b3f4d-f27b-4ea0-b245-faf4a640b57f][strike action]], supporting [[id:0044e470-b5d2-46e7-890f-704da89c5e97][social equity]] and [[id:47b5a145-cfcd-433a-86af-87139e2578f3][agency]], to resist [[id:c0096cd9-ee40-4ea6-8f22-c4ba6143c56f][worker exploitation]]).
- [[id:97040857-2bb2-4059-8b2f-c29c523ccf32][Big win for right to repair with new EU rules for batteries - but legislators must get the implementation right]]. Good right to repair news - progress on battery removability and availability.  Though still lacking on the affordability of repair.  ([[id:5c070378-f193-47b9-ac19-7a11094d9538][Regulation]] on the [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]], supporting [[id:5380f9c6-e7a4-49de-8a9f-72b3f97100df][planetary stability]] and agency, to resist [[id:5859b765-2844-4c2e-b3ab-04fbe84ab963][growthism]] and [[id:2821287c-79b4-447d-a9ac-4c0cf2a5b7a5][overconsumption]]).

* Inputs

/Finally, a few other things I've been reading, listening to, and watching that are adjacent to the topics of ecosocialism and ICT./

** Reading

- [[id:eb09dfe6-3ad3-46df-a1f9-e1f439d1d88a][How an eccentric English tech guru helped guide Allende’s socialist Chile]] (h/t [[id:62180b97-a71d-419f-997e-f3e81291e4f6][Panda]])
  + A short article on Chile, [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]] and [[id:7f194828-b798-46d2-872b-24459a13bf51][Stafford Beer]], apropos [[id:59a0f5ec-ba65-4ceb-98cf-8e2d8ee2c72c][Evgeny Morozov]]'s new podcast series on the topic, [[id:fc6bf9f2-805f-4324-b0b1-30ce9e313246][The Santiago Boys]].

- [[id:55d8652c-80e8-4f3c-82bd-4098621e0c6c][ICT: A top horizontal priority in sustainable product policy]]
  - A report on the need for horizontal measures for the regulation of various hardware products in the ICT sector to make them last longer.

- [[id:01b01114-c64b-4767-abc2-3ae62d4aa3ec]['A certain danger lurks there': how the inventor of the first chatbot turned against AI]]
  + /"Perhaps his most fundamental heresy was the belief that the computer revolution, which Weizenbaum not only lived through but centrally participated in, was actually a counter-revolution. It strengthened repressive power structures instead of upending them."/

** Listening

+ [[id:7f6303c2-64e6-408c-a167-c4880a5bcc27][Class Politics in a Warming World with Keir Milburn]]
  + I like [[id:101619b4-b87b-4771-aa2b-14d52edf62c1][Keir Milburn]].    This interview is about the [[id:be692058-1315-4284-81ac-13d2240abd11][intersection of class politics and environmental politics]], and ranges across lots of interesting topics related to that, including Milburn's personal history in both of those movements.
+ [[id:30941957-be6a-4838-8fa8-6a56a5827230][Microdose: Californian Capitalism]].
  + Interview with Malcolm Harris on his book [[id:3cbec8b6-a514-4eb4-adcc-cfe309200e54][Palo Alto: A History of California, Capitalism, and the World]].  Includes some interesting takes on [[id:d5f80718-5ca1-4f9a-b7d3-979d594e30bb][The Californian Ideology]] and the left and technology.

* Until next time

That’s it! See you next month. Until then, you can find latest streams of thoughts over at my [[https://doubleloop.net][website]].

* Footnotes

[fn:1] Which I've partially documented in a ramshackle way in my digital garden at [[id:c8784e29-ead7-4c90-881c-1e062c7b8e17][ways to reclaim the stacks]], and am currently trying to put a bit more structure around in Anytype.
