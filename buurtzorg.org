:PROPERTIES:
:ID:       a8228835-f3df-418c-88e0-19c7e856cfe7
:mtime:    20230402182654
:ctime:    20230402182654
:END:
#+TITLE: Buurtzorg
#+CREATED: [2023-04-02 Sun]
#+LAST_MODIFIED: [2023-04-02 Sun 18:27]

#+begin_quote
the Buurtzorg social care co-operative in the Netherlands, which works with the needs of the client, is rated extremely highly by users and employees, and moreover saves 40 per cent in costs to the national healthcare system by prioritising quality and need over profit

-- [[id:2bedad4c-2f7f-4e3e-bfe0-140bfdd74b51][The Care Manifesto]]
#+end_quote
