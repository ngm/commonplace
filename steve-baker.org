:PROPERTIES:
:ID:       032eab29-4b3a-46d5-8b80-570f0df02149
:mtime:    20220708115013 20220330184327
:ctime:    20220330184327
:END:
#+TITLE: Steve Baker
#+CREATED: [2022-03-30 Wed]
#+LAST_MODIFIED: [2022-07-08 Fri 11:50]

#+begin_quote
Baker is a key member of the [[id:8719759a-0a84-434b-87a7-e0c3f569e060][Net Zero Scrutiny Group]] (NZSG), a Tory grouping set up by his fellow Conservative MP Craig Mackinlay last summer.

-- [[https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda][‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero ...]] 
#+end_quote

#+begin_quote
The self-described [[id:4fa734f0-8b65-4c28-a631-51d100c569cc][Brexit]] hardman, who as the then head of the [[id:17e47ebb-c34f-4195-88b5-f21bb9d1d093][European Research Group]] had relentlessly harried Theresa May’s government over Brexit and then become a thorn in Boris Johnson’s side with his anti-lockdown [[id:144ea29b-b0b0-4546-8238-229baea63262][Covid Recovery Group]], had a new – and he believed potentially explosive – target: the government’s climate agenda.

-- [[https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda][‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero ...]] 
#+end_quote

 #+begin_quote
 Baker is a trustee of the [[id:4dda971e-70e4-4b1f-bcad-fd6ad125c2cc][Global Warming Policy Foundation]] (GWPF).
 
-- [[https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda][‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero ...]] 
 #+end_quote

 A proponent of [[id:a2c4748d-2c2e-4f08-b108-82e22030cf49][Austrian School]] economics.
