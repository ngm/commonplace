:PROPERTIES:
:ID:       97040857-2bb2-4059-8b2f-c29c523ccf32
:mtime:    20230805151708
:ctime:    20230805151708
:END:
#+TITLE: Big win for right to repair with new EU rules for batteries - but legislators must get the implementation right
#+CREATED: [2023-08-05 Sat]
#+LAST_MODIFIED: [2023-08-05 Sat 15:21]

+ URL :: https://repair.eu/news/big-win-for-right-to-repair-with-new-eu-rules-for-batteries-but-legislators-must-get-the-implementation-right/

#+begin_quote
In an important development for the right to repair, the European Parliament just approved a new battery regulation. The progress made on *battery removability and availability* is a step in the right direction, although the *affordability of repair* must still be addressed – in both the battery regulation and the European Commission’s linked “Right to Repair” proposal.
#+end_quote

#+begin_quote
In a big success for the right to repair, all new portable devices and light means of transport put on the market will now have to be *designed with replaceable batteries*. In many cases *users will be able to replace them themselves*. Manufacturers will also have to make batteries *available as spare parts for 5 years* after placing the last unit of a model on the market. The text states that *spare batteries must be sold at a reasonable and non-discriminatory price*, and we will keep a eye on OEMs to make sure that this is actually implemented
#+end_quote

#+begin_quote
We also celebrate that manufacturers will *no longer be able to use the unfair practice of [[id:0ad788aa-9183-4207-8471-9e108a99b969][part-pairing]] in batteries*, which they use to attempt to control what spare parts should be used for repairs.
#+end_quote
