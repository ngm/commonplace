:PROPERTIES:
:ID:       61178bdb-37c5-4cde-8cfa-105b1c54aa58
:mtime:    20220212111555 20220208174826 20220206210728
:ctime:    20220206210728
:END:
#+TITLE: Cooperative ownership structures
#+CREATED: [2022-02-06 Sun]
#+LAST_MODIFIED: [2022-02-12 Sat 11:18]

* [[id:21f4ca5f-b0b2-4b3c-86cb-da206ab7ee08][Worker cooperatives]]

#+begin_quote
Worker-owned: the workers of the co-operative collectively own and manage the organisation. This could involve close collaboration in the day-to-day management of the collective or allowing a group of individuals to run the platform while workers gain increased benefits from their work due to their co-ownership of the platform.

-- [[id:8dd0882c-d3c9-4347-b10a-e6eabee29efe][The Co-operativist Challenge to the Platform Economy]] 
#+end_quote

* [[id:3f6d48c8-1d39-4365-91df-ea1693f3cdef][Producer cooperatives]]

#+begin_quote
Producer-owned: producers of the goods or services sold on the platform collectively own and manage the organisation. Producers of products such as music, photography and household goods can use a shared platform to pool resources and benefit from network effects without necessarily collaborating in the design and marketing of their products.

-- [[id:8dd0882c-d3c9-4347-b10a-e6eabee29efe][The Co-operativist Challenge to the Platform Economy]] 
#+end_quote

* [[id:7084d104-b5ef-4b0a-8b98-ea8be64486f2][Consumer cooperatives]]

#+begin_quote
Consumer-owned: businesses owned and managed by consumers with the aim of fulfilling their needs. There are many examples of non-digital consumer collectives from credit unions and electricity co-ops to food co-ops. A number of data co- operatives have emerged in which individuals pool their data to form a trust to be controlled democratically by its members.

-- [[id:8dd0882c-d3c9-4347-b10a-e6eabee29efe][The Co-operativist Challenge to the Platform Economy]] 
#+end_quote

* [[id:17e3f8ef-e295-476f-b32c-cbc055dc1728][Multi-stakeholder cooperatives]]

#+begin_quote
Multi-stakeholder: An umbrella term which incorporates a range of different co-operatives that include workers, users, founders, service providers and broader community members as part of their ownership and governance structure. An example is [[id:7ee5817d-0c6f-41ad-9dc6-ceadab048134][Resonate]], a stream-to-own music platform in which artists (45%), listeners (35%) and workers (20%) all have a stake in the co-operative.

-- [[id:8dd0882c-d3c9-4347-b10a-e6eabee29efe][The Co-operativist Challenge to the Platform Economy]] 
#+end_quote
