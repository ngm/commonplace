:PROPERTIES:
:ID:       9a67d339-c4c7-45d2-b8df-cf8502a41620
:mtime:    20211127120917 20210724222235
:ctime:    20200510093318
:END:
#+TITLE: Wiki itches

- [X] ignore certain pages (e.g. sitemap) in graphviz export
- [ ] in selection, pull out all the bolded parts and put them into a bullet list (for [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]]) 

* publishing
- [ ] use git-timemachine to publish previous versions and history
  - not sure how much this is needed on publish.  does anyone care?
- [ ] use git log to pull out and publish recent changes
  - rather than linking to commits on gitlab
