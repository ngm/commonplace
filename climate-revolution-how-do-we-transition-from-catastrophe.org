:PROPERTIES:
:ID:       9f9f9edc-e823-47ea-933f-9ebae4ce80de
:mtime:    20220918211524
:ctime:    20220918211524
:END:
#+TITLE: Climate & Revolution: How do we transition from catastrophe?
#+CREATED: [2022-09-18 Sun]
#+LAST_MODIFIED: [2022-09-18 Sun 21:33]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL ::  https://www.gndmedia.co.uk/podcast-episodes/climate-revolution-how-do-we-transition-from-catastrophe
+ Featuring :: [[id:ddb998c8-abf9-47a0-b666-e17e441a7e6f][Kai Heron]] / [[id:86dba301-4116-4681-af37-ab7d61fe584c][Jodi Dean]]

Nice interview.  I like Heron and Dean.  On [[id:3ddc4c92-47dc-4e82-ab5b-c1bc1fa36e34][revolutionary transition]] and the ideas from [[id:b2b56d4b-f05d-4470-b033-2d6882c97594][Leninism]] (though not necessarily the [[id:c72c27e2-a184-44fe-9283-3118fee3aa29][Bolsheviks]] / [[id:067f1716-cedf-4788-82ac-e42ef570e109][Russian Revolution]]) as a means to work towards that.

+ ~00:05:39  Some kind of revolution/transition is going to happen regardless - is happening already to some degree.  So we should be on top of the direction that it takes.  (Sounds similar to [[id:59ac676d-71b1-4e8f-a50f-ee409d4d1e4b][Climate Leviathan]] here?)
+ Need for party, criticisms of Occupy, etc.
+ ~00:16:25  Positives of COP -  they're talking about transition, and at least theiyre working at the global scale.
+ Rupture vs incrementalism - why we don't necessarily need to work within the bounds of existing structures (e.g. the existing Labour Party).
+ ~00:27:25  Leninism contributes towards the idea of revolutionary non-linearity.
+ ~00:29:45  Leninism is thinking at the right scale. And it thinks about transition.
+ ~00:43:34  Direct action is a tactic not a strategy - e.g. XR use it well as a tactic - but to support a flawed strategy.

Advice: join a union.  Support [[id:809aafcd-52b8-482c-817b-45526db74428][Don't Pay UK]].  Get involved in organising.
