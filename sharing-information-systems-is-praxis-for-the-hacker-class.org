:PROPERTIES:
:ID:       cd2fdec5-b69d-4393-a634-99811e634e28
:mtime:    20211127120949 20210724222235
:ctime:    20210724222235
:END:
#+title: Sharing information systems is praxis for the hacker class

- I kind of see something like Ton's public sharing of the details of his [[id:703b1d84-d999-430f-9331-85d39a653ae4][PKM]] as part of a body of artifacts for the [[id:6bf4c9fa-8c23-45f3-90ae-f784dd8af415][Hacker class]].
  - That perhaps sounds a bit grandiose, but hey if [[id:4db92dd5-f837-4ecb-b0cc-f97cb3d6f5ce][Vectoralism]] is about appropriation of knowledge work and [[id:f709307b-b37e-4eb1-b6e3-6bf5036cc8d1][commodification of information]], then open descriptions and sharing of how we do knowledge work without [[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]] tools is a small counter to that.
