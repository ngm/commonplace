:PROPERTIES:
:ID:       21130396-635a-4740-96f3-9dda2e66e191
:mtime:    20220612163202
:ctime:    20220612163202
:END:
#+TITLE: Decentralized planning
#+CREATED: [2022-06-12 Sun]
#+LAST_MODIFIED: [2022-06-12 Sun 16:33]

Decentralized economic [[id:31c25452-2977-4054-ae2e-8bc09704a9ce][planning]].  As opposed to [[id:4d234874-5fa3-419a-ba13-6dc499b591ec][central planning]].  Seems like my kind of thing.

[[id:88ca7847-6209-445e-9158-20189a396784][Viable system model]], [[id:eae4078f-002e-402d-8fc2-ed9cef5326b5][P2P accounting]] are examples?
