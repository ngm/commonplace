:PROPERTIES:
:ID:       8db18b19-6889-41cd-a99d-6168586d31fd
:mtime:    20230625115652
:ctime:    20230625115652
:END:
#+TITLE: The ultimate eco building - made of salt, sunflowers and recycled urine
#+CREATED: [2023-06-25 Sun]
#+LAST_MODIFIED: [2023-06-25 Sun 11:57]

+ URL ::
https://www.theguardian.com/artanddesign/2023/jun/05/ultimate-eco-building-salt-sunflowers-recycled-urine-atelier-luma-architecture

[[id:23af5c5c-0050-4ae3-9c64-180f2dd02859][Atelier Luma]]
