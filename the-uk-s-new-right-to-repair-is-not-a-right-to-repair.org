:PROPERTIES:
:ID:       02833094-e6e5-41a2-b854-a244f3c12513
:mtime:    20220526125147 20211127120755 20210724222235
:ctime:    20210724222235
:END:
#+title: The UK’s new 'right to repair' is not a right to repair
#+CREATED: [2021-07-14 Wed 22:56]
#+LAST_MODIFIED: [2022-05-26 Thu 12:51]

+ URL :: https://therestartproject.org/news/the-uks-new-right-to-repair-is-not-a-right-to-repair/
+ Author :: Libby Peake / Ugo Vallauri
+ Publisher :: [[id:bac1368b-df71-478f-9f9f-43ea13e7d449][The Restart Project]]

