:PROPERTIES:
:ID:       64b275a1-3153-45d3-bd16-be4fdff6dae4
:mtime:    20220731093749
:ctime:    20220731093749
:END:
#+TITLE: Commoning is based on a very different ontology from capitalism
#+CREATED: [2022-07-31 Sun]
#+LAST_MODIFIED: [2022-07-31 Sun 09:50]

[[id:a7ee13af-296f-478b-bbb3-3d7f60547b33][Commoning]] is based on a very different ontology from [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].

#+begin_quote
This is not widely appreciated because many people continue to view the commons through archaic ontological perspectives — which is to say, through the normative lens of modern, Western culture. 

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

"The normative lens of modern, Western culture".  What is that?  It must surely be hard to pin down such a concept as 'Western culture'.  I'll take it here as a shorthand for [[id:738f965a-d12b-4f71-819a-2fe73e591efc][rationalism]], [[id:1babd5b7-0800-45d7-b2aa-46bb01ac6ed2][individualism]], [[id:5562fdc2-0387-4ebb-b37b-b7bc7e8cf48b][the humanization of nature]], etc.

Capitalism's ontology:

#+begin_quote
They have internalized the language of separation and methodological individualism. They view objects as having fixed, essential attributes and being disconnected from their origins and context.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

Separation. Individualism. Fixed.

Commoning's ontology:

#+begin_quote
Commoning has a different orientation to the world because its actions are based on a deep relationality of everything. It is a world of dense interpersonal connections and interdependencies. Actions are not simply matters of direct cause-and-effect between the most proximate, visible actors; they stem from a pulsating web of culture and myriad relationships through which new things emerge.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

Relationships.  Relationality.  Connection.  Interdependence.
