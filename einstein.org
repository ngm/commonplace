:PROPERTIES:
:ID:       84921fa4-39bd-483e-96dd-04df23582623
:mtime:    20220316181637
:ctime:    20220316181637
:END:
#+TITLE: Einstein
#+CREATED: [2022-03-16 Wed]
#+LAST_MODIFIED: [2022-03-16 Wed 18:17]

#+begin_quote
While most famous for his scientific theories, Einstein was also a [[id:9b1da676-96c5-4613-a12e-2cc5ec1d253e][socialist]], and wrote this excellent explanation of the capitalist system and its inherent flaws https://libcom.org/library/einstein-capitalism

https://mastodon.social/@workingclasshistory/107957423299714594
#+end_quote
