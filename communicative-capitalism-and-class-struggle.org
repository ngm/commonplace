:PROPERTIES:
:ID:       bf1c3d4e-1e85-419e-bfa8-748787190927
:mtime:    20221016162034 20221016112050 20211127120939 20210921092033
:ctime:    20210921092033
:END:
#+TITLE: Communicative Capitalism and Class Struggle 
#+CREATED: [2021-09-21 Tue]
#+LAST_MODIFIED: [2022-10-16 Sun 16:45]

+ URL :: https://spheres-journal.org/contribution/communicative-capitalism-and-class-struggle/
+ Author :: [[id:86dba301-4116-4681-af37-ab7d61fe584c][Jodi Dean]]
+ Year :: 2014

[[id:451989b1-5163-461f-bf61-7fdfd80e96f7][Communicative capitalism]] and [[id:5c06724b-1bbe-42f7-b611-896c83222936][class struggle]].

Viewing protests of ~2011 as class struggle of the knowledge class.  Communicative capitalism as that which they are struggling against.  Appropriation of value from social interactions and big data surveillance as the thing which is being protested against.

A bit of overlap with [[id:2b50993b-0d3f-4d3a-87ae-a93220461ab8][surveillance capitalism]], which was being formulated around the same time (2014) I think.

#+begin_quote
We have entered the first phase of the revolt of the knowledge class. The protests associated with the [[id:2df7e386-5ea3-4c83-9f06-c89980b17753][Occupy]] movement, [[id:c955474f-0bb3-413f-9bbf-67e414b87379][Chilean student protests]], the Montreal protests [[[id:95b0ee2d-9669-4c27-bc37-e45b87d254ba][Quebec student protests]]], European anti-austerity protests [[[id:8adcf8c0-8b7b-465f-bda8-d120a43f4c08][Anti-austerity movement]]], some components of the protests of the [[id:afd1f758-24fb-4332-a21f-6eed5d5b15fb][Arab spring]], as well as multiple ongoing and intermittent strikes of teachers, civil servants, and medical workers all over the world, are protests of those proletarianized under communicative capitalism.
#+end_quote

Dean prefers the party to crowds (see [[id:d84b39b1-38ee-4357-87b6-1e1951576446][Crowds and Party]]), so I think she is aiming to frame the various protests of 2011 onwards as class struggles rather than spontaneous networked protest.

#+begin_quote
These revolts make sense as class struggle, as the political struggle of a [[id:976e23c8-1f2e-4e78-862d-0637a1c2c739][knowledge class]] whose work is exploited and lives are expropriated by communicative capitalism.
#+end_quote

Yep, it looks so.

#+begin_quote
Looked at most broadly, the demographics of recent protests point to heavy involvement by those who are young, well-educated, and un- or underemployed.
#+end_quote

#+begin_quote
That a struggle does not take the form of a classic workplace struggle, in other words, does not mean that it is not class struggle.
#+end_quote

Class struggle does not mean only workplace struggle.

#+begin_quote
Demographics and workplace struggles support the idea of the revolt of the knowledge class.
#+end_quote

#+begin_quote
The revolts of the past few years exemplify class struggle under communicative capitalism. Accepting this position entails rejecting the idea that they are primarily post-political, democratic, or strictly local movements.
#+end_quote

#+begin_quote
It entails a recognition of changes in class struggles’ mode of appearance; it looks different under conditions of distributed, precarious, and unpaid communicative labor, from how it appeared during the industrial labor movement.
#+end_quote

* Action

#+begin_quote
We would expect struggles to extend beyond the workplace, perhaps involving hacking as a kind of contemporary sabotage as well as various kinds of misuse of communicative devices.
#+end_quote

#+begin_quote
But more fundamentally given the changes in communication and subjectivity, we would expect the expropriated to face real difficulties in organization, in constructing clear narratives, and symbols.
#+end_quote
