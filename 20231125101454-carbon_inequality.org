:PROPERTIES:
:ID:       4e08f86e-d777-4e0c-a671-80bc58e34745
:END:
#+title: Carbon inequality

[[id:b8553ef1-db6f-4e71-b0e0-c5a4855b9941][Carbon]] [[id:29e21482-9612-480b-bebf-ad6fcbf96151][inequality]].

A small number of the ultra-rich (the 1%) constitute much of the world's carbon emissions.

(The next 10% also contribute too much too.)

#+begin_quote
the richest 1% of the population produced as much carbon pollution in one year as the 5 billion people who make up the poorest two-thirds

-- [[id:87c7ed1d-2c39-459e-a5da-879868ee8b07][The great carbon divide]]
#+end_quote

#+begin_quote
At the top is the wide, flat, very shallow bowl of the richest 10% of humanity, whose carbon appetite – through personal consumption, investment portfolios, and share of government subsidies and infrastructure benefits – accounts for about 50% of all emissions.

-- [[id:87c7ed1d-2c39-459e-a5da-879868ee8b07][The great carbon divide]]
#+end_quote
