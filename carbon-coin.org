:PROPERTIES:
:ID:       36f89bdf-5c57-421f-9b40-c2bd1b34dde5
:mtime:    20220830220156
:ctime:    20220830220156
:END:
#+TITLE: carbon coin
#+CREATED: [2022-08-30 Tue]
#+LAST_MODIFIED: [2022-08-30 Tue 22:04]

#+begin_quote
carbon coin. This to be a digital currency, disbursed on proof of carbon sequestration to provide carrot as well as stick, thus enticing loose global capital into virtuous actions on carbon burn reduction. Making an effective carrot of this sort would work best if the central banks backed it, or created it. A new influx of fiat money, paid into the world to reward biosphere-sustaining actions

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote

#+begin_quote
carbon coin. Noted that some environmental economists now discussing the Chen plan and its ramifications, as an aspect of commons theory and sustainability theory

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote

#+begin_quote
The Chen papers sometimes call it CQE, carbon quantitative easing.

-- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
#+end_quote
