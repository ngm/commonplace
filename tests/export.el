(require 'ert)
(require 'org)

(ert-deftest test-simple-sanity-check ()
  (should (string= "yo" "yo")))

; https://emacs.stackexchange.com/questions/50676/testing-emacs-lisp-code-involving-org-mode
(defun promote-next-heading ()
  (org-next-visible-heading 1)
  (org-metaright))

(ert-deftest test-simple-org-sanity-check ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert "
* test heading
")
      (goto-char (point-min))
      (promote-next-heading)
      (buffer-string))
    "

** test heading
")))

(ert-deftest test-simple-export-to-buffer ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert "#+TITLE: Title\n")
      (insert "* Test heading")
      (setq org-export-with-author nil)  ; Turn off author name
      (setq org-export-with-toc nil)     ; Turn off table of contents
      (org-export-to-buffer 'ascii (current-buffer))
      (buffer-string))
    "                                _______\n\n                                 TITLE\n                                _______\n\n\n1 Test heading\n==============\n" 
    )))

(ert-deftest test--default-export-with-properties ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert ":PROPERTIES:\n
:ID:    20240330103638\n
:END:\n
")
      (insert "#+TITLE: Title")
      (insert "* Test heading")
      (setq org-export-with-drawers nil)
      (setq org-export-with-author nil)  ; Turn off author name
      (setq org-export-with-toc nil)     ; Turn off table of contents
      (org-export-to-buffer 'ascii (current-buffer))
      (buffer-string))
    "                         _____________________\n\n                          TITLE* TEST HEADING\n                         _____________________\n\n\n"
    )))

(ert-deftest test--default-simple-html-export ()
  (defun org-html-template (contents info)
    "<html></html>"
    )
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert "#+TITLE: Title\n")
      (insert "* Test heading")
      (setq org-export-with-drawers nil)
      (setq org-export-with-author nil)  ; Turn off author name
      (setq org-export-with-toc nil)     ; Turn off table of contents
      (org-export-to-buffer 'html (current-buffer))
      (buffer-string))
    "<html></html>"
    )))

(defun commonplace/format-date (date-str)
  (let ((year (substring date-str 0 4))
        (month (substring date-str 4 6))
        (day (substring date-str 6 8)))
    (format "%s/%s/%s" day month year)))

(defun commonplace/org-html-template (contents info)
  (let* ((ctime-property (car (org-property-values "ctime")))
         (mtime-property (car (org-property-values "mtime")))
         (ctime-date (if ctime-property
                         (car (split-string ctime-property))
                       nil))
         (mtime-date (if mtime-property
                         (car (split-string mtime-property))
                       nil))
         )
    (concat "<html>"
            (if ctime-date (concat "ctime: " (commonplace/format-date ctime-date)))
            " "
            (if mtime-date (concat "mtime: " (commonplace/format-date mtime-date)))
            "</html>")))

(ert-deftest test--html-export-with-both-date-properties ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert ":PROPERTIES:
:ID:       9bdc71a4-aed8-44eb-920a-01968a5550fb
:mtime:    20220820104734 20211127120918 20210724222235
:ctime:    20210724222235
:END:
")
      (insert "#+TITLE: Title\n")
      (insert "* Test heading")
      (advice-add 'org-html-template :override #'commonplace/org-html-template)
      (org-export-to-buffer 'html (current-buffer))
      (buffer-string))
    "<html>ctime: 24/07/2021 mtime: 20/08/2022</html>"
    )))

(ert-deftest test--html-export-with-no-date-properties ()
  (should
   (string=
    (with-temp-buffer
      (org-mode)
      (insert ":PROPERTIES:
:ID:       9bdc71a4-aed8-44eb-920a-01968a5550fb
:END:
")
      (insert "#+TITLE: Title\n")
      (insert "* Test heading")
      (advice-add 'org-html-template :override #'commonplace/org-html-template)
      (org-export-to-buffer 'html (current-buffer))
      (buffer-string))
    "<html> </html>"
)))
