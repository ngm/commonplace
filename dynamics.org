:PROPERTIES:
:ID:       c630eecf-cc5b-48e0-a63d-d29880dc3018
:mtime:    20211127120944 20210819092701
:ctime:    20210819092701
:END:
#+TITLE: Dynamics
#+CREATED: [2021-08-19 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Dynamics is the science of how systems change over time.

How does behaviour unfold and how does it change over time.

e.g. planetary dynamics; fluid dynamics; electrical dynamics; climate dynamics; crowd dynamics; population dynamics; financial dynamics; group dynamics; dynamics of conflicts and dynamics of cooperation.
