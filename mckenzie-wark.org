:PROPERTIES:
:ID:       cbcdc3c9-42c2-4159-9682-a3192f720b61
:mtime:    20211127120948 20210724222235
:ctime:    20210724222235
:END:
#+title: McKenzie Wark

Author.

I came across [[id:cbcdc3c9-42c2-4159-9682-a3192f720b61][McKenzie Wark]] via the latest issue of [[id:4fc02abc-a97d-45c2-a1d2-c8797b294214][STIR]].  Her work on the [[id:f709307b-b37e-4eb1-b6e3-6bf5036cc8d1][commodification of information]] sounds really interesting.  Will have to check out [[id:4ae309e4-0084-42b8-9206-16ad8c3238b3][A Hacker Manifesto]] and other works.  The identification of the '[[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]]' back in 2003 sounds pretty on point.

* Books
  
- [[id:4ae309e4-0084-42b8-9206-16ad8c3238b3][A Hacker Manifesto]]
- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
