:PROPERTIES:
:ID:       25ee8a10-0783-43fb-9d22-30aa2f7d00a1
:mtime:    20220221180332
:ctime:    20220221180332
:END:
#+TITLE: General Idea of the Revolution in the Twenty-First Century
#+CREATED: [2022-02-21 Mon]
#+LAST_MODIFIED: [2022-02-21 Mon 18:05]

+ URL :: https://c4ss.org/content/9258
+ Publisher :: [[id:a5fc50a9-93f6-4eb8-b6b7-6796f8bc347d][Center for a Stateless Society]]

[[id:7fbd9f37-a2eb-4305-a924-69d4214b000a][Revolution in the twenty-first century]].

Written around the time of [[id:2df7e386-5ea3-4c83-9f06-c89980b17753][Occupy]].
