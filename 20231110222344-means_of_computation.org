:PROPERTIES:
:ID:       80e056a6-6991-4194-b3a6-e38d38510dbc
:END:
#+title: means of computation

What are the "means of computation"?

I guess it is [[id:b831ac58-9d10-4fa6-a44f-a0189242bc8a][digital technologies]], [[id:72bfc383-fb4b-489b-a71e-7de0f3e980de][ICT]].

But also things beyond that, like intellectual property?

I guess looking at it from [[id:e1680617-e291-427f-ac3c-ebd8a12f890d][the Stacks]] perspective might be wise.
