:PROPERTIES:
:ID:       1a2615bd-9efc-473f-8c62-aacb217ecaf6
:mtime:    20240512193915 20211127120807 20210724222235
:ctime:    20210724222235
:END:
#+title: Parable of the Sower
#+CREATED: [2021-04-24 Sat 13:17]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ Author :: [[id:07415344-9e5d-4964-8e3a-7ddac4609e12][Octavia E. Butler]]
  
Reading this at the moment and really liking it.  It's pretty grim, but really compellingly written.

Sower seems quite explicitly about what happens following human-induced climate collapse.

In some ways I see similarities in [[id:7bb0e8bf-1875-473a-8641-681e466ef7ab][84K]] to Parable.  In that you have corporations running emergency services, and people not able to afford them. And the very dispossessed going a bit wild and feral.  (Those with the 'flame' drug in Parable, the ragers in 84K).

* [2021-06-30 Wed] 

  The [[id:ecf3b6b0-fd84-4c43-b39a-9b963a13897d][God is Change]] stuff is interesting.  That the most universal and consistent force in the universe is that of change.  

  The flamers are interesting too.  They are sometimes presented as drug-addicted psychos.  But sometimes referenced as those who are burning the property of the rich in some kind of class struggle.

  Also interested that the end game of Lauren's religion is to leave Earth and start again out in space.  I'll see how that pans out, and I'm sure it's not the intent here, but it smacks a bit of the school of thought of 'we're fucking Earth up, let's just try again somewhere else' rather than let's fix it.  

* [2021-07-01 Thu] 

  There's slavery in the world of Parable.  [[id:a39f2ae3-2d3c-40cf-b20e-cee55498fa77][Debt slavery]].  At times parallels are drawn to the history of slavery in America.

