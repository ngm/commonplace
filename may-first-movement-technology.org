:PROPERTIES:
:ID:       9bf42d1b-2a72-4480-8d99-0303924b9b43
:mtime:    20211127120918 20210724222235
:ctime:    20210724222235
:END:
#+title: May First Movement Technology
#+CREATED: [2021-01-31 Sun 14:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

-- https://mayfirst.coop/en/

#+begin_quote
May First Movement Technology is a non-profit membership organization that engages in building movements by advancing the strategic use and collective control of technology for local struggles, global transformation, and emancipation without borders.
#+end_quote
