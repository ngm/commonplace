:PROPERTIES:
:ID:       4dc90a5b-0ed8-47f9-9776-198225ab5546
:mtime:    20211127120835 20211021202323
:ctime:    20211021202323
:END:
#+TITLE: Face off: the government versus GPs
#+CREATED: [2021-10-21 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.theguardian.com/news/audio/2021/oct/21/face-off-the-government-versus-gps

The government is playing chicken with GPs.  Ultimately to undermine the [[id:1ac681b1-22c3-4e25-9731-df84b3bd7d14][NHS]].  Part of a narrative that [[id:0fcd061a-73f3-4992-9a5f-fac2b11cfb99][Sajid Javid]] wants to push that it is to blame for any shortcomings.  A narrative aided and abetted by papers like the [[id:ac4a804b-fe5e-4fee-8762-be55ef0f5561][Daily Mail]].
