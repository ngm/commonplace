:PROPERTIES:
:ID:       ff656185-faf5-4321-a778-60fce3f631b8
:mtime:    20230611091842
:ctime:    20230611091842
:END:
#+TITLE: After the Robots: Aaron Benanav on Work, Automation and Utopia
#+CREATED: [2023-06-11 Sun]
#+LAST_MODIFIED: [2023-06-11 Sun 09:20]

+ URL :: https://novaramedia.com/2020/12/18/after-the-robots-aaron-benanav-on-work-automation-and-utopia/
+ Featuring :: [[id:93a9a7f1-8704-4a47-8c95-dab1c30ec92a][Aaron Benanav]]

