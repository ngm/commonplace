:PROPERTIES:
:ID:       a5a21918-05aa-40f6-bf21-0cf430248315
:mtime:    20221105143702 20220303224815
:ctime:    20220303224815
:END:
#+TITLE: third industrial revolution
#+CREATED: [2022-03-03 Thu]
#+LAST_MODIFIED: [2022-11-05 Sat 14:45]

#+begin_quote
The Third Industrial Revolution (3IR), also known as the [[id:dffcc016-a30d-4e4b-b26f-44232c696dd4][digital revolution]], started in the 1960s, but exploded in the late 1980s and 1990s, and is still expanding today.  This revolution refers to the advancement of technology from analog electronic and mechanical devices to the digital technologies we have now.  The main technologies of this revolution include the personal computer, the internet, and advanced information and communications technologies, like our cell phones.

-- [[id:2626df33-d085-4fae-90c5-c34536bd5fb8][Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]
#+end_quote

The beginning of the [[id:cc9dd9d9-434a-49db-91ba-d6104cedd3ea][Information Age]].

- "refers to the advancement of technology from analog electronic and mechanical devices to the digital technologies we have now"

- computers
- internet
- mobile phones
