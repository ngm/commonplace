:PROPERTIES:
:ID:       9c6a4a14-1c63-4c49-b769-a241477e5767
:mtime:    20211127120918 20210724222235
:ctime:    20200524104257
:END:
#+TITLE: Hypercore Protocol

What once was dat.

#+begin_quote
The Dat protocol has been renamed to the Hypercore protocol, to reflect the fact it has found many uses beyond the Dat project it originated in. While invented for sharing scientific data sets, Hypercore serves equally well as a foundation for many decentralised projects, from chat apps to web browsers.

-- [[https://redecentralize.org/redigest/2020/05][Redecentralize Digest — May 2020 — Redecentralize.org]]
#+end_quote
