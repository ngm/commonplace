:PROPERTIES:
:ID:       e68dca42-5fad-42c6-88a5-f74f261d2e64
:mtime:    20211127121004 20210724222235
:ctime:    20210724222235
:END:
#+title: Federated Education: New Directions in Digital Collaboration
#+CREATED: [2021-06-20 Sun 13:30]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

+ URL :: 
[[https://hapgood.us/2014/11/06/federated-education-new-directions-in-digital-collaboration/][Federated Education: New Directions in Digital Collaboration | Hapgood]]
+ Author :: [[id:e566624e-5d3b-4b96-95df-5da966b24593][Mike Caulfield]]
+ Date published :: 2014-11-06

Really good.  And interesting to note, predates [[id:81340413-1481-4376-a906-b65e32f10fef][The Garden and the Stream: A Technopastoral]].

Talks about [[id:135938b7-cae2-4c1b-aa2c-e27101626d35][learning in public]], through the lens of Arthur C. Clarke and his outlining of the idea of geostationary satellites and global positioning before they were developed by the US military.

I like this idea of the [[id:b535207a-0ccc-43f6-a288-a9db60cfb1f0][Kinneavy triangle]] as a [[id:61ceb44b-069e-470d-9123-56192c1959f7][lifecycle of information]]. Narrative (I), dialogue/persuasion (focused on the you), and [[id:747b7141-3b8a-4619-a69d-78051b1955dd][exposition]] (focused on the 'it').

#+begin_quote
An idea starts out with what it means to you, the “I” in this situation. Then it pings around a social network and is discussed (the “you” phase). And then in the final phase it sort of transcends that conversation, and becomes more expository, more timeless, less personal, more accessible to conversational outsiders.
#+end_quote

He mentions Evernote and Delicious as the I part.  Twitter and blogging as the dialogic/persuasive part.  And wiki as best as expository.

#+begin_quote
For the expository phase, it’s wiki that excels. By cracking open ideas and co-editing them, we turn these time-bound, person-bound comments into something more expansive and timeless. We get something bigger than the single point of view, smarter than any single person.
#+end_quote

In terms I'm familiar with right now, I'd say something like:

| writing    | subject | tools  | dialectics |
|------------+---------+--------+------------|
| [[id:a2ad9967-5926-4be8-b9bc-0f36e57ec746][narrative]]  | I       | garden | thesis     |
| [[id:c1edbf6b-f6e4-4206-82b5-77f6769f6192][dialogic]]   | you     | stream | antithesis |
| exposition | we/it   | agora  | synthesis  |

#+begin_quote
So one thing I’m interested is how we create a system that allows information to flow in this way. One way might be to link up Evernote, Twitter, RSS Feeds and Wiki in a certain way.
#+end_quote

#+begin_quote
Another way is to start at the end technology — in this case wiki — and look at what it would take to make it work better in the other stages, the I and the You, the personal and the dialogic.
#+end_quote

^ This is huge.  And what I think I'm going for with my site.  Well - combine the garden and the stream there.  Actually not just there.  And then the agora (or interlinking wikis, one way or another) layered on top of that.

But YES - the goal is to combine garden, stream, and agora.  The way one does that might take many forms.  My chosen form is some kind of indieweb / fediverse / agora mashup.
