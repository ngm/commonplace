:PROPERTIES:
:ID:       fed3af82-d31b-4f7e-9ff2-3274b5f7401e
:mtime:    20221113162209 20220807161247 20220526125140 20220402094149 20211127121018 20210724222235
:ctime:    20200308230733
:END:
#+TITLE: right to repair

#+begin_quote
the three pillars of the right to repair, which are: 1. products need to be designed for repairability; 2. spare parts and repair services need to be affordable; and 3. people should have access to the information they need to carry out repairs.

-- [[id:02833094-e6e5-41a2-b854-a244f3c12513][The UK’s new 'right to repair' is not a right to repair]]
#+end_quote

#+begin_quote
The availability of repair info is going to be critical for riding out a long-term crisis, both in terms of increasing the longevity of devices & minimizing the amount of travel that "approved technicians" need to make. Give people the resources they need to make repairs locally.

-- https://twitter.com/mcforelle/status/1240637926935089155
#+end_quote
* In Europe

- [[id:f1c0dea7-d5a4-4d7a-8c7d-4dd8c845496e][Right to Repair in Europe]]
 
* UK

- [[id:8d1c73e2-6ee0-4f39-a75e-bff499e8ba69][Right to Repair in the UK]]
  
