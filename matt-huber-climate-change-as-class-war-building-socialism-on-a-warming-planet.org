:PROPERTIES:
:ID:       f0d2a10e-2ceb-4e0a-9777-0c088e2d3325
:mtime:    20220903182051 20220704212237 20220703162812 20220702191628
:ctime:    20220702191628
:END:
#+TITLE: Matt Huber, "Climate Change as Class War: Building Socialism on a Warming Planet"
#+CREATED: [2022-07-02 Sat]
#+LAST_MODIFIED: [2022-09-03 Sat 18:21]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://revolutionaryleftradio.libsyn.com/climate-change-as-class-war
+ Featuring :: [[id:e1e50dce-7727-4f48-8939-8ec843e8f0a0][Matthew Huber]] / [[id:16cc5364-20fd-449e-b4a7-f975051fc0c2][Breht O'Shea]]

Based around the book [[id:4ab7e432-2182-4651-bf7d-c0f0805ab970][Climate Change as Class War: Building Socialism on a Warming Planet]].

[[id:dea564a6-cfcb-4937-bfdd-3d678e3e0106][Marxism]]. [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][eco-socialism]].

Emphasis on [[id:5c06724b-1bbe-42f7-b611-896c83222936][class struggle]], structure building, [[id:14be0045-2209-4ea0-829a-58659f2624f0][trade unionism]].

Similar to Richard Seymour, mentions [[id:abdb6222-bf1d-49ce-b472-fc33ae57b21a][carbon footprint]] as problematic. (Invented by BP I think he said?)

Environmentalism currently as generally the preserve of the [[id:f1e20e95-e8a3-4041-aa65-b55583092669][professional-managerial class]]. Needs to appeal to working class. A lot of degrowth and rationing stuff doesn't. PM class likes that sort of thing because they are already at a reasonable level of material comfort, so losing a bit doesn't hurt, and makes them feel good.

Lack of appeal for working class makes it possible for right wingers to claim that environmental policies are *against* the working class. Makes me thing of [[id:8719759a-0a84-434b-87a7-e0c3f569e060][Net Zero Scrutiny Group]] for example.

~01:07:40  Decarbonise electricity, then electrify everything.


