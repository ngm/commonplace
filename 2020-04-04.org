:PROPERTIES:
:ID:       b92976e9-df51-45d7-b37c-1bd9c69dc984
:mtime:    20211127120936 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-04-04

* [[id:04845f29-d619-4459-aa3f-4b89c5b88c48][Separation of article concerns]]
  
I just wrote a big ol' blog post about [[id:243dc6e6-a3af-4ddb-8eba-be321da8e4a5][indiewebifying my event discovery and RSVPs]].  Thinking about it just now, however, it's a bit of a mish-mash between *why* I wanted to do it, and *how* I did it.

For someone coming to the post who is new to the IndieWeb, it's probably bit off-putting (and maybe fuel for the fire of 'IndieWeb is too complicated').  And for someone who already knows about the IndieWeb, but isn't using WordPress, they might skip over the hows and in the process miss some of the whys.

So in future I might try and split these kinds of articles into two - a 'why' post, and a 'how I did it' post.  The 'why' post will kind of be my [[id:3ba3c6af-9dde-4c44-af8b-7186eb85934d][behaviour-driven development]] specs, so to speak, and probably mostly links to various pattern pages on the IndieWeb wiki.  And the 'how' post will get into the weeds of one very specific implementation, liberally referring back to the 'why' post.

I think that would work well and make the articles a bit more reusable and less niche.

* [[id:ea2360f5-de34-4a2e-a507-783c5396f96c][Cultural references]]
  
Been doing a small bit of wiki gardening on my [[id:5f333662-2e97-4ede-88bc-b0e312fc7b6a][books]] pages today.  Nothing major, just starting to link the different ideas from different books to the concepts they're discussing.  

I really like how [[https://nadiaeghbal.com/][Nadia Eghbal]]'s writings are peppered with cultural references.  When I'm getting to the point where I'm writing longer-form articles on an idea, I would like them to have a similar cross-pollination vibe.  Like [[https://k-punk.org][Mark Fisher]], too.

* [[id:c5b2143a-75b9-438a-a02f-b158909305b5][Wiki graphs]]

[[id:20210326T232003.148801][org-roam]] has a nice feature that lets you [[https://org-roam.readthedocs.io/en/develop/tour/#exporting-the-graph][graph the notes in your wiki]] and the links between them.  I just saw that there's a pull request to [[https://github.com/jethrokuan/org-roam/pull/398][produce that map for the current note]].  

When that lands, I'd like to try and hook up my [[id:bc94cd9c-f9f6-4029-a351-71d4f415af70][publish]] step to add the note-specific graph to each published page.  That'd give a navigation path something like the one in [[id:363b6ef6-b565-450e-87f6-820daeca653e][FedWiki]]:

#+ATTR_HTML: :width 100%
[[file:Wiki_graphs/2020-04-04_09-39-41_screenshot.png]]

Though I would still want my own curated [[id:0b207b9d-480d-4709-9746-212dc53a626c][paths]] in addition to this generated map o' everything.

* Watched [[https://www.youtube.com/watch?v=Lg61ocfxk3c][Making Connections in your Notes]]
  
Fairly high-level intro to [[id:20210326T232003.148801][org-roam]], and why you might want to use it instead of Roam (and also why you might not).

* [[id:1ab6b61e-e5f6-4c01-bd4a-e66566983974][Stream-first]]
  
The [[id:0ba0c8ce-613b-4145-bbb3-e952c4fa41e8][Roam]] approach to [[id:abcd71aa-9a1f-4df3-b637-7c05ab1416fb][note-taking]] is to start with your daily page, and then link to things from there.  This makes a lot of sense to me, and fits in with my [[id:5777bbd3-dec4-4e13-aa9c-68b947f58047][current process]] for the [[id:ffbea278-7011-49d6-8c1e-c784d40abb6b][blog and wiki combo]].  

You just start with whatever is currently on your mind, and that goes in the stream, but links out to things in the wiki.  I tend to copy it into the wiki at the same time, too, if it makes sense.  Probably one thing to think about though is what in the stream do I want to be [[id:88de9943-00c5-4d30-a81b-b6b35fbff23a][public versus private]] - that would change the workflow a bit.
