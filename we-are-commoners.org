:PROPERTIES:
:ID:       0ead3e7e-6ea9-44a6-92aa-a2527043e17c
:mtime:    20211127120802 20210724222235
:ctime:    20210724222235
:END:
#+title: We are Commoners
#+CREATED: [2021-06-17 Thu 20:28]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://craftspace.co.uk/wearecommoners/

#+begin_quote
This exhibition invites you to become or recognise yourself as a ‘Commoner’. Featuring UK and international artists, the projects exhibited in this exhibition represent ideas and resources to inspire acts of commoning.
#+end_quote
