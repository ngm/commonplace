:PROPERTIES:
:ID:       4e9bb0b2-fd07-470b-8cda-8b6da6dce31f
:mtime:    20211127120835 20210724222235
:ctime:    20210724222235
:END:
#+title: The Judgement of Paris: Facebook vs. the Communards
#+CREATED: [2021-03-31 Wed 22:17]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

- [[id:c2ccadb7-581f-4d00-9d58-42cc16783dbb][Lizzie O'Shea]]
- https://thebaffler.com/salvos/the-judgment-of-paris-oshea

  I like Lizzie O'Shea's writing, e.g. [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]]. I like the link to left-wing movements from the past. I do find them a little lacking in practical application though.  There are a lot of 'what if' questions, what if we did this the way the Communards did it, etc, without really fleshing out what that means in practice in the present.

#+begin_quote
The city was transformed into an autonomously organized society, a commune that experimented with alternative ways of structuring social and political life based on collaboration and cooperation
#+end_quote

#+begin_quote
Art, as a social institution, was reimagined as open and belonging to the public, rather than a curated, officially sanctioned set of rules about aesthetics
#+end_quote

#+begin_quote
“Solidarity grows through increasing liberty, not through constraint or obligation,” writes Ross. “Personal autonomy and social solidarity do not oppose each other, but instead reinforce each other.” In an age in which online spaces feel more divisive and polarized than ever, perhaps it is time to ponder how we can create conditions of personal autonomy that give rise to greater social solidarity
#+end_quote

#+begin_quote
The left is more dependent than ever on these digital platforms, and to trust executives to make calls about content moderation leaves activists vulnerable
#+end_quote

#+begin_quote
the company preferred to focus on global activity that posed public relations risks, rather than issues of electoral or civic harm
#+end_quote

#+begin_quote
The central problem is that Facebook has been charged with resolving philosophical conundrums despite being temperamentally ill-qualified and structurally unmotivated to do so
#+end_quote

#+begin_quote
If nudity can be artistic, exploitative, smutty, and empowering, then the depiction of violence can be about hate and accountability. A video of a shooting can be an expression of deadly bigotry, but it can also expose police wrongdoing. Distinguishing between them requires human decision-making, and resolving a range of contested ideas. At present, private institutions bear significant responsibility for defining the boundaries of acceptability, and they are not very good at it.
#+end_quote

#+begin_quote
The story of Courbet’s painting illuminates how the best decisions around content moderation are those that involve public deliberation, not carefully calibrated algorithms with their implicit biases. We need social platforms to offer cultural and political experiences that are not placating or addictive, but rather are surprising, ambiguous, challenging, and educational.
#+end_quote
