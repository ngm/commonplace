:PROPERTIES:
:ID:       2c89cdfc-82bf-4fb8-ba32-0065c02c4a74
:mtime:    20220604203433
:ctime:    20220604203433
:END:
#+TITLE: interfacer
#+CREATED: [2022-06-04 Sat]
#+LAST_MODIFIED: [2022-06-04 Sat 20:34]

Digital Infrastructure for Fab Cities, Regions and Nations

#+begin_quote
Fab(rication) City concept of distributed production of open-source hardware and distributed design processes for building a digital infrastructure enabling ecological, economic and social sustainability. In cooperation with FabCity.Hamburg.
#+end_quote

[[id:83988816-9a82-430d-8e98-cc877101ec6d][dyne.org]]
