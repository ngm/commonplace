:PROPERTIES:
:ID:       2446d0dd-825d-44a7-b620-0daacdd73a35
:mtime:    20211127120813 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Evergreen notes

#+begin_quote
Evergreen notes are written and organized to evolve, contribute, and accumulate over time, across projects.

-- [[https://notes.andymatuschak.org/Evergreen_notes][Evergreen notes]] 
#+end_quote

I first came across this term on Andy Matuschak's notes: 
https://notes.andymatuschak.org/Evergreen_notes.  Andy coined the term, in this context at least.

#+begin_quote
Andy Matuschak proposed the term Evergreen Notes to describe a system of note-taking that aspires towards cumulative personal knowledge, rather than simply information capture.

- [[https://maggieappleton.com/evergreens][Growing the Evergreens]] 
#+end_quote

* Resources

- [[https://maggieappleton.com/evergreens][Growing the Evergreens]] 
