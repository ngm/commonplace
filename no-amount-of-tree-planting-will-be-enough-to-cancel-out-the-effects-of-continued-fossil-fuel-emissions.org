:PROPERTIES:
:ID:       e27cd691-9b6a-41c2-a0d3-8f74198dbd04
:mtime:    20220406214026
:ctime:    20220406214026
:END:
#+TITLE: No amount of tree planting will be enough to cancel out the effects of continued fossil fuel emissions
#+CREATED: [2022-04-06 Wed]
#+LAST_MODIFIED: [2022-04-06 Wed 21:40]

[[id:37748bf5-f947-4e2e-b4b6-87f9723af5b6][Tree planting]].
