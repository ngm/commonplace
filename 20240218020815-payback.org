:PROPERTIES:
:ID:       ce762203-1a0a-485a-aed5-c805ce964dbc
:END:
#+title: Payback

#+begin_quote
Margaret Atwood’s Payback, which I recommend unreservedly as perhaps the best, and most entertaining, book ever written on [[id:17ce80df-5f2a-4000-acf7-044fdfb50b4d][debt]]

-- [[id:3c1d0b2a-2c14-4f23-ad30-8e418f69a6ed][Talking to My Daughter About the Economy]]
#+end_quote
