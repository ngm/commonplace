:PROPERTIES:
:ID:       eb441741-1cd9-44ff-aef4-8b3554a47faf
:mtime:    20220812161605 20220526114909 20220226095823 20211127121007 20211120200235
:ctime:    20200523153554
:END:
#+TITLE: Revolutionary socialism

I would class myself as a revolutionary socialist.  

Just gonna quote Wikipedia extensively here:

#+begin_quote
Revolutionary socialism is the [[id:1574c602-e123-4f31-afc4-31b92e874e49][socialist]] doctrine that [[id:0399477c-a9ed-43fb-bce8-47e16904618f][social revolution is necessary in order to bring about structural changes to society]]. More specifically, it is the view that [[id:8c0df211-cacd-4320-8a1b-b7b993f753c0][revolution is a necessary precondition for a transition from capitalism to socialism]].
#+end_quote

Important to point out revolution doesn't explicitly mean armed uprising.  Unclear where I stand on the exact nature of the revolution. Not a fan of violent insurrection, but some do make strong argument that without it, your revolution is doomed.  So.  There's that.

#+begin_quote
 Revolution is not necessarily defined as a violent insurrection; it is defined as seizure of political power by mass movements of the working class so that the state is directly controlled or abolished by the working class as opposed to the capitalist class and its interests.
#+end_quote

All good so far.

#+begin_quote
Revolutionary socialists believe such a state of affairs is a precondition for establishing socialism and orthodox Marxists believe that it is inevitable but not predetermined.
#+end_quote

Fair.

#+begin_quote
Revolutionary socialism encompasses multiple political and social movements that may define "revolution" differently from one another. These include movements based on orthodox Marxist theory, such as De Leonism, impossibilism, and Luxemburgism; as well as movements based on Leninism and the theory of vanguardist-led revolution, such as Maoism, Marxism–Leninism, and Trotskyism. Revolutionary socialism also includes non-Marxist movements, such as those found in anarchism, revolutionary syndicalism, and democratic socialism.
#+end_quote

Yeah, so this is where I'm not 100% sure of my stripes.  I see plenty of problems with vanguardism, and tend to lean towards anarchism, but really not well versed enough in all the theory to pick a tendency and wave my flag for it.  Happy to be somewhat cross-cutting, too, if that's where I sit.

See also: [[id:b89e870d-071a-42d8-95e1-0c9b36bff6b0][logics of resistance]].

#+begin_quote
It is used in contrast to the reformism of social democracy and other evolutionary approaches to socialism. Revolutionary socialism is opposed to social movements that seek to gradually ameliorate the economic and social problems of capitalism through political reform. 
#+end_quote

#+begin_quote
If revolutionary socialism proposes that state power should be seized so that capitalism can be smashed, and social democracy argues that the capitalist state should be used to tame capitalism, anarchists have generally argued that the state should be avoided — perhaps even ignored — because in the end it can only serve as a machine of domination, not liberation.

-- [[https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/][How to Be an Anticapitalist Today]] , Erik Olin Wright
#+end_quote

I'm confused - suggests that revolutionary socialism is explicitly about seizing and smashing.  Maybe Wright isn't necessarily equating 'smashing' with violent insurrection.

* Context

Some snippets from an ep of Rev Left [[https://revolutionaryleftradio.libsyn.com/humanism][podcast]]:

- Some societies, like the Russian revolution, had identifiable targets of power that could be overthrown.  You knew who they were, where there buildings were.  You can take a more vanguard approach and a small group overthrow these targets.  This is what the Bolsheviks did.

- In other places and times, that's not so clear cut.  Maybe then you need to movement build and raise class consciousness for a popular uprising.

* Struggle
  
  #+begin_quote
  The powerful few may kill one, two, or three roses, but they can never stop the spring from coming. And our struggle is for the spring.
  
-- [[https://tribunemag.co.uk/2019/11/lula-livre/][Lula Livre]], Tribune
  #+end_quote
”
* Misc

#+begin_quote
the holy trinity of socialism: [[id:0a0fe7fd-8cf8-4bbc-866c-888606a08786][educate, agitate, organise]]

-- [[https://www.weareplanc.org/blog/none-of-us-cracked-the-corbyn-moment-but-comrades-we-can-all-build-on-it/][None of Us Cracked the Corbyn Moment – but, Comrades, We Can All Build On It]] 
#+end_quote
