:PROPERTIES:
:ID:       32eea82b-ae67-402b-865e-b596c1a956ff
:mtime:    20221106122349
:ctime:    20221106122349
:END:
#+TITLE: planetary-scale computation
#+CREATED: [2022-11-06 Sun]
#+LAST_MODIFIED: [2022-11-06 Sun 12:51]

#+begin_quote
Planetaryscale computation” describes the shifting nature of cloud computation by the use of distributed systems. Production is no longer fixed to one location or to single pieces of machinery. Instead, it is realised through a flow of computational procedures within a distributed global infrastructure. Thus, planetary-scale computation represents a historically novel ‘skin’ enveloping the planet, aimed at recording, measuring, and facilitating human and non-human processes in the form of data. Data become the central means of mediation and coordination in economies worldwide, leading to a transformation of their institutional forms and their agents’ patterns of behaviour and practices.

-- [[id:a0da7876-5d03-4efb-8f7f-4fbfb91ed234][The Stack as an Integrative Model of Global Capitalism]]
#+end_quote

#+begin_quote
From a political-economic perspective, planetary-scale computation represents a complex economic phenomenon. It involves a modular global production structure concerning interdependent feedback between software (knowledge as means of production) and hardware (material accumulation).

-- [[id:a0da7876-5d03-4efb-8f7f-4fbfb91ed234][The Stack as an Integrative Model of Global Capitalism]]
#+end_quote
