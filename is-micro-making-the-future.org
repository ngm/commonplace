:PROPERTIES:
:ID:       446671ef-c353-49c0-9730-801b37c53cf0
:mtime:    20220421205740
:ctime:    20220421205740
:END:
#+TITLE: Is Micro Making The Future?
#+CREATED: [2022-04-21 Thu]
#+LAST_MODIFIED: [2022-04-21 Thu 20:58]

+ URL :: 
https://www.rettswood.com/is-micro-making-the-future-for-wallpaper-magazine

A scan of a print article?

[[id:8739a121-4895-4b25-ba23-71469ffd107a][3D printing]]. [[id:dfd88f59-61bd-4e58-91cf-38b0f3c8a325][decentralised manufacture]].
