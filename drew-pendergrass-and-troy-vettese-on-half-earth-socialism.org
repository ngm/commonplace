:PROPERTIES:
:ID:       1c420e80-fcf9-421d-a0da-8df5809a737c
:mtime:    20220716110251 20220715224558
:ctime:    20220715224558
:END:
#+TITLE: Drew Pendergrass and Troy Vettese on Half Earth Socialism
#+CREATED: [2022-07-15 Fri]
#+LAST_MODIFIED: [2022-07-16 Sat 11:29]

+ URL :: https://www.futurehistories.today/episoden-blog/s02/e18-drew-pendergrass-and-troy-vettese-on-half-earth-socialism/

Another good interview with the [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]] authors.

This one has a strong focus on the planning aspects of it.

~00:02:18  Nice summary definition of Half-Earth Socialism.

~00:03:50 Critiques of [[id:b60ee88d-b3ad-4b37-923b-b6be7381cc55][BECCS]], [[id:f5b88483-be1a-4e03-a153-990920945688][nuclear power]], and [[id:f4c76bf6-24f5-4032-a2de-034f24d11fef][Half-Earth]].

~00:30:09  Some good overview here on how the planning might actually work.

