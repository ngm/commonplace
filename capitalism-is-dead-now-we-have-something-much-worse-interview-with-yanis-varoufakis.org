:PROPERTIES:
:ID:       11693185-4562-450a-96db-faa697bd4fe3
:mtime:    20231007120641
:ctime:    20231007120641
:END:
#+TITLE: 'Capitalism is dead. Now we have something much worse': interview with Yanis Varoufakis
#+CREATED: [2023-10-07 Sat]
#+LAST_MODIFIED: [2023-10-07 Sat 12:09]

+ URL :: https://www.theguardian.com/world/2023/sep/24/yanis-varoufakis-technofeudalism-capitalism-ukraine-interview

Interview with [[id:12357f53-8bb2-497f-86f6-00b6353a44ac][Yanis Varoufakis]].

I wonder if he's read [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]?  He is calling the new worse thing [[id:ac0e4d66-7b99-444b-86b4-4b4d986c833c][technofeudalism]].
