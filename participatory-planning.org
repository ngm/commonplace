:PROPERTIES:
:ID:       97418a34-6229-406b-9947-ac61c58f742d
:mtime:    20221216155045
:ctime:    20221216155045
:END:
#+TITLE: Participatory planning
#+CREATED: [2022-12-16 Fri]
#+LAST_MODIFIED: [2022-12-16 Fri 15:55]

maybe merge with see [[id:c15efb85-a454-455e-93f2-72bb2b339529][Participatory economics]], [[id:04c1c125-b6a3-4269-a1ec-309f1ab074a2][Participatory budgeting]]?

#+begin_quote
we arrive at a broader notion of participatory planning, a system through which different associations engage in negotiated coordination as to how resources could be most effectively allocated between them.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
Participatory planning today can also take advantage of the mass participatory tools enabled through digital technology that could help facilitate this process and make coordination easier. Evgeny Morozov has written about the technological possibilities of today’s ‘feedback infrastructure’, which make it possible to bring different parties together to help distribute information about economic needs through non-market forms of social coordination.

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
Responding to Hayek’s theory about markets as the best discovery mechanism, Morozov proposes a form of ‘solidarity as a discovery mechanism’ through the design of new social institutions that could coordinate economic activity more effectively than the market. Digital platforms make it easier than ever before to examine new ways of distributing resources through democratic methods

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

