:PROPERTIES:
:ID:       2e57bd3c-73bb-4d80-85d4-215edad5412f
:mtime:    20220618162734
:ctime:    20220618162734
:END:
#+TITLE: Software
#+CREATED: [2022-06-18 Sat]
#+LAST_MODIFIED: [2022-06-18 Sat 16:28]

 #+begin_quote
Software has become an indispensable resource of modern societies. Whether industrial production, science, public administration, our media consumption or even our everyday communication: almost all areas are now permeated by software. A modern society without the use of software no longer seems conceivable. The existence of and access to software thus becomes a prerequisite for modern social organisation and functioning.

 -- [[id:8b811b25-4a6f-456c-9116-378734ab88f3][On the Sustainability of Free Software]]
 #+end_quote

#+begin_quote
 But not only our social organization is based on software, so are our machines and our tools as well. Hardware needs software to function and vice versa. All machines and automated systems around us - whether at home, at work, or in public infrastructure - need software as an indispensable resource to do whatever they were designed to do.

 -- [[id:8b811b25-4a6f-456c-9116-378734ab88f3][On the Sustainability of Free Software]]
 #+end_quote

#+begin_quote
Both dependencies – the one on the functioning of our machines and the one on the functioning of our social organisation - together make software indispensable for modern, networked societies. 

 -- [[id:8b811b25-4a6f-456c-9116-378734ab88f3][On the Sustainability of Free Software]]
#+end_quote
