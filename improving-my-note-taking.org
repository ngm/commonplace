:PROPERTIES:
:ID:       5fa08c53-3793-4eda-83c2-47ee672324e1
:mtime:    20211127120844 20210724222235
:ctime:    20210724222235
:END:
#+title: Improving my note-taking
#+CREATED: [2021-04-04 Sun 08:40]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

I take notes on articles but it feels a bit of a piecemeal process at present.  I'd like to improve it.  [[id:2591f574-0cc0-4447-b147-5773413db33e][How To Take Smart Notes]], [[id:ae66efe9-667d-4f51-b56a-b2d46e2a99f7][Building a second brain]] et al could probably help.

* Context

  Sometimes I read articles on my laptop.  Sometimes on my phone.  Sometimes on my Kobo.  The process is going to be slightly different for each.

* Current
  
When I'm reading articles at the moment on my laptop I create a page named after the article.  I put some metadata about the article, and then if present, the article's own summary of itself at the top.  I find it useful to have this in mind when reading.  I'll try to write a short idea about what I expect from the article too, to also help frame my reading of it.

As I'm reading through, I copy quotes that stand out to me.  If they're related to some other topic, I'll put them in to that topic's page, with a link back to the page for the article.  This way, I see all the pages that are referencing this article in the backlinks buffer, which is quite often a handy overview of the topics it covers.  These type of quotes are usually related to the setting of the scene of the article - they are defining their terms, and if I find those definitions useful, I'm pasting them in to the pages I have that also define those terms.

I've found this quite handy, but it probably goes against [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]] a bit, where you have all your quotes collected together.  Maybe if I transcluded them in to the page.  That said, if they are just for terms definition, then it probably doesn't matter that much.

Sometimes I'll also copy quotes I'm interested in to the page specifically about the article, if they don't seem to sit anywhere else.  This usually suggests that they are this article's unique point.  

When I finish the article, I'll try to make a short summary.  I could definitely do this better, and this is where [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]] could help.  I'll quite often share this summary or a version of it on social media.
