:PROPERTIES:
:ID:       658de5bb-5214-47f0-9ef6-77ef66bd66ad
:mtime:    20240320081436
:ctime:    20240318215218
:END:
#+title: Black Box: Prologue: The collision

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ Part of :: [[id:10f0b227-64dd-4aa1-8ab4-c4c3afb68338][Black Box]]

Woman begins dating an [[id:2e1f998d-e505-4f42-8d13-3400a023a514][AI]], finds genuine positivity from it. She suffers from CFS/ME and has to shield from COVID.

Presenter talks about '[[id:909bea94-f94d-4160-9aff-57f040ff5619][AI vertigo]]' - dizziness when trying to comprehend what is coming with AI. The 'collision' of the title refers to the collision between artificial intelligence and us humans. We being the first generations to truly experience it.
