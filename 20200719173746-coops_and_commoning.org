:PROPERTIES:
:ID:       232f76b5-2ae9-4c5a-b9d5-8aea675f1777
:mtime:    20220201193655 20211127120812 20210724222235
:ctime:    20200719173746
:END:
#+title: Coops and commoning
#+begin_quote
[...] [[id:ca6a27d0-3025-4fe1-a72a-264bf9587ffa][co-ops]] formalized the ancestral practice of [[id:0cb7cb18-cbb2-4060-bf7b-4ef6497dc375][commoning]] into applicable legal structures offering islands of resistance in capitalist markets.

-- [[id:ecceb327-8788-47b6-af9a-33c65e1f27d2][DisCO Manifesto]] 
#+end_quote

Coops as a way to make money is a positive way under capitalism?  Commoning as way to change the system long-term?

