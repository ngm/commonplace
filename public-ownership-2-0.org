:PROPERTIES:
:ID:       437fd660-a01c-4ff7-96a7-4801d1335901
:mtime:    20220816171253
:ctime:    20220816171253
:END:
#+TITLE: Public ownership 2.0
#+CREATED: [2022-08-16 Tue]
#+LAST_MODIFIED: [2022-08-16 Tue 17:41]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://neweconomics.org/2019/02/weekly-economics-podcast-public-ownership-2.0

[[id:52a0b7a0-b65c-486c-8166-969d9b273279][public ownership]].

+ ~00:05:00  There was a lack of democratic and public purpose in some of the big post-war nationalised industries.  (NHS perhaps an exception?)
+ ~00:10:51  [[id:f0243d58-8b00-40a0-896e-029bb1f5d9b3][New Public Management]] - public services but run in a way akin to private sector management.  Not good.

