:PROPERTIES:
:ID:       5a016697-57a7-4f5f-a890-cef53ee51a24
:mtime:    20220526120915
:ctime:    20220526120915
:END:
#+TITLE: animal husbandry
#+CREATED: [2022-05-26 Thu]
#+LAST_MODIFIED: [2022-05-26 Thu 12:32]

#+begin_quote
The food we eat masks so much cruelty. The fact that we can sit down and eat a piece of chicken without thinking about the horrendous conditions under which chickens are industrially bred in this country is a sign of the dangers of capitalism, of how capitalism has colonized our minds. We look no further than the commodity itself. We refuse to understand the relationships that underlie the commodities that we use on a daily basis

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
Instead of worrying that animal husbandry could spark foreign wars, More lambasted its role in the internal colonization of England. In his day, wool merchants turned ‘meek and tame’ sheep into monsters that ‘consume, destroy, and devour whole fields, houses, and cities

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
husbandry and early capitalism, a relationship that would become more obvious by the end of the century

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
Animal husbandry takes up 4 billion hectares – 40 per cent of Earth’s inhabitable land

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
