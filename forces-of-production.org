:PROPERTIES:
:ID:       8cd5d652-6206-4141-b9d5-e053fd6cefb3
:mtime:    20240420101905 20230520103519 20230514175006 20230424205215
:ctime:    20230424205215
:END:
#+TITLE: forces of production
#+CREATED: [2023-04-24 Mon]
#+LAST_MODIFIED: [2023-05-20 Sat 10:35]

#+begin_quote
refers to the combination of the means of labor (tools, machinery, land, infrastructure, and so on) with human labour power.

-- [[https://en.wikipedia.org/wiki/Productive_forces][Productive forces - Wikipedia]] 
#+end_quote

Quite concomitant to a broad concept of simply '[[id:30397894-746d-4c0d-922f-a5e816ae9cf7][technology]]', but:

#+begin_quote
Looking closely at the forces of production is not quite the same thing as the study of technology. The difference is that the former asks questions about [[id:47b5a145-cfcd-433a-86af-87139e2578f3][agency]], and in particular [[id:de558615-bff9-46d4-ab33-963d8dbe8000][class agency]].

-- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
#+end_quote
