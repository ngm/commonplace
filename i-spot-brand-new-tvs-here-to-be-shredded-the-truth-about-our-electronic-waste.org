:PROPERTIES:
:ID:       9315fab6-6dfa-4b14-9e50-b605b4c46901
:mtime:    20230702163727 20230630165121
:ctime:    20230630165121
:END:
#+TITLE: 'I spot brand new TVs, here to be shredded': the truth about our electronic waste
#+CREATED: [2023-06-30 Fri]
#+LAST_MODIFIED: [2023-07-02 Sun 16:40]

+ URL :: https://www.theguardian.com/environment/2023/jun/03/i-spot-brand-new-tvs-here-to-be-shredded-the-truth-about-our-electronic-waste

Expensive, fully-working electronic and electrical items shredded, for no good reason.  The problem of [[id:ec831cf9-bbfb-4f3a-bad3-53cc59707c72][e-waste]] and [[id:953aaec6-d1be-491a-9a91-aeabcf31f2c5][consumption]], counter to [[id:5380f9c6-e7a4-49de-8a9f-72b3f97100df][planetary stability]], at the hardware layer of the stack.
