:PROPERTIES:
:ID:       f60eef76-c360-452f-bf55-860207871c4b
:mtime:    20211127121013 20210724222235
:ctime:    20200509085738
:END:
#+TITLE: cyberflâneur

A [[id:289b77dd-4c57-4c8f-94fb-13abee4cdcfe][flâneur]] on the web. 

- https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/
- https://www.nytimes.com/2012/02/05/opinion/sunday/the-death-of-the-cyberflaneur.html
- https://www.theatlantic.com/technology/archive/2012/02/the-life-of-the-cyberfl-neur/252687/
- https://the-syllabus.com/cyberflaneur/
