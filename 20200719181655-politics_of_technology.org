:PROPERTIES:
:ID:       527d3e09-f4bf-432a-9318-9762664a865a
:mtime:    20211127120837 20210724222235
:ctime:    20200719181655
:END:
#+title: politics of technology

[[id:30397894-746d-4c0d-922f-a5e816ae9cf7][Technology]].  [[id:412bfe28-1785-481b-9b92-74243583f26d][Politics]].

[[id:98822387-9731-4f34-bb97-5971f627b1a2][Technology should be liberatory]].
[[id:46ebd2a2-812f-42e5-9d3c-ebee6a1ef1d7][Technological decentralisation]].
[[id:62eebbf1-7f44-4be1-bdf1-ad7ec8118dbc][digital self-determination]].

* Misc
  - [[id:1cd41bcf-e2b5-4b30-bdbf-654bf3d8603e][Digital parties]]
