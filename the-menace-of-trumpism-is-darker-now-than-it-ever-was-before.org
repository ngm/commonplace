:PROPERTIES:
:ID:       60702f21-f1dc-466a-9f25-8d40faad3b0d
:mtime:    20220108124039
:ctime:    20220108124039
:END:
#+TITLE: The menace of Trumpism is darker now than it ever was before
#+CREATED: [2022-01-08 Sat]
#+LAST_MODIFIED: [2022-01-08 Sat 12:48]

So sayeth this article in early 2022 - [[https://www.theguardian.com/commentisfree/2022/jan/07/trump-biden-republicans-election-lies-midterms][The Trump menace is darker than ever – and he’s snapping at Biden’s heels | J...]] 

Fucking depressing if true.

[[id:3cd2777d-7004-4b84-ad74-ffc98cc9beef][Donald Trump]], [[id:da9dc506-b38f-49de-a8b7-c2c510d58bcb][Trumpism]]

* Because

+ Joe Biden isn't popular
  + Biden has the lowest approval rating of any US president at this stage of his term (barring Trump himself)
+ Republican politicians fear Trump and they fear his supporters
+ Two-thirds of Republicans still think the 2020 election was rigged
+ Republican-run states are rewriting electoral law to make it harder to vote

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:a7800287-cff2-458c-abdd-873f2c05c944][Ask again later]]

I don't know, but the article seems well reasoned.  Sigh.
