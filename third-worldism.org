:PROPERTIES:
:ID:       60c219fe-a931-4c3c-a20a-122da0084ba5
:mtime:    20221111120831
:ctime:    20221111120831
:END:
#+TITLE: Third Worldism
#+CREATED: [2022-11-11 Fri]
#+LAST_MODIFIED: [2022-11-11 Fri 12:08]

#+begin_quote
However, the majority of proponents typically argue for the centrality of anti-imperialism to the victory of global communist revolution as well as against the idea that the working class in the First World is majority-exploited (sometimes arguing that it experiences no exploitation at all) and therefore it is not a part of the international proletariat.

-- https://en.wikipedia.org/wiki/Third-Worldism
#+end_quote
