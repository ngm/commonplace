:PROPERTIES:
:ID:       b330b154-99c2-46d2-87d6-bf4bb0027e90
:mtime:    20230611012914 20211127120933 20210818091509
:ctime:    20210818091509
:END:
#+TITLE: self-organisation
#+CREATED: [2021-08-18 Wed]
#+LAST_MODIFIED: [2023-06-11 Sun 01:29]

#+begin_quote
By confronting a common enemy in neoliberalism, environment and society are united in a common cause.  The principle of self-organisation must be salvaged from the laissez-faire perversion, and restored in a more authentic form: reversing the erosion of commons, and instead expanding them, in their role as stewards of nature.

-- [[id:7b2a9c52-fb28-4add-a86b-74ceb6aeaa49][What is wrong with a system of laissez-faire economics?]]
#+end_quote
