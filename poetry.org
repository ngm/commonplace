:PROPERTIES:
:ID:       8a9f84c2-4cbb-48b2-9f86-ad5e7ad147ba
:mtime:    20211127120908 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Poems

Some poems that I've liked.

- [[id:4ceb1daf-8e92-42d8-bacd-f9d7bacbdded][All Watched Over by Machines of Loving Grace, Richard Brautigan]]
- [[id:a4da160e-d766-452a-bc7b-054a01de8959][To Posterity, Bertolt Brecht]]
- [[id:d0651178-0732-4009-9d03-a6f27bc53da3][East Coker, T.S. Eliot]]

