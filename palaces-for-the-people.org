:PROPERTIES:
:ID:       eef99ad4-3249-4ac9-a84f-359c55d2c1da
:mtime:    20211227211827
:ctime:    20211227211827
:END:
#+TITLE: Palaces for the people
#+CREATED: [2021-12-27 Mon]
#+LAST_MODIFIED: [2021-12-27 Mon 21:27]

A name given by [[id:f6914175-d3ac-461d-9109-60d990b0a6bb][Andrew Carnegie]] to [[id:d7554ff8-0ea1-48d5-9243-4289db74e616][public libraries]].
