:PROPERTIES:
:ID:       c952b211-1139-4ada-9c03-e9817bf3e139
:mtime:    20211127120946 20210724222235
:ctime:    20200707235009
:END:
#+title: How personal is a personal wiki?

At the beginning, I'm keeping it pretty academic.  I'm not putting anything about my personal life, or more personal thoughts, in here.  Mainly just as I'm hesitant to share that stuff.  But it's a personal wiki, should it have personal thoughts?  I guess somewhere you have to make the distinction between personal thoughts and public and private thoughts.

Stuff that will never get published, but benefits from collecting and connecting the dots.

Ideally I'd like to have some parts of my wiki private/encrypted, so everything could be interlinked, but only what I choose to share is ever shared when I publish. I need [[id:88de9943-00c5-4d30-a81b-b6b35fbff23a][gevulot]].

* Private wiki
  
The simplest solution is probably to just separate public/private thoughts out into different wikis. I am currently just using a separate [[id:20210326T232003.148801][org-roam]] private wiki. 

[[id:999d15b3-84ff-4b10-8146-9f7b575b0215][Bill Seitz]] has lots to say about private wikis. [[http://webseitz.fluxent.com/wiki/PrivateWiki][Private Wiki - WebSeitz/wiki]] 
