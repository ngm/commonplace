:PROPERTIES:
:ID:       2908a4ca-5a22-436c-9c55-151557f93e7f
:mtime:    20220122133411
:ctime:    20220122133411
:END:
#+TITLE: Covid antiviral drugs significantly reduce the chances of hospitalisation and death
#+CREATED: [2022-01-22 Sat]
#+LAST_MODIFIED: [2022-01-22 Sat 13:37]

[[id:15ecf8a8-1975-4096-8eb5-b6550708bac1][Coronavirus]] [[id:6dcc582d-39d8-4dfb-8239-d2bebf9c49cf][Covid antiviral drugs]]

[[https://www.theguardian.com/world/2022/jan/22/more-people-will-die-fears-clinically-vulnerable-england-axes-plan-b][‘More people will die’: fears for clinically vulnerable as England axes plan ...]]

* If

+ taken during the first few days of illness.

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:8d2ee51c-3a2c-487d-9247-fb0389148bd0][Most likely]]

I believe it.
