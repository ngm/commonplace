:PROPERTIES:
:ID:       018a16d2-0db3-4d46-b21c-22a6361794ae
:mtime:    20211127120754 20210724222235
:ctime:    20210724222235
:END:
#+title: Beating the bounds
#+CREATED: [2021-06-12 Sat 12:03]
#+LAST_MODIFIED: [2021-11-27 Sat 12:07]

#+begin_quote
Beating the bounds, you may recall, is the practice used by many English villages of walking the perimeter of their land to identify any fences or hedges that had encroached upon their shared wealth.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
In our times, beating the bounds may initially involve [[id:fab9f5a0-7cf2-4b31-be96-fb3f560935a3][direct action]] resistance and [[id:6c0036f1-d430-41d6-807e-0a623cabe0ad][civil disobedience]] against [[id:03f7f762-f450-41fa-83d4-3c0faecf1be2][enclosure]]s, and attempts to "de-enclose" them.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote


