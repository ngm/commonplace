:PROPERTIES:
:ID:       a676b553-ab00-423e-a453-2fe224ba7d98
:mtime:    20211127120925 20211025225740
:ctime:    20211025225740
:END:
#+TITLE: Labour theory of value
#+CREATED: [2021-10-17 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:9c9043ce-3119-4e5f-a7a2-f960b5662c27][Value]].

#+begin_quote
The one property that all commodities have in common, and through which their “value” can be determined, is that each is a product of human labor

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

#+begin_quote
Commodities can exchange according to the relative amount of labor-time that it takes to produce them

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

#+begin_quote
That’s why mainstream economists today are known as neoclassical, rather than classical, economists. They profess dedication to the likes of Smith and Ricardo but have discarded the troubling labor theory of value. After all, if the producers of wealth are laborers and not the bosses, then the popular slogan of May 1968, “The boss needs you; You don’t need the boss,” is not abstract hyperbole, but a concrete roadmap for the future

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote
