:PROPERTIES:
:ID:       c499a402-92d0-48d3-ab33-f3b40dda401e
:mtime:    20220803230508 20220607213807 20220603161930 20220316194701 20220206075409
:ctime:    20220206075409
:END:
#+TITLE: Cost of living crisis
#+CREATED: [2022-02-06 Sun]
#+LAST_MODIFIED: [2022-08-03 Wed 23:12]

#+begin_quote
Without another Covid wave, it is the cost of living crisis and particularly its impact on families already struggling to meet their rent, put food on the table and pay their heating bills that will dominate people’s lives in the next two years.

-- [[https://www.theguardian.com/commentisfree/2022/feb/06/observer-view-on-britain-cost-of-living-crisis][The Observer view on Britain’s growing cost of living crisis | Observer edito...]] 
#+end_quote

#+begin_quote
The poorest households in the UK could see their cost of living jump by as much as 10% by this autumn if Russia’s invasion of Ukraine leads to a prolonged conflict, the Resolution Foundation thinktank has warned.

[[https://www.theguardian.com/business/2022/mar/14/cost-of-living-rise-uk-poorest-households-resolution-foundation][Cost of living for UK’s poorest could be 10% higher by autumn, thinktank warn...]]
#+end_quote

#+begin_quote
The crisis impacting working people isn’t a result of blind economic forces — it is the result of a class war waged from above

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
The year 2022 will, in all likelihood, be the toughest one for working people since the immediate aftermath of the 2008 Financial Crash

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
In its response to rising inflation, The Bank of England was quick to lay the blame on workers, calling for wage restraint. But this crisis hasn’t been caused by an increase in demand. The proximate cause is a crisis in supply, chiefly of energy but also in the structure of the supply chain itself, against which the only protection that workers have is the fight for higher wages

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
In the end, prices are not set by workers or by consumers, but by firms. This is one of the entitlements afforded to capital by the private ownership of the economy. So, when those prices go up, a decision is being made. In the case of Tesco, for example, Britain’s largest retailer, their decision is to increase the cost of food while sitting on an annual operating profit of £2.6 billion

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Much the same is the case in the energy sector, where the so-called Big Six raked in over £1 billion in profits on the eve of a truly astronomical hike in bills for their customers. British Gas alone saw a 44 per cent jump in its profits

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
When a crisis like the one we’re experiencing emerges, there is always a question: who pays for it? If the cost of production rises, will workers and consumers bear the brunt or will profits be squeezed? In an economy dominated by private ownership, this is in reality not much of a question at all. Profits will be protected; everything else is a lower-order concern

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
The only answer to the cost-of-living crisis is fundamental economic change. A government that truly represented the public interest would today be intervening in any number of ways to prevent living standards from dramatically deteriorating

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
This situation puts a particular focus on extra-parliamentary politics, from trade unions to social movements and campaigns. These are now the only ways that pressure felt in working-class communities across the country can be expressed in an organised form. And, in turn, the only way that pressure can be directed against the business interests profiting from this crisis and the politicians doing their bidding

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
This means that government by consent is fading and, in its place, government by force is becoming more commonplace. As the cost-of-living crisis deepens this year, and millions of people who were already struggling realise that their living standards will decline even further, the anger will be difficult for the government to contain. And so, they are providing themselves with the tools necessary for a crackdown

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Like steam, even great quantities of anger can soon dissipate. It is only when that steam is gathered in a structure that the pistons move and the wheels of history can begin to turn

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
We must set out our alternative of wage rises, price caps, and [[id:52a0b7a0-b65c-486c-8166-969d9b273279][public ownership]], and of a fundamentally different political and economic system which serves a different set of interests

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
A great wave is about to come crashing down on this country, and the only thing which can protect us from it is [[id:5c06724b-1bbe-42f7-b611-896c83222936][class struggle]]

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Rising inflation is driving the cost of living crisis, but it isn’t an act of God. It’s the result of policy decisions that favour the rich — and socialists need to have an alternative

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
there is reason to believe that inflation is running at even higher rates for poorer households than it is for wealthier ones

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote
