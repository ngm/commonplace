:PROPERTIES:
:ID:       c284eaea-02ce-4ecd-a7d1-241fe21f8c13
:mtime:    20240417212026 20211127120941 20210724222235
:ctime:    20210724222235
:END:
#+title: Bashar al-Assad's decade of destruction in Syria
#+CREATED: [2021-06-03 Thu 17:47]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ URL :: https://www.theguardian.com/news/audio/2021/jun/02/bashar-al-assads-decade-of-destruction-in-syria-podcast
+ Publisher :: [[id:7f5a7ae4-7af4-44e8-876f-d1d83fb4c43d][The Guardian]]
 
[[id:52c24760-a030-4de5-8094-84cc590c80be][Syria]].  [[id:763b0de8-e6e5-4eda-86d0-da2cd37a78e4][Bashar al-Assad]].

#+begin_quote
as Assad lost control of vast areas of Syria following the uprising, he was at maximum vulnerability. But the intervention of [[id:a7c00d60-0c87-420a-a1c9-0752d1b16f15][Iran]] and Russia proved decisive. Now, after a widely discredited election victory with 95% of the vote, Assad begins his fourth term as president of a country deeply scarred by war.
#+end_quote
