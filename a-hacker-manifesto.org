:PROPERTIES:
:ID:       4ae309e4-0084-42b8-9206-16ad8c3238b3
:mtime:    20211127120833 20210724222235
:ctime:    20210724222235
:END:
#+title: A Hacker Manifesto

A book by [[id:cbcdc3c9-42c2-4159-9682-a3192f720b61][McKenzie Wark]].

#+ATTR_HTML: :width 50% :alt "Frontcover of A Hacker Manifesto book.  Plain red background, black rectangle in the centre with book and author name."
[[file:images/a-hacker-manifesto.jpg]]

* Overview
  
#+begin_quote
criticizes the commodification of information in the age of digital culture and globalization
#+end_quote

#+begin_quote
mimics the epigrammic style of Guy Debord's [[id:8529cc43-f4ea-42a0-81c5-5bc53d4362fa][The Society of the Spectacle]]
#+end_quote

Yeah... which on first attempt makes it hard to read for me.

#+begin_quote
Wark builds on Marx and Engels’ ideas, alongside [[id:bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa][Deleuze and Guattari]], by adding two new classes of workers into the mix - the "hacker class" and the "vectoralist class". 
#+end_quote

^ Hmm keen to learn how it is building on D&G.

* Themes

  - [[id:f709307b-b37e-4eb1-b6e3-6bf5036cc8d1][commodification of information]]
  - [[id:f20e30b7-4ba5-4fb3-b61c-78dccb5fb136][Vectoralist class]]
