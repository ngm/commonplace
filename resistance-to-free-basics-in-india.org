:PROPERTIES:
:ID:       3f9c8424-bf4d-4885-aca2-9032d39e3b7b
:mtime:    20230508205241
:ctime:    20230508205241
:END:
#+TITLE: Resistance to Free Basics in India
#+CREATED: [2023-05-08 Mon]
#+LAST_MODIFIED: [2023-05-08 Mon 20:53]

#+begin_quote
However, successful resistance to developments like Facebook’s [[id:b8dd741a-74b9-45ec-83fe-11055cb16e97][Free Basics]] in India and the construction of Amazon’s headquarters on sacred Indigenous land in Cape Town, South Africa show the possibility and potential of civic opposition.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote

^ https://indianexpress.com/article/technology/tech-news-technology/facebook-free-basics-india-shut-down
