:PROPERTIES:
:ID:       28e95f1d-b6b9-418b-a41c-b2560e42981f
:mtime:    20211127120816 20210724222235
:ctime:    20210724222235
:END:
#+title: k2pdfopt
#+CREATED: [2021-04-17 Sat 18:59]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Convert PDFs to make them more readable on ebook readers.  The website is awesome.

+ https://www.willus.com/k2pdfopt/

  
