:PROPERTIES:
:ID:       aab864e8-b8bf-4db8-bf1f-6b1588c8ee0d
:mtime:    20220618161711
:ctime:    20220618161711
:END:
#+TITLE: All about Upcycling Android
#+CREATED: [2022-06-18 Sat]
#+LAST_MODIFIED: [2022-06-18 Sat 16:22]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://fsfe.org/news/podcast/episode-15.en.html

Interview with [[id:40a7ee20-dedf-4cb1-99aa-73495ee96c2e][Erik Albers]] on [[id:37f8b154-8d8b-47cc-85b2-bdf157420a9c][software sustainability]] and the [[id:ef72f3f4-d06a-4e55-ae32-9a000d80f79c][Upcycling Android]] campaign.

