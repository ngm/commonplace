:PROPERTIES:
:ID:       ae66efe9-667d-4f51-b56a-b2d46e2a99f7
:ROAM_ALIASES: BASB
:mtime:    20211127120929 20210724222235
:ctime:    20200309190319
:END:
#+TITLE: Building a second brain

[[https://www.buildingasecondbrain.com/][Building a second brain]], from Tiago Forte.

Essentially an [[id:922a74d8-b8d5-4238-b26b-216e79dd6c0b][information strategy]].  A way of capturing, connecting, and creating information and knowledge.

Perhaps more a form of [[id:89ddd5c6-a73a-4e4b-9676-21ab0fd9cc39][personal knowledge management]].  It results in the construction of something akin to a [[id:516147aa-d559-470b-9c65-93188052804c][personal wiki]].

(What's the difference between personal knowledge management and information strategies?)

- remember ([[id:fdfd958d-1534-4525-b6f4-6e68b7628974][collecting the dots]]?)
- connect ([[id:053ee741-a4e1-4021-a992-6a326734603c][connecting the dots]]?)
- create

#+begin_quote
an external, centralized, digital repository for the things you learn and the resources from which they come.

-- [[https://fortelabs.co/blog/basboverview/][Building a Second Brain: An Overview]]
#+end_quote


Thinking like a curator is a kind of [[id:922a74d8-b8d5-4238-b26b-216e79dd6c0b][information strategy]].

[[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][Progressive summarisation]].

PARA.  Projects, areas, resources, archives.

- https://fortelabs.co/blog/basboverview/

- don't just consume information passively - put it to use
- create smaller, reusable units of work
- share your work with the world

* Resources
** General
*** [[https://douglastoft.com/turn-your-notes-app-into-a-personal-knowledge-base-tiago-forte-on-building-a-second-brain/][Turn Your Notes App Into a Personal Knowledge Base — Tiago Forte on Building ...]] 
** Emacs
*** [[https://medium.com/@tasshin/implementing-a-second-brain-in-emacs-and-org-mode-ef0e44fb7ca5][Implementing A Second Brain in Emacs and Org-Mode - Tasshin Fogleman - Medium]] 
*** [[https://praxis.fortelabs.co/building-a-second-brain-in-emacs-and-org-mode-faa20ae06fc/][Building a Second Brain in Emacs and Org-Mode - Forte Labs]] 
