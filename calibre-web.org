:PROPERTIES:
:ID:       e3180240-961b-4384-9f8a-5f23c5493af6
:mtime:    20220610102136
:ctime:    20220610102136
:END:
#+TITLE: Calibre-Web
#+CREATED: [2022-06-10 Fri]
#+LAST_MODIFIED: [2022-06-10 Fri 10:41]

+ URL :: https://github.com/janeczku/calibre-web
  
A web interface for your [[id:6cab8448-b3ee-41e6-b8ca-c749adb35911][Calibre]] library.
