:PROPERTIES:
:ID:       611df3a2-9afe-4652-b171-8869a9b9001d
:mtime:    20211127120845 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Internet of Things

Generally sceptical of the hype around IoT.  No doubt there are some positives of abundant sensor information, but the amount of devices it causes to be produced and junked, and the privacy concerns, need a lot to outweight them.

At OggCamp the guy from IBM gave a talk about MQTT and IoT.  There was a bunch of commercial stuff, but some positive-ish uses of IoT and sensor information that I remembered:

- a way to monitor animals in zoos to only use the heaters when necessary
- a way of reducing energy use in homes
