:PROPERTIES:
:ID:       7a13ead4-eba2-4de7-ac81-f0021c8af1fd
:mtime:    20220303214922 20220226131745
:ctime:    20220226131745
:END:
#+TITLE: socialism from below
#+CREATED: [2022-02-26 Sat]
#+LAST_MODIFIED: [2022-03-03 Thu 21:50]

+ [[https://newsocialist.org/socialism-from-below/][Socialism from Below – New Socialist]]
+ [[https://theanarchistlibrary.org/library/george-woodcock-socialism-from-below][Socialism from Below | The Anarchist Library]] 

#+begin_quote
'Socialism From Below' was the most important principle of the [[id:220ecbbd-7c26-47a4-a888-ac72a0d83226][Prague Spring]]. ‘Socialism from below’ was similarly a central principle of [[id:3176ea46-0b18-4abe-be7c-cc8b7a441c90][Salvador Allende]]’s Chilean movement and government, so cruelly destroyed by the US-backed coup in 1973, which left such a profound impression on the young Jeremy Corbyn

-- [[id:3cf0ea78-ef38-4002-9974-9a7604b68313][the revolution will be networked]]
#+end_quote
