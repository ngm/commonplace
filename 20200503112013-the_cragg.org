:PROPERTIES:
:ID:       fae17700-94f3-4de8-a26f-61b9e595942c
:mtime:    20211127121016 20210724222235
:ctime:    20200503112013
:END:
#+TITLE: The Cragg

A recent discovery for me near my new home town is The Cragg - a mini-peak nestled in the outskirts of the [[id:73576fd3-939c-41a4-b102-550645cfc03b][Forest of Bowland]].

It's a short (hilly!) bike ride from my home in [[id:016050a0-1628-45f5-aa63-3c4730b17beb][Lancaster]].

Right now, the journey there has a lot of lapwings in the fields, with their distinctive calls, and fields full of sheep, lambs, and cows.

From The Cragg you get a close up view over towards a small hilltop windfarm of eight turbines.  I love to watch them.

#+ATTR_HTML: :width 100%
[[file:photos/the-cragg-turbines.jpg]]

And it has absolutely stunning vistas - you can see the Yorkshire Dales, the [[id:bf99679c-0eb4-49f3-b05f-6265cf2ef96e][Lake District]], and [[id:04a333e4-56e2-45aa-9590-b25786c0e7f6][Morecambe Bay]].  You're accompanied pretty much all of the way by [[id:059bb8d0-1dfb-43e2-bb8b-4133398d547c][Clougha Pike]].

I feel very lucky to have it so close by.

* The Cragg towards Bowland in December
  
#+ATTR_HTML: :width 100%
[[file:photos/the-cragg-towards-bowland.jpg]]
