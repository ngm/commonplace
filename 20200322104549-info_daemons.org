:PROPERTIES:
:ID:       77af1e9a-2931-4bde-bc97-517519120dab
:ROAM_ALIASES: "virtual assistant"
:mtime:    20220205181401 20220119201354 20211127120857 20210724222235
:ctime:    20200322104549
:END:
#+TITLE: info daemons

Some kind of virtual assistant that helps you with your [[id:922a74d8-b8d5-4238-b26b-216e79dd6c0b][information strategy]].

- Fictional 
  - [[id:58f601cc-0770-4902-bded-0fd4324d2d9c][Tom]] in [[id:63c7937f-dca5-4c55-b8e0-1c60e6babf06][Hyperland]]
  - [[id:fbd23090-f807-4a00-94b8-d89ab00bf581][The Librarian]] in [[id:0ecb1650-ca8a-4ef3-9302-1e8432c45ac3][Snow Crash]]
  - [[id:44162300-b322-400d-847d-e5f082abc4f9][Jane]] in [[id:78452b54-49f6-4fec-9545-942eaeb02174][Speaker for the Dead]]
- Real
  - Clippy(!)
    - Clippy might have sucked, but it's not a good counter argument to the idea of virtual assistants. (If a bridge collapses, do we say bridges are a terrible idea?)
- Things that need more work
  - [[id:b98f1e69-4efe-4595-bbef-b3a428868b57][Huginn]]
  - Node-Red (https://nodered.org/)
  - [[id:087e20dd-371e-450f-9cb1-f3de4e581a80][geist]]s
  - Dual https://paulbricman.com/thoughtware/dual

Why do we need virtual assistants? How about we just work together and collaborate on things with other humans?  Maybe a virtual assistant does the drudge that you wouldn't want to ask another person to do. 

