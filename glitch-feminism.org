:PROPERTIES:
:ID:       9abafc20-b2a5-466a-92db-5846a015e21e
:mtime:    20211127120917 20210724222235
:ctime:    20210724222235
:END:
#+title: Glitch Feminism

Book by Legacy Russell.

#+begin_quote
Our software and our wetware are constantly glitching. How could it be otherwise? Rather than try for perfect order, let’s embrace the glitch and find out how else it all could play out. Let’s just admit we’re done with the old empire of imperatives about both flesh and tech and tune in to those who are playing in the ruins, hacking their way through to another life. This book takes you there.

-- [[id:cbcdc3c9-42c2-4159-9682-a3192f720b61][McKenzie Wark]]
#+end_quote

[[id:9a1f2426-0e14-4015-bc61-a9b6d08c88f5][Glitch]].
