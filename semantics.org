:PROPERTIES:
:ID:       2b4b760d-e841-4434-9954-ff2fd91011e6
:mtime:    20220718204702
:ctime:    20220718204702
:END:
#+TITLE: semantics
#+CREATED: [2022-07-18 Mon]
#+LAST_MODIFIED: [2022-07-18 Mon 20:47]

#+begin_quote
the way in which words and language ascribe meaning to the objects being represented is the focus of study in semantics

-- [[id:f9e7173e-4659-4d10-859d-161c412b7989][Nature Matters: Systems thinking and experts]]
#+end_quote
