:PROPERTIES:
:ID:       5737edb0-1282-4914-9f37-91748218fd3b
:mtime:    20211230211908
:ctime:    20211230211908
:END:
#+TITLE: Free software is a gift economy
#+CREATED: [2021-12-30 Thu]
#+LAST_MODIFIED: [2021-12-30 Thu 21:32]

Not sure I agree with this claim or not. [[id:6d2ed2f0-9a53-40a5-805f-2757feb66bcd][I like free software]] and [[id:185f8876-0afd-4b46-bf2c-17b88dd12418][I like gift economies]] so I'd be fine if it was a good one.

But part of me feels like it's actually more communist (from each according to their ability, to each according to their need).  [[id:d04d7a0a-d1c6-4d15-a870-4ceebc5fc103][Gift economies]] as I understand it have some expectation of (delayed) reciprocity that I'm not sure exists in free software.

Note that this doesn't necessary claim that free software is *exclusively* a gift economy.  Different projects could be different types of exchange systems.  

See

+ [[id:0461dcd6-e448-4b4c-b091-bb94303462c3][The Hi-Tech Gift Economy]]
+ [[id:727ffe4f-5db0-4b21-9be6-c44479b19f57][Maggie Appleton on Open Source as a Gift Economy]]
+ [[https://rhnh.net/2009/11/07/participation-in-the-open-source-gift-economy/][Participation in the open source gift economy –]] 

for some places to possibly dig in to this.

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Gut feeling :: 4
+ Confidence :: 2
