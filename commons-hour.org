:PROPERTIES:
:ID:       979228a2-4da0-4a66-a8de-27919bac355b
:mtime:    20211127120915 20211025200559
:ctime:    20211025200559
:END:
#+TITLE: commons.hour
#+CREATED: [2021-10-25 Mon]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]


#+begin_quote
commons.hour is a [[id:327839f2-d3f0-4dca-8910-b752e7884cce][participatory design]] venue under principles of [[id:642b4b21-c46d-45d5-8dee-30877198d0dc][design justice]], building [[id:4dae6c1f-a51b-41b9-b887-4d9eae246a84][tools for conviviality]] in the spaces of the coop’s commons: platform spaces, media spaces and venue spaces. 

- [[https://meet-coop-1.gitbook.io/handbook-trial/5-commons.hour/commons.hour-programme][commons.hour - The programme - handbook trial]]
#+end_quote

* Sessions
  
- [[id:1aeb8124-d741-4a4e-ba87-1ead2ffb8abe][commons.hour #2]]

