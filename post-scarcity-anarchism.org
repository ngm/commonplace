:PROPERTIES:
:ID:       63c6d3fd-69e7-41e8-99ae-a42a01f7f2f9
:mtime:    20211127120847 20210724222235
:ctime:    20210724222235
:END:
#+title: Post-Scarcity Anarchism
#+CREATED: [2021-03-12 Fri 12:02]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
His 1968 essay “Post-Scarcity Anarchism” reformulated anarchist theory for a new era, providing a coherent framework for the reorganization of society along ecological-anarchistic lines

-- [[id:ef7488bc-0577-42ce-be10-bd45b311911c][The Next Revolution]]
#+end_quote
