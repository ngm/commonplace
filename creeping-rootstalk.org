:PROPERTIES:
:ID:       54a7c33c-6cbb-4089-a187-f992a3a67416
:mtime:    20211127120838 20210912164511
:ctime:    20210912164511
:END:
#+TITLE: creeping rootstalk
#+CREATED: [2021-09-12 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Another name for a [[id:8042b857-d223-4947-840e-bf5bbbb29e73][rhizome]].
