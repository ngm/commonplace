:PROPERTIES:
:ID:       27c1b0e0-8d6b-4ea0-a981-f385f4ea8fb2
:mtime:    20211127120815 20211031111238
:ctime:    20211031111238
:END:
#+TITLE: Notes are conversations across time
#+CREATED: [2021-10-31 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: 
https://subconscious.substack.com/p/notes-are-conversations-across-time
+ Author :: [[id:910a5921-4de6-462c-837b-34133ad5cc93][Gordon Brander]]
