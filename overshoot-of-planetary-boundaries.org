:PROPERTIES:
:ID:       1d5a84bb-9e9e-4bda-9458-44d017f2b617
:mtime:    20231026113323 20231025213438
:ctime:    20231025213438
:END:
#+TITLE: overshoot of planetary boundaries
#+CREATED: [2023-10-25 Wed]
#+LAST_MODIFIED: [2023-10-26 Thu 11:33]

Overshoot of [[id:d3ff8cf0-f7b7-4f4e-bf53-a278b257c9f7][planetary boundaries]].

#+begin_quote
The [[id:b7a1fc95-27f4-4fa5-acf7-0fd5fc63a8b8][chemical pollution]] planetary boundary is the fifth of nine that scientists say have been crossed, with the others being [[id:81275feb-bcb4-472a-913b-a2541866e56b][global heating]], the [[id:a3bdcefb-a3ad-4443-92ad-47c63e286f4f][destruction of wild habitats]], [[id:2250e5c9-1e8e-4bf0-be75-d7de2cec4445][loss of biodiversity]] and excessive [[id:c50654a1-5098-44ab-9400-a547cf5b94c5][nitrogen and phosphorus pollution]].

-- [[https://www.theguardian.com/environment/2022/jan/18/chemical-pollution-has-passed-safe-limit-for-humanity-say-scientists?s=09][Chemical pollution has passed safe limit for humanity, say scientists | Pollu...]]
#+end_quote
