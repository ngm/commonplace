:PROPERTIES:
:ID:       703aacbe-9b35-457b-a0b2-0aa1d6deaa2d
:mtime:    20230407155725 20230407112551
:ctime:    20230407112551
:END:
#+TITLE: Green Socialist Notes: Decentralization 101
#+CREATED: [2023-04-07 Fri]
#+LAST_MODIFIED: [2023-04-07 Fri 15:57]

+ URL :: https://greensocialist.net/101s/

+ ~00:16:46  [[id:710ed987-7b38-43bd-9d12-fd335d355515][The Tyranny of Structurelessness]]

+ ~ 00:22:54 [[id:f29506a8-c399-4432-b41a-8550195f0cf3][Confederation and federation]]

