:PROPERTIES:
:ID:       731e3993-d923-4020-85b6-30ca22b958f0
:mtime:    20220730110747 20211127120854 20211126214418
:ctime:    20211126214418
:END:
#+title: Holochain
#+CREATED: [2020-12-15 Tue 23:25]
#+LAST_MODIFIED: [2022-07-30 Sat 11:07]

#+begin_quote
Unlike Bitcoin and Ethereum (the two most prominent digital ledger technologies), Holochain is far more energy efficient and flexible in the way that it authenticates digital objects on networks.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
It is a lighter, more versatile set of software protocols than the blockchain.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
Rather than relying on a single ledger — a “heavy” solution that requires the work of countless computers on the network — Holochain is a simpler, lightweight approach that lets every user have his or her own secure ledger and distributed storage system to store their personal data and digital identities.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
In this sense, Holochain is a tool based on a differentiated [[id:b85a9356-9e1f-468c-a8ee-2e72184522e9][relational ontology]] (each user can express value in different, particular ways when interacting with people) while blockchain as a tool ultimately reflects an undifferentiated ontology (each user must accept the prevailing standard of the system).
#+end_quote

#+begin_quote
Holochain is a data integrity layer for distributed applications. Basically a way to have both the applications and their required data storage run on all of the users machines instead of centralizing it on a server.

It is a framework that is made to be agent-centric. That is to say, that all applications built on Holochain will enable the agent (person, device or collective) to communicate with other agents through a bunch of applications it has installed.

*This means that an agent stores all of the data connected to the agent and allows this data to be accessed by applications of the agents choice*. Agents also hold a share of other agents data, one piece of the collective dataset through the use of Distributed Hash Tables.

-- [[https://forum.holochain.org/t/shiro-coordination-for-local-food-communities/642/2][Shiro - coordination for local food communities - App Ideas - Holochain Forum]] 
#+end_quote

#+begin_quote
Harris-Braun claims that Holo Fuel will not simply be a substitute form of money that will end up replicating capitalism, but will instead propagate a “different grammatics of value.” Just as a different grammatical structure in a human language helps us to articulate different ideas and realities, the Holochain grammar is intended as a tool to express social forms of value — flows

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
Instead of market-exchange being the dominant form of value, it is envisioned that Holochain-based apps will enable other forms of value to be expressed and circulated within networked communities — in other words, not just the money values represented by prices.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

- [[id:ce827fcf-d234-467f-8f39-5637f3420dc8][Holochain and basic income]].
- [[id:e8fc6c71-6739-4d23-84f0-1bfaf3ce0b0a][Holochain and commoning]].

 #+begin_quote
 Third, I’m really fascinated with the Holochain approach, which uses a set of robust networking protocols that are more versatile in facilitating networked interactions than the blockchain. The Holochain project aspires to help secure, authenticated identity and commoning to flourish in online contexts, which is obviously much more interesting than frictionless, ubiquitous commercial applications. They have a project called the [[id:986a2d59-62d4-42ac-acc9-391b44ba6375][Commons Engine]] – https://commonsengine.org/about – that Fernanda Ibarra is helping to develop.

 -- [[id:fb059889-f149-4e1c-8fdc-a3dc0302d34c][David Bollier, P2P Models interview on digital commons]]
 #+end_quote
