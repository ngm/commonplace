:PROPERTIES:
:ID:       59b8356d-2a7e-45c4-aad4-4adec40e4593
:mtime:    20211127120840 20210724222235
:ctime:    20200317114439
:END:
#+TITLE: Free software licensing

* Various GPL licenses
I find this answer a short but sweet description of GPL-license nuances: https://opensource.stackexchange.com/a/6365

Basically if you build something on GPLed code and distribute it, you have to license as GPL.  If you don't *distribute* it, e.g. put it on a server, then you don't have to license it as GPL.   But Affero gets around this loophole.  If you license it as LGPL, people can build plugins etc to it without having to license them as GPL.

A good example is WordPress - it's GPLed, so any plugins have to be GPL too.  If it was LGPLed, plugins wouldn't have to be licensed in kind.

#+begin_quote
If the app is distributed to other users and contains GPL-covered code, its source must be published under the same (or compatible) license.[source] This is one of the basic principles of GPL.

This also applies to source code that contains/links unmodified GPL-covered code.[source] (This is different in the GNU Lesser GPL)

If however the entire app runs on server or is only used internally in an organization, it doesn't have to be open-source. (This is different in the GNU Affero GPL)

lukasrozs on opensource stackexchange: https://opensource.stackexchange.com/users/10045/lukasrozs
#+end_quote

* AGPL
#+begin_quote
What AGPL does on top of GPL is the redefinition of user. For GPL programs running on your server, you are the user, for AGPL, the real users of the app are the users of your website or service. Therefore you are distributing the app if someone other than you is using it. And that of course implies all the standard GPL requirements.
#+end_quote

* Post Open Source
See [[id:5d291c71-c3ec-4e47-8423-ffe36cfd6194][General Intellect Unit 066 - Post Open Source]] for some discussion of different licenses that might work better than the existing ones.
