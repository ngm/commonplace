:PROPERTIES:
:ID:       4ef2c96b-942c-45af-a70e-cf228476afff
:mtime:    20220531210719
:ctime:    20220531210719
:END:
#+TITLE: Frankfurt School
#+CREATED: [2022-05-31 Tue]
#+LAST_MODIFIED: [2022-05-31 Tue 21:11]

#+begin_quote
The other inheritor of the unorthodox Left was the Frankfurt School, whose theorists criticized the destruction wrought by the mindless conquest of nature. [[id:c5533c49-9c21-427d-8eac-5090bb51cb95][Adorno]], [[id:d04383b3-437f-4bf7-8e39-5d618873b282][Horkheimer]], and [[id:30298de1-3166-4dee-8ad0-2e31e204f833][Benjamin]], as well as [[id:4362887c-95b7-4fec-a587-05e3aa462c76][Alfred Schmidt]] and [[id:e65b15cc-7e36-478b-a354-8eb8aa2c339f][Herbert Marcuse]], all contributed to the foundations of [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][eco-socialism]]

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
