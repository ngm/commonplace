:PROPERTIES:
:ID:       75462007-2d12-4585-b537-b5dc4d49c93f
:mtime:    20211127120855 20210724222235
:ctime:    20200315211735
:END:
#+TITLE: The IndieWeb and the right to repair are related

- tags: [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][right to repair]] | [[id:818c3f88-9507-472b-9bee-1b53b42474cc][IndieWeb]] 

I feel like there's quite a lot of overlaps between the two.  Both in terms of goals and how we operate.  It might be a bit of a tenuous link in some places... but fun to think about.

I remember Nicole from Mozilla saying to Tantek at MozFest 2019, at our Restart stall - "hey Tantek check this out, it's like IndieWeb but for your stuff!"

See also [[id:a158d0b8-0f1e-441d-bd46-ccba61867c48][Goodbye iSlave]], where Jack Qiu links device manufacture and device addiction as two parts of the same system.

In this article they are referenced as two parallel means to counteract the dominance of the big GAMA tech firms:

#+begin_quote
A “right to repair” would stop planned obsolescence in phones, or firms buying up competitors just to cut them off from the cloud they need to run. A “right to interoperate” would force systems from different providers, including online platforms, to talk to each other in real time, allowing people to leave a social network without leaving their friends.

-- [[https://www.theguardian.com/commentisfree/2020/jul/01/apple-google-contact-tracing-app-tech-giant-digital-rights][Privacy is not the problem with the Apple-Google contact-tracing toolkit | Mi...]]  
#+end_quote

* Goals

To be able to fix your own stuff is a bit like being able to own your own data.  Access to spare parts, repair guides, is maybe like having access to APIs of the big silos.

* Organisation

- Local groups that help people fix / build their sites.  
- National(ish) meetups roughly yearly.
- A regular 'summit'.
