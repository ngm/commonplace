:PROPERTIES:
:ID:       22f7d385-bcab-4e52-885e-904291a7dabc
:mtime:    20241103133416
:ctime:    20241103133412
:END:
#+title: Some small experiments in 'microblurting'

* Context

- [[id:0b89ba1c-e3c0-4208-afd4-eaa1f0d01fb6][blurting]]
- [[id:c80ed4fa-cb15-463a-a8d1-770b965ddd44][microblurting]]
- [[id:66bbcffc-6d66-4487-a19b-598f693589d5][my blurts]]

* Thoughts

Microblurting feels similar to using flashcards. Which makes sense, as they're both forms of [[id:60c5e9a3-0be1-4b4a-ae30-872a4a5e54e7][active recall]].

But the micro aspect of it makes it closer to flashcards than doing a bigger blurt.

I've found it beneficial so far. Helping to refine and remember my definitions of some concepts that I'm interested in.

It also feels somewhat similar to doing [[id:35cf0465-98cd-4a65-ab94-ff1a95b350a4][code kata]], in that I'm refining my writing tools and my use of them while repeating and rewriting simple definitions. e.g.[[id:6e31407d-632a-4486-99aa-8e423fc3371f][making it easier to open the session drawer in Termux in right-handed one-handed mode]], [[id:d07175d4-2f7a-41de-acaf-b417a1d6c92f][issue with evil-escape in Doom on Termux]] both arose from microblurting on my phone.

I find the words blurt and microblurt pleasingly comical, but I imagine there's a more sensible term for the practice in learning science.

I'm not certain in the utility yet of publishing your microblurts, but I find it somehow motivational, and as long as you're not overloading a public stream that people are expecting to be something else, I see no harm in it.

Should it stand the test of time, I could imagine a microblurt having its own microformat.
