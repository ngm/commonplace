:PROPERTIES:
:ID:       95d28f51-dfdb-4300-bd34-19d131818512
:mtime:    20220413161905
:ctime:    20220413161905
:END:
#+TITLE: Manufacturers are Joining the Fight for Repair
#+CREATED: [2022-04-13 Wed]
#+LAST_MODIFIED: [2022-04-13 Wed 16:22]

+ URL :: https://www.ifixit.com/News/58484/manufacturers-are-joining-the-fight-for-repair

#+begin_quote
Samsung and Apple are pledging to make parts and tools available to the public. Valve is launching the Steam Deck with spare parts in mind. And in a turn from their legacy of glue, Microsoft has been redesigning products to be more repairable. This makes good business sense, even if some of these “features” will soon be legally required—they’re just getting ahead on their homework.
#+end_quote

I mean yeah they announce stuff, not always or slow to follow through though (e.g. Apple...)

#+begin_quote
Consumer electronics leaders didn’t get where they are by ignoring trends. On the contrary, they are expert cool hunters and are  usually right on top of big paradigm shifts. The Right to Repair has become a swell worth noticing. With bipartisan US bills and international regulations gaining steam, presidential endorsement, and even internal calls to action, these companies need to move fast to catch this wave before it crests. 
#+end_quote
