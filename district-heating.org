:PROPERTIES:
:ID:       f7ec252d-1c46-4431-a373-98b34754f8a6
:mtime:    20220724100428 20220722212108
:ctime:    20220722212108
:END:
#+TITLE: district heating
#+CREATED: [2022-07-22 Fri]
#+LAST_MODIFIED: [2022-07-24 Sun 10:04]

Heat on tap, so to speak.  You don't generate the heat in the building itself.

#+begin_quote
where heat is generated at large, centralised facilities, sometimes as a byproduct of industrial processes such as burning biomass. It’s then distributed to homes, replacing the use of localised generation – boilers and the like

-- [[https://www.positive.news/environment/energy/how-will-the-uk-achieve-net-zero-by-2050/][How will the UK achieve net zero by 2050? Here are four pathways - Positive News]]
#+end_quote
