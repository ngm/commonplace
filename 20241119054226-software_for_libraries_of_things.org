:PROPERTIES:
:ID:       21f11f33-9a8b-42cb-ad5f-3c5a727ef1f3
:mtime:    20241119054230
:ctime:    20241119054226
:END:
#+title: Software for libraries of things

Software to help you run your [[id:d8876adf-693c-441e-b6d2-c643f6b656be][library of things]].

The two main ones seem to be [[id:b67f1425-73b0-4a02-86e9-66b44ec1b1db][Lend Engine]] and [[id:5128a5d0-4059-4ba2-8b13-77dead36d69b][MyTurn]].

Snipe IT is an open source asset management tool that might work... but it's not its main focus.
