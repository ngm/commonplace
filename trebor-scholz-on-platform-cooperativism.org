:PROPERTIES:
:ID:       c7d99736-668b-4601-93d6-021df00d273d
:mtime:    20230409171931 20230408201050
:ctime:    20230408201050
:END:
#+TITLE: Trebor Scholz on Platform Cooperativism
#+CREATED: [2023-04-08 Sat]
#+LAST_MODIFIED: [2023-04-09 Sun 17:19]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.futurehistories.today/
+ Series :: [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]]

[[id:882c107a-f00a-4b49-bc3c-246056469215][Trebor Scholz]] on [[id:91581ce2-3841-4234-92de-452f1ae9e408][Platform coops]].
