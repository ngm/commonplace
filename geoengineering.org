:PROPERTIES:
:ID:       68b920fd-0052-447c-96ec-7f58493f0720
:mtime:    20220717120335 20220716113508 20211127120849 20210914231116
:ctime:    20210914231116
:END:
#+TITLE: Geoengineering
#+CREATED: [2021-09-14 Tue]
#+LAST_MODIFIED: [2022-07-17 Sun 12:49]

Deliberate attempts to modify and control the Earth's [[id:12baff02-8c17-4dad-966f-c49d38a4f4d4][climate system]].  

* Geoengineering and the left

Some on the eco-left are for it; some not.  Generally seems like a bad idea to me, but some argue it is necessary to mitigate climate breakdown now that we've let it get so far.

** For

#+begin_quote
In 2013, ‘accelerationists’ [[id:3deac17b-bb96-4f13-9295-e447bd531183][Alex Williams]] and [[id:ed2de7ec-f383-4409-928f-bfc903cb8426][Nick Srnicek]] called for a ‘Promethean politics of maximal mastery over society and its environment’.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

I think Srnicek and Williams moved on quite a bit from their 2013 take.  Would be interesting to see if they mentioned geoengineering in [[id:62fe9e0f-1460-4b3b-ba14-c5e9f10f4fe0][Inventing the Future]].

#+begin_quote
Four years later, [[id:4c8422ed-6330-49dd-97da-e9e7d80a5ce5][Jacobin]] published a special issue on the environment which included essays praising geoengineering and nuclear power to undergird cornucopian communism.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
[[id:3cdaf695-621a-44cc-8221-0cc878e32fab][Holly Jean Buck]], a socialist and self-described ‘geoengineer’, warns the Left that entrepreneurs who are ‘taking action, being creative, or disrupting’ are ‘the wrong focus of critique’. She speculates that one day there might be carbon-credit ‘gift cards’ and an AI-controlled SRM (a true Skynet).

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

I haven't read it yet, but I would guess that [[id:1d0fa725-6b14-4ecb-9705-283dd56b782e][Fully Automated Luxury Communism]] by Aaron Bastani would be pro-geoengineering to some degree.

** Against

#+begin_quote
geoengineering, it seems, had always been a form of planetary class war.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
Fossil-fuel companies, conservative think tanks, and economics departments, after all, had been among the earliest supporters of geoengineering.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote


#+begin_quote
In an era of climate change, this logic leads to geoengineering despite its manifest threat to the essential and extremely complex Earth system. Neoliberals willingly gamble with something as risky as SRM rather than countenance restrictions on their revered market.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
To get a sense of how foolish it is to think that privatized geoengineering will produce an optimal climate, it is worth remembering the shock of the [[id:23708a2e-0f8d-4386-8b4e-0f37396a541a][ozone hole]].

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
The ozone crisis is not just a useful allegory to warn against an overweening geoengineering programme but is directly relevant because SRM might itself damage the ozone layer.

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote
