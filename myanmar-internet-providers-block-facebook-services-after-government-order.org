:PROPERTIES:
:ID:       f2f70f60-eb56-4695-bc23-77f03e46c7e0
:mtime:    20221023113222
:ctime:    20221023113222
:END:
#+TITLE: Myanmar internet providers block Facebook services after government order
#+CREATED: [2022-10-23 Sun]
#+LAST_MODIFIED: [2022-10-23 Sun 11:41]

+ URL :: https://www.reuters.com/article/us-myanmar-politics-facebook-idUSKBN2A32ZE
+ Year :: [[id:5de24503-954d-42a0-84d4-5d9ceae173b6][2021]]

[[id:0df245e5-c264-430c-8721-9ee0aa3e7c1b][Myanmar]].

#+begin_quote
Half of Myanmar’s 53 million people use Facebook, which for many is synonymous with the internet.
#+end_quote

#+begin_quote
“Currently the people who are troubling the country’s stability ... are spreading fake news and misinformation and causing misunderstanding among people by using Facebook,” the Ministry letter said.
#+end_quote
