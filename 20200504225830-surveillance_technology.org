:PROPERTIES:
:ID:       0cdce9eb-8c3d-4fbd-98d3-9c8fd5681d85
:mtime:    20240602181334 20211127120801 20210724222235
:ctime:    20200504225830
:END:
#+TITLE: Surveillance technology

#+begin_quote
the purported benefits of the kinds of technology implemented for surveillance purposes are commonly overinflated

-- [[https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711][Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance]] 
#+end_quote

#+begin_quote
Technologies in this field, much like the humans that create them, invariably have [[id:84384c14-99db-4abb-ba9c-1a646e73bed4][biases]] and inaccuracies, and these biases and inaccuracies disproportionately impact historically marginalized groups.

-- [[https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711][Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance]] 
#+end_quote

#+begin_quote
It’s no coincidence that there are so many links between neo-nazis, massive digital surveillance, and the tech industry. 

-- [[https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711][Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance]] 
#+end_quote

#+begin_quote
For corporations concerned with monitoring and interpreting peoples’ behaviour in order to turn a profit, and for state agencies concerned with doing so in order to maintain control, the possibilities offered by digital technologies are vast

-- [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]]
#+end_quote
