:PROPERTIES:
:ID:       bbff0cf8-39a5-4a5d-9e70-9fc01b3f7ffa
:mtime:    20211127120937 20210918092358
:ctime:    20210918092358
:END:
#+TITLE: Deleuze and Guattari

[[file:images/deleuze-guatarri.png]]

#+begin_quote
Deleuze and Guattari were both resolutely anti-individualist: whether in the realm of politics, psychotherapy or philosophy, they strived to show that the individual was a deception, summoned up to obscure the nature of reality.

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

#+begin_quote
a progressive, Marxist-inspired, anti-capitalist politics of joy

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

Sounds pretty good to me.

#+begin_quote
In the political arena, Guattari claimed that the ‘centralist disease of communist parties is due less to the ill intentions of their leaders than to the false relationships they establish with mass movements’. His problem was never a particular person – be they schizophrenic or Stalinist – but the process by which groups break up into discrete units, detached from one another, and from their own lives.

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

#+begin_quote
contrary to the assumptions of most philosophers, thought isn’t representational – which is to say, it doesn’t function by making pictures of the world, which can be judged as true or false depending on their degree of accuracy. By contrast, said Deleuze, thought is creative, and always connected to that which it thinks about.

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

 #+begin_quote
 thought is not grounded in identity; rather, it is generated out of difference. 
 
-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
 #+end_quote

#+begin_quote
Why do collaborations always collapse into hierarchies, he asked himself? Why does the group get atomised, rather than retaining a unified voice?

-- [[id:d767ee81-daf8-4ae8-b0ab-0b768b3f9342][A creative multiplicity: the philosophy of Deleuze and Guattari]] 
#+end_quote

* See also

 - A City is not a Tree - [[https://threadreaderapp.com/thread/1315329838559965191.html][Thread by @NajlaAlariefy on Thread Reader App – Thread Reader App]]  <-- feels kind of D&G
