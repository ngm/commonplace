:PROPERTIES:
:ID:       46becfcb-361c-410f-9a09-deac930d927b
:END:
#+title: wealth and income

[[id:237387f7-eb0f-4dc6-8b62-d23d308c43ac][Wealth]] and [[id:bd55ab2d-ec86-4d81-99f9-458c522b1db2][income]].  What's the difference?

#+begin_quote
Personal wealth means a stock of valuable possessions: anything from cash under your mattress, through shares and bonds, to the value of your house or your car. Income, on the other hand, is a flow of money you receive, such as wages for employment.

-- [[https://positivemoney.org/2017/10/wealth-inequality/][What’s the difference between wealth inequality and income inequality, and wh...]]
#+end_quote
