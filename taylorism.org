:PROPERTIES:
:ID:       1b90c292-7b49-46fb-be11-18ae333078c0
:mtime:    20211127120808 20210724222235
:ctime:    20210724222235
:END:
#+title: Taylorism
#+CREATED: [2020-12-19 Sat 18:14]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
Taylorism is a methodology developed in the early 20th century that breaks every action, job, or task into small and simple segments which can be easily analyzed and taught. It was designed to optimize the efficiency of assembly-line factories. It prioritizes breaking work into well defined pieces of work which can be estimated precisely, and workers who fulfill a defined roll which can be trained for, and therefore easily replaceable. This approach was hugely successful in manufacturing at reducing labor costs, but it should be noted that it had a downside in the fact that it alienates workers by (indirectly but substantially) treating them as easily replaceable factors of production

-- [[https://wakingrufus.neocities.org/fail-agile.html][Why Does Agile Fail?]] 
#+end_quote


