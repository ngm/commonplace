:PROPERTIES:
:ID:       f2104948-3c53-4229-9880-8b47f48b7bcf
:mtime:    20220731093034 20211231171030
:ctime:    20211231171030
:END:
#+TITLE: Commoning represents a profound challenge to capitalism
#+CREATED: [2021-12-31 Fri]
#+LAST_MODIFIED: [2022-07-31 Sun 09:37]

[[id:a7ee13af-296f-478b-bbb3-3d7f60547b33][Commoning]] represents a profound challenge to [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]].

A claim I first came across in [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]].

* Because...

+ [[id:64b275a1-3153-45d3-bd16-be4fdff6dae4][Commoning is based on a very different ontology from capitalism]]. OK...  Need a bit more than that though.
- Commoning is a means of provisioning with minimal reliance on markets or states


* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Gut feeling :: 6
+ Confidence :: 2

I believe the claim, but need a bit more evidence.
