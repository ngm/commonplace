:PROPERTIES:
:ID:       4248e698-ef4a-40ad-9156-0a0712808820
:mtime:    20231110174632
:ctime:    20231110174632
:END:
#+TITLE: Reclaim the Stacks: October 2023 roundup
#+CREATED: [2023-11-10 Fri]
#+LAST_MODIFIED: [2023-11-10 Fri 18:16]

I've been reading [[id:f613abf1-a680-410a-9be7-871f52a04c3e][The Internet Con]] by [[id:2449228e-60cf-4c50-9948-91cb0595991b][Cory Doctorow]].  The strapline is [[id:538ac4f2-b16c-4911-a4d5-9ebc9ac5b672][Seize the means of computation]], which is very Reclaim the Stacks adjacent.

#+begin_quote
This is a book for people who want to destroy Big Tech
#+end_quote

Doctorow's main weapon for this destruction is [[id:2c5ef29f-09ef-4f2a-a41f-ecf8a9b04025][interoperability]].  There's three types: voluntary interoperability (often achieved through standards), indifferent interoperability (not because anyone particular cares, but because they don't care enough to block it), and [[id:ca60df51-4212-4cb8-b62d-1e1317a154da][adversarial interoperability]] (the guerilla warfare of interoperability, AKA  [[id:c4a097de-e4da-4fb5-a3ca-c36953d5672a][competitive compatibility]]).

I'm all in for adversarial interoperability.  But as discussed in [[id:f4bea82f-c14b-483c-8f7a-1db6a8d6125d][The Material Power That Rules Computation (ft. Cory Doctorow)]], the technology to build competitive compatibility is one thing - coming up against the laws that have been built to protect [[id:cfb08fe6-d0bf-4317-9f9f-142b680ae02c][intellectual property]] is another.  ([[id:326d2c48-0cb2-4f20-a888-b26228e1b9be][We should phase out intellectual property]]).

Degrowth is a big part of (some flavours of) [[id:0e1618fe-3960-4a83-85d4-247e8384be7f][ecosocialism]].  Therefore, [[id:ccde652c-45a4-4518-9451-9296d87566a5][digital degrowth]] might also be a part of [[id:e3868d08-bfb3-4d5e-aeaf-784cc7543e0d][digital ecosocialism]].
