:PROPERTIES:
:ID:       4e547d87-7d52-479d-90da-1b1d1586a9b6
:mtime:    20220206174908 20220206083530
:ctime:    20220206083530
:END:
#+TITLE: How can we achieve vaccine equity?
#+CREATED: [2022-02-06 Sun]
#+LAST_MODIFIED: [2022-02-06 Sun 17:49]

+ We should have Covid [[id:94ea8339-d408-470d-8a55-ab5bd8c93d10][patent waivers]]
+ We should have Covid [[id:a37edafa-e2b5-426d-854c-0757cae9af40][technology transfer]] 

#+begin_quote
In order for vaccine equity to be achieved, developing nations need to be supported in their pursuit of manufacturing vaccines on home soil. This starts with wealthy nations supporting the TRIPS Waiver, and pharmaceutical companies sharing the knowledge and technology needed for these countries to produce doses

- [[https://www.globalcitizen.org/en/content/covid-19-vaccine-equity-issues-to-overcome-2022/][Vaccine Equity Issues We Need to Overcome in 2022]]
#+end_quote

[[id:94a8b80f-e5c4-44f3-ab3b-fc16becf1baa][Cuba]] is doing it.

 #+begin_quote
Cuban government announced its plan to get these doses into the arms of those who need them in the Global South, including:

+ Solidarity prices for Covid-19 vaccines for low-income countries;
+ Technology transfer where possible for production in low-income countries; 
+ Extending medical brigades to build medical capacity and training for vaccine distribution in partner countries. 

- [[https://progressive.international/wire/2022-01-25-cuba-pledges-lifesaving-package-of-covid-19-vaccine-support-to-global-south-at-progressive-international-briefing/en][Cuba pledges “lifesaving package” of Covid-19 vaccine support to Global South...]] 
 #+end_quote

