:PROPERTIES:
:ID:       69b49bc0-7154-4cd6-9164-261a1abb128a
:mtime:    20211127120849 20210724222235
:ctime:    20200503134523
:END:
#+TITLE: Read feeder

[[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]] made a [[https://www.zylstra.org/blog/2020/04/federated-bookshelves/][post]] recently about federated bookshelves, sparked by a [[https://tomcritchlow.com/2020/04/15/library-json/][post]] from [[id:dc6f6f4d-0a5b-450b-b2ad-45bc0a4ca10a][Tom]].  It's an idea that [[id:84fc36bf-395d-497f-98df-a68e7a76bcf7][Gregor]] has done a good bit of [[https://gregorlove.com/2020/04/a-lot-of-interesting-ideas/][thinking about]] from an IndieWeb perspective.

Book recommendations is something I'm always interesting in.  At base, all it needs is a feed you can follow just of what people have been reading.  I've set up a channel in my [[id:c106204f-5bdc-4e7a-9243-a27b2f03bda5][social reader]] called 'Good Reads', and subscribed to Ton's [[https://www.zylstra.org/blog/category/myreads/][list of books]], as the sci-fi focus looks right up my street.  If anyone else has a feed of read books, let me know!

#+ATTR_HTML: :width 100%
[[file:Read_reader/2020-05-03_13-40-11_Y2gD4mv.png]]


I am keeping my own list of [[id:5f333662-2e97-4ede-88bc-b0e312fc7b6a][books I've read]] in my wiki - sadly not marked up in any useful way at present - something for me to do there.
