:PROPERTIES:
:ID:       76753346-e1a1-4734-8b12-c9f41f21c443
:mtime:    20220820181031
:ctime:    20220820181031
:END:
#+TITLE: Prefer knowledge commoning over knowledge work
#+CREATED: [2022-08-20 Sat]
#+LAST_MODIFIED: [2022-08-20 Sat 18:21]

Prefer [[id:97b1942a-b075-4953-9ec8-897de2662e53][knowledge commoning]] over [[id:be007b21-b0a3-4a93-bc7d-f74721a96a5a][knowledge work]].

* Because

- Knowledge work is generally a force of capitalism
