:PROPERTIES:
:ID:       aad51368-24a7-460e-8944-255958dcf67f
:mtime:    20211127120927 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Emacs

I use Emacs as my text editor (and much more besides).

#+begin_quote
GNU Emacs [...] is a sort of hybrid between Windows Notepad, a monolithic-kernel operating system, and the International Space Station.
-- [[https://medium.com/@steve.yegge/dear-google-cloud-your-deprecation-policy-is-killing-you-ee7525dc05dc][Dear Google Cloud: Your Deprecation Policy is Killing You]] 
#+end_quote

#+begin_quote
It’s a bit tricky to explain, but in a nutshell, Emacs is a platform written in 1976 (yes, almost half a century ago) for writing software to make you more productive, masquerading as a text editor.

-- [[https://medium.com/@steve.yegge/dear-google-cloud-your-deprecation-policy-is-killing-you-ee7525dc05dc][Dear Google Cloud: Your Deprecation Policy is Killing You]] 
#+end_quote

* Why Emacs

It took me a lot of perseverance to get into emacs. I love it now though. The reason I got into it again (I’d dabbled a long time ago, before subsequently using vim instead) was I was looking for a libre alternative to Workflowy. This brought me to org-mode and then I also came across spacemacs, which combines emacs with the vi-like keybindings that I was very used to. It also has a very nice default look and feel and plenty of useful features configured out of the box. So I gave emacs another shot.

I persevered this time because I really want to use libre software wherever possible. Emacs must be one of the longest running free software projects out there, and I feel it will exist for a long time after other editors have come and gone.  It is really hackable and has a great community of people hacking on it.

I would never make the claim that Emacs is better than any other editor, there are many good ones out there, and I think it really depends on what you want and why. But I can quite definitely say that after using emacs regularly for a few years, I absolutely love it, and can’t imagine myself using anything else anytime soon.

#+begin_quote
One of the delightful and surprising things about Emacs, as you get to know it better, is the depth of customisation which is available. Emacs can be a completely different editor for different people and for different purposes. Being able to tweak things on the fly and try them out before you commit to them, or even as a temporary fix to solve the particular problem you have right now, is empowering.

-- [[https://www.rousette.org.uk/archives/advising-emacs/][BSAG » Advising Emacs]] 
#+end_quote

* Tutorials

I can’t recommend a good tutorial as I never really did one end-to-end, just dipped into different things here and there.

Bit I think series of short videos can be better than text tutorials for this, sometimes.

I haven’t watched this series, Using Emacs, but I see it linked a lot, so it might be useful: www.youtube.com/watch?v=49kBWM3RQQ8&list=PL9KxKa8NpFxIcNQa9js7dQQIHc81b0-Xg

This one is quite good for org-mode, not least because he sounds like Arnold Schwarzenegger: www.youtube.com/watch?v=sQS06Qjnkcc

* Do you spend more time configuring Emacs than doing work with it?

Not gonna lie, I do probably spend more time fiddling with Emacs than I really should.  But I enjoy it!  

I also feel like it's an investment that's paid off - understanding how it works has let me do more with it, so it's probably been a net gain overall.

And unlike other programs that might just be setting options etc, I feel like at least when I'm tweaking it I'm learning skills that might be applicable elsewhere.  (In that elisp is kind of coding at times.)

* Misc
** process editor: https://www.masteringemacs.org/article/displaying-interacting-processes-proced
** literate dotfiles: https://www.reddit.com/r/orgmode/comments/a7rgr1/organizing_your_dotfiles_with_org_mode/
** engineering notebook: https://www.youtube.com/watch?v=YXhpwLKCtf0&feature=youtu.be
** elisp koans https://github.com/jtmoulia/elisp-koans
