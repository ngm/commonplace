:PROPERTIES:
:ID:       75d4a14d-010b-4e3c-ab9b-e9a5b80196f9
:mtime:    20211127120856 20211113121415
:ctime:    20211113121415
:END:
#+TITLE: SecureDrop
#+CREATED: [2021-11-13 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://securedrop.org/

#+begin_quote
open source whistleblower submission system that media organizations and NGOs can install to securely accept documents from anonymous sources
#+end_quote
