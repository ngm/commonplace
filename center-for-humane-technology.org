:PROPERTIES:
:ID:       985d34be-7013-487f-9ba4-0f0ed370f862
:mtime:    20211127120915 20211010181840
:ctime:    20211010181840
:END:
#+TITLE: Center for Humane Technology
#+CREATED: [2021-10-10 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
the Center for Humane Technology ultimately functions not as a solution to our technologically exacerbated problems, but simply as a way of making those problems slightly more palatable.

-- [[https://librarianshipwreck.wordpress.com/2018/02/13/be-wary-of-silicon-valleys-guilty-conscience-on-the-center-for-humane-technology/][Be Wary of Silicon Valley’s Guilty Conscience: on The Center for Humane Technology]] 
#+end_quote
