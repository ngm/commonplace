:PROPERTIES:
:ID:       7b9555fb-8282-4726-a6b2-03cbad9fd2fe
:mtime:    20220811215950 20220810232418
:ctime:    20220810232418
:END:
#+TITLE: GND Summer Book club
#+CREATED: [2022-08-10 Wed]
#+LAST_MODIFIED: [2022-08-11 Thu 21:59]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.gndmedia.co.uk/podcast-episodes/gnd-media-summer-book-club

Very enjoyable.  Talk about a bunch of [[id:85bb9033-ecfa-4f81-a8c8-2152e6e13f71][climate fiction]].

Mentions:

- [[id:95a17b04-906c-4dd1-8f62-75d731206aec][News from Nowhere]]
- [[id:dc5d4d17-5902-4436-ba48-6783bd768c0d][The Ministry for the Future]]
- [[id:e808c0b1-5805-49f8-aac7-594afc213b5c][Oryx and Crake]]
- [[id:2a5b7a87-4713-44f8-925b-19369fa174a1][The Monkey Wrench Gang]]
- The Overstory

A bunch of others that sounds worth checking out.

Some good reflections of the role of fiction, climate fiction, and fiction on the left, too.
