:PROPERTIES:
:ID:       d724ef4e-0aa6-4407-adc8-6c7ef3dd9048
:mtime:    20230520102840 20221022165542 20221022133921 20211127120955 20210828131429
:ctime:    20210828131429
:END:
#+TITLE: accelerationism
#+CREATED: [2021-08-28 Sat]
#+LAST_MODIFIED: [2023-05-20 Sat 10:30]

There's a few different versions of it.

#+begin_quote
The new wave of acceleration began with Deleuze and Guattari’s book, [[id:0a980cd1-0054-42a9-af21-f40443bed553][Anti-Oedipus]] (1972), and in its most charmingly delirious form, Jean-François Lyotard’s Libidinal Economy (1974).8 It took a right-accelerationist turn in the writings of Nick Land, collected in the book Fanged Noumena (2011). I offered a left-accelerationist version in [[id:4ae309e4-0084-42b8-9206-16ad8c3238b3][A Hacker Manifesto]] (2004).

-- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
#+end_quote

#+begin_quote
What I think of as a centrist accelerationism emerged later in Nick Srnicek and Alex Williams’s [[id:62fe9e0f-1460-4b3b-ba14-c5e9f10f4fe0][Inventing the Future]].

-- [[id:b10ad958-1a15-4ba9-9c2a-b99ebe0bc02a][Capital is Dead]]
#+end_quote

#+begin_quote
The accelerationists are, as they often point out, subscribing to a prevalent view from within the Marxist tradition. Historically, Marxists have not been critical of technology, even when that technology is deployed in the workplace in ways that seem detrimental for workers. For many Marxists, technology is at worst neutral: it is not the technology itself, but who controls it, labor or capital. And for some of them, technology, even when wielded by capitalists, is a boon to socialism, creating the conditions of radical transformation right under the bosses’ noses. This means that a socialist movement should treat technological development, even if it has negative consequences in the short term, as something positive.

-- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
#+end_quote

#+begin_quote
Similarly, contemporary accelerationists such as [[id:18a08651-d0e7-4d96-bc55-de5b92004f20][Paul Mason]] and [[id:d8e04986-97be-4348-8403-86a4a695039e][Aaron Bastani]] continue to refer to the passage as a harbinger to a future utopia

-- [[id:96833ac5-3c9a-4993-87ce-adf6e1625b48][Breaking Things at Work]]
#+end_quote
