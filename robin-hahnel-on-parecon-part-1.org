:PROPERTIES:
:ID:       f104c6fb-d222-4a47-965f-a3b801301e95
:mtime:    20230517223137 20230409171810
:ctime:    20230409171810
:END:
#+TITLE: Robin Hahnel on Parecon (Part 1)
#+CREATED: [2023-04-09 Sun]
#+LAST_MODIFIED: [2023-05-17 Wed 22:31]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://www.futurehistories.today/episoden-blog/s02/e21-robin-hahnel-on-parecon-part1/
+ Series :: [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]]

[[id:68983b4c-861d-4736-af04-ab170b59afb5][Robin Hahnel]] on [[id:c15efb85-a454-455e-93f2-72bb2b339529][Participatory economics]].
