:PROPERTIES:
:ID:       6a29f57a-25c2-46ed-9db2-8e6498d17e45
:END:
#+title: Markets don't give you what you need, they give you what you can afford
#+filetags: :claim:

+ A :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]

[[id:8f4fed05-9449-4099-86ac-8cbe4e2585d3][Markets]] don’t give you what you need—they give you what you can afford.

#+begin_quote
Markets don’t give you what you need—they give you what you can afford.

-- [[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]]
#+end_quote
