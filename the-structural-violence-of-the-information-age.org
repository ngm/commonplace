:PROPERTIES:
:ID:       df4dd7c1-42cd-40ed-88fd-5b2f85341e6d
:mtime:    20220712213352
:ctime:    20220712213352
:END:
#+TITLE: the structural violence of the information age
#+CREATED: [2022-07-12 Tue]
#+LAST_MODIFIED: [2022-07-12 Tue 21:43]

The [[id:63219d04-8035-4e04-9e90-66f547ca185e][structural violence]] of the [[id:cc9dd9d9-434a-49db-91ba-d6104cedd3ea][information age]].

A phrase I came across from [[id:43961e5f-42e4-4749-aa48-d690730e3a9f][Technology of the Oppressed]].
