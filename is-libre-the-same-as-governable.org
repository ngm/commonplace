:PROPERTIES:
:ID:       b2b87b7f-2fdd-414c-8e78-b8c51ae08ba6
:mtime:    20221121145906 20221121134930
:ctime:    20221121134930
:END:
#+TITLE: Is libre the same as governable?
#+CREATED: [2022-11-21 Mon]
#+LAST_MODIFIED: [2022-11-21 Mon 15:04]

Is [[id:7581655a-425d-45d6-a055-3811ebcba6a2][libre]] the same as [[id:823c1ed3-7a83-45b7-b761-ef465167205c][governable]]?

I'm thinking from the perspective of [[id:53468f9a-8ce4-4765-8790-0d438d19f3e3][libre software]] and [[id:3862763b-409e-4c92-bf44-8c30a649ef83][governable stacks]].

Is governable a layer above libre?  Does libre necessary mean that you can have some input in to the direction of the thing?  I'm thinking probably not.

#+begin_quote
Technologists seeking alternative visions have often gravitated to the Free Software and Open Source movements, which employ creative licensing to enable the sharing of accessible and modifiable code. These movements have been successful in terms of the sheer volume of widely used software in their commons. But their emphasis on the freedoms of individual users, as well as of corporations, has privileged those with the technical know-how to take advantage.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote

#+begin_quote
Governable stacks should prioritise community accountability alongside individual freedom.

-- [[id:59efd20a-e09b-459a-921e-dfbd3522d27e][Governable Stacks against Digital Colonialism]]
#+end_quote
