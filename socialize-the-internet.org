:PROPERTIES:
:ID:       b6691c4c-4b68-409f-8469-c911b8179b9e
:mtime:    20220805194301
:ctime:    20220805194301
:END:
#+TITLE: Socialize the internet
#+CREATED: [2022-08-05 Fri]
#+LAST_MODIFIED: [2022-08-05 Fri 19:43]

[[id:53529904-4f5d-466d-ad36-00ea399a9c8d][Internet for the People]].

- [[id:a18cf441-2a06-493f-b8c7-86da2bbafda3][We should socialise ICT infrastructure]]
- [[id:6b5548ec-51c0-430d-9598-dd30545b58d1][We should socialise the platforms]]
