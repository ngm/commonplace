:PROPERTIES:
:ID:       7b9274d5-32c5-463a-9dfb-600050b77400
:mtime:    20211224140242
:END:
#+TITLE: OntoShift
#+CREATED: [2021-12-24 Fri]
#+LAST_MODIFIED: [2021-12-24 Fri 14:02]

#+begin_quote
So to truly understand the dynamics of the commons, one must first escape the onto-political framework of the modern West. One must make what we call an “OntoShift” — a recognition that relational cat- egories of thought and experience are primary.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
