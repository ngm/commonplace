:PROPERTIES:
:ID:       8478d3d4-85ff-4f24-9b39-3043b5b4c56e
:mtime:    20211127120904 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-05-31

* [[id:52ada3d1-fc79-4e72-82b5-1bc0e465519b][Putting the space in cyberspace]]
  
* [[id:5c27cb3c-64f3-4f69-9294-1057c3980e75][Wandering the web]]
  

* Duxtapes
  
I've been having fun making a couple of playlists on Kicks' [[https://www.kickscondor.com/duxtape/][Duxtape]].

#+ATTR_HTML: :width 100%
[[file:Duxtapes/2020-05-31_11-38-27_2hRf3LY.png]]

I've always seen mixtapes a way for discovering artists, not piracy.  As it's on [[id:9c6a4a14-1c63-4c49-b769-a241477e5767][hyper]]/[[id:08f20186-b4e0-4070-be89-9082b2a2b2d0][Beaker]], the mixtapes are in theory decentralised and less likely to get taken offline. 

For some artificial constraints, I’m making them all 6 tracks, under 30 minutes, and picking out tunes based on the alphabetical order of the artists.  (And naming them after peaks in Wainwright’s guides to the Lake District…)

* [[id:3e1e8e45-eb46-4cab-a197-7541d1fab213][Sunderland Point]]
  
I cycled to a tiny place called [[id:3e1e8e45-eb46-4cab-a197-7541d1fab213][Sunderland Point]] today.

To get there you have to go via a small road through a [[id:6bf128a8-7a13-4c34-9385-79cdfe0ef977][salt marsh]] that may or may not be passable, depending on the tide.  I like salt marshes.

#+ATTR_HTML: :width 100%
[[file:photos/sunderland-point.jpg]]

According to Wikipedia, "Sunderland is unique in the United Kingdom as being the only community to be on the mainland and yet dependent upon tidal access."

It's got a shitty history as being a port that was part of the slave trade.  Sadly there's quite a bit of that history around Lancaster.
