:PROPERTIES:
:ID:       14048905-f22d-4896-ba9c-def1d13934e4
:mtime:    20231009165902 20230922171917
:ctime:    20230922171917
:END:
#+TITLE: stock and flow diagram
#+CREATED: [2023-09-22 Fri]
#+LAST_MODIFIED: [2023-10-09 Mon 16:59]

Stock and flow diagrams are used in [[id:4f571d3a-8373-438b-a1a2-124d72cc3a14][system dynamics]] and [[id:e044a716-096c-438e-b69a-9a2f177d781b][systems thinking]].

They provide a more quantitative model than causal loop diagrams.  They're often a next step after a [[id:6dc6c8d2-c43e-4309-ad23-5aa8a1cec9d7][causal loop diagram]].
