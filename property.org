:PROPERTIES:
:ID:       d8af8c7b-75fd-4681-a323-a62549e43304
:mtime:    20211127120956 20210724222235
:ctime:    20210724222235
:END:
#+title: Property
#+CREATED: [2021-05-30 Sun 21:45]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
Property is, approximately, “who can do what with what” and it is a legal and social construct.
-- [[id:3d2df42e-cc0c-4d25-982e-ef3f2fa6a99c][Governing the Information Commons]]
#+end_quote

#+begin_quote
Property isn’t some naturally occurring thing that’s the same in all places and times. Every culture is different and every culture changes how they conceive of property over time.

-- [[id:3d2df42e-cc0c-4d25-982e-ef3f2fa6a99c][Governing the Information Commons]]
#+end_quote
