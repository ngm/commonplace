:PROPERTIES:
:ID:       d40e8eb5-ded7-4a79-91b4-ca0cffc2b247
:mtime:    20220126111916 20211127120953 20210724222235
:ctime:    20210724222235
:END:
#+title: Recipes for the cookshops of the future
#+CREATED: [2021-03-14 Sun 21:28]
#+LAST_MODIFIED: [2022-01-26 Wed 11:19]

- [[https://novaramedia.com/2021/03/05/in-the-cookshops-of-the-future/][In the Cookshops of the Future | Novara Media]]

#+begin_quote
Marx and Engels declined to write ‘recipes’ for the ‘cook-shops of the future’ and concentrated on a detailed analysis of the capitalist economy. They opposed their own [[id:5e4bc086-7fdd-4549-acf7-c7b8e4069ad0][scientific socialism]] to the ‘[[id:72c29bc6-6095-4b77-b69e-bf038a5bbb61][utopian socialism]]’ of those who imagined societies of the future, but who failed to base their theories on the movement of existing social forces

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote
