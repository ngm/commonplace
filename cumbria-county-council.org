:PROPERTIES:
:ID:       017d39b9-3754-4b74-8dac-5428037fb3f4
:mtime:    20220419224606
:ctime:    20220419224606
:END:
#+TITLE: Cumbria County Council
#+CREATED: [2022-04-19 Tue]
#+LAST_MODIFIED: [2022-04-19 Tue 22:47]

#+begin_quote
The current council is controlled by a coalition of Labour and Liberal Democrats.

-- [[id:e2d719e5-bb1f-41d9-abec-5f173cbcd1bc][Cumbria County Council launches legal action over shake-up]]
#+end_quote
