:PROPERTIES:
:ID:       0c6b6aa7-a228-4f7e-af93-14632503b85e
:mtime:    20240512213859 20220126111610 20211127120800 20210905221918
:ctime:    20210905221918
:END:
#+TITLE: platform capitalism
#+CREATED: [2021-09-05 Sun]
#+LAST_MODIFIED: [2022-01-26 Wed 11:16]

#+begin_quote
By creating the digital infrastructure that facilitates online communities, platform companies have inserted value capture mechanisms between people seeking to interact and exchange online

-- [[id:7ed63e13-4432-4748-ab16-c66ec91944a6][Platform socialism]]
#+end_quote

#+begin_quote
Platform Capitalism, referring to the market power that these technology companies have by controlling key platforms we use for our basic internet services.

-- [[id:868227ea-d77c-4d93-822e-067edddbc608][Digital Capitalism online course]]
#+end_quote
