:PROPERTIES:
:ID:       84dfaf30-8cd5-4930-be23-338f0d8e949a
:mtime:    20211127120905 20210724222235
:ctime:    20210724222235
:END:
#+title: Repertoires of action
#+CREATED: [2021-07-10 Sat 22:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
For anarchism, then, the kind of repertoires of action that are of interest are those that aim at changing reality in the here and now, at creating alternatives to what presently exists and, where judged to be necessary, directly challenging the dominance or even existence of what exists.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote


