:PROPERTIES:
:ID:       5c17484b-fd1e-4efe-88ba-770f7a8ebde0
:mtime:    20211127120843 20211120123737
:ctime:    20211120123737
:END:
#+TITLE: Provisioning Through Commons
#+CREATED: [2021-11-11 Thu]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Part of the [[id:719b8750-131a-40b1-8185-b991c7d08d5d][Triad of Commoning]].

* Patterns

+ Make & Use Together
+ Support Care & Decommodified Work
+ Share the Risks of Provisioning
+ Contribute & Share
+ Pool, Cap & Divide Up
+ Pool, Cap & Mutualize
+ Trade with Price Sovereignty
+ [[id:fa9cf455-5d51-4575-b786-17b4645e295e][Use Convivial Tools]]
+ Rely on Distributed Structures
+ Creatively Adapt & Renew
