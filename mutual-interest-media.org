:PROPERTIES:
:ID:       60a66ca5-1f56-42af-b2a5-b0d10202b9dd
:mtime:    20211127120845 20210410155355
:ctime:    20210410155355
:END:
#+title: Mutual Interest Media
#+CREATED: [2021-04-10 Sat 15:32]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

"a co-operative owned by the readers and the writers"

+ https://www.mutualinterest.coop/
+ https://opencollective.com/mutual-interest-media
