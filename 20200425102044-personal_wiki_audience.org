:PROPERTIES:
:ID:       db75b03e-2c1a-4eac-a896-64eeb2a7e95e
:ROAM_ALIASES: audience wiki Personal
:mtime:    20211127120958 20210724222235
:ctime:    20200425102044
:END:
#+TITLE: A personal wiki is primarily for you

I think that a [[id:516147aa-d559-470b-9c65-93188052804c][personal wiki]] should be primarily 'for you'.  Keep the barrier to writing in it low.  It's not performative.  If it's a jumbled mess, that's fine, as long as you can navigate through it.  If others' happen to find some needles of information in your haystack, great.  But that's not the goal.  If you want to concretely share some info with the wider world, shape part of your wiki into an article.

In terms of removing friction to writing, it has certainly helped me to think of it as for me 'first'.  I guess it is for me 'first' as opposed to me 'only' (which would be an entirely private wiki - which is also fine, but not what I'm currently doing).

I think the important thing is whatever motivates you to write, at the same time as removing the friction. So, for me personally, having it semi-public is partly a motivator for me, as its something I occassionally point people to a particular page. 

But at the same time, I let it be pretty unstructured, because if I thought it had to be perfectly coherently and well-formed I would rarely write in it. 

Though I do think wikis should be tools for [[id:a322a380-4fe5-49c7-868f-1869341ccef1][networked thought]], if you're worrying about how many views you're getting, something is wrong - you're not getting intrinsic benefit.
