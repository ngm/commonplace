:PROPERTIES:
:ID:       03d11fc6-6fd6-4d26-a8f5-cff132e25a21
:mtime:    20240302115708
:ctime:    20240302115708
:END:
#+TITLE: I love podcasts
#+CREATED: [2024-03-02 Sat]
#+LAST_MODIFIED: [2024-03-02 Sat 11:58]

[[id:ac0c8c81-738c-49ba-9378-7e861f0effeb][I love]] [[id:d5aab574-a7f5-45e2-a5b6-01f496f847d2][podcasts]].

* Because

- They are such a phenomenal source of information.
- They are a knowledge commons.
- I can listen to them while doing other things (walking, chores, etc).
