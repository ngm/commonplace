:PROPERTIES:
:ID:       26a6a3c9-5a51-4c0d-91b3-a619e86cf3d3
:mtime:    20211127120814 20210724222235
:ctime:    20210724222235
:END:
#+title: Anarchism Triumphant: Free Software and the Death of Copyright
#+CREATED: [2021-05-20 Thu 12:13]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://firstmonday.org/ojs/index.php/fm/article/view/684/594
+ Author :: [[id:0f46c257-575a-4e70-a424-71d6a2b9c0b9][Eben Moglen]]
