:PROPERTIES:
:ID:       ca693c81-2a60-4389-abf5-5f534c53c4a0
:mtime:    20220918115624 20220905215436 20220904181713 20220904152828 20220904125018 20220904114910 20220904104705 20220903100736 20220902221211 20220810201345 20220805193103 20220716123529 20220716113008 20220611100808 20220610134848 20220604173036 20220531215752 20220531203933 20220526160921 20220526143054 20220526124324 20220526114102 20220514201946 20220510210124 20220508214405
:ctime:    20220508214405
:END:
#+TITLE: Half-Earth Socialism
#+CREATED: [2022-05-08 Sun]
#+LAST_MODIFIED: [2022-09-18 Sun 11:59]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ URL :: https://www.versobooks.com/books/3818-half-earth-socialism

An excellent book with a bold programme for [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][eco-socialism]].  Very readable.

Touches on [[id:4ed9e584-c86c-440d-9d10-35d52ea3a2d8][socialist calculation debate]], [[id:c6aaf865-bb7e-4930-94dc-1195e6e2b39a][Markets vs planning]], [[id:65f5cf66-af34-4021-ba45-36b6597a05a5][climate science]], [[id:e2cb34e4-326c-4f7d-96ce-3ee857c69d1d][climate breakdown]], [[id:d71070e2-ab67-4951-a1e9-cc9d51d619d0][eco-socialism]], [[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Project Cybersyn]], [[id:88ca7847-6209-445e-9158-20189a396784][Viable system model]], etc etc.

I like that it has some [[id:8d214cf1-3f82-4123-a69e-bb00c74e2675][speculative fiction]] and homage to things like [[id:95a17b04-906c-4dd1-8f62-75d731206aec][News from Nowhere]].

The accompanying game that you can play at https://play.half.earth is brilliant.

The main thing that it is lacking is a plan for [[id:3ddc4c92-47dc-4e82-ab5b-c1bc1fa36e34][revolutionary transition]].  But they're quite open about that, to be fair, happy to be a work of [[id:72c29bc6-6095-4b77-b69e-bf038a5bbb61][utopian socialism]].  The appropriation of the loaded term "Half-Earth" is questionable, but probably deliberately provocative.

Main nub of it:

- [[id:f19b404a-a0d4-4733-86c2-baf4c5e5e93f][Rewilding]] half the earth - [[id:f4c76bf6-24f5-4032-a2de-034f24d11fef][Half-Earth]]
- [[id:86659224-9876-4041-83c6-46fd646fc8ac][Veganism]]
- A rapid transition to [[id:2f0cc071-0606-495f-8194-535d09cae40c][renewable energy]], [[id:5011fc2d-c537-4c5a-b26b-6e42f11a53d3][decarbonisation]], and [[id:4903dc01-6cfd-49ec-8f0f-489db54af9a9][energy quotas]]
- Worldwide socialist planning

#+begin_quote
A kind of socialism based on the recognition of ecological limits and the limits of our understanding of the natural world.  Envronmental crisis can only be overcome by socialism, because only socialism allows humans to decide to manage economic growth (impossible under capitalism).  Socialists have to be environmentalists because they have to realise that total control over nature is impossible, the more we intervene in nature the more risks we take, such as pandemics and climate change.  Also arguging for Neurathian conscious control of the economy.  We propose rewilding half of the planet, having energy quotas, and veganism, to reduce our impact on nature, while also ensuring a good life for all.

-- paraphrasing from [[id:1c420e80-fcf9-421d-a0da-8df5809a737c][Drew Pendergrass and Troy Vettese on Half Earth Socialism]]
#+end_quote

In [[id:59ac676d-71b1-4e8f-a50f-ee409d4d1e4b][Climate Leviathan]] terms, I would suggest that their proposals fit as a mixture of [[id:111de04c-7bff-4034-ad09-254532b4aa60][Climate Mao]] and [[id:8c307836-6e03-4114-884a-cc23b934b43e][Climate X]] - i.e. staunchly socialist and non-capitalist, with a mixture of central authority and democratic decentralisation.

* Sections of the book

#+begin_quote
Think then of Half-Earth Socialism as a cookbook divided into four courses: the philosophical, the material, the technical, and the imaginative
#+end_quote

#+begin_src plantuml :file hes-sections.png :exports results
@startmindmap
skin rose
skinparam handwritten true
+[#eeeeee] HES
++ philosophical
+++_ philosophical principles for a new eco-socialism
+++_ contrasts
++++_ <&thumb-down> hegel
++++_ <&thumb-down> malthus
++++_ <&thumb-up> jenner
++ material
+++_ surveys array of solutions based on material characteristics
+++_ <&thumb-down> BECCS, nuclear power, Half-Earth
+++_ <&thumb-up> veganism, renewables + energy quotas, planetary rewilding
-- technical
---_ planning
----_ cybernetics
----_ cybersyn
----_ meteorology
----_ IAMs
-- imaginative
---_ speculative HES future
@endmindmap
#+end_src

#+RESULTS:
[[file:hes-sections.png]]

#+begin_quote
Our framework draws on [[id:9bdc71a4-aed8-44eb-920a-01968a5550fb][ecology]], [[id:f49a6cb2-a44c-453f-ac03-aebcfcc45eaa][energy studies]], [[id:2214ce24-8f32-4229-9377-c139fcd9c72e][epidemiology]], [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]], [[id:7c190ba7-68f8-438f-a95f-97031cec809d][history]], [[id:28f6f43f-cba8-43a6-9808-ee10cdc4d117][mathematics]], [[id:eeb51d4f-e015-4895-949f-6bf5c1248335][climate modelling]], [[id:72c29bc6-6095-4b77-b69e-bf038a5bbb61][utopian socialism]], and, yes, [[id:66556d59-cc01-4eb4-8b07-72b7ecb10097][neoliberalism]]
#+end_quote

#+begin_quote
We invite everyone from all liberatory traditions to join us in the revolutionary kitchen to think up many new recipes and work together to realize them
#+end_quote

** Philosophical

*** The neoliberals and the unknowability of nature.

#+begin_quote
The environmental crisis forces us to decide between controlling the market and controlling nature
#+end_quote

#+begin_quote
We follow his example of digging deep into the epistemic layers of our beliefs, but whereas Hayek covered the market with the veil of ignorance, we drape that veil over nature
#+end_quote

Don't plan nature, plan the market.

#+begin_quote
The environmental crisis, however, strains the neoliberals’ foundational postulates because they have to decide what is more complex and unknowable – the market or nature
#+end_quote

#+begin_quote
Rather than the conscious control of nature, the neoliberals seek one unconscious realm (nature) to be subdued by another (capital
#+end_quote

#+begin_quote
In response, we try to out-Hayek Hayek by arguing that nature is more unknowable than the market, and therefore far more deserving of our awe as an unconscious, decentralized, and unimaginably complex system
#+end_quote

*** Hegel, Malthus and Jenner

#+begin_quote
Each of these thinkers is a primogenitor of one of the three great lineages in environmental thought: Hegel’s [[id:8a7d7495-772f-40d9-acb9-e0e329ade740][Prometheanism]], [[id:41d72157-63ad-477e-ab90-601b3c303f26][Malthusianism]], and [[id:80052a10-1701-433f-8d9f-7b49bc46db62][Jennerite ecological scepticism]]
#+end_quote

#+begin_quote
One can go beyond Boulding by giving his framework a Jennerite twist: Earth is a natural machine, both ancient and alien, whose operating systems we will never fathom, and therefore it is wisest to let the ghost in the shell control the circuitry even if we do not always understand it ourselves
#+end_quote

#+begin_quote
The process of nature’s humanization stops not when it is realized, but when our species comes to recognize that this process undermines the basis of human freedom. Climate change, emerging zoonotic diseases, and other environmental crises make a mockery of this pretence of control. To bring the humanization of nature to an end, the collective consciousness (Geist) must become aware of its own limits
#+end_quote

#+begin_quote
The task of unbuilding makes clear that environmentalism isn’t so much the idealization of ‘pristine’ nature (though it is vital to protect intact ecosystems) but the recognition that it is still possible to repair our broken world
#+end_quote

** Material

#+begin_src plantuml :file hes-material.png :exports results
@startmindmap
skin rose
skinparam handwritten true
+[#eeeeee] material
++[#lightgreen] <size:30><&thumb-up>
+++ decarbonization
++++_ energy quotas
++++_ renewables
+++ rewilding
+++ 'natural' geoengineering
--[#ffbbcc] <size:30><&thumb-down>
--- BECCS
--- nuclear power
--- Half-Earth
@endmindmap
#+end_src

#+RESULTS:
[[file:hes-material.png]]

#+begin_quote
The goals of Half-Earth socialism are simple enough: prevent the [[id:fa91f034-1b2a-4995-ab40-daa52579cbd8][Sixth Extinction]], practise ‘natural geoengineering’ to draw down carbon through rewilded ecosystems rather than SRM, and create a fully [[id:2f0cc071-0606-495f-8194-535d09cae40c][renewable energy]] system
#+end_quote

#+begin_quote
way to save land would be to produce less energy, which is why Half-Earth socialism embraces quotas. The exact number can be debated, but we admire the target of the [[id:26fa47fd-0345-40bc-89eb-690254195c81][2,000-Watt Society]]
#+end_quote

#+begin_quote
An economic system resembling Half-Earth socialism can actually be found in recent history: Cuba’s [[id:ebafa43e-e085-41cc-9b98-51e4dbc4512d][Período Especial]]
#+end_quote

** Technical

[[id:adb0dfc9-3092-4af2-abd7-a3b4e0ed286f][Socialist planning]].

#+begin_quote
the third chapter that tackles the difficult problem of organizing production and distribution without the market.
#+end_quote

#+begin_quote
If it is necessary to prevent the market commodifying and controlling all of nature, and we also have a sense of what material goals we want to achieve, how then is it possible to organize production and consumption without a market? We draw on a range of influences, including [[id:f4b0a823-0d28-4b8b-b264-77d94811601d][Soviet cybernetics]] and mathematics, Chile’s ‘[[id:51ef15a8-1bdc-46c1-a55d-27f6da0d6f41][Cybersyn]]’ programme, [[id:1fc8a380-b4bf-4d06-b853-7d879b390e18][meteorology]], and cutting-edge [[id:68d8c6c5-e131-41f0-a0c3-04eb7f09a560][integrated assessment models]] (IAMs) used by climate scientists today.
#+end_quote

#+begin_quote
Half-Earth socialist planning is inspired by a slew of traditions, including Neurath’s [[id:8a21b75a-fe2e-40a7-b024-05cd811cd0da][in natura calculation]], Kantorovich’s [[id:ed597c35-e6aa-4469-861e-954f58691831][linear programming]], Beer’s Cybersyn, and the climate-economy models of Austria’s IIASA
#+end_quote

#+begin_quote
necessary to marry Kantorovich’s technical vision to Neurath’s [[id:4998a12f-0133-4c39-8be9-65f435119aaf][democratic socialism]], in which planners lay out their goals and constraints in natural units and then devise different plans that could be chosen by an informed public
#+end_quote

similar to [[id:ff5cc35d-fc3a-4d87-961d-3b3dc30b39fa][P2P Accounting for Planetary Survival]]?

#+begin_quote
In this chapter, we will detail how to organize a democratically planned economy in an age of ecological crisis
#+end_quote

#+begin_quote
Even a wisely managed eco-socialist utopia would still be plagued by some inefficiencies and shortages, as we will discuss later in the chapter. However, we believe that it is worth paying to gain other advantages, such as a [[id:9f68b60f-5537-414e-a3cc-16b6e13266c0][stable climate]], wondrous [[id:364238f1-41d1-4d41-846a-19d2c826d756][biodiversity]], and a respite from pandemics. Half-Earth socialism also promises the prospect of a unified humanity, peace, and equality, with an economy
#+end_quote

#+begin_quote
Half-Earth socialism requires a similar balancing act, supplying everyone with the material foundations for a good life – sustenance, shelter, education, art, health – while protecting the biosphere from destabilization
#+end_quote

[[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]]-y.

#+begin_quote
While the static, one-off calculations made using linear programming are a valuable tool in managing any complicated project – the method is ubiquitous in contemporary applied mathematics, including in planning renewable energy systems – we will need other tools to allow local administrators to meet the needs of the people they serve, while simultaneously achieving global goals such as rewilding or long-distance trade.
#+end_quote

#+begin_quote
There were two main currents that shaped planning debates in the Soviet Union over the following decade: the theory of mathematical optimization (e.g., linear programming) and the cybernetic theory of control, built around differential equations
#+end_quote

#+begin_quote
Thirdly, and perhaps more importantly, these loosely connected models create space for democracy, diversity, and political initiative, all unified in pursuit of national or even global goals
#+end_quote

#+begin_quote
Although Chile in the early 1970s might seem far removed from the global crises in the 2020s, Cybersyn is perhaps the closest historical analogue to Half-Earth socialism
#+end_quote

#+begin_quote
In our effort to understand how Half-Earth socialism could work in practice, we have undertaken a tour of socialist planning theory that has travelled from Leningrad to Vienna to Novosibirsk and finally to Santiago. At this point, we return to Siberia some two decades after Kantorovich’s work at CEMI began. 
#+end_quote

[[id:df31c872-19bc-4a6b-b30c-d334f93a176f][Olga Burmatova]]

#+begin_quote
As Kantorovich noted in his vision of multilevel linear programming, a planned society needs a way to balance an overarching national or global vision with sensitivity to local conditions
#+end_quote

[[id:65f5cf66-af34-4021-ba45-36b6597a05a5][climate science]]. [[id:a1b1a7f5-2c19-4161-9cfa-496a5e526c49][Downscaling]].  [[id:14730f2b-39e1-40cf-ac90-1b7d81899c8f][Data assimilation]].

#+begin_quote
All this data does not mean that we fully know nature, only that Half-Earth socialist planners would have access to the vital signs of the planet so they could modify humanity’s interchange with nature when necessary
#+end_quote

[[id:a198e19d-c3cd-46da-9e3b-aecee47c2303][János Kornai]], [[id:89a54bee-1758-45fc-8039-7a50f3131d65][shortage economy]]

** Imaginative

#+begin_quote
British socialist utopians in this period were a tightly knit group. [[id:da355bf5-129a-4bd9-8eda-94500a962ac9][Oswald]] might have been an acquaintance of [[id:f4605961-c757-46a9-8d2c-651af76a9bf9][William Godwin]], whose utopian writings provoked Malthus to write his Essay on Population. Godwin, who dabbled in vegetarianism, knew herbivorous radicals [[id:1384c425-6d66-4fe7-93aa-54cb09bbe664][Robert Owen]] and [[id:1d76fe22-bda5-41e0-8487-c93d6e6187df][Percy Shelley]]
#+end_quote

#+begin_quote
We hope that Orwell spins in his grave when Half-Earth socialism becomes a movement, radiant in its queer, feminist, vegetarian glory
#+end_quote

#+begin_quote
The other inheritor of the unorthodox Left was the [[id:4ef2c96b-942c-45af-a70e-cf228476afff][Frankfurt School]], whose theorists criticized the destruction wrought by the mindless conquest of nature. Adorno, Horkheimer, and Benjamin, as well as Alfred Schmidt and Herbert Marcuse, all contributed to the foundations of eco-socialism
#+end_quote

#+begin_quote
The point, however, is not simply to substitute socialist utopianism for Promethean Marxism, but rather to strive for a synthesis of the two to create a new, epistemically humble socialism.
#+end_quote

#+begin_quote
humanity faces a choice: to futilely try to further the humanization of nature through mad schemes like geoengineering, or to plan an economy within planetary boundaries.
#+end_quote

* Criticism

- [[https://nitter.noodlemaps.net/Leigh_Phillips/status/1532818367026892800?cursor=LBkWgICjzem%2F1cUqJQQRAAA%3D#r][Leigh Phillips (@Leigh_Phillips): "I am still want to publish a review of the...]]

** Too austere
#+begin_quote
By contrast, the authors’ vision is almost as austere as Pol Pot’s

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

#+begin_quote
And their plan to rewild half the Earth’s habitable surface would require perhaps the most preposterous proposal of all: the imposition of universal mandatory veganism (otherwise the numbers would never add up).

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

Doesn't seem that preposterous to me.

** Weak on transition

Weak on the question of [[id:3ddc4c92-47dc-4e82-ab5b-c1bc1fa36e34][revolutionary transition]], but to be fair they clear from the outset that this text is on programme.

#+begin_quote
Vettese and Pendergrass invite us to imagine that ‘the Half-Earth socialist revolution happens tomorrow’, but they do not explain how this might occur. Though they gesture towards a pro-Half Earth political coalition, its members are vaguely delineated: ‘there should be animal-rights activists and organic farmers there, as well as socialists, feminists and scientists’ – *constituencies that make up miniscule fractions of the eight billion-strong population they hope to corral*. As for broader layers such as social classes, Half Earth Socialism is largely silent. Like The Future is Degrowth and much of the left for the last half-century, *the authors assume that a ‘movements of movements’ – uniting various disparate and subaltern groups – will eventually gain enough power to confront capital*.

-- [[id:d61fc860-82c7-46e3-86a6-b21f7df116a3][Mish-Mash Ecologism]]
#+end_quote

* Misc

** Transition

#+begin_quote
In some countries it might follow a path similar to the rise of Nelson Mandela and the African National Congress in South Africa: a mixture of strikes, divestment, sabotage, elections, boycotts, and violence. In other countries a purely electoral strategy might work, but such a victory would only mark a new phase of struggle.
#+end_quote

* Reviews

- [[id:0031eb48-3914-44a7-911b-8a6670547044][Review of Half-Earth Socialism]] - my in process review
- [[https://sentientmedia.org/yearning-for-utopia-a-review-of-half-earth-socialism/][Yearning for Utopia: A Review of Half Earth Socialism]] 
- [[https://blogs.lse.ac.uk/lsereviewofbooks/2022/05/30/book-review-half-earth-socialism-a-plan-to-save-the-future-from-extinction-climate-change-and-pandemics-by-drew-pendergrass-and-troy-vettese/][Book Review: Half-Earth Socialism: A Plan to Save the Future from Extinction,...]] 
- https://journals.sagepub.com/doi/abs/10.1177/03098168221101949h
