:PROPERTIES:
:ID:       bf1529d7-260c-427c-9202-29e620b9daf9
:mtime:    20211127120939 20210724222235
:ctime:    20210724222235
:END:
#+title: When Preston Meets the Doughnut
#+CREATED: [2021-02-04 Thu 21:33]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Really enjoyed the When Preston Meets the Doughnut webinar.  Inspiring to hear more about what's going on in [[id:174ed8ef-0f25-4a92-bdf3-c06dcaf6493c][Preston]], and to see how Kate Raworth has been working on adaptations of [[id:82d6ea4e-db58-42dc-9974-903a1d68a139][Doughnut Economics]] to the city-level.

[[id:8572adc8-6416-4f24-bb33-f74bdb0ff88b][Community wealth building]], and the ideas of thriving but living within planetary boundaries, seem to work well together.

- [[https://www.eventbrite.co.uk/e/when-preston-meets-the-doughnut-community-wealth-building-c21-economics-tickets-131573386541][When Preston Meets the Doughnut: Community Wealth Building & C21 Economics Ti...]]
