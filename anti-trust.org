:PROPERTIES:
:ID:       eefd8420-fa47-4239-a0f3-ad6ee3d0ed7e
:mtime:    20221016165350
:ctime:    20221016165350
:END:
#+TITLE: Anti-trust
#+CREATED: [2022-10-16 Sun]
#+LAST_MODIFIED: [2022-10-16 Sun 16:55]

#+begin_quote
Antitrust laws were created in the United States to promote competition and restrain the abusive practices of monopolies (then called “trusts”) in the late 19th century.

-- [[id:10d85685-be02-448b-88f4-2d2c927d5048][Digital Ecosocialism: Breaking the power of Big Tech]]
#+end_quote

#+begin_quote
Enter the trustbusters, led by Senator John Sherman, author of the 1890 Sherman Act, America’s first antitrust law. In arguing for his bill, Sherman said to the Senate: “If we will not endure a King as a political power we should not endure a King over the production, transportation, and sale of the necessaries of life. If we would not submit to an emperor we should not submit to an autocrat of trade with power to prevent competition and to fix the price of any commodity

-- [[id:f613abf1-a680-410a-9be7-871f52a04c3e][The Internet Con]]
#+end_quote
