:PROPERTIES:
:ID:       c37608cf-02ac-4236-bacc-e757e3bae010
:mtime:    20211127120942 20210724222235
:ctime:    20200404145333
:END:
#+TITLE: Digital urban planning

In [[id:990b268e-6564-416d-accc-ae40070a2a41][Future Histories]] Lizzie O’Shea is using urban planning as an analogy for thinking about how we could design our digital spaces. Riffing off [[id:9900a9a0-d5ad-4754-a16f-c100fe20b5fd][Freud]]’s thoughts about the mind as a city, and [[id:53675183-4947-47e8-b328-fac299313391][Jane Jacobs]]’s work on cities and planning.

I’m liking this, I was thinking about it recently, with an online presence being like a person’s home on the web. Taking it up a layer you think about digital urban planning, how these homes (and other things) fit together to make a city. I like it as a frame.  (Probably because I’ve been living in a big city the last 10 years.)

* [[id:d921c6fa-6fbc-4e8e-8881-b35ba6d48019][The vulgar and the strange (web)]]
* [[http://thoughtstorms.info/view/BlogosphereAsACity][BlogosphereAsACity]]
  
#+begin_quote
If you think of the web as an analogue of the city environment in which we live, at present the urban master plan is imposed by Facebook , Instagram, Medium, Twitter and all the other social networks: the buildings must be of a certain height, with a predetermined number of windows per facade, and the colors that can be used are reduced to a minimum.

-- [[https://www.vice.com/it/article/pky7vv/come-creare-blog-giardino-digitale][Giardini digitali: come creare un blog davvero personale]] 
#+end_quote
