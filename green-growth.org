:PROPERTIES:
:ID:       32cbb06f-0bbd-4ad7-a116-6260f3e1bbe0
:mtime:    20231007172011 20220708090057
:ctime:    20220708090057
:END:
#+TITLE: green growth
#+CREATED: [2022-07-08 Fri]
#+LAST_MODIFIED: [2023-10-07 Sat 17:20]

#+begin_quote
In sum, green growth scenarios play loose with science, assume incredibly unjust arrangements, and gamble with the future of humanity—and all of life on Earth—simply to maintain ever-increasing levels of aggregate output in high-income countries, which, as we will see, is not even needed.

-- [[id:8cf3f09a-398f-4bee-9207-2a88ee516d05][On Technology and Degrowth]]
#+end_quote
