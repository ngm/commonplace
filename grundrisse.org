:PROPERTIES:
:ID:       c8047940-87d3-4516-8bd6-fcbbb5563629
:mtime:    20221022135118
:ctime:    20221022135118
:END:
#+TITLE: Grundrisse
#+CREATED: [2022-10-22 Sat]
#+LAST_MODIFIED: [2022-10-22 Sat 13:51]

[[id:03dce730-0ec6-4f6e-86e1-653ad8d1587b][Marx]]’s notebooks on which he worked in preparation for writing Capital .
