:PROPERTIES:
:ID:       b1006504-16a3-4913-92f6-a8669284113c
:mtime:    20211127120931 20210724222235
:ctime:    20210724222235
:END:
#+title: Cloudron vs Yunohost
#+CREATED: [2020-12-13 Sun 19:53]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

These are my rough notes from when I was looking for a self-hosting platform.  I tried both [[id:908b88e7-6702-4ca6-ae89-690a1242a10d][Cloudron]] and [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]], and eventually stuck with YunoHost.

----

YunoHost has more apps and a stronger ethos. It would be a no-brainer for me, but for a couple of things:

- I don't think installed apps run in any kind of isolation on Yunohost, whereas Cloudron is all containers
- Cloudron apps seem well maintained, whereas Yunohost looks more hit and miss, dependent on volunteer contributions

So it kind of swings on if I have time to properly maintain it, which was partially what I was hoping to avoid having to do.

But now just reading more about Yunohost's aims - https://yunohost.org/#/faq and https://yunohost.org/#/project_organization - I feel like I can't *not* support them.  If if needs a bit more time going in, so be it... gotta live your principles if you can.  So that's kind of made my mind up.  I'll definitely start with Yunohost for personal stuff and switch only if I hit some major bumps.

If this was for work purposes, I would probably have to choose Cloudron right now, for the sake of robustness.

An excellent thread on the topic here: https://forum.cloudron.io/post/10860

A good post from [[https://socialism.tools][socialism.tools]] that mirrors my thoughts pretty closely: [[https://socialism.tools/cloudron-yunohost-self-host-app-showdown/][Cloudron vs. YunoHost - the self-host app showdown]] 

* Approach

 Cloudron basically sets up docker and then pulls in containers.

YunoHost installs Debian and configures the software directly on top of that, mingled together.
  
* Ethics
  
 https://yunohost.org/#/faq and https://yunohost.org/#/project_organization 
 
The fact YunoHost has the Anarchist Library as an app tells you something about its ethics :)  (though I think it's partly enabled by it building on top of [[id:d819826b-1a39-437a-a59d-8aa61fdbc63b][Debian]], which also bundles that library IIRC).

* Security model
  
  #+begin_quote
Yunohost is best if you are price sensitive but it doesn't use containers and really depends if you want to take that risk.
  #+end_quote

  #+begin_quote
 > keeping those apps updates is a lot of work. I imagine it's a lot of thankless work.

> It's a side effect of not using containers.
  #+end_quote

Interesting, yunohost seems to be idealogically opposed to things such as docker and ansible: https://forum.yunohost.org/t/suggestion-lemmy-une-alternative-federee-a-reddit/10508/2

* Resources
- https://news.ycombinator.com/item?id=17999854
- https://news.ycombinator.com/item?id=22191416
