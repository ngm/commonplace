:PROPERTIES:
:ID:       e94157ce-4b84-4cf7-a700-ccc1958ce852
:mtime:    20211127121006 20210724222235
:ctime:    20210724222235
:END:
#+title: Setting up Apache sites on a new port
#+CREATED: [2021-03-30 Tue 10:57]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

For our staging environments, it's useful to have multiple ports available to test different features that are in flight at the same time.

Notes on the basic config for this, in [[id:af2e799b-0fe2-4359-8c09-9912cffba881][ufw]] and [[id:bdb622c6-35e3-494e-a528-bde45e9451a5][Apache]].

* Firewall

#+begin_src shell
ufw allow 8443
#+end_src
  
* Set up the config in Apache

** Edit /etc/apache2/ports.conf

   Add

   #+begin_quote
   Listen 8443
   #+end_quote

   to the relevant part of the file.

** Set up Apache site config

   Same as usual, except

   #+begin_src xml
   <VirtualHost *:8443>
   #+end_src

