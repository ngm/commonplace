:PROPERTIES:
:ID:       a8bc2cef-369a-4d8d-9738-71bccf87dfa9
:mtime:    20220113142856 20220108191119 20220108125950
:ctime:    20220108125950
:END:
#+TITLE: Britain is better off without Bristol's monument to Colston
#+CREATED: [2022-01-08 Sat]
#+LAST_MODIFIED: [2022-01-13 Thu 14:36]

Articulated here: [[https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down][The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The...]] 

Fuck yes.

Not in a public celebratory view, anyway.  It's still in a museum to reckon with the past.

[[id:4b0335ff-d306-40db-887d-1b4e7839571c][Colston]].  [[id:5177f316-2d21-46de-a6d8-271529e7f166][Slavery]].

* Because

+ Colston was a slave trader, one of Britain's wealthiest
+ [[id:86cc72a0-58eb-4b2d-8f4e-363db59e0a0c][Figures such as Colston are not simply remote characters from history]]
+ [[id:ba58ea6b-bd21-44e7-bad4-d0654928c5b9][Britain's history contains astonishing levels of greed and cruelty]]
+ [[id:da3cbec3-18e9-4c7b-97ef-87657a3dc885][Acknowledging historic injustices is part of building a more equal society today]]
+ [[id:8193c80e-50ac-4595-ab89-818bc0f2d7d0][The built environment reinforces cultural messages]]

* But

+ Tackling racism requires a lot more than removing statues

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Agreement level :: [[id:cca69ddf-2626-4c72-a23a-1077ddd610ca][Without a doubt]]
