:PROPERTIES:
:ID:       bd7c0199-4d9e-42b2-b579-747a6c713dc7
:mtime:    20211127120938 20210724222235
:ctime:    20210724222235
:END:
#+title: participatory culture
#+CREATED: [2021-05-01 Sat 08:06]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
Understanding participatory culture as just a numbers game can be part of the problem. Participation is not only about quantity but, perhaps more importantly, about how it materializes—who speaks and in what way—and technology can hide understanding of these processes.

-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
#+end_quote

#+begin_quote
Creating a participatory culture requires thinking beyond technology and participation numbers. It entails building an infrastructure that continuously supports citizens in participating. Cities can look to open source and its decades of experience in creating this type of infrastructure to find valuable lessons.

-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
#+end_quote

#+begin_quote
Open sourcing a city requires helping citizens participate in the best possible conditions and paying attention to their own contexts and requirements. This is important since the modern understanding of "citizens" is articulated around the idea that they renounce their own power in exchange for security. This is called the [[id:6df4b150-82da-43df-97d4-a321dc9240d5][social contract]], and it is a foundational concept in constructing the contemporary idea of "nation." It is a weak form of democracy, relying on regular yet infrequent participation (and sometimes even discouraging active participation).

-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
#+end_quote
