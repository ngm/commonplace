:PROPERTIES:
:ID:       fd68df82-2639-4508-bc6b-959b01919dee
:mtime:    20220526122333
:ctime:    20220526122333
:END:
#+TITLE: cap-and-trade
#+CREATED: [2022-05-26 Thu]
#+LAST_MODIFIED: [2022-05-26 Thu 12:23]

#+begin_quote
Cap-and-trade creates a fungible right to inflict environmental harm, and the market rather than scientific expertise or democratic opinion decides who exercises such rights

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
Of their many different approaches, cap-and-trade is the neoliberals’ most sophisticated and widespread environmental policy, one that has been applied to everything from dredging cold-water sponges, mercury pollution, acid rain, and carbon emissions

[[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

