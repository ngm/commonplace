:PROPERTIES:
:ID:       78be4083-23fa-4e89-ac7c-c1673a32984f
:mtime:    20211127120857 20210724222235
:ctime:    20210724222235
:END:
#+title: Omnibear
#+CREATED: [2021-06-12 Sat 10:58]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ URL :: https://omnibear.com

A Micropub client browser extension.
