:PROPERTIES:
:ID:       5562fdc2-0387-4ebb-b37b-b7bc7e8cf48b
:mtime:    20220731095127 20220628214513
:ctime:    20220628214513
:END:
#+TITLE: The humanization of nature
#+CREATED: [2022-06-28 Tue]
#+LAST_MODIFIED: [2022-07-31 Sun 09:51]

#+begin_quote
The humanization of nature is the process by which humanity overcomes its alienation from nature by instilling the latter with human consciousness through the process of labour – transforming wilderness into a garden

-- [[id:ca693c81-2a60-4389-abf5-5f534c53c4a0][Half-Earth Socialism]]
#+end_quote

#+begin_quote
The humanisation of nature is the process by which humanity employs labour as a means to re-direct flows of energy within natural systems to achieve social ends. Or as Marx puts it in Capital: ‘man confronts the material of nature as one of her own forces. He sets in motion arms and legs, head and hands, the natural forces of his body, in order to appropriate the material of nature in a form suitable for his own needs. By thus acting through this motion on the nature which is outside him and changing it, he at the same time changes his own nature’.

-- [[https://www.historicalmaterialism.org/book-review/humanisation-nature-and-naturalisation-marxism][The Humanisation of Nature and the Naturalisation of Marxism | Historical Mat...]] 
#+end_quote
