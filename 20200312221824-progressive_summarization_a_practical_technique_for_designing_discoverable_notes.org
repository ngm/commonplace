:PROPERTIES:
:ID:       0a19a704-020c-41f9-8f6c-7cb67b1e238d
:mtime:    20211127120758 20210724222235
:ctime:    20200312221824
:END:
#+TITLE: Progressive Summarization: A Practical Technique for Designing Discoverable Notes

- source:  [[https://fortelabs.co/blog/progressive-summarization-a-practical-technique-for-designing-discoverable-notes][Progressive Summarization: A Practical Technique for Designing Discoverable N...]] 

#+begin_quote
I want to suggest an alternative to all the approaches above: what you read is good and useful and very important, you’re just reading it at the wrong time
#+end_quote

#+begin_quote
The challenge is knowing which knowledge is worth acquiring. And then building a system to forward bits of it through time, to the future situation or problem or challenge where it is most applicable, and most needed.
#+end_quote

#+begin_quote
The four top-level categories of [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][P.A.R.A.]] — Projects, Areas, Resources, and Archives — are designed to facilitate this process of forwarding knowledge through time.  

- By placing a note in a project folder, you are essentially scheduling it for review on the short time horizon of an individual project 
- Notes in area folders are scheduled for less frequent review, whenever you evaluate that area of your work or life 
- Notes in resource folders stand ready for review if and when you decide to take action on that topic 
- And notes in archive folders are in “cold storage,” available if needed but not scheduled for review at any particular time
#+end_quote


[[id:637ea6a9-fe3e-457d-844b-8ce5ba908f22][Note-first approach]] sounds interesting and the kind of thing i like.  What does it mean in practice?


#+DOWNLOADED: screenshot @ 2020-03-21 11:17:23
[[file:2020-03-21_11-17-23_screenshot.png]]


layers:
- layer 0: is the original, full-length source text
- layer 1: notes
- layer 2: bold passages
- layer 3: highlighed passages
- layer 4: mini-summary
- layer 5: remix

OK, I like layer 1 - basically just pull in whatever you want, then you come back to it and refine it later.  make sure you've got it, somewhere you can discover it.

#+begin_quote
[[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][Progressive Summarization]] turns the jungle into an archipelago of islands. It reveals your personal information landscape — the unique topography of your goals, values, interests, and pursuits. With a clear landscape, you gain the ability to steer. Toward what you like, or don’t. Toward what makes you comfortable, or what doesn’t. Toward what you 
need, or what you want. You are the pilot, so you decide.
#+end_quote

Yeah, I like the layers approach.  Where layer 0 is literally the source text.  So that's why it's useful to put the source for articles in the note.  Even that in itself, with nothing else added, could be potentially useful to future me.
