This is the first in a series of regular monthly updates exploring ecosocialism and ICT and all things adjacent.  Welcome!


# Reclaiming the stacks

Last month I finished the Open University module that I started back in September 2022 ([YXM830](https://commonplace.doubleloop.net/yxm830)).  The course provided the opportunity to undertake a research project on the topic of your choice, which I used to research how ICT is implicated in present-day ecological and social crises, and what we can do about it.

My final output is now online here: [Reclaiming the stacks: how ICT workers can contribute to a transition to ecosocialism](https://commonplace.doubleloop.net/reclaiming-the-stacks-ecosocialism-and-ict), and a shorter blog post on the topic was recently published on laps.ripe.net:[ Amazon Doesn’t Care How You Heat Swimming Pools: ICT and Ecosocialism](https://labs.ripe.net/author/neil-mather/amazon-doesnt-care-how-you-heat-swimming-pools-ict-and-ecosocialism/).

I'm very pleased with where I got to with the project - it helped me to fill in plenty of gaps in my background knowledge - but there is a whole lot more I'd like to do with the topic to take it further.  Starting this newsletter and nudging myself to write on the topic regularly is one way to keep that momentum going.

### So where next?

*First thing I'd like to do*: make the research more usable to others.  A key motiviation was to learn for myself various ways that someone working in ICT (e.g. me) could contribute to an [ecosocialist transition](https://commonplace.doubleloop.net/ecosocialist-transition) by direct, practical action.  I pulled plenty of such initiatives and actions out of books and papers, but they're not that easy to find in my final output.  I had started putting them in a database with a simple website to make them searchable and linkable for my own use - <https://reclaim.doubleloop.net>.  I will work on this more - but ultimately I'd like to have them in a community-curatable format.  Perhaps Anagora or Fedwiki could help with this.

*Second*: situate the initiatives in a larger theory of change.  How could supporting a libre software project, or the right to repair movement, for example, add up to being part of a real transition?  While I was primarily looking at them for examples of grassroots initiatives, some of the papers I reviewed were also painting a broader stroke strategy (e.g. [Platform Socialism](https://commonplace.doubleloop.net/platform-socialism), [Digital Ecosocialism: Breaking the power of Big Tech](https://commonplace.doubleloop.net/digital-ecosocialism-breaking-the-power-of-big-tech)).  It would be interesting to re-review from that systemic perspective.  Some of them (e.g. [Digitalization and the Anthropocene](https://commonplace.doubleloop.net/digitalization-and-the-anthropocene)) started hinting at leverage points from systems thinking, and I would like to explore where systems theory and an ecosocialist ICT intersect.


# Inputs

Here's what I've been reading, listening to, and watching in the last month or so on the topics of ecosocialism and ICT.


## Long reads

### Capital is Dead

I just re-read [Capital is Dead](https://commonplace.doubleloop.net/capital-is-dead).  I find the [Vectoralism](https://commonplace.doubleloop.net/vectoralism) angle of McKenzie Wark quite compelling.  The idea is that the consolidation of power through the control of information vectors has led to a whole new system (and corresponding classes and struggles) that is in fact worse than capitalism.  There is some critique as to whether this naming of an entirely new system and class is necessary, but regardless, there is lots of good analysis in the book.  Plenty to say about [intellectual property](https://commonplace.doubleloop.net/intellectual-property).  The book doesn't outline many practical things to do to counter the vectoral class, but a read through of Wark's earlier [A Hacker Manifesto](https://commonplace.doubleloop.net/a-hacker-manifesto) might fill in some of those gaps.  To some degree, I see my [reclaim the stacks](https://commonplace.doubleloop.net/reclaim-the-stacks) project as one effort to outline a [praxis for the hacker class](https://commonplace.doubleloop.net/praxis-for-the-hacker-class).


### The Entropy of Capitalism

After listening to an interview by Cosmopod ([Entropy and the Capitalist System with Robert Biel](https://commonplace.doubleloop.net/entropy-and-the-capitalist-system-with-robert-biel)), I've started reading [The Entropy of Capitalism](https://commonplace.doubleloop.net/the-entropy-of-capitalism).  It takes a more robust systems theoretical approach to raging against what we often colloquially call 'the system'.

I'm finding particularly interesting the way he relates topics of self-organisation.  I've always been drawn to self-organising, agent-based systems, etc, as opposed to top-down models.  But I've thought before how there's a tension for me in that neoliberal ideas also have a focus on self-organisation - individualism, decentralised information exchange in markets, etc.

> neo-liberalism exploits the proposition (true in itself) that self-generated order is better than designed, as an excuse to outlaw social projects or any attempt to better the human condition.

Biel outlines how self-organisation could be used from a socialist perspective.

> Our answer to this would have to focus on a particular aspect of information theory which emphasises ‘information about the future’: in human systems, structural emergent order inevitably has a strong dose of agency. The future is not predetermined; we can choose it, on the basis of existing possibilities.

This reminds me of some of Thomas Swann's approaches to the [viable system model](https://commonplace.doubleloop.net/viable-system-model) in [Anarchist Cybernetics](https://commonplace.doubleloop.net/anarchist-cybernetics), and of the exploration of [Nested-I](https://commonplace.doubleloop.net/nested-i) in [Free, Fair and Alive](https://commonplace.doubleloop.net/free--fair-and-alive).


## Reading

-   [André Gorz's Non-Reformist Reforms Show How We Can Transform the World Today](https://commonplace.doubleloop.net/andre-gorzs-non-reformist-reforms-show-how-we-can-transform-the-world-today). Overview of André Gorz's ideas on non-reformist reforms.  Something that might help us on the way to transition.
-   [An Ecosocialist Strategy to Win the Future](https://commonplace.doubleloop.net/an-ecosocialist-strategy-to-win-the-future). "When we propose socialism as a system that will save us from capitalism, it is not enough to simply assert that socialist revolution is necessary because society will not survive without it. To those already familiar with the dire need to overthrow capitalism, these statements are nothing more than truisms used to assert our own radical positions." Yes.


## Listening

-   [Entropy and the Capitalist System with Robert Biel](https://commonplace.doubleloop.net/entropy-and-the-capitalist-system-with-robert-biel).  Fascinating interview with Robert Biel by Cosmopod.
-   [McKenzie Wark, Molecular Red](https://commonplace.doubleloop.net/mckenzie-wark-molecular-red) and [McKenzie Wark, General Intellects](https://commonplace.doubleloop.net/mckenzie-wark-general-intellects).  A couple of interviews with McKenzie Wark on prior books to Capital is Dead.
-   [A Beginner's Guide To Neoliberalism](https://commonplace.doubleloop.net/a-beginners-guide-to-neoliberalism). Relistening to these after recommending to a friend.  Still very handy podcast series.  What is this dominant system and where did it come from?
-   [Eat Sleep Protest Repeat](https://commonplace.doubleloop.net/eat-sleep-protest-repeat). Discussion of tactics, strategies, actions in climate action.
-   [Tout Le Monde Déteste La Police](https://commonplace.doubleloop.net/tout-le-monde-deteste-la-police). On the 2023 French pension reform unrest.
-   [The Myth of Freedom Under Capitalism](https://commonplace.doubleloop.net/the-myth-of-freedom-under-capitalism).  Documentary podcast from Upstream.  Despite what we're constantly being told, living under capitalism we are far from free.  Including interviews with Jessica Gordon Nembhard, David Bollier, and more.


## Watching

-   [Vesna Manojlovic - The Environmental Impact of Internet: Urgency, De-Growth, Rebellion](https://commonplace.doubleloop.net/vesna-manojlovic-the-environmental-impact-of-internet-urgency-de-growth-rebellion).  Presentation by Vesna Manojlovic on ICT and environmentalism.  Check out the downloadable slides for lots of great resources.


# Until next time

See you in next newsletter.  Until then, you can find latest streams of thoughts over at [my website](https://doubleloop.net).

Neil
