:PROPERTIES:
:ID:       d02dc796-2ec8-47a5-9de7-90d4cfa019e2
:mtime:    20240427121733
:ctime:    20240427121733
:END:
#+TITLE: org-agenda
#+CREATED: [2024-04-27 Sat]
#+LAST_MODIFIED: [2024-04-27 Sat 12:17]

Part of [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]].

I use it a lot for both work and personal life.
