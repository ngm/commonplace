:PROPERTIES:
:ID:       b0a92d8a-6700-4626-a590-08399b16b9f1
:mtime:    20230611154026
:ctime:    20230611154026
:END:
#+TITLE: ChatGPT
#+CREATED: [2023-06-11 Sun]
#+LAST_MODIFIED: [2023-06-11 Sun 15:40]

#+begin_quote
For inference (i.e., conversation with ChatGPT), our estimate shows that ChatGPT needs a 500-ml bottle of water for a short conversation of roughly 20 to 50 questions and answers, depending on when and where the model is deployed. Given ChatGPT’s huge user base, the total water footprint for inference can be enormous.

-- [[https://themarkup.org/hello-world/2023/04/15/the-secret-water-footprint-of-ai-technology][The Secret Water Footprint of AI Technology – The Markup]]
#+end_quote
