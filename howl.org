:PROPERTIES:
:ID:       240e7d27-c285-472b-bbb5-22000977ada0
:mtime:    20240428082233 20211127120813 20210905134948
:ctime:    20210905134948
:END:
#+TITLE: Howl
#+CREATED: [2021-09-05 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

+ A :: [[id:ee51cdc7-aab2-4663-b3a1-12f48e7b9988][poem]]
+ Written by :: [[id:9db53d58-569b-4d4d-b897-dcc60d2e4c04][Allen Ginsberg]]
+ Found at ::  https://www.poetryfoundation.org/poems/49303/howl

part II is [[id:5fe249b4-7286-425b-af9a-a33eda8c4c1c][Moloch]]
