:PROPERTIES:
:ID:       38787646-aaf5-41b6-be47-f78cd5222fca
:mtime:    20211127120824 20210724222235
:ctime:    20200628231553
:END:
#+title: Technology does not guarantee progress

#+begin_quote
Technology does not guarantee progress. It is, instead, often abused to cause regress.

-- [[id:a158d0b8-0f1e-441d-bd46-ccba61867c48][Goodbye iSlave]] 
#+end_quote

