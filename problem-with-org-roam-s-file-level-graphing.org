:PROPERTIES:
:ID:       d4bca8ee-17ab-4b25-96e7-05e48392dba2
:mtime:    20211127120953 20210724222235
:ctime:    20210724222235
:END:
#+title: Problem with org-roam's file-level graphing
#+CREATED: [2020-11-28 Sat 14:14]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

Slight problem at the mo being that org-roam's file-level graphing doesn't exclude the nodes in your exclude matcher.  I started a [[https://org-roam.discourse.group/t/graph-nodes-not-excluded-on-file-level-graphs/1002][thread to ask about that]].  

First thing that I tried was putting ~                   ,@(org-roam-graph--expand-matcher 'file t)~ in ~org-roam-graph--build-connected-component~.  That didn't work, because

    #+begin_src elisp
         (files (or (if (and max-distance (>= max-distance 0))
                        (org-roam-db--links-with-max-distance file max-distance)
                      (org-roam-db--connected-component file))
                    (list file)))
    #+end_src

    already includes everything in ~files~, so saying 'and not these other files' after the fact doesn't help.  So next I've just simply hacked the SQL in ~org-roam-db--links-with-max-distance~.  Literally changes the raw SQL query.  Obviously not sustainable, but gets me through what I want to do for today, and something to return to.
