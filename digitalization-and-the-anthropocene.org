:PROPERTIES:
:ID:       9b8ea846-9247-47b3-b775-082ba1a26630
:mtime:    20230219175343 20230218172919 20230204104720
:ctime:    20230204104720
:END:
#+TITLE: Digitalization and the Anthropocene
#+CREATED: [2023-02-04 Sat]
#+LAST_MODIFIED: [2023-02-19 Sun 18:19]

+ URL :: https://www.annualreviews.org/doi/10.1146/annurev-environ-120920-100056

* Handwritten notes

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-01.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-02.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-03.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-04.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-05.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-06.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-07.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-08.jpg]]

#+begin_quote
Digitalization has the ability to influence what Meadows highlighted as some of the strongest leverage points over system dynamics: those that alter information flows and controls, rules of the system, the power structures and dynamics that uphold existing rules, and the mindsets that define them.

-- page 19
#+end_quote

** [[id:29e973a9-1671-46f5-b7a7-1afb3d3d509e][Leverage points]].

*** Rules and feedback
#+begin_quote
The first leverage point is the reformalization of rules and feedback, as discussed in the preceding section. Important options include mandatory data sharing in big tech and with urban/local administrations as condition for license; default data management as data commons or data trusts; a requirement for circular electronics designed for increased life span, repair, and reuse; and the use of tax incentives and price signals as feedback to steer digital use cases toward low environ- mental impact.

-- page 19
#+end_quote

*** Structures

#+begin_quote
The second leverage point is the modification of structures and the creation of new ones. Digitalization can play a crucial role by offering a holistic data-based Earth system understanding (116), which would translate into an informational governance that leverages the use of information to drive innovations in governance mechanisms and institutions

-- page 20
#+end_quote

*** Goals

#+begin_quote
The third leverage point is the resetting of goals, at both individual and societal levels, away from aspirational resource-intensive consumption (e.g., private cars, mansions) toward positive outcomes for the commons and society.

-- page 20
#+end_quote

*** Mindsets

#+begin_quote
The fourth and broadest leverage point is a mindset paradigm shift away from disjointed economic, legal, natural, or cultural systems toward a synthetic consideration, as captured by the notion of the Anthropocene

-- page 21
#+end_quote

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-09.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-10.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-11.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-12.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-13.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-14.jpg]]

#+ATTR_HTML: :width 100%
[[file:files/handwritten-notes/digitalization-and-the-anthropocene/page-15.jpg]]
