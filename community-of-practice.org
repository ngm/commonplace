:PROPERTIES:
:ID:       9708006a-52ad-4864-90c0-9f0f08616382
:mtime:    20230514161710 20220212114059 20211127120915 20211012230643
:ctime:    20211012230643
:END:
#+title: Community of practice
#+CREATED: [2021-05-01 Sat 15:33]
#+LAST_MODIFIED: [2023-05-14 Sun 16:17]

 #+begin_quote
 a group of people who "share a concern or a passion for something they do and learn how to do it better as they interact regularly".

 -- [[https://en.wikipedia.org/wiki/Community_of_practice][Community of practice - Wikipedia]] 
 #+end_quote

I like the stuff on moving from network to community of practice in [[id:762e9a61-d3d4-4df6-9692-ce51bb97d403][Margaret Wheatley]] and Deborah Frieze's paper - 
[[id:86571237-6070-40aa-a70b-feeb456930d4][Lifecycle of Emergence: Using Emergence to Take Social Innovation to Scale]]

