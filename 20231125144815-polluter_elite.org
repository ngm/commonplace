:PROPERTIES:
:ID:       0899a9ab-5fb7-4f33-b1a8-6dfd2dfbd3e8
:END:
#+title: polluter elite

#+begin_quote
polluter elite”: anyone with a net worth over $1m who reinforces the use of fossil fuel technologies through their high carbon consumption, investments in polluting companies and, most importantly, political influence. “The polluter elite have blocked an alternative history where the destruction of extreme weather events and air pollution could have been reduced,” he told the Guardian.

-- [[id:87c7ed1d-2c39-459e-a5da-879868ee8b07][The great carbon divide]]
#+end_quote
