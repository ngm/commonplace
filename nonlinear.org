:PROPERTIES:
:ID:       e663d1d7-7e24-48aa-b293-ae66a72799bd
:mtime:    20211127121004 20210818091422
:ctime:    20210818091422
:END:
#+TITLE: nonlinear
#+CREATED: [2021-08-18 Wed]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

The whole is more than the sum of the parts.

You can't just sum up all the individual interactions and derive what the whole system is doing.
