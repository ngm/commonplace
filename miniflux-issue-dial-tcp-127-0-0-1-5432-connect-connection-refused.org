:PROPERTIES:
:ID:       1ed0cb4a-8e8c-4e46-9c3b-602659d28c96
:mtime:    20230324211223
:ctime:    20230324211223
:END:
#+TITLE: miniflux issue: dial tcp 127.0.0.1:5432: connect: connection refused
#+CREATED: [2023-03-24 Fri]
#+LAST_MODIFIED: [2023-03-24 Fri 21:15]

I was getting the error:

#+begin_quote
dial tcp 127.0.0.1:5432: connect: connection refused
#+end_quote

When trying to load up my install of [[id:d4d7efc2-c373-4a63-8da0-df8b81cd098d][miniflux]] on [[id:fe9521ff-3672-49e4-a680-fa70ba4022cb][YunoHost]].

Turns out it was because postgresql had stopped running - don't know why.  Starting the postgresql service from Tools -> Services sorted it out.
