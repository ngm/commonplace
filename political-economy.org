:PROPERTIES:
:ID:       40cd8dcb-9f30-4011-b2b8-a3d3be94371e
:mtime:    20221210111055 20211127120828 20210724222235
:ctime:    20200712201142
:END:
#+title: Political economy

The interaction of [[id:bcbec977-296c-4276-9b23-919c204a4ef7][power]] and [[id:9c9043ce-3119-4e5f-a7a2-f960b5662c27][value]].

#+begin_quote
Political economy is the study of production and trade and their relations with law, custom and government; and with the distribution of national income and wealth.

-- [[https://en.wikipedia.org/wiki/Political_economy][Political economy - Wikipedia]] 
#+end_quote

I am am particularly interested in [[id:d5fb2efd-2143-4393-af78-5beacd47fd86][technology and political economy]].

* Resources

  - [[https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy][The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy]]
  
* Flashcards

** Political economy
   :PROPERTIES:
   :ANKI_DECK: Default
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1596558920827
   :END:

*** Front
    What is political economy?

*** Back
The study of production and trade and their relations with law, custom and government

and with the distribution of national income and wealth.
