:PROPERTIES:
:ID:       fd23a3eb-0cfe-4db0-9704-87cc381ea683
:mtime:    20211127121016 20210724222235
:ctime:    20210724222235
:END:
#+title: Branch
#+CREATED: [2021-06-19 Sat 11:20]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

+ URL :: https://branch.climateaction.tech/
+ Strapline  :: A Sustainable Internet for All

Lovely online magazine on [[id:89446ec8-bf0d-4df8-9f17-8fc4638de8dd][sustainable tech]].
