:PROPERTIES:
:ID:       a7002180-8d76-4a54-8b6e-38c72d78a334
:mtime:    20220828152546 20220414210357 20220330193519
:ctime:    20220330193519
:END:
#+TITLE: Net zero
#+CREATED: [2022-03-30 Wed]
#+LAST_MODIFIED: [2022-08-28 Sun 15:30]

#+begin_quote
It is not just environmentalists who champion net zero. Last week, Alistair Phillips-Davies, the chief executive of one of the big six energy companies, SSE, said: “Net zero is not only an environmental decision, it’s a rational economic one. Investing now will not only reduce our future exposure to gas markets but it will also support jobs and growth.”

-- [[https://www.theguardian.com/environment/2022/feb/08/its-all-a-bit-cynical-the-politicians-behind-the-tory-attack-on-net-zero-agenda][‘It’s all a bit cynical’: the politicians behind the Tory attack on net zero ...]] 
#+end_quote

* Criticism from the left

- [[id:9e1ef266-2583-4562-84dd-79f8479334ba][Net zero is part of a global imperialist climate policy]]
