:PROPERTIES:
:ID:       771717b4-d7ce-474b-8197-7660fad52172
:mtime:    20230402094845 20211127120857 20210724222235
:ctime:    20200630152443
:END:
#+title: Provision a WordPress server at Digital Ocean

Steps I usually do to get a WordPress server up and running at [[id:4b918007-1bb5-4231-9d69-8c77280e2efd][Digital Ocean]].

* Create the droplet

They do have a WP server in the marketplace, but I tend to do it from scratch.

Usually go with Ubuntu 18.04 LTS.

I always provision with an ssh key set up from the get go, rather than getting sent a root password.

* Security things
  
- ufw
  - enable http, https, and ssh
- fail2ban
- disable remote password access (usually done already)
- but *do* set a root password and store in password manager, for those times you need to log in via the access console
  
* WP related apps
  
- Apache
- [[id:beeb75e4-6038-4816-9870-ad2f98982e93][MySQL]]
- PHP
  - and a bunch of php extensions (php-xml, php-json, php-mysql, php-mbstring, php-zip, any others?)

#+begin_src
apt install apache2
apt install mysql-server
apt install php-xml php-json php-mysql php-mbstring php-zip
ufw allow http
ufw allow https
ufw allow ssh
ufw enable
apt install fail2ban
#+end_src
