:PROPERTIES:
:ID:       c359a955-3331-40d1-ad1c-341e5cb5e71e
:mtime:    20230624093033
:ctime:    20230624093033
:END:
#+TITLE: Revolutionary Communist League of Britain
#+CREATED: [2023-06-24 Sat]
#+LAST_MODIFIED: [2023-06-24 Sat 09:32]

#+begin_quote
In the RCLB, it was really important that we had this commitment to developing a programme; it’s that which really pushed us to struggle and resolve issues of line, and bring to fruition the rethink which opened up all these issues around gender, racism and the national question

-- [[id:b7717dd4-9d86-451c-bfae-aa7df4a49c17]['Imperialism runs deep': Interview with Robert Biel on British Maoism and its afterlives]]
#+end_quote
