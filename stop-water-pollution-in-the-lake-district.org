:PROPERTIES:
:ID:       9480151c-c0ef-4834-ae6c-116714b580c9
:mtime:    20220430121155
:ctime:    20220430121155
:END:
#+TITLE: Stop water pollution in the Lake District
#+CREATED: [2022-04-30 Sat]
#+LAST_MODIFIED: [2022-04-30 Sat 12:12]

A petition to stop [[id:df1c29de-ef51-4646-bac4-ec0fec121873][Water pollution in the Lake District]].

https://www.change.org/p/uk-government-stop-water-pollution-in-the-lake-district
