:PROPERTIES:
:ID:       b1eeb7d6-117f-46b1-b251-543baa3f403b
:mtime:    20211127120932 20210724222235
:ctime:    20210724222235
:END:
#+title: Strategy and tactics in anarchist politics
#+CREATED: [2021-07-10 Sat 22:36]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

[[id:4c6b73d3-df53-4cb7-9c9a-43d6e0903c3f][Strategy]]. [[id:eb86fb3a-b1e3-4da9-a855-c7d60a8316e5][Tactics]].

#+begin_quote
For anarchism, then, the kind of [[id:84dfaf30-8cd5-4930-be23-338f0d8e949a][repertoires of action]] that are of interest are those that aim at changing reality in the here and now, at creating alternatives to what presently exists and, where judged to be necessary, directly challenging the dominance or even existence of what exists.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

#+begin_quote
A sticking point in anarchist theory and practice, at least since the alterglobalisation movement’s prominence around the turn of the millennium, has been whether the concept of strategy can be applied to anarchism or whether anarchism is, or ought to be in principle, purely tactical.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

* Strategy
  
#+begin_quote
in the language of Beer’s cybernetics and the VSM, the strategic function in an organisation is concerned with regulating the overall behaviour of the organisation in line with defined goals and in response to change both inside and outside the organisation.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

#+begin_quote
First, strategy should operate to frame tactical action within the overall goals of the organisation. Second, strategy should be informed by the anarchist politics of selforganisation and participatory democracy discussed throughout this book so far. Third, strategy should be flexible and responsive to change.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

#+begin_quote
strategy is concerned with both what is happening in the moment inside the organisation and how best to regulate it to achieve set goals as well as what is happening outside in the external environment and with respect to the possible futures of the organisation.

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote

* Tactics
  
#+begin_quote
the tactics of anarchist organising cover this range of collective actions that, in one way or another, enact the ideals of anarchist politics – a concept called ‘[[id:55f4dec4-a979-4bac-86b9-49be08c6527a][prefiguration]]’

-- [[id:41a37ab8-edb7-42b6-9bb4-6e70ee62ce42][Anarchist Cybernetics]]
#+end_quote
