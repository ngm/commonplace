:PROPERTIES:
:ID:       9c9043ce-3119-4e5f-a7a2-f960b5662c27
:mtime:    20220326122134 20211127120918 20211017152050
:ctime:    20211017152050
:END:
#+TITLE: Value
#+CREATED: [2021-10-17 Sun]
#+LAST_MODIFIED: [2022-03-26 Sat 12:22]

#+begin_quote
The predominant explanation of value in mainstream economics today is the neoclassical theory of “[[id:a97a6bff-5c28-43fb-aad2-2eccecd7b60e][marginalism]].” It assumes that a commodity’s worth is determined by laws of [[id:3b808ab5-7f30-4e22-8b84-226f8081d272][supply and demand]], mediated by what they term “marginal utility"

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

[[id:a676b553-ab00-423e-a453-2fe224ba7d98][Labour theory of value]].

#+begin_quote
One type of value does not cause the other (how useful an item is does not determine its value on the market; nor does its value on the market determine how useful an item may be to us

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

#+begin_quote
In fact, it’s precisely the fact that an item has no use-value to its producer that allows it to be an exchange-value

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

#+begin_quote
Simply put, you can either live in your house, or you can sell your house. You can’t have your cake and eat it too.

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

^ what about non-rivalrous goods?

#+begin_quote
The world was sweaty, the world was dusty, but it all made sense, because beneath the thousand thousand physical differences of things, [[id:63a3f178-5e11-42dd-8607-b82a6f6cc0cd][economics]] saw one substance which mattered, perpetually being created and destroyed, being distributed, being poured from vessel to vessel, and in the process keeping the whole of human society in motion. It wasn’t [[id:6368f96e-ec8d-48ba-b166-419f463e45b4][money]], this one common element shining through all its temporary disguises; money only expressed it. It wasn’t [[id:76c3d510-e8b8-4d3c-a66f-eb2b34a903d6][labour]] either, though labour created it. It was value. Value shone in material things once labour had made them useful, and then they could indeed be used or, since value gave the world a common denominator, exchanged for other useful things; which might look as dissimilar from one another as a trained elephant did from a cut diamond, and consequently as hard to compare, yet which, just then, contained equal value for their possessors, the proof being that they were willing to make the exchange.

-- [[id:636573c9-34d2-4707-a480-36e82f65b8ed][Red Plenty]]
#+end_quote
