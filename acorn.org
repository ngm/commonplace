:PROPERTIES:
:ID:       5f8d1179-34a7-41d0-a93e-af3f770c4ade
:mtime:    20221219111102 20221218223637 20221218130031 20221217172330
:ctime:    20221217172330
:END:
#+TITLE: Acorn
#+CREATED: [2022-12-17 Sat]
#+LAST_MODIFIED: [2022-12-19 Mon 11:11]

+ URL :: https://acorn.software

#+begin_quote
Peer-to-Peer Agile Project Management For Software Teams
#+end_quote

#+begin_quote
Acorn is an open-source, [[id:48b98aa9-dcc9-4f23-af5b-4a11b203c18b][peer-to-peer]] [[id:9b8aba7a-7577-4ec6-a0d3-57836a541145][project management]] application. It is designed and built as a scrum-alternative, Agile Development Pattern for distributed software development teams.
#+end_quote

* Testing Acorn

- join a project, put in the five word code
  - 
  #+DOWNLOADED: screenshot @ 2022-12-18 22:36:10
  [[file:Testing_Acorn/2022-12-18_22-36-10_screenshot.png]]
- Luckily, Flancian was online so I didn't have to wait :)
- I can see the issues that Flancian has created.  I've commented on one - wonder if he sees it.
- Immediate initial thought is that there is a need for async chat, that comments probably won't cut it for.  So you'd still need to discuss matters in e.g. Element.
  - looks like you can't @ people.
- Also I don't think you can edit comments.
- I wonder where is the data stored on my machine?
- You don't seem to be able to remove children.  A bit annoying as I want to amend the structure.
