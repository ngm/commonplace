:PROPERTIES:
:ID:       6695a0c9-be94-403c-a2d7-9900d5afd450
:mtime:    20211127120848 20211017150840
:ctime:    20211017150840
:END:
#+TITLE: Bourgeoisie
#+CREATED: [2021-10-17 Sun]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

[[id:3b6ee14b-22b1-41cb-9f40-7d5580e30608][Capitalist class]].

#+begin_quote
The BOURGEOISIE—or capitalists—are the class of people who own the means to produce (land, factories, tools, and materials), and employ laborers to do the work of production

-- [[id:259060ac-fb9e-408b-845e-06af5154aba9][A People's Guide to Capitalism]]
#+end_quote

