:PROPERTIES:
:ID:       4dc15984-4e07-471d-8dbd-499e3136fad1
:mtime:    20211127120835 20210724222235
:ctime:    20210724222235
:END:
#+title: 2020-06-26

* [[id:36238cf6-7470-4c4f-8b17-8e8e24223f0d][rclone]]
  
Finding rclone super handy for backups and things where cloud services are involved.

Especially useful as my internet connection is currently so poor that downloading from a cloud service to then upload to a server somewhere is terribly slow.
