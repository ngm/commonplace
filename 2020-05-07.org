:PROPERTIES:
:ID:       359aab20-7f0b-4f41-a4a1-a055bb56fa9e
:mtime:    20211127120822 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: 2020-05-07

* Finished: Ctrl+S

Finished [[id:62cc69b5-4556-4751-95c6-284ad4cc86d8][Ctrl+S]].  It was a page turner, no doubt.  Very visual, almost more a script for a film than a novel.  It's a fast-paced adventure story with a bit of a detective/whodunnit edge.

Not particularly nuanced or thought-provoking.  Cliched, but good fun.  The written equivalent of watching a blockbuster, I guess.

Set in a near future, where people 'ascend' to a [[id:d9c8d5f0-bf62-449c-aa34-52001f99f9e4][virtual reality]] world built on quantum computers called SPACE.   For many it's a way of escaping from a dreary actual reality.

There's a race of artificial life creatures called Slif who have evolved within SPACE.

It's a bit gruesome in places, with this idea of harvesting and then recreating emotions from others.

* Books and hyperlinks 
  
#+begin_quote
 I like books because they don’t have hyperlinks, I like the web because it does.
 
-- https://prtksxna.com/2020/04/13/books-hyperlinks/
 #+end_quote
  
