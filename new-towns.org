:PROPERTIES:
:ID:       8d7a4e3b-22a0-4ee5-b489-7940c43e9918
:mtime:    20220603161759 20220603094041
:ctime:    20220603094041
:END:
#+TITLE: New Towns
#+CREATED: [2022-06-03 Fri]
#+LAST_MODIFIED: [2022-06-03 Fri 16:18]

#+begin_quote
Like the current trend for mid-century modern design, the idea (perhaps more than the material reality) of new towns is attractive, because they’re associated with a perceived utopianism and optimism of the post-war period in Britain

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Proposed by Sir Patrick Abercrombie in his reconstruction plans of 1944–45, new towns were a bold solution to overcrowding and poor housing through decentralisation of inhabitants from London and the other big cities

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Attlee’s Labour government passed the 1946 New Towns Act among the other key pieces of legislation framing the welfare state. Stevenage was the first new town to be chosen

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Stevenage was the first pedestrian town centre in England. Not even the nearby garden cities of Letchworth and Welwyn, with their carefully planned green space, prioritised the pedestrian shopper over the car user

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
Other than a discussion of the leisure activities, there is little recognition of the people who migrated to live in Stevenage, east-enders from blitzed London, and the Irish builders who physically built the houses, churches, and community centres, and then stayed put

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
The radical novelty of the post-war new towns programme lay in the centralised control of planning and finance through public development corporations

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote
