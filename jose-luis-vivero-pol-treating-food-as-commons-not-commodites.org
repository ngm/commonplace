:PROPERTIES:
:ID:       29757499-50d0-4203-8787-a9e77a01642b
:mtime:    20220102225118
:ctime:    20220102225118
:END:
#+TITLE: Jose Luis Vivero Pol: Treating Food as Commons, Not Commodites
#+CREATED: [2022-01-02 Sun]
#+LAST_MODIFIED: [2022-01-02 Sun 22:53]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://david-bollier.simplecast.com/episodes/jose-luis-vivero-pol-treating-food-as-commons-not-commodites
+ Series :: [[id:d5682dcd-68e5-4da6-b260-87382b960781][Frontiers of Commoning]]
+ Featuring :: [[id:29757499-50d0-4203-8787-a9e77a01642b][Jose Luis Vivero Pol]]

[[id:d5a75ee0-d5e8-42b4-ac91-282355535f16][Food commons]]
