:PROPERTIES:
:ID:       17730e97-0a35-4b6f-a9d1-9e30b42437f0
:mtime:    20220607214506
:ctime:    20220607214506
:END:
#+TITLE: stagflation
#+CREATED: [2022-06-07 Tue]
#+LAST_MODIFIED: [2022-06-07 Tue 21:46]

#+begin_quote
The most noticeable effect on the rich world was the novel phenomenon of ‘stagflation’. According to the Keynesian consensus of the post-war period, a stable relationship was supposed to exist between inflation and employment. When the economy was operating at full capacity, prices would increase because all available resources (including labour) were being used in production, pushing up prices (and wages). The inverse was thought to be true when production and growth slowed.

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote

#+begin_quote
But during the 1970s, unemployment and inflation were both high at the same time. Looking back, the reason for this — the formation of OPEC — is quite obvious. But the chaos and confusion of the time provided neoliberal economists with an opportunity to push a new theory of inflation: [[id:eccd052b-d8e3-4ec2-ae93-fa37b5059214][monetarism]]

-- [[id:c16e2b0a-5984-484d-8125-8f7de9dbb137][Tribune Winter 2022]]
#+end_quote
