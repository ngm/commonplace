:PROPERTIES:
:ID:       da9da848-aa7a-4754-8c3a-225fcbc5a261
:mtime:    20220129152848 20220129124052
:ctime:    20220129124052
:END:
#+TITLE: I dislike big tech
#+CREATED: [2022-01-29 Sat]
#+LAST_MODIFIED: [2022-01-29 Sat 16:25]

* Because

+ [[id:d6d9ad9e-0ee6-4ca0-9917-3b0e447d5a6b][Big tech is complicit in climate breakdown]]
+ [[id:0dc24c55-4e63-41fd-aef6-1cbb39ab7c67][Big tech is a huge part of capitalism]] and [[id:de90c272-78c5-48b0-9aeb-ef397feeb679][I dislike capitalism]]
+ [[id:9ce674b9-4e50-45e7-b6a1-2f4a861bfa65][Big tech exploits the victims of economic collapse]]
+ [[id:19d169e8-a6e1-4de1-b2c3-c1f1e72da533][For big tech, user benefits are secondary to business model]]

* But

+ This does not mean I dislike people who use the big tech platforms.
+ Nor the people who work there.
+ Big tech is a systemic problem, not just a matter of individual choices.

* Epistemic status

+ Type :: [[id:c4b32a97-6206-4649-8218-39c25526c3a8][feeling]]
+ Strength :: 7

7 because yeah for sure I recognise the problem.  But not a 10 because I don't feel physically angry about it (yet).  It's not emotive in that way.
