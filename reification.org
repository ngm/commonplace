:PROPERTIES:
:ID:       da291779-0dc6-4ba7-95f7-f21744341ee7
:mtime:    20211127120957 20210724222235
:ctime:    20210724222235
:END:
#+title: reification
#+CREATED: [2021-04-05 Mon 21:43]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
the process by which social relations are perceived as inherent attributes of the people involved in them, or attributes of some product of the relation, such as a traded commodity

- https://en.wikipedia.org/wiki/Reification_(Marxism)
#+end_quote

[[id:896c7e2e-d75c-4c31-9822-1c04bb6f5605][The map is not the territory]].
