:PROPERTIES:
:ID:       b981cbf1-d947-4f34-9e5c-e2b5648d4219
:mtime:    20211127120936 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:Politics and the North-West of England

* History
 
[[id:587322a2-d23f-4731-ae63-c056c06383ee][Industrial revolution]] and [[id:7c45cc90-a185-41f8-a24a-6b377c4ed95b][labour movements]] are strongly linked to the North-West of England.

[[id:75982453-a0d6-460e-b0d3-a56f5970ed99][Engels]] wrote about [[id:7dc2cffe-dce5-4e95-b147-9ba5565e19e9][the conditions of the working class in England]].  Some parts (all?) centred around [[id:97156766-afa3-4bd6-9b35-83dcf0fc5da6][Manchester]].

Also the history of the [[id:c7c1a3d1-5350-4ae3-a8e7-f1b77c170e33][textile industry]], a bit part of the history of the industrial revolution, is very closely linked to Lancashire and Manchester.

* [[id:30a2497b-6c3e-444a-acb8-f4d48cc53429][Salford]]
   
* Present
  
   - [[https://www.invidio.us/watch?v=y4uIC0AwD68][Brexit breakdown: a big day in the north]], Anywhere But Westminster
   - [[https://www.hopenothate.org.uk/yes-we-need-climate-action-but-it-needs-to-be-rooted-in-peoples-daily-reality/][Yes, we need climate action; but it needs to be rooted in people’s daily reality]], Lisa Nandy

     
