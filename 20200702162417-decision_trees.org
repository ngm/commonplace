:PROPERTIES:
:ID:       89d5998b-b749-489a-8b16-a136ecd2ea3d
:mtime:    20211127120908 20210724222235
:ctime:    20200702162417
:END:
#+title: Decision trees

Like expert system but on steroids.

One of the basic machine-learning algorithms.  Each tree "knows" a limited number of classes.  

For each element, the tree "asks" some questions and chooses the class most similar to the element.

https://www.analyticsvidhya.com/blog/2016/04/complete-tutorial-tree-based-modeling-scratch-in-python/

An automatic data analysis method.

Dates back to Hoveland and Hunt.
