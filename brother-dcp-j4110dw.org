:PROPERTIES:
:ID:       0e6ed16d-769c-42a8-beed-a088c157f472
:mtime:    20230314192037
:ctime:    20230314192037
:END:
#+TITLE: Brother DCP-J4110DW
#+CREATED: [2023-03-14 Tue]
#+LAST_MODIFIED: [2023-03-14 Tue 19:38]

- Not powering on at all
- Power cable was fine (tested with a spare)
- Service manual:  https://www.manualslib.com/download/1538829/Brother-Dcp-J4110dw.html
  - huge disassembly guide!
- file:///home/neil/Downloads/dcpj4110dw.pdf
