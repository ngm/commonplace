:PROPERTIES:
:ID:       443e0fa2-a383-4118-a41d-f1ae7904a1c7
:mtime:    20230423031212 20230421171735
:ctime:    20230421171735
:END:
#+TITLE: To Talk of Organization – on Nunes’ Neither Vertical nor Horizotal
#+CREATED: [2023-04-21 Fri]
#+LAST_MODIFIED: [2023-04-23 Sun 03:12]

+ An :: [[id:6e8b90e2-8fa6-4c55-b588-adcc86753111][article]] and [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://cosmonautmag.com/2022/02/to-talk-of-organization-on-nunes-neither-vertical-nor-horizontal/
+ Series :: [[id:5ea8715c-c8a8-472f-8ec7-6684c77ef74c][Cosmopod]]

Nice overview and review of Rodrigo Nunes' book [[id:96a37fb7-282b-44a5-bb88-d5332d49c581][Neither Vertical Nor Horizontal]] on political organisation.  Also applies some of Nunes' ideas to the strategies of English Left post-Corbyn (stay and fight, form a new party, focus on [[id:4b4a49a4-26eb-43f0-b217-73a2bbb60266][base-building]]?)
