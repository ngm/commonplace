:PROPERTIES:
:ID:       dc0e5aef-dc3e-4688-9b94-4003a08f4b2c
:mtime:    20211127120958 20210816215506
:ctime:    20210816215506
:END:
#+TITLE: COUNTDOWN TO THE CLIMATE SUMMIT #1: How did we get here?
#+CREATED: [2021-08-16 Mon]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ URL :: https://www.cheerfulpodcast.com/rtbc-episodes/countdown-climate-summit-how-did-we-get-here
+ Topics :: COP / [[id:28c04890-c035-49ce-b120-17c43260e95f][COP 26]] / [[id:d5e4abd3-fcd6-48eb-a6aa-303498ada7a5][Climate change]]
+ Featuring :: [[id:470f92d4-cf66-4be4-8c48-7a89176ca895][Alice Bell]]

Some background history on [[id:65f5cf66-af34-4021-ba45-36b6597a05a5][climate science]], [[id:c34f033d-3204-4cae-8ca2-4fe581d6457c][climate denial]], and [[id:25051a56-3adc-490f-892b-75a8f9696dd9][COP]]s.
