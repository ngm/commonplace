:PROPERTIES:
:ID:       f4bb2dbc-7c31-4926-b5cc-eb750b04b620
:mtime:    20220815103329
:ctime:    20220815103329
:END:
#+TITLE: TyskySour: The Tories Have Broken Britain
#+CREATED: [2022-08-15 Mon]
#+LAST_MODIFIED: [2022-08-15 Mon 10:47]

+ [[id:54baa5f8-f7b1-412a-ac98-59d6a5c3b7e3][UK water shortages 2022]].
  + [[id:6a7a698b-c817-4dbc-9919-1bb44fd0f0a2][Water privatisation]].
  + Privatised companies have invested very little in water infrastructure since it was privatised in 1989.

+ Liz Truss saying how terrible it looks where there's solar panels instead of farmland in the UK... good god.
