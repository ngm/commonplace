:PROPERTIES:
:ID:       dfd1d77a-2fed-44e6-8edd-ea5eb1cf2732
:mtime:    20211127120959 20210724222235
:ctime:    20200730162230
:END:
#+title: Social distance

As described by [[id:6059b0dd-34bb-465f-a689-1f8eeee7b6c5][Ton]], a way of helping to filter down the wealth of information out there that you could be reading, by how socially close you are to people.

-- [[https://www.zylstra.org/blog/2019/06/feed-reading-by-social-distance/][Feed Reading By Social Distance – Interdependent Thoughts]] 
