:PROPERTIES:
:ID:       e4546ce5-7536-4e9c-bdbc-89985c333665
:mtime:    20211127121003 20210724222235
:ctime:    20200412133800
:END:
#+TITLE: Wallabag

* Getting API access set up

If you're using the hosted version, the API endpoint for oauth is this:

https://app.wallabag.it/oauth/v2/token

OK, so now I have the access token and refresh token.

What do I need to do with these?

The access token is my bearer token.  OK, cool.

#+BEGIN_SRC json
   access_token = xxx

  refresh_token = xxx
#+END_SRC

OK, a very hacky first attempt at this would be to chuck in a class into the syndication links plugin, with everything hardcoded in it.

How can I register the plugin?  Or do I need to expose an endpoint?

Next step would be to do this in a custom plugin or something custom file in my theme, still with access tokens hardcoded.

The next step after that would be to have the custom plugin do the authentication as a client to Wallabag.  Does everyone need to have it as their own client?  I guess so?

So I need to just pass the URL.  Ahhh.  OK.  Will syndication links provide that information?  Well you can get the post in there, so I guess you can pull it out.

OK - bigger thought.  To move this up the stack and make it possible for more people, it could make sense to add it as something for Bridgy to translate via webmentions.  But let's start here for now.

Should I also include a syndication link to wallabag?  I mean it shouldn't really be public, right?

Actually, is this the right way to do it?  Am I really syndicating my post?

you have to refresh constantly...

to get a refresh token:
http POST https://app.wallabag.it/oauth/v2/token grant_type=refresh_token client_id=xxx client_secret=xxx refresh_token=xxx
