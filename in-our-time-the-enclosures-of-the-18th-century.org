:PROPERTIES:
:ID:       d7129606-e49c-4d93-8ce6-443907b9e908
:mtime:    20220731151752
:ctime:    20220731151752
:END:
#+TITLE: In Our Time, The Enclosures of the 18th Century
#+CREATED: [2022-07-31 Sun]
#+LAST_MODIFIED: [2022-07-31 Sun 15:22]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL ::  https://www.bbc.co.uk/programmes/b00b1m9b

#+begin_quote
Melvyn Bragg and guests discuss the [[id:82db8b70-f14f-4905-8705-d106b8ce6b3f][enclosure movement]] of the 18th and 19th centuries. In the early 19th century, the Northamptonshire poet John Clare took a good look at the countryside and didn’t like what he saw. He wrote: "Fence meeting fence in owners little boundsOf field and meadow, large as garden-grounds,In little parcels little minds to please,With men and flocks imprisoned, ill at ease."

Enclosure means literally enclosing a field with a fence or a hedge to prevent others using it. This seemingly innocuous act triggered a revolution in land holding that dispossessed many, enriched a few but helped make the agricultural and industrial revolutions possible. It saw the dominance of private property as the model of ownership, as against the collective rights of previous generations.

For some Enclosure underpinned the economic and agricultural development of Modern Britain. But it has also been a cause celebre for the political left ever since Karl Marx argued that enclosures created the industrialised working class and ushered in the capitalist society.

What really happened during the era of 18th and 19th century enclosures? Who gained, who lost and what role did Enclosures play in the agricultural and industrial transformation of this country? With Rosemary Sweet, Director of the Centre for Urban History at the University of Leicester; Murray Pittock, Bradley Professor of English Literature at the University of Glasgow; Mark Overton, Professor of Economic and Social History at the University of Exeter.
#+end_quote
