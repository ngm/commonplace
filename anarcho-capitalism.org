:PROPERTIES:
:ID:       f8e1e4d1-288d-443e-8534-c2449974c032
:mtime:    20211127121015 20210925095009
:ctime:    20210925095009
:END:
#+TITLE: Anarcho-capitalism
#+CREATED: [2021-09-25 Sat]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

#+begin_quote
Anarcho-capitalism is a [[id:4feea343-745b-4a64-b822-a6e08ceba5f5][political philosophy]] and [[id:bdceeeb7-9aa8-4717-bf40-812ede76e83c][economic theory]] that advocates the elimination of centralized states in favor of a system of [[id:5b50c6ec-d1c5-485e-9acc-18d7e418e057][private property]] enforced by private agencies, [[id:3709823c-0fa7-4e7d-8ecf-8314bb93022a][free market]]s and the [[id:be3ddb92-0251-4f9b-8b6c-93701aa34a66][right-libertarian]] interpretation of [[id:59a92db3-13e9-435e-a0b1-3597876ccdca][self-ownership]], which extends the concept to include control of private property as part of the self. 

-- [[https://en.wikipedia.org/wiki/Anarcho-capitalism][Anarcho-capitalism - Wikipedia]] 
#+end_quote

What a terrible, ridiculous concept.  Reification of private property and [[id:f44ed014-5e02-4b3d-9eb4-77b33a69804b][capitalism]] is not [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]].
