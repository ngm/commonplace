:PROPERTIES:
:ID:       aaa03642-6c89-4efa-923d-955272e4d881
:mtime:    20211127120927 20210724222235
:ctime:    20210724222235
:END:
#+title: Decidim
#+CREATED: [2021-04-24 Sat 16:08]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

#+begin_quote
Free Open-Source participatory democracy, [[id:ab13f4cd-571e-414c-922e-b32eb41901b3][citizen participation]] and open government for cities and organizations
#+end_quote

#+begin_quote
originally developed for the Barcelona City government online and offline participation website
#+end_quote

+ Strategic planning
+ [[id:04c1c125-b6a3-4269-a1ec-309f1ab074a2][Participatory budgeting]]
+ Initiatives and citizen consultations
+ Participative processes
+ Assemblies
+ Networked communication
