:PROPERTIES:
:ID:       2262f604-46a0-4929-a993-68da8c3e782d
:mtime:    20211127120812 20210724222235
:ctime:    20200707234511
:END:
#+title: Wiki content

What do you put in a wiki?  Whatever you want!  It's your wiki.  

For me, I feel it's a lot more than just a set of 'facts' that I've discovered about my particular interests.  It would then just be a really poorly maintained subset of Wikipedia.  I think the point is to make sure not to lose the personal and the [[id:383691fb-2d07-4865-b69d-c1a479865056][personality]].  (That which Wikipedia deliberately avoids).

   #+begin_quote
Wisdom, not facts. We’re not just looking random pieces of information. What’s the point of that? Your commonplace book, over a lifetime (or even just several years), can accumulate a mass of true wisdom–that you can turn to in times of crisis, opportunity, depression or job.
   
-- [[https://ryanholiday.net/how-and-why-to-keep-a-commonplace-book/][How and why to keep a commonplace book]]
   #+end_quote
   
I agree with that, except to de-emphasise wisdom a /little/.  I don't want to feel a pressure that what I put in here has to be wise.  It's a place for percolation.  It might turn into wisdom eventually.
