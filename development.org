:PROPERTIES:
:ID:       3665fed6-e28e-4c0f-911c-dd1cb691700c
:mtime:    20211127120823 20210724222235
:ctime:    20210724222235
:END:
#+title: Development
#+CREATED: [2021-05-22 Sat 12:00]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
Development is a term of political economy used by the US and European nations to prod "undeveloped" countries to embrace global commerce, resource extrativism, and consumerism along with improvements in infrastructure, education, and healthcare.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

#+begin_quote
The harmful side-effects of "development" typically include ecological destruction, inequality, political repression, and cultural dispossession.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote

