:PROPERTIES:
:ID:       18bd9889-fad4-4db4-a080-0028f8d006b2
:mtime:    20211127120807 20210818113335
:ctime:    20210818113335
:END:
#+TITLE: Gaia hypothesis
#+CREATED: [2021-08-18 Wed]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Considers the Earth as a single, self-regulating system including both living and non-living parts. 
