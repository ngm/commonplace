:PROPERTIES:
:ID:       b6408353-6673-4b9f-957c-84e11e889c52
:mtime:    20230324164157 20230319205858
:ctime:    20230319205858
:END:
#+TITLE: Full of Noises
#+CREATED: [2023-03-19 Sun]
#+LAST_MODIFIED: [2023-03-24 Fri 16:43]

+ URL :: https://fonfestival.org/

I love Full of Noises.  They promote weird music in [[id:3a752635-5d8d-4044-8168-8109ff31cee9][Barrow]].

#+begin_quote
Full of Noises is a sound art and new music organisation based in a public park on Cumbria’s Furness Peninsula. We produce and commission new work from contemporary composers and sound artists through a programme of residencies, performances and public realm installations.
#+end_quote
