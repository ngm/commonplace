:PROPERTIES:
:ID:       6df4b150-82da-43df-97d4-a321dc9240d5
:mtime:    20220121132720 20211127120852 20210724222235
:ctime:    20210724222235
:END:
#+title: social contract
#+CREATED: [2021-05-01 Sat 08:39]
#+LAST_MODIFIED: [2022-01-21 Fri 13:29]

#+begin_quote
foundational concept in constructing the contemporary idea of "nation." It is a weak form of democracy, relying on regular yet infrequent participation (and sometimes even discouraging active participation).

-- [[id:88465cb6-f93c-495f-8a8f-975464f649e8][Building an open infrastructure for civic participation]]
#+end_quote

#+begin_quote
ponder the general belief system developed during the Renaissance and expanded in the eighteenth and nineteenth centuries by the capitalist societies that arose from it. We moderns live within a grand narrative about individual freedom, property, and the state developed by philosophers such as [[id:700a4524-4d0a-48bf-bde1-75b3db5440ca][René Descartes]], [[id:ee2e02cd-a31f-49f9-9afb-f6c30507aa60][Thomas Hobbes]], and [[id:e85d1a84-9efd-42b8-a2c4-8ee6eaa769ee][John Locke]]. The OntoStory that we tell ourselves sees individuals as the primary agents of a world filled with inert objects that have fixed, essential qualities. (Most notably, we have a habit of referring to “nature” and “humanity,” as if each were an entity separate from the other.) This Western, secular narrative claims that we humans are born with boundless freedom in a prepolitical “[[id:e658e77c-499b-4adf-a65d-90a80bcc8021][state of nature]].” But our imagined ancestors (who exactly? when? where?) were allegedly worried about protecting our individual property and liberty, and so they supposedly came together (despite their radical individualism) to forge a “social contract” with each other. As the story goes, everyone authorized the establishment of a state to become the guarantor of everyone’s individual liberty and property.

-- [[id:5d4564e2-1512-4174-bafe-40c335f8d135][Free, Fair and Alive]]
#+end_quote
