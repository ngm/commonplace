:PROPERTIES:
:ID:       1f09a137-a2b9-4d9f-8e21-0c9a1d11ff5e
:mtime:    20220720200020 20211127120810 20211030234934
:ctime:    20211030234934
:END:
#+TITLE: Centralisation
#+CREATED: [2021-10-30 Sat]
#+LAST_MODIFIED: [2022-07-20 Wed 20:00]

#+begin_quote
Another way to see this is that a centralized bureaucracy does not have requisite variety to govern organic growth. The edge is more expressive than the center, and changes more quickly. This forces the center to limit the variety at the edge in order to maintain control of the system.

-- [[id:7ff6e244-f292-47c9-a8ac-1d2301266c96][Network intersubjectives]] 
#+end_quote

^ [[id:2305f4a9-4fe4-4501-aa7d-d237cb4ab6ce][Law of Requisite Variety]]

#+begin_quote
While centralization first evolves for reasons of efficiency, the purpose of the resulting hierarchy soon shifts toward its own perpetuation.

-- [[id:7ff6e244-f292-47c9-a8ac-1d2301266c96][Network intersubjectives]] 
#+end_quote

#+begin_quote
The original purpose of a hierarchy is always to help its originating subsystems do their jobs better. This is something, unfortunately, that both the higher and the lower levels of a greatly articulated hierarchy easily can forget. Therefore, many systems are not meeting our goals because of malfunctioning hierarchies.

- [[id:2137fca1-2f70-4500-8a35-017ecdb4139d][Thinking in Systems]]
#+end_quote
