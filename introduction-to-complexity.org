:PROPERTIES:
:ID:       d52cd62f-d595-4db7-9233-7a4a7bba257e
:mtime:    20211127120954 20210824125243
:ctime:    20210824125243
:END:
#+TITLE: Introduction to Complexity
#+CREATED: [2021-08-18 Wed]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]

+ URL :: https://www.complexityexplorer.org/courses/119-introduction-to-complexity

I enrolled on this in August 2021.   I see it as a bit of a refresher on what I did in [[id:0b43d410-b347-4e06-851c-ce9a2abde212][Evolutionary and adaptive systems]].  And to give me a fun intro to [[id:430fb5f7-df03-4417-ad97-9dd414b1817f][agent-based modelling]] with  [[id:2bea5ba6-5176-4f4c-b45f-cca48ccc2d56][NetLogo]].

And then, the aim would be, to be followed up by some study of [[id:e044a716-096c-438e-b69a-9a2f177d781b][Systems thinking]]. They're related, but slightly different.  See [[id:496a3dde-2f8a-4d9a-93ed-ed6bc6b47c59][Complex Adaptive Systems, Systems Thinking, and Agent-Based Modeling]].

That I can then apply to questions around [[id:9d79cc54-da5c-4932-8e0a-1cee8adffbce][political organisation]], [[id:d5e4abd3-fcd6-48eb-a6aa-303498ada7a5][climate change]], and [[id:1834c5af-3f7f-44a4-a90b-3e3ecd9cdbe9][social network]]s.

  #+begin_quote
  In this course you'll learn about the tools used by scientists to understand [[id:d201b97a-3067-4a7c-adb3-61d0fd08932d][complex systems]]. The topics you'll learn about include dynamics, chaos, fractals, information theory, self-organization, agent-based modeling, and networks. You’ll also get a sense of how these topics fit together to help explain how complexity arises and evolves in nature, society, and technology.
 #+end_quote
  
* The Course
** Unit 1: What is Complexity? 

   Some examples of complex systems given are [[id:6a5d3cf0-b59f-4cb8-b64a-ab91fadc0da0][ant colonies]], [[id:1bb02081-40bb-4ab4-9dd7-13eda5eadf14][the brain]], [[id:1834c5af-3f7f-44a4-a90b-3e3ecd9cdbe9][social network]]s, the web, the human genome, the economy, [[id:a7a5415c-3a1d-4b4a-91c6-8ac1834d5ae5][food webs]], the [[id:e089f6e1-b88a-4ae2-bb06-49a4a754109f][immune system]], cities.

   I'm probably most interested in the [[id:942ab7ee-9c05-4a45-bd11-3ce2d3152331][networks]] complex systems.  But they're all interesting.

   Biological, social, technological.

*** Properties common to complex systems

    - [[id:90896b2a-1555-426f-a0f6-32584280d0a2][agents]]
    - [[id:e663d1d7-7e24-48aa-b293-ae66a72799bd][nonlinear]] interactions
    - no central control ([[id:bf300569-4788-4de9-b53a-632d883427ac][decentralisation]])
    - [[id:b330b154-99c2-46d2-87d6-bf4bb0027e90][self-organisation]]
    - [[id:f9d8e2e7-8be4-4851-b1ed-45460d3ca14a][emergence]] (emergent behaviours)
      - hierarchical organisation
      - information processing
      - complex dynamics
        - e.g. foraging trails in ants
        - e.g. stock prices
      - evolution and learning

*** Core Disciplines, Goals, and Methodologies of the Sciences of Complexity
**** disicplines 
    - dynamics :: the study of continually changing structure and behaviour of systems
    - information :: the study of representation, symbols, and communication
    - computation :: the study of how systems process information and act on the results
    - evolution :: the study of how systems adapt to constantly changing environments
     
**** goals
     - cross-disciplinary insights into complex systems
       - e.g how does information processing in ant colonies relate to information processing in cities
       - e.g. how is information flow in the brain simalar to information flow in an economic network
     - general theory
       - is it possible?

**** methodologies
    - experimental work
    - theoretical work
    - computer simulation

      This course has a focus on computer simulation of complex systems.

*** Definitions of complexity
    Hard to define... lots of definitions.
    We'll look at [[id:89631dec-157b-4906-b081-791008f420f3][Shannon information]] and [[id:429e4941-97c8-484b-9e0f-2d56312284e2][Fractal dimension]].

**** [[id:2f965ab8-0084-4bbd-82b2-cd7ebc81faee][Warren Weaver]]
     - problems of simplicity
       - a few variables, e.g. pressure and temperature; current, resistance, voltage; population vs time
     - problems of disorganized complexity
       - billions or trillions of variables
       - e.g. laws of temperature and pressure
       - averages, statistical mechanics
       - we assume little interaction between variables
     - problems of organized complexity
       - moderate to large number of variables
       - strong non linear interactions
         - can't be averaged meaningfully
       - 

**** Problems of organized complexity
     - what makes an evening primrose open when it does?
     - what is aging?
     - what is a gene?
     - on what does the price of wheat depend?
     - how can you explain the behaviour of e.g. a labour union?

*** What are Complex Systems? The Experts Weigh In
    - something where there's no simple compact way of describing the system
      - systems that encode long histories
    - sophisticated internal architecture of how it stores information
    - interacting things with emergent behaviour
    - evolution and adaptation is a key part of complex systems?
    - and feedback?

*** Introduction to NetLogo

    NetLogo is super simple to set up and get running the demos.  The Ants model is very cool - foraging for food sources and finding the closest thanks to pheromone trails.  This is the kind of thing I faffed around with graphics programming on in my Masters, surely would have been easier to use a pre-built system for it.  I wonder why we didn't...

    I really like the way it's presented, in that it gets you thinking about how the agent-based models might run and their dynamics.  And it also makes  you make predictions as to how changes in parameters and behaviours might change the dynamics.  Thinking a bit more scientifically about it.  Making a prediction and testing it with an experiment.

    I also love the NetLogo agent-based modelling stuff because it is very much thinking visually.  When some result isn't what you expected, you actually view the behaviour on screen.

** Unit 2: Dynamics and chaos

*** Introduction to dynamics
    [[id:c630eecf-cc5b-48e0-a63d-d29880dc3018][Dynamics]] is the science of how systems change over time.
    How does behaviour unfold and how does it change over time.

    e.g. planetary dynamics; fluid dynamics; electrical dynamics; climate dynamics; crowd dynamics; population dynamics; financial dynamics; group dynamics; dynamics of conflicts and dynamics of cooperation.

**** Dynamical systems theory
     - branch of maths of how systems change over time
       - calculus
       - diff eqs
       - iterated maps
       - etc
     - dynamics of a system
       - manner in which the system changes
     - gives us vocabularly and set of tools for describing dynamics

**** Brief history
     - (in the west) Aristotle
       - one set of laws for the earth, one for the heavens
     - copernicus
       - sun is stationary
     - galileo
       - experimental method
       - proved aristotle laws were false
     - newton
       - founder of modern science of dynamics
       - laws of motion same on earth and in heavens
     - laplace
       - proponent of newtonian reductionism
       - thought you could have complete prediction of the future
     - poincare
       - small differences in intial conditions produce very great ones in the final phenonema
       - "sensitive dependence on initial conditions"
         - so-called butterfly effect

**** Chaos
    - one particular type of dynamics of a system
    - defined as "sensitive dependence on initial conditions"
    - chaos is present in lots of places in nature
      - solar system orbits, weather and climate, computer networks, population growth and dynamics, and more
      - we'll look at population growth
      - what is the difference between chaos and randomness?

*** Iteration
    - Doing something again and again.
    - population growth is iterative
      - iterative part is we take last years pop to calculate this years pop
    - we have a linear equation because we have a linear system
    - linear equation because no interaction between bunnies
    - independence yields linearity

*** Linear vs non-linear systems
    - what happens when the parts interact in a non-linear way?
    - linearity: "the whole *is* the sum of the parts"
    - for non-linear - we add in death through overcrowding
      - plus a death rate
      - this gives us the "[[id:80ce7d60-9af9-4190-8a69-270f7f3b4cea][logistic model]]"
    - with non-linear systems, the whole *is not* the sum of the parts
