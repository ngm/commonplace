:PROPERTIES:
:ID:       7ee5a1b6-9c3b-41a3-8683-e1812b02b192
:mtime:    20211127120900 20210724222235
:ctime:    20200727204218
:END:
#+title: appropriate technology

#+begin_quote
Appropriate technology is a movement (and its manifestations) encompassing technological choice and application that is small-scale, affordable by locals, decentralized, labor-intensive, energy-efficient, environmentally sound, and locally autonomous

-- [[https://en.wikipedia.org/wiki/Appropriate_technology][Appropriate technology - Wikipedia]]
#+end_quote

[[id:351d0c3c-7862-429d-b29b-61d61d19231c][E. F. Schumacher]].

See also: [[id:b2da45d6-5d8c-4263-b82e-4c5930e54be0][Convivial Tools]].  

* Flashcard
  :PROPERTIES:
  :ANKI_DECK: Default
  :ANKI_NOTE_TYPE: Basic
  :ANKI_NOTE_ID: 1596379428782
  :END:
** Front
   What is appropriate technology?
   
** Back
A movement (and its manifestations) encompassing technological choice and application that is:

- small-scale
- affordable by locals
- decentralized
- labor-intensive
- energy-efficient
- environmentally sound
- and locally autonomous
