:PROPERTIES:
:ID:       badbdf44-246b-4ae0-953c-0383995e979e
:mtime:    20211127120937 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: HyperCard

A kind of visual [[id:0e6d2f7b-9fcb-48dd-bf81-f2a5046dbf9e][hypertext]]. 

-  [[https://arstechnica.com/gadgets/2019/05/25-years-of-hypercard-the-missing-link-to-the-web/][30-plus years of HyperCard, the missing link to the Web | Ars Technica]] 
- [[https://boingboing.net/2018/06/18/apples-hypercard-was-inspire.html][Apple's HyperCard was inspired by an acid trip / Boing Boing]]
