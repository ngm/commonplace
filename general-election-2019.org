:PROPERTIES:
:ID:       4519caac-19ee-4845-bb40-9f0a2b00258e
:mtime:    20211127120830 20210724222235
:ctime:    20210724222235
:END:
#+TITLE:General Election 2019

This episode of Rev Left Radio sums up most of my views around the [[id:1be6d045-1893-45c7-94d8-08e122b31f72][2019]] election pretty well: on Corbyn, the media, Brexit, limits of our electoral system, where to focus for these next five years.

https://revolutionaryleftradio.libsyn.com/uk-election

** Anti-semitism
- there have been problems with anti-semitism in the Labour party, but Corbyn himself is not anti-semitic
- it was blown out of proportion by the media

** Corbyn
- people on the doorstep personally didn't like Corbyn, thanks to a huge smear campaign over the course of 4 years.

** Brexit
- Labour saying a second referendum would happen lost them a lot of votes

** Where next?
   
- community focus

#+begin_quote
The alternative is to build a greater resilience among working-class people to the siren song of the billionaires. This can only be achieved by renewing the working-class infrastructure of the country. This means a trade union movement that can provide power at work and a Labour Party that can fight the daily battles to improve people’s lives, but it also means cultural institutions that give people a sense of ownership over the places that they live and the ability to express class as something collective. It is only by overcoming the atomisation and alienation of our communities that we can prevent people being picked off by the right-wing press
  
-- [[https://tribunemag.co.uk/2019/12/in-defence-of-salford/][In Defence of Salford]] 
  #+end_quote
.
#+begin_quote
Canvassing attempts to talk to everyone in an area and to know who they are, a quite different approach to standing in a busy spot and talking to whoever comes by. It is an example of structural rather than self-selecting organising, which Jane McAlevey argues has much more potential for building big majorities and real power – growing your army rather than just mobilising those who are already convinced. 

-- [[https://www.rs21.org.uk/2019/12/13/how-we-fight-back/][How we fight back | rs21]] 
#+end_quote
