:PROPERTIES:
:ID:       48a85253-febf-41fa-ada6-443e2b02c5ac
:mtime:    20211127120832 20210724222235
:ctime:    20200322185957
:END:
#+TITLE: Fraidycat

#+begin_quote
Fraidycat is a very small attempt to move toward tools that give us some power. It really only adds the ability to assign "importance" to someone you are following - allowing you to track them without needing to be aware of them every second. 

-- [[https://news.ycombinator.com/item?id=22545878][Show HN: Fraidycat | Hacker News]] 
#+end_quote



