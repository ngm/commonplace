:PROPERTIES:
:ID:       e26ea33c-cc49-44a9-b5a8-434f5961c892
:mtime:    20221214204557
:ctime:    20221214204557
:END:
#+TITLE: The Green Transition – The Problem with Green Capitalism Part 1
#+CREATED: [2022-12-14 Wed]
#+LAST_MODIFIED: [2022-12-14 Wed 20:48]

  (Documentary) Starting from: 00:30:22  Episode webpage: https://www.upstreampodcast.org/greentransitionpt1  Media file: https://traffic.libsyn.com/secure/bb336368-b933-4b06-88b6-e51256ac6d81/Green_New_Deal_Pt1.mp3?dest-id=3632352#t=1822
:PROPERTIES:
:CREATED:  [2022-12-04 Sun 17:43]
:END:

+ URL :: https://www.upstreampodcast.org/greentransitionpt1
+ Series :: [[id:b7b70123-178d-4155-8dab-b845c3a6867c][Upstream]]

.

+ ~ 00:30:22 [[id:1988a011-a5df-42a2-83e1-8409b708cc15][Thomas Sankara]], debt is neocolonialism.
+ ~00:43:50 [[id:fed3af82-d31b-4f7e-9ff2-3274b5f7401e][Right to repair]], [[id:3068b80e-6bbd-4db9-9d67-fd7e5ccbfbe7][tool libraries]], as part of [[id:4a1b7d1b-be3f-4bb4-b357-6fab2e1da23c][degrowth]].


