:PROPERTIES:
:ID:       766443e2-b2d1-4e7f-a753-4139368ca744
:mtime:    20211127120856 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Layer 0

A total galaxy brain for me just now with regards to [[id:e1cab250-a245-4841-a6fa-4bb7012b3fee][progressive summarisation]].  I've realised that just by virtue of having 'layer 0', the source text, as part of the progressive summarisation process - I feel less bad about having hundreds of unread articles saved in Wallabag.  I feel good about it in fact!  It's the first step of note-taking - I know I have source material on the topic to come back to, recommended by someone I trust, when this topic comes back into my focus.  I can dig deeper into it then if I want.

This is very positive.  I had started to think of the wealth of information out there on the web as [[id:9b8f7693-479f-4663-bd63-a2177b6ee4d5][information overload]].  But now I can go back to thinking about it as an amazing resource, to be tapped into when needed.

See: [[id:bd5e24f5-6e9b-40a4-b07b-ed29e23a3665][Antilibrary]].

