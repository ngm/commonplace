:PROPERTIES:
:ID:       dab602b4-c6a8-4947-aaf3-3bfb70f40546
:mtime:    20220925141134 20211127120957 20210724222235
:ctime:    20210724222235
:END:
#+TITLE: Paris Commune

Paris, 1871.

#+begin_quote
The city was transformed into an autonomously organized society, a [[id:e708b718-90d1-4d23-8c67-ea851522f1de][commune]] that experimented with alternative ways of structuring social and political life based on collaboration and cooperation

- [[https://thebaffler.com/salvos/the-judgment-of-paris-oshea][The Judgment of Paris | Lizzie O’Shea]] 
#+end_quote

An example of the [[id:00e61c22-4f33-489b-843b-4ffac1fd8c5b][dictatorship of the proletariat]].

Didn't last long (72 days) - it was crushed by the state.

It was not completely [[id:dea564a6-cfcb-4937-bfdd-3d678e3e0106][Marxist]], not completely [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchist]].  It had its own thing going on.  (And existed before these distinctions were that clear, anyway).

The Paris Commune was instructive to both Marx and Lenin.

#+begin_quote
Marx’s observation in the context of his analysis of the lessons of the Paris Commune, that 'the working class cannot simply lay hold of the ready-made state machinery, and wield it for its own purposes' - is hard to miss.

-- [[https://newsocialist.org.uk/bolsheviks-did-not-smash-old-state/][The Bolsheviks did not 'smash' the old state // New Socialist]] 
#+end_quote

#+begin_quote
when the armed people of the French capital raised barricades not only to defend the city council of Paris and its administrative substructures but also to create a nationwide confederation of cities and towns to replace the republican nation-state.

-- [[id:052ebff3-a758-45c9-8e98-d58ec721c1f4][The Communalist Project]]
#+end_quote

* Resources
 - [[https://revolutionaryleftradio.libsyn.com/the-paris-commune-a-brief-blossoming-of-proletarian-power][ Revolutionary Left Radio: The Paris Commune: A Brief Blossoming of Proletarian Power]]
 - [[id:65112bf2-8f0e-49ba-acc7-ce78f82ee76d][After Storming Heaven]]
 - [[id:237998d4-8b6b-4530-82c5-7ccfba7cb295][Communal Luxury - NovaraFM]]
 - [[id:e4fad9ab-e8ec-4ec5-8041-0c27509d8a9d][The Radical Imagination of the Paris Commune]]
  


  
