:PROPERTIES:
:ID:       318bd9a8-2c7e-4975-bc4b-7d7862bd996b
:mtime:    20231011093122 20231010185034 20230512090041 20230510090719 20230509212737
:ctime:    20230509212737
:END:
#+TITLE: Repairing an Acer Aspire One 753 with no power
#+CREATED: [2023-05-09 Tue]
#+LAST_MODIFIED: [2023-10-11 Wed 09:31]

Owner reports that it was working fairly recently, but all of a sudden fails to power on.

No signs of life at all.  No LEDs lit when connected to charger.

* Things tried

** Power reset

Tried simple power reset / static discharge (disconnect power, remove battery, hold on button for over 30 seconds, reconnect battery and power).

** Charge overnight

Try charging for a long time in case it's a discharged battery.

 #+begin_quote
A laptop with a battery discharged to its bare minimum may not power on even when connected to power. This is a built-in protection to prevent further battery discharge of a battery already at its minimum.

- https://www.lifewire.com/fix-acer-laptop-that-will-not-turn-on-5222644
 #+end_quote

That said, it doesn't work with the battery removed, either.

** Check the charger

Doesn't look like it's Acer branded, but it has the correct output rating - 19.5V / 2.15A.

It's rated 19.5V.  Using a multimeter, it's got a reading of 19.37/8, kind of fluctuates between the two.

** Remove/reseat components

** Check continuity to battery pins

** Check power jack connection to motherboard

* Resources

- https://community.acer.com/en/discussion/366230/ao753-not-starting
- https://forums.tomsguide.com/threads/acer-aspire-one-no-charge-light-wont-turn-on.155791/
- [[https://wiki.restarters.net/Laptop_power_problems][Laptop power problems - Restarters Wiki]]

  - [[https://www.youtube.com/watch?v=sszSAa3ySZk][Acer Aspire One 753 MS2296 disassembly, thermal paste replacement, upgrade - ...]] 

   [[https://www.youtube.com/watch?v=tWoqVfFsg8E][acer laptop not turning on and not charging I Motherboard repair. - YouTube]] 
