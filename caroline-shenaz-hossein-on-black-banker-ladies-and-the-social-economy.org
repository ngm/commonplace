:PROPERTIES:
:ID:       7bdfb620-7087-499f-8f6a-beecf6d958ab
:mtime:    20211201214712
:ctime:    20211201214712
:END:
#+TITLE: Caroline Shenaz Hossein on 'Black Banker Ladies' and the Social Economy
#+CREATED: [2021-12-01 Wed]
#+LAST_MODIFIED: [2021-12-01 Wed 21:49]

+ A :: [[id:6d885bc4-ba2e-4552-a5b0-2f6060ece0dc][podcast]]
+ URL :: https://david-bollier.simplecast.com/episodes/caroline-shenaz-hossein-on-black-banker-ladies-and-the-social-economy
+ Featuring :: [[id:bf6f43ea-fcb9-480f-8d8e-f595bfba36da][Caroline Shenaz Hossein]] / [[id:b5173915-bea2-4036-b353-b3617c7f3263][David Bollier]]
+ Series :: [[id:d5682dcd-68e5-4da6-b260-87382b960781][Frontiers of Commoning]]
  
#+begin_quote
Among millions of Black women in Africa, the Caribbean, and North America, [[id:2fb379da-682a-44b3-9635-07260755115c][ROSCA]]s, or 'rotating savings and credit associations', are trusted alternatives to racialized, exclusionary systems of formal banking. The self-organized, informal pooling of money among friends and neighbors offer a way to help people amass the money to buy a used car, pay for school, and meet other household expenses. Professor Hossein of the University of Toronto at Scarborough, in Ontario, Canada, discusses the resourcefulness and resilience of the Black social economy despite attacks by many state authorities and mainstream banks.
#+end_quote

[[id:59ef4476-71ec-4354-8ba0-631c51e0b81a][Social and Solidarity economy]]
