:PROPERTIES:
:ID:       219fbba1-588b-4426-87da-d800648b0331
:mtime:    20220102225038
:ctime:    20220102225038
:END:
#+TITLE: Food is a human right, not a commodity
#+CREATED: [2022-01-02 Sun]
#+LAST_MODIFIED: [2022-01-02 Sun 22:55]

Heard first from [[id:f2f99bbd-f724-4e54-818d-6eaf6f9f5ad3][Jose Luis Vivero Pol]] on [[id:29757499-50d0-4203-8787-a9e77a01642b][Jose Luis Vivero Pol: Treating Food as Commons, Not Commodites]]

* Epistemic status

+ Type :: [[id:ba86731e-84ea-4f0a-9dc7-dee4743bd8b2][claim]]
+ Gut feeling :: 10
+ Confidence :: 5
