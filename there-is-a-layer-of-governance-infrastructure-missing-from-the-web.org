:PROPERTIES:
:ID:       cba713bf-4fe3-4187-8226-871729b43a7e
:mtime:    20211127120948 20211122205012
:ctime:    20211122205012
:END:
#+title: There is a layer of governance infrastructure missing from the web
#+CREATED: [2021-05-09 Sun 22:56]
#+LAST_MODIFIED: [2021-11-27 Sat 12:09]


* Resources
+ [[https://gitlab.com/medlabboulder/communityrule/-/wikis/Points-of-inspiration][Points of inspiration · Wiki · Media Enterprise Design Lab / CommunityRule · ...]]
+ [[id:17ad2816-fec5-4d3f-a8b0-c55e5c9512e9][Metagov]] [[https://blog.opencollective.com/metagov-gateway/][Govern your Collective with Metagov Gateway]] 

* Log

** [2021-05-31 Mon]  
   Read [[id:3d2df42e-cc0c-4d25-982e-ef3f2fa6a99c][Governing the Information Commons]].
   Linking in [[id:874cb75d-eb8e-4f78-81d9-4afaed3ed986][Online Communities Are Still Catching Up to My Mother's Garden Club]].

** [2021-05-17 Mon] 
   
+ [[id:6e86c2e0-ae04-4e59-b4cb-7b6768941fb9][Flancian]] asks a good question on the social.coop tech channel - why does [[id:57910eea-4f81-42a1-a4eb-6e0076c3dc92][social.coop]] not use [[id:e710d724-14ef-4e5a-9ed1-00d1a10f503e][Mastodon]] to coordinate [[id:ee227d4a-4ef2-436f-a68e-9877f741c0d9][governance]]?
  + In my opinion because Mastodon is not very good for governance.  It only has polls and even that is fairly new.
  + In fact I don't find Mastodon very good for even just conversation.
  + That said, having two/more platforms to coordinate things is definitely a huge piece of friction to involving a community in governance.
  + h's mockups from way back when, are interesting: https://social.coop/@h/1868600 https://social.coop/@h/1870490
    
** [2021-05-09 Sun] 
   Listened to [[id:74096166-bbcb-4877-8cd2-ac46f1e12a16][Nathan Schneider on Cooperatives and Digital Governance]].
  
