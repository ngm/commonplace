:PROPERTIES:
:ID:       3043872e-4aff-4fff-bb86-11cdf9f5b624
:mtime:    20211127120820 20210724222235
:ctime:    20210724222235
:END:
#+title: Two Loop Model
#+CREATED: [2021-05-01 Sat 11:49]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

A [[id:949b16e0-6cc3-4be4-91b3-c09ac539ad5b][theory of change]].

#+ATTR_HTML: :width 100%
 [[file:images/two-loop-model.png]]
 
+ [[https://www.systemsinnovation.io/post/two-loop-model][Two Loop Model]] 
+ [[https://medium.com/thefarewellfund/hospicing-the-old-16e537396c4b][Hospicing The Old]]
+ [[https://berkana.org/resources/pioneering-a-new-paradigm/][Pioneering a New Paradigm - Berkana Institute]]
+ [[https://www.youtube.com/watch?v=2jTdZSPBRRE][How I Became a Localist | Deborah Frieze | TEDxJamaicaPlain - YouTube]] 

