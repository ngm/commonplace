:PROPERTIES:
:ID:       a9500cb8-c555-4a6f-a750-9af6ae29a042
:mtime:    20211127120927 20210724222235
:ctime:    20210724222235
:END:
#+title: Pulling stats out of org-roam

Inspired by Alex Kehayias' note on [[https://notes.alexkehayias.com/querying-stats-from-org-roam/][Querying Stats From Org-Roam]], I'm going to use org-babel inline evaluation of sql to pull out and publish some stats too.

* How many notes do I have?
  
#+BEGIN_SRC sqlite :db org-roam.db :colnames yes :results value :wrap example :exports both
  select count(*) as number_of_notes from files
#+END_SRC

#+RESULTS:
#+begin_example
| number_of_notes |
|-----------------|
|            1019 |
#+end_example

* Notes over time

  Hmm.  Alex gets notes per month from the date stamp in the headline.  I've removed that as I don't want it in my published filenames.  That's a bit annoying then, does it remove ability to do querying based on trends over time.

The ~files~ table does hold mtime and atime.  So maybe I can do something with mtime at least.
