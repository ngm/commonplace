:PROPERTIES:
:ID:       4c0b69f5-5550-441c-be66-13ec186ecf81
:mtime:    20211127120834 20210724222235
:ctime:    20200923161404
:END:
#+title: Outlining vs bullet-pointing in org-mode

A great overview from rooster of a distinction you can make between outlining and bullet-pointing in [[id:926ab200-dfcb-4eaa-9bc2-8e9bfb47da03][org-mode]].  You can see something like workflowy or [[id:0ba0c8ce-613b-4145-bbb3-e952c4fa41e8][Roam]] as a bullet-pointer.

- [[https://org-roam.discourse.group/t/plain-text-vs-outliner-focused-features-discussion/110/8][Plain-text vs Outliner Focused Features Discussion - Development - Org-roam]]

See also [[id:663b1854-4f6d-402f-928a-683f8b0b1703][Outlining vs writing]].

