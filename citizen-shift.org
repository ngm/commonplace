:PROPERTIES:
:ID:       f4d90ff4-0957-46b9-b753-c12a29323010
:mtime:    20211127121013 20210724222235
:ctime:    20210724222235
:END:
#+title: Citizen Shift
#+CREATED: [2021-05-02 Sun 15:47]
#+LAST_MODIFIED: [2021-11-27 Sat 12:10]

+ https://www.citizenshift.info/

 #+begin_quote
 this approach prompts us to think of ourselves instead as Citizens, and to ask what it is we can all participate in and contribute to. The core proposition is that this shift in thinking provides a far more generative platform for ideas, innovation, social and environmental justice, and indeed economic prosperity.
 #+end_quote
