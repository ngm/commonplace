:PROPERTIES:
:ID:       fdb7e9a3-44a1-4c88-ae62-a9cd7a2bc377
:mtime:    20241028071604 20240207182044
:ctime:    20240207182044
:END:
#+TITLE: The Dawn of Everything
#+CREATED: [2024-02-07 Wed]
#+LAST_MODIFIED: [2024-02-07 Wed 18:20]

* Criticism

#+begin_quote
However, Graeber and Wengrow's overall narrative rejects basic materialist analysis in favor of a muddy historical idealism and often deploys questionable rhetoric in the place of rigorous argument, as Walter Scheidel has pointed out in: "Resetting History's Dial? A Critique of David Graeber and David Wengrow, The Dawn of Everything: A New History of Humanity",

-- [[id:831702a8-0348-49bb-ab60-fd8f164b5a19][Forest and Factory]]
#+end_quote
