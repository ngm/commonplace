:PROPERTIES:
:ID:       5e54980d-c493-4b46-a0fd-59fb91359308
:mtime:    20240202101340
:ctime:    20240202101340
:END:
#+TITLE: Red Enlightenment: On Socialism, Science and Spirituality
#+CREATED: [2024-02-02 Fri]
#+LAST_MODIFIED: [2024-02-02 Fri 10:21]

+ A :: [[id:c9e1c38c-ae84-453a-8940-d25667f14d7a][book]]
+ Found at :: https://repeaterbooks.com/product/red-enlightenment/
+ Written by :: [[id:754d52c7-3cae-412a-bb8a-82246f333636][Graham Jones]]

[[id:dc3a1608-a607-4a1d-9178-07e94ea1b1ea][Red Enlightenment]]: On [[id:1574c602-e123-4f31-afc4-31b92e874e49][Socialism]], [[id:2f3b1753-c4ca-4e67-a584-518fd08d2ba7][Science]] and [[id:ddd4659c-ef8d-4dcd-9301-d5ce5725cb60][Spirituality]].

#+begin_quote
Why we need a materialist spirituality for the secular left, and how to build one.

The left commonly rejects [[id:760b1c51-c45f-46e8-b0f7-016abd90dd13][religion]] and spirituality as counter-revolutionary forces, citing Marx’s famous dictum that “[[id:48ea7a99-d19a-4c1b-99cd-ae06e590cce1][religion is the opium of the people]].” Yet forms of spirituality have motivated struggles throughout history, ranging from medieval peasant uprisings and colonial slave revolts, to South American [[id:9716ce72-8f81-4e50-b677-2d4a4cf083e2][liberation theology]] and the US [[id:c80897af-6926-44c1-829b-4588d274fdff][civil rights movement]]. And in a world where religion is growing, and political movements are ridden with conflict, burnout, and failure, what can the left learn from religion?

Red Enlightenment argues not only for a deepened understanding of religious matters, but calls for the secular left to develop its own spiritual perspectives. It proposes a materialist spirituality built from socialist and scientific sources, finding points of contact with the global history of philosophy and religion.

From [[id:d6a5c5a1-7912-4ea4-8b67-ce62e73a69a5][cybernetics]] to liberation theology, from ancient Indian and Chinese philosophy to Marxist [[id:0f3004f9-f547-4449-b49e-c0173eb221d8][dialectical materialism]], from traditional religious practices to contemporary art, music, and film, Red Enlightenment sets out a plausible secular spirituality, a new socialist praxis, and a utopian vision.
#+end_quote
