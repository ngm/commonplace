:PROPERTIES:
:ID:       26911c08-b63a-4a53-91ee-df835eb333df
:mtime:    20211127120814 20210724222235
:ctime:    20210724222235
:END:
#+title: community gardens
#+CREATED: [2021-07-07 Wed 22:00]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

#+begin_quote
The communal and social benefits of gardening, as well as the nascent political potential of gardens and other common urban spaces, are often celebrated. Researchers looking at Glasgow’s community gardens saw the gardens as spaces of social transformation that held the potential for a new political practice. “Enabled by an interlocking process of community and spatial production, this form of citizen participation encourages us to reconsider our relationships with one another, our environment, and what constitutes effective political practice.”

-- [[id:e41f8a67-173a-470f-bb94-2d50c4693154][Undoing Optimization: Civic Action in Smart Cities]]
#+end_quote

