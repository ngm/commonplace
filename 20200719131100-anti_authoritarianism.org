:PROPERTIES:
:ID:       8cf3e787-6482-47f3-b2d5-55eb83568350
:mtime:    20211127120909 20210724222235
:ctime:    20200719131100
:END:
#+title: Anti-authoritarianism

#+begin_quote
Anti-authoritarianism is opposition to authoritarianism, which is defined as "a form of social organisation characterised by submission to authority", "favoring complete obedience or subjection to authority as opposed to individual freedom" and to authoritarian government. Anti-authoritarians usually believe in full equality before the law and strong [[id:bc0dfe40-3129-4e13-9313-e4e516a748d3][civil liberties]]. 

-- [[https://en.wikipedia.org/wiki/Anti-authoritarianism][Anti-authoritarianism - Wikipedia]] 
#+end_quote

#+begin_quote
Sometimes the term is used interchangeably with [[id:764297f5-01e2-4f92-adf2-09a02eeee7ef][anarchism]], an ideology which entails opposing authority or hierarchical organization in the conduct of human relations, including the state system.

-- [[https://en.wikipedia.org/wiki/Anti-authoritarianism][Anti-authoritarianism - Wikipedia]] 
#+end_quote

