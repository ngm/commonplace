:PROPERTIES:
:ID:       677d63b2-a098-4783-a453-0487102487c1
:mtime:    20211127120848 20210724222235
:ctime:    20210724222235
:END:
#+title: North West Mutual
#+CREATED: [2021-02-25 Thu 19:35]
#+LAST_MODIFIED: [2021-11-27 Sat 12:08]

Mentioned in [[id:2be448a3-fa31-4479-8df1-9766d233f322][Community Wealth Building 2.0]], this will cover the North-West, running up to the Scottish Borders.  So Cumbria will be part of that.
