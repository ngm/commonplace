:PROPERTIES:
:ID:       0e0d162a-30e7-41c9-bd60-5441deab9ab9
:mtime:    20220209194000 20211127120801 20211006212259
:ctime:    20211006212259
:END:
#+TITLE: Levelling up
#+CREATED: [2021-10-06 Wed]
#+LAST_MODIFIED: [2022-02-09 Wed 19:40]

Tory nonsense.

- [[https://www.theguardian.com/commentisfree/2021/sep/19/level-up-uk-regions-local-authority-funding][Want to ‘level up’ the UK? Just give places the power and money they need | J...]] 

- [[https://www.theguardian.com/commentisfree/2021/oct/06/andy-burnham-boris-johnson-levelling-up-north-of-england][Boris Johnson now has the chance to make ‘levelling up’ mean something | Andy...]]


"Real levelling-up means going local"
[[https://www.theguardian.com/theobserver/commentisfree/2022/jan/09/real-levelling-up-means-going-local-letters][Letters: real levelling-up means going local | Inequality | The Guardian]]
